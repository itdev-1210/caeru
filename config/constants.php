<?php

return [

    // This File contains many constants


    /*
    | Genders
    |--------------------
    | 性別
    */
    'male'                              => 1,
    'female'                            => 2,

    /*
    | Maebarai Settings
    |--------------------
    | 前払
    */
    'maebarai_enable'                           => 1,
    'maebarai_disable'                          => 0,

    /*
    | Payment Status
    |--------------------
    | 性別
    */
    'direct'                              => 1,
    'indirect'                            => 2,

    /*
    | Payment Errors
    |--------------------
    | 性別
    */
    'error_none'                        => 0,

    /*
    | Schedule Types
    |--------------------
    | 就労形態
    */
    'normal_schedule'              => 1,
    'monthly_based_schedule'       => 2,
    'yearly_based_schedule'        => 3,
    'flexible_schedule'            => 4,



    /*
    | Employment Types
    |--------------------
    | 採用形態
    */
    'official_employee'                 => 1,
    'contracted_employee'               => 2,
    'part_time_1_employee'              => 3,
    'part_time_2_employee'              => 4,
    'dispatched_employee'               => 5,



    /*
    | Salary Types
    |--------------------
    | 給与形態
    */
    'monthly_salary'                    => 1,
    'hourly_salary'                     => 2,
    'daily_salary'                      => 3,



    /*
    | Work Statuses
    |--------------------
    | 雇用状態
    */
    'working'                           => 1,
    'on_vacation'                       => 2,
    'retired'                           => 3,



    /*
    | Paid Holiday Bonus Types
    |--------------------
    | 有給休暇
    */
    'normal_bonus'                       => 1,
    'four_days_per_week_bonus'           => 2,
    'three_days_per_week_bonus'          => 3,
    'two_days_per_week_bonus'            => 4,
    'one_day_per_week_bonus'             => 5,
    'manually_input_bonus'               => 6,



    /**
     * Constants for the day of week Configuration
     */
    'monday'                             => 1,
    'tuesday'                            => 2,
    'wednesday'                          => 3,
    'thursday'                           => 4,
    'friday'                             => 5,
    'saturday'                           => 6,
    'sunday'                             => 0,


    /*
    | GPS device type
    |----------------------
    | GPS端末種類
    */
    'gps_android'           => 1,
    'gps_iphone'            => 2,
    'gps_docomo_device'     => 3,

];

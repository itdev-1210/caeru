<?php

use Illuminate\Database\Seeder;

class TodofukensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todofukens')->insert([

            [ 'id' => 1, 'name' => '北海道', 'unicode_name' => 'Hokkaido' ],
            [ 'id' => 2, 'name' => '青森県', 'unicode_name' => 'Aomori' ],
            [ 'id' => 3, 'name' => '秋田県', 'unicode_name' => 'Akita' ],
            [ 'id' => 4, 'name' => '岩手県', 'unicode_name' => 'Iwate' ],
            [ 'id' => 5, 'name' => '宮城県', 'unicode_name' => 'Miyagi' ],
            [ 'id' => 6, 'name' => '山形県', 'unicode_name' => 'Yamagata' ],
            [ 'id' => 7, 'name' => '福島県', 'unicode_name' => 'Fukushima' ],
            [ 'id' => 8, 'name' => '茨城県', 'unicode_name' => 'Ibaraki' ],
            [ 'id' => 9, 'name' => '栃木県', 'unicode_name' => 'Tochigi' ],
            [ 'id' => 10, 'name' => '群馬県', 'unicode_name' => 'Gunma' ],
            [ 'id' => 11, 'name' => '埼玉県', 'unicode_name' => 'Saitama' ],
            [ 'id' => 12, 'name' => '千葉県', 'unicode_name' => 'Chiba' ],
            [ 'id' => 13, 'name' => '東京都', 'unicode_name' => 'Tokyo' ],
            [ 'id' => 14, 'name' => '神奈川県', 'unicode_name' => 'Kanagawa' ],
            [ 'id' => 15, 'name' => '新潟県', 'unicode_name' => 'Niigata' ],
            [ 'id' => 16, 'name' => '富山県', 'unicode_name' => 'Toyama' ],
            [ 'id' => 17, 'name' => '石川県', 'unicode_name' => 'Ishikawa' ],
            [ 'id' => 18, 'name' => '福井県', 'unicode_name' => 'Fukui' ],
            [ 'id' => 19, 'name' => '山梨県', 'unicode_name' => 'Yamanashi' ],
            [ 'id' => 20, 'name' => '長野県', 'unicode_name' => 'Nagano' ],
            [ 'id' => 21, 'name' => '静岡県', 'unicode_name' => 'Shizuoka' ],
            [ 'id' => 22, 'name' => '岐阜県', 'unicode_name' => 'Gifu' ],
            [ 'id' => 23, 'name' => '愛知県', 'unicode_name' => 'Aichi' ],
            [ 'id' => 24, 'name' => '三重県', 'unicode_name' => 'Mie' ],
            [ 'id' => 25, 'name' => '京都府', 'unicode_name' => 'Kyoto' ],
            [ 'id' => 26, 'name' => '滋賀県', 'unicode_name' => 'Shiga' ],
            [ 'id' => 27, 'name' => '奈良県', 'unicode_name' => 'Nara' ],
            [ 'id' => 28, 'name' => '和歌山県', 'unicode_name' => 'Wakayama' ],
            [ 'id' => 29, 'name' => '大阪府', 'unicode_name' => 'Osaka' ],
            [ 'id' => 30, 'name' => '兵庫県', 'unicode_name' => 'Hyōgo' ],
            [ 'id' => 31, 'name' => '岡山県', 'unicode_name' => 'Okayama' ],
            [ 'id' => 32, 'name' => '広島県', 'unicode_name' => 'Hiroshima' ],
            [ 'id' => 33, 'name' => '鳥取県', 'unicode_name' => 'Tottori' ],
            [ 'id' => 34, 'name' => '島根県', 'unicode_name' => 'Shimane' ],
            [ 'id' => 35, 'name' => '山口県', 'unicode_name' => 'Yamaguchi' ],
            [ 'id' => 36, 'name' => '徳島県', 'unicode_name' => 'Tokushima' ],
            [ 'id' => 37, 'name' => '高知県', 'unicode_name' => 'Kōchi' ],
            [ 'id' => 38, 'name' => '香川県', 'unicode_name' => 'Kagawa' ],
            [ 'id' => 39, 'name' => '愛媛県', 'unicode_name' => 'Ehime' ],
            [ 'id' => 40, 'name' => '福岡県', 'unicode_name' => 'Fukuoka' ],
            [ 'id' => 41, 'name' => '長崎県', 'unicode_name' => 'Nagasaki' ],
            [ 'id' => 42, 'name' => '熊本県', 'unicode_name' => 'Kumamoto' ],
            [ 'id' => 43, 'name' => '大分県', 'unicode_name' => 'Ōita' ],
            [ 'id' => 44, 'name' => '佐賀県', 'unicode_name' => 'Saga' ],
            [ 'id' => 45, 'name' => '宮崎県', 'unicode_name' => 'Miyazaki' ],
            [ 'id' => 46, 'name' => '鹿児島県', 'unicode_name' => 'Kagoshima' ],
            [ 'id' => 47, 'name' => '沖縄県', 'unicode_name' => 'Okinawa' ]

        ]);
    }
}

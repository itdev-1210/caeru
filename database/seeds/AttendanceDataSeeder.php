<?php

use Illuminate\Database\Seeder;
use App\Employee;
use App\EmployeeWorkingDay;
use App\WorkingTimestamp;
use App\PlannedSchedule;
use App\EmployeeWorkingInformation;

class AttendanceDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * This seeder should be call after the 'developer' mode has already been migrated once. Use should change settings Company and WorkLocation properly before seed this file.
     * You can customize this seed file however you like (should not commit the changes though, just keep the changes at local).
     *
     * @return void
     */
    public function run()
    {
        // Right now, it just seed data for the first employee only.
        // この場合は、従業員の初めの百人に反映する（従業員の本id必要がない）
        $employees = Employee::paginate(100);

        // この場合は、従業員の一人に反映する（従業員の本id必要がある）
        // $employees = Employee::where('id', 1)->get();

        // この場合は、本id１の従業員から本id10の従業員までに反映する（従業員の本id必要がある）
        // $employees = Employee::where('id', '>=', 1)->where('id', '<=', 10)->get();

        // Save the current company code to session, so that the events can use it later.
        $buffer = explode('_', \DB::getDatabaseName());
        $company_code = $buffer[count($buffer) - 1];
        session(['current_company_code' => $company_code]);

        // Use for when want to seed schedule with work_address
        // $work_address_offset = 0;

        // First, we need to seed a PlannedSchedule
        foreach($employees as $employee) {

            // Use for when want to seed schedule with work_address
            // $work_address_offset = ($work_address_offset >= 10) ? 0 : $work_address_offset;
            // $work_address_ids = $employee->workLocation->workAddresses->pluck('id')->toArray();

            $schedule = new PlannedSchedule([
                'employee_id'       => $employee->id,
                'work_location_id'  => $employee->workLocation->id,

                // Use for when want to seed schedule with work_address
                // 'work_address_id'   => $work_address_ids[$work_address_offset],
                // 'candidating_type' => 0,

                'prioritize_company_calendar' => true,
                'working_days_of_week' => [1,1,1,1,1,1,1],

                // スケジュールの開始日、働く時間をいつからにする
                'start_date' => '2019-01-01',
                ////

                'rest_on_holiday' => false,
                'start_work_time' => '09:30:00',
                'end_work_time' => '18:30:00',
                'break_time' => 60,
                'working_hour' => '08:00:00'
            ]);
            $schedule->save();

            // Use for when want to seed schedule with work_address
            // $work_address_offset++;
        }

        // With that PlannedSchedule,the EmployeeWorkingDay and EmployeeWorkingInformation will be generated.
        // $working_infos = EmployeeWorkingInformation::with('employeeWorkingDay')->whereHas('employeeWorkingDay', function($query) use ($first_employee) {
        //     return $query->where('employee_id', $first_employee->id);
        // // })->get();
        // foreach($working_infos as $working_info) {
        //     $working_info->timestamped_start_work_time = $working_info->employeeWorkingDay->date . ' 09:21:00';
        //     $working_info->timestamped_end_work_time = $working_info->employeeWorkingDay->date . ' 18:37:00';
        //     $working_info->real_work_location_id = $first_employee->workLocation->id;
        //     $working_info->manually_modified = true;
        //     $working_info->save();
        // }

        // This script should be run once, right after refresh the database
        // $working_infos = EmployeeWorkingInformation::with('employeeWorkingDay.employee.workLocation')->get();
        // foreach($working_infos as $working_info) {
        //     $working_info->timestamped_start_work_time = $working_info->employeeWorkingDay->date . ' 09:21:00';
        //     $working_info->timestamped_end_work_time = $working_info->employeeWorkingDay->date . ' 18:37:00';
        //     $working_info->real_work_location_id = $working_info->employeeWorkingDay->employee->workLocation->id;
        //     $working_info->manually_modified = true;
        //     $working_info->save();
        // }

        // 働く時間をいつまでにする
        $employee_working_days = EmployeeWorkingDay::with('employee.workLocation.workAddresses')
        ->whereIn('employee_working_days.employee_id', $employees->pluck('id')->toArray())
        ->where('date', '<', '2019-03-01')

        // Use for when want to seed schedule with work_address
        // ->join('employees', 'employees.id', '=', 'employee_working_days.employee_id')
        // ->join('planned_schedules', 'planned_schedules.employee_id', '=', 'employees.id')
        // ->orderBy('employees.id')
        // ->orderBy('employees.work_location_id')

        ->get(['employee_working_days.*',]); // , 'work_address_id']);  Use for when want to seed schedule with work_address


        foreach($employee_working_days as $working_day) {

            $start_work = new WorkingTimestamp([
                'enable' => true,
                'name_id' => 'Asia/Seoul',
                'raw_date_time_value' => $working_day->date . ' 09:30:00',
                'timestamped_type' => WorkingTimestamp::START_WORK,
                'registerer_type' => WorkingTimestamp::MANAGER,
                'registerer_id' => 1,
                'work_location_id' => $working_day->employee->workLocation->id,

                // Use for when want to seed schedule with work_address
                // 'work_address_id' => $working_day->work_address_id,

                'employee_working_day_id' => $working_day->id,
            ]);
            $end_work = new WorkingTimestamp([
                'enable' => true,
                'name_id' => 'Asia/Seoul',
                'raw_date_time_value' => $working_day->date . ' 18:30:00',
                'timestamped_type' => WorkingTimestamp::END_WORK,
                'registerer_type' => WorkingTimestamp::MANAGER,
                'registerer_id' => 1,
                'work_location_id' => $working_day->employee->workLocation->id,

                // Use for when want to seed schedule with work_address
                // 'work_address_id' => $working_day->work_address_id,

                'employee_working_day_id' => $working_day->id,
            ]);

            $start_work->save();
            $end_work->save();
        }

        // Clean up
        session()->forget('current_company_code');
    }
}

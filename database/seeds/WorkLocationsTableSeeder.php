<?php

use Illuminate\Database\Seeder;

class WorkLocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $head_office = factory(\App\WorkLocation::class)->states('head_office')->make();

        // If you want to number the work locations, uncomment these lines
        // $head_office->name = $head_office->name . '1';
        // $branches_count = 2;

        $head_office->company()->associate(\App\Company::first())->save();

        $branches = factory(\App\WorkLocation::class,29)->states('branches')->make();
        foreach ($branches as $branch) {

            // If you want to number the work locations, uncomment these lines
            // $branch->name = $branch->name . $branches_count;
            // $branches_count++;

            $branch->company()->associate(\App\Company::first())->save();
        };
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeWorkTimePerDayTimersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_work_time_per_day_timers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code', 32);
            $table->integer('employee_id');
            $table->float('work_time_per_day');
            $table->date('due_day');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_work_time_per_day_timers');
    }
}

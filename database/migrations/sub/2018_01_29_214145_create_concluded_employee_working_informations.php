<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcludedEmployeeWorkingInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concluded_employee_working_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_working_day_id');
            $table->integer('employee_working_information_id');
            $table->integer('work_status_id')->nullable();
            $table->integer('rest_status_id')->nullable();
            $table->integer('planned_work_location_id')->nullable();
            $table->integer('real_work_location_id')->nullable();
            $table->integer('planned_work_address_id')->nullable();
            $table->integer('real_work_address_id')->nullable();
            $table->dateTime('planned_start_work_time')->nullable();
            $table->dateTime('timestamped_start_work_time')->nullable();
            $table->dateTime('real_start_work_time')->nullable();
            $table->dateTime('planned_end_work_time')->nullable();
            $table->dateTime('timestamped_end_work_time')->nullable();
            $table->dateTime('real_end_work_time')->nullable();
            $table->integer('planned_break_time')->nullable();
            $table->integer('real_break_time')->nullable();
            $table->integer('planned_night_break_time')->nullable();
            $table->integer('real_night_break_time')->nullable();
            $table->integer('planned_go_out_time')->nullable();
            $table->integer('real_go_out_time')->nullable();
            $table->integer('planned_total_late_and_leave_early')->nullable();
            $table->integer('real_total_late_and_leave_early')->nullable();
            $table->integer('planned_total_early_arrive_and_overtime')->nullable();
            $table->integer('real_total_early_arrive_and_overtime')->nullable();
            $table->integer('planned_work_span')->nullable();
            $table->integer('real_work_span')->nullable();
            $table->integer('planned_working_hour')->nullable();
            $table->integer('real_working_hour')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concluded_employee_working_informations');
    }
}

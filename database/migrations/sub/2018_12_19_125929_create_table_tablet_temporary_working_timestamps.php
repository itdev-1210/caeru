<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTabletTemporaryWorkingTimestamps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tablet_temporary_working_timestamps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code', 32);
            $table->string('device_id')->nullable();
            $table->integer('work_location_id')->nullable();
            $table->integer('work_address_id')->nullable();
            $table->bigInteger('timestamped_value');
            $table->string('card_code')->nullable();
            $table->integer('timestamped_type');
            $table->string('name_id')->nullable();
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->string('request_key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tablet_temporary_working_timestamps');
    }
}

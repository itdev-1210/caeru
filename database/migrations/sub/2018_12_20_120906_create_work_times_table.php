<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_times', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('work_location_id');
            $table->string('name')->collation('utf8mb4_bin');
            $table->time('start_work_time')->nullable();
            $table->time('end_work_time')->nullable();
            $table->integer('break_time')->nullable();
            $table->integer('night_break_time')->nullable();
            $table->time('working_hour')->nullable();
            $table->integer('visible')->default(1);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_times');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCachedPaidHolidayInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cached_paid_holiday_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paid_holiday_information_id');
            $table->boolean('old');
            $table->float('attendance_rate')->nullable();
            $table->float('consumed_paid_holidays')->nullable();
            $table->float('consumed_paid_holidays_hour')->nullable();
            $table->float('planned_consumed_paid_holidays')->nullable();
            $table->float('planned_consumed_paid_holidays_hour')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cached_paid_holiday_informations');
    }
}

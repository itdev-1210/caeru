<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcludedEmployeeWorkingMonthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concluded_employee_working_months', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->integer('business_month');
            $table->integer('business_year');
            // $table->boolean('all_level_one_concluded')->nullable();
            // $table->boolean('concluded_level_two')->nullable();
            $table->integer('concluded_level_two_manager_id')->nullable();

            // Below attributes may not be neccessary
            $table->integer('have_schedule_days')->nullable();
            $table->integer('planned_work_days')->nullable();
            $table->integer('real_work_days')->nullable();
            $table->float('planned_sum_working_time')->nullable();
            $table->float('real_sum_working_time')->nullable();
            $table->float('sum_work_span_time_from_schedules')->nullable();
            $table->float('planned_work_span_time')->nullable();
            $table->float('real_work_span_time')->nullable();
            $table->float('planned_paid_rest_time')->nullable();
            $table->float('real_paid_rest_time')->nullable();
            $table->float('planned_unpaid_rest_time')->nullable();
            $table->float('real_unpaid_rest_time')->nullable();
            $table->float('planned_non_work_time')->nullable();
            $table->float('real_non_work_time')->nullable();
            $table->float('planned_overtime_work_time')->nullable();
            $table->float('real_overtime_work_time')->nullable();
            $table->float('planned_night_work_time')->nullable();
            $table->float('real_night_work_time')->nullable();
            $table->integer('planned_non_work_days')->nullable();
            $table->integer('real_non_work_days')->nullable();
            $table->float('planned_taken_paid_rest_days')->nullable();
            $table->float('real_taken_paid_rest_days')->nullable();
            $table->float('planned_taken_paid_rest_time')->nullable();
            $table->float('real_taken_paid_rest_time')->nullable();
            $table->float('planned_remaining_paid_rest_days')->nullable();
            $table->float('real_remaining_paid_rest_days')->nullable();
            $table->float('planned_remaining_paid_rest_time')->nullable();
            $table->float('real_remaining_paid_rest_time')->nullable();
            // $table->bigInteger('current_work_time_per_day');
            $table->integer('total_request_number')->nullable();
            $table->integer('considering_number')->nullable();
            $table->integer('approved_number')->nullable();
            $table->integer('timestamp_errors_number')->nullable();
            $table->integer('confirm_needed_errors_number')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concluded_employee_working_months');
    }
}

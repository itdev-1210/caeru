<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('color_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('colorable_type')->nullable();
            $table->integer('colorable_id')->nullable();
            $table->string('field_name');
            $table->string('field_css_class');
            $table->string('field_fake_value')->nullable();
            $table->boolean('enable')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('color_statuses');
    }
}

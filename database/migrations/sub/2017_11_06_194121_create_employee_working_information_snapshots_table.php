<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeWorkingInformationSnapshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_working_information_snapshots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_working_day_id');
            $table->integer('employee_working_information_id')->nullable();
            $table->integer('soon_to_be_EWI_id')->nullable();
            $table->integer('target_absorb_EWI_id')->nullable();

            $table->integer('left_status');
            $table->integer('left_timestamped_start_work_time_work_location_id')->nullable();
            $table->integer('left_timestamped_start_work_time_work_address_id')->nullable();
            $table->date('left_timestamped_start_work_date')->nullable();
            $table->time('left_timestamped_start_work_time')->nullable();
            $table->integer('left_timestamped_end_work_time_work_location_id')->nullable();
            $table->integer('left_timestamped_end_work_time_work_address_id')->nullable();
            $table->date('left_timestamped_end_work_date')->nullable();
            $table->time('left_timestamped_end_work_time')->nullable();
            $table->integer('left_work_status_id')->nullable();
            $table->integer('left_rest_status_id')->nullable();
            $table->dateTime('left_paid_rest_time_start')->nullable();
            $table->dateTime('left_paid_rest_time_end')->nullable();
            $table->time('left_paid_rest_time_period')->nullable();
            $table->date('left_switch_planned_schedule_target')->nullable();
            $table->integer('left_planned_work_location_id')->nullable();
            $table->integer('left_planned_work_address_id')->nullable();
            $table->integer('left_real_work_location_id')->nullable();
            $table->integer('left_real_work_address_id')->nullable();
            $table->dateTime('left_planned_early_arrive_start')->nullable();
            $table->dateTime('left_real_early_arrive_start')->nullable();
            $table->dateTime('left_planned_early_arrive_end')->nullable();
            $table->dateTime('left_real_early_arrive_end')->nullable();
            $table->dateTime('left_planned_work_span_start')->nullable();
            $table->dateTime('left_real_work_span_start')->nullable();
            $table->dateTime('left_planned_work_span_end')->nullable();
            $table->dateTime('left_real_work_span_end')->nullable();
            $table->time('left_planned_work_span')->nullable();
            $table->time('left_real_work_span')->nullable();
            $table->dateTime('left_planned_overtime_start')->nullable();
            $table->dateTime('left_real_overtime_start')->nullable();
            $table->dateTime('left_planned_overtime_end')->nullable();
            $table->dateTime('left_real_overtime_end')->nullable();
            $table->integer('left_planned_break_time')->nullable();
            $table->integer('left_real_break_time')->nullable();
            $table->integer('left_planned_night_break_time')->nullable();
            $table->integer('left_real_night_break_time')->nullable();
            $table->integer('left_planned_late_time')->nullable();
            $table->integer('left_real_late_time')->nullable();
            $table->integer('left_planned_early_leave_time')->nullable();
            $table->integer('left_real_early_leave_time')->nullable();
            $table->integer('left_planned_go_out_time')->nullable();
            $table->integer('left_real_go_out_time')->nullable();
            $table->text('left_requester_note')->nullable();
            $table->text('left_approver_note')->nullable();
            $table->dateTime('left_requester_modify_time')->nullable();
            $table->dateTime('left_approval_time')->nullable();
            $table->integer('left_approver_id')->nullable();
            $table->boolean('left_soon_to_be_absorb')->default(false);
            $table->boolean('left_not_include_break_time_when_display_planned_time')->nullable();

            $table->integer('right_status');
            $table->integer('right_timestamped_start_work_time_work_location_id')->nullable();
            $table->integer('right_timestamped_start_work_time_work_address_id')->nullable();
            $table->date('right_timestamped_start_work_date')->nullable();
            $table->time('right_timestamped_start_work_time')->nullable();
            $table->integer('right_timestamped_end_work_time_work_location_id')->nullable();
            $table->integer('right_timestamped_end_work_time_work_address_id')->nullable();
            $table->date('right_timestamped_end_work_date')->nullable();
            $table->time('right_timestamped_end_work_time')->nullable();
            $table->integer('right_work_status_id')->nullable();
            $table->integer('right_rest_status_id')->nullable();
            $table->dateTime('right_paid_rest_time_start')->nullable();
            $table->dateTime('right_paid_rest_time_end')->nullable();
            $table->time('right_paid_rest_time_period')->nullable();
            $table->date('right_switch_planned_schedule_target')->nullable();
            $table->integer('right_planned_work_location_id')->nullable();
            $table->integer('right_planned_work_address_id')->nullable();
            $table->integer('right_real_work_location_id')->nullable();
            $table->integer('right_real_work_address_id')->nullable();
            $table->dateTime('right_planned_early_arrive_start')->nullable();
            $table->dateTime('right_real_early_arrive_start')->nullable();
            $table->dateTime('right_planned_early_arrive_end')->nullable();
            $table->dateTime('right_real_early_arrive_end')->nullable();
            $table->dateTime('right_planned_work_span_start')->nullable();
            $table->dateTime('right_real_work_span_start')->nullable();
            $table->dateTime('right_planned_work_span_end')->nullable();
            $table->dateTime('right_real_work_span_end')->nullable();
            $table->time('right_planned_work_span')->nullable();
            $table->time('right_real_work_span')->nullable();
            $table->dateTime('right_planned_overtime_start')->nullable();
            $table->dateTime('right_real_overtime_start')->nullable();
            $table->dateTime('right_planned_overtime_end')->nullable();
            $table->dateTime('right_real_overtime_end')->nullable();
            $table->integer('right_planned_break_time')->nullable();
            $table->integer('right_real_break_time')->nullable();
            $table->integer('right_planned_night_break_time')->nullable();
            $table->integer('right_real_night_break_time')->nullable();
            $table->integer('right_planned_late_time')->nullable();
            $table->integer('right_real_late_time')->nullable();
            $table->integer('right_planned_early_leave_time')->nullable();
            $table->integer('right_real_early_leave_time')->nullable();
            $table->integer('right_planned_go_out_time')->nullable();
            $table->integer('right_real_go_out_time')->nullable();
            $table->text('right_requester_note')->nullable();
            $table->text('right_approver_note')->nullable();
            $table->dateTime('right_requester_modify_time')->nullable();
            $table->dateTime('right_approval_time')->nullable();
            $table->integer('right_approver_id')->nullable();
            $table->boolean('right_soon_to_be_absorb')->default(false);
            $table->boolean('right_not_include_break_time_when_display_planned_time')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_working_information_snapshots');
    }
}

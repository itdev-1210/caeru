<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaebaraiTransactionResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maebarai_transaction_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('maebarai_history_id');
            $table->integer('status_code');
            $table->string('message');
            $table->string('message_id');
            $table->string('tran_date')->nullable();
            $table->string('tran_time')->nullable();
            $table->string('receipt_id')->nullable();
            $table->string('tran_id')->nullable();
            $table->string('transfer_date')->nullable();
            $table->string('resrv_appt')->nullable();
            $table->string('c_bank_code')->nullable();
            $table->string('c_bank_name')->nullable();
            $table->string('c_branch_code')->nullable();
            $table->string('c_branch_name')->nullable();
            $table->string('c_account_type')->nullable();
            $table->string('c_account_number')->nullable();
            $table->string('c_account_name')->nullable();
            $table->string('transfer_name')->nullable();
            $table->string('r_bank_code')->nullable();
            $table->string('r_bank_name')->nullable();
            $table->string('r_branch_code')->nullable();
            $table->string('r_branch_name')->nullable();
            $table->string('r_account_type')->nullable();
            $table->string('r_account_number')->nullable();
            $table->string('r_account_name')->nullable();
            $table->decimal('amount')->nullable();
            $table->string('transfer_type')->nullable();
            $table->string('partner_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maebarai_transaction_responses');
    }
}

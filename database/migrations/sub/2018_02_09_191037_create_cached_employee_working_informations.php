<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCachedEmployeeWorkingInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cached_employee_working_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_working_information_id');
            $table->unique('employee_working_information_id', 'EWI_id_unique');
            $table->boolean('old')->default(true);
            $table->dateTime('schedule_start_work_time')->nullable();
            $table->dateTime('schedule_end_work_time')->nullable();
            $table->integer('schedule_break_time')->nullable();
            $table->integer('schedule_night_break_time')->nullable();
            $table->time('schedule_working_hour')->nullable();
            $table->integer('planned_work_status_id')->nullable();
            $table->integer('planned_rest_status_id')->nullable();
            $table->boolean('rest_status_unit_day')->nullable();
            $table->integer('planned_work_location_id')->nullable();
            $table->integer('real_work_location_id')->nullable();
            $table->dateTime('planned_start_work_time')->nullable();
            $table->dateTime('timestamped_start_work_time')->nullable();
            $table->dateTime('real_start_work_time')->nullable();
            $table->dateTime('planned_end_work_time')->nullable();
            $table->dateTime('timestamped_end_work_time')->nullable();
            $table->dateTime('real_end_work_time')->nullable();
            $table->time('planned_working_hour')->nullable();
            $table->time('real_working_hour')->nullable();
            $table->dateTime('planned_early_arrive_start')->nullable();
            $table->dateTime('real_early_arrive_start')->nullable();
            $table->dateTime('planned_early_arrive_end')->nullable();
            $table->dateTime('real_early_arrive_end')->nullable();
            $table->integer('real_late_time')->nullable();
            $table->dateTime('planned_work_span_start')->nullable();
            $table->dateTime('real_work_span_start')->nullable();
            $table->dateTime('planned_work_span_end')->nullable();
            $table->dateTime('real_work_span_end')->nullable();
            $table->time('planned_work_span')->nullable();
            $table->time('real_work_span')->nullable();
            $table->integer('planned_break_time')->nullable();
            $table->integer('real_break_time')->nullable();
            $table->integer('real_early_leave_time')->nullable();
            $table->dateTime('planned_overtime_start')->nullable();
            $table->dateTime('real_overtime_start')->nullable();
            $table->dateTime('planned_overtime_end')->nullable();
            $table->dateTime('real_overtime_end')->nullable();
            $table->integer('planned_overtime_due_to_reduce_break_time')->nullable();
            $table->integer('real_overtime_due_to_reduce_break_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cached_employee_working_informations');
    }
}

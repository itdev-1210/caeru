<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcludedEmployeeWorkingDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concluded_employee_working_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->date('date');
            $table->boolean('have_schedule');
            $table->boolean('planned_work_day');
            $table->boolean('real_work_day');
            $table->integer('planned_working_time');
            $table->integer('real_working_time');
            $table->integer('schedule_working_time');
            $table->integer('planned_work_span_time');
            $table->integer('real_work_span_time');
            $table->integer('planned_paid_rest_time');
            $table->integer('real_paid_rest_time');
            $table->integer('planned_unpaid_rest_time');
            $table->integer('real_unpaid_rest_time');
            $table->integer('planned_non_work_time');
            $table->integer('real_non_work_time');
            $table->integer('planned_overtime_work_time');
            $table->integer('real_overtime_work_time');
            $table->integer('planned_night_work_time');
            $table->integer('real_night_work_time');
            $table->boolean('planned_is_kekkin');
            $table->boolean('real_is_kekkin');
            $table->float('planned_taken_paid_rest_days');
            $table->float('real_taken_paid_rest_days');
            $table->integer('planned_taken_paid_rest_time');
            $table->integer('real_taken_paid_rest_time');
            $table->integer('employee_working_day_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concluded_employee_working_days');
    }
}

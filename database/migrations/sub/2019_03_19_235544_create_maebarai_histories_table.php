<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaebaraiHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maebarai_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('transaction_id');
            $table->string('receipt_id')->nullable();
            $table->integer('payment_format');
            $table->integer('maebarai_amount');
            $table->integer('transfer_amount');
            $table->integer('transfer_fee');
            $table->integer('api_usage_fee');
            $table->integer('transaction_status');
            $table->dateTime('tran_date');
            $table->dateTime('transfer_date')->nullable();
            $table->string('error_code')->nullable();
            $table->dateTime('error_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maebarai_histories');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Reusables\TodofukenTrait;
use App\Reusables\EnableTrait;
use App\Setting;
use App\Company;
use DB;
use Carbon\Carbon;
use App\Services\TimezonesService;
use App\Services\WorkLocationSettingService;

class WorkLocation extends Model
{
    use SoftDeletes, TodofukenTrait, EnableTrait;


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the company of this work location
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the employees of this work location
     */
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    /**
     * Get work times
     */
    public function workTimes()
    {
        return $this->hasMany(WorkTime::class);
    }

    /**
     * Get the checklists of this work location
     */
    public function checklistItems(){
        return $this->hasManyThrough(ChecklistItem::class, Employee::class);
    }
    /**
     * Get all the managers managing this work location
     */
    public function managers()
    {
        return $this->belongsToMany(Manager::class);
    }

    /**
     * Get the work addresses of this work location (if it has any)
     */
    public function workAddresses()
    {
        return $this->hasMany(WorkAddress::class);
    }

    /**
     * Get the schedules of this work location (if it has any)
     */
    public function schedules()
    {
        return $this->hasMany(PlannedSchedule::class);
    }

    /**
     * Get holidays
     */
    public function calendarRestDays()
    {
        return $this->hasMany(CalendarRestDay::class);
    }

    /**
     * Get default holidays
     */
    public function defaultCalendarRestDays()
    {
        return $this->company->calendarRestDays();
    }

    /**
     * Get total work time by months
     */
    public function calendarTotalWorkTimes()
    {
        return $this->hasMany(CalendarTotalWorkTime::class);
    }

    /**
     * Get default total work time by months
     */
    public function defaultCalendarTotalWorkTimes()
    {
        return $this->company->calendarTotalWorkTimes();
    }

    /**
     * Get setting
     */
    public function setting()
    {
        return $this->hasOne(Setting::class);
    }

    /**
     * Get default setting
     */
    public function defaultSetting()
    {
        return $this->company->setting;
    }

    /**
     * Get unused work statuses of  this work location
     */
    public function unusedWorkStatuses()
    {
        return $this->hasMany(UnusedWorkStatus::class);
    }

    /**
     * Get unused rest statuses of  this work location
     */
    public function unusedRestStatuses()
    {
        return $this->hasMany(UnusedRestStatus::class);
    }

    /**
     * Get the current setting of this work location. If this work location does not have any setting, that means it uses the default setting (company's).
     * If it has its own setting, that setting need to be merged with the default setting.
     *
     * @return \App\Setting
     */
    public function currentSetting()
    {
        if (!isset($this->setting)) {

            return $this->defaultSetting();

        } else {

            $work_location_only_setting = $this->setting;

            // Replicate from the company's setting
            $current_setting = $this->defaultSetting()->replicate();

            // Then merge anything of this work location's setting that is different from the company's.
            foreach ($work_location_only_setting->attributesToArray() as $key => $value) {

                if (isset($value)) {
                    $current_setting->setAttribute($key, ($value !== config('caeru.empty')) ? $value : null);
                }

                $current_setting->company_id = null;

            }

            return $current_setting;
        }
    }

    /**
     * Save the current setting of this work location. If the setting have any difference with company's, save that difference into this work location's setting instance.
     */
    public function saveCurrentSetting($data)
    {

        $default_setting = $this->defaultSetting();

        foreach ($data as $key => $value) {
            if ($data[$key] == $default_setting->$key  && !isset($this->setting->$key)) {
                unset($data[$key]);
            }else{
                if ($data[$key] == "") $data[$key]= config('caeru.empty');
            }
        }

        if (count($data) !== 0)
            Setting::updateOrCreate( ['work_location_id' => $this->id], $data);

    }


    /**
     * Return list of activating departments of this work location
     */
    public function activatingWorkStatuses()
    {
        // $unused_work_statuses = DB::table('work_statuses_unused')->where('work_location_id', $this->id)->pluck('work_status_id')->toArray();
        // return WorkStatus::where('company_id', $this->company->id)->whereNotIn('id', $unused_work_statuses)->orderBy('id')->get();
        $unused_work_statuses = $this->unusedWorkStatuses->pluck('work_status_id')->toArray();
        return $this->company->workStatuses->whereNotIn('id', $unused_work_statuses)->sortBy('id');
    }

    /**
     * Return list of activating work statuses of this work location
     */
    public function activatingRestStatuses()
    {
        // $unused_rest_statuses = DB::table('rest_statuses_unused')->where('work_location_id', $this->id)->pluck('rest_status_id')->toArray();
        // return RestStatus::where('company_id', $this->company->id)->whereNotIn('id', $unused_rest_statuses)->orderBy('id')->get();
        $unused_rest_statuses = $this->unusedRestStatuses->pluck('rest_status_id')->toArray();
        return $this->company->restStatuses->whereNotIn('id', $unused_rest_statuses)->sortBy('id');
    }

    /**
     * Return list of activating rest statuses of this work location
     */
    public function activatingDepartments()
    {
        $unused_departments = DB::table('departments_unused')->where('work_location_id', $this->id)->pluck('department_id')->toArray();
        return Department::where('company_id', $this->company->id)->whereNotIn('id', $unused_departments)->orderBy('id')->get();
    }

    /**
     * Get an collection of rest days of this work location (already merge with company's properly). This collection only contains the date string, and not contain type 0(not a rest day).
     * Can accept 2 parameters to limit the result by date.
     *
     * @param string    $start_date
     * @param string    $end_date
     * @return collection
     */
    public function getArrayRestDays($start_date = null, $end_date = null)
    {
        $rest_days = $this->getRestDays($start_date, $end_date);

        $rest_days = collect($rest_days)->filter(function($value) {
            return $value['type'] != CalendarRestDay::NOT_A_REST_DAY;
        })->pluck('assigned_date');

        return $rest_days;
    }

    /**
     * Get an array of rest days of this work location, including type 0 (Not a rest day).
     * Can receive two optional parameters to limit the results.
     *
     * @param string    $start_date
     * @param string    $end_date
     * @return array
     */
    public function getRestDays($start_date = null, $end_date = null)
    {
        $company_rest_days = $this->defaultCalendarRestDays();
        $work_location_rest_days = $this->calendarRestDays();

        if ($start_date) {
            $company_rest_days->where('assigned_date', '>=', $start_date);
            $work_location_rest_days->where('assigned_date', '>=', $start_date);
        }

        if ($end_date) {
             $company_rest_days->where('assigned_date', '<=', $end_date);
             $work_location_rest_days->where('assigned_date', '<=', $end_date);
        }

        $company_rest_days = $company_rest_days->orderBy('assigned_date')->get(['type', 'assigned_date'])->keyBy(function($item) {
            return date('Y-n-j', strtotime($item->assigned_date));
        })->toArray();
        $work_location_rest_days = $work_location_rest_days->orderBy('assigned_date')->get(['type', 'assigned_date'])->keyBy(function($item) {
            return date('Y-n-j', strtotime($item->assigned_date));
        })->toArray();
        $combine_rest_days = array_merge($company_rest_days, $work_location_rest_days);

        return $combine_rest_days;
    }

    /**
     * Get days that is specifically set as 'Not a rest day' of this work location (only type 0 - Not a rest day).
     * Can receive two optional parameters to limit the results.
     *
     * @param string    $start_date
     * @param string    $end_date
     * @return array
     */
    public function getSpecifiedNonRestDaysOfThisWorkLocation($start_date = null, $end_date = null)
    {
        $non_rest_days = $this->calendarRestDays;

        if ($start_date) {
            $non_rest_days = $non_rest_days->filter(function($day) use ($start_date) {
                $carbon_start_date = new Carbon($start_date);
                $carbon_day = new Carbon($day->assigned_date);

                return $carbon_day->gte($carbon_start_date);
            });
        }

        if ($end_date) {
            $non_rest_days = $non_rest_days->filter(function($day) use($end_date) {
                $carbon_end_date = new Carbon($end_date);
                $carbon_day = new Carbon($day->assigned_date);

                return $carbon_day->lte($carbon_end_date);
            });
        }

        $non_rest_days = $non_rest_days->filter(function($day) {
            return $day->type == CalendarRestDay::NOT_A_REST_DAY;
        });

        return $non_rest_days;
    }

    /**
     * Get days that is specifically set as 'a rest day' of this work location (not type 0).
     * Can receive two optional parameters to limit the results.
     *
     * @param string    $start_date
     * @param string    $end_date
     * @return array
     */
    public function getSpecifiedRestDaysOfThisWorkLocation($start_date = null, $end_date = null)
    {
        $rest_days = $this->calendarRestDays;

        if ($start_date) {
            $rest_days = $rest_days->filter(function($day) use($start_date) {
                $carbon_start_date = new Carbon($start_date);
                $carbon_day = new Carbon($day->assigned_date);

                return $carbon_day->gte($carbon_start_date);
            });
        }

        if ($end_date) {
            $rest_days = $rest_days->filter(function($day) use($end_date) {
                $carbon_end_date = new Carbon($end_date);
                $carbon_day = new Carbon($day->assigned_date);

                return $carbon_day->lte($carbon_end_date);
            });
        }

        $rest_days = $rest_days->filter(function($day) {
            return $day->type != CalendarRestDay::NOT_A_REST_DAY;
        });

        return $rest_days;
    }

    /**
     * Get the total work times by months of this WorkLocation. Already take into account of company's calendarTotalWorkTimes.
     *
     * @param integer       $year
     * @return collection
     */
    public function getTotalWorkTimes($year = null)
    {
        $default_total_work_times = $year ? $this->defaultCalendarTotalWorkTimes()->where('year', $year)->get() : $this->defaultCalendarTotalWorkTimes()->get();

        $default_total_work_times = $default_total_work_times->keyBy(function($work_time) {
            return $work_time->year . '-' . $work_time->month;
        })->map(function($row) {
            return $row->time;
        });

        $this_work_location_only_total_work_times = $year ? $this->calendarTotalWorkTimes->where('year', $year) : $this->calendarTotalWorkTimes;

        $this_work_location_only_total_work_times = $this_work_location_only_total_work_times->keyBy(function($work_time) {
            return $work_time->year . '-' . $work_time->month;
        })->map(function($row) {
            return $row->time;
        });

        return array_merge($default_total_work_times->toArray(), $this_work_location_only_total_work_times->toArray());
    }

    /**
     * Get timezone's worklocation
     *
     * @return null|Timezone     an instance of Timezone model or null
     */
    public function getTimezone()
    {
        $timezones_service = resolve(TimezonesService::class);
        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $current_setting = $work_location_setting_service->getCurrentSettingOfWorkLocationById($this->id);
        return $current_setting ? $timezones_service->getTimezoneById($current_setting->timezone) : null;
    }
}

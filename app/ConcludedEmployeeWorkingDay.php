<?php

namespace App;

class ConcludedEmployeeWorkingDay extends Model
{

    /**
     * Get the original version of this working day
     */
    public function employeeWorkingDay()
    {
        return $this->belongsTo(EmployeeWorkingDay::class);
    }

}

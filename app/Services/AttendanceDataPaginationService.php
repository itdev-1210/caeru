<?php

namespace App\Services;

use Illuminate\Support\Collection;

class AttendanceDataPaginationService
{
    /*
     * Define some suffixes
     */
    protected $data_suffix = null;
    protected $current_page_suffix = "_current_page";

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data_suffix = "_data";
        $this->current_page_suffix = "_current_page";
    }

    /**
     * Paginate the given data_set and save that paginated data to the session using the given key_to_save,
     * then return the first page.
     * When use this function, the previous paginated data (with the same key) will be overwriten.
     *
     * @param  string               $key_to_save   key to save this paginated data in session
     * @param  array|Collection     $data_set      the data set to be paginated
     * @param  integer              $rows_per_page number of rows per page
     * @param  boolean              $disable       to disable the pagination and instead, give all the data
     * @return array                first page of the paginated data
     */
    public function paginateDataSetAndSaveToSession($key_to_save, $data_set, $rows_per_page, $disable = false)
    {
        if (!($data_set instanceof Collection)) {
            $data_set = collect($data_set);
        }

        if ($disable == false) {
            $paginated_data = $data_set->chunk($rows_per_page);

        // If disable then return data just like normal
        } else {
            $paginated_data = collect([$data_set]);
        }

        // Save the paginated data_set and the current page(which is 1) to session with the given key
        session([
            $key_to_save . $this->data_suffix => $paginated_data,
            $key_to_save . $this->current_page_suffix => 1,
        ]);

        // Get the first page and return
        $data_for_first_page = $paginated_data->first();


        return [
            'data' => $data_for_first_page,
            'paginate_information' => [
                'total_page' => $paginated_data->count(),
                'current_page' => 1,
                'can_go_previous' => false,
                'can_go_next' => $paginated_data->count() > 1,
            ],
        ];
    }

    /**
     * Get specific page of an already saved paginated data.
     *
     * @param  string       $key_to_get         the key of the saved paginated data set
     * @param  integer      $page               the page number
     * @return array        the page
     */
    public function getDataByPage($key_to_get, $page)
    {
        $paginated_data = session($key_to_get . $this->data_suffix);

        $page = intval($page);
        $max_page = $paginated_data->count();

        // Handle the edge cages
        $page = ($page <= 1) ? 1 : $page;
        $page = ($page >= $max_page) ? $max_page : $page;

        // Turn the page number into paginated_data index
        $index = $page - 1;

        $data = $paginated_data[$index];

        // Save the new current_page
        session([ $key_to_get . $this->current_page_suffix => $page ]);

        return [
            'data' => $data,
            'paginate_information' => [
                'total_page' => $max_page,
                'current_page' => $page,
                'can_go_previous' => $page > 1,
                'can_go_next' => $page < $max_page,
            ],
        ];
    }

}
<?php

namespace App\Services;

use App\Timezone;

class TimezonesService
{
    /**
     * List of all timezones and some utilize arrays.
     */
    private $timezones_list = [];
    private $lookup_by_id_array = [];
    private $lookup_by_name_id_array = [];

    /**
     * Initiate the timezones lists
     */
    public function __construct()
    {
        $this->timezones_list = Timezone::all();
        $this->lookup_by_id_array = $this->timezones_list->keyBy('id');
        $this->lookup_by_name_id_array = $this->timezones_list->keyBy('name_id');
    }

    /**
     * Get all timezones.
     * @return Collection
     */
    public function getAllTimezones()
    {
        return $this->timezones_list;
    }

    /**
     * Get timezone by the given id.
     *
     * @param  integer  $id
     * @return Timezone|null
     */
    public function getTimezoneById($id)
    {
        return $this->lookup_by_id_array[$id];
    }

    /**
     * Get timezone by the given name_id.
     *
     * @param  string  $name_id
     * @return Timezone|null
     */
    public function getTimezoneByNameId($name_id)
    {
        return $this->lookup_by_id_array[$name_id];
    }
}
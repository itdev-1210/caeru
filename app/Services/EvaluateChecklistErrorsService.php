<?php

namespace App\Services;

use Auth;
use App\WorkingTimestamp;
use App\WorkLocation;
use App\ChecklistItem;
use App\ChecklistErrorTimer;
use App\WorkStatus;
use App\Setting;
use App\EmployeeWorkingInformation;
use App\CachedEmployeeWorkingInformation;
use App\EmployeeWorkingDay;
use App\WorkAddressWorkingEmployee;
use App\WorkAddressWorkingInformation;
use App\ColorStatus;
use App\Scopes\NotShowWhenTemporaryScope;
use App\Services\WorkAddressWorkingDayLoadingService;
use App\Events\TimestampStartWorkError;
use App\Events\TimestampEndWorkError;
use App\Events\TimestampGoOutReturnError;
use App\Events\TimestampForgotEndWorkError;
use App\Events\TimestampForgotReturnError;
use App\Events\ConfimNeededWorkWithoutScheduleError;
use App\Events\ConfimNeededDifferenceFromScheduleError;
use App\Events\TimeStampedStartWorkTimeAttributeManuallyChanged;
use App\Events\TimeStampedEndWorkTimeAttributeManuallyChanged;
use App\Events\RealGoOutTimeAttributeManuallyChanged;
use App\Events\ConfimNeededLateOrLeaveEarlyError;
use App\Events\ConfimNeededOffScheduleError;
use App\Events\ConfimNeededStatusMistakenError;
use App\Events\ConfimNeededOverlimitBreakError;
use App\Events\ConfimNeededHaveScheduleButOfflineError;
use App\Events\ConfimNeededEmptyEWIError;
use App\Listeners\Reusables\RelateToChecklistErrorTrait;
use Carbon\Carbon;

class EvaluateChecklistErrorsService
{
    use RelateToChecklistErrorTrait;

    /**
     * Initiate the Carbon instance. Just another shortcut
     *
     * @param string    $time_string
     * @return Carbon
     */
    protected function getCarbonInstance($time_string)
    {
        return ($time_string !== null) ? Carbon::createFromFormat('Y-m-d H:i:s', $time_string) : null;
    }

    //////////////////////////////////////////// Evaluate Checklist Error of an EmployeeWorkingInformation instance /////////////////////////////
    /////////////////////////////////////////// This part evaluate most of ConfirmNeeded Errors and some Timestamp Errors ///////////////////////

    /**
     * Evaluate checklist errors of an EmployeeWorkingInformation instance. Mainly, ConfirmNeeded errors are evaluated in this function.
     *
     * @param EmployeeWorkingInformation    $working_info
     * @return void
     */
    public function evaluateChecklistErrorsFromAnInstanceOfEmployeeWorkingInformation(EmployeeWorkingInformation $working_info)
    {
        if (!$working_info->employeeWorkingDay->isConcluded()) {

            // Reset all confirm_needed errors except for some.
            $this->resetConfirmNeededErrorRecords($working_info, $working_info->employeeWorkingDay, [ChecklistItem::WORK_WITHOUT_SCHEDULE, ChecklistItem::HAVE_SCHEDULE_BUT_OFFLINE, ChecklistItem::DIFFERENCE_FROM_SCHEDULE]);

            // The reset of this type of error is different from the others. So it need its own reset function
            $this->resetTimestampErrorRecordsOfASpecificType($working_info->employeeWorkingDay, ChecklistItem::FORGOT_END_WORK_ERROR, $working_info->id);
            // And also reset the ColorStatus of that error
            // $this->resetColorStatusOfASpecificField($working_info, 'timestamped_end_work_time');

            // If this EmployeeWorkingInformation was a temporary one, and this is its first time updated, reset the WORK_WITHHOUT_SCHEDULE error of this day for this employee
            if ($working_info->getOriginal('temporary') == true && $working_info->temporary == false) {
                $this->resetConfirmNeededErrorRecordsOfASpecificType($working_info, $working_info->employeeWorkingDay, ChecklistItem::WORK_WITHOUT_SCHEDULE);
                $this->resetConfirmNeededErrorRecordsOfASpecificType($working_info, $working_info->employeeWorkingDay, ChecklistItem::DIFFERENCE_FROM_SCHEDULE);
            }

            // If this instance was having a timestamped_end_work_time without timestamped_start_work_time(START_WORK_ERROR), and here it was changed to have timestamped_start_work_time
            // then we need to remove the START_WORK_ERROR. (Because, the WorkingTimestamps list's changed event was not triggered)
            // if ($working_info->getOriginal('timestamped_start_work_time') === null && $working_info->getOriginal('timestamped_end_work_time') !== null &&
            //     $working_info->timestamped_start_work_time !== null && $working_info->timestamped_end_work_time !== null) {
            //     $this->resetTimestampErrorRecordsOfASpecificType($working_info->employeeWorkingDay, ChecklistItem::START_WORK_ERROR, $working_info->id);
            // }

            // If this EmployeeWorkingInformation have any timestamp, then the HAVE_SCHEDULE_BUT_OFFLINE error will be void
            if ($working_info->timestamped_start_work_time !== null || $working_info->timestamped_end_work_time !== null || $working_info->takeAWholeDayOff() == true || $working_info->planned_work_status_id == WorkStatus::KEKKIN || $working_info->planned_work_status_id == WorkStatus::FURIKYUU || $working_info->planned_working_hour == null) {
                $this->resetConfirmNeededErrorRecordsOfASpecificType($working_info, $working_info->employeeWorkingDay, ChecklistItem::HAVE_SCHEDULE_BUT_OFFLINE);
            }

            // But if this EmployeeWorkingInformation doesnt have any timestamps, then it will be checked for HAVE_SCHEDULE_BUT_OFFLINE error
            if ($working_info->timestamped_start_work_time === null && $working_info->timestamped_end_work_time === null && $working_info->takeAWholeDayOff() != true && $working_info->planned_work_status_id !== WorkStatus::KEKKIN && $working_info->planned_work_status_id !== WorkStatus::FURIKYUU && $working_info->planned_working_hour !== null) {
                event(new ConfimNeededHaveScheduleButOfflineError($working_info->employeeWorkingDay, $working_info->id, $working_info->employeeWorkingDay->employee->workLocation->company));
            }

            // ATTENTION: unlike the other blocks checking for ConfirmedNeeded errors, this block checks the TimestampError FORGOT_END_WORK_TIME
            if ($working_info->timestamped_start_work_time !== null && $working_info->timestamped_end_work_time === null && $working_info->takeAWholeDayOff() != true && $working_info->planned_work_status_id !== WorkStatus::KEKKIN && $working_info->planned_work_status_id != WorkStatus::FURIKYUU) {
                event(new TimestampForgotEndWorkError($working_info->employeeWorkingDay, $working_info->id, $working_info->timestamped_start_work_time));
            }


            // 遅刻・早退 when the real_late_time is different from planned_late_time
            if ($working_info->real_start_work_time !== null) {
                $diff_limit = $working_info->affectedByRealWorkLocation()->currentSetting()->start_time_diff_limit;
                $diff = $working_info->real_late_time - $working_info->planned_late_time;

                if ($diff >= $diff_limit) {
                    event(new ConfimNeededLateOrLeaveEarlyError($working_info->employeeWorkingDay, $working_info->id));
                }
            }

            if ($working_info->real_end_work_time !== null) {
                $diff_limit = $working_info->affectedByRealWorkLocation()->currentSetting()->end_time_diff_limit;
                $diff = $working_info->real_early_leave_time - $working_info->planned_early_leave_time;

                if ($diff >= $diff_limit) {
                    event(new ConfimNeededLateOrLeaveEarlyError($working_info->employeeWorkingDay, $working_info->id));
                }
            }


            // 時間外 when the real_early_arrive_start is different from planned_early_arrive_start of when the real_overtime_end is different from planned_overtime_end
            if (($working_info->timestamped_start_work_time !== null && $working_info->real_early_arrive_start !== $working_info->planned_early_arrive_start) ||
                ($working_info->timestamped_end_work_time !== null && $working_info->real_overtime_end !== $working_info->planned_overtime_end)) {
                event(new ConfimNeededOffScheduleError($working_info->employeeWorkingDay, $working_info->id));
            }


            // Also 時間外, when the timestamped_start_work_time is smaller than the planned_start_work_time, or when the timestamped_start_work_time is smaller than the estimatedLatestStartWorkTime.
            // The differences have to be bigger than the start_time_diff_limit of the work_location's setting.
            if ($working_info->timestamped_start_work_time !== null) {
                $start_time_diff_limit = $working_info->affectedByRealWorkLocation()->currentSetting()->start_time_diff_limit;

                $carbon_timestamped = $this->getCarbonInstance($working_info->timestamped_start_work_time);
                $carbon_planned_or_estimated = ($working_info->planned_start_work_time !== null) ? $this->getCarbonInstance($working_info->planned_start_work_time) : $this->getCarbonInstance($working_info->estimatedLatestStartWorkTime());

                if ($carbon_timestamped !== null && $carbon_planned_or_estimated !== null) {
                    $diff = $carbon_timestamped->diffInMinutes($carbon_planned_or_estimated, false);

                    if ($diff >= $start_time_diff_limit) {
                        event(new ConfimNeededOffScheduleError($working_info->employeeWorkingDay, $working_info->id));
                    }
                }
            }


            // And also 時間外, when the timestamped_end_work_time is bigger than the planned_end_work_time, or when timestamped_end_work_time is bigger than the real_end_work_time.
            // The differences have to be bigger than the end_time_diff_limit of the work_location's setting.
            if ($working_info->timestamped_end_work_time !== null) {
                $end_time_diff_limit = $working_info->affectedByRealWorkLocation()->currentSetting()->end_time_diff_limit;

                $carbon_timestamped = $this->getCarbonInstance($working_info->timestamped_end_work_time);
                $carbon_planned_or_real = ($working_info->planned_end_work_time !== null) ? $this->getCarbonInstance($working_info->planned_end_work_time) : $this->getCarbonInstance($working_info->real_end_work_time);

                if ($carbon_timestamped !== null && $carbon_planned_or_real !== null) {
                    $diff = $carbon_planned_or_real->diffInMinutes($carbon_timestamped, false);

                    if ($diff >= $end_time_diff_limit) {
                        event(new ConfimNeededOffScheduleError($working_info->employeeWorkingDay, $working_info->id));
                    }
                }
            }


            // 形態 when work_status is  furikyuu or kekkin or when the rest_status is yuukyuu or any of the whole-day rest day type or when the
            // rest_status is jiyuu and the paid_rest_time is all of the work time AND there is real_date on that instance.
            if (($working_info->isPlannedWorkStatus(WorkStatus::KEKKIN, WorkStatus::FURIKYUU) || $working_info->takeAWholeDayOff()) &&
                ($working_info->timestamped_start_work_time !== null || $working_info->timestamped_end_work_time !== null || $working_info->real_go_out_time !== null)) {
                event(new ConfimNeededStatusMistakenError($working_info->employeeWorkingDay, $working_info->id));
            }


            // 休憩・外出 if the real_work_location have setting: 'use the go_out button to calculate break time' and the real_go_out_time  bigger then the planned_break_time
            if ($work_location = $working_info->affectedByRealWorkLocation()) {
                if ($work_location->currentSetting()->go_out_button_usage === Setting::USE_AS_BREAK_TIME_BUTTON) {
                    if ($working_info->real_break_time !== null) {
                        if ($working_info->real_go_out_time > $working_info->real_break_time) {
                            event(new ConfimNeededOverlimitBreakError($working_info->employeeWorkingDay, $working_info->id));
                        }

                    } else {
                        if ($working_info->real_go_out_time > $working_info->planned_break_time) {
                            event(new ConfimNeededOverlimitBreakError($working_info->employeeWorkingDay, $working_info->id));
                        }
                    }
                }
            }

            // 休出: This error's text is the same with WORK_WITHOUT_SCHEDULE, but it's different. If an EWI doesnt have
            // planned_working_hour and doesn't have schedule_working_hour and isn't a temporary working info, throw this error
            if ($working_info->schedule_working_hour === null &&
                $working_info->planned_working_hour === null &&
                $working_info->temporary == false &&
                ($working_info->timestamped_start_work_time !== null || $working_info->timestamped_end_work_time !== null)) {
                event(new ConfimNeededEmptyEWIError($working_info->employeeWorkingDay, $working_info->id));
            }

        }
    }


    /**
     * Delete the ColorStatus of a specific field.
     *
     * @param EmployeeWorkingInformation        $working_info
     * @param string                            $field_name
     * @return void
     */
    protected function resetColorStatusOfASpecificField($working_info, $field_name)
    {
        ColorStatus::where('colorable_type', EmployeeWorkingInformation::class)
                    ->where('field_css_class', ColorStatus::ERROR_COLOR)
                    ->where('colorable_id', $working_info->id)
                    ->where('field_name', $field_name)->delete();
    }

    ////////////////////////////////////// End of Checklist Errors Evaluation of an EmployeeWorkingInformation Instance //////////////////////////


    /////////////////////////////////////////////// Evaluate Checklist Error of an EmployeeWorkingDay instance ///////////////////////////////////
    /////////////////////////////////////////// This part evaluate most of Timestamp Errors and some ConfirmNeeded Errors ////////////////////////

    /**
     * Delete all the temporary EmployeeWorkingInformation belong to all of the given EmployeeWorkingDays.
     * NOTE: This function must be use before the function distributeWorkingTimestampWithoutEvaluatingChecklistErrors() or
     * evaluateChecklistErrorFromWorkingTimestampListOfManyWorkingDay()
     * ATTENTION: the above is the old description. It's not entirely correct anymore, but is still kept for reference's sake.
     * New description: This function will hide all the current 'temporary' EmployeeWorkingInformations. And also reset the value on two 'timestamped_' attributes.
     * These 'hiding temporary' EmployeeWorkingInformation will be pull out of hiding later, if there is a suitable WorkingTimestamp for them.
     *
     * @param array             $working_day_ids
     * @return Collection       the collection of all hiding temporary EmployeeWorkingInformation of the given EmployeeWorkingDays
     */
    public function resetAllTemporaryEmployeeWorkingInformationsOfTheseEmployeeWorkingDays($working_day_ids)
    {
        ////////////// TRY OUT SOMETHING TEMPORARY COMMENT THESE OUT //////////////////
        // $to_be_deleted_working_info_ids = EmployeeWorkingInformation::whereIn('employee_working_day_id', $working_day_ids)->where('temporary', true)->pluck('id')->toArray();

        // This is where you need to include all operations in the event deleting of EmployeeWorkingInformation model, make sure to cascade delete all the relating models
        // Clean up checklist errors of these temporary working infos
        // ChecklistItem::whereIn('employee_working_information_id', $to_be_deleted_working_info_ids)->delete();

        // Also, clean up checklist error timers of these temporary working infos
        // ChecklistErrorTimer::whereIn('employee_working_information_id', $to_be_deleted_working_info_ids)->delete();

        // Also also, we have to delete the color statuses too
        // ColorStatus::where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION)->whereIn('colorable_id', $to_be_deleted_working_info_ids)->delete();

        // Also also also, we have to delete the cached models
        // CachedEmployeeWorkingInformation::whereIn('employee_working_information_id', $to_be_deleted_working_info_ids)->delete();

        // Then, delete them
        // EmployeeWorkingInformation::whereIn('id', $to_be_deleted_working_info_ids)->delete();
        ////////////////// END OF TRY OUT ////////////////

        // First, reset their checklist errors
        $temp_working_infos = EmployeeWorkingInformation::whereIn('employee_working_day_id', $working_day_ids)->where('temporary', true)->get();
        $temp_working_info_ids = $temp_working_infos->pluck('id')->toArray();
        ChecklistItem::whereIn('employee_working_information_id', $temp_working_info_ids)->delete();
        ChecklistErrorTimer::whereIn('employee_working_information_id', $temp_working_info_ids)->delete();

        // Update 2019-05-18: new specification for temporary EWI with work address flow, now we just make a temporary EWI without making WAWI nor WAWE
        // $work_address_working_employee_ids = $temp_working_infos->pluck('work_address_working_employee_id')->toArray();
        // WorkAddressWorkingEmployee::whereIn('id', $work_address_working_employee_ids)->update([
        //     'working_confirm' => false,
        // ]);

        // And delete the work address related temporary employee working info
        EmployeeWorkingInformation::whereNotNull('work_address_working_employee_id')->where('temporary', true)->delete();

        // Then hide the temporary working infos and reset their timestamped_ attributes
        EmployeeWorkingInformation::whereIn('employee_working_day_id', $working_day_ids)->where('temporary', true)->update([
            'not_show_when_temporary' => true,
            'timestamped_start_work_time' => null,
            'timestamped_end_work_time' => null,
            'real_go_out_time' => null,
        ]);

        $all_the_hiding_temporary_ewis = EmployeeWorkingInformation::withoutGlobalScope(NotShowWhenTemporaryScope::class)->whereIn('employee_working_day_id', $working_day_ids)->where('temporary', true)->where('not_show_when_temporary', true)->get()->groupBy('employee_working_day_id');
        return $all_the_hiding_temporary_ewis;
    }

    /**
     * This function is basically just a wrapper of evaluateChecklistErrorFromWorkingTimestampList(). Its purpose is: to call a mass query to delete all
     * the temporary EmployeeWorkingInformation, instead of just delete it one by one.
     *
     * Now that we change it to this way, we have to make all the function calls to evaluateChecklistErrorFromWorkingTimestampList() redirect to
     * evaluateChecklistErrorFromWorkingTimestampListOfManyWorkingDay()
     *
     * @param array     $working_day_ids
     * @return void
     */
    public function evaluateChecklistErrorFromWorkingTimestampListOfManyWorkingDay($working_day_ids)
    {
        $temporary_ewis = $this->resetAllTemporaryEmployeeWorkingInformationsOfTheseEmployeeWorkingDays($working_day_ids);

        $working_days = EmployeeWorkingDay::with([
            'workingTimestamps',
            'employee.workLocation.company',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            'employeeWorkingInformations.colorStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
            'employeeWorkingInformations.workAddressWorkingEmployee.plannedSchedule',
            'employeeWorkingInformations.workAddressWorkingEmployee.workAddressWorkingInformation.workAddressWorkingEmployees.plannedSchedule',
            'employeeWorkingInformations.workAddressWorkingEmployee.workAddressWorkingInformation.workAddressWorkingDay.workAddress.workLocation.company',
            // 'employeeWorkingInformations.currentPlannedWorkLocation',
            // 'employeeWorkingInformations.currentPlannedWorkLocation.setting',
            // 'employeeWorkingInformations.currentPlannedWorkLocation.company.setting',
            // 'employeeWorkingInformations.currentRealWorkLocation.setting',
            // 'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.setting',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
        ])->whereIn('id', $working_day_ids)->notConcluded()->get();

        foreach($working_days as $working_day) {
            $this->evaluateChecklistErrorFromWorkingTimestampList($working_day, $temporary_ewis);
        }
    }

    /**
     * Evaluate ChecklistError from the WorkingTimestapList of an EmployeeWorkingDay instance. In this function, mainly, TimestampErrors are evaluated.
     * Need to be provided a collection of hiding temporary EmployeeWorkingInformations of this EmployeeWorkingDay.
     *
     * @param EmployeeWorkingDay        $working_day
     * @param Collection                $temporary_ewis
     * @return void
     */
    public function evaluateChecklistErrorFromWorkingTimestampList(EmployeeWorkingDay $working_day, $temporary_ewis)
    {
        if (!$working_day->isConcluded()) {

            $temporary_ewis_of_this_working_day = $temporary_ewis->has($working_day->id) ? $temporary_ewis[$working_day->id] : collect([]);

            // In this event, first, we reset all the temporary EmployeeWorkingInformation
            // $this->resetTemporaryEmployeeWorkingInformation($working_day);

            // IMPORTANT: we always need to reset all the temporary EmployeeWorkingInformation. But now that we commented out the above line,
            // we have to call the wrapper evaluateChecklistErrorFromWorkingTimestampListOfManyWorkingDay(). This function will reset the temporary
            // EmployeeWorkingInformations of multiple days.
            // Why do we need to do that? Because EmployeeWorkingInformations table gets big very fast, so that query is actually make a lot of things
            // slower. So we need to do that in a more efficient way: mass query.

            $working_infos = $working_day->employeeWorkingInformations->sortBy('planned_start_work_time');
            $timestamps = $working_day->workingTimestamps;

            $timestamps = $timestamps->filter(function($timestamp) {
                return $timestamp->enable == true;
            })->sortBy('raw_date_time_value');

            // Reset all the TimestampError records except for FORGOT_END_WORK_ERROR and FORGOT_RETURN_ERROR
            $this->resetTimestampErrorRecords($working_day, [ChecklistItem::FORGOT_END_WORK_ERROR, ChecklistItem::FORGOT_RETURN_ERROR]);

            // The reset of this type of error is different from the others. So it need its own reset function
            $this->resetTimestampErrorRecordsOfASpecificType($working_day, ChecklistItem::FORGOT_RETURN_ERROR);

            // Also, reset all the 'error' type ColorStatuses of this day
            $this->resetAllErrorColorStatusOfThisDay($working_day);

            // Updated 2019-01-30: to accommodate the WorkAddress's logic, resolve the service and try to preload WorkAddressWorkingDay as much as we can
            $this->initializeWorkAddressWorkingDayLoadingServiceIfNotYetAndPreloadSomeInstances($working_day, $timestamps);


            // First we reset all the timestamped_start/end_work_time, real_work_location/address_id and real_go_out_time of all the working_infos
            foreach ($working_infos as $working_info) {
                $working_info->timestamped_start_work_time = null;
                $working_info->timestamped_start_work_time_work_location_id = null;
                $working_info->timestamped_start_work_time_work_address_id = null;
                $working_info->timestamped_start_work_time_working_timestamp_id = null;
                $working_info->timestamped_end_work_time = null;
                $working_info->timestamped_end_work_time_work_location_id = null;
                $working_info->timestamped_end_work_time_work_address_id = null;
                $working_info->timestamped_end_work_time_working_timestamp_id = null;
                $working_info->real_work_location_id = null;
                $working_info->real_work_address_id = null;
                $working_info->real_go_out_time = null;
            }



            // Then calculate
            // $enter_work_flag = false;
            $go_out_flag = false;
            $go_out_start = null;
            // $go_out_end = null;
            $sum_go_out_time = 0;
            $current_working_info_to_which_current_go_out_time_belong = null;
            $current_working_info_that_have_go_out_error = null;
            $current_working_infor_that_have_start_work_error = null;
            $the_odd_go_out = null;
            // $current_working_info = null;

            $last_timestamp = null;
            $last_work_address_timestamp = null;

            $full_working_info_list = $working_infos;
            $only_work_location_working_info_list = null;
            $only_work_address_working_info_list = null;
            /**
             * Update 2019-05-06: Base on if timestamp has work address or not, we have to separate work location's working infos and work address's working infos
             * here.
             */
            $only_work_location_working_info_list = $full_working_info_list->filter(function($working_info) {
                return $working_info->planned_work_address_id == null;
            });
            $only_work_address_working_info_list = $full_working_info_list->filter(function($working_info) {
                return $working_info->planned_work_address_id != null;
            });

            // Check if there exist a working info without planned_start/end_work_time, if so turn on pair_mode.
            $exist_ewi_without_planned_start_end_work_time = $only_work_location_working_info_list->first(function($working_info) {
                return $working_info->planned_start_work_time == null || $working_info->planned_end_work_time == null;
            });
            $pair_rule_mode = $exist_ewi_without_planned_start_end_work_time !== null;
            $pair_rule_mode_current_working_info = null;


            foreach ($timestamps as $timestamp) {

                if ($timestamp->work_address_id != null) {
                    $working_infos = &$only_work_address_working_info_list;
                } else {
                    $working_infos = &$only_work_location_working_info_list;
                }

                if ($working_infos->isEmpty()) {

                    $temporary_working_info = $this->searchHidingTemporaryEmployeeWorkingInformations($timestamp, $temporary_ewis_of_this_working_day);

                    if ($temporary_working_info == null) {
                        $temporary_working_info = new EmployeeWorkingInformation();
                        $temporary_working_info->employeeWorkingDay()->associate($working_day);
                        $temporary_working_info->temporary = true;


                        $this->assignPlaceInfo($temporary_working_info, $timestamp, true);
                        $temporary_working_info->save();

                        // Update 2019-05-18: With the new flow of temp EWI with work address, we don't establish connection between temp EWI with WAWI nor WAWE
                        // if ($temporary_working_info->planned_work_address_id != null) {
                        //     $this->establishConnectionWithWorkAddressWorkingInformationForTemporaryEmployeeWorkingInformation($temporary_working_info, $working_day);
                        // }

                        // Throw a confirm_needed checklist error for this
                        event(new ConfimNeededWorkWithoutScheduleError($working_day, $temporary_working_info->id));

                    } else {
                        $this->assignPlaceInfo($temporary_working_info, $timestamp, true);
                        $temporary_working_info->save();

                        // Throw a confirm_needed checklist error for this
                        event(new ConfimNeededWorkWithoutScheduleError($working_day, $temporary_working_info->id));
                    }

                    // Push that new temporary working_info into the working infos list
                    $full_working_info_list->push($temporary_working_info);
                    $working_infos->push($temporary_working_info);

                    if (!$timestamp->work_address_id) {
                        $pair_rule_mode = true;
                        $pair_rule_mode_current_working_info = null;
                    }
                }

                /**
                 * If it's a start_work timestamp, we find the suitable working info (based on the schedule of that working info), then assign value for
                 * timestamped_start_work_time, real_work_location_id, real_work_address_id attributes.
                 */
                if ($timestamp->timestamped_type === WorkingTimestamp::START_WORK) {

                    // If a timestamp of anytype other than GO_OUT is at the last of the list, the forgot_return_error will be void
                    $the_odd_go_out = null;

                    // 外出後に戻り以外がある場合 - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                    if (($timestamp->work_address_id == null && ($last_timestamp !== null && $last_timestamp->timestamped_type === WorkingTimestamp::GO_OUT)) ||
                        ($timestamp->work_address_id != null && ($last_work_address_timestamp !== null && $last_work_address_timestamp->timestamped_type === WorkingTimestamp::GO_OUT))) {
                        // 2018-04-13, when there's no current working info to which the current go out time belong, we connect that error to the last working info
                        event(new TimestampGoOutReturnError($working_day, ($current_working_info_to_which_current_go_out_time_belong !== null) ? $current_working_info_to_which_current_go_out_time_belong->id : $working_infos->last()->id));
                        $current_working_info_that_have_go_out_error = ($current_working_info_to_which_current_go_out_time_belong !== null) ? $current_working_info_to_which_current_go_out_time_belong : $working_infos->last();
                    }

                    $suitable_working_info = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisStartWorkTimestamp($working_infos, $timestamp);

                    if ($suitable_working_info !== null && $suitable_working_info !== -1 && $suitable_working_info !== -2 && $suitable_working_info !== -3) {

                        // 出勤・出勤→出勤エラー
                        if ($suitable_working_info->timestamped_start_work_time !== null) {
                            if (!$suitable_working_info->haveConsideringTimestampedStartWorkTime()) {
                                event(new TimestampStartWorkError($working_day, $suitable_working_info->id));
                            }

                        } else {
                            $suitable_working_info->timestamped_start_work_time = $timestamp->raw_date_time_value;
                            $suitable_working_info->timestamped_start_work_time_work_location_id = $timestamp->work_location_id;
                            $suitable_working_info->timestamped_start_work_time_work_address_id = $timestamp->work_address_id;
                            $suitable_working_info->timestamped_start_work_time_working_timestamp_id = $timestamp->id;
                            $this->assignPlaceInfo($suitable_working_info, $timestamp);

                            // Reset go out time
                            $current_working_info_to_which_current_go_out_time_belong = $suitable_working_info;
                            $go_out_flag = false;
                            $sum_go_out_time = 0;

                            // Set the pivot for pair_rule_mode - Update 2019-05-13 Work address flow will not use the pair rule
                            if ($timestamp->work_address_id == null && $pair_rule_mode) {
                                $pair_rule_mode_current_working_info = $suitable_working_info;
                            }

                            if (!$suitable_working_info->haveConsideringTimestampedStartWorkTime()) {
                                if ($current_working_infor_that_have_start_work_error == null || $suitable_working_info->id !== $current_working_infor_that_have_start_work_error->id) {
                                    // Change color status base on this timestamp is a manually added timestamp or not.
                                    event(new TimeStampedStartWorkTimeAttributeManuallyChanged($suitable_working_info, $timestamp->registerer_type == WorkingTimestamp::TABLET || $timestamp->registerer_type == WorkingTimestamp::MOBILE_APP, $timestamp->approved));
                                }
                            }
                        }

                    // If the location of this timestamp is not match,
                    // or if all the working infos with the same location have already had both 'timestamped_start/end_work_time',
                    // create a temporary one, throw an check-list error, reload the working_infos list.
                    } else if ($suitable_working_info == -1 || $suitable_working_info == -2) {
                        $temporary_working_info = $this->createATemporaryWorkingInformationWithAStartWorkTimestamp($working_day, $timestamp, $temporary_ewis_of_this_working_day);
                        event(new ConfimNeededDifferenceFromScheduleError($working_day, $temporary_working_info->id));

                        // Change color status base on this timestamp is a manually added timestamp or not.
                        event(new TimeStampedStartWorkTimeAttributeManuallyChanged($temporary_working_info, $timestamp->registerer_type == WorkingTimestamp::TABLET || $timestamp->registerer_type == WorkingTimestamp::MOBILE_APP, $timestamp->approved));

                        $full_working_info_list->push($temporary_working_info);
                        $working_infos->push($temporary_working_info);

                        // Toggle pair mode ON and set the pivot for pair_rule_mode if the temporary working info is not a work address's working info
                        if ($temporary_working_info->planned_work_address_id == null) {
                            $pair_rule_mode = true;
                            $pair_rule_mode_current_working_info = $temporary_working_info;
                        }

                    // Even after searching among the not-have-planned_start/end_work_time EWIs, and we still can't find a suitable working info, create a start work error and connect it to the last working info
                    } else if ($suitable_working_info == -3) {
                        if (!$working_infos->last()->haveConsideringTimestampedStartWorkTime()) {
                            event(new TimestampStartWorkError($working_day, $working_infos->last()->id));
                        }
                    }


                /**
                 * If it's a go_out_start timestamp, we find the suitable working info, if there is a current working information to which this go_out_time belong, check weather or not
                 * the suitable working info is different from the current working info. If they are the same just proceed normally. If not, reset the go_out_time ($go_out_start and $sum_go_out_time)
                 * and update the current working information, then proceed.
                 */
                } else if ($timestamp->timestamped_type === WorkingTimestamp::GO_OUT) {

                    // If a timestamp of anytype other than GO_OUT is at the last of the list, the forgot_return_error will be void
                    $the_odd_go_out = null;

                    // Update 2019-05-13 Work address flow will not use the pair rule
                    if ($timestamp->work_address_id == null && $pair_rule_mode) {

                        if ($pair_rule_mode_current_working_info) {
                            $suitable_working_info = $pair_rule_mode_current_working_info;
                        } else {
                            $suitable_working_info = null;
                        }
                    } else {
                        $suitable_working_info = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisGoOutOrReturnTimestamp($working_infos, $timestamp);
                    }


                    // 外出の前が出勤または戻りでない場合 - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                    if (($timestamp->work_address_id == null && (($last_timestamp === null) || ($last_timestamp !== null && $last_timestamp->timestamped_type === WorkingTimestamp::GO_OUT) || ($last_timestamp !== null && $last_timestamp->timestamped_type === WorkingTimestamp::END_WORK))) ||
                        ($timestamp->work_address_id != null && (($last_work_address_timestamp === null) || ($last_work_address_timestamp !== null && $last_work_address_timestamp->timestamped_type === WorkingTimestamp::GO_OUT) || ($last_work_address_timestamp !== null && $last_work_address_timestamp->timestamped_type === WorkingTimestamp::END_WORK)))) {

                            // 2018-04-13, when we cant find the suitable working info, but there's error, then we connect that error to the last working info
                        event(new TimestampGoOutReturnError($working_day, (isset($suitable_working_info)) ? $suitable_working_info->id : $working_infos->last()->id));
                        $current_working_info_that_have_go_out_error = (isset($suitable_working_info)) ? $suitable_working_info : $working_infos->last();
                    }

                    if (isset($suitable_working_info)) {

                        if ($current_working_info_to_which_current_go_out_time_belong !== null) {

                            if ($current_working_info_to_which_current_go_out_time_belong->id !== $suitable_working_info->id) {
                                $current_working_info_to_which_current_go_out_time_belong = $suitable_working_info;
                                $go_out_start = $timestamp->raw_date_time_value;
                                $sum_go_out_time = 0;

                            }
                        } else {
                            $current_working_info_to_which_current_go_out_time_belong = $suitable_working_info;
                        }

                        if ($go_out_flag === false) {
                            $go_out_start = $timestamp->raw_date_time_value;
                            $go_out_flag = true;
                        }
                    }

                    // $this->assignPlaceInfo($suitable_working_info, $timestamp);

                    // If this go_out timestamp is the last timestamp of this day, then the will be an odd go_out timestamp, which
                    // can be used to fire a forgot_return_error event later.
                    $the_odd_go_out = $timestamp->raw_date_time_value;

                /**
                 * If it's a return timestamp, confirm if this timestamp is within the current working information to proceed with the calculation. If proceed, calculate the go_out_time
                 * then update the real_go_out_time of that working info, then reset the $go_out_flag.
                 */
                } else if ($timestamp->timestamped_type === WorkingTimestamp::RETURN) {

                    // If a timestamp of anytype other than GO_OUT is at the last of the list, the forgot_return_error will be void
                    $the_odd_go_out = null;

                    // 戻りの前が外出以外の場合 - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                    if (($timestamp->work_address_id == null && ($last_timestamp === null || $last_timestamp->timestamped_type !== WorkingTimestamp::GO_OUT)) ||
                        ($timestamp->work_address_id != null && ($last_work_address_timestamp === null || $last_work_address_timestamp->timestamped_type !== WorkingTimestamp::GO_OUT))) {

                        $the_working_info_to_attach_error_to = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisGoOutOrReturnTimestamp($working_infos, $timestamp);
                        // 2018-04-13, when we cant find the suitable working info, but there's error, then we connect that error to the last working info
                        event(new TimestampGoOutReturnError($working_day, ($the_working_info_to_attach_error_to !== null) ? $the_working_info_to_attach_error_to->id : $working_infos->last()->id));
                        $current_working_info_that_have_go_out_error = ($the_working_info_to_attach_error_to !== null) ? $the_working_info_to_attach_error_to : $working_infos->last();
                    }

                    if ($current_working_info_to_which_current_go_out_time_belong !== null &&
                        $go_out_flag === true) {

                        $carbon_go_out_start = $this->getCarbonInstance($go_out_start);
                        $carbon_go_out_end = $this->getCarbonInstance($timestamp->raw_date_time_value);
                        $sum_go_out_time += $carbon_go_out_end->diffInMinutes($carbon_go_out_start);

                        $current_working_info_to_which_current_go_out_time_belong->real_go_out_time = $sum_go_out_time;
                        $go_out_flag = false;

                        if ($current_working_info_that_have_go_out_error == null || $current_working_info_to_which_current_go_out_time_belong->id !== $current_working_info_that_have_go_out_error->id) {
                            // Change color status base on this timestamp is a manually added timestamp or not.
                            event(new RealGoOutTimeAttributeManuallyChanged($current_working_info_to_which_current_go_out_time_belong, $timestamp->registerer_type == WorkingTimestamp::TABLET));
                        }
                    }

                /**
                 * If it's an end_work timestamp, find the suitable working info for this timestamp, assign values to the timestamped_end_work_time, work_location_id, work_address_id attributes.
                 */
                } else if ($timestamp->timestamped_type === WorkingTimestamp::END_WORK) {

                    // If a timestamp of anytype other than GO_OUT is at the last of the list, the forgot_return_error will be void
                    $the_odd_go_out = null;

                    // 外出後に戻り以外がある場合 - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                    if (($timestamp->work_address_id == null && ($last_timestamp !== null && $last_timestamp->timestamped_type === WorkingTimestamp::GO_OUT)) ||
                        ($timestamp->work_address_id != null && ($last_work_address_timestamp !== null && $last_work_address_timestamp->timestamped_type === WorkingTimestamp::GO_OUT))) {

                        // 2018-04-13, when there's no current working info to which the current go out time belong, we connect that error to the last working info
                        event(new TimestampGoOutReturnError($working_day, ($current_working_info_to_which_current_go_out_time_belong !== null) ? $current_working_info_to_which_current_go_out_time_belong->id : $working_infos->last()->id));
                        $current_working_info_that_have_go_out_error = ($current_working_info_to_which_current_go_out_time_belong !== null) ? $current_working_info_to_which_current_go_out_time_belong : $working_infos->last();
                    }

                    // Update 2019-05-13 Work address flow will not use the pair rule
                    if ($timestamp->work_address_id == null && $pair_rule_mode) {
                        if ($pair_rule_mode_current_working_info == null) {
                            $suitable_working_info = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisEndWorkTimestamp($working_infos, $timestamp);
                        } else {
                            $suitable_working_info = $pair_rule_mode_current_working_info;
                            $pair_rule_mode_current_working_info = null;
                        }

                    } else {
                        $suitable_working_info = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisEndWorkTimestamp($working_infos, $timestamp);
                    }

                    if ($suitable_working_info !== null && $suitable_working_info !== -3) {
                        // 退勤・退勤
                        if ($suitable_working_info->timestamped_end_work_time !== null) {
                            if (!$suitable_working_info->haveConsideringTimestampedEndWorkTime()) {
                                event(new TimestampEndWorkError($working_day, $suitable_working_info->id));
                            }

                        } else {

                            $suitable_working_info->timestamped_end_work_time = $timestamp->raw_date_time_value;
                            $suitable_working_info->timestamped_end_work_time_work_location_id = $timestamp->work_location_id;
                            $suitable_working_info->timestamped_end_work_time_work_address_id = $timestamp->work_address_id;
                            $suitable_working_info->timestamped_end_work_time_working_timestamp_id = $timestamp->id;
                            $this->assignPlaceInfo($suitable_working_info, $timestamp);

                            if (!$suitable_working_info->haveConsideringTimestampedEndWorkTime()) {
                                // Change color status base on this timestamp is a manually added timestamp or not.
                                event(new TimeStampedEndWorkTimeAttributeManuallyChanged($suitable_working_info, $timestamp->registerer_type == WorkingTimestamp::TABLET || $timestamp->registerer_type == WorkingTimestamp::MOBILE_APP, $timestamp->approved));
                            }
                        }

                        // 出勤が無い時に退勤を打刻した場合
                        if ($suitable_working_info->timestamped_start_work_time === null) {
                            if (!$suitable_working_info->haveConsideringTimestampedStartWorkTime()) {
                                event(new TimestampStartWorkError($working_day, $suitable_working_info->id));
                            }
                            $current_working_infor_that_have_start_work_error = $suitable_working_info;
                        }

                    // Even after searching among the not-have-planned_start/end_work_time EWIs, and we still can't find a suitable working info, create a end work error and connect it to the last working info
                    } else if ($suitable_working_info == -3) {
                        if ($timestamp->work_address_id != null) {
                            $temporary_working_info = $this->searchHidingTemporaryEmployeeWorkingInformations($timestamp, $temporary_ewis_of_this_working_day);
                            if ($temporary_working_info == null) {
                                $temporary_working_info = new EmployeeWorkingInformation();
                                $temporary_working_info->employeeWorkingDay()->associate($working_day);
                                $temporary_working_info->temporary = true;


                                $this->assignPlaceInfo($temporary_working_info, $timestamp, true);
                                $temporary_working_info->timestamped_end_work_time = $timestamp->raw_date_time_value;
                                $temporary_working_info->timestamped_end_work_time_work_location_id = $timestamp->work_location_id;
                                $temporary_working_info->timestamped_end_work_time_work_address_id = $timestamp->work_address_id;
                                $temporary_working_info->timestamped_end_work_time_working_timestamp_id = $timestamp->id;
                                $temporary_working_info->save();

                                // Update 2019-05-18: With the new flow of temp EWI with work address, we don't establish connection between temp EWI with WAWI nor WAWE
                                // if ($temporary_working_info->planned_work_address_id != null) {
                                //     $this->establishConnectionWithWorkAddressWorkingInformationForTemporaryEmployeeWorkingInformation($temporary_working_info, $working_day);
                                // }

                                // Throw a confirm_needed checklist error for this
                                // event(new ConfimNeededWorkWithoutScheduleError($working_day, $temporary_working_info->id));

                            } else {
                                $this->assignPlaceInfo($temporary_working_info, $timestamp, true);
                                $temporary_working_info->timestamped_end_work_time = $timestamp->raw_date_time_value;
                                $temporary_working_info->timestamped_end_work_time_work_location_id = $timestamp->work_location_id;
                                $temporary_working_info->timestamped_end_work_time_work_address_id = $timestamp->work_address_id;
                                $temporary_working_info->timestamped_end_work_time_working_timestamp_id = $timestamp->id;
                                $temporary_working_info->save();

                                // Throw a confirm_needed checklist error for this
                                // event(new ConfimNeededWorkWithoutScheduleError($working_day, $temporary_working_info->id));
                            }

                            // Push that new temporary working_info into the working infos list
                            $full_working_info_list->push($temporary_working_info);
                            $working_infos->push($temporary_working_info);

                        } else {
                            if (!$working_infos->last()->haveConsideringTimestampedEndWorkTime()) {
                                event(new TimestampEndWorkError($working_day, $working_infos->last()->id));
                            }
                        }
                    }

                }

                // Assign the last timestamp - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                if ($timestamp->work_address_id == null) {
                    $last_timestamp = $timestamp;
                } else {
                    $last_work_address_timestamp = $timestamp;
                }
            }

            // Fire forgot_return_error event if the last timestamp is a go_out timestamp (odd go_out timestamp)
            if ($the_odd_go_out !== null) {
                event(new TimestampForgotReturnError($working_day, $current_working_info_to_which_current_go_out_time_belong == null ? null : $current_working_info_to_which_current_go_out_time_belong->id, $the_odd_go_out));
            }

            foreach ($full_working_info_list as $working_info) {
                $working_info->save();

                // If the model is not dirty then the call save() will not trigger 'updating' event on the Observer, that's why we need to call this function
                // here manually. Doing this will reduce some queries to the database.
                if (!$working_info->isDirty()) {
                    $this->evaluateChecklistErrorsFromAnInstanceOfEmployeeWorkingInformation($working_info);
                }
            }
        }
    }


    /**
     * This function distribute the working timestamps of an EmployeeWorkingDay into its EmployeeWorkingInformations.
     * It works almost identical to the above function except it wont fire any event to create checklist error entry, color status nor trigger evaluating checklist
     * errors on EmployeeWorkingInformations
     *
     * @param EmployeeWorkingDay        $working_day
     * @param Collection                $temporary_ewis
     * @return void
     */
    public function distributeWorkingTimestampWithoutEvaluatingChecklistErrors(EmployeeWorkingDay $working_day, $temporary_ewis)
    {
        $temporary_ewis_of_this_working_day = $temporary_ewis->has($working_day->id) ? $temporary_ewis[$working_day->id] : collect([]);

        $working_infos = $working_day->employeeWorkingInformations->sortBy('planned_start_work_time');
        $timestamps = $working_day->workingTimestamps;

        $timestamps = $timestamps->filter(function($timestamp) {
            return $timestamp->enable == true;
        })->sortBy('raw_date_time_value');

        // Updated 2019-01-30: to accommodate the WorkAddress's logic, resolve the service and try to preload WorkAddressWorkingDay as much as we can
        $this->initializeWorkAddressWorkingDayLoadingServiceIfNotYetAndPreloadSomeInstances($working_day, $timestamps);

        // First we reset all the timestamped_start/end_work_time, real_work_location/address_id and real_go_out_time of all the working_infos
        foreach ($working_infos as $working_info) {
            $working_info->timestamped_start_work_time = null;
            $working_info->timestamped_start_work_time_work_location_id = null;
            $working_info->timestamped_start_work_time_work_address_id = null;
            $working_info->timestamped_start_work_time_working_timestamp_id = null;
            $working_info->timestamped_end_work_time = null;
            $working_info->timestamped_end_work_time_work_location_id = null;
            $working_info->timestamped_end_work_time_work_address_id = null;
            $working_info->timestamped_end_work_time_working_timestamp_id = null;
            $working_info->real_work_location_id = null;
            $working_info->real_work_address_id = null;
            $working_info->real_go_out_time = null;
        }


        // Then calculate
        // $enter_work_flag = false;
        $go_out_flag = false;
        $go_out_start = null;
        // $go_out_end = null;
        $sum_go_out_time = 0;
        $current_working_info_to_which_current_go_out_time_belong = null;
        $current_working_info_that_have_go_out_error = null;
        $current_working_infor_that_have_start_work_error = null;
        $the_odd_go_out = null;
        // $current_working_info = null;

        $last_timestamp = null;
        $last_work_address_timestamp = null;

        $full_working_info_list = $working_infos;
        $only_work_location_working_info_list = null;
        $only_work_address_working_info_list = null;
        /**
         * Update 2019-05-06: Base on if timestamp has work address or not, we have to separate work location's working infos and work address's working infos
         * here.
         */
        $only_work_location_working_info_list = $full_working_info_list->filter(function($working_info) {
            return $working_info->planned_work_address_id == null;
        });
        $only_work_address_working_info_list = $full_working_info_list->filter(function($working_info) {
            return $working_info->planned_work_address_id != null;
        });

        // Check if there exist a working info without planned_start/end_work_time, if so turn on pair_mode.
        $exist_ewi_without_planned_start_end_work_time = $only_work_location_working_info_list->first(function($working_info) {
            return $working_info->planned_start_work_time == null || $working_info->planned_end_work_time == null;
        });
        $pair_rule_mode = $exist_ewi_without_planned_start_end_work_time !== null;
        $pair_rule_mode_current_working_info = null;

        foreach ($timestamps as $timestamp) {

            if ($timestamp->work_address_id != null) {
                $working_infos = &$only_work_address_working_info_list;
            } else {
                $working_infos = &$only_work_location_working_info_list;
            }

            if ($working_infos->isEmpty()) {

                $temporary_working_info = $this->searchHidingTemporaryEmployeeWorkingInformations($timestamp, $temporary_ewis_of_this_working_day);

                if ($temporary_working_info == null) {
                    $temporary_working_info = new EmployeeWorkingInformation();
                    $temporary_working_info->employeeWorkingDay()->associate($working_day);
                    $temporary_working_info->temporary = true;


                    $this->assignPlaceInfo($temporary_working_info, $timestamp, true);
                    $temporary_working_info->save();
                    $temporary_working_info->just_been_created = true;

                    // Update 2019-05-18: With the new flow of temp EWI with work address, we don't establish connection between temp EWI with WAWI nor WAWE
                    // if ($temporary_working_info->planned_work_address_id != null) {
                    //     $this->establishConnectionWithWorkAddressWorkingInformationForTemporaryEmployeeWorkingInformation($temporary_working_info, $working_day);
                    // }

                    // Throw a confirm_needed checklist error for this
                    // event(new ConfimNeededWorkWithoutScheduleError($working_day, $temporary_working_info->id));

                } else {
                    $this->assignPlaceInfo($temporary_working_info, $timestamp, true);
                    $temporary_working_info->save();
                    $temporary_working_info->just_been_created = true;

                    // Throw a confirm_needed checklist error for this
                    // event(new ConfimNeededWorkWithoutScheduleError($working_day, $temporary_working_info->id));
                }

                // Push that new temporary working_info into the working infos list
                $full_working_info_list->push($temporary_working_info);
                $working_infos->push($temporary_working_info);

                if (!$timestamp->work_address_id) {
                    $pair_rule_mode = true;
                    $pair_rule_mode_current_working_info = null;
                }
            }

            /**
             * If it's a start_work timestamp, we find the suitable working info (based on the schedule of that working info), then assign value for
             * timestamped_start_work_time, real_work_location_id, real_work_address_id attributes.
             */
            if ($timestamp->timestamped_type === WorkingTimestamp::START_WORK) {

                // If a timestamp of anytype other than GO_OUT is at the last of the list, the forgot_return_error will be void
                $the_odd_go_out = null;

                // 外出後に戻り以外がある場合 - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                if (($timestamp->work_address_id == null && ($last_timestamp !== null && $last_timestamp->timestamped_type === WorkingTimestamp::GO_OUT)) ||
                    ($timestamp->work_address_id != null && ($last_work_address_timestamp !== null && $last_work_address_timestamp->timestamped_type === WorkingTimestamp::GO_OUT))) {

                    // 2018-04-13, when there's no current working info to which the current go out time belong, we connect that error to the last working info
                    // event(new TimestampGoOutReturnError($working_day, ($current_working_info_to_which_current_go_out_time_belong !== null) ? $current_working_info_to_which_current_go_out_time_belong->id : $working_infos->last()->id));
                    $current_working_info_that_have_go_out_error = ($current_working_info_to_which_current_go_out_time_belong !== null) ? $current_working_info_to_which_current_go_out_time_belong : $working_infos->last();
                }

                $suitable_working_info = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisStartWorkTimestamp($working_infos, $timestamp);

                if ($suitable_working_info !== null && $suitable_working_info !== -1 && $suitable_working_info !== -2 && $suitable_working_info !== -3) {

                    // 出勤・出勤→出勤エラー
                    if ($suitable_working_info->timestamped_start_work_time !== null) {
                        // event(new TimestampStartWorkError($working_day, $suitable_working_info->id));

                    } else {
                        $suitable_working_info->timestamped_start_work_time = $timestamp->raw_date_time_value;
                        $suitable_working_info->timestamped_start_work_time_work_location_id = $timestamp->work_location_id;
                        $suitable_working_info->timestamped_start_work_time_work_address_id = $timestamp->work_address_id;
                        $suitable_working_info->timestamped_start_work_time_working_timestamp_id = $timestamp->id;
                        $this->assignPlaceInfo($suitable_working_info, $timestamp);

                        // Reset go out time
                        $current_working_info_to_which_current_go_out_time_belong = $suitable_working_info;
                        $go_out_flag = false;
                        $sum_go_out_time = 0;

                        // Set the pivot for pair_rule_mode - Update 2019-05-13 Work address flow will not use the pair rule
                        if ($timestamp->work_address_id == null && $pair_rule_mode) {
                            $pair_rule_mode_current_working_info = $suitable_working_info;
                        }

                        if (($suitable_working_info->just_been_created == true) || !$suitable_working_info->haveConsideringTimestampedStartWorkTime()) {
                            if ($current_working_infor_that_have_start_work_error == null || $suitable_working_info->id !== $current_working_infor_that_have_start_work_error->id) {
                                // Change color status base on this timestamp is a manually added timestamp or not.
                                event(new TimeStampedStartWorkTimeAttributeManuallyChanged($suitable_working_info, $timestamp->registerer_type == WorkingTimestamp::TABLET || $timestamp->registerer_type == WorkingTimestamp::MOBILE_APP, $timestamp->approved));
                            }
                        }
                    }

                // If the location of this timestamp is not match,
                // or if all the working infos with the same location have already had both 'timestamped_start/end_work_time',
                // create a temporary one, throw an check-list error, reload the working_infos list.
                } else if ($suitable_working_info == -1 || $suitable_working_info == -2) {
                    $temporary_working_info = $this->createATemporaryWorkingInformationWithAStartWorkTimestamp($working_day, $timestamp, $temporary_ewis_of_this_working_day, true);
                    // event(new ConfimNeededDifferenceFromScheduleError($working_day, $temporary_working_info->id));

                    // Change color status base on this timestamp is a manually added timestamp or not.
                    event(new TimeStampedStartWorkTimeAttributeManuallyChanged($temporary_working_info, $timestamp->registerer_type == WorkingTimestamp::TABLET || $timestamp->registerer_type == WorkingTimestamp::MOBILE_APP, $timestamp->approved));

                    $full_working_info_list->push($temporary_working_info);
                    $working_infos->push($temporary_working_info);

                    // Toggle pair mode ON and set the pivot for pair_rule_mode if the temporary working info is not a work address's working info
                    if ($temporary_working_info->planned_work_address_id == null) {
                        $pair_rule_mode = true;
                        $pair_rule_mode_current_working_info = $temporary_working_info;
                    }

                // Even after searching among the not-have-planned_start/end_work_time EWIs, and we still can't find a suitable working info, create a start work error and connect it to the last working info
                } else if ($suitable_working_info == -3) {
                    // event(new TimestampStartWorkError($working_day, $working_infos->last()->id));
                }


            /**
             * If it's a go_out_start timestamp, we find the suitable working info, if there is a current working information to which this go_out_time belong, check weather or not
             * the suitable working info is different from the current working info. If they are the same just proceed normally. If not, reset the go_out_time ($go_out_start and $sum_go_out_time)
             * and update the current working information, then proceed.
             */
            } else if ($timestamp->timestamped_type === WorkingTimestamp::GO_OUT) {

                // If a timestamp of anytype other than GO_OUT is at the last of the list, the forgot_return_error will be void
                $the_odd_go_out = null;

                // Update 2019-05-13 Work address flow will not use the pair rule
                if ($timestamp->work_address_id == null && $pair_rule_mode) {
                    if ($pair_rule_mode_current_working_info) {
                        $suitable_working_info = $pair_rule_mode_current_working_info;
                    } else {
                        $suitable_working_info = null;
                    }
                } else {
                    $suitable_working_info = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisGoOutOrReturnTimestamp($working_infos, $timestamp);
                }

                // 外出の前が出勤または戻りでない場合 - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                if (($timestamp->work_address_id == null && (($last_timestamp === null) || ($last_timestamp !== null && $last_timestamp->timestamped_type === WorkingTimestamp::GO_OUT) || ($last_timestamp !== null && $last_timestamp->timestamped_type === WorkingTimestamp::END_WORK))) ||
                ($timestamp->work_address_id != null && (($last_work_address_timestamp === null) || ($last_work_address_timestamp !== null && $last_work_address_timestamp->timestamped_type === WorkingTimestamp::GO_OUT) || ($last_work_address_timestamp !== null && $last_work_address_timestamp->timestamped_type === WorkingTimestamp::END_WORK)))) {

                    // 2018-04-13, when we cant find the suitable working info, but there's error, then we connect that error to the last working info
                    // event(new TimestampGoOutReturnError($working_day, ($suitable_working_info !== null) ? $suitable_working_info->id : $working_infos->last()->id));
                    $current_working_info_that_have_go_out_error = ($suitable_working_info !== null) ? $suitable_working_info : $working_infos->last();
                }

                if ($suitable_working_info !== null) {

                    if ($current_working_info_to_which_current_go_out_time_belong !== null) {

                        if ($current_working_info_to_which_current_go_out_time_belong->id !== $suitable_working_info->id) {
                            $current_working_info_to_which_current_go_out_time_belong = $suitable_working_info;
                            $go_out_start = $timestamp->raw_date_time_value;
                            $sum_go_out_time = 0;

                        }
                    } else {
                        $current_working_info_to_which_current_go_out_time_belong = $suitable_working_info;
                    }

                    if ($go_out_flag === false) {
                        $go_out_start = $timestamp->raw_date_time_value;
                        $go_out_flag = true;
                    }
                }

                // $this->assignPlaceInfo($suitable_working_info, $timestamp);

                // If this go_out timestamp is the last timestamp of this day, then the will be an odd go_out timestamp, which
                // can be used to fire a forgot_return_error event later.
                $the_odd_go_out = $timestamp->raw_date_time_value;

            /**
             * If it's a return timestamp, confirm if this timestamp is within the current working information to proceed with the calculation. If proceed, calculate the go_out_time
             * then update the real_go_out_time of that working info, then reset the $go_out_flag.
             */
            } else if ($timestamp->timestamped_type === WorkingTimestamp::RETURN) {

                // If a timestamp of anytype other than GO_OUT is at the last of the list, the forgot_return_error will be void
                $the_odd_go_out = null;

                // 戻りの前が外出以外の場合 - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                if (($timestamp->work_address_id == null && ($last_timestamp === null || $last_timestamp->timestamped_type !== WorkingTimestamp::GO_OUT)) ||
                    ($timestamp->work_address_id != null && ($last_work_address_timestamp === null || $last_work_address_timestamp->timestamped_type !== WorkingTimestamp::GO_OUT))) {

                    $the_working_info_to_attach_error_to = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisGoOutOrReturnTimestamp($working_infos, $timestamp);
                    // 2018-04-13, when we cant find the suitable working info, but there's error, then we connect that error to the last working info
                    // event(new TimestampGoOutReturnError($working_day, ($the_working_info_to_attach_error_to !== null) ? $the_working_info_to_attach_error_to->id : $working_infos->last()->id));
                    $current_working_info_that_have_go_out_error = ($the_working_info_to_attach_error_to !== null) ? $the_working_info_to_attach_error_to : $working_infos->last();
                }

                if ($current_working_info_to_which_current_go_out_time_belong !== null &&
                    $go_out_flag === true) {

                    $carbon_go_out_start = $this->getCarbonInstance($go_out_start);
                    $carbon_go_out_end = $this->getCarbonInstance($timestamp->raw_date_time_value);
                    $sum_go_out_time += $carbon_go_out_end->diffInMinutes($carbon_go_out_start);

                    $current_working_info_to_which_current_go_out_time_belong->real_go_out_time = $sum_go_out_time;
                    $go_out_flag = false;

                    if ($current_working_info_that_have_go_out_error == null || $current_working_info_to_which_current_go_out_time_belong->id !== $current_working_info_that_have_go_out_error->id) {
                        // Change color status base on this timestamp is a manually added timestamp or not.
                        event(new RealGoOutTimeAttributeManuallyChanged($current_working_info_to_which_current_go_out_time_belong, $timestamp->registerer_type == WorkingTimestamp::TABLET));
                    }
                }

            /**
             * If it's an end_work timestamp, find the suitable working info for this timestamp, assign values to the timestamped_end_work_time, work_location_id, work_address_id attributes.
             */
            } else if ($timestamp->timestamped_type === WorkingTimestamp::END_WORK) {

                // If a timestamp of anytype other than GO_OUT is at the last of the list, the forgot_return_error will be void
                $the_odd_go_out = null;

                // 外出後に戻り以外がある場合 - Update 2019-05-13: Add one condition for Work Address flow, using another variable to hold the last timestamp of work address flow.
                if (($timestamp->work_address_id == null && ($last_timestamp !== null && $last_timestamp->timestamped_type === WorkingTimestamp::GO_OUT)) ||
                    ($timestamp->work_address_id != null && ($last_work_address_timestamp !== null && $last_work_address_timestamp->timestamped_type === WorkingTimestamp::GO_OUT))) {

                    // 2018-04-13, when there's no current working info to which the current go out time belong, we connect that error to the last working info
                    // event(new TimestampGoOutReturnError($working_day, ($current_working_info_to_which_current_go_out_time_belong !== null) ? $current_working_info_to_which_current_go_out_time_belong->id : $working_infos->last()->id));
                    $current_working_info_that_have_go_out_error = ($current_working_info_to_which_current_go_out_time_belong !== null) ? $current_working_info_to_which_current_go_out_time_belong : $working_infos->last();
                }

                // Update 2019-05-13 Work address flow will not use the pair rule
                if ($timestamp->work_address_id == null && $pair_rule_mode) {
                    if ($pair_rule_mode_current_working_info == null) {
                        $suitable_working_info = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisEndWorkTimestamp($working_infos, $timestamp);
                    } else {
                        $suitable_working_info = $pair_rule_mode_current_working_info;
                        $pair_rule_mode_current_working_info = null;
                    }

                } else {
                    $suitable_working_info = $this->getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisEndWorkTimestamp($working_infos, $timestamp);
                }

                if ($suitable_working_info !== null && $suitable_working_info !== -3) {
                    // 退勤・退勤
                    if ($suitable_working_info->timestamped_end_work_time !== null) {

                        // event(new TimestampEndWorkError($working_day, $suitable_working_info->id));

                    } else {

                        $suitable_working_info->timestamped_end_work_time = $timestamp->raw_date_time_value;
                        $suitable_working_info->timestamped_end_work_time_work_location_id = $timestamp->work_location_id;
                        $suitable_working_info->timestamped_end_work_time_work_address_id = $timestamp->work_address_id;
                        $suitable_working_info->timestamped_end_work_time_working_timestamp_id = $timestamp->id;
                        $this->assignPlaceInfo($suitable_working_info, $timestamp);

                        if (($suitable_working_info->just_been_created == true) || !$suitable_working_info->haveConsideringTimestampedEndWorkTime()) {
                            // Change color status base on this timestamp is a manually added timestamp or not.
                            event(new TimeStampedEndWorkTimeAttributeManuallyChanged($suitable_working_info, $timestamp->registerer_type == WorkingTimestamp::TABLET || $timestamp->registerer_type == WorkingTimestamp::MOBILE_APP, $timestamp->approved));
                        }
                    }

                    // 出勤が無い時に退勤を打刻した場合
                    if ($suitable_working_info->timestamped_start_work_time === null) {
                        // event(new TimestampStartWorkError($working_day, $suitable_working_info->id));
                        $current_working_infor_that_have_start_work_error = $suitable_working_info;
                    }

                // Even after searching among the not-have-planned_start/end_work_time EWIs, and we still can't find a suitable working info, create a end work error and connect it to the last working info
                } else if ($suitable_working_info == -3) {
                    // event(new TimestampEndWorkError($working_day, $working_infos->last()->id));
                    if ($timestamp->work_address_id != null) {
                        $temporary_working_info = $this->searchHidingTemporaryEmployeeWorkingInformations($timestamp, $temporary_ewis_of_this_working_day);
                        if ($temporary_working_info == null) {
                            $temporary_working_info = new EmployeeWorkingInformation();
                            $temporary_working_info->employeeWorkingDay()->associate($working_day);
                            $temporary_working_info->temporary = true;


                            $this->assignPlaceInfo($temporary_working_info, $timestamp, true);
                            $temporary_working_info->timestamped_end_work_time = $timestamp->raw_date_time_value;
                            $temporary_working_info->timestamped_end_work_time_work_location_id = $timestamp->work_location_id;
                            $temporary_working_info->timestamped_end_work_time_work_address_id = $timestamp->work_address_id;
                            $temporary_working_info->timestamped_end_work_time_working_timestamp_id = $timestamp->id;
                            $temporary_working_info->save();
                            $temporary_working_info->just_been_created = true;

                            // Update 2019-05-18: With the new flow of temp EWI with work address, we don't establish connection between temp EWI with WAWI nor WAWE
                            // if ($temporary_working_info->planned_work_address_id != null) {
                            //     $this->establishConnectionWithWorkAddressWorkingInformationForTemporaryEmployeeWorkingInformation($temporary_working_info, $working_day);
                            // }

                            // Throw a confirm_needed checklist error for this
                            // event(new ConfimNeededWorkWithoutScheduleError($working_day, $temporary_working_info->id));

                        } else {
                            $this->assignPlaceInfo($temporary_working_info, $timestamp, true);
                            $temporary_working_info->timestamped_end_work_time = $timestamp->raw_date_time_value;
                            $temporary_working_info->timestamped_end_work_time_work_location_id = $timestamp->work_location_id;
                            $temporary_working_info->timestamped_end_work_time_work_address_id = $timestamp->work_address_id;
                            $temporary_working_info->timestamped_end_work_time_working_timestamp_id = $timestamp->id;
                            $temporary_working_info->save();
                            $temporary_working_info->just_been_created = true;

                            // Throw a confirm_needed checklist error for this
                            // event(new ConfimNeededWorkWithoutScheduleError($working_day, $temporary_working_info->id));
                        }

                        // Push that new temporary working_info into the working infos list
                        $full_working_info_list->push($temporary_working_info);
                        $working_infos->push($temporary_working_info);

                    } else {
                        // if (!$working_infos->last()->haveConsideringTimestampedEndWorkTime()) {
                            // event(new TimestampEndWorkError($working_day, $working_infos->last()->id));
                        // }
                    }
                }

            }

            // Assign the last timestamp
            if ($timestamp->work_address_id == null) {
                $last_timestamp = $timestamp;
            } else {
                $last_work_address_timestamp = $timestamp;
            }
        }

        foreach ($full_working_info_list as $working_info) {
            $working_info->turnOffChecklistEvaluationInEvents();
            $working_info->save();
            $working_info->turnOnChecklistEvaluationInEvents();
            // If the model is not dirty then the call save() will not trigger 'updating' event on the Observer, that's why we need to call this function
            // here manually. Doing this will reduce some queries to the database.
            // if (!$working_info->isDirty()) {
            //     $this->evaluateChecklistErrorsFromAnInstanceOfEmployeeWorkingInformation($working_info);
            // }
        }

    }


    /**
     * Create a temporary working information with a start_work_time timestamp. This is different from the case wheren there is no working_info.
     *
     * @param EmployeeWorkingDay        $working_day
     * @param WorkingTimestamp          $start_work_timestamp
     * @param boolean                   $temporarily_turn_off_checklist_errors_evaluation: in case you want to create EWI but also want to ignore the checklist errors evaluation when saving
     * @return EmployeeWorkingInformation
     */
    protected function createATemporaryWorkingInformationWithAStartWorkTimestamp($working_day, $start_work_timestamp, $temporary_working_infos, $temporarily_turn_off_checklist_errors_evaluation = false)
    {
        $temporary_working_info = $this->searchHidingTemporaryEmployeeWorkingInformations($start_work_timestamp, $temporary_working_infos);

        if ($temporary_working_info == null) {
            $temporary_working_info = new EmployeeWorkingInformation();
            $temporary_working_info->employeeWorkingDay()->associate($working_day);
            $temporary_working_info->temporary = true;
        }

        $temporary_working_info->timestamped_start_work_time = $start_work_timestamp->raw_date_time_value;
        $temporary_working_info->timestamped_start_work_time_work_location_id = $start_work_timestamp->work_location_id;
        $temporary_working_info->timestamped_start_work_time_work_address_id = $start_work_timestamp->work_address_id;
        $temporary_working_info->timestamped_start_work_time_working_timestamp_id = $start_work_timestamp->id;

        $this->assignPlaceInfo($temporary_working_info, $start_work_timestamp, true);

        if ($temporarily_turn_off_checklist_errors_evaluation == true) {
            $temporary_working_info->turnOffChecklistEvaluationInEvents();
        }
        $temporary_working_info->save();

        // Update 2019-05-18: With the new flow of temp EWI with work address, we don't establish connection between temp EWI with WAWI nor WAWE
        // if ($temporary_working_info->planned_work_address_id != null) {
        //     $this->establishConnectionWithWorkAddressWorkingInformationForTemporaryEmployeeWorkingInformation($temporary_working_info, $working_day);
        // }

        return $temporary_working_info;
    }

    /**
     * Different from a normal temporary EmployeeWorkingInformation, a temporary EmployeeWorkingInformation with a WorkAddress need more attention.
     * We need to correctly establish the connection between it and the suitable WorkAddressWorkingInformation.
     *
     * @param   EmployeeWorkingInformation      $temporary_employee_working_info
     * @param   EmployeeWorkingDay              $employee_working_day
     * @return  void
     */
    protected function establishConnectionWithWorkAddressWorkingInformationForTemporaryEmployeeWorkingInformation($temporary_employee_working_info, $employee_working_day)
    {
        if ($employee_working_day->employee->workLocation->company->use_address_system == true) {

            $work_address_working_day_loading_service = resolve(WorkAddressWorkingDayLoadingService::class);

            $work_address_working_day = $work_address_working_day_loading_service->getWorkAddressWorkingDayByDateAndWorkAddressId($employee_working_day->date, $temporary_employee_working_info->planned_work_address_id);

            $work_address_working_info = $work_address_working_day->workAddressWorkingInformations->first(function($work_address_working_info) {
                return $work_address_working_info->schedule_start_work_time == null && $work_address_working_info->schedule_end_work_time == null;
            });

            // If there is a suitable WorkAddressWorkingInformation, establish the connection to this temporary EmployeeWorkingInformation
            if ($work_address_working_info) {
                $work_address_working_employee = $work_address_working_info->workAddressWorkingEmployees->first(function ($working_employee) use ($employee_working_day) {
                    return $working_employee->employee_id === $employee_working_day->employee_id;
                });

                if ($work_address_working_employee) {
                    if (!$work_address_working_employee->working_confirm) {
                        $work_address_working_employee->working_confirm = true;
                        $temporary_employee_working_info->work_address_working_employee_id = $work_address_working_employee->id;
                        $work_address_working_employee->save();
                        $temporary_employee_working_info->save();
                    } // If the working_confirm is true then there must be something wrong here

                // Create a WorkAddressWorkingEmployee if not exist for this $temporary_employee_working_info
                } else {
                    $new_work_address_working_employee = $this->createWorkAddressWorkingEmployeeForTemporaryEmployeeWorkingInformation(
                        $work_address_working_info,
                        $employee_working_day,
                        $temporary_employee_working_info
                    );

                    // Update the relationship of this WorkAddressWorkingInformation, need to do this to persist the change to the service
                    $work_address_working_info->workAddressWorkingEmployees->push($new_work_address_working_employee);
                }

            // If not create everything then establish the connection
            } else {
                $new_work_address_working_info = new WorkAddressWorkingInformation([
                    'work_address_working_day_id' => $work_address_working_day->id,
                ]);
                $new_work_address_working_info->save();

                $new_work_address_working_employee = $this->createWorkAddressWorkingEmployeeForTemporaryEmployeeWorkingInformation(
                    $new_work_address_working_info,
                    $employee_working_day,
                    $temporary_employee_working_info
                );

                // This is for updating the relationship of WorkAddressWorkingInformation, but why is it different from the above ?
                // Because, this instance is just newly created, it doesnt have a 'workAddressWorkingEmployees' in the relationship array yet.
                // If we use the same syntax as above, it will first query 'workAddressWorkingEmployees' relationship, then it add this new
                // WorkAddressWorkingEmployee, and thus will result in duplications.
                $new_work_address_working_info->setRelation('workAddressWorkingEmployees', collect([$new_work_address_working_employee]));

                // Update relationship of WorkAddressWorkingDay
                $work_address_working_day->workAddressWorkingInformations->push($new_work_address_working_info);
            }

            // Replace the old instance which doesn't have the relationship with the new things in the preloaded array
            $work_address_working_day_loading_service->updateASpecificWorkAddressWorkingDay($work_address_working_day);
        }
    }

    /**
     * Create a WorkAddressWorkingEmployee for a temporary EmployeeWorkingInformation.
     * Just a utility function.
     *
     * @param   WorkAddressWorkingInformation   $work_address_working_info,
     * @param   EmployeeWorkingDay              $employee_working_day,
     * @param   EmployeeWorkingInformation      $temporary_employee_working_info
     * @return  WorkAddressWorkingEmployee
     */
    protected function createWorkAddressWorkingEmployeeForTemporaryEmployeeWorkingInformation(
        $work_address_working_info,
        $employee_working_day,
        $temporary_employee_working_info
    )   {
        $new_working_employee = new WorkAddressWorkingEmployee([
            'work_address_working_information_id' => $work_address_working_info->id,
            'employee_id' => $employee_working_day->employee_id,
            'employee_working_day_id' => $employee_working_day->id,
            'working_confirm' => true,
        ]);
        $new_working_employee->save();

        $temporary_employee_working_info->work_address_working_employee_id = $new_working_employee->id;
        $temporary_employee_working_info->save();

        return $new_working_employee;
    }

    /**
     * Initialize service and preload some instances of WorkAddressWorkingDays.
     *
     * @param   EmployeeWorkingDay      $employee_working_day
     * @param   Collection              $timestamps
     * @return  void
     */
    protected function initializeWorkAddressWorkingDayLoadingServiceIfNotYetAndPreloadSomeInstances($employee_working_day, $timestamps)
    {
        if ($employee_working_day->employee->workLocation->company->use_address_system == true) {
            $work_address_working_day_loading_service = resolve(WorkAddressWorkingDayLoadingService::class);

            $work_address_working_day_loading_service->preloadWorkAddressWorkingDaysUsingDateAndWorkAddressIds($employee_working_day->date, $timestamps->pluck('work_address_id')->toArray());
        }
    }

    /**
     * Search in a given bag of hiding temporary EmployeeWorkingInformations for one that match with the given WorkingTimestamp.
     * ATTENTION: after call this function, if you do find the matched working info, then you have to save that working info at some point after that.
     * or else, the temporary working info will go back to hiding.
     *
     * @param WorkingTimestamp      $timestamp
     * @param Collection            $temporary_working_infos
     * @return EmployeeWorkingInformation|null
     */
    protected function searchHidingTemporaryEmployeeWorkingInformations($timestamp, $temporary_working_infos)
    {
        $matched_working_info = $temporary_working_infos->first(function($working_info) use ($timestamp) {
            return $working_info->real_work_location_id === $timestamp->work_location_id && $working_info->real_work_address_id == $timestamp->work_address_id && $working_info->not_show_when_temporary == true;
        });

        if ($matched_working_info) {
            $matched_working_info->not_show_when_temporary = false;
        }

        return $matched_working_info;
    }


    /**
     * Find the suitable EmployeeWorkingInformation for this start_work_time timestamp
     *
     * @param Collection            $working_infos
     * @param WorkingTimestamp      $timestamp
     * @return  -1|-2|-3|EmployeeWorkingInformation      Explain:
     *
     *          -1      => the working info with the matched location does not exist
     *          -2      => all the matched location working infos has already had 'timestamped_'
     *          -3      => there is at least a working info with empty timestamped_end_work_time, but its timestamped_start_work_time has already been taken
     *          or an instance of EmployeeWorkingInformation, which is the suitable one.
     */
    protected function getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisStartWorkTimestamp($working_infos, $timestamp)
    {
        // First, filter the list to get the working_infos that have the same place with the timestamp
        $same_place_working_infos = $working_infos->filter(function($working_info) use ($timestamp) {
            return $working_info->planned_work_location_id === $timestamp->work_location_id && $working_info->planned_work_address_id === $timestamp->work_address_id;
        });

        if (!$same_place_working_infos->isEmpty()) {

            // Filter out all working infos that have both timestamp attributes filled
            $have_empty_timestamped_atributes_working_infos = $same_place_working_infos->filter(function($working_info) {
                return $working_info->timestamped_start_work_time === null || $working_info->timestamped_end_work_time === null;
            });

            if ($have_empty_timestamped_atributes_working_infos->isNotEmpty()) {

                // Then we have to separate the working infos: those have 'planned_start/end_work_time' attributes and those dont.
                $have_planned_time_attributes_working_infos = $have_empty_timestamped_atributes_working_infos->filter(function($working_info){
                    return $working_info->planned_start_work_time !== null && $working_info->planned_end_work_time !== null;
                });
                $dont_have_planned_time_attributes_working_infos = $same_place_working_infos->diff($have_planned_time_attributes_working_infos);

                // Initial
                $current_working_info = null;

                // Search in those have 'planned_start/end_work_time' for the suitable one
                $the_smallest_diff = null;
                $carbon_timestamp = $this->getCarbonInstance($timestamp->raw_date_time_value);
                foreach ($have_planned_time_attributes_working_infos as $working_info) {

                    $carbon_instance = $this->getCarbonInstance($working_info->planned_start_work_time);
                    $current_diff = $carbon_instance->diffInMinutes($carbon_timestamp);
                    if (($the_smallest_diff === null) || (($the_smallest_diff !== null) && ($current_diff < $the_smallest_diff))) {
                        $current_working_info = $working_info;
                        $the_smallest_diff = $current_diff;
                    }
                }

                if ($current_working_info !== null) {
                    return $current_working_info;
                }

                // At this point, if the function hasn't returned yet, that means we need to find among the not-have-planned_start/end_work_time EWIs
                // If there is no suitable one in those, we'll just find the first one that have empty timstamped_start_work_time in these
                $count = $dont_have_planned_time_attributes_working_infos->count();
                $current_working_info = $dont_have_planned_time_attributes_working_infos->first(function($working_info) use ($count) {
                    if ($count > 1) {
                        return $working_info->takeAllWorkingHourAsPaidRestTime() != true && $working_info->timestamped_start_work_time === null;
                    } else {
                        return $working_info->timestamped_start_work_time === null;
                    }
                });

                // And if we cant find the suitable EWI even among the not-have-planned_start/end_work_time EIWs, return -3
                $current_working_info = $current_working_info !== null ? $current_working_info : -3;
                return $current_working_info;

            // All the working infos of this WorkLocation has already have full timestamp
            } else {
                return -2;
            }

        // Dont have any matched WorkLocation working info
        } else {
            return -1;
        }
    }

    /**
     * Find the suitable EmployeeWorkingInformation for this end_work_time timestamp
     *
     * @param Collection            $working_infos
     * @param WorkingTimestamp      $timestamp
     * @return -3|EmployeeWorkingInformation
     */
    protected function getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisEndWorkTimestamp($working_infos, $timestamp)
    {

        // 2019-05-06 Update: If the timestamp has work address, use only the working info with the exact work location and work address. In other words:
        // If the timestamp has work address, ignore the rule: "end work timestamp will ignore places information, when determine which working infor is suitable"
        if ($timestamp->work_address_id) {
            $working_infos = $working_infos->filter(function($working_info) use ($timestamp) {
                return $working_info->planned_work_location_id === $timestamp->work_location_id && $working_info->planned_work_address_id === $timestamp->work_address_id;
            });

        // If the timestamp doesn't have work address, ignore all working info that have work address
        } else {
            $working_infos = $working_infos->filter(function($working_info) {
                return $working_info->planned_work_address_id == null;
            });
        }

        // separate the working_infos for those have 'planned_' attributes and those dont
        $have_planned_time_attributes_working_infos = $working_infos->filter(function($working_info){
            return $working_info->planned_start_work_time !== null && $working_info->planned_end_work_time !== null;
        });
        $dont_have_planned_time_attributes_working_infos = $working_infos->diff($have_planned_time_attributes_working_infos);

        // Initial
        $current_working_info = null;

        // Search in those have 'planned_start/end_work_time' for the suitable one
        $the_smallest_diff = null;
        $carbon_timestamp = $this->getCarbonInstance($timestamp->raw_date_time_value);
        foreach ($have_planned_time_attributes_working_infos as $working_info) {

            // Ignore all the working infos that already have both 'timestamped_start/end_work_time' data
            // if ($working_info->timestamped_start_work_time === null || $working_info->timestamped_end_work_time === null) {
                $carbon_instance = $this->getCarbonInstance($working_info->planned_end_work_time);
                $current_diff = $carbon_instance->diffInMinutes($carbon_timestamp);
                if (($the_smallest_diff === null) || (($the_smallest_diff !== null) && ($current_diff < $the_smallest_diff))) {
                    $current_working_info = $working_info;
                    $the_smallest_diff = $current_diff;
                }
            // }
        }

        if ($current_working_info !== null) {
            return $current_working_info;
        }

        // At this point, if the function hasn't returned yet, that means we need to find among the not-have-planned_start/end_work_time EWIs
        // If there is no suitable one in those, we'll just find the first one that have empty timestamped_end_work_time in these
        $count = $dont_have_planned_time_attributes_working_infos->count();
        $current_working_info = $dont_have_planned_time_attributes_working_infos->first(function($working_info) use ($count) {
            if ($count > 1) {
                return $working_info->takeAllWorkingHourAsPaidRestTime() != true && $working_info->timestamped_end_work_time === null;
            } else {
                return $working_info->timestamped_end_work_time === null;
            }
        });

        // And if we cant find the suitable EWI even among the not-have-planned_start/end_work_time EIWs, return -3
        $current_working_info = $current_working_info !== null ? $current_working_info : -3;

        return $current_working_info;

        // If all the working_infos have already full timestamps, then this the case of 'end work timestamp can not be inserted',
        // which also yields an TimestampEndWorkError
        // return -3;
    }

    /**
     * Find the suitable EmployeeWorkingInformation for this go_out_start / return timestamp.
     *
     * @param Collection            $working_infos
     * @param WorkingTimestamp      $timestamp
     */
    protected function getTheAvailableWorkingInfoThatHaveTheSuitablePlanForThisGoOutOrReturnTimestamp($working_infos, $timestamp)
    {
        $current_working_info = null;

        foreach ($working_infos as $working_info) {

            // if ($working_info->isOnlyWorkingHourAndBreakTimeMode() === true) {

            //     if ($working_info->real_go_out_time === null) {
            //         $current_working_info = $working_info;
            //         break;
            //     }
            // } else {

            if ($this->isThisWorkingInfoSuitableForThisGoOutOrReturnTimestamp($working_info, $timestamp->raw_date_time_value) == true) {
                $current_working_info = $working_info;
                break;
            }
            // }
        }
        return $current_working_info;
    }

    /**
     * Check if a go_out timestamp is within the plan of a given EmployeeWorkingInformation.
     *
     * @param EmployeeWorkingInformation            $working_info
     * @param WorkingTimestamp                      $timestamp
     */
    protected function isThisWorkingInfoSuitableForThisGoOutOrReturnTimestamp($working_info, $timestamp)
    {
        if ($working_info->planned_start_work_time !== null && $working_info->planned_end_work_time !== null) {

            $carbon_planned_start = $this->getCarbonInstance($working_info->planned_start_work_time);
            $carbon_planned_end = $this->getCarbonInstance($working_info->planned_end_work_time);
            $carbon_go_out_timestamp = $this->getCarbonInstance($timestamp);

            return ($carbon_planned_start->lte($carbon_go_out_timestamp) && $carbon_go_out_timestamp->lte($carbon_planned_end));

        } else if ($working_info->timestamped_start_work_time !== null && $working_info->timestamped_end_work_time !== null) {

            $carbon_timestamped_start = $this->getCarbonInstance($working_info->timestamped_start_work_time);
            $carbon_timestamped_end = $this->getCarbonInstance($working_info->timestamped_end_work_time);
            $carbon_go_out_timestamp = $this->getCarbonInstance($timestamp);

            return $carbon_timestamped_start->lte($carbon_go_out_timestamp) && $carbon_go_out_timestamp->lte($carbon_timestamped_end);

        } else if ($working_info->timestamped_start_work_time !== null && $working_info->estimatedEndWorkTime() !== null) {

            $carbon_timestamped_start = $this->getCarbonInstance($working_info->timestamped_start_work_time);
            $carbon_estimated_end = $working_info->estimatedEndWorkTime();
            $carbon_go_out_timestamp = $this->getCarbonInstance($timestamp);

            return $carbon_timestamped_start->lte($carbon_go_out_timestamp) && $carbon_go_out_timestamp->lte($carbon_estimated_end);
        }

        return false;
    }

    /**
     * Just a shortcut function: assign the places related information from a WorkingTimestamp instance to a EmployeeWorkingInformation instance's plan.
     *
     * @param EmployeeWorkingInformation            $working_info
     * @param WorkingTimestamp                      $timestamp
     */
    protected function assignPlaceInfo($working_info, $timestamp, $override_planned = false)
    {
        if (!$working_info->real_work_location_id) {
            $working_info->real_work_location_id = $timestamp->work_location_id;
        }
        if (!$working_info->real_work_address_id) {
            $working_info->real_work_address_id = $timestamp->work_address_id;
        }

        if ($override_planned == true) {
            $working_info->planned_work_location_id = $timestamp->work_location_id;
            $working_info->planned_work_address_id = $timestamp->work_address_id;
        }

        // if (Auth::user()) {
        //     $working_info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
        //     $working_info->last_modify_person_id = Auth::user()->id;
        // }

        return $working_info;
    }

    /**
     * Delete all the ColorStatus belong to this day and have type 'error'
     *
     * @param EmployeeWorkingDay        $working_day
     * @return void
     */
    protected function resetAllErrorColorStatusOfThisDay(EmployeeWorkingDay $working_day)
    {
        ColorStatus::where('colorable_type', EmployeeWorkingInformation::class)->where('field_css_class', ColorStatus::ERROR_COLOR)
                    ->whereIn('colorable_id', $working_day->employeeWorkingInformations()->pluck('id')->toArray())->delete();
    }


    /**
     * Delete all the temporary EmployeeWorkingInformation belong to this EmployeeWorkingDay
     *
     * @param EmployeeWorkingDay        $working_day
     */
    protected function resetTemporaryEmployeeWorkingInformation(EmployeeWorkingDay $working_day)
    {
        $temp_working_infos = $working_day->employeeWorkingInformations()->where('temporary', true)->get();

        foreach($temp_working_infos as $working_info) {
            $working_info->delete();
        }
    }

    /////////////////////////////////////////// End of Checklist Errors Evaluation of an EmployeeWorkingDay Instance /////////////////////////////
}
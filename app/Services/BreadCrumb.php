<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class BreadCrumb
{

    /**
     * Constants for pages's names to use with breadcrumb object
     */
    const ROOT_ATTENDANCE = "勤怠管理";

    const ROOT_MAEBARAI = "前払管理";

    const EMPLOYEE_ATTENDANCE = "勤怠データ管理";

    const ADVANCE_SEARCH = "勤怠データ検索";

    const WORKING_MONTH = "個人別詳細";

    const WORKING_DAY = "勤怠データ詳細";

    const CHECKLIST = "チェックリスト";

    const PAID_HOLIDAY = "有給休暇管理";

    const PAID_HOLIDAY_DETAIL = "有給休暇詳細";

    const MONTH_SUMMARY = "勤怠集計";

    const MAEBARAI_SETTING = "前払設定";

    const MAEBARAI_INFORMATION = "前払情報";

    const WORK_ADDRESS_ATTENDANCE = "訪問先別勤務情報";

    const WORK_ADDRESS_WORKING_MONTH = "訪問先別詳細";

    const WORK_ADDRESS_WORKING_DAY = "訪問勤務詳細";

    const EMPLOYEE_PRINT = "個人別詳細印刷";

    const MAEBARAI_DETAIL = "前払情報詳細";
    /**
     * Name of the BreadCrumb. This is displayed in the template of breadcrumbs
     */
    private $name;

    /**
     * The heigh of this node in the breadcrumbs tree
     */
    private $level;

    /**
     * Name of the route that this breadcrumb represents
     */
    private $route_name;

    /**
     * Parameters list of that route. These parameters will be used to redirect to that route
     */
    private $parameters;

    /**
     * For example: in advance search page, the second dimension will be the id of the search result.
     * To be more specific, when the user click the link on advance search page to go the employee's working month page. In that page, there are the green square arrow buttons.
     * These arrow buttons's purpose are: Traveling around in the search result.
     * The search result will be the second dimension of advance search page.
     */
    private $second_dimension;

    /**
     * The name of the parameter, that change when traveling through the second dimension.
     *
     * From below example, the search result is the second dimension of advance search page. And the employee's working month page is the platform to travel through 2nd dimension.
     * The employee's working month page has several parameters: employee, business-month.
     * When traveling through the second dimension, one of these parameter will change while the others do not. In employee's working month page's case, that parameter is employee.
     * So we say, the pivot of the second dimension of advance search page is 'employee'.
     */
    private $pivot_of_second_dimension;

    /**
     * There might be several global parameters that is unneccessary for traveling through 2nd dimension, add those paramters here to ignore them.
     */
    private $parameter_exceptions = [
        'company_code',
        // 'reset_search_condition',
    ];

    /**
     * Create a BreadCrumb instance.
     *
     * @param   string  $name
     * @param   integer  $level
     * @return  void
     */
    public function __construct($name, $level)
    {
        $this->name = $name;
        $this->level = $level;
        $this->parameters = collect();
    }

    /**
     * Get name of the BreadCrumb
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the height of the node in the breadcrumbs tree
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Get route of this breadcrumb.
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route_name;
    }

    /**
     * Set route for this breadcrumb.
     *
     * @param string    $route_name
     */
    public function setRoute($route_name)
    {
        $this->route_name = $route_name;
    }

    /**
     * Get paramters of this breadcrumb's route
     *
     * @return array
     */
    public function getParameters()
    {
        return empty($this->parameters) ? null : $this->parameters;
    }

    /**
     * Set the parametes of this breadcrumb's route
     * This function will ignore the exceptions and, in the case of: the parameter is an Eloquent model, only save the id.
     *
     * @param array     $parameter_names
     * @param array     $parameters
     */
    public function setParameters($parameter_names, $parameters)
    {        
        $save_parameters = [];

        foreach($parameter_names as $name) {
            if (!in_array($name, $this->parameter_exceptions)) {
                if (isset($parameters[$name])) {

                    $save_parameters[$name] = is_a($parameters[$name], Model::class) ? $parameters[$name]->id : $parameters[$name];
                }
            }
        }

        $this->parameters = $save_parameters;
    }

    /**
     * Overwrite a specific parameter.
     *
     * @param string    $name   name of the parameter
     * @param string    $value  new value for that parameter
     */
    public function overwriteParameter($name, $value)
    {
        if (!in_array($name, $this->parameter_exceptions)) {
            $this->parameters[$name] = is_a($value, Model::class) ? $value->id : $value;
        }
    }

    /**
     * Assign the second dimenstion
     *
     * @param Collection    $things     it should be a collection of something's id.
     */
    public function setSecondDimension(Collection $things)
    {
        $this->second_dimension = $things;
    }

    /**
     * Check if this breadcrumb has second dimentions or not
     *
     * @return boolean
     */
    public function hasSecondDimension()
    {
        return isset($this->second_dimension);
    }

    /**
     * Set the pivot, aka: the name of the important parameter.
     *
     * @param string    $parameter_name
     */
    public function setPivot($parameter_name)
    {
        $this->pivot_of_second_dimension = $parameter_name;
    }

    /**
     * Get the name of the important parameter
     *
     * @return string
     */
    public function getPivot()
    {
        return $this->pivot_of_second_dimension;
    }

    /**
     * Give a pivot value, in other words, a value of that parameter to get the index of the current element in the 2nd dimension array.
     *
     * @param mix   $pivot_value
     * @return int
     */
    public function getPositionInSecondDimension($pivot_value)
    {
        if (!empty($this->second_dimension)) {

            return $this->second_dimension->search($pivot_value);

        } else {
            return null;
        }
    }

    /**
     * Get the next position of a given position in the 2nd dimension
     *
     * @param int   $position
     * @return mix|null
     */
    public function getTheNextPositionInSecondDimension($position)
    {
        if (!empty($this->second_dimension)) {

            return $this->getTraversalInformationOfElementInSecondDimentionWithAGivenPosition($position + 1);
        }
        return null;
    }

    /**
     * The same as above, only the opposite direction
     *
     * @param int   $position
     * @return mix|null
     */
    public function getThePreviousPositionInSecondDimension($position)
    {
        if (!empty($this->second_dimension)) {

            return $this->getTraversalInformationOfElementInSecondDimentionWithAGivenPosition($position - 1);
        }
        return null;
    }

    /**
     * Get the pivot value of a given position in the 2nd dimension
     *
     * @param int   $position
     * @return mix|null
     */
    public function getTraversalInformationOfElementInSecondDimentionWithAGivenPosition($position)
    {
        if (isset($this->second_dimension[$position])) {
            $detination_pivot_value = $this->second_dimension[$position];

            return $detination_pivot_value;
        }
        return null;
    }

    /**
     * Check if can go to the next element in the second dimension or not, given the pivot value of the current element
     *
     * @param mix   $pivot_value
     * @return boolean
     */
    public function canGoNextInSecondDimension($pivot_value)
    {
        if (!empty($this->second_dimension)) {
            $current_position = $this->second_dimension->search($pivot_value);

            return $current_position === false ? false : isset($this->second_dimension[$current_position + 1]);
        }
        return false;
    }

    /**
     * Check if can go to the previous element in the second dimension or not, given the pivot value of the current element
     *
     * @param mix   $pivot_value
     * @return boolean
     */
    public function canGoPreviousInSecondDimension($pivot_value)
    {
        if (!empty($this->second_dimension)) {
            $current_position = $this->second_dimension->search($pivot_value);

            return $current_position === false ? false : isset($this->second_dimension[$current_position - 1]);
        }
        return false;
    }

    /**
     * Check if this node can be travel to or not
     *
     * @return boolean
     */
    public function isTraversable()
    {
        return isset($this->route_name);
    }

}
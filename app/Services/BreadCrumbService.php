<?php

namespace App\Services;

use Illuminate\Support\Collection;

class BreadCrumbService
{
    private $tree;

    // private $max_height;

    public function __construct()
    {
        if (session('breadcrumbs') !== null) {
            $this->tree = session('breadcrumbs');

        } else {
            $this->tree = collect();
        }
    }

    /**
     * Reset the breadcrumbs tree to a tree with a single root node. And that node is '勤怠管理'
     */
    public function resetToAttendanceRoot()
    {
        $this->tree = collect();

        $root = new BreadCrumb(BreadCrumb::ROOT_ATTENDANCE, 0);
        $this->tree->push($root);
    }

    /**
     * Reset the breadcrumbs tree to a tree with a single root node. And that node is '前払管理'
     */
    public function resetToMaebaraiRoot()
    {
        $this->tree = collect();

        $root = new BreadCrumb(BreadCrumb::ROOT_MAEBARAI, 0);
        $this->tree->push($root);
    }

    /**
     * Add a node to the current tree
     *
     * @param BreadCrumb    $breadcrumb
     */
    public function addBreadCrumb(BreadCrumb $breadcrumb)
    {
        $this->tree->push($breadcrumb);
    }

    /**
     * Get the breadcrumb tree
     */
    public function getBreadCrumbs()
    {
        return $this->tree;
    }

    /**
     * Go one level deeper(bad wording), in other words, go one level higher in the tree.
     *
     * @param string                        $breadcrumb_name
     * @param string                        $route_name
     * @param \Illuminate\Routing\Route     $route
     */
    public function goOneLevelDeeper($breadcrumb_name, $route_name, $route)
    {
        $new_breadcrumb = new BreadCrumb($breadcrumb_name, $this->tree->count());

        $new_breadcrumb->setRoute($route_name);
        $new_breadcrumb->setParameters($route->parameterNames(), $route->parameters());

        $this->addBreadCrumb($new_breadcrumb);

        session([ 'breadcrumbs' => $this->tree ]);
    }

    /**
     * Discard the last node and go back one level on the tree.
     *
     * @return BreadCrumb
     */
    public function goBackOneLevel()
    {
        $this->tree->pop();

        $last_node = $this->tree->last();

        return $last_node->isTraversable() ? $last_node : null;
    }

    /**
     * Check if the current page has been already in the current tree or not. If yes just update the parameters, otherwise add another breadcrumb to the tree
     *
     * @param string                        $breadcrumb_name
     * @param string                        $route_name
     * @param \Illuminate\Routing\Route     $route
     */
    public function goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist($breadcrumb_name,  $route_name, $route)
    {
        $search_node = $this->tree->first(function($node) use ($breadcrumb_name) {
            return $node->getName() === $breadcrumb_name;
        });

        if (isset($search_node)) {
            $this->goToLevel($search_node->getLevel(), $route);
        } else {
            $this->goOneLevelDeeper($breadcrumb_name, $route_name, $route);
        }
    }

    /**
     * Check if the parent node of the current node is a specific breadcrumb.
     *
     * @param string        $breadcrumb_name
     * @return boolean
     */
    public function isParentOfCurrentNode($breadcrumb_name)
    {
        $current_node = $this->tree->last();

        $parent_node = $this->tree[$current_node->getLevel() - 1];

        if ($parent_node) {
            return $parent_node->getName() === $breadcrumb_name;
        } else {
            return false;
        }
    }
    
    /**
     * Go to a specific node of the tree, given a level(height).
     *
     * @param int                           $level (height)
     * @param \Illuminate\Routing\Route     $route
     * @return BreadCrumb|null
     */
    public function goToLevel($level, $route = null)
    {
        $this->tree->splice($level + 1);
        
        $last_node = $this->tree->last();

        if ($route !== null) {
            $last_node->setParameters($route->parameterNames(), $route->parameters());
        }
        
        return $last_node->isTraversable() ? $last_node : null;
    }

    /**
     * Set the second dimension to a specific BreadCrumb node.
     *
     * @param Collection    $dimension
     * @param int           $level      level(height) of the node that you want to assign the 2nd dimension to
     * @param string        $pivot      the name of the important parameter, that a child BreadCrumb's page will use to traverse through this 2nd dimension
     */
    public function setSecondDimensionAtLevel(Collection $dimension, $level, $pivot)
    {
        $desired_node = $this->tree[$level];

        $desired_node->setSecondDimension($dimension);

        $desired_node->setPivot($pivot);
    }

    /**
     * Set the second dimension for the current node, aka the last node of the tree.
     *
     * @param Collection    $dimension
     * @param string        $pivot      the name of the important parameter, that a child BreadCrumb's page will use to traverse through this 2nd dimension
     */
    public function setTheSecondDimensionForCurrentNode($dimension, $name_of_parameter_as_pivot)
    {
        $current_node = $this->tree->last();

        if ($current_node !== null) {
            $current_node->setSecondDimension($dimension);

            $current_node->setPivot($name_of_parameter_as_pivot);
        }
    }

    /**
     * Go to next position in the second dimension of the parent node of the current node
     *
     * @param mix           $pivot_value    the value of the important parameter of the current element
     * @return BreadCrumb|null
     */
    public function gotoNextElementInSecondDimensionOfParentNode($pivot_value)
    {
        if ($this->tree->count() > 1) {

            $last_node = $this->tree->last();

            $parent_node_of_the_last_node = $this->tree[$last_node->getLevel() - 1];
            
            $current_position = $parent_node_of_the_last_node->getPositionInSecondDimension($pivot_value);

            $destination_pivot_value = $parent_node_of_the_last_node->getTheNextPositionInSecondDimension($current_position);

            $destination_node = clone($last_node);
            $destination_node->overwriteParameter($parent_node_of_the_last_node->getPivot(), isset($destination_pivot_value) ? $destination_pivot_value : $pivot_value);

            return $destination_node;
        }

        return null;
    }

    /**
     * The same as above, only in the opposite direction.
     *
     * @param mix           $pivot_value    the value of the important parameter of the current element
     * @return BreadCrumb|null
     */
    public function gotoPreviousElementInSecondDimensionOfParentNode($pivot_value)
    {
        if ($this->tree->count() > 1) {

            $last_node = $this->tree->last();

            $parent_node_of_the_last_node = $this->tree[$last_node->getLevel() - 1];
            
            $current_position = $parent_node_of_the_last_node->getPositionInSecondDimension($pivot_value);

            $destination_pivot_value = $parent_node_of_the_last_node->getThePreviousPositionInSecondDimension($current_position);

            $destination_node = clone($last_node);
            $destination_node->overwriteParameter($parent_node_of_the_last_node->getPivot(), isset($destination_pivot_value) ? $destination_pivot_value : $pivot_value);

            return $destination_node;
        }

        return null;
    }

    /**
     * Check if can go to the next element in the second dimenstion of the parent node of the current node
     *
     * @param mix   $pivot_value
     * @return boolean
     */
    public static function canGoNext($pivot_value)
    {
        $tree = session('breadcrumbs');

        if ($tree !== null && $tree->count() > 1) {

            $last_node = $tree->last();

            $parent_node_of_the_last_node = $tree[$last_node->getLevel() - 1];

            return $parent_node_of_the_last_node->canGoNextInSecondDimension($pivot_value);
        }

        return false;
    }

    /**
     * Check if can go to the previous element in the second dimenstion of the parent node of the current node
     *
     * @param mix   $pivot_value
     * @return boolean
     */
    public static function canGoPrevious($pivot_value)
    {
        $tree = session('breadcrumbs');

        if ($tree !== null && $tree->count() > 1) {

            $last_node = $tree->last();

            $parent_node_of_the_last_node = $tree[$last_node->getLevel() - 1];

            return $parent_node_of_the_last_node->canGoPreviousInSecondDimension($pivot_value);
        }

        return false;
    }

    /**
     * Change the parameters of the current node's route.
     * We need this function for kintai kanri page.
     *
     * @param array|collection  $parameters
     * @return void
     */
    public function changeParametersOfTheCurrentNode($parameters)
    {
        $current_node = $this->tree->last();

        if ($current_node !== null) {
            foreach ($parameters as $name => $value) {
                $current_node->overwriteParameter($name, $value);
            }
        }
    }

    /**
     * Change the parameters of the parent of the current node's route.
     *
     * @param array|collection      $parameters
     * @return void
     */
    public function changeParametersOfTheParentOfTheCurrentNode($parameters)
    {
        $current_node = $this->tree->last();

        $parent_node = $this->tree[$current_node->getLevel() - 1];

        if ($parent_node !== null) {
            foreach ($parameters as $name => $value) {
                $parent_node->overwriteParameter($name, $value);
            }
        }
    }

}
<?php

namespace App\Services;

use App\Employee;
use App\PaidHolidayInformation;
use App\ChangeWorkTimePerDayTimer;
use App\ChangeEmployeeLocationTimer;
use Carbon\Carbon;

class ChangeWorkTimePerDayService
{

    /**
     * When an Employee's work_time_per_day attribute is officially changed. If that Employee currently has a PaidHolidayInformation, and the changed day is
     * between the period of that PaidHolidayInformation, that info will be split into two. Some of those two's attributes will be the same while some others will be
     * calculated independently.
     * This function's purpose is to split the former PaidHolidayInformation into two new ones, and make the chain link relationship between them.
     *
     * @param Employee      $employee
     * @param float         $new_work_time_per_day  (this will be in hour unit, it will be converted to minute later)
     * @return void
     */
    public function changeWorkTimePerDayOfThisEmployee(Employee $employee, $new_work_time_per_day)
    {
        $paid_holiday_infos = $employee->paidHolidayInformations()->orderBy('period_start', 'des')->get();

        if ($paid_holiday_infos) {
            $current_paid_holiday_info = $paid_holiday_infos->first();

            if ($current_paid_holiday_info) {
                $current_start_day = new Carbon($current_paid_holiday_info->period_start);
                $current_end_day = new Carbon($current_paid_holiday_info->period_end);
                $today = Carbon::today();

                if ($current_start_day->lte($today) && $today->lte($current_end_day)) {

                    $new_paid_holiday_info = new PaidHolidayInformation([
                        'employee_id' => $employee->id,
                        'period_start' => $today->toDateString(),
                        'period_end' => $current_paid_holiday_info->period_end,
                        'work_time_per_day' => $new_work_time_per_day * 60,
                        'attendance_rate' => $current_paid_holiday_info->attendance_rate,
                        'provided_paid_holidays' => $current_paid_holiday_info->provided_paid_holidays,
                        'linked_paid_holiday_information_id' => $current_paid_holiday_info->id,
                    ]);

                    $current_paid_holiday_info->period_end = $today->copy()->subDay()->toDateString();

                    $current_paid_holiday_info->save();
                    $new_paid_holiday_info->save();
                }
            }
        }
    }

    /**
     * Create a ChangeWorkTimePerDayTimer instance and save it to the main database.
     *
     * @param Employee      $employee
     * @param float         $new_work_time_per_day
     * @param string        $activate_day
     * @return void
     */
    public function addChangeWorkTimePerDayTimer(Employee $employee, $new_work_time_per_day, $activate_day)
    {
        $timer = new ChangeWorkTimePerDayTimer([
            'company_code' => session('current_company_code'),
            'employee_id' => $employee->id,
            'work_time_per_day' => $new_work_time_per_day,
            'due_day' => $activate_day,
        ]);

        $timer->save();
    }

    /**
     * Create a ChangeEmployeeLocationTimer instance and save it to the main database.
     *
     * @param Employee      $employee
     * @return void
     */
    public function addLocationChangeTimer(Employee $employee)
    {
        $timer = ChangeEmployeeLocationTimer::where('employee_id', $employee->id)->where('company_code', session('current_company_code'))->first();
        if ($timer) {
            $timer->change_date = $employee->change_date;
        } else {
            $timer = new ChangeEmployeeLocationTimer([
                'company_code' => session('current_company_code'),
                'employee_id' => $employee->id,
                'change_date' => $employee->change_date,
            ]);
        }

        $timer->save();
    }

    /**
     * Remove a ChangeEmployeeLocationTimer instance from the main database.
     *
     * @param Employee      $employee
     * @return void
     */
    public function removeLocationChangeTimer(Employee $employee)
    {
        $timer = ChangeEmployeeLocationTimer::where('employee_id', $employee->id)->where('company_code', session('current_company_code'))->first();
        if ($timer) {
            $timer->change_date = $employee->change_date;
            $timer->delete();
        }
    }
}
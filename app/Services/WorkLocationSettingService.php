<?php

namespace App\Services;

use App\Company;
use App\WorkLocation;
use App\Services\TimezoneService;

class WorkLocationSettingService
{
    /**
     * List of all WorkLocations of this company and the company itself.
     */
    private $work_locations = [];
    private $lookup_array_by_id = [];
    private $company;

    /**
     * Initiate the WorkLocation and Setting list
     */
    public function __construct()
    {
        $this->work_locations = WorkLocation::with([
            'setting',
            'unusedWorkStatuses',
            'unusedRestStatuses',
            'company.setting',
            'company.workStatuses',
            'company.restStatuses',
        ])->get();

        $this->lookup_array_by_id = $this->work_locations->keyBy('id');
        $this->company = Company::with([
            'setting',
            'workStatuses',
            'restStatuses'
        ])->get()->first();
    }

    /**
     * Get the current setting of a WorkLocation using the given $id to search.
     *
     * @param integer   $id
     * @return null|\App\Setting
     */
    public function getCurrentSettingOfWorkLocationById($id)
    {
        return $this->lookup_array_by_id[$id] ? $this->lookup_array_by_id[$id]->currentSetting() : null;
    }

    /**
     * Get a WorkLocation using the given $id to search.
     *
     * @param integer   $id
     * @return null|\App\WorkLocation
     */
    public function getWorkLocationById($id)
    {
        return $this->lookup_array_by_id[$id];
    }

    /**
     * Get all WorkLocations of this database(a sub database), and because right now a sub database belongs to a company.
     * This will result in "all WorkLocations of the current company".
     *
     * @return collection
     */
    public function getAllWorkLocations()
    {
        return $this->work_locations;
    }

    /**
     * Get the company and its settings.
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
<?php

namespace App\Services;

use App\Manager;
use App\Employee;

class ManagerAndEmployeeService
{
    /**
     * Managers list
     */
    private $managers;

    /**
     * Employees list
     */
    private $employees;

    /**
     * Employees list but key by presentation id
     */
    private $employees_by_presentation_id;

    /**
     * Initiate the WorkLocation list
     */
    public function __construct()
    {
        $this->managers = Manager::get()->keyBy('id');
        $employees = Employee::all();
        $this->employees = $employees->keyBy('id');
        $this->employees_by_presentation_id = $employees->keyBy('presentation_id');
    }

    /**
     * Get a specific Manager.
     *
     * @param int   $id
     * @return Manager|null
     */
    public function getManager($id)
    {
        if ($this->managers->has($id)) {
            return $this->managers[$id];
        } else {
            return null;
        }
    }

    /**
     * Get a specific Employee.
     *
     * @param int   $id
     * @return Employee|null
     */
    public function getEmployee($id)
    {
        if ($this->employees->has($id)) {
            return $this->employees[$id];
        } else {
            return null;
        }
    }

    /**
     * Get a specific Employee with the given presentation_id.
     *
     * @param   string      $presentation_id
     * @return  Employee|null
     */
    public function getEmployeeByPresentationId($presentation_id)
    {
        if ($this->employees_by_presentation_id->has($presentation_id)) {
            return $this->employees_by_presentation_id[$presentation_id];
        } else {
            return null;
        }
    }
}
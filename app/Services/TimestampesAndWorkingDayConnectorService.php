<?php

namespace App\Services;

use App\Employee;
use App\Company;
use App\EmployeeWorkingDay;
use App\WorkingTimestamp;
use App\Services\EvaluateChecklistErrorsService;
use App\Events\WorkingTimestampChanged;
use App\Events\CachedPaidHolidayInformationsBecomeOld;
use Carbon\Carbon;
use DB;
use App\Events\WorkingTimestampChangedOnManyWorkingDays;
use App\Events\DistributeTimestampsAndReevaluateChecklistErrors;

/**
 *  This service's purpose is to distribute the WorkingTimestamp into EmployeeWorkingDays (This service is different from EvaluateChecklistErrorsService
 *  in which, there is a function whose purpose is to distribute the WorkingTimestamps into EmployeeWorkingInformations ).
 */
class TimestampesAndWorkingDayConnectorService
{

    /**
     * Add several new WorkingTimestamps, then along with several old WorkingTimestamps connect
     * to a single or several EmployeeWorkingDay(s) of an Employee.
     *
     * @param array|Collection      $timestamps         the list of WorkingTimestamps
     * @param Employee              $employee           the employee of the EmployeeWorkingDays
     *
     * @return void
     */
    public function connectTimestampsToWorkingDaysOfEmployee($timestamps, Employee $employee)
    {
        if (is_array($timestamps)) {
            $timestamps = collect($timestamps);
        }
        $sorted_timestamps = $timestamps->sortBy('timestamped_value');
        $minimum_day = Carbon::createFromFormat('Y-m-d H:i:s', $sorted_timestamps->first()->raw_date_time_value)->subDay()->toDateString();
        $maximum_day = Carbon::createFromFormat('Y-m-d H:i:s', $sorted_timestamps->last()->raw_date_time_value)->addDay()->toDateString();

        $this->ensureExistenceOfEmployeeWorkingDay($employee, $minimum_day, $maximum_day);

        $working_days = EmployeeWorkingDay::where('date', '>=', $minimum_day)->where('date', '<=', $maximum_day)->where('employee_id', $employee->id)->get();
        $old_timestamps = WorkingTimestamp::whereHas('employeeWorkingDay', function($query) use ($minimum_day, $maximum_day, $employee) {
            return $query->where('date', '>=', $minimum_day)->where('date', '<=', $maximum_day)->where('employee_id', $employee->id)->notConcluded();
        })->get();

        $all_timestamps = $old_timestamps->concat($sorted_timestamps);

        $this->arrangeAndConnectWorkingTimestamps($all_timestamps, $working_days);
    }

    /**
     * Re-arrange and re-connect the WorkingTimestamps into the given EmployeeWorkingDays
     *
     * @param array     $working_days
     * @return void
     */
    public function rearrangeAndConnectTheTimestampsToWorkingDays($working_days)
    {
        $sorted_working_days = collect($working_days)->sortBy('date');
        $minimum_day = Carbon::createFromFormat('Y-m-d', $sorted_working_days->first()->date)->subDay()->toDateString();
        $maximum_day = Carbon::createFromFormat('Y-m-d', $sorted_working_days->last()->date)->addDay()->toDateString();

        $employee = $sorted_working_days->first()->employee;

        $this->ensureExistenceOfEmployeeWorkingDay($employee, $minimum_day, $maximum_day);

        /////////////////////////////////
        $all_working_days = EmployeeWorkingDay::where('date', '>=', $minimum_day)->where('date', '<=', $maximum_day)->where('employee_id', $employee->id)->get();
        $all_working_timestamps = WorkingTimestamp::whereHas('employeeWorkingDay', function($query) use ($minimum_day, $maximum_day, $employee) {
            return $query->where('date', '>=', $minimum_day)->where('date', '<=', $maximum_day)->where('employee_id', $employee->id)->notConcluded();
        })->get();

        $this->arrangeAndConnectWorkingTimestamps($all_working_timestamps, $all_working_days, true);
    }

    /**
     * Re-arrange the WorkingTimestamps list then connect them to EmployeeWorkingDays in the $working_days respectively.
     *
     * @param collection    $timestamps         the list of WorkingTimestamps
     * @param collection    $working_days       the list of EmployeeWorkingDays
     *
     * @return void
     */
    private function arrangeAndConnectWorkingTimestamps($all_timestamps, $working_days)
    {
        // Sort the timestamp list by time order
        $all_timestamps = $all_timestamps->sortBy(function($timestamp) {
            return $timestamp->raw_date_time_value . '#' . $timestamp->created_at;
        });

        $company = $working_days->first()->employee->workLocation->company;

        // Initialize some variables for the 'pair'
        $pair = false;
        $pair_working_day = null;
        $pair_upper_limit = null;

        foreach($all_timestamps as $timestamp) {

            $current_timestamp_carbon = new Carbon($timestamp->raw_date_time_value);

            // The disabled working timestamps will be placed with their respective days
            if ($timestamp->enable == false) {
                $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                $timestamp->employee_working_day_id = $suitable_day->id;
            } else {
                if ($timestamp->timestamped_type === WorkingTimestamp::START_WORK) {

                    $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);

                    // Assign an EmployeeWorkingDay for this timestamp
                    $timestamp->employee_working_day_id = $suitable_day->id;

                    // Check if this day is concluded or not, if not init the pair, else do not init the pair, instead disable this timestamp
                    if ($suitable_day->isConcluded()) {
                        $timestamp->enable = false;

                    } else {
                        $pair = true;
                        $pair_working_day = $suitable_day;
                        $pair_upper_limit = $this->upperLimitOfTheNextDayOfThisDay($suitable_day, $company);
                    }

                } else {
                    if ($pair) {
                        if ($timestamp->timestamped_type === WorkingTimestamp::END_WORK && $current_timestamp_carbon->lte($pair_upper_limit)) {
                            // Assign the pair day to this timestamp and close the pair if the current timestamp is a valid end work timestamp
                            $timestamp->employee_working_day_id = $pair_working_day->id;

                            $pair = false;
                            $pair_upper_limit = null;
                            $pair_working_day = null;

                        } else if ($current_timestamp_carbon->lte($pair_upper_limit)) {
                            $timestamp->employee_working_day_id = $pair_working_day->id;

                        } else {
                            // Close the pair
                            $pair = false;
                            $pair_upper_limit = null;
                            $pair_working_day = null;

                            $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                            $timestamp->employee_working_day_id = $suitable_day->id;
                        }
                    } else {
                        $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                        $timestamp->employee_working_day_id = $suitable_day->id;
                        $timestamp->enable = $suitable_day->isConcluded() ? false : $timestamp->enable;
                    }
                }
            }

            $timestamp->save();
        }

        // foreach($working_days as $working_day) {
        //     $working_day->fresh();
        //     event(new WorkingTimestampChanged($working_day));
        // }
        // **UPDATED: replace the inefficient way above by this mass call from the service
        $evaluate_service = resolve(EvaluateChecklistErrorsService::class);
        $evaluate_service->evaluateChecklistErrorFromWorkingTimestampListOfManyWorkingDay($working_days->pluck('id')->toArray());

        //Mass toggle caches of multiple paid_holiday_informations
        event(new CachedPaidHolidayInformationsBecomeOld($working_days));
    }

    /**
     * Re-arrange and re-connect the WorkingTimestamps into the given EmployeeWorkingDays
     *
     * @param array     $working_days
     * @return void
     */
    public function rearrangeAndConnectTheTimestampsToWorkingDaysWithoutImmediatelyEvaluateChecklistError($working_days)
    {
        $sorted_working_days = collect($working_days)->sortBy('date');
        $minimum_day = Carbon::createFromFormat('Y-m-d', $sorted_working_days->first()->date)->subDay()->toDateString();
        $maximum_day = Carbon::createFromFormat('Y-m-d', $sorted_working_days->last()->date)->addDay()->toDateString();

        $employee = $sorted_working_days->first()->employee;

        $this->ensureExistenceOfEmployeeWorkingDay($employee, $minimum_day, $maximum_day);

        /////////////////////////////////
        $all_working_days = EmployeeWorkingDay::where('date', '>=', $minimum_day)->where('date', '<=', $maximum_day)->where('employee_id', $employee->id)->get();
        $all_working_timestamps = WorkingTimestamp::whereHas('employeeWorkingDay', function($query) use ($minimum_day, $maximum_day, $employee) {
            return $query->where('date', '>=', $minimum_day)->where('date', '<=', $maximum_day)->where('employee_id', $employee->id)->notConcluded();
        })->get();

        $this->arrangeAndConnectWorkingTimestampsWithoutImmediatelyEvaluateChecklistError($all_working_timestamps, $all_working_days, true);
    }

    /**
     * Re-arrange the WorkingTimestamps list then connect them to EmployeeWorkingDays in the $working_days respectively.
     *
     * @param collection    $timestamps         the list of WorkingTimestamps
     * @param collection    $working_days       the list of EmployeeWorkingDays
     *
     * @return void
     */
    private function arrangeAndConnectWorkingTimestampsWithoutImmediatelyEvaluateChecklistError($all_timestamps, $working_days)
    {
        // Sort the timestamp list by time order
        $all_timestamps = $all_timestamps->sortBy(function($timestamp) {
            return $timestamp->raw_date_time_value . '#' . $timestamp->created_at;
        });

        $company = $working_days->first()->employee->workLocation->company;

        // Initialize some variables for the 'pair'
        $pair = false;
        $pair_working_day = null;
        $pair_upper_limit = null;
        $timestamps_to_be_updated = [];

        foreach($all_timestamps as $timestamp) {

            $current_timestamp_carbon = new Carbon($timestamp->raw_date_time_value);

            // The disabled working timestamps will be placed with their respective days
            if ($timestamp->enable == false) {
                $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                $timestamp->employee_working_day_id = $suitable_day->id;
            } else {
                if ($timestamp->timestamped_type === WorkingTimestamp::START_WORK) {

                    $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);

                    // Assign an EmployeeWorkingDay for this timestamp
                    $timestamp->employee_working_day_id = $suitable_day->id;

                    // Check if this day is concluded or not, if not init the pair, else do not init the pair, instead disable this timestamp
                    if ($suitable_day->isConcluded()) {
                        $timestamp->enable = false;

                    } else {
                        $pair = true;
                        $pair_working_day = $suitable_day;
                        $pair_upper_limit = $this->upperLimitOfTheNextDayOfThisDay($suitable_day, $company);
                    }

                } else {
                    if ($pair) {
                        if ($timestamp->timestamped_type === WorkingTimestamp::END_WORK && $current_timestamp_carbon->lte($pair_upper_limit)) {
                            // Assign the pair day to this timestamp and close the pair if the current timestamp is a valid end work timestamp
                            $timestamp->employee_working_day_id = $pair_working_day->id;

                            $pair = false;
                            $pair_upper_limit = null;
                            $pair_working_day = null;

                        } else if ($current_timestamp_carbon->lte($pair_upper_limit)) {
                            $timestamp->employee_working_day_id = $pair_working_day->id;

                        } else {
                            // Close the pair
                            $pair = false;
                            $pair_upper_limit = null;
                            $pair_working_day = null;

                            $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                            $timestamp->employee_working_day_id = $suitable_day->id;
                        }
                    } else {
                        $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                        $timestamp->employee_working_day_id = $suitable_day->id;
                        $timestamp->enable = $suitable_day->isConcluded() ? false : $timestamp->enable;
                    }
                }
            }

            // $timestamp->save();
            $timestamps_to_be_updated[] = "('" . implode("','", collect($timestamp->getAttributes())->only(['id', 'enable', 'timestamped_type', 'registerer_type', 'employee_working_day_id', 'updated_at'])->toArray()) . "')";
        }

        $values_string = implode(',', $timestamps_to_be_updated);
        $db_table_prefix = DB::getTablePrefix();
        DB::statement(
            'INSERT INTO ' . $db_table_prefix . 'working_timestamps (`id`, `enable`, `timestamped_type`, `registerer_type`, `employee_working_day_id`, `updated_at`) VALUES ' . $values_string . '
            ON DUPLICATE KEY UPDATE
                enable=VALUES(enable),
                timestamped_type=VALUES(timestamped_type),
                registerer_type=VALUES(registerer_type),
                employee_working_day_id=VALUES(employee_working_day_id),
                updated_at=VALUES(updated_at)'
        );

        event(new WorkingTimestampChangedOnManyWorkingDays($working_days->pluck('id')->toArray()));
        event(new DistributeTimestampsAndReevaluateChecklistErrors($working_days->pluck('id')->toArray(), session('current_company_code')));

        //Mass toggle caches of multiple paid_holiday_informations
        event(new CachedPaidHolidayInformationsBecomeOld($working_days));
    }

    /**
     * Find the suitable day for a current timestamp among the list of EmployeeWorkingDays.
     *
     * @param collection    $working_days
     * @param Company       $company
     * @param Carbon        $current_timestamp_carbon
     * @return EmployeeWorkingDay
     */
    private function findTheSuitableDay($working_days, $company, $current_timestamp_carbon)
    {
        return $working_days->first(function($working_day) use ($company, $current_timestamp_carbon) {
            return $current_timestamp_carbon->gte($this->lowerLimitOfThisDay($working_day, $company)) && $current_timestamp_carbon->lt($this->upperLimitOfThisDay($working_day, $company));
        });
    }

    /**
     * Find the upper limit of the given EmployeeWorkingDay(calculate base on the company's attributes).
     *
     * @param EmployeeWorkingDay    $working_day,
     * @param Company               $company
     * @return Carbon
     */
    private function upperLimitOfThisDay($working_day, $company)
    {
        $carbon = Carbon::createFromFormat('Y-m-d H:i', $working_day->date . ' ' . $company->date_separate_time);

        $carbon = $company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE ? $carbon->addDay() : $carbon;

        return $carbon;
    }

    /**
     * Just like the function above, except it finds the limit of the next day of the given day.
     *
     * @param EmployeeWorkingDay    $working_day
     * @param Company               $company
     * @return Carbon
     */
    private function upperLimitOfTheNextDayOfThisDay($working_day, $company)
    {
        $carbon = Carbon::createFromFormat('Y-m-d H:i', $working_day->date . ' ' . $company->date_separate_time)->addDay();

        $carbon = $company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE ? $carbon->addDay() : $carbon;

        return $carbon;
    }

    /**
     * Find the lower limit of the given EmployeeWorkingDay
     *
     * @param EmployeeWorkingDay    $working_day,
     * @param Company               $company
     * @return Carbon
     */
    private function lowerLimitOfThisDay($working_day, $company)
    {
        $carbon = Carbon::createFromFormat('Y-m-d H:i', $working_day->date . ' ' . $company->date_separate_time);

        $carbon = $company->date_separate_type === Company::APPLY_TO_THE_DAY_AFTER ? $carbon->subDay() : $carbon;

        return $carbon;
    }

    /**
     * Ensure the existence of EmployeeWorkingDay in the given range. If the day does not exist, create one on the fly.
     *
     * @param Employee  $employee
     * @param string    $minimum_day
     * @param string    $maximum_day
     * @return void
     */
    private function ensureExistenceOfEmployeeWorkingDay($employee, $minimum_day, $maximum_day)
    {
        $minimum_day = new Carbon($minimum_day);
        $maximum_day = new Carbon($maximum_day);

        $existed_working_days = EmployeeWorkingDay::where('date', '>=', $minimum_day)
                                                    ->where('date', '<=', $maximum_day)
                                                    ->where('employee_id', $employee->id)
                                                    ->get();
        $existed_working_days = $existed_working_days->keyBy(function($day) {
            return $day->date;
        });

        $working_day_instances = array();
        while($minimum_day->lte($maximum_day)) {
            $day_string = $minimum_day->format('Y-m-d');
            if (!isset($existed_working_days[$day_string])) {
                $new_working_day_instance['date'] = $day_string;
                $new_working_day_instance['employee_id'] = $employee->id;
                $working_day_instances[] = $new_working_day_instance;
            }

            $minimum_day->addDay();
        }
        EmployeeWorkingDay::insert($working_day_instances);
    }

    /**
     * Add several new AutoWorkingTimestamps, then along with several old WorkingTimestamps connect
     * to a single or several EmployeeWorkingDay(s) of an Employee.
     *
     * @param array|Collection      $timestamps         the list of WorkingTimestamps
     * @param Employee              $employee           the employee of the EmployeeWorkingDays
     *
     * @return void
     */
    public function connectAutoTimestampsToWorkingDaysOfEmployee($timestamps, Employee $employee) {
        $minimum_day = Carbon::createFromFormat('Y-m-d H:i:s', current($timestamps)['raw_date_time_value'])->subDay()->toDateString();
        $maximum_day = Carbon::createFromFormat('Y-m-d H:i:s', end($timestamps)['raw_date_time_value'])->addDay()->toDateString();

        $this->ensureExistenceOfEmployeeWorkingDay($employee, $minimum_day, $maximum_day);

        $all_working_days = EmployeeWorkingDay::where('date', '>=', $minimum_day)->where('date', '<=', $maximum_day)->where('employee_id', $employee->id)->get();
        $old_working_timestamps = WorkingTimestamp::whereHas('employeeWorkingDay', function($query) use ($minimum_day, $maximum_day, $employee) {
            return $query->where('date', '>=', $minimum_day)->where('date', '<=', $maximum_day)->where('employee_id', $employee->id)->notConcluded();
        })->orderBy('raw_date_time_value')->orderBy('timestamped_type', 'desc')->get();

        $this->arrangeAndConnectAutoWorkingTimestamps($old_working_timestamps, $all_working_days, $timestamps);
    }

    /**
     * Re-arrange the AutoWorkingTimestamps list then connect them to EmployeeWorkingDays in the $working_days respectively.
     *
     * @param collection    $working_days       the list of EmployeeWorkingDays
     *
     * @return void
     */
    private function arrangeAndConnectAutoWorkingTimestamps($old_working_timestamps, $working_days, $new_timestamps)
    {
        $company = $working_days->first()->employee->workLocation->company;

        // Initialize some variables for the 'pair'
        $pair = false;
        $pair_working_day = null;
        $pair_upper_limit = null;

        foreach($old_working_timestamps as $timestamp) {

            $current_timestamp_carbon = new Carbon($timestamp->raw_date_time_value);

            $employee_working_day_id = $timestamp->employee_working_day_id; $enable = $timestamp->enable;
            // The disabled working timestamps will be placed with their respective days
            if ($timestamp->enable == false) {
                $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                $employee_working_day_id = $suitable_day->id;
            } else {
                if ($timestamp->timestamped_type === WorkingTimestamp::START_WORK) {

                    $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);

                    // Assign an EmployeeWorkingDay for this timestamp
                    $employee_working_day_id = $suitable_day->id;

                    // Check if this day is concluded or not, if not init the pair, else do not init the pair, instead disable this timestamp
                    if ($suitable_day->isConcluded()) {
                        $timestamp->enable = false;

                    } else {
                        $pair = true;
                        $pair_working_day = $suitable_day;
                        $pair_upper_limit = $this->upperLimitOfTheNextDayOfThisDay($suitable_day, $company);
                    }

                } else {
                    if ($pair) {
                        if ($timestamp->timestamped_type === WorkingTimestamp::END_WORK && $current_timestamp_carbon->lte($pair_upper_limit)) {
                            // Assign the pair day to this timestamp and close the pair if the current timestamp is a valid end work timestamp
                            $timestamp->employee_working_day_id = $pair_working_day->id;

                            $pair = false;
                            $pair_upper_limit = null;
                            $pair_working_day = null;

                        } else if ($current_timestamp_carbon->lte($pair_upper_limit)) {
                            $employee_working_day_id = $pair_working_day->id;

                        } else {
                            // Close the pair
                            $pair = false;
                            $pair_upper_limit = null;
                            $pair_working_day = null;

                            $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                            $employee_working_day_id = $suitable_day->id;
                        }
                    } else {
                        $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                        $employee_working_day_id = $suitable_day->id;
                        $enable = $suitable_day->isConcluded() ? false : $timestamp->enable;
                    }
                }
            }

            if ($employee_working_day_id != $timestamp->employee_working_day_id || $enable != $timestamp->enable)
                $timestamp->save();
        }

        foreach($new_timestamps as $key => $timestamp) {
            if ($timestamp['employee_working_day_id'] !== null) continue;
            $current_timestamp_carbon = new Carbon($timestamp['raw_date_time_value']);

            if ($timestamp['timestamped_type'] === WorkingTimestamp::START_WORK) {

                $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);

                // Assign an EmployeeWorkingDay for this timestamp
                $new_timestamps[$key]['employee_working_day_id'] = $suitable_day->id;

                // Check if this day is concluded or not, if not init the pair, else do not init the pair, instead disable this timestamp
                if ($suitable_day->isConcluded()) {
                    $new_timestamps[$key]['enable'] = false;

                } else {
                    $pair = true;
                    $pair_working_day = $suitable_day;
                    $pair_upper_limit = $this->upperLimitOfTheNextDayOfThisDay($suitable_day, $company);
                }

            } else {
                if ($pair) {
                    if ($current_timestamp_carbon->lte($pair_upper_limit)) {
                        $new_timestamps[$key]['employee_working_day_id'] = $pair_working_day->id;

                    } else {
                        $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                        $new_timestamps[$key]['employee_working_day_id'] = $suitable_day->id;
                    }

                    // Close the pair
                    $pair = false;
                    $pair_upper_limit = null;
                    $pair_working_day = null;
                } else {
                    $suitable_day = $this->findTheSuitableDay($working_days, $company, $current_timestamp_carbon);
                    $new_timestamps[$key]['employee_working_day_id'] = $suitable_day->id;
                    $new_timestamps[$key]['enable'] = $suitable_day->isConcluded() ? false : $timestamp['enable'];
                }
            }
        }
        WorkingTimestamp::insert($new_timestamps);

        event(new WorkingTimestampChangedOnManyWorkingDays($working_days->pluck('id')->toArray()));
        event(new DistributeTimestampsAndReevaluateChecklistErrors($working_days->pluck('id')->toArray(), session('current_company_code')));

        //Mass toggle caches of multiple paid_holiday_informations
        event(new CachedPaidHolidayInformationsBecomeOld($working_days));
    }
}
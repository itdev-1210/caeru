<?php

namespace App\Services;

use App\WorkAddressWorkingDay;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;

/**
 * This service is exclusively used by the EvaluateChecklistErrorsService, for now.
 */
class WorkAddressWorkingDayLoadingService
{
    use CanCreateWorkingDayOnTheFlyTrait;

    /**
     * List of all WorkAddressWorkingDay
     */
    private $work_address_working_days = null;

    /**
     * Initialize the list
     */
    public function __construct()
    {
        $this->work_address_working_days = collect([]);
    }

    /**
     * Preload the WorkAddressWorkingDays if they haven't existed in the list yet.
     *
     * @param   string      $date
     * @param   array       $work_address_ids
     * @return  void
     */
    public function preloadWorkAddressWorkingDaysUsingDateAndWorkAddressIds($date, $work_address_ids)
    {

        $not_loaded_yet_work_address_ids = [];

        foreach ($work_address_ids as $work_address_id) {
            $exist = $this->checkWorkAddressWorkingDayByDateAndWorkAddressId($date, $work_address_id);

            if (!$exist) {
                $not_loaded_yet_work_address_ids[] = $work_address_id;
            }
        }

        if ($not_loaded_yet_work_address_ids) {
            $this->loadAndAddWorkAddressWorkingDayToTheList($date, $not_loaded_yet_work_address_ids);
        }
    }

    /**
     * Check if a specific WorkAddressWorkingDay has been loaded yet.
     *
     * @param   string      $date
     * @param   integer     $work_address_id
     * @return  boolean
     */
    public function checkWorkAddressWorkingDayByDateAndWorkAddressId($date, $work_address_id)
    {
        $working_day = $this->searchAWorkAddressWorkingDayInPreloadedList($date, $work_address_id);

        return $working_day != null;
    }

    /**
     * Retrieve a specific WorkAddressWorkingDay, if that working day hasn't been preloaded yet, load it and add to the list.
     * Definitely return a WorkAddressWorkingDay.
     *
     * @param   string      $date
     * @param   integer     $work_address_id
     * @param   WorkAddressWorkingDay
     */
    public function getWorkAddressWorkingDayByDateAndWorkAddressId($date, $work_address_id)
    {
        if (!$this->checkWorkAddressWorkingDayByDateAndWorkAddressId($date, $work_address_id)) {
            $this->loadAndAddWorkAddressWorkingDayToTheList($date, [$work_address_id]);
        }

        $working_day = $this->searchAWorkAddressWorkingDayInPreloadedList($date, $work_address_id);

        if (!$working_day) {
            $working_day = $this->createWorkAddressWorkingDayOnTheFly($work_address_id, $date);

            $this->work_address_working_days->push($working_day);
        }

        return $working_day;
    }

    /**
     * Replace a WorkAddressWorkingDay instance that has already been in the preloaded array with a
     * new WorkAddressWorkingDay.
     *
     * @param   WorkAddressWorkingDay   $new_work_address_working_day
     * @return  void
     */
    public function updateASpecificWorkAddressWorkingDay($new_work_address_working_day)
    {
        $index_in_the_list = $this->work_address_working_days->search(function ($working_day) use ($new_work_address_working_day) {
            return $working_day->id === $new_work_address_working_day->id;
        });

        $this->work_address_working_days->put($index_in_the_list, $new_work_address_working_day);
    }

    /**
     * Search a specific WorkAddressWorkingDay in the preloaded list.
     *
     * @param   string      $date
     * @param   integer     $work_address_id
     * @return  WorkAddressWorkingDay|null
     */
    protected function searchAWorkAddressWorkingDayInPreloadedList($date, $work_address_id)
    {
        return $this->work_address_working_days->first(function ($working_day) use ($date, $work_address_id) {
            return $working_day->date == $date && $working_day->work_address_id == $work_address_id;
        });
    }

    /**
     * Load some WorkAddressWorkingDays to the list of preloaded WorkingAddressWorkingDays.
     *
     * @param   string      $date
     * @param   array       $work_address_ids
     * @param   void
     */
    protected function loadAndAddWorkAddressWorkingDayToTheList($date, $work_address_ids)
    {
        $new_working_days = WorkAddressWorkingDay::with([
            'workAddressWorkingInformations.workAddressWorkingEmployees.plannedSchedule',
        ])->where('date', $date)->whereIn('work_address_id', $work_address_ids)->get();

        $this->work_address_working_days = $this->work_address_working_days->concat($new_working_days);
    }

}
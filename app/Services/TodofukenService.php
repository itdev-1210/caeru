<?php

namespace App\Services;

use App\Todofuken;

class TodofukenService
{
    /**
     * List of all todofuken of this company
     */
    private $todofuken_list = [];
    private $lookup_array = [];

    /**
     * Initiate the todofuken list
     */
    public function __construct()
    {
        $this->todofuken_list = Todofuken::all();
        $this->lookup_array = $this->todofuken_list->pluck('name', 'id')->toArray();
    }

    /**
     * Get ids of all todofukens
     *
     * @return array
     */
    public function getIds()
    {
        return $this->todofuken_list->pluck('id')->toArray();
    }

    /**
     * Get an array of all todofuken names(key by id).
     *
     * @return array
     */
    public function getNames()
    {
        return $this->lookup_array;
    }

    /**
     * Get name of a given todofuken
     *
     * @return string
     */
    public function getName($id)
    {
        return isset($this->lookup_array[$id]) ? $this->lookup_array[$id] : null;
    }

    /**
     * Get id of a todofuken searched by a given name.
     *
     * @return int|false
     */
    public function getId($name)
    {
        if ($name) {
            return array_search($name, $this->lookup_array);
        } else {
            return false;
        }
    }

    /**
     * Get full information of all todofukens.
     */
    public function getAll()
    {
        return $this->todofuken_list;
    }
}
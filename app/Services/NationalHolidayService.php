<?php

namespace App\Services;

use App\NationalHoliday;

class NationalHolidayService
{
    /**
     * List of all querried national holidays
     */
    public $cached_national_holidays;

    /**
     * Initiate the national holiday cache
     */
    public function __construct()
    {
        $this->cached_national_holidays = [];
    }

    public function is($date = null)
    {
        $date = $date ? $date : date('Y-m-d');

        if (!array_key_exists($date, $this->cached_national_holidays)) {

            $exist = NationalHoliday::where('date', $date)->exists();
            $this->cached_national_holidays[$date] =  $exist;

        }

        return $this->cached_national_holidays[$date];
    }

    public function get($start_date = null, $end_date = null)
    {
        $query = NationalHoliday::query();

        if ($start_date) {
            $query->where('date', '>=', $start_date);
        }

        if ($end_date) {
            $query->where('date', '<=', $end_date);
        }

        $holidays = $query->pluck('date');

        $holidays_array = $holidays->toArray();

        if ($holidays_array) {
            foreach ($holidays_array as $date) {
                $this->cached_national_holidays[$date] = true;
            }
        }

        return $holidays;
    }
}
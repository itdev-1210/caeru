<?php

namespace App\Services;

use DB;
use Carbon\Carbon;
use App\PlannedSchedule;
use App\WorkLocation;
use App\Employee;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use App\EmployeeWorkingInformationSnapshot;
use App\SubstituteEmployeeWorkingInformation;
use App\CachedEmployeeWorkingInformation;
use App\WorkAddressWorkingDay;
use App\WorkAddressWorkingInformation;
use App\WorkAddressWorkingEmployee;
use App\ChecklistItem;
use App\ChecklistErrorTimer;
use App\ColorStatus;
use App\CalendarRestDay;
use App\Services\NationalHolidayService;
use App\Events\EvaluateChecklistErrorsForEWIFromPlannedSchedule;
use App\Events\WorkingTimestampChangedOnManyWorkingDays;
use App\Events\DistributeTimestampsAndReevaluateChecklistErrors;

class ScheduleProcessingService
{

    /**
     * Get the start day and end day of a real world month(not a business month).
     *
     * @param Carbon        a valid carbon instance. Only its values of year and month will be used.
     * @return array        an array of two elements: start_day(index: 0) and end_day(index: 1)
     */
    public function startAndEndDayOfMonth(Carbon $date)
    {
        $start_date = $date->copy()->day(1);
        $end_date = $start_date->copy()->addMonth()->subDay();

        return [$start_date->toDateString(), $end_date->toDateString()];
    }

    /**
     * Generate the \DatePeriod base on the start_date and end_date of the given schedule. Normally, one schedule is enough,
     * but if you provide the optional second parameter: $day_range, t will limit the period to the day_range( if the schedule's range is bigger then
     * the provided range).
     *
     * @param PlannedSchedule   $schedule       the given schedule
     * @param array             $day_range      an array contain two element, the first one is the start of a day range, the second is the end.
     * @return \DatePeriod      $period         the DatePeriod instance
     */
    public function getDatePeriod($schedule, $day_range = null)
    {
        $start_date = null;
        $end_date = null;

        // if ($schedule->start_date) {

        //     $start_date = new \DateTime($schedule->start_date);

        //     if ($schedule->end_date) {
        //         $end_date = new \DateTime($schedule->end_date);
        //         $end_date = new \DateTime($end_date->modify('+1 day')->format('Y-m-d'));
        //     } else {
        //         $end_date = new \DateTime();
        //         $end_date = new \DateTime($end_date->modify('+4 months')->format('Y-m' . '-01'));
        //     }

        // // Have end_date but start_date is empty
        // } elseif ($schedule->end_date) {

        //     $start_date = new \DateTime($schedule->end_date);
        //     $start_date = new \DateTime($start_date->modify('-4 months')->format('Y-m-t'));

        //     $end_date = new \DateTime($schedule->end_date);

        // // Both start_date and end_date is empty
        // } else {

        //     $start_date = new \DateTime();

        //     $end_date = new \DateTime('+4 months');
        //     $end_date = new \DateTime($end_date->format('Y-m' . '-01'));

        // }

        // Well all of the above is not neccessary any more, now that the PlannedSchedule always has start_date
        $start_date = new Carbon($schedule->start_date);

        if ($schedule->end_date) {
            $end_date = new Carbon($schedule->end_date);
            $end_date->addDay();
        } else {
            $end_date = Carbon::now();
            $end_date = $end_date->addMonths(4)->day(1)->subDay();
        }

        if ($day_range) {
            $start_limit = new Carbon($day_range[0]);
            $end_limit = new Carbon($day_range[1]);

            $start_date = $start_date->lt($start_limit) ? $start_limit : $start_date;
            $end_date = $end_date->gt($end_limit) ? $end_limit : $end_date;
        }

        $interval = \DateInterval::createFromDateString('1 day');

        return new \DatePeriod($start_date, $interval, $end_date);
    }

    /**
     * Create WorkingDay instances of Employee or WorkAddress base on a period of time.
     *
     * @param Eloquent $model               it's either employee or work address
     * @param \DatePeriod $period           the period to insert working day records
     * @return void
     */
    public function initializeWorkingDay($model, $period)
    {

        // For perfomance, we will be using QueryBuilder instead of Eloquent in this part
        $to_be_created_working_days = [];

        // For each day, create a set of data, then add into a container array...
        foreach ($period as $day) {

            $working_day = '(' . $model->id . ', "' . $day->format('Y-m-d') . '", "' . Carbon::now() . '")';

            $to_be_created_working_days[] = $working_day;
        }

        $config = DB::getConfig();

        // ..then use that array to generate the sql query
        if (is_a($model, Employee::class)) {

            DB::insert('insert ignore into ' . $config['database'] . '.' . $config['prefix'] . 'employee_working_days (employee_id, date, created_at) values ' . implode(',', $to_be_created_working_days));

        } else {

            DB::insert('insert ignore into ' . $config['database'] . '.' . $config['prefix'] . 'work_address_working_days (work_address_id, date, created_at) values ' . implode(',', $to_be_created_working_days));
        }
    }

    /**
     * Base on the schedule's relationship to delete all those related models
     * If the schedule has work_address_id then we have to delete EmployeeWorkingInformation, WorkAddressWorkingEmployee, and maybe
     * some WorkAddressWorkingInformation.
     * Else, we only have to delete EmployeeWorkingInformation.
     * UPDATED: now can receive an optional second parameter: a day range. To act like a hard limit.
     * UPDATEDD: now can receive another optional parameter: $need_remain, if $need_remain is true, it will return the remaining employee working informations
     * of this schedule after deletion. if $need_remain is false(which is default value) it will return the employee working days of those deleted employee
     * working informations
     *
     * @param PlannedSchedule   $schedule
     * @param array             $day_range
     * @param boolean           $need_remain
     * @return array
     */
    public function deleteRelatingModels(PlannedSchedule $schedule, $day_range = null, $need_remain = false)
    {

        // If this schedule, originally, has work_address_id, and that work_address_id now has been changed to other values or null(delete work_address case),
        // then we go with the flow below
        if ($schedule->getOriginal('work_address_id')) {

            if ($need_remain) {
                $all_employee_working_info_ids = $schedule->workAddressWorkingEmployees->pluck('employeeWorkingInformation.id')->toArray();
            }

            $working_employees_query = $schedule->workAddressWorkingEmployees()->notHavingConcludedWorkingInformations()
                                                                        ->notHavingScheduleModified()
                                                                        // ->notHavingContainingTimestampWorkingInformation()
                                                                        ->with('employeeWorkingInformation')
                                                                        ->with('workAddressWorkingInformation');
                                                                        // ->where('employee_id', $schedule->employee_id);

            // If the day_range is provided, we have to add more conditions
            if ($day_range) {
                $working_employees = $working_employees_query->whereHas('workAddressWorkingInformation', function($query) use ($day_range) {
                    return $query->whereHas('workAddressWorkingDay', function($query) use ($day_range) {
                        return $query->where('date', '>=', $day_range[0])->where('date', '<=', $day_range[1]);
                    });
                })->get();

            } else {
                $working_employees = $working_employees_query->get();
            }


            $employee_working_info_ids = $working_employees->pluck('employeeWorkingInformation.id')->toArray();

            if (!$need_remain) {
                $have_ewi_deleted_working_days = EmployeeWorkingDay::whereIn('id', $working_employees->pluck('employeeWorkingInformation.employee_working_day_id')->toArray())->get();
            }

            $this->deleteSomeEmployeeWorkingInformationAndRelatingModels($employee_working_info_ids);

            if ($need_remain) {
                $remaining_employee_working_info_ids = array_diff($all_employee_working_info_ids, $employee_working_info_ids);
            }

            DB::table('work_address_working_employees')->whereIn('id', $working_employees->pluck('id')->toArray())->delete();

            // For any WorkAddressWorkingInformation left empty by the two deletions above, delete it too
            $work_address_working_info_ids = WorkAddressWorkingInformation::has('workAddressWorkingEmployees', '=', 0)->pluck('id')->toArray();
            DB::table('work_address_working_informations')->whereIn('id', $work_address_working_info_ids)->delete();

        // If this schedule is not related to a work address, then we only need to remove the related EmployeeWorkingInformations
        } else {
            if ($need_remain) {
                $all_working_info_ids = $schedule->employeeWorkingInformations->pluck('id')->toArray();
            }

            $working_infos_query = $schedule->employeeWorkingInformations()
                                            ->notConcluded()
                                            ->notScheduleModified();
                                            // ->notManuallyModified()
                                            // ->notHaveTimestamp();

            // If the day_range is provided, we have to add more conditions
            if ($day_range) {
                $working_infos = $working_infos_query->whereHas('employeeWorkingDay', function($query) use ($day_range) {
                    return $query->where('date', '>=', $day_range[0])->where('date', '<=', $day_range[1]);
                })->get();

            } else {
                $working_infos = $working_infos_query->get();
            }

            $working_info_ids = $working_infos->pluck('id')->toArray();

            if (!$need_remain) {
                $have_ewi_deleted_working_days = EmployeeWorkingDay::whereIn('id', $working_infos->pluck('employee_working_day_id')->toArray())->get();
            }

            $this->deleteSomeEmployeeWorkingInformationAndRelatingModels($working_info_ids);

            if ($need_remain) {
                $remaining_employee_working_info_ids = array_diff($all_working_info_ids, $working_info_ids);
            }
        }

        return $need_remain ? $remaining_employee_working_info_ids : $have_ewi_deleted_working_days;
    }

    /**
     * Create all the WorkAddressWorkingInformation for WorkAddressWorkingDays, that match with the time range of the given schedule.
     * At the same time, also create suitable WorkAddressWorkingEmployee and EmployeeWorkingInformation.
     * UPDATED: now can receive an optional second parameter $day_range,  which will act like a hard limit.
     *
     * @param PlannedSchedule   $schedule
     * @param collection        $extra_working_days
     * @param array             $day_range      an array of two elements: start_day and end_day
     * @param string            $reserved_company_code      :in the case of this function being call from a console command, there won't be a company code in session
     * @return void
     */
    public function initializeWorkAddressWorkingInformationsMatchedTimeRange(PlannedSchedule $schedule, $extra_working_days = null, $day_range = null, $reserved_company_code = null)
    {
        $employee_working_days = EmployeeWorkingDay::with('workAddressWorkingEmployees.plannedSchedule')->where('employee_id', $schedule->employee_id)
                                    ->notConcluded()
                                    // Stop using this query due to low performance
                                    // ->notHaveEmployeeWorkingInformationThatAssociateWithThisScheduleThroughWorkAddressWorkingEmployee($schedule->id)
                                    ->orderBy('date')->get(['id', 'date']);

        // Instead of the commented out query above, now we filter the result set by php
        $employee_working_days = $employee_working_days->filter(function($working_day) use ($schedule) {
            $has_it = $working_day->workAddressWorkingEmployees->first(function($working_employee) use ($schedule) {
                $planned_schedule = $working_employee->plannedSchedule;
                return  $planned_schedule &&
                        $planned_schedule->id == $schedule->id &&
                        $planned_schedule->work_location_id == $schedule->work_location_id &&
                        $planned_schedule->work_address_id == $schedule->work_address_id;
            });

            return $has_it === null;
        });

        // Make a look up table to for the employee working day ids, to be used later.
        $employee_working_days_date_to_id_look_up_array = $employee_working_days->pluck('id','date');

        // Find all WorkAddressWorkingDay that match with the time range of the schedule
        $working_days = WorkAddressWorkingDay::with([
            'workAddressWorkingInformations.workAddressWorkingEmployees.plannedSchedule',
            'workAddressWorkingInformations.workAddressWorkingDay.workAddress.workLocation.company',
        ])->where('work_address_id', $schedule->work_address_id)->whereIn('date', $employee_working_days->pluck('date'))->orderBy('date');
        $working_days = $this->findWorkingDaysMatchedTimeRange($working_days, $schedule, $day_range);

        $neccessary_info_to_create_employee_working_informations = [];
        $to_be_created_employee_working_informations = [];

        // Loop through each day, see if need to create a new WorkAddressWorkingInformation or not, and also, create some other models
        foreach ($working_days as $day) {

            // Retrieve working information of a working day
            $working_informations = $day->workAddressWorkingInformations;

            // Check if this day already have a working information with the same planned_start_work_time and planned_end_work_time
            $match = $working_informations->first(function($info) use($schedule) {
                $schedule_start_carbon = new Carbon($info->schedule_start_work_time);
                $schedule_end_carbon = new Carbon($info->schedule_end_work_time);

                return $schedule_start_carbon->format('H:i:s') == $schedule->start_work_time && $schedule_end_carbon->format('H:i:s') == $schedule->end_work_time;
            });

            // If there is one, get that WorkAddressWorkingInformation's id
            if ($match) {
                $work_address_working_info_id = $match->id;

            // If none, create a new WorkAddressWorkingInformation and get that id
            } else {
                $result = new WorkAddressWorkingInformation();
                $result->workAddressWorkingDay()->associate($day);
                $result->save();

                $work_address_working_info_id = $result->id;
            }

            // Create a WorkAddressWorkingEmployee for each day
            $new_working_employee = new WorkAddressWorkingEmployee([
                'work_address_working_information_id'   => $work_address_working_info_id,
                'planned_schedule_id'                   => $schedule->id,
                'employee_id'                           => $schedule->employee_id,
                'employee_working_day_id'               => $employee_working_days_date_to_id_look_up_array[$day->date],
                'pocket_break_time'                     => $schedule->break_time,
                'pocket_night_break_time'               => $schedule->night_break_time,
                'created_at'                            => Carbon::now(),
                'updated_at'                            => Carbon::now(),
            ]);

            $new_working_employee->working_confirm = ($schedule->candidating_type == false) ? true : false;
            $new_working_employee->save();


            // And then, we have to prepare the data to mass create EmployeeWorkingInformation
            if ($schedule->candidating_type == false) {
                $employee_working_day = $employee_working_days->first(function($employee_day) use($day) {
                    return $employee_day->date == $day->date;
                });

                $to_be_created_employee_working_informations[] = [
                    'work_address_working_employee_id' => $new_working_employee->id,
                    'employee_working_day_id' => $employee_working_day->id
                ];
            }
        }

        // Now the flow is different. We will distribute the working timestamp of each working day to their respective day's working infos
        // And then we will dispatch job to re-evaluate checklist errors for those days later
        // Remember to merge from the extra working days
        $working_days_that_need_to_be_redistribute_timestamp = collect([]);

        if ($schedule->candidating_type == false) {

            // Do it like this improve performance, but we cant catch the created event this way though.
            DB::table('employee_working_informations')->insert($to_be_created_employee_working_informations);

            $working_days_that_need_to_be_redistribute_timestamp = $working_days_that_need_to_be_redistribute_timestamp->merge($working_days->pluck('id'));

            // We also need to delete all the snapshot of the 'houde' and 'kyuude' requests, and also the substitute model.
            EmployeeWorkingInformationSnapshot::whereIn('employee_working_day_id', $working_days_that_need_to_be_redistribute_timestamp->toArray())->whereNull('employee_working_information_id')->whereNull('soon_to_be_EWI_id')->delete();
            SubstituteEmployeeWorkingInformation::whereIn('employee_working_day_id', $working_days_that_need_to_be_redistribute_timestamp->toArray())->whereNull('employee_working_information_id')->delete();
        }

        if ($extra_working_days) {
            $working_days_that_need_to_be_redistribute_timestamp = $working_days_that_need_to_be_redistribute_timestamp->merge($extra_working_days->pluck('id'))->unique();
        }

        // dispatch event to evaluate the checklist error
        // event(new EvaluateChecklistErrorsForEWIFromPlannedSchedule($schedule->id, true));

        event(new WorkingTimestampChangedOnManyWorkingDays($working_days_that_need_to_be_redistribute_timestamp->toArray()));

        event(new DistributeTimestampsAndReevaluateChecklistErrors($working_days_that_need_to_be_redistribute_timestamp->toArray(), $reserved_company_code));

    }

    /**
     * Create all the EmployeeWorkingInformation for EmployeeWorkingDays, that match with the time range of the given schedule and are not concluded yet
     * UPDATED: now can receive an optional parameter $day_range, which will act like a hard limit to get the days.
     * UPDATEDD: have another optional parameter: $extra_working_days. This variable will be merge with the set of to_be_redistributed working days
     *
     * @param PlannedSchedule   $schedule
     * @param collection        $extra_working_days
     * @param array             $day_range      an array of two elements: start_day and end_day
     * @param string            $reserved_company_code      :in the case of this function being call from a console command, there won't be a company code in session
     * @return void
     */
    public function initializeEmployeeWorkingInformationsMatchedTimeRange(PlannedSchedule $schedule, $extra_working_days = null, $day_range = null, $reserved_company_code = null)
    {

        // Find all the EmployeeWorkingDay that is not concluded yet and match with the time range of the schedule
        $working_days = EmployeeWorkingDay::with('employeeWorkingInformations')->where('employee_id', $schedule->employee_id)
                        ->notConcluded();
                        // Stop using this query due to low performance
                        // ->notHaveEmployeeWorkingInformationThatAssociateWithThisSchedule($schedule->id);
        $working_days = $this->findWorkingDaysMatchedTimeRange($working_days, $schedule, $day_range);

        // Instead of the commented out query above, now we filter the result set by php
        $working_days = $working_days->filter(function($working_day) use ($schedule) {
            $has_it = $working_day->employeeWorkingInformations->first(function($working_info) use ($schedule) {
                return $working_info->planned_schedule_id == $schedule->id;
            });

            return $has_it === null;
        });

        // And create one EmployeeWorkingInformation for each of those EmployeeWorkingDay
        // Also, For perfomance we won't use Eloquent here, instead, we'll use the QueryBuilder
        $to_be_created_working_info = [];

        foreach ($working_days as $day) {

            $to_be_created_working_info[] = [
                'employee_working_day_id'   => $day->id,
                'planned_schedule_id'       => $schedule->id,
                'created_at'                => Carbon::now(),
                'work_time'                 => $schedule->work_time,
            ];
        }

        DB::table('employee_working_informations')->insert($to_be_created_working_info);

        // dispatch event to evaluate the checklist error
        // event(new EvaluateChecklistErrorsForEWIFromPlannedSchedule($schedule->id));

        // Now the flow is different. We will distribute the working timestamp of each working day to their respective day's working infos
        // And then we will dispatch job to re-evaluate checklist errors for those days later
        // Remember to merge from the extra working days
        $working_days_that_need_to_be_redistribute_timestamp = $working_days->pluck('id');

        // We also need to delete all the snapshot of the 'houde' and 'kyuude' requests, and also the substitute model.
        EmployeeWorkingInformationSnapshot::whereIn('employee_working_day_id', $working_days_that_need_to_be_redistribute_timestamp->toArray())->whereNull('employee_working_information_id')->whereNull('soon_to_be_EWI_id')->delete();
        SubstituteEmployeeWorkingInformation::whereIn('employee_working_day_id', $working_days_that_need_to_be_redistribute_timestamp->toArray())->whereNull('employee_working_information_id')->delete();

        if ($extra_working_days) {
            $working_days_that_need_to_be_redistribute_timestamp = $working_days_that_need_to_be_redistribute_timestamp->merge($extra_working_days->pluck('id'))->unique();
        }

        event(new WorkingTimestampChangedOnManyWorkingDays($working_days_that_need_to_be_redistribute_timestamp->toArray()));

        event(new DistributeTimestampsAndReevaluateChecklistErrors($working_days_that_need_to_be_redistribute_timestamp->toArray(), $reserved_company_code));

    }

    /**
     * Add query conditions base on the time range of the schedule. FYI, the "time range" of a schedule base on: start_date, end_date,
     * working_days_of_week and whether or not the employee work on a national holiday (rest_on_holiday check box).
     * UPDATED: now can receive a third optional parameter $day_range, which will act like a hard limit to get the days
     *
     * @param QueryBuilder      $query      a query builder instance
     * @param PlannedSchedule   $schedule   the given schedule
     * @param array             $day_range  and array contain two element: start_day and end_day
     * @return QueryBuilder     $query      that query builder instance with additional suitable where clauses.
     */
    protected function findWorkingDaysMatchedTimeRange($query, PlannedSchedule $schedule, $day_range = null)
    {
        $start_date = null;
        $end_date = null;

        if ($schedule->start_date) {
            $start_date = new Carbon($schedule->start_date);
            $start_limit = $day_range ? new Carbon($day_range[0]) : $start_date;
            $start_date = $start_date->lt($start_limit) ? $start_limit->format('Y-m-d') : $start_date->format('Y-m-d');

        // If the PlannedSchedule does not have start_date, but the day_range is provided, then we'll use the day_range[0] as the start_date
        } else if ($day_range) {
            $start_date = $day_range[0];
        }

        if ($schedule->end_date) {
            $end_date = new Carbon($schedule->end_date);
            $end_limit = $day_range ? new Carbon($day_range[1]) : $end_date;
            $end_date = $end_date->gt($end_limit) ? $end_limit->format('Y-m-d') : $end_date->format('Y-m-d');

        // If the PlannedSchedule does not have end_date, but the day_range is provided, then we'll use the day_range[1] as the end_date
        } else if ($day_range) {
            $end_date = $day_range[1];
        }

        // Must be greater than or equal to the start date of the schedule
        if ($start_date) {
            $query->where('date', '>=', $start_date);
        }

        // Must be smaller than or equal to the end date of the schedule
        if ($end_date) {
            $query->where('date', '<=', $end_date);
        }

        // Satisfy the day of week condition
        $query->whereIn(\DB::raw('WEEKDAY(date)'), $schedule->chosenDaysOfWeek());

        // If the schedule does not include national holidays (in other words, the box '祝日は休む' is checked)
        if ($schedule->rest_on_holiday == true) {
            $national_holiday_service = resolve(NationalHolidayService::class);
            $query->whereNotIn('date', $national_holiday_service->get($start_date, $end_date));
        }

        // If the schedule obey the WorkLocation calendar's rest day rule (企業カレンダーに従う)
        if ($schedule->prioritize_company_calendar == true) {

            $work_location = WorkLocation::find($schedule->work_location_id);
            $rest_days = $work_location->getArrayRestDays($start_date, $end_date);
            $query->whereNotIn('date', $rest_days);

        }

        return $query->get();
    }

    protected function deleteSomeEmployeeWorkingInformationAndRelatingModels($working_info_ids)
    {
        EmployeeWorkingInformation::whereIn('id', $working_info_ids)->delete();

        // Delete the checklist items (or errors)
        ChecklistItem::whereIn('employee_working_information_id', $working_info_ids)->delete();

        // Delete the timers too
        ChecklistErrorTimer::whereIn('employee_working_information_id', $working_info_ids)->delete();

        // Delete the cached model
        CachedEmployeeWorkingInformation::whereIn('employee_working_information_id', $working_info_ids)->delete();

        // Delete the snapshot, also, we have to take the id so that we can use it to delete the color statuses of those snapshots too
        $snapshot_ids = EmployeeWorkingInformationSnapshot::whereIn('employee_working_information_id', $working_info_ids)->pluck('id')->toArray();
        EmployeeWorkingInformationSnapshot::whereIn('employee_working_information_id', $working_info_ids)->orWhereIn('soon_to_be_EWI_id', $working_info_ids)->delete();

        // Delete the color status of this employee working information and of the snapshots that this working information has
        ColorStatus::where(function($query) use ($working_info_ids) {
            $query->where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION)->whereIn('colorable_id', $working_info_ids);
        })->orWhere(function($query) use ($snapshot_ids) {
            $query->where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION_SNAPSHOT)->whereIn('colorable_id', $snapshot_ids);
        })->delete();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////// These function are use for the CalenderRestDaysChangedListener ///////////////////////

    /**
     * Process a schedule. Including: remove EWI on new rest days, and add new EWI to new non rest days
     *
     * @param PlannedSchedule       $schedule
     * @param Carbon                $affected_month
     * @param array                 $changed_days
     * @param boolean               $changes_come_from_company_setting
     * @return void
     */
    public function processSchedule(PlannedSchedule $schedule, Carbon $affected_month, $changed_days, $changes_come_from_company_setting = false)
    {
        // $changed_days = collect($changed_days);
        // Filter out all the day that is not in range of this schedule - also, we dont need to change it to collection, the function below's already done it
        $changed_days = $this->filterOutRangeDays($schedule, $changed_days);

        if ($changes_come_from_company_setting == true) {

            $day_range = $this->startAndEndDayOfMonth($affected_month);

            $this_work_location_specified_rest_days = $schedule->workLocation->getSpecifiedRestDaysOfThisWorkLocation($day_range[0], $day_range[1]);

            $this_work_location_specified_non_rest_days = $schedule->workLocation->getSpecifiedNonRestDaysOfThisWorkLocation($day_range[0], $day_range[1])->pluck('assigned_date');

            $new_rest_days = $changed_days->filter(function($day) use ($this_work_location_specified_non_rest_days, $this_work_location_specified_rest_days) {
                $carbon_instance = new Carbon($day['day']);
                $formal_day = $carbon_instance->format('Y-m-d');

                return $day['type'] != CalendarRestDay::NOT_A_REST_DAY &&
                        !$this_work_location_specified_non_rest_days->contains($formal_day) &&
                        !$this_work_location_specified_rest_days->contains($formal_day);
            });

            $new_not_rest_days = $changed_days->filter(function($day) use ($this_work_location_specified_rest_days, $this_work_location_specified_non_rest_days) {
                $carbon_instance = new Carbon($day['day']);
                $formal_day = $carbon_instance->format('Y-m-d');

                return $day['type'] == CalendarRestDay::NOT_A_REST_DAY &&
                        !$this_work_location_specified_rest_days->contains($formal_day) &&
                        !$this_work_location_specified_non_rest_days->contains($formal_day);
            });

        } else {
            $new_rest_days = $changed_days->filter(function($day) {
                return $day['type'] != CalendarRestDay::NOT_A_REST_DAY;
            });

            $new_not_rest_days = $changed_days->filter(function($day) {
                return $day['type'] == CalendarRestDay::NOT_A_REST_DAY;
            });
        }

        $standardized_new_rest_days = $this->standardizeCollectionOfDays($new_rest_days);

        $to_be_deleted_EWIs = $schedule->employeeWorkingInformations->filter(function($working_info) use ($standardized_new_rest_days) {
            $working_day = $working_info->employeeWorkingDay;
            $carbon_instance = new Carbon($working_day->date);
            $day_string = $carbon_instance->format('Y-m-d');
            return $standardized_new_rest_days->contains($day_string) && !$working_day->isConcluded() && $working_info->schedule_modified == false;

        });

        $to_be_deleted_EWI_ids = $to_be_deleted_EWIs->pluck('id');

        $this->deleteSomeEmployeeWorkingInformationAndRelatingModels($to_be_deleted_EWI_ids);

        $have_EWIs_deleted_EWD_ids = $to_be_deleted_EWIs->pluck('employee_working_day_id');

        //////////////// Create new EmployeeWorkingInformation for new not rest days ////////////////

        $standardized_new_not_rest_days = $this->standardizeCollectionOfDays($new_not_rest_days);

        $working_days_that_need_to_add_new_EWI = EmployeeWorkingDay::where('employee_id', $schedule->employee_id)
                                                    ->whereIn('date', $standardized_new_not_rest_days->toArray())
                                                    ->notConcluded()->get();

        $to_be_created_working_info = [];

        foreach ($working_days_that_need_to_add_new_EWI as $day) {

            $to_be_created_working_info[] = [
                'employee_working_day_id'   => $day->id,
                'planned_schedule_id'       => $schedule->id,
                'created_at'                => Carbon::now(),
                'work_time'                 => $schedule->work_time,
            ];
        }

        DB::table('employee_working_informations')->insert($to_be_created_working_info);

        $working_days_that_need_to_be_redistribute_timestamp = $working_days_that_need_to_add_new_EWI->pluck('id');

        // We also need to delete all the snapshot of the 'houde' and 'kyuude' requests, and also the substitute model.
        EmployeeWorkingInformationSnapshot::whereIn('employee_working_day_id', $working_days_that_need_to_be_redistribute_timestamp->toArray())->whereNull('employee_working_information_id')->whereNull('soon_to_be_EWI_id')->delete();
        SubstituteEmployeeWorkingInformation::whereIn('employee_working_day_id', $working_days_that_need_to_be_redistribute_timestamp->toArray())->whereNull('employee_working_information_id')->delete();

        $working_days_that_need_to_be_redistribute_timestamp = $working_days_that_need_to_be_redistribute_timestamp->merge($have_EWIs_deleted_EWD_ids);

        event(new WorkingTimestampChangedOnManyWorkingDays($working_days_that_need_to_be_redistribute_timestamp->toArray()));

        event(new DistributeTimestampsAndReevaluateChecklistErrors($working_days_that_need_to_be_redistribute_timestamp->toArray()));

    }

    /**
     * A shortcut to turn a collection of object('day' => 'Y-m-d', 'type' => int) into a collection of day strings.
     *
     * @param collection
     * @return collection
     */
    protected function standardizeCollectionOfDays($day_set)
    {
        return $day_set->map(function($day) {
            $carbon = new Carbon($day['day']);
            return $carbon->format('Y-m-d');
        });
    }

    /**
     * Filtering out all the days that is not in the range of the given PlannedSchedule.
     *
     * @param PlannedSchedule   $schedule
     * @param array             $changed_days
     * @return Collection       $changed_days
     */
    protected function filterOutRangeDays(PlannedSchedule $schedule, $changed_days)
    {
        $changed_days = collect($changed_days);

        return $changed_days->filter(function($day) use ($schedule) {
            return $schedule->checkIfADayIsInRangeOfThisSchedule($day['day']);
        });
    }
}

<?php

namespace App;
use App\WorkLocantion;
use Carbon\Carbon;
use App\Reusables\PaidHolidayInformationTrait;
use App\Reusables\BelongsToWorkLocationTrait;

class PaidHolidayInformation extends Model
{

    use BelongsToWorkLocationTrait, PaidHolidayInformationTrait;

    /**
     * Cache the previous paid_holiday_information of this one.
     */
    protected $previous_paid_holiday_info = null;

    /**
     * Cache the available paid holidays
     */
    protected $cache_available_paid_holidays = null;

    /**
     * Cache the carried forward paid holidays in the case of work time per day changed
     */
    protected $cache_carried_forward_paid_holidays_in_case_work_time_per_day_changed = null;

    /**
     * Get the employee of this paid holiday information instance
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     *
     * get manager of this of this paidholiday information
     * 
     */
    public function manager()
    {
        return $this->belongsTo(Manager::class, 'last_modified_manager_id');
    }

    /**
     * Get the cached paid holiday information instance
     */
    public function cachedPaidHolidayInformation()
    {
        return $this->hasOne(CachedPaidHolidayInformation::class);
    }

    /**
     * Get the paid holiday information that this instance is directly linked.
     */
    public function linkedPaidHolidayInformation()
    {
        return $this->belongsTo(PaidHolidayInformation::class, 'linked_paid_holiday_information_id');
    }

    /**
     * Get all the paid holiday information instances of this employee
     */
    public function changeMilestone()
    {
        return $this->hasMany(WorkTimePerDayChangeMilestone::class);
    }

    /**
     * A PaidHolidayInformation may chain link with another PaidHolidayInformation(s)(when a PaidHolidayInformation is split).
     * This function will get the depth of the chain link (if the relation ship exists).
     *
     * @return integer
     */
    public function linkDepth()
    {
        if ($this->linkedPaidHolidayInformation) {
            return $this->linkedPaidHolidayInformation->linkDepth() + 1;
        } else {
            return 1;
        }
    }

    // Accessors

    /**
     * Accessor for consumed_paid_holidays
     */
    public function getConsumedPaidHolidaysAttribute($value)
    {
        if ($this->cachedPaidHolidayInformation != null && !$this->cachedPaidHolidayInformation->old) {
            return $this->cachedPaidHolidayInformation->consumed_paid_holidays;
        } else {
            $this->loadWorkingDaysCalculateAndCacheAttributes();
            return $this->cachedPaidHolidayInformation->consumed_paid_holidays;
        }
    }

    /**
     * Accessor for consumed_paid_holidays_hour
     */
    public function getConsumedPaidHolidaysHourAttribute($value)
    {
        if ($this->cachedPaidHolidayInformation != null && !$this->cachedPaidHolidayInformation->old) {
            return $this->cachedPaidHolidayInformation->consumed_paid_holidays_hour;
        } else {
            $this->loadWorkingDaysCalculateAndCacheAttributes();
            return $this->cachedPaidHolidayInformation->consumed_paid_holidays_hour;
        }
    }

    /**
     * Accessor for carried_forward_paid_holidays
     */
    public function getCarriedForwardPaidHolidaysAttribute($value)
    {
        if (!isset($value)) {
            $previous_paid_holiday_info = $this->previousPaidHolidayInformation();

            if ($previous_paid_holiday_info) {

                // This is the case when this period was born from another period being split
                if ($previous_paid_holiday_info->id === $this->linked_paid_holiday_information_id && $previous_paid_holiday_info->work_time_per_day != $this->work_time_per_day) {

                    return $this->calculateCarriedForwardPaidHolidaysWhenChangeWorkTimePerDay()['days'];

                } else {
                    $total_available_paid_holidays_from_previous_info = $previous_paid_holiday_info->available_paid_holidays + $previous_paid_holiday_info->available_paid_holidays_hour/$previous_paid_holiday_info->work_time_per_day;

                    if ($total_available_paid_holidays_from_previous_info >= $previous_paid_holiday_info->provided_paid_holidays) {
                        return $previous_paid_holiday_info->provided_paid_holidays;
                    } else {
                        return $previous_paid_holiday_info->available_paid_holidays;
                    }
                }

            } else {
                return 0;
            }
        } else {
            return $value;
        }
    }

    /**
     * Accessor for carried_forward_paid_holidays_hour attribute
     */
    public function getCarriedForwardPaidHolidaysHourAttribute($value)
    {
        if (!isset($value)) {
            $previous_paid_holiday_info = $this->previousPaidHolidayInformation();

            if ($previous_paid_holiday_info) {

                // This is the case when this period was born from another period being split
                if ($previous_paid_holiday_info->id === $this->linked_paid_holiday_information_id && $previous_paid_holiday_info->work_time_per_day != $this->work_time_per_day) {

                    return $this->calculateCarriedForwardPaidHolidaysWhenChangeWorkTimePerDay()['time'];

                } else {
                    $total_available_paid_holidays_from_previous_info = $previous_paid_holiday_info->available_paid_holidays + $previous_paid_holiday_info->available_paid_holidays_hour/$previous_paid_holiday_info->work_time_per_day;

                    if ($total_available_paid_holidays_from_previous_info >= $previous_paid_holiday_info->provided_paid_holidays) {
                        return 0;
                    } else {
                        return $previous_paid_holiday_info->available_paid_holidays_hour;
                    }
                }

            } else {
                return 0;
            }
        } else {
            return $value;
        }
    }

    /**
     * Accessor for available_paid_holidays attribute
     */
    public function getAvailablePaidHolidaysAttribute($value)
    {
        $available_paid_holidays = $this->availablePaidHolidays();

        return $available_paid_holidays['days'];
    }

    /**
     * Accessor for available_paid_holidays_hour attribute
     */
    public function getAvailablePaidHolidaysHourAttribute($value)
    {
        $available_paid_holidays = $this->availablePaidHolidays();

        return $available_paid_holidays['time'];
    }

    /**
     * Get planned available paid holidays of this period. 'Planned' means that: it will include all the working day that have working info have paid holiday,
     * even day from the future.
     *
     * @return array        consist of two keys: days and time
     */
    public function plannedAvailablePaidHolidays()
    {
        // If this instance is a tail or middle part in a linked chain, then we dont need to add the provided_paid_holidays.
        // Because it's already added that provided_paid_holidays before (in the previous period)
        $total_paid_days = $this->linked_paid_holiday_information_id ? $this->carried_forward_paid_holidays : $this->provided_paid_holidays + $this->carried_forward_paid_holidays;
        $total_paid_time = $this->carried_forward_paid_holidays_hour;

        while ($total_paid_time < $this->getPlannedConsumedPaidHolidaysHour()) {
            $total_paid_days -= 1;
            $total_paid_time += $this->work_time_per_day;
        }

        $total_paid_time -= $this->getPlannedConsumedPaidHolidaysHour();
        $total_paid_days -= $this->getPlannedConsumedPaidHolidays();

        return [
            'days' => $total_paid_days,
            'time' => $total_paid_time,
        ];
    }


    /**
     * Get full name of the manager who last modified this paid holiday information
     *
     * @return string
     */
    public function lastModifiedManagerName()
    {
        $last_modified_manager = Manager::find($this->last_modified_manager_id);

        if ($last_modified_manager) {
            return $last_modified_manager->fullName();
        } else {
            return null;
        }
    }

    /**
     * When Employee's work_time_per_day is changed by Manager, if that employee have a current PaidHolidayInformation, that period will be split into two.
     * Some of those two period's attributes will be the same, while some others will be calculated independently.
     * This function's purpose is to re-calculate the carried forward holidays for the second period.
     *
     * @return array
     */
    protected function calculateCarriedForwardPaidHolidaysWhenChangeWorkTimePerDay()
    {
        if ($this->cache_carried_forward_paid_holidays_in_case_work_time_per_day_changed == null) {
            $previous_paid_holiday_info = $this->previousPaidHolidayInformation();

            $to_be_carried_forward_days = $previous_paid_holiday_info->available_paid_holidays;

            $to_be_carried_forward_time = $previous_paid_holiday_info->available_paid_holidays_hour;

            $to_be_carried_forward_time = ($to_be_carried_forward_time * $this->work_time_per_day)/$previous_paid_holiday_info->work_time_per_day;

            // For some reason we have to switch the unit to hour, then round it up(繰り上げる)
            $to_be_carried_forward_time = ceil($to_be_carried_forward_time/60) * 60;

            while ($to_be_carried_forward_time >= $this->work_time_per_day) {
                $to_be_carried_forward_time -= $this->work_time_per_day;
                $to_be_carried_forward_days++;
            }

            $this->cache_carried_forward_paid_holidays_in_case_work_time_per_day_changed = [
                'days' => $to_be_carried_forward_days,
                'time' => $to_be_carried_forward_time,
            ];
        }

        return $this->cache_carried_forward_paid_holidays_in_case_work_time_per_day_changed;

    }

    /**
     * Calculate the available paid holidays and time, and then cache them.
     *
     * @return array    contain two elements: 'days' => available_paid_holidays
     *                                        'time' => available_paid_holidays_time
     */
    protected function availablePaidHolidays()
    {
        if ($this->cache_available_paid_holidays == null) {

            // If this instance is a tail or middle part in a linked chain, then we dont need to add the provided_paid_holidays.
            // Because it's already added that provided_paid_holidays before (in the previous period)
            $total_paid_days = $this->linked_paid_holiday_information_id ? $this->carried_forward_paid_holidays : $this->provided_paid_holidays + $this->carried_forward_paid_holidays;
            $total_paid_time = $this->carried_forward_paid_holidays_hour;

            while ($total_paid_time < $this->consumed_paid_holidays_hour) {
                $total_paid_days -= 1;
                $total_paid_time += $this->work_time_per_day;
            }

            $total_paid_time -= $this->consumed_paid_holidays_hour;
            $total_paid_days -= $this->consumed_paid_holidays;

            $this->cache_available_paid_holidays = [
                'days' => $total_paid_days,
                'time' => $total_paid_time,
            ];

        }

        return $this->cache_available_paid_holidays;
    }

    /**
     * Calculate the available paid rest days of a specific PaidHolidayInformation up until a give date.
     * If a suitable for that given date does not exist, return everything as 0.
     *
     * @param   string      $date ('Y-m-d')
     * @return  array       Has two keys: 'days' -> number of paid rest days
     *                                    'time' -> paid rest time in minutes
     */
    public function availablePaidHolidaysAtSpecificDate($date)
    {
        if ($date) {
            /**
             * Calculate the consumed paid rest days/time 消化日数
             */
            $suitable_working_days = EmployeeWorkingDay::with([
                'concludedEmployeeWorkingDay',
                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                'employeeWorkingInformations.plannedSchedule.workLocation.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.setting',
                'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.company.setting',
                'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
                'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
            ])->where('employee_id', $this->employee_id)->where('date', '>=', $this->period_start)->where('date', '<=', $date)->get();

            $taken_paid_rest_days = 0;
            $taken_paid_rest_time = 0;

            foreach ($suitable_working_days as $working_day) {
                $data = $working_day->getSummaryInformation();
                $taken_paid_rest_days += $data['real_taken_paid_rest_days'];
                $taken_paid_rest_time += $data['real_taken_paid_rest_time'];
            }

            if ($taken_paid_rest_time > $this->work_time_per_day) {
                $taken_paid_rest_days += floor($taken_paid_rest_time/$this->work_time_per_day);
                $taken_paid_rest_time = $taken_paid_rest_time % $this->work_time_per_day;
            }
            /**
             * Finish calculating consumed paid rest days/time　消化日数
             */


            /**
             * Calculate the total paid rest days/time　
             */
            $total_paid_days = $this->linked_paid_holiday_information_id ? $this->carried_forward_paid_holidays : $this->provided_paid_holidays + $this->carried_forward_paid_holidays;
            $total_paid_time = $this->carried_forward_paid_holidays_hour;

            // Increase total paid rest time before hand if it is smaller than consumed paid rest time
            while ($total_paid_time < $taken_paid_rest_time) {
                $total_paid_days -= 1;
                $total_paid_time += $this->work_time_per_day;
            }
            /**
             * Finish calculating the total paid rest days/time
             */


            /**
             * Calculate the available paid rest days/time　残日数
             */
            $total_paid_days -= $taken_paid_rest_days;
            $total_paid_time -= $taken_paid_rest_time;
            /**
             * Finish calculating the available paid rest days/time 残日数
             */

            return [
                'days' => $total_paid_days,
                'time' => $total_paid_time,
            ];
        }
    }

    /**
     * Get all paid holidays, even day from the future.
     *
     * @return float
     */
    protected function getPlannedConsumedPaidHolidays()
    {
        if ($this->cachedPaidHolidayInformation != null && !$this->cachedPaidHolidayInformation->old) {
            return $this->cachedPaidHolidayInformation->planned_consumed_paid_holidays;
        } else {
            $this->loadWorkingDaysCalculateAndCacheAttributes();
            return $this->cachedPaidHolidayInformation->planned_consumed_paid_holidays;
        }
    }

    /**
     * Get all paid_holidays_hout, even the one from the future.
     *
     * @return float
     */
    protected function getPlannedConsumedPaidHolidaysHour()
    {
        if ($this->cachedPaidHolidayInformation != null && !$this->cachedPaidHolidayInformation->old) {
            return $this->cachedPaidHolidayInformation->planned_consumed_paid_holidays_hour;
        } else {
            $this->loadWorkingDaysCalculateAndCacheAttributes();
            return $this->cachedPaidHolidayInformation->planned_consumed_paid_holidays_hour;
        }
    }

    /**
     * Load needed EmployeeWorkingDays to calculate the attendance_rate, consumed_paid_holidays and consumed_paid_holidays_hour
     *
     * @return void
     */
    public function loadWorkingDaysCalculateAndCacheAttributes()
    {

        $cache_not_exist = false;
        if ($this->cachedPaidHolidayInformation === null) {
            $cache = new CachedPaidHolidayInformation();
            $cache->paid_holiday_information_id = $this->id;
            $cache_not_exist = true;
        } else {
            $cache = $this->cachedPaidHolidayInformation;
        }

        $query = EmployeeWorkingDay::with([
            'concludedEmployeeWorkingDay',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            'employeeWorkingInformations.plannedSchedule.workLocation.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.setting',
            'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.setting',
            'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
        ]);

        //** AttendanceRate - cache this attribute
        // $start_day = new Carbon($this->period_start);
        // $one_year_ago_working_days = with(clone $query)->where('employee_id', $this->employee_id)
        //     ->where('date', '>=', $start_day->copy()->subYear()->format('Y-m-d'))
        //     ->where('date', '<=', $start_day->copy()->subDay()->format('Y-m-d'))->get();

        // $have_schedule_days = 0;
        // $kekkin_days = 0;

        // foreach ($one_year_ago_working_days as $working_day) {
        //     $data = $working_day->getSummaryInformation();
        //     $have_schedule_days = $data['have_schedule'] ? $have_schedule_days + 1 : $have_schedule_days;
        //     $kekkin_days = $data['real_is_kekkin'] ? $kekkin_days + 1 : $kekkin_days;
        // }

        // if ($have_schedule_days != 0) {
        //     $cache->attendance_rate = (($have_schedule_days - $kekkin_days) * 100)/$have_schedule_days;
        // } else {
        //     $cache->attendance_rate = null;
        // }
        /////////////////////////////////////////////////////////////////////

        //** ConsumedPaidHolidays and ConsumedPaidHolidaysHour - cache these attributes
        $this_period_working_days = with(clone $query)->where('employee_id', $this->employee_id)
            ->where('date', '>=', $this->period_start)
            ->where('date', '<=', $this->period_end)->get();

        $taken_paid_rest_days = 0;
        $taken_paid_rest_time = 0;
        $planned_taken_paid_rest_days = 0;
        $planned_taken_paid_rest_time = 0;


        foreach ($this_period_working_days as $working_day) {
            $data = $working_day->getSummaryInformation();
            $taken_paid_rest_days += $data['real_taken_paid_rest_days'];
            $taken_paid_rest_time += $data['real_taken_paid_rest_time'];
            $planned_taken_paid_rest_days += $data['planned_taken_paid_rest_days'];
            $planned_taken_paid_rest_time += $data['planned_taken_paid_rest_time'];
        }

        if ($taken_paid_rest_time > $this->work_time_per_day) {
            $taken_paid_rest_days += floor($taken_paid_rest_time/$this->work_time_per_day);
            $taken_paid_rest_time = $taken_paid_rest_time % $this->work_time_per_day;
        }

        if ($planned_taken_paid_rest_time > $this->work_time_per_day) {
            $planned_taken_paid_rest_days += floor($planned_taken_paid_rest_time/$this->work_time_per_day);
            $planned_taken_paid_rest_time = $planned_taken_paid_rest_time % $this->work_time_per_day;
        }

        $cache->consumed_paid_holidays = $taken_paid_rest_days;
        $cache->consumed_paid_holidays_hour = $taken_paid_rest_time;
        $cache->planned_consumed_paid_holidays = $planned_taken_paid_rest_days;
        $cache->planned_consumed_paid_holidays_hour = $planned_taken_paid_rest_time;
        /////////////////////////////////////////////////////////////////////

        // After all the calculation, we flag this cache instance as 'not old' and save it.
        $cache->old = false;
        $cache->save();
        if ($cache_not_exist) $this->load('cachedPaidHolidayInformation');
    }

    /**
     * Get the previous PaidHolidayInformation of this instance.(Ex:if this instance is 2018/05/01 ~ 2019/04/30, then the previous
     * instance is likely to be 2017/05/01 ~ 2018/04/30)
     *
     * @return PaidHolidayInformation
     */
    protected function previousPaidHolidayInformation()
    {
        if ($this->previous_paid_holiday_info == null) {
            $current_employee_all_paid_holiday_infos = $this->employee->paidHolidayInformations;

            $limit_period_start = new Carbon($this->period_start);
            $paid_holiday_infos_before_this = $current_employee_all_paid_holiday_infos->filter(function($info) use ($limit_period_start) {
                $current_period_start = new Carbon($info->period_start);
                return $current_period_start->lt($limit_period_start);
            })->sortBy('period_start');

            $this->previous_paid_holiday_info = $paid_holiday_infos_before_this->last();
        }

        return $this->previous_paid_holiday_info;
    }

}

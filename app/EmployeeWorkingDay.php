<?php

namespace App;

use Carbon\Carbon;

class EmployeeWorkingDay extends Model
{

    /**
     * Get the employee of this working day instance
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * Get all the working information instances of this working day instance
     */
    public function employeeWorkingInformations()
    {
        return $this->hasMany(EmployeeWorkingInformation::class);
    }

    /**
     * Get all the snapshot instances of this working day instance
     */
    public function employeeWorkingInformationSnapshots()
    {
        return $this->hasMany(EmployeeWorkingInformationSnapshot::class);
    }

    /**
     * Auto merge all of working information to all snap shot for every employee working day
     * Return new list snapshot
     */
    public function mergeSnapshotsAndEmployeeWorkingInformations()
    {
        $list_snapshot_after_updated = collect([]);
        $employee_working_informations = $this->employeeWorkingInformations()->with([
            'employeeWorkingInformationSnapshot',
            'cachedEmployeeWorkingInformation',
            'concludedEmployeeWorkingInformation',
            'plannedSchedule.workLocation.setting',
            'plannedSchedule.workLocation.unusedWorkStatuses',
            'plannedSchedule.workLocation.unusedRestStatuses',
            'plannedSchedule.workLocation.company.setting',
            'plannedSchedule.workLocation.company.workStatuses',
            'plannedSchedule.workLocation.company.restStatuses',
            'currentRealWorkLocation.setting',
            'currentRealWorkLocation.unusedWorkStatuses',
            'currentRealWorkLocation.unusedRestStatuses',
            'currentRealWorkLocation.company.setting',
            'currentRealWorkLocation.company.workStatuses',
            'currentRealWorkLocation.company.restStatuses',
            'employeeWorkingDay.employee.workLocation.company',
        ])->get();
        foreach ($employee_working_informations as $employee_working_information) {
            $list_snapshot_after_updated->push($employee_working_information->mergeWorkingInformationToSnapshot());
        }

        // UPDATE 2019-01-22: Now we will always query for transfer snapshots
        $transfer_snapshots = $this->employeeWorkingInformationSnapshots()->whereNull('employee_working_information_id')->whereNotNull('soon_to_be_EWI_id')->get();
        foreach ($transfer_snapshots as $snapshot) {
            $ewi = EmployeeWorkingInformation::find($snapshot->soon_to_be_EWI_id);
            if (isset($ewi)) {
                $from = Carbon::createFromFormat('Y-m-d', $ewi->employeeWorkingDay->date);
                $to = Carbon::createFromFormat('Y-m-d', $this->date);
                $diff = $to->diffInDays($from);

                $snapshot->left_schedule_start_work_time = isset($ewi->schedule_start_work_time)
                    ? Carbon::createFromFormat('Y-m-d H:i:s', $ewi->schedule_start_work_time)->addDay($diff)->format('Y-m-d H:i:s') : null;
                $snapshot->left_schedule_end_work_time = isset($ewi->schedule_end_work_time)
                    ? Carbon::createFromFormat('Y-m-d H:i:s', $ewi->schedule_end_work_time)->addDay($diff)->format('Y-m-d H:i:s') : null;
                $snapshot->left_schedule_break_time = $ewi->schedule_break_time;
                $snapshot->left_schedule_night_break_time = $ewi->schedule_night_break_time;
                $snapshot->left_schedule_working_hour = $ewi->schedule_working_hour;
            }
        }

        $list_snapshot_after_updated = $list_snapshot_after_updated->concat($transfer_snapshots);

        // UPDATE 2019-01-22: Change for the new spec of furikae the 'if' below is for preparing data for HOUDE/KYUUDE cases. it will be kept for the moment.
        // The 'if' that has been commented out is for preparing data for FURIKAE feature. It has been refactored into the code above(with out the if).

        // if (count($list_snapshot_after_updated) == 0) {
            // $list_snapshot_after_updated = $this->employeeWorkingInformationSnapshots()->whereNull('employee_working_information_id')->whereNotNull('soon_to_be_EWI_id')->get();
            // if(count($list_snapshot_after_updated) == 0) {
                $snapshot = $this->employeeWorkingInformationSnapshots()->whereNull('employee_working_information_id')->whereNull('soon_to_be_EWI_id')->first();
                if ($snapshot) {
                    $list_snapshot_after_updated->push($snapshot);
                }
            // }
            // } else {
                // foreach ($list_snapshot_after_updated as $snapshot) {
                //     $ewi = EmployeeWorkingInformation::find($snapshot->soon_to_be_EWI_id);
                //     if (isset($ewi)) {
                //         $from = Carbon::createFromFormat('Y-m-d', $ewi->employeeWorkingDay->date);
                //         $to = Carbon::createFromFormat('Y-m-d', $this->date);
                //         $diff = $to->diffInDays($from);

                //         $snapshot->left_schedule_start_work_time = isset($ewi->schedule_start_work_time)
                //             ? Carbon::createFromFormat('Y-m-d H:i:s', $ewi->schedule_start_work_time)->addDay($diff)->format('Y-m-d H:i:s') : null;
                //         $snapshot->left_schedule_end_work_time = isset($ewi->schedule_end_work_time)
                //             ? Carbon::createFromFormat('Y-m-d H:i:s', $ewi->schedule_end_work_time)->addDay($diff)->format('Y-m-d H:i:s') : null;
                //         $snapshot->left_schedule_break_time = $ewi->schedule_break_time;
                //         $snapshot->left_schedule_night_break_time = $ewi->schedule_night_break_time;
                //         $snapshot->left_schedule_working_hour = $ewi->schedule_working_hour;
                //     }
                // }
            // }
        // }
        return $list_snapshot_after_updated;
    }

    /**
     * Another version of the above function, but without the data loading part.
     * So that we can eager loading from the outside.
     *
     * @return Collection
     */
    public function mergeSnapshotsAndEmployeeWorkingInformationsWithoutLoadingDataVersion()
    {
        $list_snapshot_after_updated = collect([]);
        $employee_working_informations = $this->employeeWorkingInformations;
        foreach ($employee_working_informations as $employee_working_information) {
            $list_snapshot_after_updated->push($employee_working_information->mergeWorkingInformationToSnapshot());
        }

        // UPDATE 2019-01-22: Now we will always query for transfer snapshots
        $transfer_snapshots = $this->employeeWorkingInformationSnapshots->filter(function($snapshot) {
            return $snapshot->employee_working_information_id == null && $snapshot->soon_to_be_EWI_id != null;
        });
        foreach ($transfer_snapshots as $snapshot) {
            $ewi = EmployeeWorkingInformation::find($snapshot->soon_to_be_EWI_id);
            if (isset($ewi)) {
                $from = Carbon::createFromFormat('Y-m-d', $ewi->employeeWorkingDay->date);
                $to = Carbon::createFromFormat('Y-m-d', $this->date);
                $diff = $to->diffInDays($from);

                $snapshot->left_schedule_start_work_time = isset($ewi->schedule_start_work_time)
                    ? Carbon::createFromFormat('Y-m-d H:i:s', $ewi->schedule_start_work_time)->addDay($diff)->format('Y-m-d H:i:s') : null;
                $snapshot->left_schedule_end_work_time = isset($ewi->schedule_end_work_time)
                    ? Carbon::createFromFormat('Y-m-d H:i:s', $ewi->schedule_end_work_time)->addDay($diff)->format('Y-m-d H:i:s') : null;
                $snapshot->left_schedule_break_time = $ewi->schedule_break_time;
                $snapshot->left_schedule_night_break_time = $ewi->schedule_night_break_time;
                $snapshot->left_schedule_working_hour = $ewi->schedule_working_hour;
            }
        }

        $list_snapshot_after_updated = $list_snapshot_after_updated->concat($transfer_snapshots);

        // if(count($list_snapshot_after_updated) == 0) {
            $snapshot = $this->employeeWorkingInformationSnapshots()->whereNull('employee_working_information_id')->whereNull('soon_to_be_EWI_id')->first();
            if ($snapshot) {
                $list_snapshot_after_updated->push($snapshot);
            }
        // }

        return $list_snapshot_after_updated;
    }

    /**
     * Get all the concluded working information instances of this working day instance
     */
    public function concludedEmployeeWorkingInformations()
    {
        return $this->hasMany(ConcludedEmployeeWorkingInformation::class);
    }

    /**
     * Get all the working timestamp instances of this working day instance
     */
    public function workingTimestamps()
    {
        return $this->hasMany(WorkingTimestamp::class);
    }

    /**
     * Get all the SubstituteEmployeeWorkingInformation instances of this working day instance
     */
    public function substituteEmployeeWorkingInformations()
    {
        return $this->hasMany(SubstituteEmployeeWorkingInformation::class);
    }

    /**
     * Get the concluded version of this instance (if there is any)
     */
    public function concludedEmployeeWorkingDay()
    {
        return $this->hasOne(ConcludedEmployeeWorkingDay::class);
    }

    /**
     * The manager who concluded level one this employee working day.(This is a level one conclude)
     */
    public function concludedLevelOneManager()
    {
        return $this->belongsTo(Manager::class, 'concluded_level_one_manager_id');
    }

    /**
     * The manager who concluded level two this employee working day.
     */
    public function concludedLevelTwoManager()
    {
        return $this->belongsTo(Manager::class, 'concluded_level_two_manager_id');
    }

    /**
     * Get all the color statuses of this working day instance
     */
    public function colorStatuses()
    {
        return $this->hasMany(ColorStatus::class);
    }

    /**
     * Get all the WorkAddressWorkingEmployees of this instance
     */
    public function workAddressWorkingEmployees()
    {
        return $this->hasMany(WorkAddressWorkingEmployee::class);
    }

    /**
     * The concluded working month to which this working day belongs (if it is concluded level 2)
     * So if it is not concluded level 2, it wont have any concluded working month
     */
    // public function concludedEmployeeWorkingMonth()
    // {
    //     return $this->belongsTo(ConcludedEmployeeWorkingMonth::class);
    // }

    //////// Query Scope ///////

    /**
     * Get the EmployeeWorkingDay which has not been concluded yet
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotConcluded($query)
    {
        return $query->where('concluded_level_one', false)->where('concluded_level_two', false);
    }

    /**
     * Get the EmployeeWorkingDay which doesnt have an EmployeeWorkingInfomation that assocciates with a given PlannedSchedule and
     * also has manually_modified flag ON.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int                                   $schedule_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHaveManuallyModifiedEmployeeWorkingInformationThatAssociateWithThisSchedule($query, $schedule_id)
    {
        return $query->whereDoesntHave('employeeWorkingInformations', function($query) use ($schedule_id) {
            $query->where('planned_schedule_id', $schedule_id)->where('manually_modified', true);
        });
    }


    /**
     * Exactly the same as the above scope, except that: It does not care about manually_modified flad.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int                                   $schedule_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHaveEmployeeWorkingInformationThatAssociateWithThisSchedule($query, $schedule_id)
    {
        return $query->whereDoesntHave('employeeWorkingInformations', function($query) use ($schedule_id) {
            $query->where('planned_schedule_id', $schedule_id);
        });
    }

    /**
     * Get the EmployeeWorkingDay which doesnt have an EmployeeWorkingInformation that was manually modified and also associates with a given PlannedSchedule,
     * but not in a normal way, through WorkAddressWorkingEmployee that is.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param int                                  $schedule_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHaveManuallyModifiedEmployeeWorkingInformationThatAssociateWithThisScheduleThroughWorkAddressWorkingEmployee($query, $schedule_id)
    {
        return $query->whereDoesntHave('employeeWorkingInformations', function($query) use ($schedule_id) {
            $query->whereHas('workAddressWorkingEmployee', function($query) use ($schedule_id) {
                $query->where('planned_schedule_id', $schedule_id);
            })->where('manually_modified', true);
        });
    }

    /**
     * Exactly the same as the above scope, except that: It does not care about manually_modified flad.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param int                                  $schedule_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHaveEmployeeWorkingInformationThatAssociateWithThisScheduleThroughWorkAddressWorkingEmployee($query, $schedule_id)
    {
        return $query->whereDoesntHave('employeeWorkingInformations', function($query) use ($schedule_id) {
            $query->whereHas('workAddressWorkingEmployee', function($query) use ($schedule_id) {
                $query->where('planned_schedule_id', $schedule_id);
            });
        });
    }

    ////////////////////////////

    /**
     * Get employee working date base on date condition
     *
     * @param date $beginDate
     * @param date $endDate
     *
     * @return Builder
     */
    public function scopeWorkingDay($query, $beginDate, $endDate)
    {
        return $query
        ->whereBetween('employee_working_days.date',[$beginDate,$endDate]);
    }

    /**
     * Check if this working day instance have any working information instance that is non-work one.
     *
     * @param int|boolean
     */
    public function haveANonWorkWorkingInformation()
    {
        $have_a_non_work_working_information = null;

        foreach ($this->employeeWorkingInformations as $working_info) {
            if ($working_info->isThisANonWorkWorkingInformation() === true) {
                $have_a_non_work_working_information = $working_info->id;
                break;
            }
        }

        return $have_a_non_work_working_information;
    }

    /**
     * Summarize this working day's information of this employee.
     *
     * @return array
     */
    public function getSummaryInformation()
    {
        if ($this->isConcluded()) {
            return $this->concludedEmployeeWorkingDay->toArray();
        } else {

            // Initialize the summary data
            $data = [
                'have_schedule' => false,
                'planned_work_day' => false,
                'real_work_day' => false,
                'planned_working_time' => 0,
                'real_working_time' => 0,
                'schedule_working_time' => 0,
                'planned_work_span_time'  => 0,
                'real_work_span_time'  => 0,
                'planned_paid_rest_time'    => 0,
                'real_paid_rest_time'    => 0,
                'planned_unpaid_rest_time'  => 0,
                'real_unpaid_rest_time'  => 0,
                'planned_non_work_time' => 0,
                'real_non_work_time' => 0,
                'planned_overtime_work_time'    => 0,
                'real_overtime_work_time'    => 0,
                'planned_night_work_time'   => 0,
                'real_night_work_time'   => 0,
                'planned_is_kekkin' => false,
                'real_is_kekkin' => false,
                'planned_taken_paid_rest_days'  => 0,
                'real_taken_paid_rest_days'  => 0,
                'planned_taken_paid_rest_time'  => 0,
                'real_taken_paid_rest_time'  => 0,
            ];

            $working_infos = $this->employeeWorkingInformations;

            if ($working_infos) {

                foreach ($working_infos as $working_info) {

                    $data['have_schedule'] = ($working_info->schedule_working_hour !== null) ? true : $data['have_schedule'];
                    $data['planned_work_day'] = ($working_info->summaryIsAPlannedWorkDay() == true) ? true : $data['planned_work_day'];
                    $data['real_work_day'] = ($working_info->summaryIsARealWorkDay() == true) ? true : $data['real_work_day'];

                    // Note: when kekkin, planned_working_hour is still displayed like normal, but is not included in calculation
                    $data['planned_working_time'] = ($working_info->planned_work_status_id === WorkStatus::KEKKIN) ? $data['planned_working_time'] : $data['planned_working_time'] + $working_info->summaryPlannedWorkingHour();

                    $data['real_working_time'] += $working_info->summaryRealWorkingHour();
                    $data['schedule_working_time'] += $working_info->summaryScheduleWorkingHour();

                    // Note: when kekkin, planned_work_span is still displayed like normal, but is not included in calculation
                    $data['planned_work_span_time'] = ($working_info->planned_work_status_id === WorkStatus::KEKKIN) ? $data['planned_work_span_time'] : $data['planned_work_span_time'] + $working_info->summaryPlannedWorkSpan();

                    $data['real_work_span_time'] += $working_info->summaryRealWorkSpan();
                    $data['planned_paid_rest_time'] += $working_info->summaryPlannedPaidRestTime();
                    $data['real_paid_rest_time'] += $working_info->summaryRealPaidRestTime();
                    $data['planned_unpaid_rest_time'] += $working_info->summaryPlannedUnpaidRestTime();
                    $data['real_unpaid_rest_time'] += $working_info->summaryRealUnpaidRestTime();
                    $data['planned_non_work_time'] += $working_info->summaryPlannedNonWorkTime();
                    $data['real_non_work_time'] += $working_info->summaryRealNonWorkTime();
                    $data['planned_overtime_work_time'] += $working_info->summaryPlannedOvertimeWorkTime();
                    $data['real_overtime_work_time'] += $working_info->summaryRealOvertimeWorkTime();
                    $data['planned_night_work_time'] += $working_info->summaryPlannedNightWorkTime();
                    $data['real_night_work_time'] += $working_info->summaryRealNightWorkTime();
                    $data['planned_is_kekkin'] = ($working_info->planned_work_status_id === WorkStatus::KEKKIN && $working_info->timestamped_start_work_time == null && $working_info->timestamped_end_work_time == null) ? true : $data['planned_is_kekkin'];
                    $data['real_is_kekkin'] = ($working_info->real_work_status_id === WorkStatus::KEKKIN && $working_info->timestamped_start_work_time == null && $working_info->timestamped_end_work_time == null) ? true : $data['real_is_kekkin'];
                    $data['planned_taken_paid_rest_days'] = ($working_info->summaryPlannedTakenPaidRestDays() > 0) ? $working_info->summaryPlannedTakenPaidRestDays() : $data['planned_taken_paid_rest_days'];
                    $data['real_taken_paid_rest_days'] = ($working_info->summaryRealTakenPaidRestDays() > 0) ? $working_info->summaryRealTakenPaidRestDays() : $data['real_taken_paid_rest_days'];
                    $data['planned_taken_paid_rest_time'] += $working_info->summaryPlannedTakenPaidRestTime();
                    $data['real_taken_paid_rest_time'] += $working_info->summaryRealTakenPaidRestTime();
                }
            }

            return $data;

        }
    }


    /**
     * Make a compacted version of EmployeeWorkingInformation of this EmployeeWorkingDay. This compacted data is used in employee_working_month page and
     * employee_working_information_management page
     *
     * @return array
     */
    public function getCompactedWorkingInformations()
    {
        $working_infos = $this->employeeWorkingInformations->sortBy('planned_start_work_time');

        $compacted_data = collect([]);

        if ($working_infos->isNotEmpty()) {

            $compacted_data = $working_infos->map(function($working_info) {

                $data = [];
                if ($working_info->concludedEmployeeWorkingInformation) {
                    $data = $working_info->concludedEmployeeWorkingInformation->toArray();
                    $data['schedule_start_work_time'] = $working_info->schedule_start_work_time;
                    $data['schedule_end_work_time'] = $working_info->schedule_end_work_time;
                } else {
                    $data['work_status_id'] = $working_info->planned_work_status_id;
                    $data['rest_status_id'] = $working_info->planned_rest_status_id;
                    $data['planned_work_location_id'] = $working_info->planned_work_location_id;
                    $data['real_work_location_id'] = $working_info->real_work_location_id;
                    $data['planned_work_address_id'] = $working_info->planned_work_address_id;
                    $data['real_work_address_id'] = $working_info->real_work_address_id;
                    $data['planned_start_work_time'] = $working_info->planned_start_work_time;
                    $data['timestamped_start_work_time'] = $working_info->timestamped_start_work_time;
                    $data['real_start_work_time'] = $working_info->real_start_work_time;
                    $data['planned_end_work_time'] = $working_info->planned_end_work_time;
                    $data['timestamped_end_work_time'] = $working_info->timestamped_end_work_time;
                    $data['real_end_work_time'] = $working_info->real_end_work_time;
                    $data['planned_break_time'] = $working_info->planned_break_time;
                    $data['real_break_time'] = $working_info->real_break_time;
                    $data['planned_night_break_time'] = $working_info->planned_night_break_time;
                    $data['real_night_break_time'] = $working_info->real_night_break_time;
                    $data['planned_go_out_time'] = $working_info->planned_go_out_time;
                    $data['real_go_out_time'] = $working_info->real_go_out_time;
                    $data['schedule_start_work_time'] = $working_info->schedule_start_work_time;
                    $data['schedule_end_work_time'] = $working_info->schedule_end_work_time;

                    // These four attributes: turn to null if equal zero - Request of Nui noi
                    $planned_total_late_and_leave_early = $working_info->summaryPlannedTotalLateAndEarlyLeave();
                    $data['planned_total_late_and_leave_early'] = $planned_total_late_and_leave_early == 0 ? null : $planned_total_late_and_leave_early;

                    // These four attributes: turn to null if equal zero - Request of Nui noi
                    $real_total_late_and_leave_early = $working_info->summaryRealTotalLateAndEarlyLeave();
                    $data['real_total_late_and_leave_early'] = $real_total_late_and_leave_early == 0 ? null : $real_total_late_and_leave_early;

                    // These four attributes: turn to null if equal zero - Request of Nui noi
                    $planned_total_early_arrive_and_overtime = $working_info->summaryPlannedOvertimeWorkTime();
                    $data['planned_total_early_arrive_and_overtime'] = $planned_total_early_arrive_and_overtime == 0 ? null : $planned_total_early_arrive_and_overtime;

                    // These four attributes: turn to null if equal zero - Request of Nui noi
                    $real_total_early_arrive_and_overtime = $working_info->summaryRealOvertimeWorkTime();
                    $data['real_total_early_arrive_and_overtime'] = $real_total_early_arrive_and_overtime == 0 ? null : $real_total_early_arrive_and_overtime;

                    $data['planned_work_span'] = $working_info->summaryPlannedWorkSpan();
                    $data['real_work_span'] = $working_info->summaryRealWorkSpan();
                    $data['planned_working_hour'] = $working_info->summaryPlannedWorkingHour();
                    $data['real_working_hour'] = $working_info->summaryRealWorkingHour();
                    $data['note'] = $working_info->note;
                }

                // After get the data, decorate them with the color statuses
                $data = $this->maskWithFakeValueFromColorStatusData($working_info, $data);
                $data['color_statuses'] = $working_info->getColorStatusData();

                // Get the setting about how to display some attributes(timestamped_start/end_work and real_go_out_time) in the front end
                $red_number_setting = $working_info->getRedNumberSetting();

                return array_merge($data, $red_number_setting);
            });
        }

        $substitute_color_statuses = $this->substituteEmployeeWorkingInformations;

        $substitute_compacted_data = $substitute_color_statuses->map(function($substitute) {
            $data = $substitute->getColorStatusButOnlyTheValues();
            $data['color_statuses'] = $substitute->getColorStatusData();

            return $data;
        });

        $compacted_data = $compacted_data->concat($substitute_compacted_data);
        return $compacted_data;
    }

    /**
     * Mask the fields which has a fake value from the ColorStatus
     *
     * @param EmployeeWorkingInformation    $working_info
     * @param mixed                         $data_to_be_masked
     * @return mixed
     */
    protected function maskWithFakeValueFromColorStatusData($working_info, $data_to_be_masked)
    {
        foreach ($data_to_be_masked as $field_name => $original_value) {
            if (isset($working_info->getColorStatusData()[$field_name])) {
                if ($working_info->getColorStatusData()[$field_name]['value'] == config('caeru.empty') ||
                    $working_info->getColorStatusData()[$field_name]['value'] == config('caeru.empty_date') ||
                    $working_info->getColorStatusData()[$field_name]['value'] == config('caeru.empty_time')) {
                        $data_to_be_masked[$field_name] = null;

                } else if (isset($working_info->getColorStatusData()[$field_name]['value'])) {
                    $data_to_be_masked[$field_name] = $working_info->getColorStatusData()[$field_name]['value'];
                }
            }
        }

        return $data_to_be_masked;
    }

    /**
     * Check whether or not this EmployeeWorkingDay is concluded
     *
     * @return boolean
     */
    public function isConcluded()
    {
        return $this->concluded_level_one || $this->concluded_level_two;
    }

    /**
     * Check if this EmployeeWorkingDay is having an 'considering' request or not.
     *
     * @return boolean
     */
    public function isHavingConsideringRequest()
    {
        if ($this->employeeWorkingInformationSnapshots->isNotEmpty()) {
            $have_considering_request = $this->employeeWorkingInformationSnapshots->first(function($snapshot) {
                return $snapshot->left_status === EmployeeWorkingInformationSnapshot::CONSIDERING;
            });
            return $have_considering_request !== null;
        }

        return false;
    }

    /**
     * Get name of the Manger who level-two concluded this working day
     *
     * @return string
     */
    public function getConcludedLevelTwoManagerName()
    {
        return $this->concluded_level_two ? $this->concludedLevelTwoManager->fullName() : null;
    }


    /**
     * Get name of the Manager who level-one concluded this working day
     *
     * @return string
     */
    public function getConcludedLevelOneManagerName()
    {
        return $this->concluded_level_one ? $this->concludedLevelOneManager->fullName() : null;
    }

}

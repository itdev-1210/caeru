<?php

namespace App\Observers;

use App\ColorStatus;
use App\EmployeeWorkingInformationSnapshot;

class EmployeeWorkingInformationSnapshotObserver
{

    /**
     * When delete a snapshot, have to cascade delete some relating models
     *
     * @return void
     */
    public function deleting(EmployeeWorkingInformationSnapshot $snapshot)
    {
        // Cascade delete the color statuses
        ColorStatus::where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION_SNAPSHOT)->where('colorable_id', $snapshot->id)->delete();
    }
}
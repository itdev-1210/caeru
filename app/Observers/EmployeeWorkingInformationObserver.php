<?php

namespace App\Observers;

use App\ChecklistItem;
use App\WorkStatus;
use App\EmployeeWorkingInformation;
use App\CachedEmployeeWorkingInformation;
use App\ChecklistErrorTimer;
use App\ColorStatus;
use App\EmployeeWorkingInformationSnapshot;
use App\Events\PaidHolidayChanged;
use App\Services\EvaluateChecklistErrorsService;
use App\Events\ConfimNeededHaveScheduleButOfflineError;
use App\Events\CachedPaidHolidayInformationBecomeOld;
use App\Listeners\Reusables\RelateToChecklistErrorTrait;

class EmployeeWorkingInformationObserver
{
    use RelateToChecklistErrorTrait;

    /**
     * Listen to the updated event of the an EmployeeWorkingInformation instance to check for confirm_needed error
     *
     * @param Eloquent $model
     * @return void
     */
    public function updated(EmployeeWorkingInformation $working_info)
    {
        if ($working_info->evaluateChecklistErrorsOrNot() == true) {

            // Re-cache this EmployeeWorkingInformation instance
            $working_info->calculateAndCacheAttributes();
            $evaluate_service = resolve(EvaluateChecklistErrorsService::class);

            $evaluate_service->evaluateChecklistErrorsFromAnInstanceOfEmployeeWorkingInformation($working_info);

            // event(new PaidHolidayChanged($working_info->employeeWorkingDay->employee));
        }
    }

    /**
     * Listen to the updating event of the an EmployeeWorkingInformation instance to modify some attributes conditionally
     *
     * @param Eloquent $model
     * @return void
     */
    public function updating(EmployeeWorkingInformation $working_info)
    {
        // Cancel HOUDE/KYUDE/ZANGYOU mechanism
        if (array_key_exists('planned_work_status_id', $working_info->getDirty())) {
            $old_value = $working_info->getOriginal('planned_work_status_id');
            $new_value = $working_info->getDirty()['planned_work_status_id'];
            if (($old_value == WorkStatus::HOUDE || $old_value == WorkStatus::KYUUDE || $old_value == WorkStatus::ZANGYOU) &&
                ($new_value != WorkStatus::HOUDE && $new_value != WorkStatus::KYUUDE && $new_value != WorkStatus::ZANGYOU)) {
                $working_info->planned_overtime_start = null;
                $working_info->planned_overtime_end = null;

                // Updated 2019-01-04: only apply the rule "modify the planned_break/night_break_time when the planned_work_status_id has been changed" for "work_location only" EWI
                if ($working_info->planned_work_address_id == null) {
                    $working_info->planned_night_break_time = null;
                    $working_info->planned_break_time = ($working_info->planned_overtime_start) ? 0 : $working_info->planned_overtime_start;
                }
            }
        }

        // Check if planned_break/night_break_time was changed when the planned work status is houde, kyuude, zangyou. If yes, check if this EWI instance has a
        // WAWE or not, it yes set the new value to that WAWE instance's pocket_break/night_break_time
        $current_work_status = array_key_exists('planned_work_status_id', $working_info->getDirty()) ? $working_info->getDirty()['planned_work_status_id'] : $working_info->getOriginal('planned_work_status_id');
        if ($current_work_status == WorkStatus::HOUDE || $current_work_status == WorkStatus::KYUUDE || $current_work_status == WorkStatus::ZANGYOU) {
            if (array_key_exists('planned_break_time', $working_info->getDirty())) {
                if ($working_info->workAddressWorkingEmployee) {
                    $working_info->workAddressWorkingEmployee->pocket_break_time = $working_info->getDirty()['planned_break_time'];
                }
            }

            if (array_key_exists('planned_night_break_time', $working_info->getDirty())) {
                if ($working_info->workAddressWorkingEmployee) {
                    $working_info->workAddressWorkingEmployee->pocket_night_break_time = $working_info->getDirty()['planned_night_break_time'];
                }
            }

            if ($working_info->workAddressWorkingEmployee) {
                $working_info->workAddressWorkingEmployee->save();
            }
        }
    }

    /**
     * Listen to the created event of the an EmployeeWorkingInformation instance to check for confirm_needed error
     *
     * @param Eloquent $model
     * @return void
     */
    public function created(EmployeeWorkingInformation $working_info)
    {
        if ($working_info->evaluateChecklistErrorsOrNot() == true) {

            // As you can see, we have to perfom this check in both 'updated' and 'created' event of this model
            // But if this EmployeeWorkingInformation doesnt have any timestamps, then it will be checked for HAVE_SCHEDULE_BUT_OFFLINE error
            if ($working_info->timestamped_start_work_time === null && $working_info->timestamped_end_work_time === null && $working_info->takeAWholeDayOff() != true && $working_info->planned_work_status_id !== WorkStatus::KEKKIN && $working_info->planned_work_status_id !== WorkStatus::FURIKYUU && $working_info->planned_working_hour !== null) {
                event(new ConfimNeededHaveScheduleButOfflineError($working_info->employeeWorkingDay, $working_info->id, $working_info->employeeWorkingDay->employee->workLocation->company));
            }

            //    event(new PaidHolidayChanged($working_info->employeeWorkingDay->employee));
            // Now, instead of the above event we use this event
            if ($working_info->planned_rest_status_id) {
                event(new CachedPaidHolidayInformationBecomeOld($working_info->employeeWorkingDay));
            }
        }
    }

    /**
     * Listen to the deleting event of the an EmployeeWorkingInformation instance to void the HAVE_SCHEDULE_BUT_OFFLINE error
     *
     * @param Eloquent $model
     * @return void
     */
    public function deleting(EmployeeWorkingInformation $working_info)
    {
       

        // $this->resetConfirmNeededErrorRecordsOfASpecificType($working_info, $working_info->employeeWorkingDay, ChecklistItem::HAVE_SCHEDULE_BUT_OFFLINE);
        // $this->resetConfirmNeededErrorRecordsOfASpecificType($working_info, $working_info->employeeWorkingDay, ChecklistItem::WORK_WITHOUT_SCHEDULE);
        // $this->resetConfirmNeededErrorRecordsOfASpecificType($working_info, $working_info->employeeWorkingDay, ChecklistItem::DIFFERENCE_FROM_SCHEDULE);

        // Delete the checklist items (or errors)
        ChecklistItem::where('employee_working_information_id', $working_info->id)->delete();

        // Delete the timers too
        ChecklistErrorTimer::where('employee_working_information_id', $working_info->id)->delete();

        // Delete the cached model
        CachedEmployeeWorkingInformation::where('employee_working_information_id', $working_info->id)->delete();

        // Delete the snapshot, also, we have to take the id so that we can use it to delete the color statuses of those snapshots too
        $snapshot_ids = EmployeeWorkingInformationSnapshot::where('employee_working_information_id', $working_info->id)->pluck('id')->toArray();
        EmployeeWorkingInformationSnapshot::where('employee_working_information_id', $working_info->id)->orWhere('soon_to_be_EWI_id', $working_info->id)->delete();

        // Delete the color status of this employee working information and of the snapshots that this working information has
        ColorStatus::where(function($query) use ($working_info) {
            $query->where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION)->where('colorable_id', $working_info->id);
        })->orWhere(function($query) use ($snapshot_ids) {
            $query->where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION_SNAPSHOT)->whereIn('colorable_id', $snapshot_ids);
        })->delete();

        // event(new PaidHolidayChanged($working_info->employeeWorkingDay->employee));
        // Now, instead of the above event we use this event
        event(new CachedPaidHolidayInformationBecomeOld($working_info->employeeWorkingDay));
    }

    /**
     * Before being saved, remove some unnecessary attributes.
     */
    public function saving(EmployeeWorkingInformation $working_info)
    {
        if (isset($working_info->just_been_created)) unset($working_info->just_been_created);
    }
}
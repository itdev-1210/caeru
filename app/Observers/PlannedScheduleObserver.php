<?php

namespace App\Observers;

use App\PlannedSchedule;
use App\EmployeeWorkingInformation;
use App\Events\CachedEmployeeWorkingInformationBecomeOld;
use App\Events\CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld;
use App\Services\ScheduleProcessingService;
use App\Services\NationalHolidayService;
use App\Events\DistributeTimestampsAndReevaluateChecklistErrors;
use App\Events\WorkingTimestampChangedOnManyWorkingDays;
use Carbon\Carbon;

class PlannedScheduleObserver
{

    /**
     * The schedule processing service instance
     */
    private $service;

    /**
     * Constructor of this observer
     */
    public function __construct(ScheduleProcessingService $service)
    {
        $this->service = $service;
    }

    /**
     * When a planned schedule is being created, if the employee or work address, associating with this planned schedule, does not have any
     * planned schedule yet, initialize the working days for that employee or work address
     *
     * @param PlannedSchedule $schedule
     * @return void
     */
    public function creating(PlannedSchedule $schedule)
    {
        $period = $this->service->getDatePeriod($schedule);

        $employee = $schedule->employee;

        $this->service->initializeWorkingDay($employee, $period);

        $work_address = $schedule->workAddress;

        if ($work_address)
            $this->service->initializeWorkingDay($work_address, $period);

    }

    /**
     * When a PlannedSchedule is created, create all the coresponding WorkingInformations (for Employee or for Work Address)
     *
     * @param PlannedSchedule $schedule
     * @return void
     */
    public function created(PlannedSchedule $schedule)
    {

        // If this schedule is related to a work address...
        if ($schedule->work_address_id) {

            $this->service->initializeWorkAddressWorkingInformationsMatchedTimeRange($schedule);

        } else {

            $this->service->initializeEmployeeWorkingInformationsMatchedTimeRange($schedule);

        }
    }

    /**
     * When a PlannedSchedule is being deleted, delete all the relating working information(employee's or work address's) and/or work address's working employee instances.
     * But even before that, we need to wrap-up all the data that the working info is depending on the planned schedule for.
     *
     * @param PlannedSchedule $schedule
     * @return void
     */
    public function deleting(PlannedSchedule $schedule)
    {
        $all_relating_employee_working_day_ids = $schedule->employeeWorkingInformations->pluck('employee_working_day_id');

        $remaining_employee_working_info_ids = $this->service->deleteRelatingModels($schedule, null, true);

        $remaining_infos = EmployeeWorkingInformation::with('employeeWorkingDay')->whereIn('id', $remaining_employee_working_info_ids)->get();

        // Get the id of the working days that were affected by this deletion
        $employee_working_day_ids_of_remaining_infos = $remaining_infos->pluck('employee_working_day_id');
        $working_days_that_need_to_be_redistribute_timestamp = $all_relating_employee_working_day_ids->diff($employee_working_day_ids_of_remaining_infos);

        $national_holidays_service = resolve(NationalHolidayService::class);
        $national_holidays = $national_holidays_service->get($schedule->start_date, $schedule->end_date);

        foreach($remaining_infos as $working_info) {
            $this->wrapUp($working_info, $schedule, $national_holidays);
        }

        // Fire events to re-distribute working timestamp and check errors for those affected working days
        event(new WorkingTimestampChangedOnManyWorkingDays($working_days_that_need_to_be_redistribute_timestamp->toArray()));
        event(new DistributeTimestampsAndReevaluateChecklistErrors($working_days_that_need_to_be_redistribute_timestamp->toArray()));
    }

    /**
     * When a PlannedSchedule is being updated, if its time range is changed, then delete all the related Models and initialize new ones
     *
     * @param PlannedSchedule $schedule
     * @return void
     */
    public function updated(PlannedSchedule $schedule)
    {
        // Check the date period again to create some new EmployeeWorkingDays
        $period = $this->service->getDatePeriod($schedule);
        $employee = $schedule->employee;
        $this->service->initializeWorkingDay($employee, $period);

        // If the schedule has WorkAddress in it, we should initialize the WorkAddressWorkingDay too.
        $work_address = $schedule->workAddress;
        if ($work_address)
            $this->service->initializeWorkingDay($work_address, $period);


        if (($this->standardizedDate($schedule->start_date) != $schedule->getOriginal('start_date')) ||
            ($this->standardizedDate($schedule->end_date) != $schedule->getOriginal('end_date')) ||
            ($this->standardizedDaysOfWeek($schedule->working_days_of_week) != $schedule->getOriginal('working_days_of_week')) ||
            ($schedule->rest_on_holiday != $schedule->getOriginal('rest_on_holiday')) ||
            ($schedule->prioritize_company_calendar != $schedule->getOriginal('prioritize_company_calendar')) ||
            ($schedule->work_location_id != $schedule->getOriginal('work_location_id')) ||
            ($schedule->work_address_id != $schedule->getOriginal('work_address_id')) ||
            ($schedule->candidating_type != $schedule->getOriginal('candidating_type')) ||
            ($schedule->work_time != $schedule->getOriginal('work_time')))
        {
            $have_ewi_deleted_working_days = $this->service->deleteRelatingModels($schedule);

            // Proceed to re-distribute the WorkingTimestamp of these EmployeeWorkingDays
            // event(new DistributeTimestampsAndReevaluateChecklistErrors($have_ewi_deleted_working_days->pluck('id')->toArray()));

            if ($schedule->work_address_id) {
                $this->service->initializeWorkAddressWorkingInformationsMatchedTimeRange($schedule, $have_ewi_deleted_working_days);
            } else {
                $this->service->initializeEmployeeWorkingInformationsMatchedTimeRange($schedule, $have_ewi_deleted_working_days);
            }

        } else {
            // In this case, all the relating employee_working_info will have their cached values refreshed.
            if ($schedule->work_address_id) {
                $have_ewi_deleted_working_days = $this->service->deleteRelatingModels($schedule);

                $this->service->initializeWorkAddressWorkingInformationsMatchedTimeRange($schedule, $have_ewi_deleted_working_days);
                $relating_work_address_working_employees = $schedule->workAddressWorkingEmployees->filter(function($working_employee) use($schedule) {return $working_employee->employee_id === $schedule->employee_id;});
                $relating_employee_working_infos = $relating_work_address_working_employees->map(function($working_employee) {if ($working_employee->employeeWorkingInformation) return $working_employee->employeeWorkingInformation->id;})->toArray();

                // $relating_employee_working_infos = $schedule->workAddressWorkingEmployees->map(function($working_employee) {if ($working_employee->employeeWorkingInformation) return $working_employee->employeeWorkingInformation->id;})->toArray();

            } else {
                $relating_employee_working_infos = $schedule->employeeWorkingInformations->pluck('id')->toArray();
                event(new CachedEmployeeWorkingInformationBecomeOld($relating_employee_working_infos));
            }

        }

        // Dispatch event to reset the PaidHolidayInformation' cache of this employee
        event(new CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld([$employee->id]));
;    }

    /**
     * Standardize the date string. Because the new date string maybe various in format ('Y-m-d', 'Y/m/d', 'Ymd', ...)
     *
     * @param string $date      the date string
     * @return string           convert the date string to format 'Y-m-d' (default format of database)
     */
    protected function standardizedDate($date)
    {
        if ($date) {
            $carbon = new Carbon($date);
            return $carbon->format('Y-m-d');
        }
        return null;
    }

    /**
     * Standardize the the days_of_the_weeks data. So that we can compare.
     *
     * @param array $day_of_week        this is the default data type come right from the request. An array of booleans for each day of the week.
     * @return string                   implode that array, use a comma (',') as glue.
     */
    protected function standardizedDaysOfWeek($days_of_week)
    {
        return implode(',', $days_of_week);
    }

    /**
     * When a PlannedSchedule is about to be deleted, some of the EmployeeWorkingInformation will be deleted, some will not.
     * And we have to wrap-data(make them independant from this PlannedSchedule) for those remaining.
     *
     * @param EmployeeWorkingInformation    $working_info
     * @param PlannedSchedule               $schedule
     * @param collection                    $national_holidays
     * @return void
     */
    protected function wrapUp($working_info, $schedule, $national_holidays)
    {
        // Wrap-up schedule data
        if ($working_info->getOriginal('schedule_start_work_time') === null && $schedule->start_work_time !== null) {
            $working_info->schedule_start_work_time = $working_info->employeeWorkingDay->date . ' ' . $schedule->start_work_time;
        }
        if ($working_info->getOriginal('schedule_end_work_time') === null && $schedule->end_work_time !== null) {
            $working_info->schedule_end_work_time = $working_info->employeeWorkingDay->date . ' ' . $schedule->end_work_time;
        }
        if ($working_info->getOriginal('schedule_break_time') === null && $schedule->break_time !== null) {
            $working_info->schedule_break_time = $schedule->break_time;
        }
        if ($working_info->getOriginal('schedule_night_break_time') === null && $schedule->night_break_time !== null) {
            $working_info->schedule_night_break_time = $schedule->night_break_time;
        }
        if ($working_info->getOriginal('schedule_working_hour') === null && $schedule->working_hour !== null) {
            $working_info->schedule_working_hour = $schedule->working_hour;
        }
        if ($working_info->getOriginal('planned_work_location_id') === null && $schedule->work_location_id !== null) {
            $working_info->planned_work_location_id = $schedule->work_location_id;
        }
        if ($working_info->getOriginal('planned_work_address_id') === null && $schedule->work_address_id !== null) {
            $working_info->planned_work_address_id = $schedule->work_address_id;
        }


        // Wrap-up salary data
        if ($working_info->getOriginal('basic_salary') === null) {
            if ($national_holidays->contains($working_info->employeeWorkingDay->date)) {
                if ($schedule->holiday_salary !== null) {
                    $working_info->basic_salary = $schedule->holiday_salary;
                }

            } else {
                if ($schedule->normal_salary !== null) {
                    $working_info->basic_salary = $schedule->normal_salary;
                }
            }
        }
        if ($working_info->getOriginal('night_salary') === null) {
            if ($national_holidays->contains($working_info->employeeWorkingDay->date)) {
                if ($schedule->holiday_night_salary !== null) {
                    $working_info->night_salary = $schedule->holiday_night_salary;
                }

            } else {
                if ($schedule->normal_night_salary !== null) {
                    $working_info->night_salary = $schedule->normal_night_salary;
                }
            }
        }
        if ($working_info->getOriginal('overtime_salary') === null) {
            if ($national_holidays->contains($working_info->employeeWorkingDay->date)) {
                if ($schedule->holiday_overtime_salary !== null) {
                    $working_info->overtime_salary = $schedule->holiday_overtime_salary;
                }

            } else {
                if ($schedule->normal_overtime_salary !== null) {
                    $working_info->overtime_salary = $schedule->normal_overtime_salary;
                }
            }
        }
        if ($working_info->getOriginal('deduction_salary') === null) {
            if ($national_holidays->contains($working_info->employeeWorkingDay->date)) {
                if ($schedule->holiday_deduction_salary !== null) {
                    $working_info->deduction_salary = $schedule->holiday_deduction_salary;
                }

            } else {
                if ($schedule->normal_deduction_salary !== null) {
                    $working_info->deduction_salary = $schedule->normal_deduction_salary;
                }
            }
        }
        if ($working_info->getOriginal('night_deduction_salary') === null) {
            if ($national_holidays->contains($working_info->employeeWorkingDay->date)) {
                if ($schedule->holiday_night_deduction_salary !== null) {
                    $working_info->night_deduction_salary = $schedule->holiday_night_deduction_salary;
                }

            } else {
                if ($schedule->normal_night_deduction_salary !== null) {
                    $working_info->night_deduction_salary = $schedule->normal_night_deduction_salary;
                }
            }
        }


        // Wrap-up traffic expenses
        if ($working_info->getOriginal('monthly_traffic_expense') === null && $schedule->monthly_traffic_expense !== null) {
            $working_info->monthly_traffic_expense = $schedule->monthly_traffic_expense;
        }
        if ($working_info->getOriginal('daily_traffic_expense') === null && $schedule->daily_traffic_expense !== null) {
            $working_info->daily_traffic_expense = $schedule->daily_traffic_expense;
        }

        // At this point, it's unlikely that this working info still have temporary = true, but just to be on the safe side.
        $working_info->temporary = false;
        $working_info->save();
    }
}
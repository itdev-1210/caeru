<?php

namespace App\Observers;

use App\ColorStatus;
use App\SubstituteEmployeeWorkingInformation;

class SubstituteEmployeeWorkingInformationObserver
{

    /**
     * When delete a substitute, have to cascade delete some relating models
     *
     * @return void
     */
    public function deleting(SubstituteEmployeeWorkingInformation $substitute)
    {
        // Cascade delete the color statuses
        ColorStatus::where('colorable_type', ColorStatus::SUBSTITUTE_EMPLOYEE_WORKING_INFORMATION)->where('colorable_id', $substitute->id)->delete();
    }
}
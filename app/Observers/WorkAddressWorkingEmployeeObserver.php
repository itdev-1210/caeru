<?php

namespace App\Observers;

use App\WorkAddressWorkingEmployee;

class WorkAddressWorkingEmployeeObserver
{

    /**
     * Listen to the deleting event of the an WorkAddressWorkingEmployee instance.
     *
     * @param Eloquent $model
     * @return void
     */
    public function deleting(WorkAddressWorkingEmployee $working_employee)
    {
        if ($working_employee->employeeWorkingInformation) $working_employee->employeeWorkingInformation->delete();
    }
}
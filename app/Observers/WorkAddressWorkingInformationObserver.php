<?php

namespace App\Observers;

use App\WorkAddressWorkingInformation;

class WorkAddressWorkingInformationObserver
{

    /**
     * Listen to the deleting event of the an WorkAddressWorkingInformation instance.
     *
     * @param Eloquent $model
     * @return void
     */
    public function deleting(WorkAddressWorkingInformation $working_info)
    {
        foreach($working_info->workAddressWorkingEmployees as $working_employee) {
            $working_employee->delete();
        };
    }
}
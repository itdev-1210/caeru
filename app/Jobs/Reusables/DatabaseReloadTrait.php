<?php

namespace App\Jobs\Reusables;

use Config;
use DB;

trait DatabaseReloadTrait
{
    /**
     * Change the database to a specific company database.
     * We have to change the connection settings accordingly.
     *
     * @param   string      $company_code   the unique code of a company, default will be 'main'.
     * @return  void
     */
    private function changeDatabaseConnectionTo($company_code)
    {
        DB::purge('sub');

        Config::set('database.default', 'sub');

        Config::set('database.connections.sub.database', config('database.connections.sub.default_prefix') . $company_code);
        Config::set('database.connections.sub.prefix', config('database.connections.sub.default_prefix') . $company_code . '_');

        DB::reconnect('sub');
    }
}
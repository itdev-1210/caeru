<?php

namespace App\Jobs\Reusables;

use App\Services\ChangeWorkTimePerDayService;
use App\Services\EvaluateChecklistErrorsService;
use App\Services\ManagerAndEmployeeService;
use App\Services\NationalHolidayService;
use App\Services\ScheduleProcessingService;
use App\Services\TimestampesAndWorkingDayConnectorService;
use App\Services\TimezonesService;
use App\Services\TodofukenService;
use App\Services\WorkLocationSettingService;

trait ServiceReloadTrait
{
    /**
     * Rebound all these services to ensure the singleton instances have up-to-date settings.
     * Since the queue worker is persistent process, all neccessary singleton instance need to be rebounded, so that their settings is fresh.
     *
     * @return void
     */
    protected function reloadApplicationsSingletonServices()
    {
        \App::singleton(ChangeWorkTimePerDayService::class, function ($app) {
            return new ChangeWorkTimePerDayService();
        });
        \App::singleton(EvaluateChecklistErrorsService::class, function ($app) {
            return new EvaluateChecklistErrorsService();
        });
        \App::singleton(ManagerAndEmployeeService::class, function ($app) {
            return new ManagerAndEmployeeService();
        });
        \App::singleton(NationalHolidayService::class, function ($app) {
            return new NationalHolidayService();
        });
        \App::singleton(ScheduleProcessingService::class, function ($app) {
            return new ScheduleProcessingService();
        });
        \App::singleton(TimestampesAndWorkingDayConnectorService::class, function ($app) {
            return new TimestampesAndWorkingDayConnectorService();
        });
        \App::singleton(TimezonesService::class, function ($app) {
            return new TimezonesService();
        });
        \App::singleton(TodofukenService::class, function ($app) {
            return new TodofukenService();
        });
        \App::singleton(WorkLocationSettingService::class, function ($app) {
            return new WorkLocationSettingService();
        });
    }
}
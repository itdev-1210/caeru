<?php

namespace App\Jobs;

use DB;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\EmployeeWorkingInformation;
use App\Services\EvaluateChecklistErrorsService;
use App\Jobs\Reusables\DatabaseReloadTrait;
use App\Jobs\Reusables\ServiceReloadTrait;

class ReevaluateChecklistErrorsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, DatabaseReloadTrait, ServiceReloadTrait;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The company of this job
     */
    public $company_code;

    /**
     * List of EmployeeWorkingInformations id
     */
    public $employee_working_information_ids;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($working_info_ids, $company_code)
    {
        $this->employee_working_information_ids = collect($working_info_ids);
        $this->company_code = $company_code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->processEvaluating($this->employee_working_information_ids->toArray());
    }

    /**
     * Proceed to evaluate checklist errors for the given EmployeeWorkingInformation.
     *
     * @param array
     * @return void
     */
    protected function processEvaluating($working_info_ids)
    {
        $this->changeDatabaseConnectionTo($this->company_code);

        // Reload all the necessary singleton services
        $this->reloadApplicationsSingletonServices();

        $evaluate_service = resolve(EvaluateChecklistErrorsService::class);

        $employee_working_informations = EmployeeWorkingInformation::whereIn('id', $working_info_ids)->notConcluded()->get();

        foreach($employee_working_informations as $working_info) {
            $evaluate_service->evaluateChecklistErrorsFromAnInstanceOfEmployeeWorkingInformation($working_info);
        }
    }
}

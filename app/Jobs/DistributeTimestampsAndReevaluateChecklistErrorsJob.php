<?php

namespace App\Jobs;

use DB;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use App\CachedEmployeeWorkingInformation;
use App\Services\EvaluateChecklistErrorsService;
use App\Jobs\Reusables\DatabaseReloadTrait;
use App\Jobs\Reusables\ServiceReloadTrait;

class DistributeTimestampsAndReevaluateChecklistErrorsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, DatabaseReloadTrait, ServiceReloadTrait;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The company of this job
     */
    public $company_code;

    /**
     * List of EmployeeWorkingDay ids
     */
    public $employee_working_day_ids;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($working_day_ids, $company_code)
    {
        $this->employee_working_day_ids = $working_day_ids;
        $this->company_code = $company_code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->distributeAndReevaluate($this->employee_working_day_ids);
    }

    /**
     * Distribute WorkingTimestamps and Re-evaluate checklist error on the given EmployeeWorkingDays
     *
     * @param array
     * @return void
     */
    protected function distributeAndReevaluate($working_day_ids)
    {
        $this->changeDatabaseConnectionTo($this->company_code);

        // Reload all the necessary singleton services
        $this->reloadApplicationsSingletonServices();

        $evaluate_service = resolve(EvaluateChecklistErrorsService::class);

        $evaluate_service->evaluateChecklistErrorFromWorkingTimestampListOfManyWorkingDay($working_day_ids);

        $employee_working_information_ids = EmployeeWorkingInformation::whereIn('employee_working_day_id', $working_day_ids)->pluck('id')->toArray();

        // Then we have to mark the cache of EWIs of those day as 'old'
        CachedEmployeeWorkingInformation::whereIn('employee_working_information_id', $employee_working_information_ids)->update([
            'old' => true,
        ]);
    }
}

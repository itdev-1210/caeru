<?php

namespace App\Jobs;

use DB;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Reusables\PaidHolidayInformationTrait;
use App\Jobs\Reusables\DatabaseReloadTrait;
use App\Jobs\Reusables\ServiceReloadTrait;
use App\Employee;


class ProcessPaidHolidayChangedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use PaidHolidayInformationTrait, DatabaseReloadTrait, ServiceReloadTrait;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * List of EmployeeWorkingInformations id
     */
    public $employee;

    /**
     * The company of this job
     */
    public $company_code;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employee, $company_code)
    {
        $this->employee = $employee;
        $this->company_code = $company_code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->processCycle();
    }

    /**
     * Proceed handle the whole cycle of an employee.
     *
     * @param array
     * @return void
     */
    protected function processCycle()
    {
        $this->changeDatabaseConnectionTo($this->company_code);

        // Reload all the necessary singleton services
        $this->reloadApplicationsSingletonServices();

        $this->updatePaidHolidayInformation($this->employee);

    }
}

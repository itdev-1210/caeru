<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Reusables\DatabaseReloadTrait;
use App\Jobs\Reusables\ServiceReloadTrait;
use App\Employee;
use App\WorkingTimestamp;
use App\TabletTemporaryWorkingTimestamp;
use App\Services\WorkLocationSettingService;
use App\Services\TimestampesAndWorkingDayConnectorService;
use Illuminate\Support\Facades\Log;
use DB;
use Exception;

class SendTabletTemporaryWorkingTimestampsToConnectorServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, DatabaseReloadTrait, ServiceReloadTrait;

    /**
     * Company's code use to select database.
     */
    protected $company_code = null;

    /**
     * WorkLocation id.
     */
    protected $work_location_id = null;

    /**
     * WorkAddress id.
     */
    protected $work_address_id = null;

    /**
     * Request key use to select the correct TabletTemporaryWorkingInformation.
     */
    protected $request_key = null;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request_key, $company_code, $work_location_id, $work_address_id)
    {
        $this->request_key = $request_key;
        $this->company_code = $company_code;
        $this->work_location_id = $work_location_id;
        $this->work_address_id = $work_address_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Change database and prepare services
        $this->changeDatabaseConnectionTo($this->company_code);

        // Reload all the necessary singleton services
        $this->reloadApplicationsSingletonServices();

        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);

        // Get all the TabletTemporaryWorkingTimestamp that are currently stored in the temporary table to process
        $temporary_working_timestamps = TabletTemporaryWorkingTimestamp::where('request_key', $this->request_key)->get();

        $have_card_code_temporary_working_timestamp = $temporary_working_timestamps->filter(function($timestamp) {
            return $timestamp->card_code;
        });

        $not_have_card_code_temporary_working_timestamp = $temporary_working_timestamps->filter(function($timestamp) {
            return !$timestamp->card_code;
        });

        if ($have_card_code_temporary_working_timestamp->count() != 0)
            $this->processHaveCardCodeTemporaryWorkingTimestamps($have_card_code_temporary_working_timestamp, $work_location_setting_service, $connector_service);

        if ($not_have_card_code_temporary_working_timestamp->count() != 0)
            $this->processNotHaveCardCodeTemporaryWorkingTimestamps($not_have_card_code_temporary_working_timestamp, $work_location_setting_service, $connector_service);
    }

    /**
     * The function would be distribute from $temporary_working_timestamps to table WorkingTimestamp
     *
     * Note: This function only run the record which having card code
     *
     * @param App\TabletTemporaryWorkingTimestamp $temporary_working_timestamps
     * @param App\Services\WorkLocationSettingService $work_location_setting_service
     * @param App\Services\TimestampesAndWorkingDayConnectorService $connector_service
     * @return void
     */
    private function processHaveCardCodeTemporaryWorkingTimestamps($temporary_working_timestamps, $work_location_setting_service, $connector_service)
    {
        // Also prepare a hash table to lookup Employee
        $card_number_to_employee_hash_table = Employee::whereNotNull('card_number')->get()->keyBy('card_number');

        $current_employee = $card_number_to_employee_hash_table[$temporary_working_timestamps->first()['card_code']];

        $new_timestamps = collect([]);

        try {
            foreach($temporary_working_timestamps as $temp_timestamp) {

                if ($temp_timestamp['card_code'] != $current_employee->card_number) {
                    $connector_service->connectTimestampsToWorkingDaysOfEmployee($new_timestamps, $current_employee);
                    $new_timestamps = collect([]);
                    $current_employee = $card_number_to_employee_hash_table[$temp_timestamp["card_code"]];
                }

                $work_location_id = $this->work_location_id;

                if ($current_employee->all_timestamps_register_to_belong_work_location && empty($this->work_address_id))
                    $work_location_id = $current_employee->work_location_id;

                $new_timestamp = new WorkingTimestamp([
                    'enable'                => true,
                    'timestamped_value'     => $temp_timestamp["timestamped_value"],
                    'name_id'               => $work_location_setting_service->getWorkLocationById($work_location_id)->getTimezone()->name_id,
                    'timestamped_type'      => $temp_timestamp["timestamped_type"],
                    'registerer_type'       => WorkingTimestamp::TABLET,
                    'work_location_id'      => $work_location_id,
                    'created_at'            => $temp_timestamp['created_at'],
                    'updated_at'            => $temp_timestamp['updated_at'],
                ]);

                if (!empty($this->work_address_id)) {
                    $new_timestamp->work_address_id = $this->work_address_id;
                }

                $duplicated_timestamp_exist = $new_timestamps->first(function($timestamp) use ($new_timestamp) {
                    return $timestamp->timestamped_value == $new_timestamp->timestamped_value;
                });
                if ($duplicated_timestamp_exist == null) {
                    $new_timestamps->push($new_timestamp);
                }
            }

            // Need to do it for the last guy
            $connector_service->connectTimestampsToWorkingDaysOfEmployee($new_timestamps, $current_employee);

            // After everything is done delete the temporary WorkingTimestamp in the table
            TabletTemporaryWorkingTimestamp::where('request_key', $this->request_key)->delete();

        } catch (Exception $e) {
            $temp_timestamps_as_string = [];
            foreach ($temporary_working_timestamps as $temp_timestamp) {
                $temp_timestamps_as_string[] = '(' . implode(', ', $temp_timestamp) . ')';
            }
            $total_data_string = implode(",\n", $temp_timestamps_as_string);
            Log::error("||**********");
            Log::error('Job "SendTabletTemporaryWorkingTimestampsToConnectorService" was not handled properly. Request key is: ' . $this->request_key . '.');
            Log::error("==========");
            Log::error("These are the failed data:\n" . $total_data_string);
            Log::error("==========");
            Log::error("Stack trace:\n");
            Log::error($e->getTraceAsString());
            Log::error("**********||");
            $this->fail($e);
        }
    }

    /**
     * The function would be distribute from $temporary_working_timestamps to table WorkingTimestamp
     *
     * Note: This function only run the record which not having card code
     *
     * @param App\TabletTemporaryWorkingTimestamp $temporary_working_timestamps
     * @param App\Services\WorkLocationSettingService $work_location_setting_service
     * @param App\Services\TimestampesAndWorkingDayConnectorService $connector_service
     * @return void
     */
    private function processNotHaveCardCodeTemporaryWorkingTimestamps($temporary_working_timestamps, $work_location_setting_service, $connector_service)
    {
        // Also prepare a hash table to lookup Employee
        $device_id_to_employee_hash_table = Employee::whereNotNull('device_id')->get()->keyBy('device_id');

        // Sort by card_code, effectively by employee
        $temporary_working_timestamps = $temporary_working_timestamps->sortBy('device_id');

        $current_employee = $device_id_to_employee_hash_table[$temporary_working_timestamps->first()['device_id']];

        $new_timestamps = [];

        try {
            foreach($temporary_working_timestamps as $temp_timestamp) {

                if ($temp_timestamp['device_id'] != $current_employee->device_id) {
                    $connector_service->connectTimestampsToWorkingDaysOfEmployee($new_timestamps, $current_employee);
                    $new_timestamps = [];
                    $current_employee = $device_id_to_employee_hash_table[$temp_timestamp["device_id"]];
                }

                $work_location_id = $this->work_location_id;

                if ($current_employee->all_timestamps_register_to_belong_work_location && empty($this->work_address_id))
                    $work_location_id = $current_employee->work_location_id;

                $new_timestamp = new WorkingTimestamp([
                    'enable'                => true,
                    'timestamped_value'     => $temp_timestamp["timestamped_value"],
                    'name_id'               => $work_location_setting_service->getWorkLocationById($work_location_id)->getTimezone()->name_id,
                    'timestamped_type'      => $temp_timestamp["timestamped_type"],
                    'registerer_type'       => WorkingTimestamp::MOBILE_APP,
                    'work_location_id'      => $work_location_id,
                    'latitude'              => $temp_timestamp["latitude"],
                    'longitude'             => $temp_timestamp["longitude"],
                    'created_at'            => $temp_timestamp['created_at'],
                    'updated_at'            => $temp_timestamp['updated_at'],
                ]);

                if (!empty($this->work_address_id)) {
                    $new_timestamp->work_address_id = $this->work_address_id;
                }

                $new_timestamps[] = $new_timestamp;
            }

            // Need to do it for the last guy
            $connector_service->connectTimestampsToWorkingDaysOfEmployee($new_timestamps, $current_employee);

            // After everything is done delete the temporary WorkingTimestamp in the table
            TabletTemporaryWorkingTimestamp::where('request_key', $this->request_key)->delete();

        } catch (Exception $e) {
            $temp_timestamps_as_string = [];
            foreach ($temporary_working_timestamps as $temp_timestamp) {
                $temp_timestamps_as_string[] = '(' . implode(', ', $temp_timestamp) . ')';
            }
            $total_data_string = implode(",\n", $temp_timestamps_as_string);
            Log::error("||**********");
            Log::error('Job "SendTabletTemporaryWorkingTimestampsToConnectorService" was not handled properly. Request key is: ' . $this->request_key . '.');
            Log::error("==========");
            Log::error("These are the failed data:\n" . $total_data_string);
            Log::error("==========");
            Log::error("Stack trace:\n");
            Log::error($e->getTraceAsString());
            Log::error("**********||");
            $this->fail($e);
        }
    }
}

<?php

namespace App\Jobs;

use DB;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Reusables\PaidHolidayInformationTrait;
use App\Jobs\Reusables\DatabaseReloadTrait;
use App\Jobs\Reusables\ServiceReloadTrait;

class ProcessPaidHolidayKomokuChangedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use PaidHolidayInformationTrait, DatabaseReloadTrait, ServiceReloadTrait;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * List of EmployeeWorkingInformations id
     */
    public $work_location_id;

    /**
     * The company of this job
     */
    public $company_code;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($work_location_id, $company_code)
    {
        $this->work_location_id = $work_location_id;
        $this->company_code = $company_code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->processCycleOfAllEmpoyee();
    }

    /**
     * Proceed handle the whole cycle of all employee.
     *
     * @param array
     * @return void
     */
    protected function processCycleOfAllEmpoyee()
    {
        $this->changeDatabaseConnectionTo($this->company_code);

        // Reload all the necessary singleton services
        $this->reloadApplicationsSingletonServices();

        $this->updatePaidHolidayInformationvsWorkLocation($this->work_location_id);

    }
}

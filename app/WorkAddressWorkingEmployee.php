<?php

namespace App;

use Carbon\Carbon;

class WorkAddressWorkingEmployee extends Model
{

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'schedule_break_time',
        'schedule_night_break_time',
        'planned_work_status_id',
        'planned_rest_status_id',
        'timestamped_start_work_time',
        'timestamped_end_work_time',
        'timestamps_exist_or_not'
    ];

    /**
     * Get the work address working information instance of this work address working employee instance
     */
    public function workAddressWorkingInformation()
    {
        return $this->belongsTo(WorkAddressWorkingInformation::class);
    }

    /**
     * Get the planned schedule instance of this working information instance
     */
    public function plannedSchedule()
    {
        return $this->belongsTo(PlannedSchedule::class);
    }

    /**
     * Get the employee working information instance of this work address working employee instance
     */
    public function employeeWorkingInformation()
    {
        return $this->hasOne(EmployeeWorkingInformation::class);
    }

    /**
     * Get the employee of this work address working employee instance
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * Get the employee working day of this work address working employee instance
     */
    public function employeeWorkingDay()
    {
        return $this->belongsTo(EmployeeWorkingDay::class);
    }

    //////// Query Scope ///////

    /**
     * Get all the WorkAddressWorkingEmployee which does not have EmployeeWorkingInformation that has been manually modified and
     * also does not have WorkAddressWorkingInformation that has been manually modified
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHavingScheduleModified($query)
    {
        return
            $query->where('schedule_modified', false);
    }

    /**
     * Get all the WorkAddressWorkingEmployee which does not have EmployeeWorkingInformation that has been concluded
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHavingConcludedWorkingInformations($query)
    {
        return $query->whereHas('employeeWorkingDay', function($query) {
            $query->where('concluded_level_one', false)->where('concluded_level_two', false);
        });
    }

    /**
     * Get all the WorkAddressWorkingEmployee which has EmployeeWorkingInformation that still does not have any WorkingTimestamp
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHavingContainingTimestampWorkingInformation($query)
    {
        return $query->whereHas('employeeWorkingInformation', function($query) {
            $query->whereNull('timestamped_start_work_time')->whereNull('timestamped_end_work_time');
        });
    }

    ////////////////////////////

    /**
     * Accessor of schedule_break_time
     *
     * @return string
     */
    public function getScheduleBreakTimeAttribute($value)
    {
        if ($this->employeeWorkingInformation) {
            return ($this->employeeWorkingInformation->planned_work_status_id == WorkStatus::HOUDE) ||
                ($this->employeeWorkingInformation->planned_work_status_id == WorkStatus::KYUUDE) ?
                $this->employeeWorkingInformation->planned_break_time:
                $this->employeeWorkingInformation->schedule_break_time;
        } else {
            return $this->pocket_break_time;
        }
    }

    /**
     * Accessor of pocket_break_time
     *
     * @return string
     */
    public function getPocketBreakTimeAttribute($value)
    {
        return $value == config('caeru.empty') ? null : $value;
    }

    /**
     * Accessor of schedule_night_break_time
     *
     * @return string
     */
    public function getScheduleNightBreakTimeAttribute()
    {
        if ($this->employeeWorkingInformation) {
            return ($this->employeeWorkingInformation->planned_work_status_id == WorkStatus::HOUDE) ||
                ($this->employeeWorkingInformation->planned_work_status_id == WorkStatus::KYUUDE) ?
                $this->employeeWorkingInformation->planned_night_break_time :
                $this->employeeWorkingInformation->schedule_night_break_time;
        } else {
            return $this->pocket_night_break_time;
        }
    }

    /**
     * Accessor of pocket_night_break_time
     *
     * @return string
     */
    public function getPocketNightBreakTimeAttribute($value)
    {
        return $value == config('caeru.empty') ? null : $value;
    }

    /**
     * Accessor of planned_work_status_id
     *
     * @return string
     */
    public function getPlannedWorkStatusIdAttribute($value)
    {
        return ($this->employeeWorkingInformation) ? $this->employeeWorkingInformation->planned_work_status_id : null;
    }

    /**
     * Accessor of planned_rest_status_id
     *
     * @return string
     */
    public function getPlannedRestStatusIdAttribute($value)
    {
        return ($this->employeeWorkingInformation) ? $this->employeeWorkingInformation->planned_rest_status_id : null;
    }


    /**
     * This is a function to get schedule working hour of this instance.
     * Do NOT change it to a virtual attribute because it will force laravel to calculate that attribute whenever a WorkAddressWorkingEmployee instance get initialized, and that will make and infinite loop(because
     * the initialization of WorkAddressWorkingInformation is depend on WorkAddressWorkingEmployee).
     *
     * @return string
     */
    public function getScheduleWorkingHour()
    {
        return ($this->employeeWorkingInformation) ?  $this->employeeWorkingInformation->schedule_working_hour : null;
    }

    /**
     * Get the timestamped_start_work_time from the EmployeeWorkingInformation(if any) associating with this WorkAddressWorkingEmployee
     *
     * @return string
     */
    public function getTimestampedStartWorkTimeAttribute()
    {
        return ($this->employeeWorkingInformation) ?  $this->employeeWorkingInformation->timestamped_start_work_time : null;
    }

    /**
     * Get the timestamped_end_work_time from the EmployeeWorkingInformation(if any) associating with this WorkAddressWorkingEmployee
     *
     * @return string
     */
    public function getTimestampedEndWorkTimeAttribute()
    {
        return ($this->employeeWorkingInformation) ?  $this->employeeWorkingInformation->timestamped_end_work_time : null;
    }

    /**
     * A boolean attribute to determine if this WorkAddressWorkingEmployee has timestamp or not.
     *
     * @return boolean
     */
    public function getTimestampsExistOrNotAttribute()
    {
        if ($this->employeeWorkingInformation) {
            return $this->employeeWorkingInformation->timestamped_start_work_time != null || $this->employeeWorkingInformation->timestamped_end_work_time != null;
        }

        return false;
    }

    /**
     * Convert a given number of minutes to a time string. Format: 'hh:mm'
     *
     * @param int       $minutes
     * @return string
     */
    protected function minutesToString($minutes)
    {
        if ($minutes <= 0) {
            return '00:00:00';
        } else {
            return str_pad(floor($minutes/60), 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes%60, 2, '0', STR_PAD_LEFT) . ':00';
        }
    }
}

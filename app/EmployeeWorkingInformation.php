<?php

namespace App;

use Carbon\Carbon;
use App\Reusables\HaveColorStatusTrait;
use App\Services\NationalHolidayService;
use App\Services\WorkLocationSettingService;
use App\Scopes\NotShowWhenTemporaryScope;

class EmployeeWorkingInformation extends Model
{
    use HaveColorStatusTrait;

    /**
     * Constants for types of modify person
     */
    const MODIFY_PERSON_TYPE_MANAGER    = 1;
    const MODIFY_PERSON_TYPE_EMPLOYEE   = 2;

    /**
     * Turn this on when you want to disable the checklist evaluation (and also the calculation of those attributes) in this model's events.
     * Remember to turn it back off.
     */
    protected $temporary_turn_off_checklist_errors_evaluation_in_events = false;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['date_upper_limit', 'day_of_the_upper_limit', 'last_modify_person_name'];

    /**
     * The WorkLocations that this instance is currently affected by.
     */
    protected $affected_by_work_location;
    protected $affected_by_real_work_location;

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->affected_by_work_location = null;
        $this->affected_by_real_work_location = null;
        $this->temporary_turn_off_checklist_errors_evaluation_in_events = false;
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new NotShowWhenTemporaryScope);
    }

    /**
     * Get the working day instance of this working information instance
     */
    public function employeeWorkingDay()
    {
        return $this->belongsTo(EmployeeWorkingDay::class);
    }

    /**
     * Get the planned schedule instance of this working information instance
     */
    public function plannedSchedule()
    {
        return $this->belongsTo(PlannedSchedule::class);
    }

    /**
     * Get the snapshot instance
     */
    public function employeeWorkingInformationSnapshot()
    {
        return $this->hasOne(EmployeeWorkingInformationSnapshot::class);
    }

    /**
     * Get the end working timestamp
     */
    public function endWorkingTimestamp()
    {
        return $this->hasOne(WorkingTimestamp::class, 'id', 'timestamped_end_work_time_working_timestamp_id');
    }
    /**
     * The function will change snapshot's attribute follow working information's attribute, if it isn't same.
     */
    public function mergeWorkingInformationToSnapshot($is_snapshot_trashed = false, $snapshot = null)
    {
        $array_attribute_name = [
            'timestamped_start_work_time_work_location_id',
            'timestamped_start_work_time_work_address_id',
            'timestamped_end_work_time_work_location_id',
            'timestamped_end_work_time_work_address_id',
            'paid_rest_time_start',
            'paid_rest_time_end',
            'paid_rest_time_period',
            'not_include_break_time_when_display_planned_time',
            'planned_work_location_id',
            'planned_work_address_id',
            'real_work_location_id',
            'real_work_address_id',
            'planned_early_arrive_start',
            'real_early_arrive_start',
            'planned_early_arrive_end',
            'real_early_arrive_end',
            'planned_work_span_start',
            'real_work_span_start',
            'planned_work_span_end',
            'real_work_span_end',
            'planned_overtime_start',
            'real_overtime_start',
            'planned_overtime_end',
            'real_overtime_end',
            'planned_work_span',
            'real_work_span',
            'planned_break_time',
            'real_break_time',
            'planned_night_break_time',
            'real_night_break_time',
            'planned_late_time',
            'real_late_time',
            'planned_early_leave_time',
            'real_early_leave_time',
            'planned_go_out_time',
            'real_go_out_time',
        ];

        if (!$is_snapshot_trashed) {
            if (!isset($snapshot))
                $snapshot = $this->employeeWorkingInformationSnapshot ?? new EmployeeWorkingInformationSnapshot();
        } else {
            if (!$snapshot) $snapshot = new EmployeeWorkingInformationSnapshot();
            $snapshot->isTrashed = true;
        }

        $snapshot->left_requester_note = $snapshot->left_requester_note ?? null;
        $snapshot->temporary = $this->temporary;
        if ($snapshot->temporary && !isset($snapshot->left_soon_to_be_absorb)) $snapshot->left_soon_to_be_absorb = false;

        $start_work_time = ($this->timestamped_start_work_time) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->getOriginal('timestamped_start_work_time')) : null;
        $end_work_time = ($this->timestamped_end_work_time) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->getOriginal('timestamped_end_work_time')) : null;

        $snapshot->employee_working_day_id = $this->employee_working_day_id;
        $snapshot->employee_working_information_id = $this->id;

        $target_absorb_EWI = isset($snapshot->target_absorb_EWI_id) ? EmployeeWorkingInformation::find($snapshot->target_absorb_EWI_id) : null;

        if (in_array($snapshot->left_status, [EmployeeWorkingInformationSnapshot::CONSIDERING])
            && $snapshot->left_work_status_id == WorkStatus::FURIKYUU && $snapshot->right_work_status_id != WorkStatus::FURIKYUU) {
            $transfer_with_EWI_id = ($snapshot->left_status == EmployeeWorkingInformationSnapshot::APPROVED)
                ? EmployeeWorkingInformation::where('transfer_with_EWI_id', $this->id)->first() : $this;

            $snapshot->right_schedule_start_work_time = $transfer_with_EWI_id->schedule_start_work_time;
            $snapshot->right_schedule_end_work_time = $transfer_with_EWI_id->schedule_end_work_time;
            $snapshot->right_schedule_break_time = $transfer_with_EWI_id->schedule_break_time;
            $snapshot->right_schedule_night_break_time = $transfer_with_EWI_id->schedule_night_break_time;
            $snapshot->right_schedule_working_hour = $transfer_with_EWI_id->schedule_working_hour;
        } elseif ($snapshot->right_status == EmployeeWorkingInformationSnapshot::DENIED
            && $snapshot->left_work_status_id != WorkStatus::FURIKYUU && $snapshot->right_work_status_id == WorkStatus::FURIKYUU) {
            $snapshot->left_schedule_start_work_time = $this->schedule_start_work_time;
            $snapshot->left_schedule_end_work_time = $this->schedule_end_work_time;
            $snapshot->left_schedule_break_time = $this->schedule_break_time;
            $snapshot->left_schedule_night_break_time = $this->schedule_night_break_time;
            $snapshot->left_schedule_working_hour = $this->schedule_working_hour;
        } elseif ($snapshot->left_status == EmployeeWorkingInformationSnapshot::APPROVED
            && $snapshot->left_work_status_id == WorkStatus::FURIDE && $snapshot->right_work_status_id != WorkStatus::FURIDE) {
            $snapshot->left_schedule_start_work_time = $this->schedule_start_work_time;
            $snapshot->left_schedule_end_work_time = $this->schedule_end_work_time;
            $snapshot->left_schedule_break_time = $this->schedule_break_time;
            $snapshot->left_schedule_night_break_time = $this->schedule_night_break_time;
            $snapshot->left_schedule_working_hour = $this->schedule_working_hour;
        } else {
            $snapshot->left_schedule_start_work_time = $snapshot->right_schedule_start_work_time = $this->schedule_start_work_time;
            $snapshot->left_schedule_end_work_time = $snapshot->right_schedule_end_work_time = $this->schedule_end_work_time;
            $snapshot->left_schedule_break_time = $snapshot->right_schedule_break_time = $this->schedule_break_time;
            $snapshot->left_schedule_night_break_time = $snapshot->right_schedule_night_break_time = $this->schedule_night_break_time;
            $snapshot->left_schedule_working_hour = $snapshot->right_schedule_working_hour = $this->schedule_working_hour;
        }

        if (!$snapshot->left_switch_planned_schedule_target || $snapshot->left_status != EmployeeWorkingInformationSnapshot::CONSIDERING) {
            $list_key_delete = [];
            $color_statuses = ($is_snapshot_trashed) ? $snapshot->colorStatuses()->disable()->get() : $snapshot->colorStatuses;
            foreach ($color_statuses as $color) {
                if ($color->field_css_class == ColorStatus::CONSIDERING_COLOR)
                    array_push($list_key_delete, str_after($color->field_name, 'left_'));
            }

            foreach (array_diff($array_attribute_name, $list_key_delete) as $value) {
                if (in_array($value, ['planned_break_time', 'real_break_time', 'planned_night_break_time', 'real_night_break_time'])) {
                    $snapshot['left_'.$value] = $this->getOriginal($value);
                } else {
                    $snapshot['left_'.$value] = $this[$value];
                }

                if (!in_array("timestamped_start_work_date", $list_key_delete) && !in_array("timestamped_start_work_time", $list_key_delete)) {
                    if (isset($target_absorb_EWI)) {
                        if (isset($target_absorb_EWI->timestamped_start_work_time)) {
                            $carbon_timestamped_start_work_time =
                                Carbon::createFromFormat('Y-m-d H:i:s', $target_absorb_EWI->getOriginal('timestamped_start_work_time'));

                            $snapshot->left_timestamped_start_work_date = $carbon_timestamped_start_work_time->toDateString();
                            $snapshot->left_timestamped_start_work_time = $carbon_timestamped_start_work_time->toTimeString();
                        }
                    } else {
                        $snapshot->left_timestamped_start_work_date = ($start_work_time) ? $start_work_time->toDateString() : null;
                        $snapshot->left_timestamped_start_work_time = ($start_work_time) ? $start_work_time->format('H:i:s') : null;
                    }
                }
                if (!in_array("timestamped_end_work_date", $list_key_delete) && !in_array("timestamped_end_work_time", $list_key_delete)) {
                    if (isset($target_absorb_EWI)) {
                        if (isset($target_absorb_EWI->timestamped_end_work_time)) {
                            $carbon_timestamped_end_work_time =
                                Carbon::createFromFormat('Y-m-d H:i:s', $target_absorb_EWI->getOriginal('timestamped_end_work_time'));

                            $snapshot->left_timestamped_end_work_date = $carbon_timestamped_end_work_time->toDateString();
                            $snapshot->left_timestamped_end_work_time = $carbon_timestamped_end_work_time->toTimeString();
                        }
                    } else {
                        $snapshot->left_timestamped_end_work_date = ($end_work_time) ? $end_work_time->toDateString() : null;
                        $snapshot->left_timestamped_end_work_time = ($end_work_time) ? $end_work_time->format('H:i:s') : null;
                    }
                }
                if (!in_array("work_status_id", $list_key_delete)) {
                    $snapshot->left_work_status_id = $this->planned_work_status_id;
                }
                if (!in_array("rest_status_id", $list_key_delete)) {
                    $snapshot->left_rest_status_id = $this->planned_rest_status_id;
                }
            }
        }

        if ($snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING) {
            $snapshot->right_timestamped_start_work_date = ($start_work_time) ? $start_work_time->toDateString() : null;
            $snapshot->right_timestamped_start_work_time = ($start_work_time) ? $start_work_time->format('H:i:s') : null;
            $snapshot->right_timestamped_end_work_date =  ($end_work_time) ? $end_work_time->toDateString() : null;
            $snapshot->right_timestamped_end_work_time = ($end_work_time) ? $end_work_time->format('H:i:s') : null;
            $snapshot->right_work_status_id = $this->planned_work_status_id;
            $snapshot->right_rest_status_id = $this->planned_rest_status_id;

            foreach ($array_attribute_name as $value) {
                if (in_array($value, ['planned_break_time', 'real_break_time', 'planned_night_break_time', 'real_night_break_time']))
                    $snapshot['right_'.$value] = $this->getOriginal($value);
                else
                    $snapshot['right_'.$value] = $this[$value];
            }
        }

        return $snapshot;
    }

    /**
     * Get all the color statuses for this working information instance
     */
    public function colorStatuses()
    {
        return $this->morphMany(ColorStatus::class, 'colorable');
    }

    /**
     * Get all the checklist items of this working information instance
     */
    public function checklistItems()
    {
        return $this->hasMany(ChecklistItem::class);
    }

    /**
     * Get the work address working employee instance
     */
    public function workAddressWorkingEmployee()
    {
        return $this->belongsTo(WorkAddressWorkingEmployee::class);
    }

    /**
     * Get the concluded employee working information instance
     */
    public function concludedEmployeeWorkingInformation()
    {
        return $this->hasOne(ConcludedEmployeeWorkingInformation::class);
    }

    /**
     * Get the cached employee working information instance
     */
    public function cachedEmployeeWorkingInformation()
    {
        return $this->hasOne(CachedEmployeeWorkingInformation::class);
    }

    /**
     * Get the current PlannedRestStatus
     * Need this relationship so that we dont have to query this RestStatus model twice.
     */
    public function plannedRestStatus()
    {
        return $this->belongsTo(RestStatus::class, 'planned_rest_status_id');
    }

    /**
     * Get the planned work location which is effecting this employee working information
     */
    public function currentPlannedWorkLocation()
    {
        return $this->belongsTo(WorkLocation::class, 'planned_work_location_id');
    }

    /**
     * Get the real work location which is effecting this employee working information
     */
    public function currentRealWorkLocation()
    {
        return $this->belongsTo(WorkLocation::class, 'real_work_location_id');
    }

    //////// Query Scope ///////

    /**
     * Get the EmployeeWorkingInformation which has not been manually modified yet
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotManuallyModified($query)
    {
        return $query->where('manually_modified', false);
    }

    /**
     * Get the EmployeeWorkingInformation which has not been schedule_modified yet
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotScheduleModified($query)
    {
        return $query->where('schedule_modified', false);
    }

    /**
     * Get the EmployeeWorkingInformation which has not been concluded yet
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotConcluded($query)
    {
        return $query->whereHas('employeeWorkingDay', function($query) {
            $query->where('concluded_level_one', false)->where('concluded_level_two', false);
        });
    }

    /**
     * Get the EmployeeWorkingInformation which still does not have any WorkingTimestamp
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotHaveTimestamp($query)
    {
        return $query->whereNull('timestamped_start_work_time')->whereNull('timestamped_end_work_time');
    }

    ////////////////////////////

    //////////////////////// ACCESSORS - A WHOLE LOT OF ACCESSORS /////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    // 5 attributes of schedule
    // Notice: the start/end work time attributes use different function from the other's
    public function getScheduleStartWorkTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->schedule_start_work_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->schedule_start_work_time;
        }
    }

    public function getScheduleEndWorkTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->schedule_end_work_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->schedule_end_work_time;
        }
    }

    public function getScheduleBreakTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->schedule_break_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->schedule_break_time;
        }
    }

    public function getScheduleNightBreakTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->schedule_night_break_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->schedule_night_break_time;
        }

    }

    public function getScheduleWorkingHourAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->schedule_working_hour;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->schedule_working_hour;
        }
    }


    public function getPlannedWorkStatusIdAttribute($value)
    {
        if ($this->temporary != true && $this->concludedEmployeeWorkingInformation) {
            return $this->concludedEmployeeWorkingInformation->work_status_id;

        } else {
            if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
                return $this->cachedEmployeeWorkingInformation->planned_work_status_id;

            } else {
                $this->calculateAndCacheAttributes();
                return $this->cachedEmployeeWorkingInformation->planned_work_status_id;
            }
        }
    }

    // Based on the date of this instance's EmployeeWorkingDay and the current date
    public function getRealWorkStatusIdAttribute($value)
    {
        if ($this->planned_work_status_id == WorkStatus::KEKKIN) {
            $today = Carbon::today();
            $date_of_this_instance = Carbon::createFromFormat('Y-m-d', $this->employeeWorkingDay->date)->hour(0)->minute(0)->second(0);

            return ($this->planned_work_status_id !== null && $date_of_this_instance->lte($today)) ? $this->planned_work_status_id : null;
        }
        return $value;
    }

    public function getPlannedRestStatusIdAttribute($value)
    {
        if ($this->temporary != true && $this->concludedEmployeeWorkingInformation) {
            return $this->concludedEmployeeWorkingInformation->rest_status_id;

        } else {
            if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
                return $this->cachedEmployeeWorkingInformation->planned_rest_status_id;

            } else {
                $this->calculateAndCacheAttributes();
                return $this->cachedEmployeeWorkingInformation->planned_rest_status_id;
            }
        }
    }

    // Based on the date of this instance's EmployeeWorkingDay and the current date
    public function getRealRestStatusIdAttribute($value)
    {
        $today = Carbon::today();
        $date_of_this_instance = Carbon::createFromFormat('Y-m-d', $this->employeeWorkingDay->date)->hour(0)->minute(0)->second(0);

        return ($this->planned_rest_status_id !== null && $date_of_this_instance->lte($today)) ? $this->planned_rest_status_id : null;
    }

    public function getNotIncludeBreakTimeWhenDisplayPlannedTimeAttribute($value)
    {
        return $value == true;
    }

    public function setNotIncludeBreakTimeWhenDisplayPlannedTimeAttribute($value)
    {
        $this->attributes['not_include_break_time_when_display_planned_time'] = $value == true;
    }


    public function getPlannedWorkLocationIdAttribute($value)
    {
        return $this->genericGetFromPlannedSchedule('work_location_id', $value);
    }

    public function getPlannedWorkAddressIdAttribute($value)
    {
        return $this->genericGetFromPlannedSchedule('work_address_id', $value);
    }


    /**
     * If there is early_arrive_start, then it will take the value of that attribute,
     * If there is no early_arrive_start and there is a late_time then this attribute is equal to the schedule_start_work_time + the late time.
     * Else, just take the schedule_start_work_time
     *
     * The same logic apply to planned_end_work_time attribute
     */
    public function getPlannedStartWorkTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_start_work_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_start_work_time;
        }
    }

    /**
     * Use the start_time_round_up setting of the current planned_work_location to round up.
     */
    public function getTimestampedStartWorkTimeAttribute($value)
    {
        if ($this->temporary != true && $this->concludedEmployeeWorkingInformation) {
            return $this->concludedEmployeeWorkingInformation->timestamped_start_work_time;

        } else {
            $carbon_instance = $this->getCarbonInstance($value);
            if ($carbon_instance) {
                $round_up_by = $this->affectedByRealWorkLocation()->currentSetting()->start_time_round_up;

                $carbon_instance->minute = ceil($carbon_instance->minute/$round_up_by) * $round_up_by;
                return $carbon_instance->format('Y-m-d H:i:s');
            }
            return $value;
        }
    }

    /**
     * Compare the timestamped_start_work and planned_start_work, take whichever is bigger.
     */
    public function getRealStartWorkTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_start_work_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_start_work_time;
        }
    }


    /**
     * Like planned_start_work_time_attribute, planned_end_work_time_attribute takes the overtime_start or early_leave_time into account
     */
    public function getPlannedEndWorkTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_end_work_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_end_work_time;
        }
    }

    /**
     * Use the end_time_round_down setting of the current planned_work_location to round down.
     */
    public function getTimestampedEndWorkTimeAttribute($value)
    {
        if ($this->temporary != true && $this->concludedEmployeeWorkingInformation) {
            return $this->concludedEmployeeWorkingInformation->timestamped_end_work_time;

        } else {
            $carbon_instance = $this->getCarbonInstance($value);
            if ($carbon_instance) {
                $round_down_by = $this->affectedByRealWorkLocation()->currentSetting()->end_time_round_down;

                $carbon_instance->minute = floor($carbon_instance->minute/$round_down_by) * $round_down_by;
                return $carbon_instance->format('Y-m-d H:i:s');
            }
            return $value;
        }
    }

    /**
     * Compare the timestamped_end_work and planned_end_work, take whichever is smaller.
     */
    public function getRealEndWorkTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_end_work_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_end_work_time;
        }
    }


    /**
     * The total work time of that day (exclude break, late, early leave and paid rest time)
     */
    public function getPlannedWorkingHourAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_working_hour;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_working_hour;
        }
    }

    /**
     * The total real work time of that day (exclude break, late, early leave and paid rest time)
     */
    public function getRealWorkingHourAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_working_hour;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_working_hour;
        }
    }


    /**
     * The planned early arrive start attribute
     */
    public function getPlannedEarlyArriveStartAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_early_arrive_start;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_early_arrive_start;
        }
    }

    /**
     * These two real_early_arrive_start/end attributes are calculated, base on the bigger/smaller relationship between planned_early_arrive_start/end and workStartTimestamp.
     *
     *      timestamp < start < end         OK
     *      start <= timestamp <= end       OK
     *      start < end < timestamp         Not OK
     */
    public function getRealEarlyArriveStartAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_early_arrive_start;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_early_arrive_start;
        }
    }


    /**
     * Same as above of above
     */
    public function getPlannedEarlyArriveEndAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_early_arrive_end;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_early_arrive_end;
        }
    }

    /**
     * Same as above of above
     */
    public function getRealEarlyArriveEndAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_early_arrive_end;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_early_arrive_end;
        }
    }


    /**
     * If 'take-the-whole-day-off' or the schedule of this working information is 'only-working-hour-and-break-time' mode then return null
     */
    public function getPlannedLateTimeAttribute($value)
    {
        return ($this->takeAWholeDayOff() || $this->isOnlyWorkingHourAndBreakTimeMode()) ? null : $value;
    }

    /**
     * If the timestamp_start_work_time is bigger than the planned_work_span_start then this is 'late for work' case.
     */
    public function getRealLateTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_late_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_late_time;
        }
    }


    /**
     * The 所定内. This is actually quite an important attribute of this model.
     */
    public function getPlannedWorkSpanAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_work_span;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_work_span;
        }
    }

    /**
     * Calculate the real_work_span of that day. If both real_work_span_start/end exist, then just calculate it like normal
     *              work_span = (start - end) - break
     * If both of that attributes are absent, and both timestamps for start/end_work have already existed, then we have to calculate base on
     * the real timestamp values
     *              work_span = min[((timestamp_start - timestamp_end) - break), planned_work_span]
     */
    public function getRealWorkSpanAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_work_span;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_work_span;
        }
    }

    /**
     * Take the planned_late_time into account. Also, if the work status of this day is 前休 (or 前給), that means the employee will take the first half
     * of the day off. Because of that, we have to in crease the planned_work_span_start by half of the total work_span of that day plus the planned break time.
     */
    public function getPlannedWorkSpanStartAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_work_span_start;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_work_span_start;
        }
    }

    /**
     * In the case of 'late for work', add those late minutes to the planned_work_span_start to get the real_work_span_start
     */
    public function getRealWorkSpanStartAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_work_span_start;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_work_span_start;
        }
    }

    /**
     * Have to take the early leave time into account. Also have to calculate the offset to subtract in the case of the Employee take half a day off.
     * For more detail, check the function offsetTimeToAddOrSubWhenTakeHalfDayOff
     */
    public function getPlannedWorkSpanEndAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_work_span_end;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_work_span_end;
        }
    }

    /**
     * In the case of 'leave work early', subtract those minutes to the planned_work_span_end to get the real_work_span_end
     */
    public function getRealWorkSpanEndAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_work_span_end;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_work_span_end;
        }
    }


    /**
     * Planned break time and planned night break time are also delegated from the PlannedSchedule
     * While the real_break_time and real_night_break_time are delegated from the planned counter parts (conditionally though)
     */
    public function getPlannedBreakTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_break_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_break_time;
        }
    }

    public function getRealBreakTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_break_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_break_time;
        }
    }

    public function getPlannedNightBreakTimeAttribute($value)
    {
        if (!$this->takeAWholeDayOff() && !$this->isPlannedRestStatus(RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2, RestStatus::GOKYUU_1, RestStatus::GOKYUU_2)) {

            if ($value === null) {

                return $this->schedule_night_break_time;

            } else {
                return ($value === config('caeru.empty')) ? null : $value;
            }

        } else {
            return null;
        }

    }

    public function getRealNightBreakTimeAttribute($value)
    {
        if ($this->real_start_work_time === null ||
            $this->real_end_work_time === null ||
            $this->takeAWholeDayOff() === true ||
            $this->planned_work_status_id === WorkStatus::KEKKIN || $this->planned_work_status_id === WorkStatus::FURIKYUU ||
            $this->planned_rest_status_id === RestStatus::ZENKYUU_1 || $this->planned_rest_status_id === RestStatus::ZENKYUU_2 ||
            $this->planned_rest_status_id === RestStatus::GOKYUU_1 || $this->planned_rest_status_id === RestStatus::GOKYUU_2) {

            return null;

        } else if ($value === null) {
            return $this->planned_night_break_time;
        } else {
            return ($value === config('caeru.empty')) ? null : $value;
        }

    }

    public function getPlannedGoOutTimeAttribute($value)
    {
        return (!$this->takeAWholeDayOff() && $this->affectedByWorkLocation()->currentSetting()->go_out_button_usage === Setting::USE_GO_OUT_BUTTON) ? $value : null;
    }

    // This go_out_time need to be round up by the work location's setting
    public function getRealGoOutTimeAttribute($value)
    {
        if ($this->concludedEmployeeWorkingInformation) {
            return $this->concludedEmployeeWorkingInformation->real_go_out_time;

        } else {
            if ($value) {
                if (!$this->takeAWholeDayOff() && $this->planned_work_status_id !== WorkStatus::KEKKIN && $this->planned_work_status_id !== WorkStatus::FURIKYUU) {
                    $real_work_location = $this->affectedByWorkLocation();
                    if ($real_work_location) {
                        $round_up_by = $real_work_location->currentSetting()->break_time_round_up;

                        $value = intval(ceil($value/$round_up_by) * $round_up_by);
                    }
                } else {
                    return null;
                }
            }
            return $value;
        }
    }


    /**
     * If 'take-a-whole-day-off' return null
     */
    public function getPlannedEarlyLeaveTimeAttribute($value)
    {
        return !$this->takeAWholeDayOff() ? $value : null;
    }

    /**
     * If the timestamp_end_work_time is smaller than the planned_work_span_end then, this is 'leave work early' case.
     */
    public function getRealEarlyLeaveTimeAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_early_leave_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_early_leave_time;
        }
    }

    /**
     * We have to calculate the date for these two attributes (because they can only be inputed as time_string in the form).
     */
    public function getPlannedOvertimeStartAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_overtime_start;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_overtime_start;
        }
    }

    /**
     * The same logic as real_early_arrive_start/end. Only the opposite.
     */
    public function getRealOvertimeStartAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_overtime_start;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_overtime_start;
        }
    }


    /**
     * The same as above of above.
     */
    public function getPlannedOvertimeEndAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_overtime_end;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_overtime_end;
        }
    }

    /**
     * The same as above of above.
     */
    public function getRealOvertimeEndAttribute($value)
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_overtime_end;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_overtime_end;
        }
    }


    // These are the salary-related attributes
    public function getBasicSalaryAttribute($value)
    {
        $result = $this->genericGetFromPlannedSchedule('salary', $value, true);
        return ($result === config('caeru.empty')) ? null : $result;
    }

    public function getNightSalaryAttribute($value)
    {
        $result = $this->genericGetFromPlannedSchedule('night_salary', $value, true);
        return ($result === config('caeru.empty')) ? null : $result;
    }

    public function getOvertimeSalaryAttribute($value)
    {
        $result = $this->genericGetFromPlannedSchedule('overtime_salary', $value, true);
        return ($result === config('caeru.empty')) ? null : $result;
    }

    public function getDeductionSalaryAttribute($value)
    {
        $result = $this->genericGetFromPlannedSchedule('deduction_salary', $value, true);
        return ($result === config('caeru.empty')) ? null : $result;
    }

    public function getNightDeductionSalaryAttribute($value)
    {
        $result = $this->genericGetFromPlannedSchedule('night_deduction_salary', $value, true);
        return ($result === config('caeru.empty')) ? null : $result;
    }

    public function getMonthlyTrafficExpenseAttribute($value)
    {
        $result = $this->genericGetFromPlannedSchedule('monthly_traffic_expense', $value);
        return ($result === config('caeru.empty')) ? null : $result;
    }

    public function getDailyTrafficExpenseAttribute($value)
    {
        $result = $this->genericGetFromPlannedSchedule('daily_traffic_expense', $value);
        return ($result === config('caeru.empty')) ? null : $result;
    }


    // Some Virtual utility attributes
    /**
     * Base on the date_separate setting of the company to determine the uppler limit of date attributes of this working day
     *
     * @return string       a date time string with format 'Y-m-d H:i:s'
     */
    public function getDateUpperLimitAttribute()
    {
        $company = $this->employeeWorkingDay->employee->workLocation->company;

        if ($company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {
            return $this->employeeWorkingDay->date . ' ' . $company->date_separate_time . ':00';
        } else {
            $day = Carbon::createFromFormat('Y-m-d', $this->employeeWorkingDay->date);
            $day->subDay();
            return $day->format('Y-m-d') . ' ' . $company->date_separate_time . ':00';
        }
    }

    /**
     * Get the day of the date_upper_limit above
     *
     * @return string    a date string with format 'Y-m-d'
     */
    public function getDayOfTheUpperLimitAttribute()
    {
        if ($this->employeeWorkingDay->employee->workLocation->company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {
            return $this->employeeWorkingDay->date;
        } else {
            return Carbon::createFromFormat('Y-m-d', $this->employeeWorkingDay->date)->subDay()->format('Y-m-d');
        }
    }

    /**
     * Get the name of the manager who last modified this working information
     *
     * @return string|null
     */
    public function getLastModifyPersonNameAttribute()
    {
        if ($this->last_modify_person_type == self::MODIFY_PERSON_TYPE_MANAGER) {
            $manager = Manager::find($this->last_modify_person_id);
            return $manager ? $manager->fullName() : null;

        } elseif ($this->last_modify_person_type == self::MODIFY_PERSON_TYPE_EMPLOYEE) {
            $employee = Employee::find($this->last_modify_person_id);
            return $employee ? $employee->fullName() : null;

        }
        return null;
    }

    ///////////////////////////////////////////////////// End of accessors //////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////// Utility Public Function //////////////////////////


    /**
     * Calculate once and then cache the neccessary attributes.
     * The purpose of this function is to reduce the calculating time of the attribute: real_working_hour (when an instance of this model have all the timestamped attributes, the calculation
     * time of that attribute may go up to ~ 2 second). That will make the process time of some operations drastically go up (for example the toArray).
     *
     * That's why when you do need the real_working_hour. Remember to call this function first (at least once).
     *
     * This is one long-ass function. The calculating order of those attributes is important. So DONT CHANGE THEM, unless you know exactly what you are doing.
     *
     * @return void
     */
    public function calculateAndCacheAttributes()
    {

        /////////////////////////////////////////////////////////////////////////////////////////
        // This is a long-ass function and it has to be executed in this order to be efficient //
        /////////////////////////////////////////////////////////////////////////////////////////

        // First thing is checking the existence of the cached model, create one if does not exist
        $cache_not_exist = false;
        if ($this->cachedEmployeeWorkingInformation === null) {
            $cache = new CachedEmployeeWorkingInformation();
            $cache->employee_working_information_id = $this->id;
            $cache_not_exist = true;
        } else {
            $cache = $this->cachedEmployeeWorkingInformation;
        }


        //** ScheduleStartWorkTime - did declare carbon instance
        $schedule_start_work_time = isset($this->attributes['schedule_start_work_time']) ? $this->attributes['schedule_start_work_time'] : null;

        $carbon_schedule_start_work_time = null;
        if (isset($schedule_start_work_time)) {
            $schedule_start_work_time = ($schedule_start_work_time === config('caeru.empty_date')) ? null : $schedule_start_work_time;
            $carbon_schedule_start_work_time = $this->getCarbonInstance($schedule_start_work_time);

        } else {
            $carbon_schedule_start_work_time = $this->getCarbonInstance($this->getTimeDataFromSchedules('start_work_time', null));
            $schedule_start_work_time = $carbon_schedule_start_work_time ? $carbon_schedule_start_work_time->format('Y-m-d H:i:s') : null;
        }

        $cache->schedule_start_work_time = $schedule_start_work_time;
        /////////////////////////////////////////////////////////////////////////////////////////


        //** ScheduleEndWorkTime - did declare carbon instance
        $schedule_end_work_time = isset($this->attributes['schedule_end_work_time']) ? $this->attributes['schedule_end_work_time'] : null;

        $carbon_schedule_end_work_time = null;
        if (isset($schedule_end_work_time)) {
            $schedule_end_work_time = ($schedule_end_work_time === config('caeru.empty_date')) ? null : $schedule_end_work_time;
            $carbon_schedule_end_work_time = $this->getCarbonInstance($schedule_end_work_time);
        } else {
            $carbon_schedule_end_work_time = $this->getCarbonInstance($this->getTimeDataFromSchedules('end_work_time', null));
            $schedule_end_work_time = $carbon_schedule_end_work_time ? $this->makeTheSecondOneBigger($carbon_schedule_start_work_time, $carbon_schedule_end_work_time)->format('Y-m-d H:i:s') : null;
        }

        $cache->schedule_end_work_time = $schedule_end_work_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** ScheduleBreakTime
        $schedule_break_time = isset($this->attributes['schedule_break_time']) ? $this->attributes['schedule_break_time'] : null;

        if (isset($schedule_break_time)) {
            $schedule_break_time = ($schedule_break_time === config('caeru.empty')) ? null : $schedule_break_time;
        } else {
            $schedule_break_time = $this->genericGetFromPlannedSchedule('break_time', null);
        }

        $cache->schedule_break_time = $schedule_break_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** ScheduleNightBreakTime
        $schedule_night_break_time = isset($this->attributes['schedule_night_break_time']) ? $this->attributes['schedule_night_break_time'] : null;

        if (isset($schedule_night_break_time)) {
            $schedule_night_break_time = ($schedule_night_break_time === config('caeru.empty')) ? null : $schedule_night_break_time;
        } else {
            $schedule_night_break_time = $this->genericGetFromPlannedSchedule('night_break_time', null);
        }

        $cache->schedule_night_break_time = $schedule_night_break_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** ScheduleWorkingHour - did declare carbon instance
        $schedule_working_hour = isset($this->attributes['schedule_working_hour']) ? $this->attributes['schedule_working_hour'] : null;

        if (isset($schedule_working_hour)) {
            $schedule_working_hour = ($schedule_working_hour === config('caeru.empty_time')) ? null : $schedule_working_hour;
        } else {
            if ($schedule_start_work_time && $schedule_end_work_time) {
                $breaktime = $schedule_break_time ? $schedule_break_time : 0;
                $total_schedule_working_hour_in_minutes = $carbon_schedule_end_work_time->diffInMinutes($carbon_schedule_start_work_time) - $breaktime;
                $schedule_working_hour = ($total_schedule_working_hour_in_minutes > 0) ?  $this->minutesToString($total_schedule_working_hour_in_minutes) : '00:00:00';
            } else {
                $schedule_working_hour = $this->genericGetFromPlannedSchedule('working_hour', null);
            }
        }

        $cache->schedule_working_hour = $schedule_working_hour;
        $carbon_schedule_working_hour = $this->getCarbonInstance($schedule_working_hour);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkLocationId - did retrieve the relating Model
        $planned_work_location_id = $this->planned_work_location_id;
        $planned_work_location = $this->affectedByWorkLocation();
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkStatusId
        $planned_work_status_id = isset($this->attributes['planned_work_status_id']) ? $this->attributes['planned_work_status_id'] : null;

        // $planned_work_location
        if ($planned_work_location) {
            $available_work_statuses = $planned_work_location->activatingWorkStatuses()->pluck('id')->toArray();
            $planned_work_status_id =  in_array($planned_work_status_id, $available_work_statuses) ? $planned_work_status_id : null;
        }

        $cache->planned_work_status_id = $planned_work_status_id;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedWorkStatusHouDeOrKyuuDe - no need to cache this
        $is_planned_work_status_houde_or_kyuude_or_zangyou = false;
        $is_planned_work_status_houde_or_kyuude_or_zangyou = in_array($planned_work_status_id, [WorkStatus::HOUDE, WorkStatus::KYUUDE, WorkStatus::ZANGYOU]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedWorkStatusKekkinOrFurikyuu - no need to cache this
        $is_planned_work_status_kekkin_or_furikyuu = false;
        $is_planned_work_status_kekkin_or_furikyuu = in_array($planned_work_status_id, [WorkStatus::KEKKIN, WorkStatus::FURIKYUU]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedRestStatusId - did retrieve the relating Model
        $planned_rest_status_id = isset($this->attributes['planned_rest_status_id']) ? $this->attributes['planned_rest_status_id'] : null;

        // $planned_work_location
        if ($planned_work_location) {
            $available_rest_statuses = $planned_work_location->activatingRestStatuses()->pluck('id')->toArray();
            $planned_rest_status_id =  in_array($planned_rest_status_id, $available_rest_statuses) ? $planned_rest_status_id : null;
        }

        // Why do i have to do this ? Because of a Freaking wierd bug, that only appears on test server in a certain situation.
        $planned_rest_status = ($planned_rest_status_id !== null) ? RestStatus::find($planned_rest_status_id) : null;
        $cache->planned_rest_status_id = $planned_rest_status_id;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsRestStatusUnitDayOrHour - no need to cache this
        $is_rest_status_unit_day = false;

        if ($planned_rest_status) {
            $is_rest_status_unit_day = ($planned_rest_status->unit_type == true);
        }

        $cache->rest_status_unit_day = $is_rest_status_unit_day;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedRestStatusGouKyuOrZenKyuu - no need to cache this
        $is_planned_rest_status_goukyuu_or_zenkyuu = null;
        $is_planned_rest_status_goukyuu_or_zenkyuu = in_array($planned_rest_status_id, [RestStatus::GOKYUU_1, RestStatus::GOKYUU_2, RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedRestStatusGouKyu - no need to cache this
        $is_planned_rest_status_goukyuu = null;
        $is_planned_rest_status_goukyuu = in_array($planned_rest_status_id, [RestStatus::GOKYUU_1, RestStatus::GOKYUU_2]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedRestStatusZenkyuu - no need to cache this
        $is_planned_rest_status_zenkyuu = null;
        $is_planned_rest_status_zenkyuu = in_array($planned_rest_status_id, [RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PaidRestTimeStart - did declare carbon instance - no need to cach this
        $paid_rest_time_start = isset($this->attributes['paid_rest_time_start']) ? $this->attributes['paid_rest_time_start'] : null;
        $carbon_paid_rest_time_start = $this->getCarbonInstance($paid_rest_time_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PaidRestTimeEnd - did declare carbon instance - no need to cach this
        $paid_rest_time_end = isset($this->attributes['paid_rest_time_end']) ? $this->attributes['paid_rest_time_end'] : null;
        $carbon_paid_rest_time_end = $this->getCarbonInstance($paid_rest_time_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PaidRestTimePeriod - no need to cache this
        $paid_rest_time_period_in_minutes = isset($this->attributes['paid_rest_time_period']) ? $this->convertToMinutes($this->getCarbonInstance($this->attributes['paid_rest_time_period'])) : null;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** NotIncludeBreakTimeWhenDisplayPlannedTime - no need to cache this
        $not_include_break_time_when_display_planned_time = isset($this->attributes['not_include_break_time_when_display_planned_time']) ? $this->attributes['not_include_break_time_when_display_planned_time'] : null;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalPaidRestTimeInMinutes - no need to cach this
        $total_paid_rest_time_in_minutes = 0;

        // $carbon_paid_rest_time_start
        // $carbon_paid_rest_time_end
        if ($is_rest_status_unit_day == false && $carbon_paid_rest_time_start !== null && $carbon_paid_rest_time_end !== null) {
            $total_paid_rest_time = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

            // $carbon_schedule_working_hour
            $working_hour_in_minutes = $this->convertToMinutes($carbon_schedule_working_hour);
            $total_paid_rest_time_in_minutes = $total_paid_rest_time > $working_hour_in_minutes ? $working_hour_in_minutes : $total_paid_rest_time;

        } elseif ($is_rest_status_unit_day == false && $paid_rest_time_period_in_minutes !== null) {
            $total_paid_rest_time_in_minutes = $paid_rest_time_period_in_minutes;

        } elseif ($is_planned_rest_status_goukyuu_or_zenkyuu) {
            // $carbon_schedule_working_hour
            $total_paid_rest_time_in_minutes = ($carbon_schedule_working_hour !== null) ? ceil($this->convertToMinutes($carbon_schedule_working_hour)/2) : null;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TakeAWholeDayOff - no need to cach this
        $take_a_whole_day_off = false;

        // $carbon_schedule_working_hour
        if ($carbon_schedule_working_hour !== null) {
            $working_hour_in_minutes = $this->convertToMinutes($carbon_schedule_working_hour);

            if ($is_rest_status_unit_day == true || ($total_paid_rest_time_in_minutes !== 0 && $total_paid_rest_time_in_minutes === $working_hour_in_minutes)) {
                $take_a_whole_day_off = true;
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedEarlyArriveStart - did declare carbon instance
        $planned_early_arrive_start = isset($this->attributes['planned_early_arrive_start']) ? $this->attributes['planned_early_arrive_start'] : null;
        $planned_early_arrive_start = $take_a_whole_day_off == true ? null : $planned_early_arrive_start;

        $carbon_planned_early_arrive_start = $this->getCarbonInstance($planned_early_arrive_start);
        $cache->planned_early_arrive_start = $planned_early_arrive_start;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedEarlyArriveEnd - did declare carbon instance - no need to cach this
        $planned_early_arrive_end = isset($this->attributes['planned_early_arrive_end']) ? $this->attributes['planned_early_arrive_end'] : null;
        $planned_early_arrive_end = $take_a_whole_day_off == true ? null : $planned_early_arrive_end;

        $carbon_planned_early_arrive_end = $this->getCarbonInstance($planned_early_arrive_end);
        $cache->planned_early_arrive_end = $planned_early_arrive_end;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalPlannedEarlyArriveTimeInMinutes - no need to cach this
        $total_planned_early_arrive_time_in_minutes = 0;

        // $carbon_planned_early_arrive_start
        // $carbon_planned_early_arrive_end
        if ($carbon_planned_early_arrive_start !== null && $carbon_planned_early_arrive_end !== null) {
            $total_planned_early_arrive_time_in_minutes = $carbon_planned_early_arrive_end->diffInMinutes($carbon_planned_early_arrive_start);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedOvertimeStart - did declare carbon instance - no need to cach this
        $planned_overtime_start = isset($this->attributes['planned_overtime_start']) ? $this->attributes['planned_overtime_start'] : null;
        $planned_overtime_start = $take_a_whole_day_off == true ? null : $planned_overtime_start;

        $carbon_planned_overtime_start = $this->getCarbonInstance($planned_overtime_start);
        $cache->planned_overtime_start = $planned_overtime_start;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedOvertimeEnd - did declare carbon instance - no need to cach this
        $planned_overtime_end = isset($this->attributes['planned_overtime_end']) ? $this->attributes['planned_overtime_end'] : null;
        $planned_overtime_end = $take_a_whole_day_off == true ? null : $planned_overtime_end;

        $carbon_planned_overtime_end = $this->getCarbonInstance($planned_overtime_end);
        $cache->planned_overtime_end = $planned_overtime_end;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalPlannedOverTimeInMinutes - no need to cach this
        $total_planned_overtime_in_minutes = 0;

        // $carbon_planned_overtime_start
        // $carbon_planned_overtime_end
        if ($carbon_planned_overtime_start !== null && $carbon_planned_overtime_end !== null) {
            $total_planned_overtime_in_minutes = $carbon_planned_overtime_end->diffInMinutes($carbon_planned_overtime_start);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsOnlyWorkingHourAndBreakTimeMode - no need to cach this
        $is_only_working_hour_and_break_time_mode = ($schedule_start_work_time === null) &&
                                                    ($schedule_end_work_time === null) &&
                                                    ($schedule_working_hour !== null);
        ///////////////////////////////////////////////////////////////////////////////////////////



        //** PlannedBreakTime
        $planned_break_time = isset($this->attributes['planned_break_time']) ? $this->attributes['planned_break_time'] : null;

        if (!$take_a_whole_day_off && !$is_planned_rest_status_goukyuu_or_zenkyuu) {
            if ($planned_break_time === null) {

                $planned_break_time = $schedule_break_time;
            }

        } else {
            $planned_break_time = null;
        }

        $cache->planned_break_time = $planned_break_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedLateTime - no need to cach this
        $planned_late_time =  isset($this->attributes['planned_late_time']) ? $this->attributes['planned_late_time'] : null;
        $planned_late_time = ($take_a_whole_day_off || $is_only_working_hour_and_break_time_mode) ? null : $planned_late_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedEarlyLeaveTime - no need to cach this
        $planned_early_leave_time = isset($this->attributes['planned_early_leave_time']) ? $this->attributes['planned_early_leave_time'] : null;
        $planned_early_leave_time = $take_a_whole_day_off ? null : $planned_early_leave_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkLocationId - did retrieve the relating model
        $real_work_location_id = $this->real_work_location_id;
        $real_work_location = $this->affectedByRealWorkLocation();
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TimestampedStartWorkTime - did declare the carbon instance. This attribute, we cach it, but not use it.
        // For using it, the timestamps distribute mechanism (in WorkingTimestampSubscriber) will get messed up.
        $timestamped_start_work_time = isset($this->attributes['timestamped_start_work_time']) ? $this->attributes['timestamped_start_work_time'] : null;

        if ($this->temporary != true && $this->concludedEmployeeWorkingInformation) {
            $timestamped_start_work_time = $this->concludedEmployeeWorkingInformation->timestamped_start_work_time;
            $carbon_timestamped_start_work_time = $this->getCarbonInstance($timestamped_start_work_time);

        } else {
            $carbon_timestamped_start_work_time = $this->getCarbonInstance($timestamped_start_work_time);
            if ($timestamped_start_work_time) {
                $round_up_by = $real_work_location->currentSetting()->start_time_round_up;

                $carbon_timestamped_start_work_time->minute = ceil($carbon_timestamped_start_work_time->minute/$round_up_by) * $round_up_by;
                $timestamped_start_work_time = $carbon_timestamped_start_work_time->format('Y-m-d H:i:s');
            }
        }

        $cache->timestamped_start_work_time = $timestamped_start_work_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TimestampedEndWorkTime - did declare the carbon instance. This attribute, we cach it, but not use it.
        // For using it, the timestamps distribute mechanism (in WorkingTimestampSubscriber) will get messed up.
        $timestamped_end_work_time = isset($this->attributes['timestamped_end_work_time']) ? $this->attributes['timestamped_end_work_time'] : null;

        if ($this->temporary != true && $this->concludedEmployeeWorkingInformation) {
            $timestamped_end_work_time = $this->concludedEmployeeWorkingInformation->timestamped_end_work_time;
            $carbon_timestamped_end_work_time = $this->getCarbonInstance($timestamped_end_work_time);

        } else {
            $carbon_timestamped_end_work_time = $this->getCarbonInstance($timestamped_end_work_time);
            if ($timestamped_end_work_time) {
                $round_down_by = $real_work_location->currentSetting()->end_time_round_down;

                $carbon_timestamped_end_work_time->minute = floor($carbon_timestamped_end_work_time->minute/$round_down_by) * $round_down_by;
                $timestamped_end_work_time = $carbon_timestamped_end_work_time->format('Y-m-d H:i:s');
            }
        }

        $cache->timestamped_end_work_time = $timestamped_end_work_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalAtWorkTimeInMinutes - no need to cach this
        $total_at_work_time_in_minutes = 0;
        $break_time = $schedule_break_time !== null ? $schedule_break_time : 0;
        $total_time = $schedule_working_hour ? $this->convertToMinutes($carbon_schedule_working_hour) + $break_time : $break_time;
        $total_at_work_time_in_minutes = $total_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RemainingPlannedWorkSpanTimeInMinutes - no need to cach this
        $remaining_planned_work_span_time_in_minutes = 0;

        $remaining_time = $total_at_work_time_in_minutes;
        if ($remaining_time > 0) {
            $late_time = ($planned_late_time !== null) ? $planned_late_time : 0;
            $early_leave = ($planned_early_leave_time !== null) ? $planned_early_leave_time : 0;
            $planned_break = ($planned_break_time !== null) ? $planned_break_time : 0;

            $remaining_time = $remaining_time - $late_time - $early_leave - $total_paid_rest_time_in_minutes - $planned_break;

            $remaining_planned_work_span_time_in_minutes = ($remaining_time >= 0) ? $remaining_time : 0;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        // //** IsPaidRestTimeInsideEstimatedWorkTime - no need to cach this
        // $is_paid_rest_time_inside_estimated_work_time = true;
        // if ($paid_rest_time_start !== null && $paid_rest_time_end !== null && $timestamped_start_work_time !== null) {
        //     $carbon_maximum_end_work_time = $carbon_timestamped_start_work_time->copy()->addMinutes($total_at_work_time_in_minutes);

        //     if ($carbon_timestamped_start_work_time->lte($carbon_paid_rest_time_start) &&
        //     $carbon_paid_rest_time_start->lte($carbon_paid_rest_time_end) &&
        //     $carbon_paid_rest_time_end->lte($carbon_maximum_end_work_time)) {

        //         $is_paid_rest_time_inside_estimated_work_time = true;
        //     } else {
        //         $is_paid_rest_time_inside_estimated_work_time = false;
        //     }
        // }
        // ///////////////////////////////////////////////////////////////////////////////////////////


        //** OffsetTimeToAddOrSubWhenTakeHalfDayOff - no need to cach this - did declare the zero carbon instance
        $offset_time_to_add_or_sub_when_take_half_day_off = 0;
        if ($schedule_start_work_time && $schedule_end_work_time) {
            // $carbon_schedule_start_work_time
            // $carbon_schedule_end_work_time
            $total_minutes = $carbon_schedule_start_work_time->diffInMinutes($carbon_schedule_end_work_time);

            // ((a + c) + c)/2 = a/2 + c. And [a/2 + c] is what we need.
            $offset_time_to_add_or_sub_when_take_half_day_off = ($total_minutes + $schedule_break_time)/2;

        } elseif ($schedule_working_hour) {
            // $carbon_schedule_working_hour
            $carbon_zero = $this->getCarbonInstance('0:0:0');

            $offset_time_to_add_or_sub_when_take_half_day_off = ($carbon_schedule_working_hour->diffInMinutes($carbon_zero))/2;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkSpan - did declare the carbon instance
        $planned_work_span = null;

        if ($take_a_whole_day_off !== true && $total_at_work_time_in_minutes > 0) {
            $total_work_span_in_minutes = $total_at_work_time_in_minutes - $planned_break_time;

            // In these case, the totalAtWorkTime is still the sum of break time and working hour, but the break time is automatically set to 0, that's why we have to subtract the break time once more before calculating
            if ($is_planned_rest_status_goukyuu_or_zenkyuu) {
                $total_work_span_in_minutes -= $schedule_break_time;
            }

            $total_work_span_in_minutes -= $total_paid_rest_time_in_minutes;

            $total_work_span_in_minutes -= $planned_late_time ?? 0;

            $total_work_span_in_minutes -= $planned_early_leave_time ?? 0;

            // With the new specs, the PlannedWorkSpan is limited to smaller than or equal to ScheduleWorkingHour
            $schedule_working_hour_in_minutes = $this->convertToMinutes($carbon_schedule_working_hour);
            $total_work_span_in_minutes = ($total_work_span_in_minutes > $schedule_working_hour_in_minutes) ? $schedule_working_hour_in_minutes : $total_work_span_in_minutes;
            $planned_work_span = ($total_work_span_in_minutes > 0) ? $this->minutesToString($total_work_span_in_minutes) : '00:00:00';
        }

        $cache->planned_work_span = $planned_work_span;
        $carbon_planned_work_span = $this->getCarbonInstance($planned_work_span);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkSpanStart - did declare carbon instance
        $planned_work_span_start = null;

        if (!$take_a_whole_day_off && $remaining_planned_work_span_time_in_minutes > 0) {

            // $carbon_schedule_start_work_time
            // $carbon_schedule_end_work_time

            if ($carbon_schedule_start_work_time && $carbon_schedule_end_work_time) {

                // Because we will need to mutate this variable, so we need to make a clone
                $carbon_schedule_start = $carbon_schedule_start_work_time->copy();

                $offset = 0;
                if ($is_planned_rest_status_zenkyuu) {
                    $offset = $offset_time_to_add_or_sub_when_take_half_day_off;

                } elseif ($is_rest_status_unit_day == false && $paid_rest_time_start && $paid_rest_time_end) {
                    // $carbon_paid_rest_time_start
                    // $carbon_paid_rest_time_end
                    if ($carbon_paid_rest_time_start->eq($carbon_schedule_start)) {
                        $offset = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

                        // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be added to this offset
                        if ($not_include_break_time_when_display_planned_time == true) {
                            $offset += $schedule_break_time;
                        }

                    } else {
                        $diff_from_schedule_start = $carbon_paid_rest_time_start->diffInMinutes($carbon_schedule_start);
                        if ($diff_from_schedule_start <= $planned_late_time) {
                            $offset = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

                            // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be added to this offset
                            if ($not_include_break_time_when_display_planned_time == true) {
                                $offset += $schedule_break_time;
                            }
                        }
                    }
                }

                if ($offset > 0) {
                    $carbon_schedule_start->addMinutes($offset);
                }
                if ($planned_late_time) {
                    $carbon_schedule_start->addMinutes($planned_late_time);
                }

                $planned_work_span_start = $carbon_schedule_start->format('Y-m-d H:i:s');
            }
        }

        $cache->planned_work_span_start = $planned_work_span_start;
        $carbon_planned_work_span_start = $this->getCarbonInstance($planned_work_span_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkSpanEnd - did declare the carbon instance
        $planned_work_span_end = null;

        if (!$take_a_whole_day_off && $remaining_planned_work_span_time_in_minutes > 0) {

            // $carbon_schedule_start_work_time
            // $carbon_schedule_end_work_time

            if ($carbon_schedule_start_work_time && $carbon_schedule_end_work_time) {

                // This one will be mutated
                $carbon_schedule_end = $carbon_schedule_end_work_time->copy();

                $offset = 0;
                if ($is_planned_rest_status_goukyuu) {
                    $offset = $offset_time_to_add_or_sub_when_take_half_day_off;

                } elseif ($is_rest_status_unit_day == false && $paid_rest_time_start && $paid_rest_time_end) {
                    // $carbon_paid_rest_time_start
                    // $carbon_paid_rest_time_end
                    if ($carbon_paid_rest_time_end->eq($carbon_schedule_end)) {
                        $offset = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

                        // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be added to this offset
                        if ($not_include_break_time_when_display_planned_time == true) {
                            $offset += $schedule_break_time;
                        }

                    } else {
                        $diff_from_schedule_end = $carbon_schedule_end->diffInMinutes($carbon_paid_rest_time_end);
                        if ($diff_from_schedule_end <= $planned_early_leave_time) {
                            $offset = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

                            // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be added to this offset
                            if ($not_include_break_time_when_display_planned_time == true) {
                                $offset += $schedule_break_time;
                            }
                        }
                    }

                }

                if ($offset > 0) {
                    $carbon_schedule_end->subMinutes($offset);
                }
                if ($planned_early_leave_time) {
                    $carbon_schedule_end->subMinutes($planned_early_leave_time);
                }

                $planned_work_span_end = $carbon_schedule_end->format('Y-m-d H:i:s');

            }
        }

        $cache->planned_work_span_end = $planned_work_span_end;
        $carbon_planned_work_span_end = $this->getCarbonInstance($planned_work_span_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedOvertimeDueToReduceBreakTime
        $planned_overtime_due_to_reduce_break_time = 0;

        if ($planned_work_span) {
            $planned_overtime_due_to_reduce_break_time = $total_at_work_time_in_minutes;

            $planned_overtime_due_to_reduce_break_time -= $this->convertToMinutes($carbon_planned_work_span);

            $planned_overtime_due_to_reduce_break_time -= $total_paid_rest_time_in_minutes;

            $planned_overtime_due_to_reduce_break_time -= $planned_late_time ?? 0;

            $planned_overtime_due_to_reduce_break_time -= $planned_early_leave_time ?? 0;

            $planned_overtime_due_to_reduce_break_time -= $planned_break_time ?? 0;

            // In these case, the totalAtWorkTime is still the sum of break time and working hour, but the break time is automatically set to 0, that's why we have to subtract the break time once more before calculating
            if ($is_planned_rest_status_goukyuu_or_zenkyuu) {
                $planned_overtime_due_to_reduce_break_time -= $schedule_break_time;
            }

            $planned_overtime_due_to_reduce_break_time = $planned_overtime_due_to_reduce_break_time > 0 ? $planned_overtime_due_to_reduce_break_time : 0;
        }
        $cache->planned_overtime_due_to_reduce_break_time = $planned_overtime_due_to_reduce_break_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedStartWorkTime - did declare the carbon instance
        $planned_start_work_time = null;

        if ($is_planned_work_status_houde_or_kyuude_or_zangyou) {
            $planned_start_work_time = $planned_overtime_start;
        } elseif ($total_planned_early_arrive_time_in_minutes > 0) {
            $planned_start_work_time = ($planned_work_span_start !== null && $planned_work_span_end !== null) ? $planned_early_arrive_start : null;
        } else {
            $planned_start_work_time = $planned_work_span_start;
        }

        $cache->planned_start_work_time = $planned_start_work_time;
        $carbon_planned_start_work_time = $this->getCarbonInstance($planned_start_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedEndWorkTime - did declare the carbon instance
        $planned_end_work_time = null;

        if ($is_planned_work_status_houde_or_kyuude_or_zangyou) {
            $planned_end_work_time = $planned_overtime_end;
        } elseif ($total_planned_overtime_in_minutes > 0) {
            $planned_end_work_time = ($planned_work_span_start !== null && $planned_work_span_end !== null) ? $planned_overtime_end : null;
        } else {
            $planned_end_work_time = $planned_work_span_end;
        }

        $cache->planned_end_work_time = $planned_end_work_time;
        $carbon_planned_end_work_time = $this->getCarbonInstance($planned_end_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkingHour - did declare the carbon instance
        $planned_working_hour = null;

        if (!$take_a_whole_day_off && $remaining_planned_work_span_time_in_minutes > 0) {
            $total_planned_work_span_in_minutes = $this->convertToMinutes($carbon_planned_work_span);

            $total_planned_work_span_in_minutes += $total_planned_early_arrive_time_in_minutes + $total_planned_overtime_in_minutes;

            if ($is_planned_rest_status_goukyuu_or_zenkyuu != true) {
                $total_planned_work_span_in_minutes += $planned_overtime_due_to_reduce_break_time;
            }

            $planned_working_hour = $this->minutesToString($total_planned_work_span_in_minutes);
        } else if ($is_planned_work_status_houde_or_kyuude_or_zangyou) {
            // $carbon_planned_start_work_time
            // $carbon_planned_end_work_time

            if ($carbon_planned_start_work_time !== null && $carbon_planned_end_work_time !== null) {
                $break_time = $planned_break_time !== null ? $planned_break_time : 0;
                $total_working_hour_in_minutes = $carbon_planned_start_work_time->diffInMinutes($carbon_planned_end_work_time) - $break_time;

                $planned_working_hour = $this->minutesToString($total_working_hour_in_minutes);
            }

        }

        $cache->planned_working_hour = $planned_working_hour;
        $carbon_planned_working_hour = $this->getCarbonInstance($planned_working_hour);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealBreakTime
        $real_break_time = isset($this->attributes['real_break_time']) ? $this->attributes['real_break_time'] : null;

        if ($timestamped_start_work_time === null ||
            $timestamped_end_work_time === null ||
            $take_a_whole_day_off == true ||
            $is_planned_work_status_kekkin_or_furikyuu == true ||
            $is_planned_rest_status_goukyuu_or_zenkyuu == true) {

            $real_break_time = null;

        } elseif ($real_break_time === null) {

            if ($timestamped_start_work_time !== null && $timestamped_end_work_time !== null) {
                $real_break_time = $planned_break_time;
            }
        }

        $cache->real_break_time = $real_break_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** CarbonEstimateEndWorkTime - no need to cach this. !!!NOTE!!!: this is a carbon instance
        $carbon_estimated_end_work_time = null;

        if ($is_only_working_hour_and_break_time_mode == true && $timestamped_start_work_time !== null) {

            if ($planned_overtime_start !== null && $planned_overtime_end !== null) {
                $carbon_estimated_end_work_time = $carbon_planned_overtime_end->copy();

            } else {
                $early_leave = ($planned_early_leave_time !== null) ? $planned_early_leave_time : 0;
                $max_working_hour_in_minutes = $total_at_work_time_in_minutes - $early_leave - $total_paid_rest_time_in_minutes;

                if ($is_planned_rest_status_goukyuu_or_zenkyuu) {
                    $break_time_from_the_schedule = ($schedule_break_time !== null) ? $schedule_break_time : 0;
                    $max_working_hour_in_minutes -= $break_time_from_the_schedule;
                }

                // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be subtracted from this
                if ($not_include_break_time_when_display_planned_time == true) {
                    $break_time_from_the_schedule = ($schedule_break_time !== null) ? $schedule_break_time : 0;
                    $max_working_hour_in_minutes -= $break_time_from_the_schedule;
                }

                $carbon_estimated_end_work_time = $carbon_timestamped_start_work_time->copy();
                $carbon_estimated_end_work_time->addMinutes($max_working_hour_in_minutes);
            }

        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** CarbonEstimatedLatestStartWorkTme - no need to cached this
        $carbon_estimated_latest_start_work_time = null;

        if ($is_only_working_hour_and_break_time_mode == true && $planned_overtime_start !== null && $planned_overtime_end !== null) {
            $work_time_in_minutes = $this->convertToMinutes($carbon_planned_work_span);
            $break_time_in_minutes = $planned_break_time !== null ? $planned_break_time : 0;

            // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be subtracted from this
            $to_be_subtracted_time = $work_time_in_minutes + $break_time_in_minutes;
            if ($not_include_break_time_when_display_planned_time == true) {
                $break_time_from_the_schedule = ($schedule_break_time !== null) ? $schedule_break_time : 0;
                $to_be_subtracted_time -= $break_time_from_the_schedule;
            }

            $carbon_estimated_latest_start_work_time = $carbon_planned_overtime_start->copy()->subMinutes($to_be_subtracted_time);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealStartWorkTime - did declare the carbon instance
        $real_start_work_time = null;
        if ($timestamped_start_work_time !== null && !$is_planned_work_status_kekkin_or_furikyuu && !$take_a_whole_day_off) {
            // $carbon_timestamped_start_work_time
            // $carbon_planned_start_work_time
            // $carbon_paid_rest_time_start
            // $carbon_paid_rest_time_end

            if ($paid_rest_time_start !== null && $paid_rest_time_end !== null &&
                ($carbon_paid_rest_time_start->lte($carbon_timestamped_start_work_time) && $carbon_timestamped_start_work_time->lte($carbon_paid_rest_time_end))) {

                // Update 2018-12-25 in the case of not_include_break_time... is on, we have to remove the schedule_breal_time from display time
                if ($not_include_break_time_when_display_planned_time == true) {
                    $carbon_paid_rest_time_end->addMinutes($schedule_break_time);
                }
                $real_start_work_time = $carbon_planned_start_work_time->max($carbon_paid_rest_time_end)->format('Y-m-d H:i:s');

            } else if ($planned_start_work_time !== null) {
                $real_start_work_time = $carbon_planned_start_work_time->max($carbon_timestamped_start_work_time)->format('Y-m-d H:i:s');

            } elseif ($schedule_working_hour !== null) {
                if ($total_planned_early_arrive_time_in_minutes > 0) {
                    // $carbon_planned_early_arrive_start
                    $real_start_work_time = $carbon_planned_early_arrive_start->max($carbon_timestamped_start_work_time)->format('Y-m-d H:i:s');

                } else if ($carbon_estimated_latest_start_work_time !== null) {
                    $real_start_work_time = $carbon_estimated_latest_start_work_time->max($carbon_timestamped_start_work_time)->format('Y-m-d H:i:s');
                } else {
                    $real_start_work_time = $carbon_timestamped_start_work_time->format('Y-m-d H:i:s');
                }
            }
        }

        $cache->real_start_work_time = $real_start_work_time;
        $carbon_real_start_work_time = $this->getCarbonInstance($real_start_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealEndWorkTime
        $real_end_work_time = null;

        if ($timestamped_end_work_time !== null && !$is_planned_work_status_kekkin_or_furikyuu && !$take_a_whole_day_off) {
            // $carbon_timestamped_end_work_time
            // $carbon_planned_end_work_time
            // $carbon_paid_rest_time_start
            // $carbon_paid_rest_time_end

            if ($paid_rest_time_start !== null && $paid_rest_time_end !== null &&
                ($carbon_paid_rest_time_start->lte($carbon_timestamped_end_work_time) && $carbon_timestamped_end_work_time->lte($carbon_paid_rest_time_end))) {

                // Update 2018-12-25 in the case of not_include_break_time... is on, we have to remove the schedule_breal_time from display time
                if ($not_include_break_time_when_display_planned_time == true) {
                    $carbon_paid_rest_time_start->subMinutes($schedule_break_time);
                }
                $real_end_work_time = $carbon_planned_end_work_time->min($carbon_paid_rest_time_start)->format('Y-m-d H:i:s');

            } else if ($planned_end_work_time !== null) {
                $real_end_work_time = $carbon_planned_end_work_time->min($carbon_timestamped_end_work_time)->format('Y-m-d H:i:s');

            } elseif ($schedule_working_hour !== null) {

                if ($total_planned_overtime_in_minutes > 0) {
                    // $carbon_planned_overtime_end
                    $real_end_work_time = $carbon_planned_overtime_end->min($carbon_timestamped_end_work_time)->format('Y-m-d H:i:s');

                } elseif ($carbon_estimated_end_work_time !== null) {
                    $real_end_work_time = $carbon_timestamped_end_work_time->min($carbon_estimated_end_work_time)->format('Y-m-d H:i:s');
                } else {
                    $real_end_work_time = $carbon_timestamped_end_work_time->format('Y-m-d H:i:s');
                }
            }
        }

        $cache->real_end_work_time = $real_end_work_time;
        $carbon_real_end_work_time = $this->getCarbonInstance($real_end_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealEarlyArriveStart - did declare the carbon instance
        $real_early_arrive_start = null;

        // $carbon_planned_early_arrive_start
        // $carbon_planned_early_arrive_end
        // $carbon_real_start_work_time
        if ($carbon_planned_early_arrive_start !== null && $carbon_planned_early_arrive_end !== null && $carbon_real_start_work_time !== null) {

            if ($carbon_real_start_work_time->lt($carbon_planned_early_arrive_start)) {
                $real_early_arrive_start = $carbon_planned_early_arrive_start->format('Y-m-d H:i:s');
            } elseif ($carbon_planned_early_arrive_start->lte($carbon_real_start_work_time) && $carbon_real_start_work_time->lt($carbon_planned_early_arrive_end)) {
                $real_early_arrive_start = $carbon_real_start_work_time->format('Y-m-d H:i:s');
            }
        }

        $cache->real_early_arrive_start = $real_early_arrive_start;
        $carbon_real_early_arrive_start = $this->getCarbonInstance($real_early_arrive_start);
        ///////////////////////////////////////////////////////////////////////////////////////////



        //** RealEarlyArriveEnd - did declare the carbon instance
        $real_early_arrive_end = null;

        // $carbon_planned_early_arrive_end
        // $carbon_real_start_work_time
        if ($carbon_planned_early_arrive_end !== null && $carbon_real_start_work_time !== null) {
            if ($carbon_real_start_work_time->lt($carbon_planned_early_arrive_end)) {
                $real_early_arrive_end = $carbon_planned_early_arrive_end->format('Y-m-d H:i:s');
            }
        }

        $cache->real_early_arrive_end = $real_early_arrive_end;
        $carbon_real_early_arrive_end = $this->getCarbonInstance($real_early_arrive_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalRealEarlyArriveTimeInMinutes - no need to cach this
        $total_real_early_arrive_time_in_minutes = 0;

        if ($carbon_real_early_arrive_start !== null && $carbon_real_early_arrive_end !== null) {
            $total_real_early_arrive_time_in_minutes = $carbon_real_early_arrive_end->diffInMinutes($carbon_real_early_arrive_start);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealOvertimeStart - did declare the carbon instance
        $real_overtime_start = null;

        // $carbon_planned_overtime_start
        // $carbon_real_end_work_time

        if ($carbon_planned_overtime_start !== null && $carbon_planned_overtime_end !== null && $carbon_real_start_work_time !== null && $carbon_real_end_work_time !== null) {
            if ($carbon_planned_overtime_start->lt($carbon_real_end_work_time)) {
                $real_overtime_start = $carbon_planned_overtime_start->max($carbon_real_start_work_time)->format('Y-m-d H:i:s');
            }
        }

        $cache->real_overtime_start = $real_overtime_start;
        $carbon_real_overtime_start = $this->getCarbonInstance($real_overtime_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealOvertimeEnd - did declare the carbon instance
        $real_overtime_end = null;

        // $carbon_planned_overtime_start
        // $carbon_planned_overtime_end
        // $carbon_real_end_work_time
        if ($carbon_planned_overtime_start !== null && $carbon_planned_overtime_end !== null && $carbon_real_end_work_time !== null) {
            if ($carbon_real_end_work_time->lte($carbon_planned_overtime_start)) {
                $real_overtime_end = null;
            } elseif ($carbon_planned_overtime_start->lt($carbon_real_end_work_time) && $carbon_real_end_work_time->lte($carbon_planned_overtime_end)) {
                $real_overtime_end = $carbon_real_end_work_time->format('Y-m-d H:i:s');
            } else {
                $real_overtime_end = $carbon_planned_overtime_end->format('Y-m-d H:i:s');
            }
        }

        $cache->real_overtime_end = $real_overtime_end;
        $carbon_real_overtime_end = $this->getCarbonInstance($real_overtime_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalRealOvertimeInMinutes - no need to cach this
        $total_real_overtime_in_minutes = 0;

        // $carbon_real_overtime_start
        // $carbon_real_overtime_end
        if ($carbon_real_overtime_start !== null && $carbon_real_overtime_end !== null) {
            $total_real_overtime_in_minutes = $carbon_real_overtime_end->diffInMinutes($carbon_real_overtime_start);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkSpanStart - did declare the carbon instance
        $real_work_span_start = null;

        if ($real_start_work_time !== null && $planned_work_span_start !== null) {
            // $carbon_real_start_work_time
            // $carbon_planned_work_span_start

            $real_work_span_start = $carbon_planned_work_span_start->max($carbon_real_start_work_time)->format('Y-m-d H:i:s');
        }

        $cache->real_work_span_start = $real_work_span_start;
        $carbon_real_work_span_start = $this->getCarbonInstance($real_work_span_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkSpanEnd - did declare the carbon instance
        $real_work_span_end = null;

        if ($real_end_work_time !== null && $planned_work_span_end !== null) {
            // $carbon_real_end_work_time
            // $carbon_planned_work_span_end

            $real_work_span_end = $carbon_real_end_work_time->min($carbon_planned_work_span_end)->format('Y-m-d H:i:s');
        }

        $cache->real_work_span_end = $real_work_span_end;
        $carbon_real_work_span_end = $this->getCarbonInstance($real_work_span_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkSpan - did declare the carbon instance
        $real_work_span = null;

        // $carbon_real_work_span_start
        // $carbon_real_work_span_end
        // $carbon_real_start_work_time
        // $carbon_real_end_work_time
        // $carbon_planned_work_span
        if ($carbon_real_work_span_start !== null && $carbon_real_work_span_end !== null) {
            $total_work_time_in_minutes = $carbon_real_work_span_start->diffInMinutes($carbon_real_work_span_end, false) - $real_break_time;

            if ($carbon_paid_rest_time_start !== null && $carbon_paid_rest_time_end !== null) {
                if ($carbon_real_start_work_time->lte($carbon_paid_rest_time_start) && $carbon_paid_rest_time_end->lte($carbon_real_end_work_time)) {
                    $total_work_time_in_minutes -= $total_paid_rest_time_in_minutes;

                // Update 2018-12-25, In the particular branch of this flow, if the not_include_break_time... is on,
                // we have to add the schedule_break_time back to the total_working_hour
                } else if ($not_include_break_time_when_display_planned_time == true) {
                    $total_work_time_in_minutes += $schedule_break_time ? $schedule_break_time : 0;
                }
            }

            // Ensure the RealWorkSpan is smaller than or equal to ScheduleWorkingHour
            $schedule_working_hour_in_minutes = $this->convertToMinutes($carbon_schedule_working_hour);
            $total_work_time_in_minutes = ($total_work_time_in_minutes > $schedule_working_hour_in_minutes) ? $schedule_working_hour_in_minutes : $total_work_time_in_minutes;
            $real_work_span = $this->minutesToString($total_work_time_in_minutes);

        } else if ($carbon_real_start_work_time !== null && $carbon_real_end_work_time !== null && $carbon_planned_work_span !== null) {
            $total_work_time_in_minutes = $carbon_real_start_work_time->diffInMinutes($carbon_real_end_work_time, false) - $real_break_time;
            $total_work_time_in_minutes -= ($total_real_early_arrive_time_in_minutes + $total_real_overtime_in_minutes);
            // Calculate the paid rest time
            // $carbon_paid_rest_time_start
            // $carbon_paid_rest_time_end

            if ($carbon_paid_rest_time_start !== null && $carbon_paid_rest_time_end !== null) {
                if ($carbon_real_start_work_time->lte($carbon_paid_rest_time_start) && $carbon_paid_rest_time_end->lte($carbon_real_end_work_time)) {
                    $total_work_time_in_minutes -= $total_paid_rest_time_in_minutes;
                }
            } else if ($paid_rest_time_period_in_minutes > 0) {
                if ($not_include_break_time_when_display_planned_time == true) {
                    $total_work_time_in_minutes += $schedule_break_time ? $schedule_break_time : 0;
                }
            }

            // Ensure the RealWorkSpan is smaller than or equal to ScheduleWorkingHour
            $schedule_working_hour_in_minutes = $this->convertToMinutes($carbon_schedule_working_hour);
            $total_work_time_in_minutes = ($total_work_time_in_minutes > $schedule_working_hour_in_minutes) ? $schedule_working_hour_in_minutes : $total_work_time_in_minutes;
            $real_work_span = $this->minutesToString($total_work_time_in_minutes);
        }

        $cache->real_work_span = $real_work_span;
        $carbon_real_work_span = $this->getCarbonInstance($real_work_span);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealEarlyLeaveTime
        $real_early_leave_time = null;

        if ($schedule_end_work_time !== null && $real_end_work_time !== null) {
            // $carbon_schedule_end_work_time
            // $carbon_real_end_work_time

            if ($carbon_real_end_work_time->lt($carbon_schedule_end_work_time)) {
                $early_leave = $carbon_schedule_end_work_time->diffInMinutes($carbon_real_end_work_time);

                if ($paid_rest_time_start !== null && $paid_rest_time_end !== null) {
                    // $carbon_paid_rest_time_start
                    // $carbon_paid_rest_time_end

                    if ($carbon_real_end_work_time->lte($carbon_paid_rest_time_start)) {
                        $early_leave -= $total_paid_rest_time_in_minutes;

                        // Update 2018-12-25 in the case of not_include_break_time... AND the real_end_work_time is BEFORE the paid_rest_start,
                        // we have to subtract the schedule_break_time from the early_leave_time
                        if ($not_include_break_time_when_display_planned_time == true && $carbon_real_end_work_time->lt($carbon_paid_rest_time_start)) {
                            $early_leave -= ($schedule_break_time) ? $schedule_break_time : 0;
                        }

                    // This flow is just for show apparently
                    } elseif ($carbon_real_end_work_time->lt($carbon_paid_rest_time_end)) {
                        $diff = $carbon_paid_rest_time_end->diffInMinutes($carbon_real_end_work_time);
                        $early_leave -= $diff;
                    }

                } elseif ($is_planned_rest_status_goukyuu) {
                    $early_leave = $early_leave - $total_paid_rest_time_in_minutes - $schedule_break_time;
                }
                $real_early_leave_time = $early_leave;
            }
        } elseif ($real_end_work_time !== null && $carbon_estimated_end_work_time !== null) {
            // In WorkingHourOnly mode, when there is planned overtime, we have to use the planned_overtime_start instead of the estimated_end_work_time
            $carbon_estimated_official_end_work_time = ($planned_overtime_start !== null && $planned_overtime_end !== null) ? $carbon_planned_overtime_start : $carbon_estimated_end_work_time;

            if ($carbon_real_end_work_time->lt($carbon_estimated_official_end_work_time)) {
                $real_early_leave_time = ($planned_early_leave_time !== null) ? $planned_early_leave_time + $carbon_real_end_work_time->diffInMinutes($carbon_estimated_official_end_work_time) :
                                                                                $carbon_real_end_work_time->diffInMinutes($carbon_estimated_official_end_work_time);
            } else if ($planned_early_leave_time !== null) {
                $real_early_leave_time = $planned_early_leave_time;
            }
        }

        $cache->real_early_leave_time = $real_early_leave_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealLateTime
        $real_late_time = null;

        if ($real_start_work_time !== null && $schedule_start_work_time !== null) {
            // $carbon_real_start_work_time
            // $carbon_schedule_start_work_time

            if ($carbon_real_start_work_time->gt($carbon_schedule_start_work_time)) {

                $late_time = $carbon_real_start_work_time->diffInMinutes($carbon_schedule_start_work_time);

                if ($paid_rest_time_start !== null && $paid_rest_time_end !== null) {
                    // $carbon_paid_rest_time_start
                    // $carbon_paid_rest_time_end

                    if ($carbon_paid_rest_time_end->lte($carbon_real_start_work_time)) {
                        $late_time -= $total_paid_rest_time_in_minutes;

                        // Update 2018-12-25 in the case of not_include_break_time... AND the real_end_work_time is BEFORE the paid_rest_start,
                        // we have to subtract the schedule_break_time from the late_time
                        if ($not_include_break_time_when_display_planned_time == true && $carbon_paid_rest_time_end->lt($carbon_real_start_work_time)) {
                            $late_time -= ($schedule_break_time) ? $schedule_break_time : 0;
                        }

                    // This flow is just for show apparently
                    } elseif ($carbon_paid_rest_time_start->lt($carbon_real_start_work_time)) {
                        $diff = $carbon_real_start_work_time->diffInMinutes($carbon_paid_rest_time_start);
                        $late_time -= $diff;
                    }
                } elseif ($is_planned_rest_status_zenkyuu) {
                    $late_time = $late_time - $total_paid_rest_time_in_minutes - $schedule_break_time;
                }
                $real_late_time = $late_time;
            }
        } elseif ($real_start_work_time !== null && $carbon_estimated_latest_start_work_time !== null) {
            if ($carbon_estimated_latest_start_work_time->lt($carbon_real_start_work_time)) {
                $real_late_time = $carbon_estimated_latest_start_work_time->diffInMinutes($carbon_real_start_work_time);
            }
        }

        $cache->real_late_time = $real_late_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealOvertimeDueToReduceBreakTime
        $real_overtime_due_to_reduce_break_time = 0;

        if ($real_work_span) {
            $real_overtime_due_to_reduce_break_time = $total_at_work_time_in_minutes;

            $real_overtime_due_to_reduce_break_time -= $this->convertToMinutes($carbon_real_work_span);

            $real_overtime_due_to_reduce_break_time -= $total_paid_rest_time_in_minutes;

            $real_overtime_due_to_reduce_break_time -= $real_late_time ?? 0;

            $real_overtime_due_to_reduce_break_time -= $real_early_leave_time ?? 0;

            $real_overtime_due_to_reduce_break_time -= $real_break_time ?? 0;

            // In these case, the totalAtWorkTime is still the sum of break time and working hour, but the break time is automatically set to 0, that's why we have to subtract the break time once more before calculating
            if ($is_planned_rest_status_goukyuu_or_zenkyuu) {
                $real_overtime_due_to_reduce_break_time -= $schedule_break_time;
            }

            $real_overtime_due_to_reduce_break_time = $real_overtime_due_to_reduce_break_time > 0 ? $real_overtime_due_to_reduce_break_time : 0;
        }
        $cache->real_overtime_due_to_reduce_break_time = $real_overtime_due_to_reduce_break_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkingHour
        $real_working_hour = null;

        if ($real_start_work_time !== null && $real_end_work_time !== null && $real_work_span !== null) {
            $total_real_work_span_in_minutes = $this->convertToMinutes($carbon_real_work_span);

            $total_real_work_span_in_minutes += $total_real_early_arrive_time_in_minutes + $total_real_overtime_in_minutes;

            if ($is_planned_rest_status_goukyuu_or_zenkyuu != true) {
                $total_real_work_span_in_minutes += $real_overtime_due_to_reduce_break_time;
            }

            $real_working_hour = $this->minutesToString($total_real_work_span_in_minutes);

        } else if ($is_planned_work_status_houde_or_kyuude_or_zangyou) {
            // $carbon_real_start_work_time
            // $carbon_real_end_work_time

            if ($carbon_real_start_work_time !== null && $carbon_real_end_work_time !== null) {
                $break_time = $real_break_time !== null ? $real_break_time : 0;
                $total_working_hour_in_minutes = $carbon_real_start_work_time->diffInMinutes($carbon_real_end_work_time) - $break_time;

                $real_working_hour = $this->minutesToString($total_working_hour_in_minutes);
            }
        }

        $cache->real_working_hour = $real_working_hour;
        ///////////////////////////////////////////////////////////////////////////////////////////


        // After all the calculation, we flag this cache instance as 'not old' and save it.
        $cache->old = false;
        $cache->save();
        if ($cache_not_exist) $this->load('cachedEmployeeWorkingInformation');

    }

    /**
     * Get the sum of the planned/real_early_arrive time(end - start) in minutes
     *
     * @param string    $prefix     it's either 'planned' or 'real'
     * @return int
     */
    public function totalEarlyArriveTimeInMinutes($prefix = 'planned')
    {
        $carbon_start = $this->getCarbonInstance($this->{$prefix . '_early_arrive_start'});
        $carbon_end = $this->getCarbonInstance($this->{$prefix . '_early_arrive_end'});

        if ($carbon_start !== null && $carbon_end !== null) {
            return $carbon_end->diffInMinutes($carbon_start);
        } else {
            return 0;
        }
    }

    /**
     * Get the sum of the planned/real_overtime time(end - start) in minutes
     *
     * @param string    $prefix     it's either 'planned' or 'real'
     * @return int
     */
    public function totalOvertimeInMinutes($prefix = 'planned')
    {
        $carbon_start = $this->getCarbonInstance($this->{$prefix . '_overtime_start'});
        $carbon_end = $this->getCarbonInstance($this->{$prefix . '_overtime_end'});

        if ($carbon_start !== null && $carbon_end !== null) {
            $total_time_in_minutes = $carbon_end->diffInMinutes($carbon_start);

            if ($this->isPlannedWorkStatus(WorkStatus::HOUDE, WorkStatus::KYUUDE, WorkStatus::ZANGYOU)) {
                $total_time_in_minutes -= ($this->{$prefix . '_break_time'} !== null) ? $this->{$prefix . '_break_time'} : 0;
            }

            return $total_time_in_minutes;
        } else {
            return 0;
        }
    }

    /**
     * Get the sum of the paid rest time in minutes
     *
     * @return int
     */
    public function totalPaidRestTimeInMinutes()
    {
        $carbon_start = $this->getCarbonInstance($this->paid_rest_time_start);
        $carbon_end = $this->getCarbonInstance($this->paid_rest_time_end);
        $carbon_period = $this->getCarbonInstance($this->paid_rest_time_period);

        if ($this->isRestStatusUnitDayOrHour('hour') && $carbon_start !== null && $carbon_end !== null) {
            $total_paid_rest_time = $carbon_end->diffInMinutes($carbon_start);

            $carbon_schedule_working_hour = $this->getCarbonInstance($this->schedule_working_hour);
            $working_hour_in_minutes = $this->convertToMinutes($carbon_schedule_working_hour);
            return $total_paid_rest_time > $working_hour_in_minutes ? $working_hour_in_minutes : $total_paid_rest_time;

        } elseif ($this->isRestStatusUnitDayOrHour('hour') && $carbon_period !== null) {
            return $this->convertToMinutes($carbon_period);

        } elseif ($this->isPlannedRestStatus(RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2, RestStatus::GOKYUU_1, RestStatus::GOKYUU_2)) {
            $carbon_schedule_working_hour = $this->getCarbonInstance($this->schedule_working_hour);
            return ($carbon_schedule_working_hour !== null) ? ceil($this->convertToMinutes($carbon_schedule_working_hour)/2) : null;

        } else {
            return 0;
        }
    }


    /**
     * Estimate the end work time base on the maximum working hour and those overtime fields, and also the planned early leave time.
     *
     * @return Carbon|null
     */
    public function estimatedEndWorkTime()
    {
        if ($this->isOnlyWorkingHourAndBreakTimeMode() == true && $this->timestamped_start_work_time !== null) {

            if ($this->planned_overtime_start !== null && $this->planned_overtime_end !== null) {
                return $this->getCarbonInstance($this->planned_overtime_end);

            } else {
                $early_leave = ($this->planned_early_leave_time !== null) ? $this->planned_early_leave_time : 0;
                $max_working_hour_in_minutes = $this->totalAtWorkTimeInMinutes() - $early_leave - $this->totalPaidRestTimeInMinutes();

                if ($this->isPlannedRestStatus(RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2, RestStatus::GOKYUU_1, RestStatus::GOKYUU_2)) {
                    // In these cases, we have to remove the schedule_break_time from the total_at_work_time
                    $schedule_break_time = ($this->schedule_break_time !== null) ? $this->schedule_break_time : 0;
                    $max_working_hour_in_minutes -= $schedule_break_time;
                }

                // Update 2018-12-28: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be subtracted from this
                if ($this->not_include_break_time_when_display_planned_time == true) {
                    $schedule_break_time = ($this->schedule_break_time !== null) ? $this->schedule_break_time : 0;
                    $max_working_hour_in_minutes -= $schedule_break_time;
                }

                $carbon_timestamped_start_work = $this->getCarbonInstance($this->timestamped_start_work_time);
                $carbon_timestamped_start_work->addMinutes($max_working_hour_in_minutes);
                return $carbon_timestamped_start_work;
            }
        } else {
            return null;
        }
    }

    /**
     * Estimate the latest start work time base on the maximum working hour and those overtime fields.
     *
     * @return Carbon|null
     */
    public function estimatedLatestStartWorkTime()
    {
        if ($this->isOnlyWorkingHourAndBreakTimeMode() == true && $this->planned_overtime_start !== null && $this->planned_overtime_end !== null) {
            $carbon_planned_work_span = $this->getCarbonInstance($this->planned_work_span);
            $work_time_in_minutes = $carbon_planned_work_span->hour * 60 + $carbon_planned_work_span->minute;
            $break_time_in_minutes = $this->planned_break_time !== null ? $this->planned_break_time : 0;
            $carbon_planned_overtime_start = $this->getCarbonInstance($this->planned_overtime_start);

            // Update 2018-12-28: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be subtracted from this
            $to_be_subtracted_time = $work_time_in_minutes + $break_time_in_minutes;
            if ($this->not_include_break_time_when_display_planned_time == true) {
                $schedule_break_time = ($this->schedule_break_time !== null) ? $this->schedule_break_time : 0;
                $to_be_subtracted_time -= $schedule_break_time;
            }

            $carbon_planned_overtime_start->subMinutes($to_be_subtracted_time);
            return $carbon_planned_overtime_start;
        }
        return null;
    }


    /**
     * Get the planned special overtime (which is created by reducing planned break time).
     *
     * @return integer
     */
    public function getPlannedOvertimeDueToReduceBreakTime()
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->planned_overtime_due_to_reduce_break_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->planned_overtime_due_to_reduce_break_time;
        }
    }

    /**
     * Get the real special overtime (which is created by reducing real break time).
     *
     * @return integer
     */
    public function getRealOvertimeDueToReduceBreakTime()
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $this->cachedEmployeeWorkingInformation->real_overtime_due_to_reduce_break_time;
        } else {
            $this->calculateAndCacheAttributes();
            return $this->cachedEmployeeWorkingInformation->real_overtime_due_to_reduce_break_time;
        }
    }


    /**
     * Check if this EWI has Paid Rest Hour to full (meaning all the working hour has been taken as paid rest hour)
     *
     * @return boolean
     */
    public function takeAllWorkingHourAsPaidRestTime()
    {
        $carbon_schedule_working_hour = $this->getCarbonInstance($this->schedule_working_hour);

        if ($carbon_schedule_working_hour != null) {
            $schedule_working_hour_in_minutes = $this->convertToMinutes($this->getCarbonInstance($this->schedule_working_hour));
            $paid_rest_time = $this->totalPaidRestTimeInMinutes();
            return $paid_rest_time != 0 && $paid_rest_time >= $schedule_working_hour_in_minutes;
        }
        return false;
    }


    /**
     * Get the WorkLocation that this instance is being affected by
     *
     * @return WorkLocation
     */
    public function affectedByWorkLocation()
    {
        if (!$this->affected_by_work_location) {
            $work_location_setting_service = resolve(WorkLocationSettingService::class);

            // $this->affected_by_work_location = $this->getInstanceOfPlannedWorkLocation();
            $this->affected_by_work_location = $work_location_setting_service->getWorkLocationById($this->planned_work_location_id);
        }

        return $this->affected_by_work_location;
    }

    /**
     * Get the real WorkLocation that this instance is being affected by
     *
     * @return WorkLocation
     */
    public function affectedByRealWorkLocation()
    {
        if (!$this->affected_by_real_work_location) {
            // Because right now, the real work location is to be the same with the planned work location, so we can do this.
            // $this->affected_by_real_work_location = $this->currentRealWorkLocation ? $this->currentRealWorkLocation : $this->currentPlannedWorkLocation;

            $work_location_setting_service = resolve(WorkLocationSettingService::class);
            $real_work_location_id = $this->real_work_location_id ? $this->real_work_location_id : $this->planned_work_location_id;
            $this->affected_by_real_work_location = $work_location_setting_service->getWorkLocationById($real_work_location_id);
        }

        return $this->affected_by_real_work_location;
    }

    /**
     * Check if this working information is in the 'Only-working-hour-and-break-time mode'
     *
     * @return boolean
     */
    public function isOnlyWorkingHourAndBreakTimeMode()
    {
        return ($this->schedule_start_work_time === null) && ($this->schedule_end_work_time === null) && ($this->schedule_working_hour !== null);
    }

    /**
     * The EmployeeWorkingInformation is a gigantic model with a lot of business logic inside of it. Just serialize it normally will cost a lot,
     * because then all the attributes will be calculated completely.
     * In most pages, we only need a some attributes of that model, not all, that's why we have to choose which attribute to expose or not. Doing this will
     * reduce the processed time much more than just let laravel convert the model to json like normal.
     *
     * @return array
     */
    public function necessaryDataForTheVueComponent()
    {
        $necessary_data = [
            'id'                                                    =>    $this->id,
            'schedule_start_work_time'                              =>    $this->schedule_start_work_time,
            'schedule_end_work_time'                                =>    $this->schedule_end_work_time,
            'schedule_break_time'                                   =>    $this->schedule_break_time,
            'schedule_night_break_time'                             =>    $this->schedule_night_break_time,
            'schedule_working_hour'                                 =>    $this->schedule_working_hour,
            'manually_inputed_schedule_start_work_time'             =>    $this->manually_inputed_schedule_start_work_time,
            'manually_inputed_schedule_end_work_time'               =>    $this->manually_inputed_schedule_end_work_time,
            'manually_inputed_schedule_break_time'                  =>    $this->manually_inputed_schedule_break_time,
            'manually_inputed_schedule_night_break_time'            =>    $this->manually_inputed_schedule_night_break_time,
            'manually_inputed_schedule_working_hour'                =>    $this->manually_inputed_schedule_working_hour,


            'planned_work_status_id'                                =>    $this->planned_work_status_id,
            'planned_rest_status_id'                                =>    $this->planned_rest_status_id,
            'paid_rest_time_start'                                  =>    $this->paid_rest_time_start,
            'paid_rest_time_end'                                    =>    $this->paid_rest_time_end,
            'paid_rest_time_period'                                 =>    $this->paid_rest_time_period,
            'planned_work_location_id'                              =>    $this->planned_work_location_id,
            'real_work_location_id'                                 =>    $this->real_work_location_id,
            'planned_work_address_id'                               =>    $this->planned_work_address_id,
            'real_work_address_id'                                  =>    $this->real_work_address_id,
            'timestamped_start_work_time'                           =>    $this->timestamped_start_work_time,
            'timestamped_end_work_time'                             =>    $this->timestamped_end_work_time,
            'note'                                                  =>    $this->note,

            'planned_go_out_time'                                   =>    $this->planned_go_out_time,
            'real_go_out_time'                                      =>    $this->real_go_out_time,
            'planned_early_arrive_start'                            =>    $this->planned_early_arrive_start,
            'planned_early_arrive_end'                              =>    $this->planned_early_arrive_end,
            'planned_late_time'                                     =>    $this->planned_late_time,

            'planned_break_time'                                    =>    $this->getOriginal('planned_break_time'),
            'real_break_time'                                       =>    $this->getOriginal('real_break_time'),
            'planned_night_break_time'                              =>    $this->getOriginal('planned_night_break_time'),
            'real_night_break_time'                                 =>    $this->getOriginal('real_night_break_time'),
            'planned_early_leave_time'                              =>    $this->planned_early_leave_time,
            'planned_overtime_start'                                =>    $this->planned_overtime_start,
            'planned_overtime_end'                                  =>    $this->planned_overtime_end,
            'last_modify_person_name'                               =>    $this->last_modify_person_name,

            'basic_salary'                                          =>    $this->basic_salary,
            'night_salary'                                          =>    $this->night_salary,
            'overtime_salary'                                       =>    $this->overtime_salary,
            'deduction_salary'                                      =>    $this->deduction_salary,
            'night_deduction_salary'                                =>    $this->night_deduction_salary,
            'monthly_traffic_expense'                               =>    $this->monthly_traffic_expense,
            'daily_traffic_expense'                                 =>    $this->daily_traffic_expense,

            'date_upper_limit'                                      =>    $this->date_upper_limit,
            'day_of_the_upper_limit'                               =>    $this->day_of_the_upper_limit,

            'dislay_planned_go_out_time'                            =>    $this->displayThePlannedGoOutTimeOrNot(),
            'temporary'                                             =>    $this->temporary,
            'not_include_break_time_when_display_planned_time'      =>    $this->not_include_break_time_when_display_planned_time,
            'work_time'                                             =>    $this->work_time,
        ];

        // Get the setting about how to display some attributes(timestamped_start/end_work and real_go_out_time) in the front end
        $red_number_setting = $this->getRedNumberSetting();
        $red_background_setting = $this->getRedBackgroundSetting();

        return array_merge($necessary_data, $red_number_setting, $red_background_setting);;
    }


    /**
     * Determine if these attributes should be displayed in red number or not:
     *      - timestamped_start_work_time
     *      - timestamped_end_work_time
     *      - real_go_out_time
     *
     * @return array
     */
    public function getRedNumberSetting()
    {
        $setting = [];

        // Update 2019-01-11, update this conditions.
        if ($this->timestamped_start_work_time !== null && $this->planned_working_hour == null) {
            $setting['start_time_red_number'] = true;

        // Update 2019-02-05: in a kekkin case with a planned_working_hour, if there is dakoku, it should be red
        } else if ($this->isPlannedWorkStatus(WorkStatus::KEKKIN) && $this->timestamped_start_work_time !== null) {
            $setting['start_time_red_number'] = true;

        } else if ($this->timestamped_start_work_time !== null && $this->planned_start_work_time !== null) {
            $carbon_planned_start_work_time = $this->getCarbonInstance($this->planned_start_work_time);
            $carbon_timestamped_start_work_time = $this->getCarbonInstance($this->timestamped_start_work_time);

            $diff = $carbon_planned_start_work_time->diffInMinutes($carbon_timestamped_start_work_time);
            $setting['start_time_red_number'] = ($diff >= $this->affectedByWorkLocation()->currentSetting()->start_time_diff_limit) ? true : false;

        } else if ($this->isOnlyWorkingHourAndBreakTimeMode() == true && $this->timestamped_start_work_time !== null && $this->estimatedLatestStartWorkTime() != null) {
            $carbon_estimated_latest_start_work_time = $this->estimatedLatestStartWorkTime();
            $carbon_timestamped_start_work_time = $this->getCarbonInstance($this->timestamped_start_work_time);

            $diff = $carbon_estimated_latest_start_work_time->diffInMinutes($carbon_timestamped_start_work_time);
            $setting['start_time_red_number'] = ($diff >= $this->affectedByWorkLocation()->currentSetting()->start_time_diff_limit) ? true : false;
        }

        // Update 2019-01-11, update this conditions.
        if ($this->timestamped_end_work_time !== null && $this->planned_working_hour == null) {
            $setting['end_time_red_number'] = true;

        // Update 2019-02-05: in a kekkin case with a planned_working_hour, if there is dakoku, it should be red
        } else if ($this->isPlannedWorkStatus(WorkStatus::KEKKIN) && $this->timestamped_end_work_time !== null) {
            $setting['end_time_red_number'] = true;

        } else if ($this->timestamped_end_work_time !== null && $this->planned_end_work_time !== null) {
            $carbon_planned_end_work_time = $this->getCarbonInstance($this->planned_end_work_time);
            $carbon_timestamped_end_work_time = $this->getCarbonInstance($this->timestamped_end_work_time);

            $diff = $carbon_planned_end_work_time->diffInMinutes($carbon_timestamped_end_work_time);
            $setting['end_time_red_number'] = ($diff >= $this->affectedByWorkLocation()->currentSetting()->end_time_diff_limit) ? true : false;

        } else if ($this->isOnlyWorkingHourAndBreakTimeMode() == true && $this->timestamped_end_work_time !== null && $this->estimatedEndWorkTime() != null) {
            $carbon_estimated_end_work_time = $this->estimatedEndWorkTime();
            $carbon_timestamped_end_work_time = $this->getCarbonInstance($this->timestamped_end_work_time);

            $diff = $carbon_estimated_end_work_time->diffInMinutes($carbon_timestamped_end_work_time);
            $setting['end_time_red_number'] = ($diff >= $this->affectedByWorkLocation()->currentSetting()->end_time_diff_limit) ? true : false;
        }

        if ($this->real_go_out_time !== null && $this->affectedByWorkLocation()->currentSetting()->go_out_button_usage === Setting::USE_AS_BREAK_TIME_BUTTON) {
            // Update 2019-01-11, update this conditions.
            if ($this->planned_working_hour == null) {
                $setting['go_out_red_number'] = true;

            // Update 2019-02-05: in a kekkin case with a planned_working_hour, if there is dakoku, it should be red
            } else if ($this->isPlannedWorkStatus(WorkStatus::KEKKIN)) {
                $setting['go_out_red_number'] = true;

            } else if ($this->real_break_time !== null) {
                $setting['go_out_red_number'] = ($this->real_go_out_time > $this->real_break_time) ? true : false;

            } else if($this->planned_break_time !== null) {
                $setting['go_out_red_number'] = ($this->real_go_out_time > $this->planned_break_time) ? true : false;
            }
        }

        return $setting;
    }


    /**
     * Determine if these attributes should be displayed in red background or not:
     *      - timestamped_start_work_time
     *      - timestamped_end_work_time
     *      - real_go_out_time
     *
     * @return array
     */
    public function getRedBackgroundSetting()
    {
        $setting = [];
        $checklist_errors = $this->checklistItems;
        $start_work_error = $checklist_errors->first(function($error) {
            return $error->error_type === ChecklistItem::START_WORK_ERROR;
        });
        $end_work_error = $checklist_errors->first(function($error) {
            return $error->error_type === ChecklistItem::END_WORK_ERROR || $error->error_type === ChecklistItem::FORGOT_END_WORK_ERROR;
        });
        $go_out_error = $checklist_errors->first(function($error) {
            return $error->error_type === ChecklistItem::GO_OUT_RETURN_ERROR || $error->error_type === ChecklistItem::FORGOT_RETURN_ERROR;
        });

        $setting['start_time_red_background'] = isset($start_work_error) ? true : false;
        $setting['end_time_red_background'] = isset($end_work_error) ? true : false;
        $setting['go_out_red_background'] = isset($go_out_error) ? true : false;

        return $setting;
    }

    /**
     * Determine if the planned_go_out_time attribute should be displayed or not
     *
     * @return boolean
     */
    public function displayThePlannedGoOutTimeOrNot()
    {
        return $this->affectedByWorkLocation()->currentSetting()->go_out_button_usage === Setting::USE_GO_OUT_BUTTON;
    }


    /**
     * Determine if this instance's case is 'take a whole day off' or not.
     *
     * @return boolean
     */
    public function takeAWholeDayOff()
    {
        $carbon_schedule_working_hour = $this->getCarbonInstance($this->schedule_working_hour);
        if ($carbon_schedule_working_hour !== null) {
            $working_hour_in_minutes = $this->convertToMinutes($carbon_schedule_working_hour);

            if ($this->isRestStatusUnitDayOrHour('day') || ($this->totalPaidRestTimeInMinutes() !== 0 && $this->totalPaidRestTimeInMinutes() === $working_hour_in_minutes)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Check if the rest_status of this instance is one of the given
     *
     * @param array         the whole arguments of this function        types of the rest_statues you want to check
     * @return boolean
     */
    public function isPlannedRestStatus()
    {
        // The arguments of this function
        $statuses = func_get_args();

        if (count($statuses) > 0) {
            return in_array($this->planned_rest_status_id, $statuses);
        }

        return null;
    }

    /**
     * Check if the work_status of this instance is one of the given
     *
     * @param array         the whole arguments of this function         types of the work_statues you want to check
     * @return boolean
     */
    public function isPlannedWorkStatus()
    {
        // The arguments of this function
        $statuses = func_get_args();

        if (count($statuses) > 0) {
            return in_array($this->planned_work_status_id, $statuses);
        }

        return null;
    }


    /**
     * Calculate the latest moment for this working information instance
     *
     * @return string       $moment in format 'Y-m-d H:i:s'
     */
    public function latestMomentOfThisWorkingInformation()
    {
        $the_latest_date = null;

        if ($this->planned_overtime_end !== null) {
            return $this->getCarbonInstance($this->planned_overtime_end)->format('Y-m-d') . ' 23:59:59';

        } else if ($this->planned_end_work_time !== null) {
            return $this->getCarbonInstance($this->planned_end_work_time)->format('Y-m-d') . ' 23:59:59';

        } else if ($this->schedule_end_work_time !== null){
            return $this->getCarbonInstance($this->schedule_end_work_time)->format('Y-m-d') . ' 23:59:59';

        } else {
            return $this->getCarbonInstance($this->date_upper_limit)->addDay()->format('Y-m-d') . ' 23:59:59';
        }
    }

    /**
     * Evaluate if this working information instance is a non-work one (the latest moment has been passed and still does not have any timestamp).
     * Relate to HAVE_SCHEDULE_BUT_OFFLINE ChecklistItem error.
     *
     * @return boolean
     */
    public function isThisANonWorkWorkingInformation()
    {
        $latest_moment = $this->getCarbonInstance($this->latestMomentOfThisWorkingInformation());
        $right_now = Carbon::now();

        return $latest_moment->lt($right_now) &&
                ($this->timestamped_start_work_time === null) &&
                ($this->timestamped_end_work_time === null) &&
                ($this->takeAWholeDayOff() != true) &&
                ($this->planned_work_status_id !== WorkStatus::KEKKIN) &&
                ($this->planned_work_status_id !== WorkStatus::FURIKYUU) &&
                ($this->planned_working_hour !== null);
    }

    ///// Summary data - These functions below are for summary data /////

    public function summaryIsAPlannedWorkDay()
    {
        return (($this->schedule_working_hour !== null) || ($this->planned_working_hour !== null)) &&
                ($this->planned_work_status_id !== WorkStatus::KEKKIN) &&
                !$this->isRestStatusUnitDayOrHour('day');
    }

    public function summaryIsARealWorkDay()
    {
        return $this->real_working_hour !== null;
    }

    public function summaryPlannedWorkingHour()
    {
        return ($this->planned_working_hour !== null) ? $this->convertToMinutes($this->getCarbonInstance($this->planned_working_hour)) : 0;
    }

    public function summaryRealWorkingHour()
    {
        return ($this->real_working_hour !== null) ? $this->convertToMinutes($this->getCarbonInstance($this->real_working_hour)) : 0;
    }

    public function summaryScheduleWorkingHour()
    {
        return ($this->schedule_working_hour !== null) ? $this->convertToMinutes($this->getCarbonInstance($this->schedule_working_hour)) : 0;
    }

    public function summaryPlannedWorkSpan()
    {
        return ($this->planned_work_span !== null) ? $this->convertToMinutes($this->getCarbonInstance($this->planned_work_span)) : 0;
    }

    public function summaryRealWorkSpan()
    {
        return ($this->real_work_span !== null) ? $this->convertToMinutes($this->getCarbonInstance($this->real_work_span)) : 0;
    }

    // NOTICE: these paid and unpaid rest time are for the '有給' and '無給' in the summarize part of the employee_working_month page.
    // They are NOT the same with '年次有給取得' in the summarize part which relates to the big '有給休暇' management feature.
    public function summaryPlannedPaidRestTime()
    {
        return $this->paidRestTimeWhichDoesNotCountTowardPaidHoliday(false, true);
    }

    public function summaryRealPaidRestTime()
    {
        return $this->paidRestTimeWhichDoesNotCountTowardPaidHoliday(true, true);
    }

    public function summaryPlannedUnpaidRestTime()
    {
        return $this->paidRestTimeWhichDoesNotCountTowardPaidHoliday(false, false);
    }

    public function summaryRealUnpaidRestTime()
    {
        return $this->paidRestTimeWhichDoesNotCountTowardPaidHoliday(true, false);
    }
    ////

    public function summaryPlannedTotalLateAndEarlyLeave()
    {
        $total = 0;

        $total += ($this->planned_late_time !== null) ? $this->planned_late_time : 0;
        $total += ($this->planned_early_leave_time !== null) ? $this->planned_early_leave_time : 0;

        return $total;
    }

    public function summaryRealTotalLateAndEarlyLeave()
    {
        $total = 0;

        $total += ($this->real_late_time !== null) ? $this->real_late_time : 0;
        $total += ($this->real_early_leave_time !== null) ? $this->real_early_leave_time : 0;

        return $total;
    }

    public function summaryPlannedNonWorkTime()
    {
        return $this->calculateNonWorkTime(false);
    }

    public function summaryRealNonWorkTime()
    {
        return $this->calculateNonWorkTime(true);
    }

    // This so called 'OvertimeWorkTime' is '時間外' in the summarize part of the employee working month page.
    // It includes the early_arrive and the overtime after work
    public function summaryPlannedOvertimeWorkTime()
    {
        return $this->totalEarlyArriveTimeInMinutes() + $this->totalOvertimeInMinutes() + $this->getPlannedOvertimeDueToReduceBreakTime();
    }

    public function summaryRealOvertimeWorkTime()
    {
        return $this->totalEarlyArriveTimeInMinutes('real') + $this->totalOvertimeInMinutes('real') + $this->getRealOvertimeDueToReduceBreakTime();
    }
    ////

    public function summaryPlannedNightWorkTime()
    {
        return $this->calculateNightWorkTime(false);
    }

    public function summaryRealNightWorkTime()
    {
        return $this->calculateNightWorkTime(true);
    }

    // NOTICE: These paid rest time/day are the '年次有給取得' in the summarize part and, as you already know, relate to the big '有給休暇' management feature.
    public function summaryPlannedTakenPaidRestDays()
    {
        return $this->paidRestTimeOrDayWhichCountTowardPaidHoliday(false, true);
    }

    public function summaryRealTakenPaidRestDays()
    {
        return $this->paidRestTimeOrDayWhichCountTowardPaidHoliday(true, true);
    }

    public function summaryPlannedTakenPaidRestTime()
    {
        return $this->paidRestTimeOrDayWhichCountTowardPaidHoliday(false, false);
    }

    public function summaryRealTakenPaidRestTime()
    {
        return $this->paidRestTimeOrDayWhichCountTowardPaidHoliday(true, false);
    }
    ////

    /**
     * Conclude this EmployeeWorkingInformation:
     *     - Export the current data to ConcludedEmployeeWorkingInformation.
     *     - Save the schedule information and salaries information to database.
     *
     * @return void
     */
    public function concludeAndExportData()
    {
        $concluded_employee_working_info = new ConcludedEmployeeWorkingInformation([
            'work_status_id'                            => $this->planned_work_status_id,
            'rest_status_id'                            => $this->planned_rest_status_id,
            'planned_work_location_id'                  => $this->planned_work_location_id,
            'real_work_location_id'                     => $this->real_work_location_id,
            'planned_work_address_id'                   => $this->planned_work_address_id,
            'real_work_address_id'                      => $this->real_work_address_id,
            'planned_start_work_time'                   => $this->planned_start_work_time,
            'timestamped_start_work_time'               => $this->timestamped_start_work_time,
            'real_start_work_time'                      => $this->real_start_work_time,
            'planned_end_work_time'                     => $this->planned_end_work_time,
            'timestamped_end_work_time'                 => $this->timestamped_end_work_time,
            'real_end_work_time'                        => $this->real_end_work_time,
            'planned_break_time'                        => $this->planned_break_time,
            'real_break_time'                           => $this->real_break_time,
            'planned_night_break_time'                  => $this->planned_night_break_time,
            'real_night_break_time'                     => $this->real_night_break_time,
            'planned_go_out_time'                       => $this->planned_go_out_time,
            'real_go_out_time'                          => $this->real_go_out_time,
            'planned_total_late_and_leave_early'        => $this->summaryPlannedTotalLateAndEarlyLeave(),
            'real_total_late_and_leave_early'           => $this->summaryRealTotalLateAndEarlyLeave(),
            'planned_total_early_arrive_and_overtime'   => $this->summaryPlannedOvertimeWorkTime(),
            'real_total_early_arrive_and_overtime'      => $this->summaryRealOvertimeWorkTime(),
            'planned_work_span'                         => $this->summaryPlannedWorkSpan(),
            'real_work_span'                            => $this->summaryRealWorkSpan(),
            'planned_working_hour'                      => $this->summaryPlannedWorkingHour(),
            'real_working_hour'                         => $this->summaryRealWorkingHour(),
            'note'                                      => $this->note,
        ]);

        $concluded_employee_working_info->employeeWorkingInformation()->associate($this);
        $concluded_employee_working_info->employeeWorkingDay()->associate($this->employeeWorkingDay);

        $concluded_employee_working_info->save();

        // When an EmployeeWorkingInformation is concluded, its schedule and salaries information will also be saved to database, so that these information
        // won't change the next time someone change the PlannedSchedule.
        $this->saveScheduleAndSalariesInformation();
    }


    ////////////// End of summary data related functions ////////////////

    /**
     * Use this function to temporarily turn off the checklist evaluation and calculating in this model's event.
     *
     * @return void
     */
    public function turnOffChecklistEvaluationInEvents()
    {
        $this->temporary_turn_off_checklist_errors_evaluation_in_events = true;
    }

    /**
     * Use this function to turn the checklist evaluation and calculating back on in this model's event.
     *
     * @return void
     */
    public function turnOnChecklistEvaluationInEvents()
    {
        $this->temporary_turn_off_checklist_errors_evaluation_in_events = false;
    }

    /**
     * Return boolean determine: to evaluate checklist errors in event or not
     *
     * @return boolean
     */
    public function evaluateChecklistErrorsOrNot()
    {
        return !$this->temporary_turn_off_checklist_errors_evaluation_in_events;
    }

    /**
     * Have snapshot that request to change timestamped_start_work_time or not
     *
     * @return boolean
     */
    public function haveConsideringTimestampedStartWorkTime()
    {
        $color_statuses = $this->colorStatuses;

        if ($color_statuses->isNotEmpty()) {
            $colors_of_timestamped_start_work_time = $color_statuses->filter(function($color) {
                return $color->field_name == 'timestamped_start_work_time';
            });

            if ($colors_of_timestamped_start_work_time->isNotEmpty()) {
                $colors_of_timestamped_start_work_time->sortBy('created_at');
                // Only the most recent color matters
                $activating_color = $colors_of_timestamped_start_work_time->last();

                return $activating_color->field_css_class == ColorStatus::CONSIDERING_COLOR;
            }
        }
        return false;
    }

    /**
     * Have snapshot that request to change timestamped_end_work_time or not
     *
     * @return boolean
     */
    public function haveConsideringTimestampedEndWorkTime()
    {
        $color_statuses = $this->colorStatuses;

        if ($color_statuses->isNotEmpty()) {
            $colors_of_timestamped_end_work_time = $color_statuses->filter(function($color) {
                return $color->field_name == 'timestamped_end_work_time';
            });

            if ($colors_of_timestamped_end_work_time->isNotEmpty()) {
                $colors_of_timestamped_end_work_time->sortBy('created_at');
                // Only the most recent color matters
                $activating_color = $colors_of_timestamped_end_work_time->last();

                return $activating_color->field_css_class == ColorStatus::CONSIDERING_COLOR;
            }
        }
        return false;
    }


    ///////////////////////////// End Utility ///////////////////////////

    //////////////////////////////////// Non-public Utility Functions /////////////////////////////////////////

    /**
     * Save the schedule information and salaries information to database, and turn the manually_modified flag ON. So that next time someone change PlannedSchedule,
     * these attributes won't be changed.
     *
     * @return void
     */
    protected function saveScheduleAndSalariesInformation()
    {
        $this->schedule_start_work_time = $this->schedule_start_work_time !== null ? $this->schedule_start_work_time : config('caeru.empty_date');
        $this->schedule_end_work_time = $this->schedule_end_work_time !== null ? $this->schedule_end_work_time : config('caeru.empty_date');
        $this->schedule_break_time = $this->schedule_break_time !== null ? $this->schedule_break_time : config('caeru.empty');
        $this->schedule_night_break_time = $this->schedule_night_break_time !== null ? $this->schedule_night_break_time : config('caeru.empty');
        $this->schedule_working_hour = $this->schedule_working_hour !== null ? $this->schedule_working_hour : config('caeru.empty_time');
        $this->planned_work_location_id = $this->planned_work_location_id;
        $this->planned_work_address_id = $this->planned_work_address_id;

        $this->basic_salary = $this->basic_salary !== null ? $this->basic_salary : config('caeru.empty');
        $this->night_salary = $this->night_salary !== null ? $this->night_salary : config('caeru.empty');
        $this->overtime_salary = $this->overtime_salary !== null ? $this->overtime_salary : config('caeru.empty');
        $this->deduction_salary = $this->deduction_salary !== null ? $this->deduction_salary : config('caeru.empty');
        $this->night_deduction_salary = $this->night_deduction_salary !== null ? $this->night_deduction_salary : config('caeru.empty');
        $this->monthly_traffic_expense = $this->monthly_traffic_expense !== null ? $this->monthly_traffic_expense : config('caeru.empty');
        $this->daily_traffic_expense = $this->daily_traffic_expense !== null ? $this->daily_traffic_expense : config('caeru.empty');

        $this->manually_modified = true;
        $this->temporary = false;
        $this->save();
    }

    /**
     * Calculate the night work time of this instance. Have to take into account the paid rest time.
     *
     * @param boolean       $real  if this is set to true, it will calculate base on the real_start_work_time and real_end_work_time. Else, it will base on the planned_start_work_time and planned_end_work_time
     * @return float
     */
    protected function calculateNightWorkTime($real = true)
    {
        if (!$this->isPlannedWorkStatus(WorkStatus::KEKKIN)) {
            $only_working_hour_mode = $this->isOnlyWorkingHourAndBreakTimeMode();

            $current_start_work_time = $only_working_hour_mode === true ? $this->real_start_work_time : ($real === true ? $this->real_work_span_start : $this->planned_work_span_start);
            $current_end_work_time = $only_working_hour_mode === true ? $this->real_end_work_time : ($real === true ? $this->real_work_span_end : $this->planned_work_span_end);

            $night_work_time = 0;

            if ($current_start_work_time !== null && $current_end_work_time !== null) {
                $carbon_start_work_time = $this->getCarbonInstance($current_start_work_time);
                $carbon_end_work_time = $this->getCarbonInstance($current_end_work_time);

                $night_work_time = $this->getTheNightTimeInThisPeriod($carbon_start_work_time, $carbon_end_work_time);

                if ($this->paid_rest_time_start !== null && $this->paid_rest_time_end !== null) {
                    $carbon_paid_rest_start = $this->getCarbonInstance($this->paid_rest_time_start);
                    $carbon_paid_rest_end = $this->getCarbonInstance($this->paid_rest_time_end);

                    if ($carbon_start_work_time->lte($carbon_paid_rest_start) && $carbon_paid_rest_end->lte($carbon_end_work_time)) {
                        $night_time_in_paid_rest_time = $this->getTheNightTimeInThisPeriod($carbon_paid_rest_start, $carbon_paid_rest_end);
                        $night_work_time -= $night_time_in_paid_rest_time;
                    }
                }
            }

            $prefix = $real ? 'real_' : 'planned_';

            if ($this->{$prefix . 'early_arrive_start'} !== null && $this->{$prefix . 'early_arrive_end'} !== null) {
                $carbon_early_arrive_start = $this->getCarbonInstance($this->{$prefix . 'early_arrive_start'});
                $carbon_early_arrive_end = $this->getCarbonInstance($this->{$prefix . 'early_arrive_end'});

                $night_time_in_early_arrive_time = $this->getTheNightTimeInThisPeriod($carbon_early_arrive_start, $carbon_early_arrive_end);
                $night_work_time += $night_time_in_early_arrive_time;
            }

            if ($only_working_hour_mode == false && $this->{$prefix . 'overtime_start'} !== null && $this->{$prefix . 'overtime_end'} !== null) {
                $carbon_overtime_start = $this->getCarbonInstance($this->{$prefix . 'overtime_start'});
                $carbon_overtime_end = $this->getCarbonInstance($this->{$prefix . 'overtime_end'});

                $night_time_in_overtime = $this->getTheNightTimeInThisPeriod($carbon_overtime_start, $carbon_overtime_end);
                $night_work_time += $night_time_in_overtime;
            }

            $night_rest_time = $this->{$prefix . 'night_break_time'} !== null ? $this->{$prefix . 'night_break_time'} : 0;

            $night_work_time -= $night_rest_time;
            return $night_work_time < 0 ? 0 : $night_work_time;
        } else {
            return 0;
        }
    }

    /**
     * Calculate the non-work time of this instance.
     * - If today is KEKKIN then non-work time will be planned_work_span
     * - Take into account the real_late_time and the real_early_leave_time
     * - Take into account the current usage of the go out button in the setting of the real_work_location
     *
     * @param boolean   $real   if this parameter is set to true, it will calculate base on the real_work_status_id, otherwise it will base on the planned_work_status_id
     * @return int
     */
    protected function calculateNonWorkTime($real = true)
    {
        $current_work_status_id = ($real === true) ? $this->real_work_status_id : $this->planned_work_status_id;

        if ($current_work_status_id === WorkStatus::KEKKIN) {
            return ($this->schedule_working_hour !== null && $this->timestamped_start_work_time == null && $this->timestamped_end_work_time == null) ? $this->convertToMinutes($this->getCarbonInstance($this->schedule_working_hour)) : 0;
        } else {
            $non_work_time = 0;

            $non_work_time += ($real === true) ? $this->summaryRealTotalLateAndEarlyLeave() : $this->summaryPlannedTotalLateAndEarlyLeave();

            // if ($this->real_work_location_id && $this->affectedByRealWorkLocation()->currentSetting()->go_out_button_usage === Setting::USE_GO_OUT_BUTTON) {
            //     $non_work_time = ($real === true) ? $non_work_time + $this->real_go_out_time : $non_work_time + $this->planned_go_out_time;
            // } elseif ($this->real_work_location_id && $this->affectedByRealWorkLocation()->currentSetting()->go_out_button_usage === Setting::USE_AS_BREAK_TIME_BUTTON) {
            if ($this->planned_work_status_id !== WorkStatus::HOUDE && $this->planned_work_status_id !== WorkStatus::KYUUDE && $this->planned_work_status_id !== WorkStatus::ZANGYOU) {
                if ($real === true) {
                    $non_work_time += ($this->real_break_time > $this->schedule_break_time) ? ($this->real_break_time - $this->schedule_break_time) : 0;
                } else {
                    $non_work_time += ($this->planned_break_time > $this->schedule_break_time) ? ($this->planned_break_time - $this->schedule_break_time) : 0;
                }
            }
            // }

            return $non_work_time;
        }
    }

    /**
     * Calculate the night time between a given pair of time.
     * The carbon_end must be bigger than the carbon_start
     *
     * @param \Carbon\Carbon    $carbon_start
     * @param \Carbon\Carbon    $carbon_end
     * @return integer
     */
    protected function getTheNightTimeInThisPeriod($carbon_start, $carbon_end)
    {
        // Today night time will be from: 22:00:00
        $today_night_time_start = $carbon_start->copy();
        $today_night_time_start->hour(22)->minute(0)->second(0);

        // to: 05:00:00 of the tomorrow
        $today_night_time_end = $today_night_time_start->copy()->addDay();
        $today_night_time_end->hour(5)->minute(0)->second(0);

        // Yesterday night time will be from yesterday's 22:00:00
        $yesterday_night_time_start = $today_night_time_start->copy()->subDay();
        $yesterday_night_time_start->hour(22)->minute(0)->second(0);

        // to today's 05:00:00
        $yesterday_night_time_end = $today_night_time_start->copy();
        $yesterday_night_time_end->hour(5)->minute(0)->second(0);

        $total_night_time = 0;

        // Calculate the today's night work time
        $today_count_period_start = $today_night_time_start->max($carbon_start);
        $today_count_period_end = $today_night_time_end->min($carbon_end);

        $today_night_time = $today_count_period_start->diffInMinutes($today_count_period_end, false);
        $today_night_time = ($today_night_time < 0) ? 0 : $today_night_time;

        // And calculate yesterday's night work time
        $yesterday_count_period_start = $yesterday_night_time_start->max($carbon_start);
        $yesterday_count_period_end = $yesterday_night_time_end->min($carbon_end);

        $yesterday_night_time = $yesterday_count_period_start->diffInMinutes($yesterday_count_period_end, false);
        $yesterday_night_time = ($yesterday_night_time < 0) ? 0 : $yesterday_night_time;

        // Then plus them up
        return $today_night_time + $yesterday_night_time;
    }

    /**
     * An utility function to calculate the paid_rest_time of this working information instance. NOTICE: this paid_rest_time is in the summary box of personal working information page.
     * It does not relate to the paid rest holiday information of an employee.
     *
     * @param boolean       $real   if set to true, it will calculate base on the real_rest_status, and will base on the planned_rest_status otherwise.
     * @param boolean       $paid   if set to true, it will calculate the paid_rest_time, and will calculate unpaid_rest_time otherwise.
     * @return string
     */
    protected function paidRestTimeWhichDoesNotCountTowardPaidHoliday($real = true, $paid = true)
    {
        $current_rest_status_id = ($real === true) ? $this->real_rest_status_id : $this->planned_rest_status_id;
        if ($current_rest_status_id) {

            switch ($current_rest_status_id) {
                case RestStatus::YUUKYU_1:
                case RestStatus::YUUKYU_2:
                    if (($this->timestamped_start_work_time !== null) || ($this->timestamped_end_work_time !== null)) {
                        return 0;
                    }
                    return ($this->schedule_working_hour !== null && $paid == true) ? $this->convertToMinutes($this->getCarbonInstance($this->schedule_working_hour)) : 0;
                case RestStatus::GOKYUU_1:
                case RestStatus::GOKYUU_2:
                case RestStatus::ZENKYUU_1:
                case RestStatus::ZENKYUU_2:
                case RestStatus::HANKYUU_1:
                case RestStatus::HANKYUU_2:
                case RestStatus::JIYUU:
                    return ($paid == true) ? $this->totalPaidRestTimeInMinutes() : 0;
                default:
                    $rest_status = $this->plannedRestStatus;

                    if ($rest_status && $rest_status->paid_type == $paid) {
                        if ($rest_status->unit_type == true) {
                            if ($real === true && ($this->timestamped_start_work_time !== null) || ($this->timestamped_end_work_time !== null)) {
                                return 0;
                            }
                            return ($this->schedule_working_hour !== null) ? $this->convertToMinutes($this->getCarbonInstance($this->schedule_working_hour)) : 0;
                        } else {
                            return $this->totalPaidRestTimeInMinutes();
                        }
                    }
            }
        }
        return 0;
    }


    /**
     * Calculate the paid_rest_time in day unit or time unit. This paid_rest_day/time is count toward the employee's paid holiday information.
     *
     * @param boolean           $real   if set to true, calculate base on the real_rest_status_id, ortherwise base on the planned_rest_status_id
     * @param boolean           $day    if $day is true, then calculate in day unit, otherwise, calculate in time unit.
     * @return float|int
     */
    protected function paidRestTimeOrDayWhichCountTowardPaidHoliday($real = true, $day = true)
    {
        $current_rest_status_id = ($real === true) ? $this->real_rest_status_id : $this->planned_rest_status_id;

        if ($current_rest_status_id) {

            switch ($current_rest_status_id) {
                case RestStatus::YUUKYU_1:
                case RestStatus::YUUKYU_2:
                    if ($this->timestamped_start_work_time !== null || $this->timestamped_end_work_time !== null) {
                        return 0;
                    }
                    return ($day == true) ? 1 : 0;
                case RestStatus::GOKYUU_1:
                case RestStatus::GOKYUU_2:
                case RestStatus::ZENKYUU_1:
                case RestStatus::ZENKYUU_2:
                case RestStatus::HANKYUU_1:
                case RestStatus::HANKYUU_2:
                    return ($day == true) ? 0.5 : 0;
                case RestStatus::JIYUU:
                    return ($day == false) ? $this->totalPaidRestTimeInMinutes() : 0;
                default:
                    return 0;
            }
        }
        return 0;
    }


    /**
     * Calculate the sum of schedule working hour and schedule break time of this working information
     *
     * @return int
     */
    protected function totalAtWorkTimeInMinutes()
    {
        if ($this->schedule_working_hour !== null) {
            $break_time = $this->schedule_break_time !== null ? $this->schedule_break_time : 0;
            $total_time = $this->convertToMinutes($this->getCarbonInstance($this->schedule_working_hour)) + $break_time;

            return $total_time;
        } else {
            return 0;
        }
    }


    /**
     * Calculate the remaining time of planned_work_span in minutes
     *
     * @return int
     */
    protected function remainingPlannedWorkSpanTimeInMinutes()
    {
        $carbon_working_hour = $this->getCarbonInstance($this->schedule_working_hour);

        if ($carbon_working_hour !== null) {
            $total_working_hour = $this->convertToMinutes($carbon_working_hour);
            $late_time = ($this->planned_late_time !== null) ? $this->planned_late_time : 0;
            $early_leave = ($this->planned_early_leave_time !== null) ? $this->planned_early_leave_time : 0;

            $remaining_time = $total_working_hour - $late_time - $early_leave - $this->totalPaidRestTimeInMinutes();

            return ($remaining_time >= 0) ? $remaining_time : 0;
        } else {
            return 0;
        }
    }


    /**
     * Calculate the offset time that need to be add to work_span_start time or subtract from work_span_end time (or the total work_span) in the case of
     * the Employee take half a day off.
     *
     * The offset time will be half the total work_span of that day + the break time. But if there is only work_span and work_span_start/end is null,
     * then it will be half of that work_span only.
     */
    protected function offsetTimeToAddOrSubWhenTakeHalfDayOff()
    {
        if ($this->schedule_start_work_time && $this->schedule_end_work_time) {

            $carbon_start = $this->getCarbonInstance($this->schedule_start_work_time);

            $carbon_end = $this->getCarbonInstance($this->schedule_end_work_time);

            $total_minutes = $carbon_start->diffInMinutes($carbon_end);

            // ((a + c) + c)/2 = a/2 + c. And [a/2 + c] is what we need.
            return ($total_minutes + $this->schedule_break_time)/2;

        } elseif ($this->schedule_working_hour) {

            $carbon_instance = $this->getCarbonInstance($this->schedule_working_hour);

            $carbon_zero = $this->getCarbonInstance('0:0:0');

            return ($carbon_instance->diffInMinutes($carbon_zero))/2;
        }

    }

    /**
     * Check if this instance's rest status is a day based type or an hour based type.
     *
     * @param string   $type       default is 'day', whatever else will check for hour types
     * @return boolean
     */
    public function isRestStatusUnitDayOrHour($type = 'day')
    {
        if ($this->cachedEmployeeWorkingInformation !== null && !$this->cachedEmployeeWorkingInformation->old) {
            return $type == 'day' ? $this->cachedEmployeeWorkingInformation->rest_status_unit_day == true : $this->cachedEmployeeWorkingInformation->rest_status_unit_day == false;
        } else {
            $this->calculateAndCacheAttributes();
            return $type == 'day' ? $this->cachedEmployeeWorkingInformation->rest_status_unit_day == true : $this->cachedEmployeeWorkingInformation->rest_status_unit_day == false;
        }
    }


    /**
     * Use this, when there is pair of dates and you want to make sure that the end_date is bigger than the start_date.
     * In other words, this function is to correct the case start_date is late hour today and end_date is early hour tomorrow. (ex: 20:00 ~ 02:00)
     * Note: the order is very important in this function.
     *
     * @param Carbon    $start
     * @param Carbon    $end
     * @return Carbon   $end    after correct the day of this instance.
     */
    protected function makeTheSecondOneBigger(Carbon $start, Carbon $end)
    {
        return $end->lt($start) ? $end->addDay() : $end;
    }

    /**
     * Convert a Carbon instance to minutes. Only calculate from the number of hours and minutes.
     *
     * @param Carbon    $instance
     * @return int      the total minutes
     */
    public function convertToMinutes($instance)
    {
        return ($instance !== null) ? ($instance->hour * 60 + $instance->minute) : null;
    }

    /**
     * Convert a given number of minutes to a time string. Format: 'hh:mm'
     *
     * @param int       $minutes
     * @return string
     */
    public function minutesToString($minutes)
    {
        if ($minutes <= 0) {
            return '00:00:00';
        } else {
            return str_pad(floor($minutes/60), 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes%60, 2, '0', STR_PAD_LEFT) . ':00';
        }
    }

    /**
     * Generate a Carbon instance from a time string with format 'hh:mm'
     *
     * @param string $time_string
     * @return Carbon
     */
    public function getCarbonInstance($time_string)
    {
        if ($time_string != null) {

            // If the $time_string is a full-fledge time string with date parts
            if (strpos($time_string, ' ') !== false) {
                return new Carbon($time_string);
            }

            $date_upper_limit = $this->date_upper_limit;
            $day_of_date_upper_limit = explode(' ', $date_upper_limit)[0];

            $instance = Carbon::createFromFormat('Y-m-d', $day_of_date_upper_limit);
            $time = explode(':', $time_string);
            $instance->hour = $time[0];
            $instance->minute = $time[1];
            if (isset($time[2])) $instance->second = $time[2];

            $carbon_date_limit = Carbon::createFromFormat('Y-m-d H:i:s', $date_upper_limit);

            if ($instance->lt($carbon_date_limit))
                $instance->addDay();

            return $instance;

        } else {
            return null;
        }

    }

    /**
     * The generic function, used to get the value of planned attributes
     *
     * @param $string   $field_name
     * @param mix       $value
     * @return mix
     */
    protected function genericGetFromPlannedSchedule($field_name, $value, $check_holiday = false)
    {
        $national_holiday_service = resolve(NationalHolidayService::class);
        // In the case the value of this field is null
        if ($value === null && $this->temporary != true) {

            // Then, first, we check if this instance is connected to a PlannedSchedule in a normal way (WorkLocation case)
            if ($this->plannedSchedule) {

                // This check is for the salary fields, because the salaries are calculated base on weather this day is a national holiday or not.
                if ($check_holiday) {

                    if ($national_holiday_service->is($this->employeeWorkingDay->date)) {
                        return $this->plannedSchedule->{'holiday_' . $field_name};
                    } else {
                        return $this->plannedSchedule->{'normal_' . $field_name};
                    }

                }
                return $this->plannedSchedule->$field_name;

            }
            // If not, we check if this instance is connected to a PlannedSchedule through a WorkAddressWorkingEmployee instance (WorkAddress case)
            elseif ($this->workAddressWorkingEmployee && $this->workAddressWorkingEmployee->plannedSchedule) {
                // The attributes that need 'holiday checking' will be fetch from PlannedSchedule (Those are the 'salary' or 'traffic expense' attributes)
                if ($check_holiday) {

                    if ($national_holiday_service->is($this->employeeWorkingDay->date)) {
                        return $this->workAddressWorkingEmployee->plannedSchedule->{'holiday_' . $field_name};
                    } else {
                        return $this->workAddressWorkingEmployee->plannedSchedule->{'normal_' . $field_name};
                    }

                // Whereas the attributes that doesnt need 'holiday checking' will be fetched from WorkAddressWorkingInformation or WorkAddressWorkingEmployee(depend on the name of the attributes).
                // UPDATED: the attributes that doesnt need 'holiday checking' will also be fetched from PlannedSchedule. They just don't need the normal_ or holiday_ prefixes.
                } else {
                    return $this->workAddressWorkingEmployee->plannedSchedule->{$field_name};
                }

            } else {
                return null;
            }

        } else {
            return $value;
        }
    }

    /**
     * Get the WorkLocation instance from the PlannedSchedule.
     * I've just noticed. When the model of the relationship 'currentPlannedWorkLocation' is called. First it will resolve the attribute
     * 'planned_work_location_id', so it's already gone pass the logic relate to PlannedSchedule/WorkAddressWorkingEmployee. So there is no need
     * for us to check that logic here.
     *
     *
     * @return null|WorkLocation
     */
    protected function getInstanceOfPlannedWorkLocation()
    {
        if ($this->currentPlannedWorkLocation) {
            return $this->currentPlannedWorkLocation;

        } else {
            return null;
        }
    }

    /**
     * Get the WorkAddress instance from the PlannedSchedule
     *
     * @return null|WorkAddress
     */
    protected function getInstanceOfWorkAddressFromPlannedSchedule()
    {
        if ($this->workAddressWorkingEmployee) {
            return $this->workAddressWorkingEmployee->plannedSchedule ? $this->workAddressWorkingEmployee->plannedSchedule->workAddress : null;
        }
        return null;
    }

    /**
     * Why do we need this special function ?
     * Because how we retrieve planned start/end work time attributes is a little special.
     * If this instance is linked with an WorkAddressWorkingEmployee (that means the case of WorkAddress) then we have to
     * get the start/end work time from the WorkAddressWorkingInformation instance itself.
     * If this instance is linked with an PlannedSchedule (WorkLocation case), then we just get it from the PlannedSchedule instance.
     *
     * @param $string   $field_name
     * @param mix       $value
     * @return mix
     */
    protected function getTimeDataFromSchedules($field_name, $value)
    {
        // In the case the value of this field is null
        if ($value === null && $this->temporary != true) {

            // Then, first, we check if this instance is connected to a PlannedSchedule in a normal way (WorkLocation case)
            if ($this->plannedSchedule) {

                return $this->plannedSchedule->$field_name;

            // If not, we check if this instance is connected to a PlannedSchedule through a WorkAddressWorkingEmployee instance (WorkAddress case)
            } elseif ($this->workAddressWorkingEmployee) {

                return $this->workAddressWorkingEmployee->workAddressWorkingInformation->{'schedule_' . $field_name};

            } else {
                return null;
            }
        } else {
            return $this->$value;
        }
    }

}

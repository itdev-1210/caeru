<?php

namespace App;

use Carbon\Carbon;
use App\Services\WorkLocationSettingService;

class WorkAddressWorkingInformation extends Model
{

     /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['date_upper_limit', 'day_of_the_upper_limit'];


    /**
     * Get the working day instance of this working information instance
     */
    public function workAddressWorkingDay()
    {
        return $this->belongsTo(WorkAddressWorkingDay::class);
    }

    /**
     * Get all the working employees of this work address working information
     */
    public function workAddressWorkingEmployees()
    {
        return $this->hasMany(WorkAddressWorkingEmployee::class);
    }

    /**
     * Get all the color statuses for this working information instance
     */
    public function colorStatuses()
    {
        return $this->hasMany(ColorStatus::class);
    }

    //// Accessor for the three attributes ////
    ///////////////////////////////////////////
    public function getScheduleStartWorkTimeAttribute($value)
    {
        if ($value === null) {
            $time_part = $this->getTimeDataFromMatchedPlannedSchedules('start_work_time');

            if ($time_part) {
                $planned_start_work_time_carbon = $this->getCarbonInstance($time_part);
                return $planned_start_work_time_carbon->format('Y-m-d H:i:s');
            }
        }

        return $value;
    }

    public function getScheduleEndWorkTimeAttribute($value)
    {
        if ($value === null) {
            $time_part = $this->getTimeDataFromMatchedPlannedSchedules('end_work_time');

            if ($time_part) {
                $planned_end_work_time_carbon = $this->getCarbonInstance($time_part);
                return $planned_end_work_time_carbon->format('Y-m-d H:i:s');
            }
        }

        return $value;
    }

    public function getCandidateNumberAttribute($value)
    {
        return $this->getPlannedCandidateNumber($value);
    }

    // Some Virtual utility attributes
    /**
     * Base on the date_separate setting of the company to determine the uppler limit of date attributes of this working day
     *
     * @return string       a date time string with format 'Y-m-d H:i:s'
     */
    public function getDateUpperLimitAttribute()
    {
        // $company = $this->workAddressWorkingDay->workAddress->workLocation->company;
        $service = resolve(WorkLocationSettingService::class);
        $company = $service->getCompany();

        if ($company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {
            return $this->workAddressWorkingDay->date . ' ' . $company->date_separate_time . ':00';
        } else {
            $day = Carbon::createFromFormat('Y-m-d', $this->workAddressWorkingDay->date);
            $day->subDay();
            return $day->format('Y-m-d') . ' ' . $company->date_separate_time . ':00';
        }
    }

    /**
     * Get the day of the date_upper_limit above
     *
     * @return string    a date string with format 'Y-m-d'
     */
    public function getDayOfTheUpperLimitAttribute()
    {
        $service = resolve(WorkLocationSettingService::class);
        $company = $service->getCompany();
        if ($company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {
            return $this->workAddressWorkingDay->date;
        } else {
            return Carbon::createFromFormat('Y-m-d', $this->workAddressWorkingDay->date)->subDay()->format('Y-m-d');
        }
    }

    /**
     * Get the actual working employees list of this WorkAddressWorkingInformation. Because the list of WorkAddressWorkingEmployee is just a list of working
     * employee candidates. Who is actually working or not will be decided by the manager.
     *
     * @return Collection
     */
    public function currentActualWorkingEmployees()
    {
        return $this->workAddressWorkingEmployees->filter(function($employee) {
            return $employee->working_confirm == true &&
                $employee->employeeWorkingInformation->planned_work_status_id != WorkStatus::KEKKIN &&
                $employee->employeeWorkingInformation->planned_work_status_id != WorkStatus::FURIKYUU &&
                !($employee->employeeWorkingInformation->planned_rest_status_id != null && $employee->employeeWorkingInformation->isRestStatusUnitDayOrHour('day'));
        });
    }

    /**
     * Check if this working info's designated working employees number and the actual working employees number is mismatch or not.
     *
     * @return boolean
     */
    public function actualWorkingEmployeesMismatchOrNot()
    {
        return $this->currentActualWorkingEmployees()->count() != $this->candidate_number;
    }

    /**
     * Get the start_work_time and end_work_time from the PlannedSchedule through the WorkAddressWorkingEmployee instances
     *
     * @param $string   $field_name
     * @param mix       $value
     * @return mix
     */
    protected function getTimeDataFromMatchedPlannedSchedules($field_name, $value = null)
    {
        // In the case the value of this field is null
        if ($value === null) {

            // And in a WorkAddressWorkingInformation instance, all employees have the save start/end work times.
            // So we can get these info from the first, last or any employee of this instance.
            $first_employee = $this->workAddressWorkingEmployees->first(function($employee) {
                return $employee->plannedSchedule != null;
            });

            if ($first_employee) {
                return $first_employee->plannedSchedule->$field_name;
            } else {
                return null;
            }

        } else {
            return $value;
        }
    }

    /**
     * Get the planned candidate number of this WorkAddressWorkingInformation instance
     *
     * @param $string   $field_name
     * @param mix       $value
     * @return mix
     */
    protected function getPlannedCandidateNumber($value)
    {
        // If the instance does not have its own candidate number, choose the biggest from the employees's planned schedules
        if ($value === null) {

            // Filter out all the WorkAddressWorkingEmployee that had been created by hand, we don't use them when calculate candidate_number.
            $working_employee_set_to_calculate_candidate_number = $this->workAddressWorkingEmployees->filter(function($employee) {
                return $employee->plannedSchedule != null;
            });

            // We have to count the employee number in the case of "All of the employee's planned schedules are fixed(固定)".
            // If that's the case, then the candidate number of this work information is fixed and is equal to the employee number.
            $current_employee_number = 0;
            // If there is one 'candidate' schedule (or some), then the candidate_number of this instance is
            // that schedule's candidate number (or the biggest candidate number in those schedules).
            $biggest_candidate_number = 0;
            $no_candidate_schedule = true;

            foreach ($working_employee_set_to_calculate_candidate_number as $employee) {

                $current_employee_number++;

                if ($employee->plannedSchedule && $employee->plannedSchedule->candidating_type == true) {
                    $biggest_candidate_number = $employee->plannedSchedule->candidate_number > $biggest_candidate_number ?
                        $employee->plannedSchedule->candidate_number :
                        $biggest_candidate_number;
                    $no_candidate_schedule = false;
                }
            }

            if ($no_candidate_schedule == true) {
                return $current_employee_number;
            } else {
                return $biggest_candidate_number;
            }

        } else {
            return $value;
        }
    }

    /**
     * Generate a Carbon instance from a time string with format 'hh:mm'
     *
     * @param string $time_string
     * @return Carbon
     */
    protected function getCarbonInstance($time_string)
    {
        if ($time_string != null) {

            // If the $time_string is a full-fledge time string with date parts
            if (strpos($time_string, ' ') !== false) {
                return new Carbon($time_string);
            }

            $date_upper_limit = $this->date_upper_limit;
            $day_of_date_upper_limit = explode(' ', $date_upper_limit)[0];

            $instance = Carbon::createFromFormat('Y-m-d', $day_of_date_upper_limit);
            $time = explode(':', $time_string);
            $instance->hour = $time[0];
            $instance->minute = $time[1];
            if (isset($time[2])) $instance->second = $time[2];

            $carbon_date_limit = Carbon::createFromFormat('Y-m-d H:i:s', $date_upper_limit);

            if ($instance->lt($carbon_date_limit))
                $instance->addDay();

            return $instance;

        } else {
            return null;
        }

    }
}

<?php

namespace App\Reusables;
use App\PaidHolidayInformation; 
Use Carbon\Carbon;
use App\WorkStatus;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Employee;
use App\EmployeeWorkingDay;

trait PaidHolidayInformationTrait
{
    /**
     * Scope a query to get the enable model .
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
       /**
     *
     * Gets available_paid_holidays attribute.
     *
     */
    public function getAvailablePaidHolidayHour()
    {
        return $this->getPaidHolidayHour($this->available_paid_holidays_hour);
    }

    /**
     * Gets consumed_paid_holidays attribute.
     */
    public function getConsumedPaidHolidayHour()
    {
        return $this->getPaidHolidayHour($this->consumed_paid_holidays_hour);
    }

    /**
     *
     * Gets carried_forward_paid_holidays attribute.
     *
     */
    public function getCarriedForwardPaidHolidayHour()
    {
        return $this->getPaidHolidayHour($this->carried_forward_paid_holidays_hour);
    }

    /**
     * Convert time to display.
     *
     * @param float $value
     *
     * @return array.
     */
    public function getPaidHolidayHour($time)
    {
        $hour = floor($time);
        $minute = ($time-$hour) * 60;
        return [
            sprintf("%02d", $hour),
            sprintf("%02d", $minute),
        ];
    }

    /**
     * Covert minutes to day and hour
     *
     * @param integer $time
     * @param integer $work_time_per_day
     *
     * @return array
     */
    private function convertTimeToDay($time, $work_time_per_day)
    {
        if(!$work_time_per_day) {
            throw new \Exception("Error division zero");
        }
        $day =floor($time/($work_time_per_day*60));
        $timeCompute = $time%($work_time_per_day*60);
        $hourCompute = $timeCompute/60;
        return [
            $day,
            $hourCompute,
        ];
    }
    
    /**
     * Revert time to save
     *
     * @param array $time
     *
     * @return float
     * 
     */
    public function revertTime(array $time)
    {
        list($hour, $minute) = $time;
        $hourSeprate = $minute/60;
        $hourComputed = $hour%$this->work_time_per_day + $hourSeprate;
        $dayPlus = floor(($hourSeprate+floor($hour)) / $this->work_time_per_day);
        return [$hourComputed, $dayPlus];
    }

    /**
     * Gets the paid holiday information.
     * 
     * EXample: 
     * 
     * Joined date: 8/6/2016, paid_holiday_update: 27/4, 
     *  => chu ki 1: 8/9/2016-8/9/2016-26/4/2018
     * 
     * Joined date: 1/1/2016, paid_holiday_update: 27/4,
     *  => chu ki 1: 1/4/2016 - 26/4/2017
     *  
     * Joined date: 8/6/2017, paid_holiday_update: 27/4
     *  => chu ki 1: 8/9/2017 - 26/4/2019
     *  => This is Current period => can edit info
     *  
     *  
     *   TODO
     *   
     *   Joined 1/1/2012, paid_holiday_update: 6/6
     *   Chi ki 1: 1/4/2012 - 5/6/2013 (temp period)
     *   If apply after paid_holiday_update (7/6/2012)
     *   =>Chu ki 1: 6/6/2012 - 5/6/2013
     */
    public function getPaidHolidayInformation($employee)
    {
        //first period
        $holidays_update_day_compute = explode("/", $employee->holidays_update_day); 

        $period_start = $employee->computePeriodOriginStart(); 
        $period_end_temp = Carbon::createFromFormat('Y-m-d',($period_start->year + 1) .'-'. $holidays_update_day_compute[0] .'-'. $holidays_update_day_compute[1]); 
        $period_start_temp = $period_start->copy()->addYear(); 
        $period_end = $period_start_temp->gt($period_end_temp)?$period_end_temp->copy()->addYear():$period_end_temp->copy(); 
        $paild_info = PaidHolidayInformation::where('employee_id', $employee->id)->orderBy('id','desc')->first();
     
        $work_time_per_day = $employee->work_time_per_day; 
        $attendance_rate = 0;
        $provided_paid_holidays = $employee->getAmountPaidHoliday()?:0; 
        $carried_forward_paid_holidays = 0;
        $carried_forward_paid_holidays_hour = 0;

        //Total number provided paid holidays
        if(!$employee->hasFirstKoushin() || $employee->isSenior() && !isset($paild_info))
        {   
            if( $employee->isSenior())
            {  
                $period_start_temp = Carbon::createFromFormat('Y-m-d',Carbon::now()->year .'-'. $holidays_update_day_compute[0] .'-'. $holidays_update_day_compute[1]); 

                $period_start = $period_start_temp->gt(Carbon::now())?(Carbon::createFromFormat('Y-m-d',(Carbon::now()->year-1) .'-'. $holidays_update_day_compute[0] .'-'. $holidays_update_day_compute[1])):$period_start_temp;
                $period_end = $period_start->copy()->addYear()->subDay();
            }

            $provided_paid_holiday_array = explode(',', $employee->getAmountPaidHolidayIncrease());
            $countPeriod = $employee->countPeriod() - 1;
                  
            if($countPeriod < count($provided_paid_holiday_array))
            { 
                $provided_paid_holidays = $provided_paid_holiday_array[$countPeriod -1];
            }
            else
            {   
                $provided_paid_holidays = $provided_paid_holiday_array[count($provided_paid_holiday_array)-1];
            }
                  
        } 
        // The case has been cyclical
        elseif (isset($paild_info)) {

            $period_start = Carbon::createFromFormat('Y-m-d',$paild_info->period_end)->addDay();
            $period_end = $period_start->copy()->addYear()->subDay();

            $provided_paid_holiday_array = explode(',', $employee->getAmountPaidHolidayIncrease());
            $countPeriod = $employee->countPeriod() -1;
                 
            if($countPeriod < count($provided_paid_holiday_array))
            { 
                $provided_paid_holidays = $provided_paid_holiday_array[$countPeriod -1];
            }
            else
            {    
                $provided_paid_holidays = $provided_paid_holiday_array[count($provided_paid_holiday_array)-1];
            }

        }
        else {  
            $period_end->subDay();
        }

        //Total number carried forward paid holidays
        $provided_paid_holidays = $provided_paid_holidays?:0;

        if($employee->isSenior() && !isset($paild_info))
        {
            $last_available_paid_holidays = Employee::getDataPaidHolidayInformation()
                ->where('employees.id',$employee->id)->get()->first()
                ->available_paid_holidays;

            $available_paid_holidays_hour = Employee::getDataPaidHolidayInformation()
                ->where('employees.id', $employee->id)->get()->first()
                ->available_paid_holidays_hour;
      
                $carried_forward_paid_holidays = null;
                $carried_forward_paid_holidays_hour = null;
          
        }
        elseif (isset($paild_info)) {
                $carried_forward_paid_holidays = $paild_info->provided_paid_holidays < $paild_info->available_paid_holidays ? $paild_info->provided_paid_holidays : $paild_info->available_paid_holidays;
                $carried_forward_paid_holidays_hour = $paild_info->provided_paid_holidays < $paild_info->available_paid_holidays ? 0 : $paild_info->available_paid_holidays_hour;
         } else{
            $carried_forward_paid_holidays = null;
            $carried_forward_paid_holidays_hour = null;
        }
    
        //--------------------
        $working_days = null;
        $shoutei = 0;
        $kekkin = 0;
        $attendance_rate = null;
        // Calculates the attendance rate period.
         if(isset($paild_info))
        {   
            $working_days = $employee->employeeWorkingDays()->whereBetween('employee_working_days.date', [$period_start, $period_end])->get();
        }
        elseif($attendanceRatePeriodArr = $employee->computeAttendanceRatePeriod())
        {   
            $working_days = $employee->employeeWorkingDays()->whereBetween('employee_working_days.date', $attendanceRatePeriodArr)->get();
        }
       else {
            $working_days = $employee->employeeWorkingDays()->whereBetween('employee_working_days.date', [$period_start, $period_end])->get();
         }
        //-----------------------------------------

        $consumed_paid_holidays = 0;
        $consumed_paid_holidays_hour = 0;
        foreach ($working_days as $working_day) {
            $working_day_information = $working_day->getSummaryInformation();
            $consumed_paid_holidays += $working_day_information['real_taken_paid_rest_days'];
            $consumed_paid_holidays_hour += $working_day_information['real_taken_paid_rest_time'];
            if($working_day_information['have_schedule'] == true){
                $shoutei += 1;
            }
            if($working_day_information['real_is_kekkin'] == true){
                $kekkin += 1;
            }
        }

        $attendance_rate = round($shoutei== 0 ? 0 :(($shoutei-$kekkin)/$shoutei)*100);
        if( !($employee->computeAttendanceRatePeriod()) && !($paild_info)){
            $attendance_rate = null;
        }
        //-------------------------------------------------
        $available_paid_holidays_hour = 0;
        $available_paid_holidays_computed = $provided_paid_holidays + $carried_forward_paid_holidays - $consumed_paid_holidays;
        $available_paid_holidays_hour_computed = $carried_forward_paid_holidays_hour*60 - $consumed_paid_holidays_hour;
        list($available_day, $available_hour) = $this->convertTimeToDay($available_paid_holidays_hour_computed, $work_time_per_day);
      
        $available_paid_holidays = $available_paid_holidays_computed + $available_day;
        if($available_hour < 0){
           $available_paid_holidays_hour = $work_time_per_day + $available_hour;
        }else {
           $available_paid_holidays_hour = $available_hour;
        }
                        
        $paidHolidayInformation = new PaidHolidayInformation([
            'employee_id'=>$employee->id,
            'period_start'=>$period_start,
            'period_end'=>$period_end,
            'work_time_per_day'=>$work_time_per_day,
            'attendance_rate'=>$attendance_rate,
            'provided_paid_holidays'=>$provided_paid_holidays,
            'carried_forward_paid_holidays'=>$carried_forward_paid_holidays,
            'carried_forward_paid_holidays_hour'=>$carried_forward_paid_holidays_hour,
            'consumed_paid_holidays'=>$consumed_paid_holidays,
            'consumed_paid_holidays_hour'=>$consumed_paid_holidays_hour/60,
            'available_paid_holidays'=>$available_paid_holidays,
            'available_paid_holidays_hour'=>$available_paid_holidays_hour,
            'last_modified_manager_id'=>auth()->user()->id,
            'last_modified_date'=>DB::raw('NOW()'),
        ]);
        return $paidHolidayInformation;
    }


    /**
     * Updated a paid holiday information.
     *
     * @param instance $employee The employee
     *
     * @return boolean
     */
    public function updatePaidHolidayInformation($employee) {

        $employee = Employee::find($employee);
        $success = true;
        // Search the whole cycle of employees
        $paid_holiday_infos  = PaidHolidayInformation::where('employee_id', $employee->id)->orderBy('id','asc')->get(); 
        $consumed_paid_holidays = 0;
        $consumed_paid_holidays_hour = 0;
        $paid_holiday_info_temp = null;
        $attendance_rate = null;
        $shoutei = 0;
        $kekkin = 0;
        $shoutei_temp = 0;
        $kekkin_temp = 0;
        $shoutei_rate = 0;
        $kekkin_rate = 0;

        foreach ($paid_holiday_infos as $key => $paid_holiday_info){
        $employee_working_days = EmployeeWorkingDay::where('employee_id',$paid_holiday_info->employee_id)->where('date','>=' ,$paid_holiday_info->period_start)->where('date','<=',$paid_holiday_info->period_end)->get();
        foreach ($employee_working_days as $employee_working_day) {
            $working_day_information = $employee_working_day->getSummaryInformation();
            $consumed_paid_holidays += $working_day_information['real_taken_paid_rest_days'];
            $consumed_paid_holidays_hour += $working_day_information['real_taken_paid_rest_time'];

            if($working_day_information['have_schedule'] == true){
                $shoutei += 1;
            }
            if($working_day_information['real_is_kekkin'] == true){
                $kekkin += 1;
            }
        }
        // recalculate the information of the cycle
        if($key == 0){ 
       
         // Calculates the attendance rate period.
           $attendanceRatePeriodArr = [$employee->joined_date, $employee->computePeriodOriginStart()];
           $working_rate_days = $employee->employeeWorkingDays()->whereBetween('employee_working_days.date', $attendanceRatePeriodArr)->get();
           foreach ($working_rate_days as $working_rate_day) {
               $working_rate_day_information = $working_rate_day->getSummaryInformation();
                if($working_rate_day_information['have_schedule'] == true){
                $shoutei_rate += 1;
                    }
                if($working_rate_day_information['real_is_kekkin'] == true){
                $kekkin_rate += 1;
                }
           }
           $attendance_rate = round($shoutei_rate== 0 ? 0 :(($shoutei_rate-$kekkin_rate)/$shoutei_rate)*100);

           $available_paid_holidays_hour = 0;
           $available_paid_holidays_computed = $paid_holiday_info->provided_paid_holidays + $paid_holiday_info->carried_forward_paid_holidays - $consumed_paid_holidays;
           $available_paid_holidays_hour_computed = $paid_holiday_info->carried_forward_paid_holidays_hour*60 - $consumed_paid_holidays_hour;
           list($available_day, $available_hour) = $this->convertTimeToDay($available_paid_holidays_hour_computed, $paid_holiday_info->work_time_per_day);
      
           $available_paid_holidays = $available_paid_holidays_computed + $available_day;
        if($available_hour < 0){
           $available_paid_holidays_hour = $paid_holiday_info->work_time_per_day + $available_hour;
        }else {
             $available_paid_holidays_hour = $available_hour;
        }
           $paid_holiday_info->attendance_rate= $attendance_rate;
           $paid_holiday_info->consumed_paid_holidays= $consumed_paid_holidays;
           $paid_holiday_info->consumed_paid_holidays_hour= $consumed_paid_holidays_hour/60;
           $paid_holiday_info->available_paid_holidays= $available_paid_holidays;
           $paid_holiday_info->available_paid_holidays_hour= $available_paid_holidays_hour;
           $paid_holiday_info->update();
           $paid_holiday_info_temp = $paid_holiday_info;
           $consumed_paid_holidays = 0;
           $consumed_paid_holidays_hour = 0;
           $shoutei_temp = $shoutei;
           $kekkin_temp = $kekkin;
           $shoutei = 0;
           $kekkin = 0;
       }else{ 
           $carried_forward_paid_holidays = $paid_holiday_info_temp->provided_paid_holidays <= $paid_holiday_info_temp->available_paid_holidays ? $paid_holiday_info_temp->provided_paid_holidays : $paid_holiday_info_temp->available_paid_holidays;
           $carried_forward_paid_holidays_hour = $paid_holiday_info_temp->provided_paid_holidays <= $paid_holiday_info_temp->available_paid_holidays ? 0 : $paid_holiday_info_temp->available_paid_holidays_hour;

            // Calculates the attendance rate period.
           $attendance_rate = round($shoutei_temp == 0 ? 0 : (($shoutei_temp - $kekkin_temp)/$shoutei_temp)*100);
           $available_paid_holidays_hour = 0;
           $available_paid_holidays_computed = $paid_holiday_info->provided_paid_holidays + $carried_forward_paid_holidays - $consumed_paid_holidays;
           $available_paid_holidays_hour_computed = ($carried_forward_paid_holidays_hour)*60 - $consumed_paid_holidays_hour;
           list($available_day, $available_hour) = $this->convertTimeToDay($available_paid_holidays_hour_computed, $paid_holiday_info->work_time_per_day);
      
           $available_paid_holidays = $available_paid_holidays_computed + $available_day;
        if($available_hour < 0){
           $available_paid_holidays_hour = $paid_holiday_info->work_time_per_day + $available_hour;
        }else {
           $available_paid_holidays_hour = $available_hour;
        }
           $paid_holiday_info->carried_forward_paid_holidays= $carried_forward_paid_holidays;
           $paid_holiday_info->carried_forward_paid_holidays_hour= $carried_forward_paid_holidays_hour;
           $paid_holiday_info->attendance_rate= $attendance_rate;
           $paid_holiday_info->consumed_paid_holidays= $consumed_paid_holidays;
           $paid_holiday_info->consumed_paid_holidays_hour= $consumed_paid_holidays_hour/60;
           $paid_holiday_info->available_paid_holidays= $available_paid_holidays;
           $paid_holiday_info->available_paid_holidays_hour= $available_paid_holidays_hour;
           $paid_holiday_info->update();
           $paid_holiday_info_temp = $paid_holiday_info;
           $consumed_paid_holidays = 0;
           $consumed_paid_holidays_hour = 0;
           $shoutei_temp = $shoutei;
           $kekkin_temp = $kekkin;
           $shoutei = 0;
           $kekkin = 0;
       }
    }// paid_holiday_info
    
        return $success;
    }

        /**
     * Updated a paid holiday information based on work location.
     *
     * @param instance $employee The employee
     *
     * @return boolean
     */
    public function updatePaidHolidayInformationvsWorkLocation($work_location_id){
        $success = true;
  
        $employee_working_days =  EmployeeWorkingDay::whereHas('employeeWorkingInformations', function($query) use ($work_location_id){
                $query->where('planned_work_location_id', $work_location_id)
                    ->orWhereHas('plannedSchedule', function($query) use ($work_location_id) {
                        $query->where('work_location_id', $work_location_id);
                    })
                    ->orWhereHas('workAddressWorkingEmployee', function($query) use ($work_location_id) {
                        $query->whereHas('plannedSchedule', function($query) use ($work_location_id) {
                            $query->where('work_location_id', $work_location_id);
                        });
                    });
        })->notConcluded()->get();
        // b1 from variable $employee_working_days retrieves distinct employee_id
        $employee_ids = $employee_working_days->unique('employee_id')->map(function($working_day) {
                 return $working_day->employee_id;
                });
        // b2 from the $employee_id, withdraw all PaidHolidayInformations from the variable $ paid_holiday_infos
        foreach ($employee_ids as $employee_id) {
                $paid_holiday_infos  = PaidHolidayInformation::where('employee_id', $employee_id)->orderBy('id','asc')->get(); 
                    $consumed_paid_holidays = 0;
                    $consumed_paid_holidays_hour = 0;
                    $paid_holiday_info_temp = null;
                    $attendance_rate = null;
                    $shoutei = 0;
                    $kekkin = 0;
                    $shoutei_temp = 0;
                    $kekkin_temp = 0;
                    $shoutei_rate = 0;
                    $kekkin_rate = 0;

                foreach ($paid_holiday_infos as $key => $paid_holiday_info){
                    $current_period_employee_working_days = EmployeeWorkingDay::where('employee_id',$paid_holiday_info->employee_id)->where('date','>=' ,$paid_holiday_info->period_start)->where('date','<=',$paid_holiday_info->period_end)->get();
                    foreach ($current_period_employee_working_days as $employee_working_day) {
                        $working_day_information = $employee_working_day->getSummaryInformation();
                        $employee = $employee_working_day->employee;
                        $consumed_paid_holidays += $working_day_information['real_taken_paid_rest_days'];
                        $consumed_paid_holidays_hour += $working_day_information['real_taken_paid_rest_time'];

                        if($working_day_information['have_schedule'] == true){
                            $shoutei += 1;
                        }
                        if($working_day_information['real_is_kekkin'] == true){
                            $kekkin += 1;
                        }
                    }
                    // recalculate the information of the cycle
                    if($key == 0){ 
                        // Calculates the attendance rate period.
                       $attendanceRatePeriodArr = [$employee->joined_date, $employee->computePeriodOriginStart()];
                       $working_rate_days = $employee->employeeWorkingDays()->whereBetween('employee_working_days.date', $attendanceRatePeriodArr)->get();
                       foreach ($working_rate_days as $working_rate_day) {
                           $working_rate_day_information = $working_rate_day->getSummaryInformation();
                            if($working_rate_day_information['have_schedule'] == true){
                            $shoutei_rate += 1;
                                }
                            if($working_rate_day_information['real_is_kekkin'] == true){
                            $kekkin_rate += 1;
                            }
                       }
                       $attendance_rate = round($shoutei_rate == 0 ? 0 : (($shoutei_rate - $kekkin_rate)/$shoutei_rate)*100);

                       $available_paid_holidays_hour = 0;
                       $available_paid_holidays_computed = $paid_holiday_info->provided_paid_holidays + $paid_holiday_info->carried_forward_paid_holidays - $consumed_paid_holidays;
                       $available_paid_holidays_hour_computed = $paid_holiday_info->carried_forward_paid_holidays_hour*60 - $consumed_paid_holidays_hour;
                       list($available_day, $available_hour) = $this->convertTimeToDay($available_paid_holidays_hour_computed, $paid_holiday_info->work_time_per_day);
                  
                       $available_paid_holidays = $available_paid_holidays_computed + $available_day;
                    if($available_hour < 0){
                       $available_paid_holidays_hour = $paid_holiday_info->work_time_per_day + $available_hour;
                    }else {
                       $available_paid_holidays_hour = $available_hour;
                    }
                       $paid_holiday_info->attendance_rate= $attendance_rate;
                       $paid_holiday_info->consumed_paid_holidays= $consumed_paid_holidays;
                       $paid_holiday_info->consumed_paid_holidays_hour= $consumed_paid_holidays_hour/60;
                       $paid_holiday_info->available_paid_holidays= $available_paid_holidays;
                       $paid_holiday_info->available_paid_holidays_hour= $available_paid_holidays_hour;
                       $paid_holiday_info->update();
                       $paid_holiday_info_temp = $paid_holiday_info;
                       $consumed_paid_holidays = 0;
                       $consumed_paid_holidays_hour =0;
                       $shoutei_temp = $shoutei;
                       $kekkin_temp = $kekkin;
                       $shoutei = 0;
                       $kekkin = 0;
                   }else{ 
                       $carried_forward_paid_holidays = $paid_holiday_info_temp->provided_paid_holidays <= $paid_holiday_info_temp->available_paid_holidays ? $paid_holiday_info_temp->provided_paid_holidays : $paid_holiday_info_temp->available_paid_holidays;
                       $carried_forward_paid_holidays_hour = $paid_holiday_info_temp->provided_paid_holidays <= $paid_holiday_info_temp->available_paid_holidays ? 0 : $paid_holiday_info_temp->available_paid_holidays_hour;

                       // Calculates the attendance rate period.
                       $attendance_rate = round($shoutei_temp == 0 ? 0 : (($shoutei_temp - $kekkin_temp)/$shoutei_temp)*100);

                       $available_paid_holidays_computed = $paid_holiday_info->provided_paid_holidays + $carried_forward_paid_holidays - $consumed_paid_holidays;
                       $available_paid_holidays_hour_computed = ($carried_forward_paid_holidays_hour)*60 - $consumed_paid_holidays_hour;
                       list($available_day, $available_hour) = $this->convertTimeToDay($available_paid_holidays_hour_computed, $paid_holiday_info->work_time_per_day);
                       $available_paid_holidays_hour = 0;
                       $available_paid_holidays = $available_paid_holidays_computed + $available_day;
                       if($available_hour < 0){
                       $available_paid_holidays_hour = $paid_holiday_info->work_time_per_day + $available_hour;
                        }else {
                           $available_paid_holidays_hour = $available_hour;
                        }
                       
                       $paid_holiday_info->carried_forward_paid_holidays= $carried_forward_paid_holidays;
                       $paid_holiday_info->carried_forward_paid_holidays_hour= $carried_forward_paid_holidays_hour;
                       $paid_holiday_info->attendance_rate= $attendance_rate;
                       $paid_holiday_info->consumed_paid_holidays= $consumed_paid_holidays;
                       $paid_holiday_info->consumed_paid_holidays_hour= $consumed_paid_holidays_hour/60;
                       $paid_holiday_info->available_paid_holidays= $available_paid_holidays;
                       $paid_holiday_info->available_paid_holidays_hour= $available_paid_holidays_hour;
                       $paid_holiday_info->update();
                       $paid_holiday_info_temp = $paid_holiday_info;
                       $consumed_paid_holidays = 0;
                       $consumed_paid_holidays_hour =0;
                       $shoutei_temp = $shoutei;
                       $kekkin_temp = $kekkin;
                       $shoutei = 0;
                       $kekkin = 0;
                   }
                }// paid_holiday_info
            }
            return $success;
    }

     /**
     * Adds a paid holiday information.
     *
     * @param instance $employee The employee
     *
     * @return boolean
     */
    public function addPaidHolidayInformation($employee) {
        $success = false;

        $paidHolidayInformation = $this->getPaidHolidayInformation($employee);
        $success = $paidHolidayInformation->save();
   
        return $success;
    }
}

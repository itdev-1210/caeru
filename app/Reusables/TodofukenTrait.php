<?php

namespace App\Reusables;

use App\Services\TodofukenService;

trait TodofukenTrait 
{
    /**
     * Get todofuken name of this model
     */
    public function todofuken()
    {
        $todofuken_service = resolve(TodofukenService::class);

        return $todofuken_service->getName($this->todofuken);
    }
}
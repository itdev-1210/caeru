<?php

namespace App\Reusables;

use App\Setting;
use App\Employee;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

trait BelongsToWorkLocationTrait
{
    /**
     * Scope a query to get the enable model .
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWorkLocationEnable($query)
    {
        return $query->whereHas('workLocation', function($query) {
            $query->where('enable', true);
        });
    }

    /**
     * Scope add the condition to get the models base on the given WorkLocations
     *
     * @param Builder $query
     * @param string $work_location_id
     *
     * @return Builder
     */
    public function scopeWorkLocations($query, $work_location_id){
        if($work_location_id == 'all') {
            return $query;
        } else {
            return $query
            ->whereIn('work_locations.id',[$work_location_id]);
        }
    }
}
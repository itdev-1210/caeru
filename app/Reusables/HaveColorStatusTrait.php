<?php

namespace App\Reusables;

use Illuminate\Support\Facades\Hash;

trait HaveColorStatusTrait
{
    /**
     * The color statuses array
     */
    protected $color_statuses;

    /**
     * Get the color statuses of this EmployeeWorkingInformation
     *
     * @return array    color status of that field and the fake value(if any)
     */
    public function getColorStatusData()
    {
        if (!isset($this->color_statuses)) {
            $this->initializeColorStatuses();
        }
        return $this->color_statuses;
    }

    /**
     * This function will output an array that include all the neccessary fields with the fake value from the color statuses data.
     * In other words, this array contain all fake data from the color statuses.
     *
     * @return array
     */
    public function getColorStatusButOnlyTheValues()
    {
        if (!isset($this->color_statuses)) {
            $this->initializeColorStatuses();
        }

        return collect($this->color_statuses)->map(function($status) {
            return $status['value'];
        })->toArray();
    }

    /**
     * Load all the ColorStatus instances of this EmployeeWorkingInformation
     *
     * @return void
     */
    protected function initializeColorStatuses()
    {
        $saved_color_statuses = $this->colorStatuses->sortBy('id');

        // Initialize the array that contain all the neccessary fields and color statuses data
        $this->color_statuses = [
            'work_status_id' => $this->emptyColorStatusCell(),
            'rest_status_id' => $this->emptyColorStatusCell(),
            'planned_work_location_id' => $this->emptyColorStatusCell(),
            'real_work_location_id' => $this->emptyColorStatusCell(),
            'planned_work_address_id' => $this->emptyColorStatusCell(),
            'real_work_address_id' => $this->emptyColorStatusCell(),
            'planned_start_work_time' => $this->emptyColorStatusCell(),
            'timestamped_start_work_time' => $this->emptyColorStatusCell(),
            'real_start_work_time' => $this->emptyColorStatusCell(),
            'planned_end_work_time' => $this->emptyColorStatusCell(),
            'timestamped_end_work_time' => $this->emptyColorStatusCell(),
            'real_end_work_time' => $this->emptyColorStatusCell(),
            'planned_break_time' => $this->emptyColorStatusCell(),
            'real_break_time' => $this->emptyColorStatusCell(),
            'planned_night_break_time' => $this->emptyColorStatusCell(),
            'real_night_break_time' => $this->emptyColorStatusCell(),
            'planned_go_out_time' => $this->emptyColorStatusCell(),
            'real_go_out_time' => $this->emptyColorStatusCell(),
            'schedule_start_work_time' => $this->emptyColorStatusCell(),
            'schedule_end_work_time' => $this->emptyColorStatusCell(),
            'planned_total_late_and_leave_early' => $this->emptyColorStatusCell(),
            'real_total_late_and_leave_early' => $this->emptyColorStatusCell(),
            'planned_total_early_arrive_and_overtime' => $this->emptyColorStatusCell(),
            'real_total_early_arrive_and_overtime' => $this->emptyColorStatusCell(),
            'planned_work_span' => $this->emptyColorStatusCell(),
            'real_work_span' => $this->emptyColorStatusCell(),
            'planned_working_hour' => $this->emptyColorStatusCell(),
            'real_working_hour' => $this->emptyColorStatusCell(),
            'note' => $this->emptyColorStatusCell(),
        ];

        // Which has saved data will be overwriten
        foreach ($saved_color_statuses as $color_status) {
            $this->color_statuses[$color_status->field_name] = [
                'color' => $color_status->field_css_class,
                'value' => $color_status->field_fake_value,
            ];
        }
    }

    /**
     * Just a short cut to create an array.
     *
     * @return array
     */
    protected function emptyColorStatusCell()
    {
        return [
            'color' => null,
            'value' => null,
        ];
    }
}
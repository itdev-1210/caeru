<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\ProcessPaidHolidayChangedJob;
use App\Employee;

class ProcessPaidHolidayChangedListener
{

    /**
     * The maximum number of PaidHolidayinfomation to be processed per job.
     */

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function __construct()
    {
     
    }
    
    /**
     * Process Cycle of all the given PaidHolidayinfomation
     *
     * @param onProcessCycle    $event
     * @return void
     */
    public function handle($event)
    {

        $current_company = session('current_company_code');
        if($event->employee instanceOf Employee){

            $job = (new ProcessPaidHolidayChangedJob($event->employee->id, $current_company))->onConnection('database');
    
            dispatch($job);
        }
    }

}
<?php

namespace App\Listeners;

use App\EmployeeWorkingDay;
use App\ChecklistItem;
use App\ConcludedEmployeeWorkingDay;
use App\Events\EmployeeWorkingDayConcluded;
use App\Events\EmployeeWorkingDayUnconcluded;
use App\Events\ManyEmployeeWorkingDaysConcluded;
use App\Events\ManyEmployeeWorkingDaysUnconcluded;
use App\Events\EmployeeWorkingMonthConcluded;
use App\Events\EmployeeWorkingMonthUnconcluded;
use App\Events\CachedPaidHolidayInformationBecomeOld;
use App\Events\CachedPaidHolidayInformationsBecomeOld;
use App\Events\AllEmployeeDataInThisWorkingMonthConcluded;
use App\Events\AllEmployeeDataInThisWorkingMonthUnConcluded;
use App\Services\EvaluateChecklistErrorsService;
use App\Services\TimestampesAndWorkingDayConnectorService;
use Carbon\Carbon;
use DB;

class ConcludedSubscriber
{


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            EmployeeWorkingDayConcluded::class,
            'App\Listeners\ConcludedSubscriber@onWorkingDayConcluded'
        );

        $events->listen(
            EmployeeWorkingDayUnconcluded::class,
            'App\Listeners\ConcludedSubscriber@onWorkingDayUnconcluded'
        );

        $events->listen(
            ManyEmployeeWorkingDaysConcluded::class,
            'App\Listeners\ConcludedSubscriber@onManyWorkingDaysConcluded'
        );

        $events->listen(
            ManyEmployeeWorkingDaysUnconcluded::class,
            'App\Listeners\ConcludedSubscriber@onManyWorkingDaysUnconcluded'
        );

        $events->listen(
            EmployeeWorkingMonthConcluded::class,
            'App\Listeners\ConcludedSubscriber@onWorkingMonthConcluded'
        );

        $events->listen(
            EmployeeWorkingMonthUnconcluded::class,
            'App\Listeners\ConcludedSubscriber@onWorkingMonthUnconcluded'
        );

        $events->listen(
            AllEmployeeDataInThisWorkingMonthConcluded::class,
            'App\Listeners\ConcludedSubscriber@onAllEmployeeDataInWorkingMonthConcluded'
        );

        $events->listen(
            AllEmployeeDataInThisWorkingMonthUnConcluded::class,
            'App\Listeners\ConcludedSubscriber@onAllEmployeeDataInWorkingMonthUnConcluded'
        );
    }


    /**
     * Summarize data for this EmployeeWorkingDay instance, then export those data to ConcludedEmployeeWorkingDay
     *
     * @param EmployeeWorkingDayConcluded    $event
     * @return void
     */
    public function onWorkingDayConcluded($event)
    {
        if (!$event->working_day->isConcluded()) {

            $this->createConcludedRecordsForAWorkingDay($event->working_day);

            // And turn flag conclude_level_one on
            $event->working_day->concluded_level_one = true;
            $event->working_day->concluded_level_one_manager_id = $event->manager->id;
            $event->working_day->save();

            // We need to clear the ConfirmNeeded error for this working day
            $this->clearConfirmNeededError($event->working_day);
        }
    }

    /**
     * On the event of unconcluding a given employee_working_day. Delete the coresponding data from the 'concluded_' table
     *
     * @param EmployeeWorkingDayConcluded    $event
     * @return void
     */
    public function onWorkingDayUnconcluded($event)
    {
        if ($event->working_day->isConcluded()) {

            $event->working_day->concludedEmployeeWorkingDay->delete();
            $event->working_day->concluded_level_one = false;
            $event->working_day->concluded_level_one_manager_id = null;
            $event->working_day->save();

            // For each working info of this working day, delete the concluded data and refresh the cached data.
            foreach ($event->working_day->concludedEmployeeWorkingInformations as $concluded_working_info) {
                $original_working_info = $concluded_working_info->employeeWorkingInformation;
                $concluded_working_info->delete();
                $original_working_info->calculateAndCacheAttributes();
            }

            // We also need to re-evaluation checklist errors for this working info
            $this->triggerRearrangeWorkingTimestampsAndChecklistErrorsEvaluation($event->working_day);

            // Dispatch event to mark the cache of PaidHolidayInformation as 'old', so that it can be re-calculated correctly
            // event(new CachedPaidHolidayInformationBecomeOld($event->working_day));
            // **UPDATED: now that the event to mass toggle paid holiday infos's caches is sent in the connector service, we dont need to do this here
        }
    }

    /**
     * Conclude level one on many working days
     *
     * @param ManyEmployeeWorkingDaysConcluded      $event
     * @return void
     */
    public function onManyWorkingDaysConcluded($event)
    {
        $working_days = EmployeeWorkingDay::with([
                                                'concludedEmployeeWorkingDay',
                                                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                                                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                                                'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
                                                'employeeWorkingInformations.currentRealWorkLocation.setting',
                                                'employeeWorkingInformations.currentRealWorkLocation.company.setting',
                                                'employeeWorkingInformations.currentPlannedWorkLocation.setting',
                                                'employeeWorkingInformations.currentPlannedWorkLocation.company.setting',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.setting',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
                                            ])
                                            ->where('employee_id', $event->employee->id)
                                            ->where('date', '>=', $event->start_date->toDateString())
                                            ->where('date', '<=', $event->end_date->toDateString())
                                            ->get();
        // Turn the collection into a hash table for easy lookup
        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '#' . $working_day->employee_id;
        });

        $new_records_for_concluded_employee_working_day = [];
        $to_be_updated_employee_working_days = [];
        $days_to_delete_confirm_needed_errors = [];
        $employees_to_delete_confirm_needed_errors = [];
        $new_working_days_to_be_created_on_the_fly = collect([]);

        $pivot = $event->start_date->copy();
        while ($pivot->lte($event->end_date)) {

            $unique_key = $pivot->toDateString() . '#' . $event->employee->id;

            $working_day_with_data = isset($working_days[$unique_key]) ? $working_days[$unique_key] : null;

            if ($working_day_with_data) {
                if (!$working_day_with_data->concluded_level_one) {
                    $new_records_for_concluded_employee_working_day[] = $this->createConcludedRecordsForAWorkingDay($working_day_with_data, true);
                    // $working_day_with_data->concluded_level_one = true;
                    // $working_day_with_data->concluded_level_one_manager_id = $event->manager->id;
                    // $working_day_with_data->save();
                    $to_be_updated_employee_working_days[] = $working_day_with_data->id;

                    // We need to clear the ConfirmNeeded error for this working day
                    // $this->clearConfirmNeededError($working_day_with_data);
                    $days_to_delete_confirm_needed_errors[] = $working_day_with_data->date;
                    $employees_to_delete_confirm_needed_errors[] = $working_day_with_data->employee_id;
                }

            } else {
                // $this->createEmployeeWorkingDayOnTheFlyWithConcludedLevelOne($event->employee->id, $pivot->toDateString(), $event->manager);
                $new_working_days_to_be_created_on_the_fly->push([
                    'date'                      => $pivot->toDateString(),
                    'employee_id'               => $event->employee->id,
                    'created_at'                => Carbon::now()->toDateTimeString(),
                    'updated_at'                => Carbon::now()->toDateTimeString(),
                ]);

            }

            $pivot->addDay();
        }

        // Mass create new employee working day on the fly
        DB::table('employee_working_days')->insert($new_working_days_to_be_created_on_the_fly->toArray());
        $newly_created_days = EmployeeWorkingDay::whereIn('date', $new_working_days_to_be_created_on_the_fly->pluck('date')->toArray())
                                ->whereIn('employee_id', $new_working_days_to_be_created_on_the_fly->pluck('employee_id')->toArray())->get();

        foreach ($newly_created_days as $day) {
            $new_records_for_concluded_employee_working_day[] = $this->createConcludedRecordsForAWorkingDay($day, true);
            // $day->concluded_level_one = true;
            // $day->concluded_level_one_manager_id = $event->manager->id;
            // $day->save();
            $to_be_updated_employee_working_days[] = $day->id;
        }

        // Mass create concluded employee working days
        DB::table('concluded_employee_working_days')->insert($new_records_for_concluded_employee_working_day);

        // Mass delete checklist items with type: confirm needed
        DB::table('checklist_items')->whereIn('date', $days_to_delete_confirm_needed_errors)
            ->whereIn('employee_id', $employees_to_delete_confirm_needed_errors)->delete();

        // Mass update attributes on many employee working days
        EmployeeWorkingDay::whereIn('id', $to_be_updated_employee_working_days)->update([
            'concluded_level_one' => true,
            'concluded_level_one_manager_id' => $event->manager->id,
        ]);
    }

    /**
     * Unconcluded level one on many working days
     *
     * @param ManyEmployeeWorkingDaysUnconcluded      $event
     * @return void
     */
    public function onManyWorkingDaysUnconcluded($event)
    {
        $working_days = EmployeeWorkingDay::with([
                                            'concludedEmployeeWorkingDay',
                                            'concludedEmployeeWorkingInformations',
                                        ])
                                        ->where('employee_id', $event->employee->id)
                                        ->where('date', '>=', $event->start_date->toDateString())
                                        ->where('date', '<=', $event->end_date->toDateString())
                                        ->get();

        // Turn the collection into a hash table for easy lookup
        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '#' . $working_day->employee_id;
        });

        $to_be_deleted_concluded_working_days = [];
        $to_be_updated_working_days = collect([]);
        $to_be_deleted_concluded_working_infos = collect([]);

        $pivot = $event->start_date->copy();
        while ($pivot->lte($event->end_date)) {

            $unique_key = $pivot->toDateString() . '#' . $event->employee->id;

            $working_day_with_data = $working_days[$unique_key];

            if ($working_day_with_data->concluded_level_one) {
                $to_be_deleted_concluded_working_days[] = $working_day_with_data->concludedEmployeeWorkingDay->id;

                $to_be_updated_working_days->push($working_day_with_data);

                // For each working info of this working day, delete the concluded data and refresh the cached data.
                // foreach ($working_day_with_data->concludedEmployeeWorkingInformations as $concluded_working_info) {
                //     $original_working_info = $concluded_working_info->employeeWorkingInformation;
                //     $concluded_working_info->delete();
                //     $original_working_info->calculateAndCacheAttributes();
                // }
                $to_be_deleted_concluded_working_infos = $to_be_deleted_concluded_working_infos->merge($working_day_with_data->concludedEmployeeWorkingInformations->pluck('id'));

                // We also need to re-evaluation checklist errors for this working info
                // $this->triggerRearrangeWorkingTimestampsAndChecklistErrorsEvaluation($working_day_with_data);

                // Dispatch event to mark the cache of PaidHolidayInformation as 'old', so that it can be re-calculated correctly
                // event(new CachedPaidHolidayInformationBecomeOld($working_day_with_data));
                // **UPDATED: now that the event to mass toggle paid holiday infos's caches is sent in the connector service, we dont need to do this here
            }

            $pivot->addDay();
        }

        // Mass delete concluded working days and concluded working infos
        DB::table('concluded_employee_working_days')->whereIn('id', $to_be_deleted_concluded_working_days)->delete();
        DB::table('concluded_employee_working_informations')->whereIn('id', $to_be_deleted_concluded_working_infos->toArray())->delete();

        // Mass update employee working days
        EmployeeWorkingDay::whereIn('id', $to_be_updated_working_days->pluck('id')->toArray())->update([
            'concluded_level_one' => false,
            'concluded_level_one_manager_id' => null,
        ]);

        $reloaded_updated_working_days = EmployeeWorkingDay::whereIn('id', $to_be_updated_working_days->pluck('id')->toArray())->get();

        // Mass re-arrange working timestamp and process checklist error evaluation
        $this->triggerRearrangeWorkingTimestampsAndChecklistErrorsEvaluation($reloaded_updated_working_days);

        // Mass toggle cached paid holiday informations **UPDATED: now that the event to mass toggle paid holiday infos's caches is sent in the connector service, we dont need to do this here.
        // event(new CachedPaidHolidayInformationsBecomeOld($reloaded_updated_working_days));

    }

    /**
     * Conclude working information for a whole working month.
     *
     * @param EmployeeWorkingMonthConcluded     $event
     * @return void
     */
    public function onWorkingMonthConcluded($event)
    {
        $working_days = EmployeeWorkingDay::with([
                                                'concludedEmployeeWorkingDay',
                                                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                                                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                                                'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
                                                'employeeWorkingInformations.currentRealWorkLocation.setting',
                                                'employeeWorkingInformations.currentRealWorkLocation.company.setting',
                                                'employeeWorkingInformations.currentPlannedWorkLocation.setting',
                                                'employeeWorkingInformations.currentPlannedWorkLocation.company.setting',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.setting',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
                                            ])
                                            ->where('employee_id', $event->employee->id)
                                            ->where('date', '>=', $event->start_date->toDateString())
                                            ->where('date', '<=', $event->end_date->toDateString())
                                            ->get();
        // Turn the collection into a hash table for easy lookup
        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '#' . $working_day->employee_id;
        });

        $new_records_for_concluded_employee_working_day = [];
        $to_be_updated_employee_working_days = [];
        $days_to_delete_confirm_needed_errors = [];
        $employees_to_delete_confirm_needed_errors = [];
        $new_working_days_to_be_created_on_the_fly = collect([]);

        $pivot = $event->start_date->copy();
        while ($pivot->lte($event->end_date)) {

            $unique_key = $pivot->toDateString() . '#' . $event->employee->id;

            $working_day_with_data = isset($working_days[$unique_key]) ? $working_days[$unique_key] : null;

            if ($working_day_with_data) {
                if (!$working_day_with_data->concluded_level_one) {
                    $new_records_for_concluded_employee_working_day[] = $this->createConcludedRecordsForAWorkingDay($working_day_with_data, true);
                }
                $to_be_updated_employee_working_days[] = $working_day_with_data->id;
                // $working_day_with_data->concluded_level_two = true;
                // $working_day_with_data->concluded_level_two_manager_id = $event->manager->id;
                // $working_day_with_data->save();

                // We need to clear the ConfirmNeeded error for this working day
                // $this->clearConfirmNeededError($working_day_with_data);
                $days_to_delete_confirm_needed_errors[] = $working_day_with_data->date;
                $employees_to_delete_confirm_needed_errors[] = $working_day_with_data->employee_id;

            } else {
                // $this->createEmployeeWorkingDayOnTheFlyWithConcludedLevelTwo($event->employee->id, $pivot->toDateString(), $event->manager);
                $new_working_days_to_be_created_on_the_fly->push([
                    'date'                      => $pivot->toDateString(),
                    'employee_id'               => $event->employee->id,
                    'created_at'                => Carbon::now()->toDateTimeString(),
                    'updated_at'                => Carbon::now()->toDateTimeString(),
                ]);
            }

            $pivot->addDay();
        }

        // Mass create new employee working day on the fly
        DB::table('employee_working_days')->insert($new_working_days_to_be_created_on_the_fly->toArray());
        $newly_created_days = EmployeeWorkingDay::whereIn('date', $new_working_days_to_be_created_on_the_fly->pluck('date')->toArray())
                                ->whereIn('employee_id', $new_working_days_to_be_created_on_the_fly->pluck('employee_id')->toArray())->get();

        foreach ($newly_created_days as $day) {
            $new_records_for_concluded_employee_working_day[] = $this->createConcludedRecordsForAWorkingDay($day, true);
            $to_be_updated_employee_working_days[] = $day->id;
        }

        // Mass create concluded employee working days
        DB::table('concluded_employee_working_days')->insert($new_records_for_concluded_employee_working_day);

        // Mass delete checklist items with type: confirm needed
        DB::table('checklist_items')->whereIn('date', $days_to_delete_confirm_needed_errors)
            ->whereIn('employee_id', $employees_to_delete_confirm_needed_errors)->delete();

        // Mass update attributes on many employee working days
        EmployeeWorkingDay::whereIn('id', $to_be_updated_employee_working_days)->update([
            'concluded_level_two' => true,
            'concluded_level_two_manager_id' => $event->manager->id,
        ]);
    }

    /**
     * Delete the 'concluded_' data, and set concluded_level_one AND concluded_level_two on the relating EmployeeWorkingDay to false.
     *
     * @param EmployeeWorkingMonthUnconcluded   $event
     * @return void
     */
    public function onWorkingMonthUnconcluded($event)
    {
        $working_days = EmployeeWorkingDay::with([
                                                'concludedEmployeeWorkingDay',
                                                'concludedEmployeeWorkingInformations',
                                            ])
                                            ->where('employee_id', $event->employee->id)
                                            ->where('date', '>=', $event->start_date->toDateString())
                                            ->where('date', '<=', $event->end_date->toDateString())
                                            ->get();

        // Turn the collection into a hash table for easy lookup
        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '#' . $working_day->employee_id;
        });

        $to_be_deleted_concluded_working_days = [];
        $to_be_updated_working_days = collect([]);
        $to_be_deleted_concluded_working_infos = collect([]);

        $pivot = $event->start_date->copy();
        while ($pivot->lte($event->end_date)) {

            $unique_key = $pivot->toDateString() . '#' . $event->employee->id;

            $working_day_with_data = $working_days[$unique_key];

            if ($working_day_with_data->concluded_level_two) {
                if (!$working_day_with_data->concluded_level_one) {
                    $to_be_deleted_concluded_working_days[] = $working_day_with_data->concludedEmployeeWorkingDay->id;
                }

                $to_be_updated_working_days->push($working_day_with_data);
                // $working_day_with_data->concluded_level_two = false;
                // $working_day_with_data->concluded_level_two_manager_id = null;
                // $working_day_with_data->save();

                // For each working info of this working day, delete the concluded data and refresh the cached data.
                // foreach ($working_day_with_data->concludedEmployeeWorkingInformations as $concluded_working_info) {
                //     $original_working_info = $concluded_working_info->employeeWorkingInformation;
                //     $concluded_working_info->delete();
                //     $original_working_info->calculateAndCacheAttributes();
                // }
                $to_be_deleted_concluded_working_infos = $to_be_deleted_concluded_working_infos->merge($working_day_with_data->concludedEmployeeWorkingInformations->pluck('id'));

                // We also need to re-evaluation checklist errors for this working info
                // $this->triggerRearrangeWorkingTimestampsAndChecklistErrorsEvaluation($working_day_with_data);

                // Dispatch event to mark the cache of PaidHolidayInformation as 'old', so that it can be re-calculated correctly
                // event(new CachedPaidHolidayInformationBecomeOld($working_day_with_data));
                // **UPDATED: now that the event to mass toggle paid holiday infos's caches is sent in the connector service, we dont need to do this here
            }

            $pivot->addDay();
        }

        // Mass delete concluded working days and concluded working infos
        DB::table('concluded_employee_working_days')->whereIn('id', $to_be_deleted_concluded_working_days)->delete();
        DB::table('concluded_employee_working_informations')->whereIn('id', $to_be_deleted_concluded_working_infos->toArray())->delete();

        // Mass update employee working days
        EmployeeWorkingDay::whereIn('id', $to_be_updated_working_days->pluck('id')->toArray())->update([
            'concluded_level_two' => false,
            'concluded_level_two_manager_id' => null,
        ]);

        $reloaded_updated_working_days = EmployeeWorkingDay::whereIn('id', $to_be_updated_working_days->pluck('id')->toArray())->get();

        // Mass re-arrange working timestamp and process checklist error evaluation
        $this->triggerRearrangeWorkingTimestampsAndChecklistErrorsEvaluation($reloaded_updated_working_days);

        // Mass toggle cached paid holiday informations **UPDATED: now that the event to mass toggle paid holiday infos's caches is sent in the connector service, we dont need to do this here.
        // event(new CachedPaidHolidayInformationsBecomeOld($reloaded_updated_working_days));
    }

    /**
     * Conclude all given employees's working data. This is level two conclude.
     *
     * @param AllEmployeeDataInThisWorkingMonthConcluded    $event
     * @return void
     */
    public function onAllEmployeeDataInWorkingMonthConcluded(AllEmployeeDataInThisWorkingMonthConcluded $event)
    {
        $working_days = EmployeeWorkingDay::with([
                'concludedEmployeeWorkingDay',
                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
                'employeeWorkingInformations.currentRealWorkLocation.setting',
                'employeeWorkingInformations.currentRealWorkLocation.company.setting',
                'employeeWorkingInformations.currentPlannedWorkLocation.setting',
                'employeeWorkingInformations.currentPlannedWorkLocation.company.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            ])
            ->whereIn('employee_id', $event->employees->pluck('id')->toArray())
            ->where('date', '>=', $event->start_date->toDateString())
            ->where('date', '<=', $event->end_date->toDateString())
            ->get();

        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '#' . $working_day->employee_id;
        });

        $new_records_for_concluded_employee_working_day = [];
        $to_be_updated_employee_working_days = [];
        $days_to_delete_confirm_needed_errors = [];
        $employees_to_delete_confirm_needed_errors = [];
        $new_working_days_to_be_created_on_the_fly = collect([]);

        foreach ($event->employees as $employee) {

            $pivot = $event->start_date->copy();

            while ($pivot->lte($event->end_date)) {

                $unique_key = $pivot->format('Y-m-d') . '#' . $employee->id;

                $working_day = isset($working_days[$unique_key]) ? $working_days[$unique_key] : null;

                if ($working_day) {
                    if (!$working_day->concluded_level_two) {
                        if (!$working_day->concluded_level_one) {
                            $new_records_for_concluded_employee_working_day[] = $this->createConcludedRecordsForAWorkingDay($working_day, true);
                        }
                        // $working_day->concluded_level_two = true;
                        // $working_day->concluded_level_two_manager_id = $event->manager->id;
                        // $working_day->save();
                        $to_be_updated_employee_working_days[] = $working_day->id;

                        // We need to clear the ConfirmNeeded error for this working day
                        // $this->clearConfirmNeededError($working_day);
                        $days_to_delete_confirm_needed_errors[] = $working_day->date;
                        $employees_to_delete_confirm_needed_errors[] = $working_day->employee_id;
                    }

                } else {
                    // $this->createEmployeeWorkingDayOnTheFlyWithConcludedLevelTwo($employee->id, $pivot->format('Y-m-d'), $event->manager);
                    $new_working_days_to_be_created_on_the_fly->push([
                        'date'                      => $pivot->toDateString(),
                        'employee_id'               => $employee->id,
                        'created_at'                => Carbon::now()->toDateTimeString(),
                        'updated_at'                => Carbon::now()->toDateTimeString(),
                    ]);
                }

                $pivot->addDay();
            }
        }

        // Mass create new employee working day on the fly
        DB::table('employee_working_days')->insert($new_working_days_to_be_created_on_the_fly->toArray());
        $newly_created_days = EmployeeWorkingDay::whereIn('date', $new_working_days_to_be_created_on_the_fly->pluck('date')->toArray())
                                ->whereIn('employee_id', $new_working_days_to_be_created_on_the_fly->pluck('employee_id')->toArray())->get();

        foreach ($newly_created_days as $day) {
            $new_records_for_concluded_employee_working_day[] = $this->createConcludedRecordsForAWorkingDay($day, true);
            $to_be_updated_employee_working_days[] = $day->id;
        }

        // Mass create concluded employee working days
        if (count($new_records_for_concluded_employee_working_day) > 1000) {
            $junks = array_chunk($new_records_for_concluded_employee_working_day, 1000);
            foreach($junks as $junk) {
                DB::table('concluded_employee_working_days')->insert($junk);
            }
        } else {
            DB::table('concluded_employee_working_days')->insert($new_records_for_concluded_employee_working_day);
        }

        // Mass delete checklist items with type: confirm needed
        DB::table('checklist_items')->whereIn('date', $days_to_delete_confirm_needed_errors)
            ->whereIn('employee_id', $employees_to_delete_confirm_needed_errors)->delete();

        // Mass update attributes on many employee working days
        EmployeeWorkingDay::whereIn('id', $to_be_updated_employee_working_days)->update([
            'concluded_level_two' => true,
            'concluded_level_two_manager_id' => $event->manager->id,
        ]);

    }



    /**
     * Unconclude all given employees's working data. This is level two conclude.
     *
     * @param AllEmployeeDataInThisWorkingMonthUnConcluded $event
     * @return void
     */
    public function onAllEmployeeDataInWorkingMonthUnConcluded($event)
    {
        $working_days = EmployeeWorkingDay::with([
                'concludedEmployeeWorkingDay',
                'concludedEmployeeWorkingInformations',
            ])
            ->whereIn('employee_id', $event->employees->pluck('id')->toArray())
            ->where('date', '>=', $event->start_date->toDateString())
            ->where('date', '<=', $event->end_date->toDateString())
            ->get();

        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '#' . $working_day->employee_id;
        });

        foreach ($event->employees as $employee) {

            $to_be_deleted_concluded_working_days = [];
            $to_be_updated_working_days = collect([]);
            $to_be_deleted_concluded_working_infos = collect([]);

            $pivot = $event->start_date->copy();

            while ($pivot->lte($event->end_date)) {

                $unique_key = $pivot->format('Y-m-d') . '#' . $employee->id;

                $working_day = $working_days[$unique_key];

                if ($working_day->concluded_level_two) {
                    if (!$working_day->concluded_level_one) {
                        $to_be_deleted_concluded_working_days[] = $working_day->concludedEmployeeWorkingDay->id;
                    }
                    // $working_day->concluded_level_two = false;
                    // $working_day->concluded_level_two_manager_id = null;
                    // $working_day->save();
                    $to_be_updated_working_days->push($working_day);

                    // For each working info of this working day, delete the concluded data and refresh the cached data.
                    // foreach ($working_day->concludedEmployeeWorkingInformations as $concluded_working_info) {
                    //     $original_working_info = $concluded_working_info->employeeWorkingInformation;
                    //     $concluded_working_info->delete();
                    //     $original_working_info->calculateAndCacheAttributes();
                    // }
                    $to_be_deleted_concluded_working_infos = $to_be_deleted_concluded_working_infos->merge($working_day->concludedEmployeeWorkingInformations->pluck('id'));

                    // We also need to re-evaluation checklist errors for this working info
                    // $this->triggerRearrangeWorkingTimestampsAndChecklistErrorsEvaluation($working_day);

                    // Dispatch event to mark the cache of PaidHolidayInformation as 'old', so that it can be re-calculated correctly
                    // event(new CachedPaidHolidayInformationBecomeOld($working_day));
                    // **UPDATED: now that the event to mass toggle paid holiday infos's caches is sent in the connector service, we dont need to do this here
                }

                $pivot->addDay();
            }

            // Mass delete concluded working days and concluded working infos
            DB::table('concluded_employee_working_days')->whereIn('id', $to_be_deleted_concluded_working_days)->delete();
            DB::table('concluded_employee_working_informations')->whereIn('id', $to_be_deleted_concluded_working_infos->toArray())->delete();

            // Mass update employee working days
            EmployeeWorkingDay::whereIn('id', $to_be_updated_working_days->pluck('id')->toArray())->update([
                'concluded_level_two' => false,
                'concluded_level_two_manager_id' => null,
            ]);

            $reloaded_updated_working_days = EmployeeWorkingDay::whereIn('id', $to_be_updated_working_days->pluck('id')->toArray())->get();

            // Mass re-arrange working timestamp and process checklist error evaluation
            $this->triggerRearrangeWorkingTimestampsAndChecklistErrorsEvaluation($reloaded_updated_working_days);
        }
    }


    /**
     * If on that day, this employee still does not have any working day instance, create one on the fly for him/her.
     * This working_day instance has an empty working_information instance, of course. And also, has concluded_level_two = true;
     *
     * @param int       $employee_id
     * @param string    $date
     * @param Manager   $manager
     * @return void
     */
    protected function createEmployeeWorkingDayOnTheFlyWithConcludedLevelTwo($employee_id, $date, $manager)
    {

        $new_working_day_instance = new EmployeeWorkingDay();

        $new_working_day_instance->date = $date;
        $new_working_day_instance->employee()->associate($employee_id);
        $new_working_day_instance->save();
        $this->createConcludedRecordsForAWorkingDay($new_working_day_instance);

        // We need to create a new working day in unconclude status, create concluded record for it, then conclude it.
        $new_working_day_instance->concluded_level_two = true;
        $new_working_day_instance->concluded_level_two_manager_id = $manager->id;
        $new_working_day_instance->save();

        return $new_working_day_instance;
    }

    /**
     * Create an EmployeeWorkingDay. Set date and employee for that instance. Also set concluded_level_one to true and set the manager who concluded it.
     *
     * @param int       $employee_id
     * @param string    $date
     * @return void
     */
    protected function createEmployeeWorkingDayOnTheFlyWithConcludedLevelOne($employee_id, $date, $manager)
    {

        $new_working_day_instance = new EmployeeWorkingDay();

        $new_working_day_instance->date = $date;
        $new_working_day_instance->employee()->associate($employee_id);
        $new_working_day_instance->save();
        $this->createConcludedRecordsForAWorkingDay($new_working_day_instance);

        // We need to create a new working day in unconclude status, create concluded record for it, then conclude it.
        $new_working_day_instance->concluded_level_one = true;
        $new_working_day_instance->concluded_level_one_manager_id = $manager->id;
        $new_working_day_instance->save();

        return $new_working_day_instance;
    }

    /**
     * Create 'concluded_' records for an EmployeeWorkingDay when it's concluded.
     *
     * @param EmployeeWorkingDay    $working_day
     * @param boolean               $out_put_record_instead_of_saving - optional : in case you dont want to save the new record right there,
     *                                                                              but return it back, so that you can mass insert later.
     * @return void
     */
    protected function createConcludedRecordsForAWorkingDay($working_day, $out_put_record_instead_of_saving = false)
    {
        // Conclude each working information
        foreach ($working_day->employeeWorkingInformations as $working_info) {
            $working_info->concludeAndExportData();
        }

        $summarized_data = $working_day->getSummaryInformation();

        if ($summarized_data !== null) {
            // Export them to ConcludedEmployeeWorkingDay
            if ($out_put_record_instead_of_saving) {
                $new_record = [
                    'employee_id'               => $working_day->employee_id,
                    'date'                      => $working_day->date,
                    'employee_working_day_id'   => $working_day->id,
                    'created_at'                => Carbon::now()->toDateTimeString(),
                    'updated_at'                => Carbon::now()->toDateTimeString(),
                ];
                $new_record = array_merge($new_record, $summarized_data);

                return $new_record;
            } else {
                $concluded_working_day = new ConcludedEmployeeWorkingDay([
                    'employee_id'               => $working_day->employee_id,
                    'date'                      => $working_day->date,
                    'employee_working_day_id'   => $working_day->id,
                ]);
                $concluded_working_day->fill($summarized_data);
                $concluded_working_day->save();
            }
        }
    }

    /**
     * Trigger re-arrange and re-connect WorkingTimestamps to EmployeeWorkingDay then evaluate Checklist Errors
     *
     * @param EmployeeWorkingDay|collection    $working_day_or_days
     * @return void
     */
    protected function triggerRearrangeWorkingTimestampsAndChecklistErrorsEvaluation($working_day_or_days)
    {
        // $evaluate_service = resolve(EvaluateChecklistErrorsService::class);

        // $evaluate_service->evaluateChecklistErrorFromWorkingTimestampList($working_day);

        $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);

        if (is_a($working_day_or_days, EmployeeWorkingDay::class)) {
            $connector_service->rearrangeAndConnectTheTimestampsToWorkingDays([$working_day_or_days]);
        } else {
            $connector_service->rearrangeAndConnectTheTimestampsToWorkingDays($working_day_or_days);
        }
    }

    /**
     * Clear ConfirmNeeded Errors for the given EmployeeWorkingDay
     *
     * @param EmployeeWorkingDay    $working_day
     */
    protected function clearConfirmNeededError($working_day)
    {
        ChecklistItem::where('date', $working_day->date)
                        ->where('employee_id', $working_day->employee_id)
                        ->where('item_type', ChecklistItem::CONFIRM_NEEDED)
                        ->delete();
    }

}
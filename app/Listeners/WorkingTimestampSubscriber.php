<?php

namespace App\Listeners;

use App\Services\EvaluateChecklistErrorsService;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use App\CachedEmployeeWorkingInformation;

class WorkingTimestampSubscriber
{

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\WorkingTimestampChanged',
            'App\Listeners\WorkingTimestampSubscriber@onWorkingTimestampListChanged'
        );

        $events->listen(
            'App\Events\WorkingTimestampChangedOnManyWorkingDays',
            'App\Listeners\WorkingTimestampSubscriber@onWorkingTimestampListOfManyDaysChanged'
        );
    }


    /**
     * Listen to the WorkingTimestampChanged event and calculate the timestamped_start_work_time, timestamped_end_work_time and also the go_out_time
     * of all EmployeeWorkingInformations of that EmployeeWorkingDay
     *
     * @param \App\Events\WorkingTimestampChanged $event
     * @return void
     */
    public function onWorkingTimestampListChanged($event)
    {

        $evaluate_service = resolve(EvaluateChecklistErrorsService::class);

        $evaluate_service->evaluateChecklistErrorFromWorkingTimestampListOfManyWorkingDay([$event->working_day->id]);

    }

    /**
     * When working timestamps of many working days have been changed, re-distribute them to their respective EmployeeWorkingDay's EmployeeWorkingInformations.
     * Only re-distribute working timestamps, we will re-evaluate checklist errors later in a background process.
     *
     * @param App\Events\WorkingTimestampChangedOnManyWorkingDays   $event
     * @return void
     */
    public function onWorkingTimestampListOfManyDaysChanged($event)
    {
        $evaluate_service = resolve(EvaluateChecklistErrorsService::class);

        $temporary_ewis = $evaluate_service->resetAllTemporaryEmployeeWorkingInformationsOfTheseEmployeeWorkingDays($event->working_day_ids);

        $working_days = EmployeeWorkingDay::with([
            'workingTimestamps',
            'employee.workLocation.company',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            'employeeWorkingInformations.colorStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
            'employeeWorkingInformations.workAddressWorkingEmployee.plannedSchedule',
            'employeeWorkingInformations.workAddressWorkingEmployee.workAddressWorkingInformation.workAddressWorkingEmployees.plannedSchedule',
            'employeeWorkingInformations.workAddressWorkingEmployee.workAddressWorkingInformation.workAddressWorkingDay.workAddress.workLocation.company',
            // 'employeeWorkingInformations.currentPlannedWorkLocation',
            // 'employeeWorkingInformations.currentPlannedWorkLocation.setting',
            // 'employeeWorkingInformations.currentPlannedWorkLocation.company.setting',
            // 'employeeWorkingInformations.currentRealWorkLocation.setting',
            // 'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.setting',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
        ])->whereIn('id', $event->working_day_ids)->get();

        foreach($working_days as $working_day) {
            $evaluate_service->distributeWorkingTimestampWithoutEvaluatingChecklistErrors($working_day, $temporary_ewis);
        }

        // Query again to ensure the new temporary employee working informations will also have cache marked as 'old' (and thus will be re-calculated when loaded)
        $employee_working_information_ids = EmployeeWorkingInformation::whereIn('employee_working_day_id', $working_days->pluck('id')->toArray())->pluck('id')->toArray();

        // Then we have to mark the cache of EWIs of those day as 'old'
        CachedEmployeeWorkingInformation::whereIn('employee_working_information_id', $employee_working_information_ids)->update([
            'old' => true,
        ]);
    }
}
<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use App\PlannedSchedule;
use App\CalendarRestDay;
use App\Services\ScheduleProcessingService;
use App\Events\DistributeTimestampsAndReevaluateChecklistErrors;
use App\Events\CachedEmployeeWorkingInformationBecomeOld;


class CalendarRestDaysChangedListener
{    
    /**
     * With the new CalendarRestDays setting
     *
     * @param onProcessCycle    $event
     * @return void
     */
    public function handle($event)
    {
        $affected_month = Carbon::createFromFormat('Y-m', $event->changed_month);

        $affected_planned_schedules = PlannedSchedule::with([
            'employeeWorkingInformations.employeeWorkingDay',
            'workLocation.calendarRestDays',
        ])->whereIn('work_location_id', $event->work_location_ids)->where('prioritize_company_calendar', true)->get();

        $service = resolve(ScheduleProcessingService::class);

        foreach ($affected_planned_schedules as $schedule) {
            $service->processSchedule($schedule, $affected_month, $event->changed_days, count($event->work_location_ids) > 1);
        }
    }

    /**
     * Process one schedule: delete relating models(except some conditionaly though) then add new those models again, with the new setting, of course.
     * This function is similar to when a schedule is being updated, but it has some differences. 
     *
     * @param PlannedSchedule   $schedule
     * @param Carbon            $affected_month     only year and month of this carbon instance will be used.
     */
    // protected function processSchedule(PlannedSchedule $schedule, Carbon $affected_month)
    // {
    //     $service = resolve(ScheduleProcessingService::class);

    //     $day_range = $service->startAndEndDayOfMonth($affected_month);

    //     // Check the date period again to create some new EmployeeWorkingDays
    //     $period = $service->getDatePeriod($schedule, $day_range);
    //     $employee = $schedule->employee;
    //     $service->initializeWorkingDay($employee, $period);

    //     $have_ewi_deleted_working_days = $service->deleteRelatingModels($schedule, $day_range);

    //     // Proceed to re-distribute the WorkingTimestamp of these EmployeeWorkingDays
    //     // event(new DistributeTimestampsAndReevaluateChecklistErrors($have_ewi_deleted_working_days->pluck('id')->toArray()));

    //     // The remaining employee_working_info will change according to the updated schedule (meaning their cached values is old, and need to be refresh)
    //     // event(new CachedEmployeeWorkingInformationBecomeOld($remaining_infos));

    //     if ($schedule->work_address_id) {
    //         $service->initializeWorkAddressWorkingInformationsMatchedTimeRange($schedule, $have_ewi_deleted_working_days, $day_range);
    //     } else {
    //         $service->initializeEmployeeWorkingInformationsMatchedTimeRange($schedule, $have_ewi_deleted_working_days, $day_range);
    //     }
    // }
}
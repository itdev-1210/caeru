<?php

namespace App\Listeners;

use App\Events\AbsorbTheMatchingTemporaryWorkingInformation;

class AbsorbTheMatchingTemporaryWorkingInformationListener
{

    /**
     * Find in the same EmployeeWorkingDay any EmployeeWorkingInformation that have temporary == true, and the same work location with the given
     * EmployeeWorkingInformation then merge it(them) with the given one.
     *
     * @param AbsorbTheMatchingTemporaryWorkingInformation      $event
     * @return void
     */
    public function handle(AbsorbTheMatchingTemporaryWorkingInformation $event)
    {
        $working_info = $event->working_info;

        // Ignore any WorkAddress related EmployeeWorkingInformation
        if ($working_info->planned_work_address_id == null) {

            // If the planned_work_location_id has been changed
            if ($working_info->planned_work_location_id != $working_info->getOriginal('planned_work_location_id')) {
                $working_infos_of_this_day = $working_info->employeeWorkingDay->employeeWorkingInformations()->orderBy('id')->get()->keyBy('id')->forget($working_info->id);

                // Find the matching temporary instance
                $first_matching_working_info = $working_infos_of_this_day->first(function($other_working_info) use($working_info) {
                    return ($other_working_info->temporary == true) && ($other_working_info->planned_work_location_id === $working_info->planned_work_location_id);
                });

                // And assign its value to this instance, and then delete it
                if ($first_matching_working_info) {
                    $working_info->real_work_location_id = ($first_matching_working_info->real_work_location_id) ? $first_matching_working_info->real_work_location_id : $working_info->real_work_location_id;
                    $working_info->real_go_out_time = ($first_matching_working_info->real_go_out_time) ? $first_matching_working_info->real_go_out_time : $working_info->real_go_out_time;
                    $working_info->timestamped_start_work_time = ($first_matching_working_info->timestamped_start_work_time) ? $first_matching_working_info->timestamped_start_work_time : $working_info->timestamped_start_work_time;
                    $working_info->timestamped_start_work_time_work_location_id = ($first_matching_working_info->timestamped_start_work_time_work_location_id) ?
                                                                                    $first_matching_working_info->timestamped_start_work_time_work_location_id :
                                                                                    $working_info->timestamped_start_work_time_work_location_id;
                    $working_info->timestamped_start_work_time_working_timestamp_id = ($first_matching_working_info->timestamped_start_work_time_working_timestamp_id) ?
                                                                                    $first_matching_working_info->timestamped_start_work_time_working_timestamp_id :
                                                                                    $working_info->timestamped_start_work_time_working_timestamp_id;
                    $working_info->timestamped_end_work_time = ($first_matching_working_info->timestamped_end_work_time) ? $first_matching_working_info->timestamped_end_work_time : $working_info->timestamped_end_work_time;
                    $working_info->timestamped_end_work_time_work_location_id = ($first_matching_working_info->timestamped_end_work_time_work_location_id) ?
                                                                                    $first_matching_working_info->timestamped_end_work_time_work_location_id :
                                                                                    $working_info->timestamped_end_work_time_work_location_id;
                    $working_info->timestamped_end_work_time_working_timestamp_id = ($first_matching_working_info->timestamped_end_work_time_working_timestamp_id) ?
                                                                                    $first_matching_working_info->timestamped_end_work_time_working_timestamp_id :
                                                                                    $working_info->timestamped_end_work_time_working_timestamp_id;

                    $first_matching_working_info->delete();
                    return true;
                }
                return false;
            }
        }
    }
}
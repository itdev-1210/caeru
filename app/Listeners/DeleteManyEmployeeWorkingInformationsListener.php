<?php

namespace App\Listeners;

use App\ColorStatus;
use App\ChecklistItem;
use App\ChecklistErrorTimer;
use App\CachedEmployeeWorkingInformation;
use App\EmployeeWorkingInformationSnapshot;
use App\EmployeeWorkingInformation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld;

class DeleteManyEmployeeWorkingInformationsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // All these procedure are from the EmployeeWorkingInformationObserver except for the event calling part, it has to use another event.

        // Delete the checklist items (or errors)
        ChecklistItem::whereIn('employee_working_information_id', $event->employee_working_information_ids)->delete();

        // Delete the timers too
        ChecklistErrorTimer::whereIn('employee_working_information_id', $event->employee_working_information_ids)->delete();

        // Delete the cached model
        CachedEmployeeWorkingInformation::whereIn('employee_working_information_id', $event->employee_working_information_ids)->delete();

        // Delete the snapshot, also, we have to take the id so that we can use it to delete the color statuses of those snapshots too
        $snapshot_ids = EmployeeWorkingInformationSnapshot::whereIn('employee_working_information_id', $event->employee_working_information_ids)->pluck('id')->toArray();
        EmployeeWorkingInformationSnapshot::whereIn('employee_working_information_id', $event->employee_working_information_ids)->orWhere('soon_to_be_EWI_id', $event->employee_working_information_ids)->delete();

        // Delete the color status of this employee working information and of the snapshots that this working information has
        ColorStatus::where(function($query) use ($event) {
            $query->where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION)->where('colorable_id', $event->employee_working_information_ids);
        })->orWhere(function($query) use ($snapshot_ids) {
            $query->where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION_SNAPSHOT)->whereIn('colorable_id', $snapshot_ids);
        })->delete();

        // event(new CachedPaidHolidayInformationBecomeOld($working_info->employeeWorkingDay));
        // Instead of firing the above event, we will use another event (for the multiple EWI case).
        event(new CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld(array_unique($event->employee_ids)));

        // Actual deletion of EmployeeWorkingInformation
        EmployeeWorkingInformation::whereIn('id', $event->employee_working_information_ids)->delete();

    }
}

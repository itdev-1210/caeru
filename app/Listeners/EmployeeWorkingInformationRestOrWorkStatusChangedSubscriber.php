<?php

namespace App\Listeners;

use App\EmployeeWorkingInformation;
use App\WorkStatus;
use App\RestStatus;
use App\Events\EmployeeWorkingInformationWorkStatusChanged;
use App\Events\EmployeeWorkingInformationRestStatusChanged;
use App\Events\CachedPaidHolidayInformationBecomeOld;
use App\Events\ResetWorkStatusColor;
use App\Events\ResetRestStatusColor;

class EmployeeWorkingInformationRestOrWorkStatusChangedSubscriber
{


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            EmployeeWorkingInformationRestStatusChanged::class,
            'App\Listeners\EmployeeWorkingInformationRestOrWorkStatusChangedSubscriber@onRestStatusChanged'
        );

        $events->listen(
            EmployeeWorkingInformationWorkStatusChanged::class,
            'App\Listeners\EmployeeWorkingInformationRestOrWorkStatusChangedSubscriber@onWorkStatusChanged'
        );
    }

    /**
     * If the new rest_status is a day_unit type (Except for gokyuu and zenkyuu), all the other EmployeeWorkingInformations of this EmployeeWorkingDay
     * will also be changed to that rest_status. And in the opposite direction, all the other will be set to null.
     *
     * @param EmployeeWorkingInformationRestStatusChanged   $event
     * @return void
     */
    public function onRestStatusChanged($event)
    {
        $new_rest_status_id = $event->new_rest_status_id;

        $old_rest_status_id = $event->working_info->planned_rest_status_id;

        if ($new_rest_status_id !== $old_rest_status_id && $this->takeAWholeDayToRest($new_rest_status_id)) {

            // In this case set the rest status of all other working infos to this new rest status
            foreach($event->working_day->employeeWorkingInformations as $working_info) {
                if ($working_info->id !== $event->working_info->id && $working_info->planned_work_address_id == null) {
                    $working_info->planned_rest_status_id = $new_rest_status_id;
                    $working_info->planned_work_status_id = null;
                    // Any 'temporary' working_info which is effected by this will become 'not-temporary'
                    $working_info->temporary = false;
                    $working_info->save();

                    event(new ResetRestStatusColor($working_info));
                    // $this->removeTheColorStatusIfAny($working_info, 'rest_status_id');
                }
            }
        } else if ($new_rest_status_id !== $old_rest_status_id && $this->takeAWholeDayToRest($old_rest_status_id)) {

            // In this case set the rest status of all other working infos to null
            foreach($event->working_day->employeeWorkingInformations as $working_info) {
                if ($working_info->id !== $event->working_info->id && $working_info->planned_work_address_id == null) {
                    $working_info->planned_rest_status_id = null;
                    // Any 'temporary' working_info which is effected by this will become 'not-temporary'
                    $working_info->temporary = false;
                    $working_info->save();

                    event(new ResetRestStatusColor($working_info));
                    // $this->removeTheColorStatusIfAny($working_info, 'rest_status_id');
                }
            }
        }

        // Dispatch event to mark the cache of PaidHolidayInformation as 'old', so that it can be re-calculated correctly
        event(new CachedPaidHolidayInformationBecomeOld($event->working_day));
    }

    /**
     * if the new work status is KEKKIN, all the other EmployeeWorkingInformations of this EmployeeWorkingDay will
     * also be changed to KEKKIN. And in the opposite direction, all the other will be set to null.
     *
     * @param EmployeeWorkingInformationWorkStatusChanged   $event
     * @return void
     */
    public function onWorkStatusChanged($event)
    {
        $new_work_status_id = $event->new_work_status_id;

        $old_work_status_id = $event->working_info->planned_work_status_id;

        if ($new_work_status_id !== $old_work_status_id && $new_work_status_id === WorkStatus::KEKKIN) {

            // In this case set the work status of all other working infos to this new work status
            foreach($event->working_day->employeeWorkingInformations as $working_info) {
                if ($working_info->id !== $event->working_info->id && $working_info->planned_work_address_id == null) {
                    $working_info->planned_work_status_id = $new_work_status_id;
                    $working_info->planned_rest_status_id = null;
                    // Any 'temporary' working_info which is effected by this will become 'not-temporary'
                    $working_info->temporary = false;
                    $working_info->save();

                    event(new ResetWorkStatusColor($working_info));
                    // $this->removeTheColorStatusIfAny($working_info, 'work_status_id');
                }
            }
        } else if ($new_work_status_id !== $old_work_status_id && $old_work_status_id === WorkStatus::KEKKIN) {

            // In this case set the work status of all other working infos to null
            foreach($event->working_day->employeeWorkingInformations as $working_info) {
                if ($working_info->id !== $event->working_info->id && $working_info->planned_work_address_id == null) {
                    $working_info->planned_work_status_id = null;
                    // Any 'temporary' working_info which is effected by this will become 'not-temporary'
                    $working_info->temporary = false;
                    $working_info->save();

                    event(new ResetWorkStatusColor($working_info));
                    // $this->removeTheColorStatusIfAny($working_info, 'work_status_id');
                }
            }
        }
    }

    /**
     * Remove the color status of a specific field on a given EmployeeWorkingInformation
     *
     * @param \App\EmployeeWorkingInformation       $working_info
     * @param string                                $field_name
     * @return void
     */
    protected function removeTheColorStatusIfAny($working_info, $field_name)
    {
        $color_statuses = $working_info->colorStatuses()->where('field_name', $field_name)->get();
        foreach ($color_statuses as $color_status) {
            $color_status->delete();
        }
    }

    /**
     * Check if the given rest_status is a day_unit type (Notice: even though gokyuu and zenkyuu are day_unit type, they are handled differently).
     *
     * @param int       $rest_status_id
     * @return boolean
     */
    protected function takeAWholeDayToRest($rest_status_id)
    {
        switch ($rest_status_id) {
            case RestStatus::YUUKYU_1:
            case RestStatus::YUUKYU_2:
                return true;
            case RestStatus::GOKYUU_1:
            case RestStatus::GOKYUU_2:
            case RestStatus::ZENKYUU_1:
            case RestStatus::ZENKYUU_2:
            case RestStatus::HANKYUU_1:
            case RestStatus::HANKYUU_2:
            case RestStatus::JIYUU:
                return false;
            default:
                $rest_status = RestStatus::find($rest_status_id);
                return $rest_status && ($rest_status->unit_type == true) ? true : false;
        }
    }

}
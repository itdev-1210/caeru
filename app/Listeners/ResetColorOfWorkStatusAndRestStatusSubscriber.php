<?php

namespace App\Listeners;

use App\EmployeeWorkingInformation;
use App\Events\ResetWorkStatusColor;
use App\Events\ResetRestStatusColor;

class ResetColorOfWorkStatusAndRestStatusSubscriber
{


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            ResetWorkStatusColor::class,
            'App\Listeners\ResetColorOfWorkStatusAndRestStatusSubscriber@resetWorkStatusColor'
        );

        $events->listen(
            ResetRestStatusColor::class,
            'App\Listeners\ResetColorOfWorkStatusAndRestStatusSubscriber@resetRestStatusColor'
        );
    }


    /**
     * Reset work_status color of a given EmployeeWorkingInformation
     *
     * @param ResetWorkStatusColor  $event
     * @return void
     */
    public function resetWorkStatusColor(ResetWorkStatusColor $event)
    {
        $working_info = $event->employee_working_info;

        $color_statuses = $working_info->colorStatuses()->where('field_name', 'work_status_id')->get();
        foreach ($color_statuses as $color_status) {
            $color_status->delete();
        }

        // This field's color status will be changed to white so we dont need the function changeColorOfSnapshot()
        // These fields are sure to be one-record-per-attribute, that's why we can just get one out and delete it
        $snapshot = $working_info->employeeWorkingInformationSnapshot;
        if ($snapshot) {
            $snapshot_color_status = $snapshot->colorStatuses()->where('field_name', 'left_work_status_id')->first();
            if ($snapshot_color_status) $snapshot_color_status->delete();
        }
    }

    /**
     * Reset rest_status color of a given EmployeeWorkingInformation
     *
     * @param ResetRestStatusColor  $event
     * @return void
     */
    public function resetRestStatusColor(ResetRestStatusColor $event)
    {
        $working_info = $event->employee_working_info;

        $color_statuses = $working_info->colorStatuses()->where('field_name', 'rest_status_id')->get();
        foreach ($color_statuses as $color_status) {
            $color_status->delete();
        }

        // This field's color status will be changed to white so we dont need the function changeColorOfSnapshot()
        // These fields are sure to be one-record-per-attribute, that's why we can just get one out and delete it
        $snapshot = $working_info->employeeWorkingInformationSnapshot;
        if ($snapshot) {

            // Remove colors on all three attributes
            $snapshot_left_rest_status_color = $snapshot->colorStatuses()->where('field_name', 'left_rest_status_id')->first();
            if ($snapshot_left_rest_status_color) $snapshot_left_rest_status_color->delete();
            $snapshot_left_paid_rest_time_start_color = $snapshot->colorStatuses()->where('field_name', 'left_paid_rest_time_start')->first();
            if ($snapshot_left_paid_rest_time_start_color) $snapshot_left_paid_rest_time_start_color->delete();
            $snapshot_left_paid_rest_time_end_color = $snapshot->colorStatuses()->where('field_name', 'left_paid_rest_time_end')->first();
            if ($snapshot_left_paid_rest_time_end_color) $snapshot_left_paid_rest_time_end_color->delete();
        }
    }
}
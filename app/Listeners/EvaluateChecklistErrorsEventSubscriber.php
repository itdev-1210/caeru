<?php

namespace App\Listeners;

use App\PlannedSchedule;
use App\EmployeeWorkingInformation;
use App\Services\EvaluateChecklistErrorsService;
use App\Events\ReevaluateChecklistErrors;
use App\Events\CachedEmployeeWorkingInformationBecomeOld;
use App\Events\EvaluateChecklistErrorsForEWIFromPlannedSchedule;
use App\Events\DistributeTimestampsAndReevaluateChecklistErrors;
use App\Jobs\ReevaluateChecklistErrorsJob;
use App\Jobs\DistributeTimestampsAndReevaluateChecklistErrorsJob;
use Carbon\Carbon;

class EvaluateChecklistErrorsEventSubscriber
{

    /**
     * The maximum number of EmployeeWorkingInformations to be processed per ReevaluateChecklistErrorsJob.
     */
    const MAXIMUM_EWI_PER_JOB = 500;

    /**
     * The maximum number of EmployeeWorkingDay to be processed per DistributeTimestampsAndReevaluateChecklistErrorsJob.
     */
    const MAXIMUM_EWD_PER_JOB = 500;

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            ReevaluateChecklistErrors::class,
            'App\Listeners\EvaluateChecklistErrorsEventSubscriber@onReevaluating'
        );

        $events->listen(
            EvaluateChecklistErrorsForEWIFromPlannedSchedule::class,
            'App\Listeners\EvaluateChecklistErrorsEventSubscriber@onEvaluatingEWIFromPlannedSchedule'
        );

        $events->listen(
            DistributeTimestampsAndReevaluateChecklistErrors::class,
            'App\Listeners\EvaluateChecklistErrorsEventSubscriber@onDistributeTimestampsAndEvaluateChecklistErrorsOfWorkingDays'
        );
    }

    /**
     * Reevalute the checklist errors of all the given EmployeeWorkingDay
     *
     * @param ReevaluateChecklistErrors    $event
     * @return void
     */
    public function onReevaluating(ReevaluateChecklistErrors $event)
    {
        $current_company = session('current_company_code');

        if (count($event->employee_working_info_ids) > self::MAXIMUM_EWI_PER_JOB) {
            $chunks = collect($event->employee_working_info_ids)->chunk(self::MAXIMUM_EWI_PER_JOB);

            // Dispatch a job for each chunks
            foreach($chunks->toArray() as $chunk) {

                $job = (new ReevaluateChecklistErrorsJob($chunk, $current_company))->onConnection('database');

                dispatch($job);

            }

        } else {

            $job = (new ReevaluateChecklistErrorsJob($event->employee_working_info_ids, $current_company))->onConnection('database');
    
            dispatch($job);
        }

    }

    /**
     * Evaluate a bunch of EmployeeWorkingInformations from the PlannedSchedule
     *
     * @param EvaluateChecklistErrorsForEWIFromPlannedSchedule    $event
     * @return void
     */
    public function onEvaluatingEWIFromPlannedSchedule(EvaluateChecklistErrorsForEWIFromPlannedSchedule $event)
    {
        if (!$event->through_WAWE) {
            $schedule = PlannedSchedule::with('employeeWorkingInformations')
                                        ->where('id', $event->planned_schedule_id)->first();

            $working_info_ids = $schedule->employeeWorkingInformations->pluck('id');

        } else {
            $schedule = PlannedSchedule::with('workAddressWorkingEmployees.employeeWorkingInformation')
                                        ->where('id', $event->planned_schedule_id)->first();

            $working_info_ids = $schedule->workAddressWorkingEmployees->map(function($working_employee) {
                return $working_employee->employeeWorkingInformation->id;
            });
        }

        event(new CachedEmployeeWorkingInformationBecomeOld($working_info_ids));
    }

    /**
     * Distribute the working timestamps of these WorkingDays to their respective WorkingInformations
     *
     * @param DistributeTimestampsAndReevaluateChecklistErrors  $event
     * @return void
     */
    public function onDistributeTimestampsAndEvaluateChecklistErrorsOfWorkingDays(DistributeTimestampsAndReevaluateChecklistErrors $event)
    {
        $current_company = session('current_company_code') ? session('current_company_code') : $event->reserved_company_code;

        if (count($event->employee_working_day_ids) > self::MAXIMUM_EWD_PER_JOB) {
            $chunks = collect($event->employee_working_day_ids)->chunk(self::MAXIMUM_EWD_PER_JOB);

            // Dispatch a job for each chunks
            foreach($chunks->toArray() as $chunk) {

                $job = (new DistributeTimestampsAndReevaluateChecklistErrorsJob($chunk, $current_company))->onConnection('database')->delay(Carbon::now()->addMinutes(2));

                dispatch($job);

            }

        } else {

            $job = (new DistributeTimestampsAndReevaluateChecklistErrorsJob($event->employee_working_day_ids, $current_company))->onConnection('database')->delay(Carbon::now()->addMinutes(2));

            dispatch($job);
        }
    }
}
<?php

namespace App\Listeners;

use App\ColorStatus;
use App\Events\TimeStampedStartWorkTimeAttributeManuallyChanged;
use App\Events\TimeStampedEndWorkTimeAttributeManuallyChanged;
use App\Events\RealGoOutTimeAttributeManuallyChanged;

class TimestampedAttributesManuallyChangedSubscriber
{


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            TimeStampedStartWorkTimeAttributeManuallyChanged::class,
            'App\Listeners\TimestampedAttributesManuallyChangedSubscriber@manuallyChangedStartWork'
        );

        $events->listen(
            TimeStampedEndWorkTimeAttributeManuallyChanged::class,
            'App\Listeners\TimestampedAttributesManuallyChangedSubscriber@manuallyChangedEndWork'
        );

        $events->listen(
            RealGoOutTimeAttributeManuallyChanged::class,
            'App\Listeners\TimestampedAttributesManuallyChangedSubscriber@manuallyChangedRealGoOutTime'
        );
    }


    /**
     * Check if those 'timestamped_' attributes were changed or not. If changed, create ColorStatus.
     *
     * @param TimeStampedStartWorkTimeAttributeManuallyChanged   $event
     * @return void
     */
    public function manuallyChangedStartWork(TimeStampedStartWorkTimeAttributeManuallyChanged $event)
    {
        $this->createColorStatus($event->working_info->id, 'timestamped_start_work_time', $event->blank_color, $event->blue_color);
    }


    /**
     * Check if those 'timestamped_' attributes were changed or not. If changed, create ColorStatus.
     *
     * @param TimeStampedEndWorkTimeAttributeManuallyChanged   $event
     * @return void
     */
    public function manuallyChangedEndWork(TimeStampedEndWorkTimeAttributeManuallyChanged $event)
    {
        $this->createColorStatus($event->working_info->id, 'timestamped_end_work_time', $event->blank_color, $event->blue_color);
    }


    /**
     * Check if real_go_out_time attributes were changed or not. If changed, create ColorStatus.
     *
     * @param RealGoOutTimeAttributeManuallyChanged   $event
     * @return void
     */
    public function manuallyChangedRealGoOutTime(RealGoOutTimeAttributeManuallyChanged $event)
    {
        $this->createColorStatus($event->working_info->id, 'real_go_out_time', $event->blank_color);
    }


    /**
     * A shortcut to create a ColorStatus record.
     *
     * @param int       $working_info_id
     * @param string    $field_name
     * @param boolean   $blank_color
     * @param boolean   $blue_color
     * @return void
     */
    protected function createColorStatus($working_info_id, $field_name, $blank_color = false, $blue_color = false)
    {
        ColorStatus::create([
            'colorable_type' => ColorStatus::EMPLOYEE_WORKING_INFORMATION,
            'colorable_id' => $working_info_id,
            'field_name' => $field_name,
            'field_css_class' => $blue_color ? ColorStatus::APPROVED_COLOR : ($blank_color ? ColorStatus::BLANK_COLOR : ColorStatus::UPDATED_COLOR),
        ]);
    }

}
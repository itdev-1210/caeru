<?php

namespace App\Listeners;

use App\PaidHolidayInformation;
use App\CachedPaidHolidayInformation;
use App\Events\CachedPaidHolidayInformationBecomeOld;
use App\Events\CachedPaidHolidayInformationsBecomeOld;
use App\Events\CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld;

class PaidHolidayInformationCacheSubscriber
{

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            CachedPaidHolidayInformationBecomeOld::class,
            'App\Listeners\PaidHolidayInformationCacheSubscriber@onPaidHolidayInfoBecomeOld'
        );

        $events->listen(
            CachedPaidHolidayInformationsBecomeOld::class,
            'App\Listeners\PaidHolidayInformationCacheSubscriber@onPaidHolidayInfosBecomeOld'
        );

        $events->listen(
            CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld::class,
            'App\Listeners\PaidHolidayInformationCacheSubscriber@onPaidHolidayInfoOfSeveralEmployeesBecomeOld'
        );
    }

    /**
     * Search the appropriate PaidHolidayInformation instance to toggle its cache's 'old' attribute.
     *
     * @param CachedPaidHolidayInformationBecomeOld     $event
     * @return void
     */
    public function onPaidHolidayInfoBecomeOld(CachedPaidHolidayInformationBecomeOld $event)
    {
        $paid_holiday_info = PaidHolidayInformation::where('period_start', '<=', $event->working_day->date)
                                ->where('period_end', '>=', $event->working_day->date)
                                ->where('employee_id', $event->working_day->employee_id)
                                ->first();
        
        if ($paid_holiday_info) {
            $cache = $paid_holiday_info->cachedPaidHolidayInformation;

            if ($cache) {
                $cache->old = true;
                $cache->save();
            }
        }
    }

    /**
     * Search the appropriate PaidHolidayInformation instances to toggle its cache's 'old' attribute.
     * The working days inside the event must be consecutive and belong to one employee
     *
     * @param CachedPaidHolidayInformationsBecomeOld     $event
     * @return void
     */
    public function onPaidHolidayInfosBecomeOld(CachedPaidHolidayInformationsBecomeOld $event)
    {

        $working_days = $event->working_days->sortBy('date');

        $smallest_day = $working_days->first();
        $biggest_day = $working_days->last();

        $paid_holiday_infos = PaidHolidayInformation::with('cachedPaidHolidayInformation')->where(function($query) use ($biggest_day) {
                                    return $query->where('period_end', '>=', $biggest_day->date)->where('period_start', '<=', $biggest_day->date);
                                })
                                ->orWhere(function($query) use ($smallest_day) {
                                    return $query->where('period_end', '>=', $smallest_day->date)->where('period_start', '<=', $smallest_day->date);
                                })
                                ->orWhere(function($query) use ($biggest_day, $smallest_day) {
                                    return $query->where('period_end', '<=', $biggest_day->date)->where('period_start', '>=', $smallest_day->date);
                                })
                                ->where('employee_id', $smallest_day->employee_id)
                                ->get();

        CachedPaidHolidayInformation::whereIn('id', $paid_holiday_infos->pluck('cachedPaidHolidayInformation.id'))->update([
            'old' => true,
        ]);
    }

    /**
     * Toggle all of these employees's PaidHolidayInformation instances to old.
     *
     * @param CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld   $event
     * @return void
     */
    public function onPaidHolidayInfoOfSeveralEmployeesBecomeOld(CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld $event)
    {
        $cache_ids = CachedPaidHolidayInformation::whereHas('paidHolidayInformation', function($query) use ($event) {
            return $query->whereIn('employee_id', $event->employee_ids);
        })->pluck('id')->toArray();

        CachedPaidHolidayInformation::whereIn('id', $cache_ids)->update([
            'old' => true,
        ]);
    }
}
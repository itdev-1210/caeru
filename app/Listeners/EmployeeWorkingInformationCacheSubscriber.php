<?php

namespace App\Listeners;

use App\CachedEmployeeWorkingInformation;
use App\EmployeeWorkingInformation;
use App\Events\CachedEmployeeWorkingInformationBecomeOld;
use App\Events\ReevaluateChecklistErrors;

class EmployeeWorkingInformationCacheSubscriber
{


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            CachedEmployeeWorkingInformationBecomeOld::class,
            'App\Listeners\EmployeeWorkingInformationCacheSubscriber@onWorkingInfoBecomeOld'
        );
    }


    /**
     * Mark all the given CachedEmployeeWorkingInformation as 'old', so that they can be refreshed.
     *
     * @param CachedEmployeeWorkingInformationBecomeOld    $event
     * @return void
     */
    public function onWorkingInfoBecomeOld(CachedEmployeeWorkingInformationBecomeOld $event)
    {
        if ($event->all == true) {
            CachedEmployeeWorkingInformation::query()->update([
                'old' => true,
            ]);

            $all_employee_working_information_ids = EmployeeWorkingInformation::pluck('id')->toArray();
            event(new ReevaluateChecklistErrors($all_employee_working_information_ids));

        } else {
            CachedEmployeeWorkingInformation::whereIn('employee_working_information_id', $event->employee_working_info_ids)->update([
                'old' => true,
            ]);

            event(new ReevaluateChecklistErrors($event->employee_working_info_ids));
        }
    }

}
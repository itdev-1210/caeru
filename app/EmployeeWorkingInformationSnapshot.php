<?php

namespace App;

use Carbon\Carbon;
use App\EmployeeWorkingInformation;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeWorkingInformationSnapshot extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Constant for the request statuses
     */
    const APPROVED      = 1;
    const CONSIDERING   = 2;
    const CURRENT       = 3;
    const PREVIOUS      = 4;
    const DENIED        = 5;

    /**
     * Get the working day instance of this working information instance
     */
    public function employeeWorkingDay()
    {
        return $this->belongsTo(EmployeeWorkingDay::class);
    }

    /**
     * Get the employee working indormation instance
     */
    public function employeeWorkingInformation()
    {
        return $this->belongsTo(EmployeeWorkingInformation::class);
    }

    /**
     * Get all the color statuses for this working information instance
     */
    public function colorStatuses()
    {
        return $this->morphMany(ColorStatus::class, 'colorable');
    }

    /**
     * Calculate Working hour CONSIDERING status
     * @return $working_hour
     */
    public function calcWorkingTime(EmployeeWorkingInformation $working_information_with_data)
    {
        //** ScheduleStartWorkTime - did declare carbon instance
        $schedule_start_work_time = $working_information_with_data->attributes['schedule_start_work_time'];

        $schedule_start_work_time = ($schedule_start_work_time === config('caeru.empty_date')) ? null : $schedule_start_work_time;
        $carbon_schedule_start_work_time = $working_information_with_data->getCarbonInstance($schedule_start_work_time);
        /////////////////////////////////////////////////////////////////////////////////////////


        //** ScheduleEndWorkTime - did declare carbon instance
        $schedule_end_work_time = $working_information_with_data->attributes['schedule_end_work_time'];

        $schedule_end_work_time = ($schedule_end_work_time === config('caeru.empty_date')) ? null : $schedule_end_work_time;
        $carbon_schedule_end_work_time = $working_information_with_data->getCarbonInstance($schedule_end_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** ScheduleBreakTime
        $schedule_break_time = $working_information_with_data->attributes['schedule_break_time'];

        $schedule_break_time = ($schedule_break_time === config('caeru.empty')) ? null : $schedule_break_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** ScheduleNightBreakTime
        $schedule_night_break_time = $working_information_with_data->attributes['schedule_night_break_time'];

        $schedule_night_break_time = ($schedule_night_break_time === config('caeru.empty')) ? null : $schedule_night_break_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** ScheduleWorkingHour - did declare carbon instance
        $schedule_working_hour = $working_information_with_data->attributes['schedule_working_hour'];

        $schedule_working_hour = ($schedule_working_hour === config('caeru.empty_time')) ? null : $schedule_working_hour;
        $carbon_schedule_working_hour = $working_information_with_data->getCarbonInstance($schedule_working_hour);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkLocationId - did retrieve the relating Model
        $planned_work_location_id = $working_information_with_data->attributes['planned_work_location_id'];
        $planned_work_location = $working_information_with_data->affectedByWorkLocation();
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkStatusId
        $planned_work_status_id = $working_information_with_data->attributes['planned_work_status_id'];

        // $planned_work_location
        if ($planned_work_location) {
            $available_work_statuses = $planned_work_location->activatingWorkStatuses()->pluck('id')->toArray();
            $planned_work_status_id =  in_array($planned_work_status_id, $available_work_statuses) ? $planned_work_status_id : null;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedWorkStatusHouDeOrKyuuDe - no need to cache this
        $is_planned_work_status_houde_or_kyuude_or_zangyou = false;
        $is_planned_work_status_houde_or_kyuude_or_zangyou = in_array($planned_work_status_id, [WorkStatus::HOUDE, WorkStatus::KYUUDE, WorkStatus::ZANGYOU]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedWorkStatusKekkinOrFurikyuu - no need to cache this
        $is_planned_work_status_kekkin_or_furikyuu = false;
        $is_planned_work_status_kekkin_or_furikyuu = in_array($planned_work_status_id, [WorkStatus::KEKKIN, WorkStatus::FURIKYUU]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedRestStatusId - did retrieve the relating Model
        $planned_rest_status_id = $working_information_with_data->attributes['planned_rest_status_id'];

        // $planned_work_location
        if ($planned_work_location) {
            $available_rest_statuses = $planned_work_location->activatingRestStatuses()->pluck('id')->toArray();
            $planned_rest_status_id =  in_array($planned_rest_status_id, $available_rest_statuses) ? $planned_rest_status_id : null;
        }

        // Why do i have to do this ? Because of a Freaking wierd bug, that only appears on test server in a certain situation.
        $planned_rest_status = ($planned_rest_status_id !== null) ? RestStatus::find($planned_rest_status_id) : null;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsRestStatusUnitDayOrHour - no need to cache this
        $is_rest_status_unit_day = false;

        if ($planned_rest_status) {
            $is_rest_status_unit_day = ($planned_rest_status->unit_type == true);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedRestStatusGouKyuOrZenKyuu - no need to cache this
        $is_planned_rest_status_goukyuu_or_zenkyuu = null;
        $is_planned_rest_status_goukyuu_or_zenkyuu = in_array($planned_rest_status_id, [RestStatus::GOKYUU_1, RestStatus::GOKYUU_2, RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedRestStatusGouKyu - no need to cache this
        $is_planned_rest_status_goukyuu = null;
        $is_planned_rest_status_goukyuu = in_array($planned_rest_status_id, [RestStatus::GOKYUU_1, RestStatus::GOKYUU_2]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPlannedRestStatusZenkyuu - no need to cache this
        $is_planned_rest_status_zenkyuu = null;
        $is_planned_rest_status_zenkyuu = in_array($planned_rest_status_id, [RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2]);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PaidRestTimeStart - did declare carbon instance - no need to cach this
        $paid_rest_time_start = $working_information_with_data->attributes['paid_rest_time_start'];
        $carbon_paid_rest_time_start = $working_information_with_data->getCarbonInstance($paid_rest_time_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PaidRestTimeEnd - did declare carbon instance - no need to cach this
        $paid_rest_time_end = $working_information_with_data->attributes['paid_rest_time_end'];
        $carbon_paid_rest_time_end = $working_information_with_data->getCarbonInstance($paid_rest_time_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PaidRestTimePeriod - no need to cache this
        $paid_rest_time_period_in_minutes = $working_information_with_data->convertToMinutes($working_information_with_data->getCarbonInstance($working_information_with_data->attributes['paid_rest_time_period']));
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** NotIncludeBreakTimeWhenDisplayPlannedTime - no need to cache this
        $not_include_break_time_when_display_planned_time = $working_information_with_data->attributes['not_include_break_time_when_display_planned_time'];
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalPaidRestTimeInMinutes - no need to cach this
        $total_paid_rest_time_in_minutes = 0;

        // $carbon_paid_rest_time_start 
        // $carbon_paid_rest_time_end
        if ($is_rest_status_unit_day == false && $carbon_paid_rest_time_start !== null && $carbon_paid_rest_time_end !== null) {
            $total_paid_rest_time = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

            // $carbon_schedule_working_hour
            $working_hour_in_minutes = $working_information_with_data->convertToMinutes($carbon_schedule_working_hour);
            $total_paid_rest_time_in_minutes = $total_paid_rest_time > $working_hour_in_minutes ? $working_hour_in_minutes : $total_paid_rest_time;

        } elseif ($is_rest_status_unit_day == false && $paid_rest_time_period_in_minutes !== null) {
            $total_paid_rest_time_in_minutes = $paid_rest_time_period_in_minutes;

        } elseif ($is_planned_rest_status_goukyuu_or_zenkyuu) {
            // $carbon_schedule_working_hour
            $total_paid_rest_time_in_minutes = ($carbon_schedule_working_hour !== null) ? ceil($working_information_with_data->convertToMinutes($carbon_schedule_working_hour)/2) : null;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TakeAWholeDayOff - no need to cach this
        $take_a_whole_day_off = false;

        // $carbon_schedule_working_hour
        if ($carbon_schedule_working_hour !== null) {
            $working_hour_in_minutes = $working_information_with_data->convertToMinutes($carbon_schedule_working_hour);

            if ($is_rest_status_unit_day == true || ($total_paid_rest_time_in_minutes !== 0 && $total_paid_rest_time_in_minutes === $working_hour_in_minutes)) {
                $take_a_whole_day_off = true;
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedEarlyArriveStart - did declare carbon instance
        $planned_early_arrive_start = $working_information_with_data->attributes['planned_early_arrive_start'];
        $planned_early_arrive_start = $take_a_whole_day_off == true ? null : $planned_early_arrive_start;

        $carbon_planned_early_arrive_start = $working_information_with_data->getCarbonInstance($planned_early_arrive_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedEarlyArriveEnd - did declare carbon instance - no need to cach this
        $planned_early_arrive_end = $working_information_with_data->attributes['planned_early_arrive_end'];
        $planned_early_arrive_end = $take_a_whole_day_off == true ? null : $planned_early_arrive_end;

        $carbon_planned_early_arrive_end = $working_information_with_data->getCarbonInstance($planned_early_arrive_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalPlannedEarlyArriveTimeInMinutes - no need to cach this
        $total_planned_early_arrive_time_in_minutes = 0;

        // $carbon_planned_early_arrive_start
        // $carbon_planned_early_arrive_end
        if ($carbon_planned_early_arrive_start !== null && $carbon_planned_early_arrive_end !== null) {
            $total_planned_early_arrive_time_in_minutes = $carbon_planned_early_arrive_end->diffInMinutes($carbon_planned_early_arrive_start);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedOvertimeStart - did declare carbon instance - no need to cach this
        $planned_overtime_start = $working_information_with_data->attributes['planned_overtime_start'];
        $planned_overtime_start = $take_a_whole_day_off == true ? null : $planned_overtime_start;

        $carbon_planned_overtime_start = $working_information_with_data->getCarbonInstance($planned_overtime_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedOvertimeEnd - did declare carbon instance - no need to cach this
        $planned_overtime_end = $working_information_with_data->attributes['planned_overtime_end'];
        $planned_overtime_end = $take_a_whole_day_off == true ? null : $planned_overtime_end;

        $carbon_planned_overtime_end = $working_information_with_data->getCarbonInstance($planned_overtime_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalPlannedOverTimeInMinutes - no need to cach this
        $total_planned_overtime_in_minutes = 0;

        // $carbon_planned_overtime_start
        // $carbon_planned_overtime_end
        if ($carbon_planned_overtime_start !== null && $carbon_planned_overtime_end !== null) {
            $total_planned_overtime_in_minutes = $carbon_planned_overtime_end->diffInMinutes($carbon_planned_overtime_start);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsOnlyWorkingHourAndBreakTimeMode - no need to cach this
        $is_only_working_hour_and_break_time_mode = ($schedule_start_work_time === null) &&
                                                    ($schedule_end_work_time === null) &&
                                                    ($schedule_working_hour !== null);
        ///////////////////////////////////////////////////////////////////////////////////////////



        //** PlannedBreakTime
        $planned_break_time = $working_information_with_data->attributes['planned_break_time'];

        if (!$take_a_whole_day_off && !$is_planned_rest_status_goukyuu_or_zenkyuu) {
            if ($planned_break_time === null) {

                $planned_break_time = $schedule_break_time;
            }

        } else {
            $planned_break_time = null;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedOvertimeDueToReduceBreakTime
        $planned_overtime_due_to_reduce_break_time =   ($planned_break_time !== null &&
                                                        $schedule_break_time !== null &&
                                                        $planned_break_time < $schedule_break_time) ? $schedule_break_time - $planned_break_time : 0;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedLateTime - no need to cach this
        $planned_late_time =  $working_information_with_data->attributes['planned_late_time'];
        $planned_late_time = ($take_a_whole_day_off || $is_only_working_hour_and_break_time_mode) ? null : $planned_late_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedEarlyLeaveTime - no need to cach this
        $planned_early_leave_time = $working_information_with_data->attributes['planned_early_leave_time'];
        $planned_early_leave_time = $take_a_whole_day_off ? null : $planned_early_leave_time;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RemainingPlannedWorkSpanTimeInMinutes - no need to cach this
        $remaining_planned_work_span_time_in_minutes = 0;

        // $carbon_schedule_working_hour
        if ($carbon_schedule_working_hour !== null) {
            $total_working_hour = $working_information_with_data->convertToMinutes($carbon_schedule_working_hour);
            $late_time = ($planned_late_time !== null) ? $planned_late_time : 0;
            $early_leave = ($planned_early_leave_time !== null) ? $planned_early_leave_time : 0;

            $remaining_time = $total_working_hour - $late_time - $early_leave - $total_paid_rest_time_in_minutes;

            $remaining_planned_work_span_time_in_minutes = ($remaining_time >= 0) ? $remaining_time : 0;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkLocationId - did retrieve the relating model
        $real_work_location_id = $working_information_with_data->real_work_location_id;
        $real_work_location = $working_information_with_data->affectedByRealWorkLocation();
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TimestampedStartWorkTime - did declare the carbon instance. This attribute, we cach it, but not use it. 
        // For using it, the timestamps distribute mechanism (in WorkingTimestampSubscriber) will get messed up.
        $timestamped_start_work_time = $this->attributes['left_timestamped_start_work_date'] . ' ' . $this->attributes['left_timestamped_start_work_time'];

        $carbon_timestamped_start_work_time = $working_information_with_data->getCarbonInstance($timestamped_start_work_time);
        if ($timestamped_start_work_time) {
            $round_up_by = $real_work_location->currentSetting()->start_time_round_up;

            $carbon_timestamped_start_work_time->minute = ceil($carbon_timestamped_start_work_time->minute/$round_up_by) * $round_up_by;
            $timestamped_start_work_time = $carbon_timestamped_start_work_time->format('Y-m-d H:i:s');
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TimestampedEndWorkTime - did declare the carbon instance. This attribute, we cach it, but not use it. 
        // For using it, the timestamps distribute mechanism (in WorkingTimestampSubscriber) will get messed up.
        $timestamped_end_work_time = $this->attributes['left_timestamped_end_work_date'] . ' ' . $this->attributes['left_timestamped_end_work_time'];

        $carbon_timestamped_end_work_time = $working_information_with_data->getCarbonInstance($timestamped_end_work_time);
        if ($timestamped_end_work_time) {
            $round_down_by = $real_work_location->currentSetting()->end_time_round_down;

            $carbon_timestamped_end_work_time->minute = floor($carbon_timestamped_end_work_time->minute/$round_down_by) * $round_down_by;
            $timestamped_end_work_time = $carbon_timestamped_end_work_time->format('Y-m-d H:i:s');
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalAtWorkTimeInMinutes - no need to cach this
        $total_at_work_time_in_minutes = 0;
        if ($schedule_break_time !== null) {
            $break_time = $schedule_break_time !== null ? $schedule_break_time : 0;
            $total_time = $working_information_with_data->convertToMinutes($carbon_schedule_working_hour) + $break_time;
            $total_at_work_time_in_minutes = $total_time;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** IsPaidRestTimeInsideEstimatedWorkTime - no need to cach this
        $is_paid_rest_time_inside_estimated_work_time = true;
        if ($paid_rest_time_start !== null && $paid_rest_time_end !== null && $timestamped_start_work_time !== null) {
            $carbon_maximum_end_work_time = $carbon_timestamped_start_work_time->copy()->addMinutes($total_at_work_time_in_minutes);

            if ($carbon_timestamped_start_work_time->lte($carbon_paid_rest_time_start) &&
            $carbon_paid_rest_time_start->lte($carbon_paid_rest_time_end) &&
            $carbon_paid_rest_time_end->lte($carbon_maximum_end_work_time)) {

                $is_paid_rest_time_inside_estimated_work_time = true;
            } else {
                $is_paid_rest_time_inside_estimated_work_time = false;
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** OffsetTimeToAddOrSubWhenTakeHalfDayOff - no need to cach this - did declare the zero carbon instance
        $offset_time_to_add_or_sub_when_take_half_day_off = 0;
        if ($schedule_start_work_time && $schedule_end_work_time) {
            // $carbon_schedule_start_work_time
            // $carbon_schedule_end_work_time
            $total_minutes = $carbon_schedule_start_work_time->diffInMinutes($carbon_schedule_end_work_time);

            // ((a + c) + c)/2 = a/2 + c. And [a/2 + c] is what we need.
            $offset_time_to_add_or_sub_when_take_half_day_off = ($total_minutes + $schedule_break_time)/2;

        } elseif ($schedule_working_hour) {
            // $carbon_schedule_working_hour
            $carbon_zero = $working_information_with_data->getCarbonInstance('0:0:0');

            $offset_time_to_add_or_sub_when_take_half_day_off = ($carbon_schedule_working_hour->diffInMinutes($carbon_zero))/2;
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkSpan - did declare the carbon instance
        $planned_work_span = null;

        if ($take_a_whole_day_off !== true && $remaining_planned_work_span_time_in_minutes > 0) {
            $total_work_span_in_minutes = $total_at_work_time_in_minutes - $planned_break_time;

            // In these case, the totalAtWorkTime is still the sum of break time and working hour, but the break time is automatically set to 0, that's why we have to subtract the break time once more before calculating
            if ($is_planned_rest_status_goukyuu_or_zenkyuu) {
                $total_work_span_in_minutes -= $schedule_break_time;
            }

            $total_work_span_in_minutes -= $total_paid_rest_time_in_minutes;

            if ($planned_late_time !== null) {
                $total_work_span_in_minutes -= $planned_late_time;
            }
            if ($planned_early_leave_time !== null) {
                $total_work_span_in_minutes -= $planned_early_leave_time;
            }

            // Remove the planned overtime-due-to-reduce-break-time if it's bigger than 0
            $total_work_span_in_minutes =   ($planned_break_time !== null &&
                                            $schedule_break_time !== null &&
                                            $planned_break_time < $schedule_break_time) ?
                                            $total_work_span_in_minutes - ($schedule_break_time - $planned_break_time) :
                                            $total_work_span_in_minutes;

            // With the new specs, the PlannedWorkSpan is limited to smaller than or equal to ScheduleWorkingHour
            $schedule_working_hour_in_minutes = $carbon_schedule_working_hour->hour * 60 + $carbon_schedule_working_hour->minute;
            $total_work_span_in_minutes = ($total_work_span_in_minutes > $schedule_working_hour_in_minutes) ? $schedule_working_hour_in_minutes : $total_work_span_in_minutes;
            $planned_work_span = ($total_work_span_in_minutes > 0) ? $working_information_with_data->minutesToString($total_work_span_in_minutes) : '00:00:00';
        }

        $carbon_planned_work_span = $working_information_with_data->getCarbonInstance($planned_work_span);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkSpanStart - did declare carbon instance
        $planned_work_span_start = null;

        if (!$take_a_whole_day_off && $remaining_planned_work_span_time_in_minutes > 0) {

            // $carbon_schedule_start_work_time
            // $carbon_schedule_end_work_time

            if ($carbon_schedule_start_work_time && $carbon_schedule_end_work_time) {

                // Because we will need to mutate this variable, so we need to make a clone
                $carbon_schedule_start = $carbon_schedule_start_work_time->copy();

                $offset = 0;
                if ($is_planned_rest_status_zenkyuu) {
                    $offset = $offset_time_to_add_or_sub_when_take_half_day_off;

                } elseif ($is_rest_status_unit_day == false && $paid_rest_time_start && $paid_rest_time_end) {
                    // $carbon_paid_rest_time_start
                    // $carbon_paid_rest_time_end
                    if ($carbon_paid_rest_time_start->eq($carbon_schedule_start)) {
                        $offset = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

                        // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be added to this offset
                        if ($not_include_break_time_when_display_planned_time == true) {
                            $offset += $schedule_break_time;
                        }

                    } else {
                        $diff_from_schedule_start = $carbon_paid_rest_time_start->diffInMinutes($carbon_schedule_start);
                        if ($diff_from_schedule_start <= $planned_late_time) {
                            $offset = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

                            // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be added to this offset
                            if ($not_include_break_time_when_display_planned_time == true) {
                                $offset += $schedule_break_time;
                            }
                        }
                    }
                }

                if ($offset > 0) {
                    $carbon_schedule_start->addMinutes($offset);
                }
                if ($planned_late_time) {
                    $carbon_schedule_start->addMinutes($planned_late_time);
                }

                $planned_work_span_start = $carbon_schedule_start->format('Y-m-d H:i:s');
            }
        }

        $carbon_planned_work_span_start = $working_information_with_data->getCarbonInstance($planned_work_span_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkSpanEnd - did declare the carbon instance
        $planned_work_span_end = null;

        if (!$take_a_whole_day_off && $remaining_planned_work_span_time_in_minutes > 0) {

            // $carbon_schedule_start_work_time
            // $carbon_schedule_end_work_time

            if ($carbon_schedule_start_work_time && $carbon_schedule_end_work_time) {

                // This one will be mutated
                $carbon_schedule_end = $carbon_schedule_end_work_time->copy();

                $offset = 0;
                if ($is_planned_rest_status_goukyuu) {
                    $offset = $offset_time_to_add_or_sub_when_take_half_day_off;

                } elseif ($is_rest_status_unit_day == false && $paid_rest_time_start && $paid_rest_time_end) {
                    // $carbon_paid_rest_time_start
                    // $carbon_paid_rest_time_end
                    if ($carbon_paid_rest_time_end->eq($carbon_schedule_end)) {
                        $offset = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

                        // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be added to this offset
                        if ($not_include_break_time_when_display_planned_time == true) {
                            $offset += $schedule_break_time;
                        }

                    } else {
                        $diff_from_schedule_end = $carbon_schedule_end->diffInMinutes($carbon_paid_rest_time_end);
                        if ($diff_from_schedule_end <= $planned_early_leave_time) {
                            $offset = $carbon_paid_rest_time_end->diffInMinutes($carbon_paid_rest_time_start);

                            // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be added to this offset
                            if ($not_include_break_time_when_display_planned_time == true) {
                                $offset += $schedule_break_time;
                            }
                        }
                    }

                }

                if ($offset > 0) {
                    $carbon_schedule_end->subMinutes($offset);
                }
                if ($planned_early_leave_time) {
                    $carbon_schedule_end->subMinutes($planned_early_leave_time);
                }

                $planned_work_span_end = $carbon_schedule_end->format('Y-m-d H:i:s');

            }
        }

        $carbon_planned_work_span_end = $working_information_with_data->getCarbonInstance($planned_work_span_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedStartWorkTime - did declare the carbon instance
        $planned_start_work_time = null;

        if ($is_planned_work_status_houde_or_kyuude_or_zangyou) {
            $planned_start_work_time = $planned_overtime_start;
        } elseif ($total_planned_early_arrive_time_in_minutes > 0) {
            $planned_start_work_time = ($planned_work_span_start !== null && $planned_work_span_end !== null) ? $planned_early_arrive_start : null;
        } else {
            $planned_start_work_time = $planned_work_span_start;
        }

        $carbon_planned_start_work_time = $working_information_with_data->getCarbonInstance($planned_start_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedEndWorkTime - did declare the carbon instance
        $planned_end_work_time = null;

        if ($is_planned_work_status_houde_or_kyuude_or_zangyou) {
            $planned_end_work_time = $planned_overtime_end;
        } elseif ($total_planned_overtime_in_minutes > 0) {
            $planned_end_work_time = ($planned_work_span_start !== null && $planned_work_span_end !== null) ? $planned_overtime_end : null;
        } else {
            $planned_end_work_time = $planned_work_span_end;
        }

        $carbon_planned_end_work_time = $working_information_with_data->getCarbonInstance($planned_end_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** PlannedWorkingHour - did declare the carbon instance
        $planned_working_hour = null;

        if (!$take_a_whole_day_off && $remaining_planned_work_span_time_in_minutes > 0) {
            $total_planned_work_span_in_minutes = $working_information_with_data->convertToMinutes($carbon_planned_work_span);

            $total_planned_work_span_in_minutes += $total_planned_early_arrive_time_in_minutes + $total_planned_overtime_in_minutes;

            if ($is_planned_rest_status_goukyuu_or_zenkyuu != true) {
                $total_planned_work_span_in_minutes += $planned_overtime_due_to_reduce_break_time;
            }

            $planned_working_hour = $working_information_with_data->minutesToString($total_planned_work_span_in_minutes);
        } else if ($is_planned_work_status_houde_or_kyuude_or_zangyou) {
            // $carbon_planned_start_work_time
            // $carbon_planned_end_work_time

            if ($carbon_planned_start_work_time !== null && $carbon_planned_end_work_time !== null) {
                $break_time = $planned_break_time !== null ? $planned_break_time : 0;
                $total_working_hour_in_minutes = $carbon_planned_start_work_time->diffInMinutes($carbon_planned_end_work_time) - $break_time;

                $planned_working_hour = $working_information_with_data->minutesToString($total_working_hour_in_minutes);
            }

        }

        $carbon_planned_working_hour = $working_information_with_data->getCarbonInstance($planned_working_hour);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealBreakTime
        $real_break_time = $this->attributes['left_real_break_time'];

        if ($timestamped_start_work_time === null ||
            $timestamped_end_work_time === null ||
            $take_a_whole_day_off == true ||
            $is_planned_work_status_kekkin_or_furikyuu == true ||
            $is_planned_rest_status_goukyuu_or_zenkyuu == true) {

            $real_break_time = null;

        } elseif ($real_break_time === null) {

            if ($timestamped_start_work_time !== null && $timestamped_end_work_time !== null) {
                $real_break_time = $planned_break_time;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealOvertimeDueToReduceBreakTime
        $real_overtime_due_to_reduce_break_time =  ($real_break_time !== null &&
                                                    $schedule_break_time !== null &&
                                                    $real_break_time < $schedule_break_time) ? $schedule_break_time - $real_break_time : 0;
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** CarbonEstimateEndWorkTime - no need to cach this. !!!NOTE!!!: this is a carbon instance
        $carbon_estimated_end_work_time = null;

        if ($is_only_working_hour_and_break_time_mode == true && $timestamped_start_work_time !== null) {

            if ($planned_overtime_start !== null && $planned_overtime_end !== null) {
                $carbon_estimated_end_work_time = $carbon_planned_overtime_end->copy();

            } else {
                $early_leave = ($planned_early_leave_time !== null) ? $planned_early_leave_time : 0;
                $max_working_hour_in_minutes = $total_at_work_time_in_minutes - $early_leave - $total_paid_rest_time_in_minutes;

                if ($is_planned_rest_status_goukyuu_or_zenkyuu) {
                    $break_time_from_the_schedule = ($schedule_break_time !== null) ? $schedule_break_time : 0;
                    $max_working_hour_in_minutes -= $break_time_from_the_schedule;
                }

                // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be subtracted from this
                if ($not_include_break_time_when_display_planned_time == true) {
                    $break_time_from_the_schedule = ($schedule_break_time !== null) ? $schedule_break_time : 0;
                    $max_working_hour_in_minutes -= $break_time_from_the_schedule;
                }

                $carbon_estimated_end_work_time = $carbon_timestamped_start_work_time->copy();
                $carbon_estimated_end_work_time->addMinutes($max_working_hour_in_minutes);
            }

        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** CarbonEstimatedLatestStartWorkTme - no need to cached this
        $carbon_estimated_latest_start_work_time = null;

        if ($is_only_working_hour_and_break_time_mode == true && $planned_overtime_start !== null && $planned_overtime_end !== null) {
            $work_time_in_minutes = $working_information_with_data->convertToMinutes($carbon_planned_work_span);
            $break_time_in_minutes = $planned_break_time !== null ? $planned_break_time : 0;

            // Update 2018-12-25: if not_include_break_time_when_display_planned_time is on, then the schedule_break_time will be subtracted from this
            $to_be_subtracted_time = $work_time_in_minutes + $break_time_in_minutes;
            if ($not_include_break_time_when_display_planned_time == true) {
                $break_time_from_the_schedule = ($schedule_break_time !== null) ? $schedule_break_time : 0;
                $to_be_subtracted_time -= $break_time_from_the_schedule;
            }
            
            $carbon_estimated_latest_start_work_time = $carbon_planned_overtime_start->copy()->subMinutes($to_be_subtracted_time);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealStartWorkTime - did declare the carbon instance
        $real_start_work_time = null;
        if ($timestamped_start_work_time !== null && !$is_planned_work_status_kekkin_or_furikyuu && !$take_a_whole_day_off) {
            // $carbon_timestamped_start_work_time

            if ($paid_rest_time_start !== null && $paid_rest_time_end !== null) {
                // $carbon_paid_rest_time_start
                // $carbon_paid_rest_time_end

                if ($carbon_paid_rest_time_start->lte($carbon_timestamped_start_work_time) && $carbon_timestamped_start_work_time->lte($carbon_paid_rest_time_end)) {

                    // Update 2018-12-25 in the case of not_include_break_time... is on, we have to remove the schedule_breal_time form display time
                    if ($not_include_break_time_when_display_planned_time == true) {
                        $carbon_paid_rest_time_end->addMinutes($schedule_break_time);
                    }
                    $real_start_work_time = $carbon_paid_rest_time_end->format('Y-m-d H:i:s');
                }
            }

            if ($planned_start_work_time !== null) {
                // $carbon_planned_start_work_time
                $real_start_work_time = $carbon_planned_start_work_time->max($carbon_timestamped_start_work_time)->format('Y-m-d H:i:s');

            } elseif ($schedule_working_hour !== null) {
                if ($total_planned_early_arrive_time_in_minutes > 0) {
                    // $carbon_planned_early_arrive_start
                    $real_start_work_time = $carbon_planned_early_arrive_start->max($carbon_timestamped_start_work_time)->format('Y-m-d H:i:s');

                } else if ($carbon_estimated_latest_start_work_time !== null) {
                    $real_start_work_time = $carbon_estimated_latest_start_work_time->max($carbon_timestamped_start_work_time)->format('Y-m-d H:i:s');
                } else {
                    $real_start_work_time = $carbon_timestamped_start_work_time->format('Y-m-d H:i:s');
                }

            }
        }

        $carbon_real_start_work_time = $working_information_with_data->getCarbonInstance($real_start_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealEndWorkTime
        $real_end_work_time = null;

        if ($timestamped_end_work_time !== null && !$is_planned_work_status_kekkin_or_furikyuu && !$take_a_whole_day_off) {
            // $carbon_timestamped_end_work_time

            if ($paid_rest_time_start !== null && $paid_rest_time_end !== null) {
                // $carbon_paid_rest_time_start
                // $carbon_paid_rest_time_end

                if ($carbon_paid_rest_time_start->lte($carbon_timestamped_end_work_time) && $carbon_timestamped_end_work_time->lte($carbon_paid_rest_time_end)) {

                    // Update 2018-12-25 in the case of not_include_break_time... is on, we have to remove the schedule_breal_time form display time
                    if ($not_include_break_time_when_display_planned_time == true) {
                        $carbon_paid_rest_time_start->subMinutes($schedule_break_time);
                    }
                    $real_end_work_time = $carbon_paid_rest_time_start->format('Y-m-d H:i:s');
                }
            }

            if ($planned_end_work_time !== null) {
                // $carbon_planned_end_work_time
                $real_end_work_time = $carbon_planned_end_work_time->min($carbon_timestamped_end_work_time)->format('Y-m-d H:i:s');

            } elseif ($schedule_working_hour !== null) {

                if ($total_planned_overtime_in_minutes > 0) {
                    // $carbon_planned_overtime_end
                    $real_end_work_time = $carbon_planned_overtime_end->min($carbon_timestamped_end_work_time)->format('Y-m-d H:i:s');

                } elseif ($carbon_estimated_end_work_time !== null) {
                    $real_end_work_time = $carbon_timestamped_end_work_time->min($carbon_estimated_end_work_time)->format('Y-m-d H:i:s');
                } else {
                    $real_end_work_time = $carbon_timestamped_end_work_time->format('Y-m-d H:i:s');
                }

            }
        }

        $carbon_real_end_work_time = $working_information_with_data->getCarbonInstance($real_end_work_time);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealEarlyArriveStart - did declare the carbon instance
        $real_early_arrive_start = null;

        // $carbon_planned_early_arrive_start
        // $carbon_planned_early_arrive_end
        // $carbon_real_start_work_time
        if ($carbon_planned_early_arrive_start !== null && $carbon_planned_early_arrive_end !== null && $carbon_real_start_work_time !== null) {

            if ($carbon_real_start_work_time->lt($carbon_planned_early_arrive_start)) {
                $real_early_arrive_start = $carbon_planned_early_arrive_start->format('Y-m-d H:i:s');
            } elseif ($carbon_planned_early_arrive_start->lte($carbon_real_start_work_time) && $carbon_real_start_work_time->lt($carbon_planned_early_arrive_end)) {
                $real_early_arrive_start = $carbon_real_start_work_time->format('Y-m-d H:i:s');
            }
        }

        $carbon_real_early_arrive_start = $working_information_with_data->getCarbonInstance($real_early_arrive_start);
        ///////////////////////////////////////////////////////////////////////////////////////////



        //** RealEarlyArriveEnd - did declare the carbon instance
        $real_early_arrive_end = null;

        // $carbon_planned_early_arrive_end
        // $carbon_real_start_work_time
        if ($carbon_planned_early_arrive_end !== null && $carbon_real_start_work_time !== null) {
            if ($carbon_real_start_work_time->lt($carbon_planned_early_arrive_end)) {
                $real_early_arrive_end = $carbon_planned_early_arrive_end->format('Y-m-d H:i:s');
            }
        }

        $carbon_real_early_arrive_end = $working_information_with_data->getCarbonInstance($real_early_arrive_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalRealEarlyArriveTimeInMinutes - no need to cach this
        $total_real_early_arrive_time_in_minutes = 0;

        if ($carbon_real_early_arrive_start !== null && $carbon_real_early_arrive_end !== null) {
            $total_real_early_arrive_time_in_minutes = $carbon_real_early_arrive_end->diffInMinutes($carbon_real_early_arrive_start);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealOvertimeStart - did declare the carbon instance
        $real_overtime_start = null;

        // $carbon_planned_overtime_start
        // $carbon_real_end_work_time

        if ($carbon_planned_overtime_start !== null && $carbon_planned_overtime_end !== null && $carbon_real_start_work_time !== null && $carbon_real_end_work_time !== null) {
            if ($carbon_planned_overtime_start->lt($carbon_real_end_work_time)) {
                $real_overtime_start = $carbon_planned_overtime_start->max($carbon_real_start_work_time)->format('Y-m-d H:i:s');
            }
        }

        $carbon_real_overtime_start = $working_information_with_data->getCarbonInstance($real_overtime_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealOvertimeEnd - did declare the carbon instance
        $real_overtime_end = null;

        // $carbon_planned_overtime_start
        // $carbon_planned_overtime_end
        // $carbon_real_end_work_time
        if ($carbon_planned_overtime_start !== null && $carbon_planned_overtime_end !== null && $carbon_real_end_work_time !== null) {
            if ($carbon_real_end_work_time->lte($carbon_planned_overtime_start)) {
                $real_overtime_end = null;
            } elseif ($carbon_planned_overtime_start->lt($carbon_real_end_work_time) && $carbon_real_end_work_time->lte($carbon_planned_overtime_end)) {
                $real_overtime_end = $carbon_real_end_work_time->format('Y-m-d H:i:s');
            } else {
                $real_overtime_end = $carbon_planned_overtime_end->format('Y-m-d H:i:s');
            }
        }

        $carbon_real_overtime_end = $working_information_with_data->getCarbonInstance($real_overtime_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** TotalRealOvertimeInMinutes - no need to cach this
        $total_real_overtime_in_minutes = 0;

        // $carbon_real_overtime_start
        // $carbon_real_overtime_end
        if ($carbon_real_overtime_start !== null && $carbon_real_overtime_end !== null) {
            $total_real_overtime_in_minutes = $carbon_real_overtime_end->diffInMinutes($carbon_real_overtime_start);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkSpanStart - did declare the carbon instance
        $real_work_span_start = null;

        if ($real_start_work_time !== null && $planned_work_span_start !== null) {
            // $carbon_real_start_work_time
            // $carbon_planned_work_span_start

            $real_work_span_start = $carbon_planned_work_span_start->max($carbon_real_start_work_time)->format('Y-m-d H:i:s');
        }

        $carbon_real_work_span_start = $working_information_with_data->getCarbonInstance($real_work_span_start);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkSpanEnd - did declare the carbon instance
        $real_work_span_end = null;

        if ($real_end_work_time !== null && $planned_work_span_end !== null) {
            // $carbon_real_end_work_time
            // $carbon_planned_work_span_end

            $real_work_span_end = $carbon_real_end_work_time->min($carbon_planned_work_span_end)->format('Y-m-d H:i:s');
        }

        $carbon_real_work_span_end = $working_information_with_data->getCarbonInstance($real_work_span_end);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkSpan - did declare the carbon instance
        $real_work_span = null;

        // $carbon_real_work_span_start
        // $carbon_real_work_span_end
        // $carbon_real_start_work_time
        // $carbon_real_end_work_time
        // $carbon_planned_work_span
        if ($carbon_real_work_span_start !== null && $carbon_real_work_span_end !== null) {
            $total_work_time_in_minutes = $carbon_real_work_span_start->diffInMinutes($carbon_real_work_span_end, false) - $real_break_time;
            // Remove the real overtime-due-to-reduce-break-time if it's bigger than 0
            $total_work_time_in_minutes =   ($real_break_time !== null &&
                                            $schedule_break_time !== null &&
                                            $real_break_time < $schedule_break_time) ?
                                            $total_work_time_in_minutes - ($schedule_break_time - $real_break_time) :
                                            $total_work_time_in_minutes;

            if ($carbon_paid_rest_time_start !== null && $carbon_paid_rest_time_end !== null) {
                if ($carbon_real_start_work_time->lte($carbon_paid_rest_time_start) && $carbon_paid_rest_time_end->lte($carbon_real_end_work_time)) {
                    $total_work_time_in_minutes -= $total_paid_rest_time_in_minutes;

                // Update 2018-12-25, In the particular branch of this flow, if the not_include_break_time... is on,
                // we have to add the schedule_break_time back to the total_working_hour
                } else if ($not_include_break_time_when_display_planned_time == true) {
                    $total_work_time_in_minutes += $schedule_break_time ? $schedule_break_time : 0;
                }
            }
            $real_work_span = $working_information_with_data->minutesToString($total_work_time_in_minutes);

        } else if ($carbon_real_start_work_time !== null && $carbon_real_end_work_time !== null && $carbon_planned_work_span !== null) {
            $total_work_time_in_minutes = $carbon_real_start_work_time->diffInMinutes($carbon_real_end_work_time, false) - $real_break_time;
            $total_work_time_in_minutes -= ($total_real_early_arrive_time_in_minutes + $total_real_overtime_in_minutes);
            // Calculate the paid rest time
            // $carbon_paid_rest_time_start
            // $carbon_paid_rest_time_end

            // Remove the real overtime-due-to-reduce-break-time if it's bigger than 0
            $total_work_time_in_minutes =   ($real_break_time !== null &&
                                            $schedule_break_time !== null &&
                                            $real_break_time < $schedule_break_time) ?
                                            $total_work_time_in_minutes - ($schedule_break_time - $real_break_time) :
                                            $total_work_time_in_minutes;

            if ($carbon_paid_rest_time_start !== null && $carbon_paid_rest_time_end !== null) {
                if ($carbon_real_start_work_time->lte($carbon_paid_rest_time_start) && $carbon_paid_rest_time_end->lte($carbon_real_end_work_time)) {
                    $total_work_time_in_minutes -= $total_paid_rest_time_in_minutes;
                }
            } else if ($paid_rest_time_period_in_minutes > 0) {
                if ($not_include_break_time_when_display_planned_time == true) {
                    $total_work_time_in_minutes += $schedule_break_time ? $schedule_break_time : 0;
                }
            }

            // Arcoding to new spec, the realWorkSpan are to be smaller than or equal to planned_work_span
            $carbon_total_work_time = $working_information_with_data->getCarbonInstance($working_information_with_data->minutesToString($total_work_time_in_minutes));
            $real_work_span = $carbon_planned_work_span->min($carbon_total_work_time)->format('H:i:s');
        }

        $carbon_real_work_span = $working_information_with_data->getCarbonInstance($real_work_span);
        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealWorkingHour
        $real_working_hour = null;
        $total_real_work_span_in_minutes = null;
        if ($real_start_work_time !== null && $real_end_work_time !== null && $real_work_span !== null) {
            $total_real_work_span_in_minutes = $working_information_with_data->convertToMinutes($carbon_real_work_span);

            $total_real_work_span_in_minutes += $total_real_early_arrive_time_in_minutes + $total_real_overtime_in_minutes;

            if ($is_planned_rest_status_goukyuu_or_zenkyuu != true) {
                $total_real_work_span_in_minutes += $real_overtime_due_to_reduce_break_time;
            }

            $real_working_hour = $working_information_with_data->minutesToString($total_real_work_span_in_minutes);

        } else if ($is_planned_work_status_houde_or_kyuude_or_zangyou) {
            // $carbon_real_start_work_time
            // $carbon_real_end_work_time

            if ($carbon_real_start_work_time !== null && $carbon_real_end_work_time !== null) {
                $break_time = $real_break_time !== null ? $real_break_time : 0;
                $total_working_hour_in_minutes = $carbon_real_start_work_time->diffInMinutes($carbon_real_end_work_time) - $break_time;

                $real_working_hour = $working_information_with_data->minutesToString($total_working_hour_in_minutes);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealEarlyLeaveTime
        $real_early_leave_time = null;

        if ($schedule_end_work_time !== null && $real_end_work_time !== null) {
            // $carbon_schedule_end_work_time
            // $carbon_real_end_work_time

            if ($carbon_real_end_work_time->lt($carbon_schedule_end_work_time)) {
                $early_leave = $carbon_schedule_end_work_time->diffInMinutes($carbon_real_end_work_time);

                if ($paid_rest_time_start !== null && $paid_rest_time_end !== null) {
                    // $carbon_paid_rest_time_start
                    // $carbon_paid_rest_time_end

                    if ($carbon_real_end_work_time->lte($carbon_paid_rest_time_start)) {
                        $early_leave -= $total_paid_rest_time_in_minutes;

                        // Update 2018-12-25 in the case of not_include_break_time... AND the real_end_work_time is BEFORE the paid_rest_start,
                        // we have to subtract the schedule_break_time from the early_leave_time
                        if ($not_include_break_time_when_display_planned_time == true && $carbon_real_end_work_time->lt($carbon_paid_rest_time_start)) {
                            $early_leave -= ($schedule_break_time) ? $schedule_break_time : 0;
                        }

                    // This flow is just for show apparently
                    } elseif ($carbon_real_end_work_time->lt($carbon_paid_rest_time_end)) {
                        $diff = $carbon_paid_rest_time_end->diffInMinutes($carbon_real_end_work_time);
                        $early_leave -= $diff;
                    }

                } elseif ($is_planned_rest_status_goukyuu) {
                    $early_leave = $early_leave - $total_paid_rest_time_in_minutes - $schedule_break_time;
                }
                $real_early_leave_time = $early_leave;
            }
        } elseif ($real_end_work_time !== null && $carbon_estimated_end_work_time !== null) {
            // In WorkingHourOnly mode, when there is planned overtime, we have to use the planned_overtime_start instead of the estimated_end_work_time
            $carbon_estimated_official_end_work_time = ($planned_overtime_start !== null && $planned_overtime_end !== null) ? $carbon_planned_overtime_start : $carbon_estimated_end_work_time;

            if ($carbon_real_end_work_time->lt($carbon_estimated_official_end_work_time)) {
                $real_early_leave_time = ($planned_early_leave_time !== null) ? $planned_early_leave_time + $carbon_real_end_work_time->diffInMinutes($carbon_estimated_official_end_work_time) :
                                                                                $carbon_real_end_work_time->diffInMinutes($carbon_estimated_official_end_work_time);
            } else if ($planned_early_leave_time !== null) {
                $real_early_leave_time = $planned_early_leave_time;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////


        //** RealLateTime
        $real_late_time = null;

        if ($real_start_work_time !== null && $schedule_start_work_time !== null) {
            // $carbon_real_start_work_time
            // $carbon_schedule_start_work_time

            if ($carbon_real_start_work_time->gt($carbon_schedule_start_work_time)) {

                $late_time = $carbon_real_start_work_time->diffInMinutes($carbon_schedule_start_work_time);

                if ($paid_rest_time_start !== null && $paid_rest_time_end !== null) {
                    // $carbon_paid_rest_time_start
                    // $carbon_paid_rest_time_end

                    if ($carbon_paid_rest_time_end->lte($carbon_real_start_work_time)) {
                        $late_time -= $total_paid_rest_time_in_minutes;

                        // Update 2018-12-25 in the case of not_include_break_time... AND the real_end_work_time is BEFORE the paid_rest_start,
                        // we have to subtract the schedule_break_time from the late_time
                        if ($not_include_break_time_when_display_planned_time == true && $carbon_paid_rest_time_end->lt($carbon_real_start_work_time)) {
                            $late_time -= ($schedule_break_time) ? $schedule_break_time : 0;
                        }

                    // This flow is just for show apparently
                    } elseif ($carbon_paid_rest_time_start->lt($carbon_real_start_work_time)) {
                        $diff = $carbon_real_start_work_time->diffInMinutes($carbon_paid_rest_time_start);
                        $late_time -= $diff;
                    }
                } elseif ($is_planned_rest_status_zenkyuu) {
                    $late_time = $late_time - $total_paid_rest_time_in_minutes - $schedule_break_time;
                }
                $real_late_time = $late_time;
            }
        } elseif ($real_start_work_time !== null && $real_work_span !== null && $carbon_estimated_latest_start_work_time !== null) {
            if ($carbon_estimated_latest_start_work_time->lt($carbon_real_start_work_time)) {
                $real_late_time = $carbon_estimated_latest_start_work_time->diffInMinutes($carbon_real_start_work_time);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        return $total_real_work_span_in_minutes;

    }
    
}

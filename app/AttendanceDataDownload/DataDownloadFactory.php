<?php

namespace App\AttendanceDataDownload;

use App\Company;
use App\WorkLocation;
use App\Employee;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use App\PaidHolidayInformation;
use App\Services\TodofukenService;
use App\Services\NationalHolidayService;
use App\Services\ManagerAndEmployeeService;
use Carbon\Carbon;
use Constants;
use InvalidArgumentException;
use DateTime;

class DataDownloadFactory
{
    /**
     * Attendace Data go by employee's order then date's order.
     */
    public $attendance_data_by_employee_date;

    /**
     * Attendance Data go by date's order, then employee's order.
     */
    public $attendance_data_by_date_employee;

    /**
     * List of general information of all WorkLocations.
     */
    public $work_location_information;

    /**
     * Name list of all WorkLocations. Key by id.
     */
    private $work_location_names;

    /**
     * Presentation id list of all WorkLocations. Key by id.
     */
    private $work_location_presentation_ids;

    /**
     * List of all departments, Key by id.
     */
    private $department_names;

    /**
     * List of all work statuses(in the item setting page). Key by id.
     */
    protected $company_work_statuses;

    /**
     * List of all rest statuses(in the item setting page). Key by id.
     */
    protected $company_rest_statuses;

    /**
     * List of week day strings
     */
    protected $week_day_strings;

    /**
     * The search condition to initialize the data for the downloading
     */
    protected $conditions;

    /**
     * The cache of all work locations's settings and calendar rest days
     */
    protected $work_location_settings;

    /**
     * List of genders. Base on the config of the application.
     */
    private $genders;

    /**
     * List of all schedule types. Base on the config of the application.
     */
    private $schedule_types;

    /**
     * List of all employment types. Base on the config of the application.
     */
    private $employment_types;

    /**
     * List of all salary types. Base on the config of the application.
     */
    private $salary_types;

    /**
     * List of all work statuses. (these work statuses are in employee's setting page. They are different with company's work statuses).
     */
    private $employee_work_statuses;

    /**
     * List of all holiday bonus types.
     */
    private $holiday_bonus_types;

    /**
     * List of all todofuken. Key by id.
     */
    private $todofuken_list;

    /**
     * National holiday service
     */
    private $national_holiday_service;

    //////////////////////////////////////////
    //// Utility functions to be exposed /////

    /**
     * Transform day, time string into timestamp
     *
     * @param string        $date_string        format Y-m-d
     * @param string        $hour_string        format H:i
     * @return int|false
     */
    public function fn_ut_hhmm_to_stamp($date_string, $hour_string)
    {
        try {
            $carbon = Carbon::createFromFormat('Y-m-d H:i', $date_string . ' ' . $hour_string);
        } catch (InvalidArgumentException $e) {
            return false;
        }

        return $carbon->timestamp;
    }

    /**
     * Transform timestamp to time string(hh:mm)
     *
     * @param int       $timestamp
     * @return string|false
     */
    public function fn_ut_stamp_to_hhmm($timestamp)
    {
        try {
            $carbon = Carbon::createFromTimestamp($timestamp);
        } catch (InvalidArgumentException $e) {
            return false;
        }

        return $carbon->format('H:i');
    }

    /**
     * Transform a number of minutes to time string(hh:mm)
     *
     * @param int       $minutes
     * @return string|false
     */
    public function fn_ut_minute_to_hhmm($minutes)
    {
        if (is_numeric($minutes) && $minutes >= 0) {
            return str_pad(floor($minutes/60), 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes%60, 2, '0', STR_PAD_LEFT);
        } else {
            return false;
        }
    }

    /**
     * Calculate the time between timband_start and timeband_end WITHIN the work_start and work_end.
     *
     * @param  string       $date
     * @param  string       $work_start
     * @param  string       $work_end
     * @param  string       $timeband_start
     * @param  string       $timeband_end
     * @return integer      number of minutes
     */
    public function fn_ut_between_time($date, $work_start, $work_end, $timeband_start, $timeband_end)
    {
        $work_start_carbon = new Carbon($date . ' ' . $work_start);
        $work_end_carbon = new Carbon($date .  ' ' . $work_end);
        if ($work_end_carbon->lt($work_start_carbon)) {
            $work_end_carbon->addDay();
        }

        $timeband_start_carbon = new Carbon($date . ' ' . $timeband_start);
        $timeband_end_carbon = new Carbon($date . ' ' . $timeband_end);
        if ($timeband_end_carbon->lt($timeband_start_carbon)) {
            $timeband_end_carbon->addDay();
        }

        if ($work_start_carbon->lt($timeband_end_carbon) && $work_end_carbon->gt($timeband_start_carbon)) {
            $start_milestone = ($work_start_carbon->gt($timeband_start_carbon)) ? $work_start_carbon : $timeband_start_carbon;
            $end_milestone = ($work_end_carbon->gt($timeband_end_carbon)) ? $timeband_end_carbon : $work_end_carbon;

            return $start_milestone->diffInMinutes($end_milestone);
        } else {
            return 0;
        }
    }

    /**
     * Calculate the time between timband_start and timeband_end WITHIN the work_start and work_end. But use timestamp instead.
     *
     * @param integer   $work_start_timestamp
     * @param integer   $work_end_timestamp
     * @param integer   $timeband_start_timestamp
     * @param integer   $timeband_end_timestamp
     * @return integer  number of minutes
     */
    public function fn_ut_between_time_bystamp($work_start_timestamp, $work_end_timestamp, $timeband_start_timestamp, $timeband_end_timestamp)
    {
        $work_start_carbon = Carbon::createFromTimestamp($work_start_timestamp);
        $work_end_carbon = Carbon::createFromTimestamp($work_end_timestamp);
        $timeband_start_carbon = Carbon::createFromTimestamp($timeband_start_timestamp);
        $timeband_end_carbon = Carbon::createFromTimestamp($timeband_end_timestamp);

        if ($work_end_carbon->gte($work_start_carbon) && $timeband_end_carbon->gte($timeband_start_carbon) && $work_start_carbon->lt($timeband_end_carbon) && $work_end_carbon->gt($timeband_start_carbon)) {
            $start_milestone = ($work_start_carbon->gt($timeband_start_carbon)) ? $work_start_carbon : $timeband_start_carbon;
            $end_milestone = ($work_end_carbon->gt($timeband_end_carbon)) ? $timeband_end_carbon : $work_end_carbon;

            return $start_milestone->diffInMinutes($end_milestone);
        } else {
            return 0;
        }
    }

    /**
     * A function to calculate the paid rest days of a given employee up until a specific given date.
     *
     * @param       string      $employee_presentation_id
     * @param       string      $date (Y-m-d)
     * @return      array       Has two keys: 'days' -> number of paid rest days
     *                                         'time' -> paid rest time in minutes
     */
    public function calculatePaidHolidayOfEmployee($employee_presentation_id, $date = null)
    {
        $employee_service = resolve(ManagerAndEmployeeService::class);
        $employee = $employee_service->getEmployeeByPresentationId($employee_presentation_id);

        // In this case, get the current available PaidHoliday, in other word, get the
        if ($date == null) {
            $latest_paid_holiday_info = $employee->paidHolidayInformations->sortBy('period_end')->last();
            return [
                'days' => $latest_paid_holiday_info->available_paid_holidays,
                'time' => $latest_paid_holiday_info->available_paid_holidays_hour,
            ];

        } else {
            $suitable_paid_holiday_info = PaidHolidayInformation::where('employee_id', $employee->id)
                ->where('period_start', '<=', $date)->where('period_end', '>=', $date)->first();

            if ($suitable_paid_holiday_info) {

                return $suitable_paid_holiday_info->availablePaidHolidaysAtSpecificDate($date);

            // In the case of suitable PaidHolidayInformation does not exist, everything will be zero
            } else {
                return [
                    'days' => 0,
                    'time' => 0,
                ];
            }
        }
    }

    //// End of utility functions to be exposed /////
    /////////////////////////////////////////////////

    /**
     * Initialize all the necessary variables.
     *
     * @param string        $company_code
     * @return void
     */
    public function __construct($company_code)
    {
        $company = Company::where('code', $company_code)->first();

        $this->department_names = $company->departments->pluck('name', 'id');

        $this->company_work_statuses = $company->workStatuses->pluck('name', 'id');

        $this->company_rest_statuses = $company->restStatuses->keyBy('id')->toArray();

        $this->genders = Constants::genders();

        $this->schedule_types = Constants::scheduleTypes();

        $this->employment_types = Constants::employmentTypes();

        $this->salary_types = Constants::salaryTypes();

        $this->employee_work_statuses = Constants::workStatuses();

        $this->holiday_bonus_types = Constants::holidayBonusTypes();

        $todofuken_service = resolve(TodofukenService::class);
        $this->todofuken_list = $todofuken_service->getNames();

        // Initiate the week day strings
        $this->week_day_strings = [
            0 => '日',
            1 => '月',
            2 => '火',
            3 => '水',
            4 => '木',
            5 => '金',
            6 => '土',
        ];

        // Load services
        $this->national_holiday_service = resolve(NationalHolidayService::class);

        $work_locations = WorkLocation::with([
            'company.setting',
            'setting',
            'calendarTotalWorkTimes',
            'defaultCalendarTotalWorkTimes',
        ])->get();

        $this->work_location_names = $work_locations->pluck('name', 'id');

        $this->work_location_presentation_ids = $work_locations->pluck('presentation_id', 'id');

        // Load setting and calendar settings of all WorkLocations
        $this->work_location_settings = $work_locations->keyBy('presentation_id')->map(function($work_location) {
            $setting = collect($work_location->currentSetting()->toArray());
            $result = [
                'setting' => $setting->only([
                    'salary_accounting_day',
                    'pay_month',
                    'pay_day',
                    'start_day_of_week',
                    'law_rest_day_mode',
                    'start_time_round_up',
                    'end_time_round_down',
                    'break_time_round_up',
                    'start_time_diff_limit',
                    'end_time_diff_limit',
                    'go_out_button_usage',
                    'display_go_out_time',
                    'use_overtime_button',
                ]),
            ];
            $result['setting']['timezone'] = $work_location->getTimezone()->name_id;
            $result['calendar']['work_time_by_months'] = $work_location->getTotalWorkTimes();
            $result['calendar']['rest_days'] = $work_location->getRestDays();

            return $result;
        })->toArray();

        // Load general information of all WorkLocations
        $this->work_location_information = $work_locations->keyBy('presentation_id')->map(function($work_location) {
            $data = collect($work_location->toArray());
            $data = $data->only([
                "presentation_id",
                "registration_number",
                "name",
                "furigana",
                "enable",
                "postal_code",
                "todofuken",
                "address",
                "login_range",
                "latitude",
                "longitude",
                "telephone",
                "chief_first_name",
                "chief_first_name_furigana",
                "chief_last_name",
                "chief_last_name_furigana",
                "chief_email",
            ]);

            $data['todofuken'] = $data['todofuken'] ? $this->todofuken_list[$data['todofuken']] : null;
            return $data;
        })->toArray();
    }

    /**
     * Prepare the Summmary Data.
     * Have an array of Employees, and in each employee, prepare data of all days from start_date to end_date.
     *
     * @param array     $conditions
     * @param array     $work_location_ids
     * @param boolean   $load_seven_day_prior_to_start_date
     * @return void     all the data will be put in the property 'attendance_data_by_employee_date'
     */
    public function prepareDataByEmployeeDate($conditions, $work_location_ids, $load_seven_day_prior_to_start_date = false)
    {
        $this->conditions = $conditions;

        $query = Employee::with('workLocation')->whereIn('work_location_id', $work_location_ids)->where('joined_date', '<=', $conditions['end_date'])->where(function($query) use ($conditions) {
            return $query->whereNull('resigned_date')->orWhere('resigned_date', '>=', $conditions['start_date']);
        });

        if (isset($conditions['presentation_id'])) {
            $query = $query->where('presentation_id', $conditions['presentation_id']);
        }

        if (isset($conditions['name'])) {
            $query = $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions['name'] . '%');
        }

        if (isset($conditions['work_status'])) {
            $query = $query->where('work_status', $conditions['work_status']);
        }

        $employees = $query->orderBy('work_location_id')->orderBy('view_order')->get();

        $start_date_to_get_attendance_data = new Carbon($conditions['start_date']);
        $end_date_to_get_attendance_data = new Carbon($conditions['end_date']);

        // Get the data from 7 days before per request of the support side.
        if ($load_seven_day_prior_to_start_date) {
            $start_date_to_get_attendance_data->subDays(7);
        }

        $employee_working_days = EmployeeWorkingDay::with([
            'employee.workLocation',
            'workingTimestamps',
            'concludedEmployeeWorkingDay',
            'substituteEmployeeWorkingInformations',
            'employeeWorkingInformationSnapshots',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            'employeeWorkingInformations.colorStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.setting',
            'employeeWorkingInformations.currentPlannedWorkLocation.unusedWorkStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.unusedRestStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.company.setting',
            'employeeWorkingInformations.currentPlannedWorkLocation.company.workStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.company.restStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.setting',
            'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.setting',
            'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
            'employeeWorkingInformations.employeeWorkingInformationSnapshot'
        ])->where('date', '>=', $start_date_to_get_attendance_data->toDateString())->where('date', '<=', $end_date_to_get_attendance_data->toDateString())->whereIn('employee_id', $employees->pluck('id')->toArray())->get();

        // Turn into hash table
        $employee_working_days = $employee_working_days->keyBy(function($working_day) {
            return $working_day->date . '_' . $working_day->employee_id;
        });

        $data = [];

        foreach ($employees as $employee) {
            $data[$employee->presentation_id] = $this->employeeOutputData($employee);

            $working_data_by_date = [];
            $carbon_pivot = $start_date_to_get_attendance_data->copy();
            while ($carbon_pivot->lte($end_date_to_get_attendance_data)) {
                $day_string = $carbon_pivot->format('Y-m-d');
                $unique_key = $day_string . '_' . $employee->id;

                if (isset($employee_working_days[$unique_key])) {
                    $working_data_by_date[$day_string] = $this->employeeWorkingDayOutputData($employee_working_days[$unique_key]);

                } else {
                    $working_data_by_date[$day_string] = $this->emptyEmployeeWorkingDayData($day_string, $employee->workLocation->presentation_id);
                }

                $carbon_pivot->addDay();
            }

            $data[$employee->presentation_id]['employee_working_days'] = $working_data_by_date;
        }

        $this->attendance_data_by_employee_date = $data;
    }

    /**
     * Download the Summary Data
     *
     * @param array     $conditions
     * @param array     $work_location_ids
     * @return array
     */
    public function downloadSummaryData($conditions, $work_location_ids)
    {
        // 集計データ処理の方は、「$attendance_data_by_employee_date」変数がデフォルトとして使われています。
        $this->prepareDataByEmployeeDate($conditions, $work_location_ids);

        // もし、「$attendance_data_by_date_employee」変数を使いたいなら、上の行をコメントアウトして、下の行をコメント外てください。
        // $this->prepareDataByDateEmployee($conditions, $work_location_ids);

        // 上の二つの関数は三つ目のパラメータを受けれます。そのパラメータは「boolean」型で、「前の７日のデータを読み込むかどうか」ということを決めます。「true」は読み込む、
        // 逆、「false」は読み込まないということです。必須ではありません。デフォルト値は「false」で、ゆえに「読み込まない」です。

        $result = $this->processSummary();

        // 前の７日を読み込むの場合で、その７日のデータを最後のデータからフィルターアウトしたいなら、下の関数を使ってくだいさい。
        // $result = $this->filterOutSevenDaysBeforeStartDay($result, $conditions);

        return $result;
    }

    /**
     * Process the Summary data. Depend on each company's need, this function can be overridden and customized.
     *
     * @return array
     */
    public function processSummary()
    {
        // Declare and set the header
        $result = [
            [
                '所属先', 'ID', '名前', '部署', '採用形態', '出勤日数', '欠勤日数', '有給日数', '時間有給', '遅・早時間', '遅・早日数', '総実績総労働時間', '総所定労働時間', '総実績総残業時間', '法定外残業時間（日）', '法定外残業時間（週）', '法定外残業時間（月）', '法定内残業時間・計', '法定外残業時間・計', '総深夜労働時間', '総法定休日労働時間'
            ],
        ];


        foreach ($this->attendance_data_by_employee_date as $employee) {

            // 「$week_kisanbi == 起算日から7日後の曜日」 (例)起算日が月曜であれば"日"が入ります
            // 「$week_kisanbi == 起算日当日」に変更したい場合は、$weekの配列を[0=>"日", 1=>"月", 2=>"火", 3=>"水", 4=>"木", 5=>"金", 6=>"土"]に差し替えてください。
            // 起算日を使用しない場合は不要のため、削除してください。
            $workarea_id = $employee['work_location_presentation_id'];
            $week_kisanbi_no = $this->work_location_settings[$workarea_id]['setting']['start_day_of_week'];
            $week = [0=>"土", 1=>"日", 2=>"月", 3=>"火", 4=>"水", 5=>"木", 6=>"金"];
            foreach($week as $key=>$value) {
                if ($week_kisanbi_no == $key) {
                    $week_kisanbi = $value;
                }
            }

// ↓↓従業員毎に初期化する必要のある変数を設定 ↓↓
            $hoteinai_week_overworktime_m = 0; //法定内休日(週)
            // ↓↓週起算日集計用変数↓↓
            $sum_shotei_worktime_m = 0; //所定労働時間
            $sum_realworktime_m = 0; //実績総労働時間
            $sum_hoteinai_day_overworktime_m = 0; //法定内残業時間(日)
            $sum_hoteigai_day_overworktime_m = 0; //法定外残業時間(日)
            $sum_hotei_restday_worktime_m = 0; //法定休日労働時間
            // ↑↑週起算日集計用変数↑↑
            $_s_branchname = 0; //所属先
            $_s_branchid = 0; //ID
            $_s_staffname = 0; //名前
            $_s_department = 0; //部署
            $_s_employtype = 0; //採用形態
            $_s_workdays = 0; //出勤日数
            $_s_absentdays = 0; //欠勤日数
            $_s_gainfuldays = 0; //有休日数
            $_s_gainfultime = 0; //時間有休
            $_s_ackurecnttime = 0; //遅・早時間
            $_s_ackurecntdays = 0; //遅・早日数
            $_s_real_worktime = 0; //総実績総労働時間
            $_s_shotei_worktime = 0; //総所定労働時間
            $_s_real_overworktime = 0; //総実績総残業時間
            $_s_hoteigai_day_overworktime = 0; //法定外残業時間（日）
            $_s_hoteigai_week_overworktime = 0; //法定外残業時間（週）
            $_s_hoteigai_month_overworktime = 0; //法定外残業時間（月）
            $_s_hoteinai_total_overworktime = 0; //法定内残業時間・計
            $_s_hoteigai_total_overworktime = 0; //法定外残業時間・計
            $_s_night_worktime = 0; //総深夜労働時間
            $_s_hotei_restday_worktime = 0; //総法定休日労働時間
            $hoteigai_week_overworktime_m = 0; //法定外残業時間（週）計算用
            $sum_hoteinai_week_overworktime_m = 0; //法定内残業時間（週）の合計 計算用

            // 勤務日を1日毎にループ
            foreach ($employee['employee_working_days'] as $working_day) {

                // 1日毎に初期化する変数
                $day_work_count = 1; //同一日勤務回数カウント
                $absentday_count = 0; //欠勤回数カウント
                $ackurecntday_count = 0; //遅・早回数カウント
                // 集計開始日に変数初期化(前月の7日間を読み込んでいる状態のため)
                if ($working_day['date'] == $_POST['start_date']) {
                    $_s_branchname = 0; //所属先
                    $_s_branchid = 0; //ID
                    $_s_staffname = 0; //名前
                    $_s_department = 0; //部署
                    $_s_employtype = 0; //採用形態
                    $_s_workdays = 0; //出勤日数
                    $_s_absentdays = 0; //欠勤日数
                    $_s_gainfuldays = 0; //有休日数
                    $_s_gainfultime = 0; //時間有休
                    $_s_ackurecnttime = 0; //遅・早時間
                    $_s_ackurecntdays = 0; //遅・早日数
                    $_s_real_worktime = 0; //総実績総労働時間
                    $_s_shotei_worktime = 0; //総所定労働時間
                    $_s_real_overworktime = 0; //総実績総残業時間
                    $_s_hoteigai_day_overworktime = 0; //法定外残業時間（日）
                    $_s_hoteigai_week_overworktime = 0; //法定外残業時間（週）
                    $_s_hoteigai_month_overworktime = 0; //法定外残業時間（月）
                    $_s_hoteinai_total_overworktime = 0; //法定内残業時間・計
                    $_s_hoteigai_total_overworktime = 0; //法定外残業時間・計
                    $_s_night_worktime = 0; //総深夜労働時間
                    $_s_hotei_restday_worktime = 0; //総法定休日労働時間
                    $sum_hoteinai_week_overworktime_m = 0; //法定内残業時間（週）の合計 計算用
                }

                if (count($working_day['employee_working_informations']) > 0) {

                    // 勤務ごとにループ（1日複数勤務の可能性あり）
                    foreach ($working_day['employee_working_informations'] as $working_info) {

// ↓↓ 判定フラグ変数の設定 ↓↓

                        // 法定休日判定(勤務形態が『法出』であれば「1」、そうでなければブランク)
                        $hotei_restday_hantei = ($working_info['planned_work_status'] == "法出") ? 1 : "";

                        // 同日最終勤務判定(同一日最終勤務であれば「1」, そうでなければブランク)
                        if (count($working_day['employee_working_informations']) > 1) {
                            if (count($working_day['employee_working_informations']) == $day_work_count) {
                                $day_lastwork_hantei = 1;
                            } else {
                                $day_lastwork_hantei = "";
                                $day_work_count++;
                            }
                        } else {
                            $day_lastwork_hantei = 1;
                        }

// ↓↓ レコード挿入変数の設定 ↓↓


                        // 日付
                        $_p_date = $working_day['date'];

                        // 曜日
                        $_p_weekday = $working_day['weekday'];

                        // 所属先
                        $_p_branchname = $employee['work_location'];

                        // ID
                        $_p_branchid = $employee['presentation_id'];

                        // 名前
                        $_p_staffname = $employee['last_name'] . $employee['first_name'];

                        // 部署
                        $_p_department = $employee['department'];

                        // 採用形態
                        $_p_employtype = $employee['employment_type'];

                        // 勤務形態
                        $_p_shifttype = $working_info['planned_work_status'];

                        // 休暇形態
                        $_p_resttype = $working_info['planned_rest_status'];

                        // 所定出勤
                        $_p_shotei_atime = (isset($working_info['schedule_start_work_time'])) ? (date("H:i", strtotime($working_info['schedule_start_work_time']))) : "";

                        // 出勤打刻時刻
                        $_p_real_atime = (isset($working_info['timestamped_start_work_time'])) ? (date("H:i", strtotime($working_info['timestamped_start_work_time']))) : "";

                        // 出勤計算時刻
                        $_p_atime_maru = (isset($working_info['real_start_work_time'])) ? (date("H:i", strtotime($working_info['real_start_work_time']))) : "";

                        // 所定退勤
                        $_p_shotei_ctime = (isset($working_info['schedule_end_work_time'])) ? (date("H:i", strtotime($working_info['schedule_end_work_time']))) : "";

                        // 退勤打刻時刻
                        $_p_real_ctime = (isset($working_info['timestamped_end_work_time'])) ? (date("H:i", strtotime($working_info['timestamped_end_work_time']))) : "";

                        // 退勤計算時刻
                        $_p_ctime_maru = (isset($working_info['real_end_work_time'])) ? (date("H:i", strtotime($working_info['real_end_work_time']))) : "";

                        // 遅・早
                        $_p_ackoure = (($working_info['real_late_time'] + $working_info['real_early_leave_time']) > 0) ? ($working_info['real_late_time'] + $working_info['real_early_leave_time']) : "";

                        // 所定休憩
                        $_p_shotei_resttime = ($working_info['schedule_break_time'] > 0) ? $working_info['schedule_break_time'] : "";

                        // 実績休憩
                        $_p_real_resttime = ($working_info['real_break_time'] > 0) ? $working_info['real_break_time'] : "";

                        // 外出時間
                        $_p_real_gtime = ($working_info['real_go_out_time'] > 0) ? $working_info['real_go_out_time'] : "";

                        // 所定労働時間
                        $shotei_worktime_m = $this->fnm_60hexToMinute($working_info['schedule_working_hour']);
                        $_p_shotei_worktime = ($shotei_worktime_m > 0) ? $shotei_worktime_m : "";

                        // 所定労働時間(日)  ※非表示項目
                        $daytotal_shotei_worktime_m = $working_day['schedule_working_time'];

                        // 実績総労働時間
                        $real_worktime_m = $this->fnm_60hexToMinute($working_info['real_working_hour']);
                        $_p_real_worktime = ($real_worktime_m > 0) ? $real_worktime_m : "";

                        // 実績総労働時間(日)  ※非表示項目
                        $daytotal_real_worktime_m = $working_day['real_working_time'];

                        // 実績の総残業時間（早出と残業）
                        $_p_real_overworktime = ($working_info['summary_real_overtime'] > 0) ? $working_info['summary_real_overtime'] : "";

                    // 同日最終勤務の場合、または1日の出勤が1回のみの場合
                            if ($day_lastwork_hantei == 1) {

                        // 法定外残業時間（日）
                                if ($employee['schedule_type'] == "通常") {
                                    $hoteigai_day_overworktime_m = $daytotal_real_worktime_m - 8*60;
                                } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                                    $hoteigai_day_overworktime_m = ($daytotal_shotei_worktime_m > 8*60) ? $daytotal_real_worktime_m - $daytotal_shotei_worktime_m : $daytotal_real_worktime_m - 8*60;
                                } else {
                                    $hoteigai_day_overworktime_m = 0;
                                }
                                $hoteigai_day_overworktime_m = ($hotei_restday_hantei == "" && $hoteigai_day_overworktime_m > 0) ? $hoteigai_day_overworktime_m : 0;
                                $_p_hoteigai_day_overworktime = ($hoteigai_day_overworktime_m > 0) ? $hoteigai_day_overworktime_m : "";

                        // '法定内残業時間（日）'
                                if ($employee['schedule_type'] == "通常") {
                                    $hoteinai_day_overworktime_m = $daytotal_real_worktime_m - $daytotal_shotei_worktime_m - $hoteigai_day_overworktime_m;
                                } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                                    $hoteinai_day_overworktime_m = ($daytotal_shotei_worktime_m < 8*60) ? $daytotal_real_worktime_m - $daytotal_shotei_worktime_m - $hoteigai_day_overworktime_m : 0;
                                } else {
                                    $hoteinai_day_overworktime_m = 0;
                                }
                                $hoteinai_day_overworktime_m = ($hotei_restday_hantei == "" && $hoteinai_day_overworktime_m > 0) ? $hoteinai_day_overworktime_m : 0;
                                $_p_hoteinai_day_overworktime = ($hoteinai_day_overworktime_m > 0) ? $hoteinai_day_overworktime_m : "";

                        // 法定外残業時間（週）
                                // 必要項目の集計
                                $sum_shotei_worktime_m = $sum_shotei_worktime_m + $daytotal_shotei_worktime_m;
                                $sum_realworktime_m = $sum_realworktime_m + $daytotal_real_worktime_m;
                                $sum_hoteigai_day_overworktime_m = $sum_hoteigai_day_overworktime_m + $hoteigai_day_overworktime_m;
                                $sum_hotei_restday_worktime_m = ($hotei_restday_hantei == 1) ? $sum_hotei_restday_worktime_m + $real_worktime_m : $sum_hotei_restday_worktime_m;

                                if ($week_kisanbi == $working_day['weekday']) {
                                // 起算日集計日の場合、
                                    if ($employee['schedule_type'] == "通常") {
                                        $hoteigai_week_overworktime_m = $sum_realworktime_m - 40*60 - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m;
                                    } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                                        $hoteigai_week_overworktime_m = ($sum_shotei_worktime_m <= 40*60 && $sum_realworktime_m > 40) ? $sum_realworktime_m - 40*60 - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m : 0;
                                        $hoteigai_week_overworktime_m = ($sum_shotei_worktime_m > 40*60 && $sum_realworktime_m > 40) ? $sum_realworktime_m - $sum_shotei_worktime_m - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m : $hoteigai_week_overworktime_m;
                                    } else {
                                        $hoteigai_week_overworktime_m = 0;
                                    }
                                    $hoteigai_week_overworktime_m = ($hoteigai_week_overworktime_m > 0) ? $hoteigai_week_overworktime_m : 0;
                                    $_p_hoteigai_week_overworktime = $hoteigai_week_overworktime_m;
                                    // 集計変数初期化
                                    $sum_shotei_worktime_m = 0;
                                    $sum_realworktime_m = 0;
                                    $sum_hoteigai_day_overworktime_m = 0;
                                    $sum_hotei_restday_worktime_m = 0;

                                // 起算日集計日以外の場合、
                                } else {
                                    $_p_hoteigai_week_overworktime = "";
                                }

                        // 法定内残業時間(週)
                                // 起算日集計日
                                if ($week_kisanbi == $working_day['weekday']) {
                                    $sum_hoteinai_day_overworktime_m = $sum_hoteinai_day_overworktime_m + $hoteinai_day_overworktime_m;
                                    $hoteinai_week_overworktime_m = $sum_hoteinai_day_overworktime_m;
                                    $sum_hoteinai_day_overworktime_m = 0;
                                // 起算日集計日以外
                                } else {
                                    $sum_hoteinai_day_overworktime_m = $sum_hoteinai_day_overworktime_m + $hoteinai_day_overworktime_m;
                                    $hoteinai_week_overworktime_m = 0;
                                }
                                // 計算結果がマイナスになれば0にする
                                $hoteinai_week_overworktime_m = ($hoteinai_week_overworktime_m - $hoteigai_week_overworktime_m > 0) ? $hoteinai_week_overworktime_m - $hoteigai_week_overworktime_m : 0;

                    // 同日最終勤務ではない場合(1日に複数出勤があり、最終勤務でない場合)
                            } else {
                                $_p_hoteinai_day_overworktime = "";
                                $_p_hoteigai_day_overworktime = "";
                                $_p_hoteigai_week_overworktime = "";
                                $hoteinai_week_overworktime_m = 0;
                                $sum_hotei_restday_worktime_m = ($hotei_restday_hantei == 1) ? $sum_hotei_restday_worktime_m + $real_worktime_m : $sum_hotei_restday_worktime_m;
                            }

                        // 深夜労働時間
                            $_p_night_worktime = ($working_info['summary_real_night_work_time'] > 0) ? $working_info['summary_real_night_work_time'] : "";

                        // 法定休日労働時間
                            $_p_hotei_restday_worktime = ($hotei_restday_hantei == 1) ? $real_worktime_m : "";

// ↓↓【コピペ対象外箇所（範囲に含まない）】↓↓

// ↓↓1日毎の集計↓↓
                    // 1日1回のみカウントする項目はこのif文の中で設定する (同日複数勤務の場合に、日数が複数カウントされるのを防ぐため)
                        if ($day_lastwork_hantei == 1) {
                            // 出勤日数
                            $_s_workdays = ($working_info['timestamped_start_work_time'] > 0) ? $_s_workdays + 1 : $_s_workdays;

                            // 欠勤日数
                            $_s_absentdays = ($working_info['planned_work_status'] == "欠勤" || $absentday_count == 1) ? $_s_absentdays + 1 : $_s_absentdays;

                            // 有給日数
                            $_s_gainfuldays = ($working_day['real_taken_paid_rest_days'] > 0) ? $_s_gainfuldays + $working_day['real_taken_paid_rest_days'] : $_s_gainfuldays;

                            // 時間有給
                            $_s_gainfultime = ($working_day['real_taken_paid_rest_time'] > 0) ? $_s_gainfultime + $working_day['real_taken_paid_rest_time'] : $_s_gainfultime;

                            // 遅・早日数
                            $_s_ackurecntdays = (($working_info['real_late_time'] > 0) || ($working_info['real_early_leave_time'] > 0) || ($ackurecntday_count == 1)) ? $_s_ackurecntdays + 1 : $_s_ackurecntdays;

                            // 法定外残業時間（日）
                            $_s_hoteigai_day_overworktime = ($_p_hoteigai_day_overworktime > 0) ? $_s_hoteigai_day_overworktime + $_p_hoteigai_day_overworktime : $_s_hoteigai_day_overworktime;

                            // 法定外残業時間（週）
                            $_s_hoteigai_week_overworktime = ($_p_hoteigai_week_overworktime > 0) ? $_s_hoteigai_week_overworktime + $_p_hoteigai_week_overworktime : $_s_hoteigai_week_overworktime;

                            // 法定内残業時間（週）の合計 計算用
                            $sum_hoteinai_week_overworktime_m = $sum_hoteinai_week_overworktime_m + $hoteinai_week_overworktime_m;

                        } else {
                            // まだ同一日に勤務が残っている場合
                            $absentday_count = ($working_info['planned_work_status'] == "欠勤") ? 1 : $absentday_count;
                            $ackurecntday_count = (($working_info['real_late_time'] > 0) || ($working_info['real_early_leave_time'] > 0)) ? 1 : $ackurecntday_count;
                        }

                    // 勤務の都度カウントする項目
                        // 遅・早時間
                        $_s_ackurecnttime = (($working_info['real_late_time'] + $working_info['real_early_leave_time']) > 0) ? $_s_ackurecnttime + ($working_info['real_late_time'] + $working_info['real_early_leave_time']) : $_s_ackurecnttime;

                        // 総実績労働時間
                        $_s_real_worktime = ($_p_real_worktime > 0) ? $_s_real_worktime + $_p_real_worktime : $_s_real_worktime;

                        // 総実績所定内時間の計算
                        $_s_shotei_worktime = ($_p_shotei_worktime > 0) ? $_s_shotei_worktime + $_p_shotei_worktime : $_s_shotei_worktime;

                        // 総実績残業時間の計算
                        $_s_real_overworktime = ($_p_real_overworktime > 0) ? $_s_real_overworktime + $_p_real_overworktime : $_s_real_overworktime;

                        // 総深夜労働時間
                        $_s_night_worktime = ($_p_night_worktime > 0) ? $_s_night_worktime + $_p_night_worktime : $_s_night_worktime;

                        // 総法定休日労働時間
                        $_s_hotei_restday_worktime = ($hotei_restday_hantei == 1 && $_p_hotei_restday_worktime > 0) ? $_s_hotei_restday_worktime + $_p_hotei_restday_worktime : $_s_hotei_restday_worktime;
// ↑↑【コピペ対象外箇所（範囲に含まない）】↑↑

                    }

                } else {

// ↓↓勤務のない日↓↓

                    // 日付
                    $_p_date = $working_day['date'];

                    // 曜日
                    $_p_weekday = $working_day['weekday'];

                    // 所属先
                    $_p_branchname = $employee['work_location'];

                    // ID
                    $_p_branchid = $employee['presentation_id'];

                    // 名前
                    $_p_staffname = $employee['last_name'] . $employee['first_name'];

                    // 部署
                    $_p_department = $employee['department'];

                    // 採用形態
                    $_p_employtype = $employee['employment_type'];

                    // 法定外残業時間（週）
                    // 週起算日集計日
                    if ($week_kisanbi == $working_day['weekday']) {
                        if ($employee['schedule_type'] == "通常") {
                            $hoteigai_week_overworktime_m = $sum_realworktime_m - 40*60 - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m;
                        } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                            $hoteigai_week_overworktime_m = ($sum_shotei_worktime_m <= 40*60 && $sum_realworktime_m > 40) ? $sum_realworktime_m - 40*60 - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m : 0;
                            $hoteigai_week_overworktime_m = ($sum_shotei_worktime_m > 40*60 && $sum_realworktime_m > 40) ? $sum_realworktime_m - $sum_shotei_worktime_m - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m : $hoteigai_week_overworktime_m;
                        } else {
                            $hoteigai_week_overworktime_m = 0;
                        }
                        // 計算結果がマイナスになれば0にする
                        $hoteigai_week_overworktime_m = ($hoteigai_week_overworktime_m > 0) ? $hoteigai_week_overworktime_m : 0;
                        $_p_hoteigai_week_overworktime = $hoteigai_week_overworktime_m;

                        $sum_shotei_worktime_m = 0;
                        $sum_realworktime_m = 0;
                        $sum_hoteigai_day_overworktime_m = 0;
                        $sum_hotei_restday_worktime_m = 0;

                    // 週起算日集計日以外
                    } else {
                        $hoteigai_week_overworktime_m = 0;
                        $_p_hoteigai_week_overworktime = null;
                    }

                    // 法定内残業時間(週)
                    if ($week_kisanbi == $working_day['weekday']) {
                        $hoteinai_week_overworktime_m = $sum_hoteinai_day_overworktime_m;
                        $sum_hoteinai_day_overworktime_m = 0;
                    } else {
                        $hoteinai_week_overworktime_m = 0;
                    }
                    // 計算結果がマイナスになれば0にする
                    $hoteinai_week_overworktime_m = ($hoteinai_week_overworktime_m - $hoteigai_week_overworktime_m > 0) ? $hoteinai_week_overworktime_m - $hoteigai_week_overworktime_m : 0;

                    // 勤務のない日はブランクとなる項目はここに追加(nullを代入)
                    // 勤務形態, 休暇形態, 所定出勤, 出勤打刻時刻, 出勤計算時刻, 所定退勤, 退勤打刻時刻, 退勤計算時刻, 遅・早, 所定休憩, 実績休憩, 外出時間, 所定労働時間, 実績総労働時間, 実績の総残業時間（早出と残業）, 法定内残業時間（日）, 法定外残業時間（日）, 深夜労働時間, 法定休日労働時間
                    $_p_shifttype = $_p_resttype = $_p_shotei_atime = $_p_real_atime = $_p_atime_maru = $_p_shotei_ctime = $_p_real_ctime = $_p_ctime_maru = $_p_ackoure = $_p_shotei_resttime = $_p_real_resttime = $_p_real_gtime = $_p_shotei_worktime = $_p_real_worktime = $_p_real_overworktime = $_p_hoteinai_day_overworktime = $_p_hoteigai_day_overworktime = $_p_night_worktime = $_p_hotei_restday_worktime = null;

// ↓↓【コピペ対象外箇所（範囲に含まない）】↓↓
                    if ($week_kisanbi == $working_day['weekday']) {
                    // 法定外残業時間（週）
                        $_s_hoteigai_week_overworktime = $_s_hoteigai_week_overworktime + $_p_hoteigai_week_overworktime;
                    }
                    // 法定内残業時間（週）の合計　計算用
                    $sum_hoteinai_week_overworktime_m = $sum_hoteinai_week_overworktime_m + $hoteinai_week_overworktime_m;
// ↑↑【コピペ対象外箇所（範囲に含まない）】↑↑

                } //勤務毎のループはここまで
            } //1日毎のループはここまで
            // exit;

// ↓↓全期間集計後の計算↓↓

            // 時間有給8時間以上⇒日数に変換
            if ($_s_gainfultime >= 8*60) {
                $_s_gainfuldays = $_s_gainfuldays + floor($_s_gainfultime/480);
                $_s_gainfultime = $_s_gainfultime % 480;
            }

            // フレックス形態でカレンダーにフレックス所定時間を登録している場合は、所定労働時間＝カレンダーの所定労働時間を参照
            $flex_month_syotei_worktime = 0;
            foreach($this->work_location_settings[$workarea_id]['calendar']['work_time_by_months'] as $key => $value) {
                if ($key == date("Y-m", strtotime($_POST['end_date']))) {
                    $flex_month_syotei_worktime = $value*60;
                }
            }

            if ($employee['schedule_type'] == "フレックス" && $flex_month_syotei_worktime > 0) {
                $_s_shotei_worktime = $flex_month_syotei_worktime;
            }

            // 1か月の法定時間  「$date_count = 集計日数」
            $day1 = new DateTime($_POST['end_date']);
            $day2 = new DateTime($_POST['start_date']);
            $interval = $day1->diff($day2);
            $date_count = $interval->format('%a') + 1;
            switch ($date_count) {
                case 28 :
                    $month_hotei_worktime = 160;
                    break;
                case 29 :
                    $month_hotei_worktime = 165.71;
                    break;
                case 30 :
                    $month_hotei_worktime = 171.42;
                    break;
                case 31 :
                    $month_hotei_worktime = 177.14;
                    break;
            }

            // 法定外残業時間（月）
            if ($employee['schedule_type'] == "通常") {
                $_s_hoteigai_month_overworktime = "";
            } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                $_s_hoteigai_month_overworktime = $_s_real_worktime - $_s_hoteigai_day_overworktime - $_s_hoteigai_week_overworktime - $month_hotei_worktime*60 - $_s_hotei_restday_worktime;
            } elseif ($employee['schedule_type'] == "フレックス"){
                $_s_hoteigai_month_overworktime = $_s_real_worktime - $month_hotei_worktime*60 - $_s_hotei_restday_worktime;
            }
            $_s_hoteigai_month_overworktime = ($_s_hoteigai_month_overworktime > 0) ? $_s_hoteigai_month_overworktime : 0;

            // 法定外残業時間・計
            $_s_hoteigai_total_overworktime = $_s_hoteigai_day_overworktime + $_s_hoteigai_week_overworktime + $_s_hoteigai_month_overworktime;

            // 法定内残業時間・計
            if ($employee['schedule_type'] == "通常" || $employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働") {
                $_s_hoteinai_total_overworktime = $sum_hoteinai_week_overworktime_m - $_s_hoteigai_month_overworktime;
            } elseif ($employee['schedule_type'] == "フレックス"){
                $_s_hoteinai_total_overworktime = $_s_real_worktime - $_s_hotei_restday_worktime - $_s_hoteigai_month_overworktime - $_s_shotei_worktime;
            }
            $_s_hoteinai_total_overworktime = ($_s_hoteinai_total_overworktime > 0) ? $_s_hoteinai_total_overworktime : 0;

// ↓↓ 勤怠集計項目表示 ↓↓

            // 所属先
            $_s_branchname = $employee['work_location'];

            // ID
            $_s_branchid = $employee['presentation_id'];

            // 名前
            $_s_staffname = $employee['last_name'] . $employee['first_name'];

            // 部署
            $_s_department = $employee['department'];

            // 採用形態
            $_s_employtype = $employee['employment_type'];

            // 出勤日数
            $_s_workdays = $_s_workdays;

            // 欠勤日数
            $_s_absentdays = $_s_absentdays;

            // 有休日数
            $_s_gainfuldays = $_s_gainfuldays;

            // 時間有給
            $_s_gainfultime = $_s_gainfultime/60;

            // 遅・早時間
            $_s_ackurecnttime = $_s_ackurecnttime/60;

            // 遅・早日数
            $_s_ackurecntdays = $_s_ackurecntdays;

            // 総実績総労働時間
            $_s_real_worktime = $_s_real_worktime/60;

            // 総所定労働時間
            $_s_shotei_worktime = $_s_shotei_worktime/60;

            // 総実績総残業時間
            $_s_real_overworktime = $_s_real_overworktime/60;

            // 法定外残業時間（日）
            $_s_hoteigai_day_overworktime = $_s_hoteigai_day_overworktime/60;

            // 法定外残業時間（週）
            $_s_hoteigai_week_overworktime = $_s_hoteigai_week_overworktime/60;

            // 法定外残業時間（月）
            $_s_hoteigai_month_overworktime = $_s_hoteigai_month_overworktime/60;

            // 法定内残業時間・計
            $_s_hoteinai_total_overworktime = $_s_hoteinai_total_overworktime/60;

            // 法定外残業時間・計
            $_s_hoteigai_total_overworktime = $_s_hoteigai_total_overworktime/60;

            // 総深夜労働時間
            $_s_night_worktime = $_s_night_worktime/60;

            // 総法定休日労働時間
            $_s_hotei_restday_worktime = $_s_hotei_restday_worktime/60;


            // CSVファイルに入れる
            $result[] = [$_s_branchname, $_s_branchid, $_s_staffname, $_s_department, $_s_employtype, $_s_workdays, $_s_absentdays, $_s_gainfuldays, $_s_gainfultime, $_s_ackurecnttime, $_s_ackurecntdays, $_s_real_worktime, $_s_shotei_worktime, $_s_real_overworktime, $_s_hoteigai_day_overworktime, $_s_hoteigai_week_overworktime, $_s_hoteigai_month_overworktime, $_s_hoteinai_total_overworktime, $_s_hoteigai_total_overworktime, $_s_night_worktime, $_s_hotei_restday_worktime];
        }
        // exit;

        return $result;
    }


    /**
     * Prepare the Attendance Data.
     * Have an array of all the days from start_date to end_date, in each day, prepare all employees working data.
     *
     * @param array         $conditions
     * @param array         $work_location_ids
     * @param boolean       $load_seven_day_prior_to_start_date
     * @return array
     */
    public function prepareDataByDateEmployee($conditions, $work_location_ids, $load_seven_day_prior_to_start_date = false)
    {
        $this->conditions = $conditions;

        $query = Employee::with('workLocation')->whereIn('work_location_id', $work_location_ids)->where('joined_date', '<=', $conditions['end_date'])->where(function($query) use ($conditions) {
            return $query->whereNull('resigned_date')->orWhere('resigned_date', '>=', $conditions['start_date']);
        });

        if (isset($conditions['presentation_id'])) {
            $query = $query->where('presentation_id', $conditions['presentation_id']);
        }

        if (isset($conditions['name'])) {
            $query = $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions['name'] . '%');
        }

        if (isset($conditions['work_status'])) {
            $query = $query->where('work_status', $conditions['work_status']);
        }

        $employees = $query->orderBy('work_location_id')->orderBy('view_order')->get();

        $start_date_to_get_attendance_data = new Carbon($conditions['start_date']);
        $end_date_to_get_attendance_data = new Carbon($conditions['end_date']);

        // Get the data from 7 days before per request of the support side.
        if ($load_seven_day_prior_to_start_date) {
            $start_date_to_get_attendance_data->subDays(7);
        }

        $employee_working_days = EmployeeWorkingDay::with([
            'employee.workLocation',
            'workingTimestamps',
            'concludedEmployeeWorkingDay',
            'substituteEmployeeWorkingInformations',
            'employeeWorkingInformationSnapshots',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            'employeeWorkingInformations.colorStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.setting',
            'employeeWorkingInformations.currentPlannedWorkLocation.unusedWorkStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.unusedRestStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.company.setting',
            'employeeWorkingInformations.currentPlannedWorkLocation.company.workStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.company.restStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.setting',
            'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.setting',
            'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
            'employeeWorkingInformations.employeeWorkingInformationSnapshot'
        ])->where('date', '>=', $start_date_to_get_attendance_data->toDateString())->where('date', '<=', $end_date_to_get_attendance_data->toDateString())->whereIn('employee_id', $employees->pluck('id')->toArray())->get();

        // Turn into hash table
        $employee_working_days = $employee_working_days->keyBy(function($working_day) {
            return $working_day->date . '_' . $working_day->employee_id;
        });

        $data = [];

        $carbon_pivot = $start_date_to_get_attendance_data->copy();
        while ($carbon_pivot->lte($end_date_to_get_attendance_data)) {
            $day_string = $carbon_pivot->format('Y-m-d');
            $data_for_each_day = [
                'employees' => [],
                'date' => $day_string,
            ];

            foreach ($employees as $employee) {
                $data_for_each_day['employees'][$employee->presentation_id] = $this->employeeOutputData($employee);

                $unique_key = $day_string . '_' . $employee->id;

                if (isset($employee_working_days[$unique_key])) {
                    $data_for_each_day['employees'][$employee->presentation_id]['working_day'] = $this->employeeWorkingDayOutputData($employee_working_days[$unique_key]);
                } else {
                    $data_for_each_day['employees'][$employee->presentation_id]['working_day'] = $this->emptyEmployeeWorkingDayData($day_string, $employee->workLocation->presentation_id);
                }
            }
            $data[$day_string] = $data_for_each_day;
            $carbon_pivot->addDay();
        }

        $this->attendance_data_by_date_employee = $data;
    }

    /**
     * Download the Attendance data.
     *
     * @param array     $conditions
     * @param array     $work_location_ids
     * @return array
     */
    public function downloadAttendanceData($conditions, $work_location_ids)
    {
        // 勤怠データ処理の方は「$attendance_data_by_date_employee」変数がデフォルトとして使われています。
        // $this->prepareDataByDateEmployee($conditions, $work_location_ids);

        // もし、「$attendance_data_by_employee_date」変数を使いたいなら、上の行をコメントアウトして、下の行をコメント外てください。
        $this->prepareDataByEmployeeDate($conditions, $work_location_ids);

        // 上の二つの関数は三つ目のパラメータを受けれます。そのパラメータは「boolean」型で、「前の７日のデータを読み込むかどうか」ということを決めます。「true」は読み込む、
        // 逆、「false」は読み込まないということです。必須ではありません。デフォルト値は「false」で、ゆえに「読み込まない」です。

        $result = $this->processAttendance();

        // 前の７日を読み込むの場合で、その７日のデータを最後のデータからフィルターアウトしたいなら、下の関数を使ってくだいさい。
        // $result = $this->filterOutSevenDaysBeforeStartDay($result, $conditions);

        return $result;
    }

    /**
     * Process the Attendance data. Depend on each company's need, this function can be overridden and customized.
     *
     * @return array
     */
    public function processAttendance()
    {
        // Declare and set the header
        $result = [
            [
                '日付', '曜日','所属先', 'ID', '名前', '部署', '採用形態', '勤務形態', '休暇形態','所定出勤', '出勤打刻時刻', '出勤計算時刻', '所定退勤', '退勤打刻時刻', '退勤計算時刻', '遅・早', '所定休憩', '実績休憩', '外出時間', '所定労働時間', '実績総労働時間', '実績の総残業時間（早出と残業）', '法定内残業時間（日）', '法定外残業時間（日）', '法定外残業時間（週）', '深夜労働時間', '法定休日労働時間',
            ],
        ];


        foreach ($this->attendance_data_by_employee_date as $employee) {


            // 「$week_kisanbi == 起算日から7日後の曜日」 (例)起算日が月曜であれば"日"が入ります
            // 「$week_kisanbi == 起算日当日」に変更したい場合は、$weekの配列を[0=>"日", 1=>"月", 2=>"火", 3=>"水", 4=>"木", 5=>"金", 6=>"土"]に差し替えてください。
            // 起算日を使用しない場合は不要のため、削除してください。
            $workarea_id = $employee['work_location_presentation_id'];
            $week_kisanbi_no = $this->work_location_settings[$workarea_id]['setting']['start_day_of_week'];
            $week = [0=>"土", 1=>"日", 2=>"月", 3=>"火", 4=>"水", 5=>"木", 6=>"金"];
            foreach($week as $key=>$value) {
                if ($week_kisanbi_no == $key) {
                    $week_kisanbi = $value;
                }
            }

// ↓↓従業員毎に初期化する必要のある変数を設定 ↓↓
            $hoteinai_week_overworktime_m = 0; //法定内残業時間(週)
            $hoteigai_week_overworktime_m = 0; //法定外残業時間(週)
            // 週起算日集計用変数
            $sum_shotei_worktime_m = 0; //所定労働時間
            $sum_realworktime_m = 0; //実績総労働時間
            $sum_hoteinai_day_overworktime_m = 0; //法定内残業時間(日)
            $sum_hoteigai_day_overworktime_m = 0; //法定外残業時間(日)
            $sum_hotei_restday_worktime_m = 0; //法定休日労働時間

            // 勤務日を1日毎にループ
			foreach ($employee['employee_working_days'] as $working_day) {
                // 1日毎に初期化する変数
                $day_work_count = 1; //同一日勤務回数カウント

                if (count($working_day['employee_working_informations']) > 0) {

                    // 勤務ごとにループ（1日複数勤務の可能性あり）
                    foreach ($working_day['employee_working_informations'] as $working_info) {
// ↓↓↓↓ ココから勤怠集計へコピぺ ↓↓↓↓

// ↓↓ 判定フラグ変数の設定 ↓↓
                        // 法定休日判定(勤務形態が『法出』であれば「1」、そうでなければブランク)
                        $hotei_restday_hantei = ($working_info['planned_work_status'] == "法出") ? 1 : "";

                        // 同日最終勤務判定(同一日最終勤務であれば「1」, そうでなければブランク)
                        if (count($working_day['employee_working_informations']) > 1) {
                            if (count($working_day['employee_working_informations']) == $day_work_count) {
                                $day_lastwork_hantei = 1;
                            } else {
                                $day_lastwork_hantei = "";
                                $day_work_count++;
                            }
                        } else {
                            $day_lastwork_hantei = 1;
                        }

// ↓↓ レコード挿入変数の設定 ↓↓

                        // 日付
                        $_p_date = $working_day['date'];

                        // 曜日
                        $_p_weekday = $working_day['weekday'];

                        // 所属先
                        $_p_branchname = $employee['work_location'];

                        // ID
                        $_p_branchid = $employee['presentation_id'];

                        // 名前
                        $_p_staffname = $employee['last_name'] . $employee['first_name'];

                        // 部署
                        $_p_department = $employee['department'];

                        // 採用形態
                        $_p_employtype = $employee['employment_type'];

                        // 勤務形態
                        $_p_shifttype = $working_info['planned_work_status'];

                        // 休暇形態
                        $_p_resttype = $working_info['planned_rest_status'];

                        // 所定出勤
                        $_p_shotei_atime = (isset($working_info['schedule_start_work_time'])) ? (date("H:i", strtotime($working_info['schedule_start_work_time']))) : "";

                        // 出勤打刻時刻
                        $_p_real_atime = (isset($working_info['timestamped_start_work_time'])) ? (date("H:i", strtotime($working_info['timestamped_start_work_time']))) : "";

                        // 出勤計算時刻
                        $_p_atime_maru = (isset($working_info['real_start_work_time'])) ? (date("H:i", strtotime($working_info['real_start_work_time']))) : "";

                        // 所定退勤
                        $_p_shotei_ctime = (isset($working_info['schedule_end_work_time'])) ? (date("H:i", strtotime($working_info['schedule_end_work_time']))) : "";

                        // 退勤打刻時刻
                        $_p_real_ctime = (isset($working_info['timestamped_end_work_time'])) ? (date("H:i", strtotime($working_info['timestamped_end_work_time']))) : "";

                        // 退勤計算時刻
                        $_p_ctime_maru = (isset($working_info['real_end_work_time'])) ? (date("H:i", strtotime($working_info['real_end_work_time']))) : "";

                        // 遅・早
                        $_p_ackoure = (($working_info['real_late_time'] + $working_info['real_early_leave_time']) > 0) ? ($working_info['real_late_time'] + $working_info['real_early_leave_time']) : "";

                        // 所定休憩
                        $_p_shotei_resttime = ($working_info['schedule_break_time'] > 0) ? $working_info['schedule_break_time'] : "";

                        // 実績休憩
                        $_p_real_resttime = ($working_info['real_break_time'] > 0) ? $working_info['real_break_time'] : "";

                        // 外出時間
                        $_p_real_gtime = ($working_info['real_go_out_time'] > 0) ? $working_info['real_go_out_time'] : "";

                        // 所定労働時間
                        $shotei_worktime_m = $this->fnm_60hexToMinute($working_info['schedule_working_hour']);
                        $_p_shotei_worktime = ($shotei_worktime_m > 0) ? $shotei_worktime_m : "";

                        // 所定労働時間(日)  ※非表示項目
                        $daytotal_shotei_worktime_m = $working_day['schedule_working_time'];

                        // 実績総労働時間
                        $real_worktime_m = $this->fnm_60hexToMinute($working_info['real_working_hour']);
                        $_p_real_worktime = ($real_worktime_m > 0) ? $real_worktime_m : "";

                        // 実績総労働時間(日)  ※非表示項目
                        $daytotal_real_worktime_m = $working_day['real_working_time'];

                        // 実績の総残業時間（早出と残業）
                        $_p_real_overworktime = ($working_info['summary_real_overtime'] > 0) ? $working_info['summary_real_overtime'] : "";


                    // 1日の出勤が1回 or 同日最終勤務の場合、法定内残業＆法定外残業を計算する
                        if ($day_lastwork_hantei == 1) {

                        // 法定外残業時間（日）
                            if ($employee['schedule_type'] == "通常") {
                                $hoteigai_day_overworktime_m = $daytotal_real_worktime_m - 8*60;
                            } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                                $hoteigai_day_overworktime_m = ($daytotal_shotei_worktime_m > 8*60) ? $daytotal_real_worktime_m - $daytotal_shotei_worktime_m : $daytotal_real_worktime_m - 8*60;
                            } else {
                                $hoteigai_day_overworktime_m = 0;
                            }
                            $hoteigai_day_overworktime_m = ($hotei_restday_hantei == "" && $hoteigai_day_overworktime_m > 0) ? $hoteigai_day_overworktime_m : 0;
                            $_p_hoteigai_day_overworktime = ($hoteigai_day_overworktime_m > 0) ? $hoteigai_day_overworktime_m : "";

                        // 法定内残業時間（日）
                            if ($employee['schedule_type'] == "通常") {
                                $hoteinai_day_overworktime_m = $daytotal_real_worktime_m - $daytotal_shotei_worktime_m - $hoteigai_day_overworktime_m;
                            } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                                $hoteinai_day_overworktime_m = ($daytotal_shotei_worktime_m < 8*60) ? $daytotal_real_worktime_m - $daytotal_shotei_worktime_m - $hoteigai_day_overworktime_m : 0;
                            } else {
                                $hoteinai_day_overworktime_m = 0;
                            }
                            $hoteinai_day_overworktime_m = ($hotei_restday_hantei == "" && $hoteinai_day_overworktime_m > 0) ? $hoteinai_day_overworktime_m : 0;
                            $_p_hoteinai_day_overworktime = ($hoteinai_day_overworktime_m > 0) ? $hoteinai_day_overworktime_m : "";

                        // 法定外残業時間（週）
                            // 必要項目の集計
                            $sum_shotei_worktime_m = $sum_shotei_worktime_m + $daytotal_shotei_worktime_m;
                            $sum_realworktime_m = $sum_realworktime_m + $daytotal_real_worktime_m;
                            $sum_hoteigai_day_overworktime_m = $sum_hoteigai_day_overworktime_m + $hoteigai_day_overworktime_m;
                            $sum_hotei_restday_worktime_m = ($hotei_restday_hantei == 1) ? $sum_hotei_restday_worktime_m + $real_worktime_m : $sum_hotei_restday_worktime_m;

                            if ($week_kisanbi == $working_day['weekday']) {
                            // 起算日集計日の場合、
                                if ($employee['schedule_type'] == "通常") {
                                    $hoteigai_week_overworktime_m = $sum_realworktime_m - 40*60 - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m;
                                } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                                    $hoteigai_week_overworktime_m = ($sum_shotei_worktime_m <= 40*60 && $sum_realworktime_m > 40) ? $sum_realworktime_m - 40*60 - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m : 0;
                                    $hoteigai_week_overworktime_m = ($sum_shotei_worktime_m > 40*60 && $sum_realworktime_m > 40) ? $sum_realworktime_m - $sum_shotei_worktime_m - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m : $hoteigai_week_overworktime_m;
                                } else {
                                    $hoteigai_week_overworktime_m = 0;
                                }
                                $hoteigai_week_overworktime_m = ($hoteigai_week_overworktime_m > 0) ? $hoteigai_week_overworktime_m : 0;
                                $_p_hoteigai_week_overworktime = $hoteigai_week_overworktime_m;
                                // 集計変数初期化
                                $sum_shotei_worktime_m = 0;
                                $sum_realworktime_m = 0;
                                $sum_hoteigai_day_overworktime_m = 0;
                                $sum_hotei_restday_worktime_m = 0;

                            // 起算日集計日以外の場合、
                            } else {
                                $_p_hoteigai_week_overworktime = "";
                            }

                        // 法定内残業時間(週)
                            // 起算日集計日
                            if ($week_kisanbi == $working_day['weekday']) {
                                $sum_hoteinai_day_overworktime_m = $sum_hoteinai_day_overworktime_m + $hoteinai_day_overworktime_m;
                                $hoteinai_week_overworktime_m = $sum_hoteinai_day_overworktime_m;
                                $sum_hoteinai_day_overworktime_m = 0;
                            // 起算日集計日以外
                            } else {
                                $sum_hoteinai_day_overworktime_m = $sum_hoteinai_day_overworktime_m + $hoteinai_day_overworktime_m;
                                $hoteinai_week_overworktime_m = 0;
                            }

                            // 計算結果がマイナスになれば0にする
                            $hoteinai_week_overworktime_m = ($hoteinai_week_overworktime_m - $hoteigai_week_overworktime_m > 0) ? $hoteinai_week_overworktime_m - $hoteigai_week_overworktime_m : 0;

                    // 同日最終勤務ではない場合(1日に複数出勤があり、最終勤務でない場合)
                        } else {
                            $_p_hoteinai_day_overworktime = "";
                            $_p_hoteigai_day_overworktime = "";
                            $_p_hoteigai_week_overworktime = "";
                            $hoteinai_week_overworktime_m = 0;
                            $sum_hotei_restday_worktime_m = ($hotei_restday_hantei == 1) ? $sum_hotei_restday_worktime_m + $real_worktime_m : $sum_hotei_restday_worktime_m;
                        }

                        // 深夜労働時間
                        $_p_night_worktime = ($working_info['summary_real_night_work_time'] > 0) ? $working_info['summary_real_night_work_time'] : "";

                        // 法定休日労働時間
                        $_p_hotei_restday_worktime = ($hotei_restday_hantei == 1) ? $real_worktime_m : "";

// ↑↑↑↑ ココまで勤怠集計へコピぺ ↑↑↑↑

                        // 表示形式の変更 （デフォルトは分⇒10進数）
                        // ※勤怠集計では全て（分）で計算するために、このタイミングで一括して表示形式を（分）から変更します。
                        $_p_ackoure = ($_p_ackoure > 0) ? $_p_ackoure/60 : "";
                        $_p_shotei_resttime = ($_p_shotei_resttime > 0) ? $_p_shotei_resttime/60 : "";
                        $_p_real_resttime = ($_p_real_resttime > 0) ? $_p_real_resttime/60 : "";
                        $_p_real_gtime = ($_p_real_gtime > 0) ? $_p_real_gtime/60 : "";
                        $_p_shotei_worktime = ($_p_shotei_worktime > 0) ? $_p_shotei_worktime/60 : "";
                        $_p_real_worktime = ($_p_real_worktime > 0) ? $_p_real_worktime/60 : "";
                        $_p_real_overworktime = ($_p_real_overworktime > 0) ? $_p_real_overworktime/60 : "";
                        $_p_hoteinai_day_overworktime = ($_p_hoteinai_day_overworktime > 0) ? $_p_hoteinai_day_overworktime/60 : "";
                        $_p_hoteigai_day_overworktime = ($_p_hoteigai_day_overworktime > 0) ? $_p_hoteigai_day_overworktime/60 : "";
                        $_p_hoteigai_week_overworktime = ($_p_hoteigai_week_overworktime > 0) ? $_p_hoteigai_week_overworktime/60 : "";
                        $_p_night_worktime = ($_p_night_worktime > 0) ? $_p_night_worktime/60 : "";
                        $_p_hotei_restday_worktime = ($_p_hotei_restday_worktime) ? $_p_hotei_restday_worktime/60 : "";

                        // 行の挿入
                        $result[] = [$_p_date, $_p_weekday, $_p_branchname, $_p_branchid, $_p_staffname, $_p_department, $_p_employtype, $_p_shifttype, $_p_resttype, $_p_shotei_atime, $_p_real_atime, $_p_atime_maru, $_p_shotei_ctime, $_p_real_ctime, $_p_ctime_maru, $_p_ackoure, $_p_shotei_resttime, $_p_real_resttime, $_p_real_gtime, $_p_shotei_worktime, $_p_real_worktime, $_p_real_overworktime, $_p_hoteinai_day_overworktime, $_p_hoteigai_day_overworktime, $_p_hoteigai_week_overworktime, $_p_night_worktime, $_p_hotei_restday_worktime];
                    }


                // 勤務がない日の場合なら、「従業員情報はあるが、勤務情報は空白」というレコードを作ります。
                } else {

// ↓↓↓↓ ココから勤怠集計へコピぺ ↓↓↓↓

                    // 日付
                    $_p_date = $working_day['date'];

                    // 曜日
                    $_p_weekday = $working_day['weekday'];

                    // 所属先
                    $_p_branchname = $employee['work_location'];

                    // ID
                    $_p_branchid = $employee['presentation_id'];

                    // 名前
                    $_p_staffname = $employee['last_name'] . $employee['first_name'];

                    // 部署
                    $_p_department = $employee['department'];

                    // 採用形態
                    $_p_employtype = $employee['employment_type'];

                    // 法定外残業時間（週）
                    // 週起算日集計日
                    if ($week_kisanbi == $working_day['weekday']) {
                        if ($employee['schedule_type'] == "通常") {
                            $hoteigai_week_overworktime_m = $sum_realworktime_m - 40*60 - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m;
                        } elseif ($employee['schedule_type'] == "1ヶ月単位変形労働" || $employee['schedule_type'] == "１年単位変形労働"){
                            $hoteigai_week_overworktime_m = ($sum_shotei_worktime_m <= 40*60 && $sum_realworktime_m > 40) ? $sum_realworktime_m - 40*60 - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m : 0;
                            $hoteigai_week_overworktime_m = ($sum_shotei_worktime_m > 40*60 && $sum_realworktime_m > 40) ? $sum_realworktime_m - $sum_shotei_worktime_m - $sum_hoteigai_day_overworktime_m - $sum_hotei_restday_worktime_m : $hoteigai_week_overworktime_m;
                        } else {
                            $hoteigai_week_overworktime_m = 0;
                        }
                        // 計算結果がマイナスになれば0にする
                        $hoteigai_week_overworktime_m = ($hoteigai_week_overworktime_m > 0) ? $hoteigai_week_overworktime_m : 0;
                        $_p_hoteigai_week_overworktime = $hoteigai_week_overworktime_m;

                        $sum_shotei_worktime_m = 0;
                        $sum_realworktime_m = 0;
                        $sum_hoteigai_day_overworktime_m = 0;
                        $sum_hotei_restday_worktime_m = 0;

                    // 週起算日集計日以外
                    } else {
                        $hoteigai_week_overworktime_m = 0;
                        $_p_hoteigai_week_overworktime = null;
                    }

                    // 法定内残業時間(週)
                    if ($week_kisanbi == $working_day['weekday']) {
                        $hoteinai_week_overworktime_m = $sum_hoteinai_day_overworktime_m;
                        $sum_hoteinai_day_overworktime_m = 0;
                    } else {
                        $hoteinai_week_overworktime_m = 0;
                    }
                    // 計算結果がマイナスになれば0にする
                    $hoteinai_week_overworktime_m = ($hoteinai_week_overworktime_m - $hoteigai_week_overworktime_m > 0) ? $hoteinai_week_overworktime_m - $hoteigai_week_overworktime_m : 0;

                    // 勤務のない日はブランクとなる項目はここに追加(nullを代入)
                    // 勤務形態, 休暇形態, 所定出勤, 出勤打刻時刻, 出勤計算時刻, 所定退勤, 退勤打刻時刻, 退勤計算時刻, 遅・早, 所定休憩, 実績休憩, 外出時間, 所定労働時間, 実績総労働時間, 実績の総残業時間（早出と残業）, 法定内残業時間（日）, 法定外残業時間（日）, 深夜労働時間, 法定休日労働時間
                    $_p_shifttype = $_p_resttype = $_p_shotei_atime = $_p_real_atime = $_p_atime_maru = $_p_shotei_ctime = $_p_real_ctime = $_p_ctime_maru = $_p_ackoure = $_p_shotei_resttime = $_p_real_resttime = $_p_real_gtime = $_p_shotei_worktime = $_p_real_worktime = $_p_real_overworktime = $_p_hoteinai_day_overworktime = $_p_hoteigai_day_overworktime = $_p_night_worktime = $_p_hotei_restday_worktime = null;

// ↑↑↑↑ ココまで勤怠集計へコピぺ ↑↑↑↑

                    // 表示形式の変更 （デフォルトは分⇒10進数）
                    // ※勤怠集計では全て（分）で計算するために、このタイミングで一括して表示形式を（分）から変更します。
                    $_p_hoteigai_week_overworktime = ($_p_hoteigai_week_overworktime > 0) ? $_p_hoteigai_week_overworktime/60 : "";

                    $result[] = [$_p_date, $_p_weekday, $_p_branchname, $_p_branchid, $_p_staffname, $_p_department, $_p_employtype, $_p_shifttype, $_p_resttype, $_p_shotei_atime, $_p_real_atime, $_p_atime_maru, $_p_shotei_ctime, $_p_real_ctime, $_p_ctime_maru, $_p_ackoure, $_p_shotei_resttime, $_p_real_resttime, $_p_real_gtime, $_p_shotei_worktime, $_p_real_worktime, $_p_real_overworktime, $_p_hoteinai_day_overworktime, $_p_hoteigai_day_overworktime, $_p_hoteigai_week_overworktime, $_p_night_worktime, $_p_hotei_restday_worktime];
                }
            }
            // exit;
        }
        // exit;

        return $result;
    }

    /**
     * According to the supporters side's request, data of seven days before start_date also be loaded, and now
     * according to another request, those seven days's data should be removed before output the result to csv file.
     * This function is for that purpose.
     *
     * @param   array       $result                 the result data set
     * @param   array       $conditions             the search conditions
     * @param   integer     $date_index             the index of the date column in the result data set, it is default to 0
     * @param   array       $to_be_ignored_texts    if the row has there values in the column 'date', then that row will be ignored(ex: useful for header case).
     */
    protected function filterOutSevenDaysBeforeStartDay($result, $conditions, $date_index = 0, $to_be_ignored_texts = [ '日付' ])
    {
        $collection = collect($result);

        $start_date = new Carbon($conditions['start_date']);

        return $collection->filter( function($row) use ($date_index, $start_date, $to_be_ignored_texts) {
            if (in_array($row[$date_index], $to_be_ignored_texts)) {
                return true;
            } else {
                $current_date = new Carbon($row[$date_index]);
                return  $current_date->gte($start_date);
            }
        })->toArray();
    }


    /**
     * Output data of an Employee
     *
     * @param Employee      $employee
     * @return array
     */
    protected function employeeOutputData($employee)
    {
        $data = $employee->toArray();
        unset($data['password']);

        $data['todofuken'] = $data['todofuken'] ? $this->todofuken_list[$data['todofuken']] : null;
        $data['work_location'] = $this->work_location_names[$data['work_location_id']];
        $data['work_location_presentation_id'] = $this->work_location_presentation_ids[$data['work_location_id']];
        $data['department'] = $data['department_id'] ? $this->department_names[$data['department_id']] : null;
        $data['schedule_type'] = $this->schedule_types[$data['schedule_type']];
        $data['employment_type'] = $this->employment_types[$data['employment_type']];
        $data['salary_type'] = $this->salary_types[$data['salary_type']];
        $data['work_status'] = $this->employee_work_statuses[$data['work_status']];
        $data['holiday_bonus_type'] = $this->holiday_bonus_types[$data['holiday_bonus_type']];

        return $data;
    }

    /**
     * Output data of an EmployeeWorkingDay
     *
     * @param EmployeeWorkingDay        $employee_working_day
     * @return array
     */
    protected function employeeWorkingDayOutputData($employee_working_day)
    {
        $data = $employee_working_day->getSummaryInformation();

        unset($data['have_schedule']);
        unset($data['planned_work_day']);
        unset($data['real_work_day']);
        // unset($data['planned_taken_paid_rest_days']);
        // unset($data['real_taken_paid_rest_days']);
        // unset($data['planned_taken_paid_rest_time']);
        // unset($data['real_taken_paid_rest_time']);

        $data['date'] = $employee_working_day->date;
        $data['weekday'] = $this->getTheWeekDayString($employee_working_day->date);
        $data['is_national_holiday'] = $this->national_holiday_service->is($employee_working_day->date);

        $data['rest_day_type'] = $this->getCalendarRestDaySetting($employee_working_day->date, $employee_working_day->employee->workLocation->presentation_id);

        $data['working_timestamps'] = $this->extractWorkingTimestamps($employee_working_day);

        // WorkingInformations
        $working_informations = [];

        $sorted_working_infos = $employee_working_day->employeeWorkingInformations->sortBy('planned_start_work_time');

        foreach ($sorted_working_infos as $working_info) {
            $working_informations[] = $this->employeeWorkingInformationOutputData($working_info);
        }

        $data['employee_working_informations'] = $working_informations;

        // Snapshots - ONLY get snapshots that don't have EWI (法出, 休出 sinsei). Because those that have, will be get through EWI
        $snapshots = [];

        $snapshots_without_ewi = $employee_working_day->employeeWorkingInformationSnapshots->filter(function($snapshot) {
            return $snapshot->employee_working_information_id == null;
        });

        foreach ($snapshots_without_ewi as $snapshot) {
            $snapshots[] = [
                'status' => $snapshot->left_status,
                'requester_note' => $snapshot->left_requester_note ? $snapshot->left_requester_note : ($snapshot->right_requester_note ? $snapshot->right_requester_note : null),
                'approver_note' => $snapshot->left_approver_note ? $snapshot->left_approver_note : ($snapshot->right_approver_note ? $snapshot->right_approver_note : null),
            ];
        }

        $data['houde_kyuude_sinsei_note'] = $snapshots;

        return $data;
    }

    /**
     * Create an empty record of an EmployeeWorkingDay.
     * Because, from time to time, the Employee doesn't have an instance of EmployeeWorkingDay for a specific day.
     *
     * @param string    $day_string
     * @param string    $work_location_presentation_id
     * @return array
     */
    protected function emptyEmployeeWorkingDayData($day_string, $work_location_presentation_id)
    {
        return [
            'date' => $day_string,
            'weekday' => $this->getTheWeekDayString($day_string),
            'is_national_holiday' => $this->national_holiday_service->is($day_string),
            'rest_day_type' => $this->getCalendarRestDaySetting($day_string, $work_location_presentation_id),
            'planned_working_time' => 0,
            'real_working_time' => 0,
            'schedule_working_time' => 0,
            'planned_work_span_time'  => 0,
            'real_work_span_time'  => 0,
            'planned_paid_rest_time'    => 0,
            'real_paid_rest_time'    => 0,
            'planned_unpaid_rest_time'  => 0,
            'real_unpaid_rest_time'  => 0,
            'planned_non_work_time' => 0,
            'real_non_work_time' => 0,
            'planned_overtime_work_time'    => 0,
            'real_overtime_work_time'    => 0,
            'planned_night_work_time'   => 0,
            'real_night_work_time'   => 0,
            'planned_is_kekkin' => false,
            'real_is_kekkin' => false,
            'planned_taken_paid_rest_days' => 0,
            'planned_taken_paid_rest_time' => 0,
            'real_taken_paid_rest_days' => 0,
            'real_taken_paid_rest_time' => 0,
            'employee_working_informations' => [],
            'working_timestamps' => [],
            'houde_kyuude_sinsei_note' => [],
        ];
    }

    /**
     * Output of an EmployeeWorkingInformation.
     *
     * @param EmployeeWorkingInformation        $employee_working_information
     * @return array
     */
    protected function employeeWorkingInformationOutputData($employee_working_information)
    {
        $data = [];

        // Update 2019/03/26: add work_time (勤務時間)
        $data['work_time'] = $employee_working_information->work_time;
        $data['schedule_start_work_time'] = $employee_working_information->schedule_start_work_time;
        $data['schedule_end_work_time'] = $employee_working_information->schedule_end_work_time;
        $data['schedule_break_time'] = $employee_working_information->schedule_break_time;
        $data['schedule_night_break_time'] = $employee_working_information->schedule_night_break_time;
        $data['schedule_working_hour'] = $employee_working_information->schedule_working_hour;
        $data['planned_work_location'] = isset($employee_working_information->planned_work_location_id) ? $this->work_location_names[$employee_working_information->planned_work_location_id] : null;
        $data['planned_work_location_presentation_id'] = isset($employee_working_information->planned_work_location_id) ? $this->work_location_presentation_ids[$employee_working_information->planned_work_location_id] : null;
        $data['real_work_location'] = isset($employee_working_information->real_work_location_id) ? $this->work_location_names[$employee_working_information->real_work_location_id] : null;
        $data['real_work_location_presentation_id'] = isset($employee_working_information->real_work_location_id) ? $this->work_location_presentation_ids[$employee_working_information->real_work_location_id] : null;
        $data['planned_work_status_id'] = $employee_working_information->planned_work_status_id;
        $data['planned_work_status'] = isset($employee_working_information->planned_work_status_id) ? $this->company_work_statuses[$employee_working_information->planned_work_status_id] : null;
        $data['planned_rest_status_id'] = $employee_working_information->planned_rest_status_id;
        $data['planned_rest_status'] = isset($employee_working_information->planned_rest_status_id) ? $this->company_rest_statuses[$employee_working_information->planned_rest_status_id]['name'] : null;
        $data['paid_rest_time_start'] = $employee_working_information->paid_rest_time_start;
        $data['paid_rest_time_end'] = $employee_working_information->paid_rest_time_end;
        $data['paid_rest_time_period'] = $employee_working_information->paid_rest_time_period;
        $data['not_include_break_time_when_display_planned_time'] = $employee_working_information->not_include_break_time_when_display_planned_time;
        $data['planned_start_work_time'] = $employee_working_information->planned_start_work_time;
        $data['real_start_work_time'] = $employee_working_information->real_start_work_time;
        $data['timestamped_start_work_time'] = $employee_working_information->timestamped_start_work_time;
        $data['planned_end_work_time'] = $employee_working_information->planned_end_work_time;
        $data['real_end_work_time'] = $employee_working_information->real_end_work_time;
        $data['timestamped_end_work_time'] = $employee_working_information->timestamped_end_work_time;
        $data['planned_working_hour'] = $employee_working_information->planned_working_hour;
        $data['real_working_hour'] = $employee_working_information->real_working_hour;
        $data['note'] = $employee_working_information->note;
        $data['planned_early_arrive_start'] = $employee_working_information->planned_early_arrive_start;
        $data['planned_early_arrive_end'] = $employee_working_information->planned_early_arrive_end;
        $data['real_early_arrive_start'] = $employee_working_information->real_early_arrive_start;
        $data['real_early_arrive_end'] = $employee_working_information->real_early_arrive_end;
        $data['planned_late_time'] = $employee_working_information->planned_late_time;
        $data['real_late_time'] = $employee_working_information->real_late_time;
        $data['planned_work_span_start'] = $employee_working_information->planned_work_span_start;
        $data['planned_work_span_end'] = $employee_working_information->planned_work_span_end;
        $data['planned_work_span'] = $employee_working_information->planned_work_span;
        $data['real_work_span_start'] = $employee_working_information->real_work_span_start;
        $data['real_work_span_end'] = $employee_working_information->real_work_span_end;
        $data['real_work_span'] = $employee_working_information->real_work_span;
        $data['planned_break_time'] = $employee_working_information->planned_break_time;
        $data['real_break_time'] = $employee_working_information->real_break_time;
        $data['planned_night_break_time'] = $employee_working_information->planned_night_break_time;
        $data['real_night_break_time'] = $employee_working_information->real_night_break_time;
        $data['planned_go_out_time'] = $employee_working_information->planned_go_out_time;
        $data['real_go_out_time'] = $employee_working_information->real_go_out_time;
        $data['planned_early_leave_time'] = $employee_working_information->planned_early_leave_time;
        $data['real_early_leave_time'] = $employee_working_information->real_early_leave_time;
        $data['planned_overtime_start'] = $employee_working_information->planned_overtime_start;
        $data['planned_overtime_end'] = $employee_working_information->planned_overtime_end;
        $data['real_overtime_start'] = $employee_working_information->real_overtime_start;
        $data['real_overtime_end'] = $employee_working_information->real_overtime_end;
        $data['basic_salary'] = $employee_working_information->basic_salary;
        $data['night_salary'] = $employee_working_information->night_salary;
        $data['overtime_salary'] = $employee_working_information->overtime_salary;
        $data['deduction_salary'] = $employee_working_information->deduction_salary;
        $data['night_deduction_salary'] = $employee_working_information->night_deduction_salary;
        $data['monthly_traffic_expense'] = $employee_working_information->monthly_traffic_expense;
        $data['daily_traffic_expense'] = $employee_working_information->daily_traffic_expense;
        $data['summary_schedule_working_hour'] = $employee_working_information->summaryScheduleWorkingHour();
        $data['summary_planned_working_hour'] = $employee_working_information->summaryPlannedWorkingHour();
        $data['summary_real_working_hour'] = $employee_working_information->summaryRealWorkingHour();
        $data['summary_planned_work_span'] = $employee_working_information->summaryPlannedWorkSpan();
        $data['summary_real_work_span'] = $employee_working_information->summaryRealWorkSpan();
        $data['summary_planned_paid_rest_time'] = $employee_working_information->summaryPlannedPaidRestTime();
        $data['summary_real_paid_rest_time'] = $employee_working_information->summaryRealPaidRestTime();
        $data['summary_planned_unpaid_rest_time'] = $employee_working_information->summaryPlannedUnpaidRestTime();
        $data['summary_real_unpaid_rest_time'] = $employee_working_information->summaryRealUnpaidRestTime();
        $data['summary_planned_non_work_time'] = $employee_working_information->summaryPlannedNonWorkTime();
        $data['summary_real_non_work_time'] = $employee_working_information->summaryRealNonWorkTime();
        $data['summary_planned_overtime'] = $employee_working_information->summaryPlannedOvertimeWorkTime();
        $data['summary_real_overtime'] = $employee_working_information->summaryRealOvertimeWorkTime();
        $data['summary_planned_night_work_time'] = $employee_working_information->summaryPlannedNightWorkTime();
        $data['summary_real_night_work_time'] = $employee_working_information->summaryRealNightWorkTime();
        $data['last_modified_person_name'] = $employee_working_information->last_modify_person_name;
        $data['last_modified_person_type'] = $employee_working_information->last_modify_person_type;

        $snapshot = $employee_working_information->employeeWorkingInformationSnapshot;
        if ($snapshot) {
            $data['sinsei_note'] = [
                'status' => $snapshot->left_status,
                'requester_note' => $snapshot->left_requester_note ? $snapshot->left_requester_note : ($snapshot->right_requester_note ? $snapshot->right_requester_note : null),
                'approver_note' => $snapshot->left_approver_note ? $snapshot->left_approver_note : ($snapshot->right_approver_note ? $snapshot->right_approver_note : null),
            ];
        } else {
            $data['sinsei_note'] = null;
        }

        return $data;
    }

    /**
     * Extract the WorkingTimestamp from a EmployeeWorkingDay and expose the necessary information.
     *
     * @param EmployeeWorkingDay    $working_day
     * @return array
     */
    protected function extractWorkingTimestamps(EmployeeWorkingDay $working_day)
    {
        return $working_day->workingTimestamps->map(function($timestamp) {
            return [
                'enable' => $timestamp->enable,
                'registered_date_time' => $timestamp->created_at->format('Y-m-d H:i:s'),
                'timestamped_date' => $timestamp->processed_date_value,
                'timestamped_time' => $timestamp->processed_time_value,
                'registerer_name' => $timestamp->registerer_name,
                'registerer_type' => $timestamp->registerer_type,
                'registerer_id' => $timestamp->registerer_id,
                'work_location' => $timestamp->place_name,
                'type' => $timestamp->timestamped_type,
            ];
        })->toArray();
    }

    /**
     * Get the string of the week day
     *
     * @param string    $data_string
     * @return string
     */
    protected function getTheWeekDayString($date_string)
    {
        $carbon = new Carbon($date_string);
        return $this->week_day_strings[$carbon->dayOfWeek];
    }

    /**
     * Get the calendar rest day setting
     *
     * @param string        $date_string
     * @param string        $work_location_presentation_id
     * @return integer|null
     */
    protected function getCalendarRestDaySetting($date_string, $work_location_presentation_id)
    {

        $rest_days = $this->work_location_settings[$work_location_presentation_id]['calendar']['rest_days'];
        $carbon = new Carbon($date_string);
        $day_key = $carbon->format('Y-n-j');

        if (array_key_exists($day_key, $rest_days)) {
            return $rest_days[$day_key]['type'] != 0 ? $rest_days[$day_key]['type'] : null;
        } else {
            return null;
        }
    }

    /**
     * 時間：分⇒分への変換
     */
	public function fnm_60hexToMinute($_60hex){
		if($_60hex === 0 || $_60hex === "" || !$_60hex){

			return 0;

		}

		$ex = explode(":", $_60hex);

		$hour = (int)$ex[0] * 60;
		$minu = (int)$ex[1];


		$minute = $hour + $minu;

		return $minute;
	}
}
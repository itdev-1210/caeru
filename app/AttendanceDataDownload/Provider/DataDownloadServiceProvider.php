<?php

namespace App\AttendanceDataDownload\Provider;

use Illuminate\Support\ServiceProvider;
use App\AttendanceDataDownload\DataDownloadService;


class DataDownloadServiceProvider extends ServiceProvider
{
    
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DataDownloadService::class, function ($app) {
            return new DataDownloadService();
        });
    }
}

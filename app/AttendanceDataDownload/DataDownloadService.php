<?php

namespace App\AttendanceDataDownload;

use League\Csv\CannotInsertRecord;
use League\Csv\Writer;
use Carbon\Carbon;

class DataDownloadService
{
    /**
     * Path to Attendace data processing files.
     */
    protected $attendance_process_file_path;

    /**
     * Path to Summary data processing files.
     */
    protected $summary_process_file_path;

    /**
     * Initialize the two paths to processing files.
     */
    public function __construct()
    {
        $this->attendance_process_file_path = dirname(__FILE__) . '/Attendance/';
        $this->summary_process_file_path = dirname(__FILE__) . '/Summary/';
    }

    /**
     * Download Attendance Data.
     * Use the company code to determine which process file to use.
     *
     * @param array         $conditions
     * @param array         $work_location_ids
     * @param string        $company_code
     * @return string       $file_name      Name of the result file, waiting to be download.
     */
    public function downloadAttendanceData($conditions, $work_location_ids, $company_code)
    {
        $class_name = $company_code . 'Attendance';
        $calculating_file = $this->attendance_process_file_path . $class_name . '.php';

        if (file_exists($calculating_file)) {
            include_once($calculating_file);
            $service = new $class_name($company_code);

        } else {
            $service = new DataDownloadFactory($company_code);
        }

        $data = $service->downloadAttendanceData($conditions, $work_location_ids);

        $file_name = $company_code . Carbon::now()->timestamp;

        $file_opener = fopen($file_name, 'w+');
        fwrite($file_opener, Writer::BOM_UTF8);
        fclose($file_opener);
        $writer = Writer::createFromPath($file_name, 'a+');
        $writer->insertAll($data);

        return $file_name;
    }

    /**
     * Download the Attendance Data Download process file of a specific company
     * If that company still doesn't have a customized Attendance Data Download process file, then it will download a
     * default template file.
     *
     * @param string    $company_code
     * @return string   first element is the file name, second element is a boolean to show if this file is already exists(true) or just newly created(false)
     */
    public function downloadCurrentAttendanceCalculatingFile($company_code)
    {
        $class_name = $company_code . 'Attendance';
        $calculating_file = $this->attendance_process_file_path . $class_name . '.php';

        if (file_exists($calculating_file)) {
            return [$calculating_file, true];
        } else {
            return [$this->createAttendanceCalculatingFileTemplate($company_code), false];
        }
    }

    /**
     * Create an default template of a Attendance Data Download process file. The content of this file
     * should be containing a class with a dynamic name base on the $company_code, and inside the class, there should be one method 'processAttendance'
     * with the exact same content as the DataDownloadFactory's one.
     *
     * @param string    $company_code
     * @return string   the name of the newly created template file
     */
    public function createAttendanceCalculatingFileTemplate($company_code)
    {
        $factory_reflection = new \ReflectionClass(DataDownloadFactory::class);

        // Get the code body of data preparation method
        $preparation_method_reflection = $factory_reflection->getMethod('downloadAttendanceData');
        $preparation_method_code_body = $this->extractMethodCodeFromClassReflection($preparation_method_reflection);

        // Get the code body of process method
        $process_method_reflection = $factory_reflection->getMethod('processAttendance');
        $process_method_code_body = $this->extractMethodCodeFromClassReflection($process_method_reflection);

        // Create the other parts of the template
        $class_name = $company_code . 'Attendance';
        $file_name = $class_name . '.php';
        $file_header = "<?php\n\nuse App\AttendanceDataDownload\DataDownloadFactory as Factory;\n\nclass {$class_name} extends Factory\n{\n";
        $file_footer = "}";

        // Create the template file
        $template_file = file_put_contents($file_name, $file_header . $preparation_method_code_body . "\n\n" . $process_method_code_body . $file_footer);

        if ($template_file === false) {
            throw new \Exception('Cannot create file!!');
        } else {
            return $file_name;
        }
    }

    /**
     * Download Summary Data.
     * Use the company code to determine which process file to use.
     *
     * @param array             $conditions
     * @param array             $work_location_ids
     * @param   string          $company_code
     * @return  string          $file_name      Name of the result file, waiting to be download.
     */
    public function downloadSummaryData($conditions, $work_location_ids, $company_code)
    {
        $class_name = $company_code . 'Summary';
        $calculating_file = $this->summary_process_file_path . $class_name . '.php';

        if (file_exists($calculating_file)) {
            include_once($calculating_file);
            $service = new $class_name($company_code);

        } else {
            $service = new DataDownloadFactory($company_code);
        }

        $data = $service->downloadSummaryData($conditions, $work_location_ids);

        $file_name = $company_code . Carbon::now()->timestamp;

        $file_opener = fopen($file_name, 'w+');
        fwrite($file_opener, Writer::BOM_UTF8);
        fclose($file_opener);
        $writer = Writer::createFromPath($file_name, 'a+');
        $writer->insertAll($data);

        return $file_name;
    }

    /**
     * Download the Summary Data Download process file of a specific company
     * If that company still doesn't have a customized Summary Data Download process file, then it will download a
     * default template file.
     *
     * @param string    $company_code
     * @return array    first element is the file name, second element is a boolean to show if this file is already exists(true) or just newly created(false)
     */
    public function downloadCurrentSummaryCalculatingFile($company_code)
    {
        $class_name = $company_code . 'Summary';
        $calculating_file = $this->summary_process_file_path . $class_name . '.php';

        if (file_exists($calculating_file)) {
            return [$calculating_file, true];
        } else {
            return [$this->createSummaryCalculatingFileTemplate($company_code), false];
        }
    }

    /**
     * Create an default template of a Summary Data Download process file. The content of this file
     * should be containing a class with a dynamic name base on the $company_code, and inside the class, there should be one method 'processSummary'
     * with the exact same content as the DataDownloadFactory's one.
     *
     * @param string    $company_code
     * @return string   the name of the newly created template file
     */
    public function createSummaryCalculatingFileTemplate($company_code)
    {
        $factory_reflection = new \ReflectionClass(DataDownloadFactory::class);

        // Get the code body of data preparation method
        $preparation_method_reflection = $factory_reflection->getMethod('downloadSummaryData');
        $preparation_method_code_body = $this->extractMethodCodeFromClassReflection($preparation_method_reflection);

        // Get the code body of process method
        $process_method_reflection = $factory_reflection->getMethod('processSummary');
        $process_method_code_body = $this->extractMethodCodeFromClassReflection($process_method_reflection);

        // Create the other parts of the template
        $class_name = $company_code . 'Summary';
        $file_name = $class_name . '.php';
        $file_header = "<?php\n\nuse App\AttendanceDataDownload\DataDownloadFactory as Factory;\n\nclass {$class_name} extends Factory\n{\n";
        $file_footer = "}";

        // Create the template file
        $template_file = file_put_contents($file_name, $file_header . $preparation_method_code_body . "\n\n" . $process_method_code_body . $file_footer);

        if ($template_file === false) {
            throw new \Exception('Cannot create file!!');
        } else {
            return $file_name;
        }
    }

    /**
     * Extract a method's code and document comment from a ClassReflection
     *
     * @param \ReflectionMethod     $method_reflection
     * @return string
     */
    protected function extractMethodCodeFromClassReflection($method_reflection)
    {
        // Extract the method's code and document
        $file_array = file($method_reflection->getFileName());
        $start_line = $method_reflection->getStartLine() - 1;
        $end_line = $method_reflection->getEndLine();
        $length = $end_line - $start_line;
        $method_document = "    " . $method_reflection->getDocComment() . "\n";
        $method_code_body = $method_document . implode("", array_slice($file_array, $start_line, $length));

        return $method_code_body;
    }
}
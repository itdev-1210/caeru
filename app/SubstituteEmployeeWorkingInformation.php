<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Reusables\HaveColorStatusTrait;

class SubstituteEmployeeWorkingInformation extends Model
{
    use HaveColorStatusTrait;

    /**
     * Get all the color statuses for this substitute instance
     */
    public function colorStatuses()
    {
        return $this->morphMany(ColorStatus::class, 'colorable');
    }

    /**
     * Get the working day instance of this substitute instance
     */
    public function employeeWorkingDay()
    {
        return $this->belongsTo(EmployeeWorkingDay::class);
    }
}

<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;

class CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld
{
    use Dispatchable;

    /**
     * Collection of employee ids
     */
    public $employee_ids;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($employee_ids)
    {
        $this->employee_ids = $employee_ids;
    }

}
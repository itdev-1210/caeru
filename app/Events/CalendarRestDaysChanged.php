<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;

class CalendarRestDaysChanged
{
    use Dispatchable;

    /**
     * The ids of work location(s). It can either be a single id or
     * all the ids of all the work locations of current company.
     */
    public $work_location_ids;

    /**
     * The changed month, it can be Y-m-01.
     */
    public $changed_month;

    public $changed_days;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->work_location_ids = $data['work_location_ids'];
        $this->changed_month = $data['changed_month'];
        $this->changed_days = $data['changed_days'];
    }

}
<?php

namespace App\Events;


use Illuminate\Foundation\Events\Dispatchable;

class CachedEmployeeWorkingInformationBecomeOld
{
    use Dispatchable;

    public $employee_working_info_ids;
    public $all;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ids, $all = false)
    {
        $this->employee_working_info_ids = $ids;
        $this->all = $all;
    }

}
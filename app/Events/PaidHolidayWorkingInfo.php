<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Employee;
use App\PaidHolidayInformation;

class PaidHolidayWorkingInfo
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $work_location_id;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($work_location_id)
    {
       $this->work_location_id = $work_location_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

}

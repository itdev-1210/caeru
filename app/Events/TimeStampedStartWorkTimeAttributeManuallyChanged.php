<?php

namespace App\Events;

use App\EmployeeWorkingInformation;
use Illuminate\Foundation\Events\Dispatchable;

class TimeStampedStartWorkTimeAttributeManuallyChanged
{
    use Dispatchable;

    /**
     * EmployeeWorkingInformation instance
     */
    public $working_info;

    /**
     * Boolean: blank color or not
     */
    public $blank_color;

    /**
     * Boolean: blue color or not(meaning: 'this timestamp was created from a sinsei request being approved or not')
     */
    public $blue_color;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EmployeeWorkingInformation $employee_working_info, $blank_color = false, $approved = false)
    {
        $this->working_info = $employee_working_info;
        $this->blank_color = $blank_color;
        $this->blue_color = $approved;
    }
}
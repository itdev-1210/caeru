<?php

namespace App\Events;

use App\EmployeeWorkingDay;
use Illuminate\Foundation\Events\Dispatchable;

class CachedPaidHolidayInformationBecomeOld
{
    use Dispatchable;

    /**
     * The EmployeeWorkingDay instance
     */
    public $working_day;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EmployeeWorkingDay $working_day)
    {
        $this->working_day = $working_day;
    }

}
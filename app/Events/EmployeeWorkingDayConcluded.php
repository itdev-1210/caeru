<?php

namespace App\Events;

use App\Manager;
use App\EmployeeWorkingDay;
use Illuminate\Foundation\Events\Dispatchable;

class EmployeeWorkingDayConcluded
{
    use Dispatchable;

    /**
     * EmployeeWorkingDay instance
     */
    public $working_day;

    /**
     * The manager who conclude
     */
    public $manager;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EmployeeWorkingDay $employee_working_day, Manager $manager)
    {
        $this->working_day = $employee_working_day;
        $this->manager = $manager;
    }
}

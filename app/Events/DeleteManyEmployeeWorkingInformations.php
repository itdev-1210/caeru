<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;

class DeleteManyEmployeeWorkingInformations
{
    use Dispatchable;

    /**
     * Id list of EmployeeWorkingInformations
     */
    public $employee_working_information_ids;

    /**
     * Id list of employee that is related to the employee working information above
     */
    public $employee_ids;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($employee_working_information_ids, $employee_ids)
    {
        $this->employee_working_information_ids = $employee_working_information_ids;
        $this->employee_ids = $employee_ids;
    }

}

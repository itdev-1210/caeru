<?php

namespace App\Events;

use App\Employee;
use Illuminate\Foundation\Events\Dispatchable;

class EmployeeWorkingMonthUnconcluded
{
    use Dispatchable;

    /**
     * Employee     the employee
     */
    public $employee;
    
    /**
     * Carbon       the start_date
     */
    public $start_date;
    
    /**
     * Carbon       the end_date
     */
    public $end_date;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Employee $employee, $start_date, $end_date)
    {
        $this->employee = $employee;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }
}

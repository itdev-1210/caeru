<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;

class CachedPaidHolidayInformationsBecomeOld
{
    use Dispatchable;

    /**
     * The collection of EmployeeWorkingDay instances. These instances must be consecutive and belong to one employee
     */
    public $working_days;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($working_days)
    {
        $this->working_days = $working_days;
    }

}
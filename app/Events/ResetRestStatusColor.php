<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;

class ResetRestStatusColor
{
    use Dispatchable;

    /**
     * The EmployeeWorkingInformation that's going to have work_status color reseted.
     */
    public $employee_working_info;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($working_info)
    {
        $this->employee_working_info = $working_info;
    }

}
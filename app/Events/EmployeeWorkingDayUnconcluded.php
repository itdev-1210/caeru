<?php

namespace App\Events;

use App\EmployeeWorkingDay;
use Illuminate\Foundation\Events\Dispatchable;

class EmployeeWorkingDayUnconcluded
{
    use Dispatchable;

    /**
     * EmployeeWorkingDay instance
     */
    public $working_day;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EmployeeWorkingDay $employee_working_day)
    {
        $this->working_day = $employee_working_day;
    }
}
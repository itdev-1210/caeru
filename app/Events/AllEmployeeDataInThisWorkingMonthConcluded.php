<?php

namespace App\Events;

use App\Manager;
use Illuminate\Foundation\Events\Dispatchable;

class AllEmployeeDataInThisWorkingMonthConcluded
{
    use Dispatchable;
    
    /**
     * Employees    all the involve employees
     */
    public $employees;

    /**
     * Manager     the manager who concluded
     */
    public $manager;

    /**
     * Carbon       the start_date
     */
    public $start_date;
    
    /**
     * Carbon       the end_date
     */
    public $end_date;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($employees, $start_date, $end_date, Manager $manager)
    {
        $this->employees = $employees;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->manager = $manager;
    }
}

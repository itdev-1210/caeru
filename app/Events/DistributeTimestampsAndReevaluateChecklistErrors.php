<?php

namespace App\Events;


use Illuminate\Foundation\Events\Dispatchable;

class DistributeTimestampsAndReevaluateChecklistErrors
{
    use Dispatchable;

    public $employee_working_day_ids;

    /**
     * In the case of this event being fired from a console command, there won't be a company_code in the session,
     * so we need to provide the event with a reserved company code.
     */
    public $reserved_company_code;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ids, $company_code = null)
    {
        $this->employee_working_day_ids = $ids;

        $this->reserved_company_code = $company_code;
    }

}
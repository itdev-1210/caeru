<?php

namespace App\Events;

use App\EmployeeWorkingInformation;
use Illuminate\Foundation\Events\Dispatchable;

class EmployeeWorkingInformationRestStatusChanged
{
    use Dispatchable;

    /**
     * The EmployeeWorkingDay instance
     */
    public $working_day;

    /**
     * The original EmployeeWorkingInformation instance
     */
    public $working_info;

    /**
     * The new rest status id
     */
    public $new_rest_status_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EmployeeWorkingInformation $working_info, $new_rest_status_id)
    {
        $this->working_day = $working_info->employeeWorkingDay;
        $this->working_info = $working_info;
        $this->new_rest_status_id = $new_rest_status_id;
    }

}
<?php

namespace App\Events;


use Illuminate\Foundation\Events\Dispatchable;

class ReevaluateChecklistErrors
{
    use Dispatchable;

    public $employee_working_info_ids;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ids)
    {
        $this->employee_working_info_ids = $ids;
    }

}
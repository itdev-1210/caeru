<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use App\EmployeeWorkingDay;

class WorkingTimestampChangedOnManyWorkingDays
{
    use Dispatchable;

    // The ids of EmployeeWorkingDays
    public $working_day_ids;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($working_day_ids)
    {
        $this->working_day_ids = $working_day_ids;
    }

}

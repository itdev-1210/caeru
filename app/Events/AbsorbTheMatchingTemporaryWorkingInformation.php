<?php

namespace App\Events;

use App\EmployeeWorkingInformation;
use Illuminate\Foundation\Events\Dispatchable;

class AbsorbTheMatchingTemporaryWorkingInformation
{
    use Dispatchable;

    /**
     * EmployeeWorkingInformation instance
     */
    public $working_info;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EmployeeWorkingInformation $employee_working_info)
    {
        $this->working_info = $employee_working_info;
    }
}
<?php

namespace App\Events;


use Illuminate\Foundation\Events\Dispatchable;

class EvaluateChecklistErrorsForEWIFromPlannedSchedule
{
    use Dispatchable;

    public $planned_schedule_id;
    public $through_WAWE;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($id, $through_work_address_working_employee = false)
    {
        $this->planned_schedule_id = $id;
        $this->through_WAWE = $through_work_address_working_employee;
    }

}
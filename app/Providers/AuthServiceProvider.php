<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Register all the gates

        // Company Information Page's gates
        Gate::define('change_company_info', 'App\Policies\AuthorityPolicy@changeCompanyInformation');
        Gate::define('view_company_info', 'App\Policies\AuthorityPolicy@viewCompanyInformation');

        // Manager pages's gate
        Gate::define('change_manager_info', 'App\Policies\AuthorityPolicy@changeManagerInformation');

        // Work Location Pages's gates
        Gate::define('change_work_location_info', 'App\Policies\AuthorityPolicy@changeWorkLocationInformation');
        Gate::define('view_work_location_info', 'App\Policies\AuthorityPolicy@viewWorkLocationInformation');

        // Work Address Pages's gates
        Gate::define('change_work_address_info', 'App\Policies\AuthorityPolicy@changeWorkAddressInformation');
        Gate::define('view_work_address_info', 'App\Policies\AuthorityPolicy@viewWorkAddressInformation');
        Gate::define('change_work_address_s_work_location', 'App\Policies\AuthorityPolicy@changeWorkAddressWorkLocationInformation');

        // Employee Pages's gates
        Gate::define('change_employee_basic_info', 'App\Policies\AuthorityPolicy@changeEmployeeBasicInformation');
        Gate::define('view_employee_basic_info', 'App\Policies\AuthorityPolicy@viewEmployeeBasicInformation');
        Gate::define('see_employee_tab', 'App\Policies\AuthorityPolicy@seeEmployeeTabInNavigationBar');
        Gate::define('change_employee_s_work_location', 'App\Policies\AuthorityPolicy@changeEmployeeWorkLocationInformation');
        Gate::define('change_employee_work_info', 'App\Policies\AuthorityPolicy@changeEmployeeWorkInformation');
        Gate::define('view_employee_work_info', 'App\Policies\AuthorityPolicy@viewEmployeeWorkInformation');
        Gate::define('change_employee_maebarai_info', 'App\Policies\AuthorityPolicy@changeEmployeeMaebaraiInformation');
        Gate::define('view_employee_maebarai_info', 'App\Policies\AuthorityPolicy@viewEmployeeMaebaraiInformation');

        // Paid holiday information
        Gate::define('view_work_data_paid_holiday_management','App\Policies\AuthorityPolicy@viewPaidHolidayInformationManagement');
        Gate::define('change_work_data_paid_holiday_management','App\Policies\AuthorityPolicy@changePaidHolidayInformationManagement');
        Gate::define('view_work_data_paid_holiday_detail','App\Policies\AuthorityPolicy@viewPaidHolidayInformationDetail');
        Gate::define('change_work_data_paid_holiday_detail','App\Policies\AuthorityPolicy@changePaidHolidayInformationDetail');

        // Calendar Pages's gates
        Gate::define('view_calendar', 'App\Policies\AuthorityPolicy@viewCalendarInformation');
        Gate::define('change_calendar', 'App\Policies\AuthorityPolicy@changeCalendarInformation');

        // Setting Pages's gates
        Gate::define('change_setting_info', 'App\Policies\AuthorityPolicy@changeSettingInformation');
        Gate::define('view_setting_info', 'App\Policies\AuthorityPolicy@viewSettingInformation');

        // Option Pages's gates
        Gate::define('change_option_info', 'App\Policies\AuthorityPolicy@changeStatusesSettingInformation');
        Gate::define('view_option_info', 'App\Policies\AuthorityPolicy@viewStatusesSettingInformation');
        Gate::define('change_department_info', 'App\Policies\AuthorityPolicy@changeDepartmentTypeSettingInformation');
        Gate::define('view_department_info', 'App\Policies\AuthorityPolicy@viewDepartmentTypeSettingInformation');
        Gate::define('change_worktime_info', 'App\Policies\AuthorityPolicy@changeWorkTimeSettingInformation');
        Gate::define('view_worktime_info', 'App\Policies\AuthorityPolicy@viewWorkTimeSettingInformation');
        Gate::define('view_option_item_info', 'App\Policies\AuthorityPolicy@viewOptionItemInformation');

        // Attendance Management Page's gate
        Gate::define('view_attendance_management_page', 'App\Policies\AuthorityPolicy@viewAttendanceManagementPage');

        // Attendance Advance Search Page's gate
        Gate::define('view_attendance_advance_search_page', 'App\Policies\AuthorityPolicy@viewAttendanceAdvanceSearchPage');

        // Attendance Data Calculation or Month Summary page's gate
        Gate::define('view_attendance_month_summary_page','App\Policies\AuthorityPolicy@viewAttendanceMonthSummaryPage');

        // Attendance Employee's Working Month Page's gate
        Gate::define('view_attendance_employee_working_month_page','App\Policies\AuthorityPolicy@viewAttendanceEmployeeWorkingMonthPage');

        // Attendance Employee's pages's gates
        Gate::define('view_attendance_employee_working_day', 'App\Policies\AuthorityPolicy@viewAttendanceEmployeeWorkingDayPage');
        Gate::define('change_attendance_data', 'App\Policies\AuthorityPolicy@changeAttendanceData');
        Gate::define('approve_modify_request', 'App\Policies\AuthorityPolicy@approveWorkDataModifyRequest');
        Gate::define('conclude_level_one', 'App\Policies\AuthorityPolicy@concludeWorkingInforLevelOne');
        Gate::define('conclude_level_two', 'App\Policies\AuthorityPolicy@concludeWorkingInforLevelTwo');

        // Maebarai Page's gate
        Gate::define('view_maebarai_page','App\Policies\AuthorityPolicy@viewMaebaraiPage');
        Gate::define('view_maebarai_setting_page','App\Policies\AuthorityPolicy@viewMaebaraiSettingPage');
        Gate::define('change_maebarai_setting_page','App\Policies\AuthorityPolicy@changeMaebaraiSettingPage');
        Gate::define('view_maebarai_information_page','App\Policies\AuthorityPolicy@viewMaebaraiInformationPage');

        // WorkAddress Attendance Page's gate (WorkAddress attendance table Page)
        Gate::define('view_work_address_attendance_page', 'App\Policies\AuthorityPolicy@viewWorkAddressAttendancePage');

        // WorkAddress working month Page's gates
        Gate::define('change_attendance_work_address_working_month_page', 'App\Policies\AuthorityPolicy@changeAttendanceWorkAddressWorkingMonthPage');
        Gate::define('view_attendance_work_address_working_month_page', 'App\Policies\AuthorityPolicy@viewAttendanceWorkAddressWorkingMonthPage');

        // WorkAddress working day Page's gates
        Gate::define('change_attendance_work_address_working_day_page', 'App\Policies\AuthorityPolicy@changeAttendanceWorkAddressWorkingDayPage');
        Gate::define('view_attendance_work_address_working_day_page', 'App\Policies\AuthorityPolicy@viewAttendanceWorkAddressWorkingDayPage');
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ChangeWorkTimePerDayService;

class ChangeWorkTimePerDayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ChangeWorkTimePerDayService::class, function ($app) {
            return new ChangeWorkTimePerDayService();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            ChangeWorkTimePerDayService::class,
        ];
    }
}

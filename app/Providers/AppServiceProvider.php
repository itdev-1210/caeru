<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\WorkLocation;
use App\Employee;
use App\PlannedSchedule;
use App\WorkingTimestamp;
use App\EmployeeWorkingInformation;
use App\WorkAddressWorkingInformation;
use App\WorkAddressWorkingEmployee;
use App\EmployeeWorkingInformationSnapshot;
use App\SubstituteEmployeeWorkingInformation;
use App\Observers\AddViewOrderNumberObserver;
use App\Observers\EmployeeObserver;
use App\Observers\WorkingTimestampObserver;
use App\Observers\PlannedScheduleObserver;
use App\Observers\EmployeeWorkingInformationObserver;
use App\Observers\WorkAddressWorkingInformationObserver;
use App\Observers\WorkAddressWorkingEmployeeObserver;
use App\Observers\EmployeeWorkingInformationSnapshotObserver;
use App\Observers\SubstituteEmployeeWorkingInformationObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(env('REDIRECT_HTTPS') == true) {
            $this->app['request']->server->set('HTTPS', true);
        }

        // Register the model to the AddViewOrderNumberObserver to use the order function
        WorkLocation::observe(AddViewOrderNumberObserver::class);
        Employee::observe(AddViewOrderNumberObserver::class);
        Employee::observe(EmployeeObserver::class);

        // Working Information related events
        PlannedSchedule::observe(PlannedScheduleObserver::class);

        // Some other observer
        EmployeeWorkingInformation::observe(EmployeeWorkingInformationObserver::class);
        WorkAddressWorkingInformation::observe(WorkAddressWorkingInformationObserver::class);
        WorkAddressWorkingEmployee::observe(WorkAddressWorkingEmployeeObserver::class);
        EmployeeWorkingInformationSnapshot::observe(EmployeeWorkingInformationSnapshotObserver::class);
        SubstituteEmployeeWorkingInformation::observe(SubstituteEmployeeWorkingInformationObserver::class);

        // Custom Validation Rules
        Validator::extend('year', '\App\Http\Requests\Reusables\ExtraValidations@year');
        Validator::extend('rest_day', '\App\Http\Requests\Reusables\ExtraValidations@restDay');
        Validator::extend('work_time', '\App\Http\Requests\Reusables\ExtraValidations@workTime');
        Validator::extend('time', '\App\Http\Requests\Reusables\ExtraValidations@time');
        Validator::extend('working_hour', '\App\Http\Requests\Reusables\ExtraValidations@workingHour');
        Validator::extend('furigana', '\App\Http\Requests\Reusables\ExtraValidations@furigana');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        \App\Events\PaidHolidayChanged::class => [
            \App\Listeners\ProcessPaidHolidayChangedListener::class,
        ],

        \App\Events\PaidHolidayWorkingInfo::class => [
            \App\Listeners\ProcessPaidHolidayKomokuChangedListeners::class,
        ],

        \App\Events\AbsorbTheMatchingTemporaryWorkingInformation::class => [
            \App\Listeners\AbsorbTheMatchingTemporaryWorkingInformationListener::class,
        ],

        \App\Events\CalendarRestDaysChanged::class => [
            \App\Listeners\CalendarRestDaysChangedListener::class,
        ],

        \App\Events\DeleteManyEmployeeWorkingInformations::class => [
            \App\Listeners\DeleteManyEmployeeWorkingInformationsListener::class,
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        'App\Listeners\ManagerEventSubscriber',
        'App\Listeners\RegenerateSearchHistoryAndSaveSubscriber',
        'App\Listeners\WorkingTimestampSubscriber',
        'App\Listeners\ChecklistErrorSubscriber',
        'App\Listeners\ConcludedSubscriber',
        'App\Listeners\EmployeeWorkingInformationCacheSubscriber',
        'App\Listeners\TimestampedAttributesManuallyChangedSubscriber',
        'App\Listeners\EmployeeWorkingInformationRestOrWorkStatusChangedSubscriber',
        'App\Listeners\EvaluateChecklistErrorsEventSubscriber',
        'App\Listeners\PaidHolidayInformationCacheSubscriber',
        'App\Listeners\ResetColorOfWorkStatusAndRestStatusSubscriber',
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

<?php

namespace App;
use App\Scopes\EnableScope;

class ColorStatus extends Model
{

    /**
     * Constants of CSS classes for colors
     * Yeah, there are some english errors. It's just honest mistakes, let's not make a big deal out of it.
     */
    const ERROR_COLOR = "bg_mistake";
    const CONSIDERING_COLOR = "bg_apply";
    const APPROVED_COLOR = "bg_approval";
    const REJECTED_COLOR = "bg_reject";
    const UPDATED_COLOR = "bg_revised";
    const BLANK_COLOR = "bg_blank";

    /**
     * Constants for colorable object types
     */
    const EMPLOYEE_WORKING_INFORMATION = EmployeeWorkingInformation::class;
    const EMPLOYEE_WORKING_INFORMATION_SNAPSHOT = EmployeeWorkingInformationSnapshot::class;
    const SUBSTITUTE_EMPLOYEE_WORKING_INFORMATION = SubstituteEmployeeWorkingInformation::class;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new EnableScope);
    }

    /**
     * Scope a query to only include disable color statuses.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDisable($query)
    {
        return $query->withoutGlobalScope(EnableScope::class)->where('enable', false);
    }

    /**
     * Get the owning model that can have color status.
     */
    public function colorable()
    {
        return $this->morphTo();
    }
}

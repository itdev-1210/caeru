<?php

namespace App;

class CachedEmployeeWorkingInformation extends Model
{
    /**
     * Get the employee working information instance of this cached instance
     */
    public function employeeWorkingInformation()
    {
        return $this->belongsTo(EmployeeWorkingInformation::class);
    }
}

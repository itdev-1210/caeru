<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaebaraiHistory extends Model
{
    /**
     * Constants for Fee
     */
    const SEVEN_BANK_FEE  = 50;
    const OTHER_BANK_FEE  = 200;
    const API_FEE   = 100;

    /**
     * Constants for BANK code
     */
    const SEVEN_BANK_CODE = "0034";

    /**
     * Constants for Transaction Status
     */
    const TRANSACTION_STATUS_SUCCESS    = 0;
    const TRANSACTION_STATUS_SENDING    = 1;
    const TRANSACTION_STATUS_FAIL       = 2;
    const TRANSACTION_API_FAIL          = 3;
    // use SoftDeletes;

    /**
     * Get the work location of this employee
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

}

<?php

namespace App\Console\Commands;

use App\EmployeeWorkingInformation;
use Illuminate\Console\Command;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use DB;

class RefreshOldEmployeeWorkingInformationCachedData extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_cache:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh all old EmployeeWorkingInformation cached data of all companies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = DB::table('companies')->get();

        foreach ($companies as $company) {

            $this->changeDatabaseConnectionTo($company->company_code);

            $need_to_be_refreshed_employee_working_infos = EmployeeWorkingInformation::whereHas('cachedEmployeeWorkingInformation', function ($query) {
                $query->where('old', true);
            })->orDoesntHave('cachedEmployeeWorkingInformation')->get();

            foreach($need_to_be_refreshed_employee_working_infos as $working_info) {
                $working_info->calculateAndCacheAttributes();
            }
        }
    }
}

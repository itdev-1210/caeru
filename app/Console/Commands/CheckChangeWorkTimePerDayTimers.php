<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ChangeWorkTimePerDayTimer;
use App\Employee;
use App\Services\ChangeWorkTimePerDayService;
use Carbon\Carbon;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;

class CheckChangeWorkTimePerDayTimers extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_timer:work-time-per-day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if there is any ChangeWorkTimePerDayTimer in due time to actually take effect';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();

        $timers = ChangeWorkTimePerDayTimer::where('due_day', $today->toDateString())->get();
        $service = resolve(ChangeWorkTimePerDayService::class);

        foreach ($timers as $timer) {
            
            // Change to the database of this timer record's company
            $this->changeDatabaseConnectionTo($timer->company_code);

            $employee = Employee::find($timer->employee_id);

            if ($employee) {
                $service->changeWorkTimePerDayOfThisEmployee($employee, $timer->work_time_per_day);
    
                // Reset the employee's timer stat back to null
                $employee->work_time_change_date = null;
                $employee->work_time_change_to = null;
                $employee->save();

                // Remove the timer
                $timer->delete();
            }
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CachedEmployeeWorkingInformation;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use DB;

class CleanUpEmployeeWorkingInformationCachedData extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_cache:clean-up';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean up the cached data of removed EmployeeWorkingInformation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = DB::table('companies')->get();

        foreach ($companies as $company) {

            $this->changeDatabaseConnectionTo($company->company_code);

            CachedEmployeeWorkingInformation::doesntHave('employeeWorkingInformation')->delete();

        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;

class RetrieveAndUpdateNationalHoliday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holiday:get {year?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the national holidays of a given year.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $year = $this->argument('year');

        if (!$year) {
            $year = $this->ask("Which year's national holidays do you want to get?");
        }

        $start_date = $year . '-01-01T00:00:00.000Z';
        $end_date = $year . '-12-31T00:00:00.000Z';

        $url = 'https://www.googleapis.com/calendar/v3/calendars/japanese__ja@holiday.calendar.google.com/events?key=' .
            config('google.api.google_api_key') .
            '&timeMin=' . urlencode($start_date) .
            '&timeMax=' . urlencode($end_date) .
            '&maxResults=999&orderBy=startTime&singleEvents=true'
        ;

        if( $results = file_get_contents($url, true)) {
            $results = json_decode($results);

            $holidays = [];

            foreach($results->items as $holiday) {
                $holidays[] = [
                    'name' => $holiday->summary,
                    'date' => $holiday->start->date,
                ];
            }

            $this->info('Retrieved result from google calendar API(' . count($holidays) . ' days):');
            $this->humanFriendlyDisplay($holidays);

            if ($this->confirm('Do you wish to save these national holidays to main database?')) {
                DB::table('national_holidays')->insert($holidays);
            }

            $this->info('Complete!');
        } else {
            $this->error('Error! Can not get response from google calendar API.');
        }
    }

    /**
     * Display the array of national holidays in a human friendly way.
     *
     * @param   array   $holidays
     * @return  void
     */
    protected function humanFriendlyDisplay($holidays)
    {
        foreach($holidays as $holiday) {
            $this->info(" {$holiday['date']}: \"{$holiday['name']}\".");
        }
    }
}

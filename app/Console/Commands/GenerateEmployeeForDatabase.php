<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;

class GenerateEmployeeForDatabase extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_db:generate_employee {company_code} {work_location_id} {employee_number}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command arguments: {company_code} {work_location_id} {employee_number}. Generate employees for the specific WorkLocation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $company_code = $this->argument('company_code');
        $work_location_id = $this->argument('work_location_id');
        $employee_number = $this->argument('employee_number');

        if ($company_code && $work_location_id && $employee_number) {
            $this->changeDatabaseConnectionTo($company_code);

            $employees = factory(\App\Employee::class, intval($employee_number))->create(['work_location_id' => $work_location_id]);
        }

        $this->info('Finish generate {$employee_number} employees for WorkLocation {$work_location_id} of company {$company_code} !');
    }
}

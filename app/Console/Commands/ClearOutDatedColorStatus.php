<?php

namespace App\Console\Commands;

use App\Employee;
use App\CachedPaidHolidayInformation;
use Illuminate\Console\Command;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use DB;

class ClearOutDatedColorStatus extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_clear:outdated_color_statuses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all outdated color statuses.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = DB::table('companies')->get();

        foreach ($companies as $company) {

            $this->changeDatabaseConnectionTo($company->company_code);

            $db_table_prefix = DB::getTablePrefix();

            // Kinda ugly but we have no choice T.T
            $data = DB::table('color_statuses AS ori')->select('ori.id', 'ori.colorable_type', 'ori.colorable_id', 'ori.field_name', 'ori.field_css_class', 'ori.field_fake_value', 'ori.enable', 'ori.created_at')
                    ->join(DB::raw('(SELECT *, count(*) AS dupNo FROM ' . $db_table_prefix . 'color_statuses GROUP BY
                            `colorable_type`, `colorable_id`, `field_name`, `field_css_class`, `field_fake_value`, `enable`
                        HAVING dupNo > 1) AS ' . $db_table_prefix . 'dup'), function($join) {
                        $join->on('ori.colorable_type', '=', 'dup.colorable_type');
                        $join->on('ori.colorable_id', '=', 'dup.colorable_id');
                        $join->on('ori.field_name', '=', 'dup.field_name');
                        $join->on('ori.field_css_class', '=', 'dup.field_css_class');
                        $join->on('ori.field_fake_value', '<=>', 'dup.field_fake_value');
                        $join->on('ori.enable', '=', 'dup.enable');
                    })
                    ->orderBy('ori.colorable_type')
                    ->orderBy('ori.colorable_id')
                    ->orderBy('ori.field_name')
                    ->orderBy('ori.field_css_class')
                    ->orderBy('ori.field_fake_value')
                    ->orderBy('ori.created_at', 'DESC')
                    ->get();

            $to_be_deleted_color_status_ids = collect([]);
            $newest_records = collect([]);

            // $small_chunk = $data->chunk(100)[0];
            foreach ($data as $record) {
                $unique_key = $record->colorable_type . '-' . $record->colorable_id . '-' . $record->field_name . '-' . $record->field_css_class . '-' . $record->field_fake_value . '-' . $record->enable;

                if (!$newest_records->has($unique_key)) {
                    $newest_records->put($unique_key, $record->id);
                } else {
                    $to_be_deleted_color_status_ids->push($record->id);
                }
            }

            DB::beginTransaction();
            DB::table('color_statuses')->whereIn('id', $to_be_deleted_color_status_ids)->delete();
            DB::commit();
        }
    }
}


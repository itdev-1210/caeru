<?php

namespace App\Console\Commands;

use App\Employee;
use App\PaidHolidayInformation;
use League\Csv\Reader;
use League\Csv\CharsetConverter;
use Illuminate\Console\Command;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use Carbon\Carbon;
use DB;

class ImportPaidHoliday extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The location to put the initial import files.
     */
    protected $data_file_location = 'database/paid_holiday_import/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_db:import_paid_holiday {company_code} {--f|file= : The data file (a .csv file).}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Import paid_holiday data into a company's database.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $company_code = $this->argument('company_code');
        $migration_folder = "";

        if ($company_code) {

            $this->info('Importing to database of company: ' . $company_code . '...');

            if (!$migration_folder = $this->changeDatabaseConnectionTo($company_code)) {
                $this->info('Sorry, the database: ' . $company_code . ' does not exists!');
                return;
            }

            // Get the file name
            $data_file = $this->option('file');

            if ($data_file) {
                $file_path = $this->data_file_location . $data_file;
                $reader = Reader::createFromPath($file_path, 'r');

                // Have header or not? If yes set the correct index of the header, comment out otherwise.
                $reader->setHeaderOffset(0);

                // ** Change these attributes according to the attributes in the csv file.
                $import_attribute = [
                    'employee_presentation_id',
                    'employee_name',
                    'attendance_rate',
                    'provided_paid_holidays',
                    'carried_forward_paid_holidays',
                    'carried_forward_paid_holidays_hour',
                ];

                $raw_data = $reader->getRecords($import_attribute);

                // Need to convert encoding or not? If yes set the correct encoding, comment out otherwise.
                $encoder = (new CharsetConverter())->inputEncoding('SJIS-win');
                $raw_data = $encoder->convert($raw_data);

                // Initialize some data
                $all_employees = Employee::all()->keyBy('presentation_id');

                DB::beginTransaction();
                $all_new_paid_holidays = [];
                try {
                    foreach ($raw_data as $line) {
                        $employee_presentation_id = $line['employee_presentation_id'];
                        if (!$employee_presentation_id) {
                            throw new \Exception('This line does not have employee presentation id.');
                        }

                        $employee = $all_employees[$employee_presentation_id];
                        if (!$employee) {
                            throw new \Exception('This presentation id does not exist: ' . $employee_presentation_id);
                        }

                        $holiday_update_date = $employee->holidays_update_day;
                        if (!$holiday_update_date) {
                            throw new \Exception('This presentation id does not exist: ' . $employee_presentation_id);
                        }
                        $holiday_update_date = explode('/', $holiday_update_date);

                        $today = Carbon::today();
                        $nearest_paid_holiday = $today->copy()->month($holiday_update_date[0])->day($holiday_update_date[1]);
                        if ($nearest_paid_holiday->gt($today)) {
                            $nearest_paid_holiday->subYear();
                        }

                        $new_paid_holiday = [
                            'employee_id' => $employee->id,
                            'period_start' => $nearest_paid_holiday->toDateString(),
                            'period_end' => $nearest_paid_holiday->copy()->addYear()->subDay()->toDateString(),
                            'work_time_per_day' => $employee->work_time_per_day * 60,
                            'attendance_rate' => $line['attendance_rate'],
                            'provided_paid_holidays' => $line['provided_paid_holidays'],
                            'carried_forward_paid_holidays' => $line['carried_forward_paid_holidays'],
                            'carried_forward_paid_holidays_hour' => $line['carried_forward_paid_holidays_hour'],
                            'created_at' => (Carbon::now())->toDateTimeString(),
                            'updated_at' => (Carbon::now())->toDateTimeString(),
                        ];

                        $all_new_paid_holidays[] = $new_paid_holiday;
                    }

                    // dd($all_new_paid_holidays);
                    DB::table('paid_holiday_informations')->insert($all_new_paid_holidays);

                } catch (\Exception $e) {
                    $this->info($e->getMessage());
                    DB::rollBack();
                    return;
                }

                DB::commit();
                $this->info("Insert paid holiday successfully!");
                return;

            } else {
                $this->error("You didn't specify the input data file. It should be a .csv file.");
                return;
            }

        } else {
            $this->error("You didn't specify a company. This command can't be run with main database.");
            return;
        }
    }
}

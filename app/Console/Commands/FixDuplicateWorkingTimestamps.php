<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\WorkingTimestamp;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use Carbon\Carbon;
use Artisan;

class FixDuplicateWorkingTimestamps extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_fix:duplicate_timestamps {company_code} {check_range_start?} {check_range_end?} {--F|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Check for duplicate timestamps in a database. Please provide {company_code} {check_range_start} {check_range_end} after the command(example: abc yyyy-mm-dd yyyy-mm-dd).";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** First, definition: Distinction of a WorkingTimestamp is the unique of the set (timestamped_value, employee_working_day_id, enable) */

        $company_code = $this->argument('company_code');

        if ($company_code) {
            $migration_folder = $this->changeDatabaseConnectionTo($company_code);

            $check_range_start = $this->argument('check_range_start');
            $check_range_start = ($check_range_start) ? new Carbon($check_range_start) : (Carbon::today())->subDays(3);
            $check_range_start->hour(0)->minute(0)->second(0);

            $check_range_end = $this->argument('check_range_end');
            $check_range_end = ($check_range_end) ? new Carbon($check_range_end) : Carbon::today();
            $check_range_end->hour(23)->minute(59)->second(59);

            // $this->info('company code:' . $company_code);
            // $this->info('start:' . $check_range_start->format('Y-m-d H:i:s'));
            // $this->info('end:' . $check_range_end->format('Y-m-d H:i:s'));
            $all_working_timestamp_in_range = WorkingTimestamp::whereNotNull('timestamped_value')->where('timestamped_value', '>=', $check_range_start->timestamp)->where('timestamped_value', '<=', $check_range_end->timestamp)->orderBy('timestamped_value')->orderBy('employee_working_day_id')->orderBy('created_at')->get();

            $distinct_set_of_working_timestamp = collect([]);
            $distinct_working_timestamp = collect([]);
            $timestamp_to_id_lookup_table = collect([]);

            $duplicated_working_timestamps_by_timestamp_value = collect([]);
            $duplicated_working_timestamps_by_id = collect([]);

            foreach ($all_working_timestamp_in_range as $working_timestamp) {
                $unique_key = $working_timestamp->timestamped_value . '_' . $working_timestamp->employee_working_day_id . '_' . $working_timestamp->enable;

                // Sort all working timestamps into distinct sets
                if ($distinct_set_of_working_timestamp->has($unique_key)) {
                    $distinct_set_of_working_timestamp[$unique_key]->push($working_timestamp);
                } else {
                    $distinct_set_of_working_timestamp->put($unique_key, collect([]));
                }

                // Instead of sorting into distinct sets, this will only take the first value of duplicated ones (effectively create ONE set contain all distinct timestamps)
                if (!$distinct_working_timestamp->has($unique_key)) {
                    $distinct_working_timestamp->put($unique_key, [
                        'id' => $working_timestamp->id,
                        'timestamped_value' => $working_timestamp->timestamped_value,
                        // 'employee_working_day_id' => $working_timestamp->employee_working_day_id,
                        // 'created_at' => $working_timestamp->created_at->format('Y-m-d H:i:s'),
                        // 'equivalent time' => (Carbon::createFromTimestamp($working_timestamp->timestamped_value))->format('Y-m-d H:i:s'),
                    ]);

                    $timestamp_to_id_lookup_table[$working_timestamp->timestamped_value] = $working_timestamp->id;
                } else {
                    $duplicated_working_timestamps_by_id->push($working_timestamp->id);

                    if (!$duplicated_working_timestamps_by_timestamp_value->has($working_timestamp->timestamped_value))
                        $duplicated_working_timestamps_by_timestamp_value->put($working_timestamp->timestamped_value, $working_timestamp->id);
                }
            }



            // $this->info('all: ' . $all_working_timestamp_in_range->count());
            // $this->info('distinct with timestamped_value and ewdi: ' . $distinct_working_timestamp->count());
            // $this->info('distinct with timestamped_value and ewdi: ' . $distinct_set_of_working_timestamp->count());
            // foreach ($distinct_set_of_working_timestamp as $unique_key => $distinct_set) {
            //     if ($distinct_set->count() > 1)
            //         $this->info('Duplicated set of: ' . $unique_key . ' has number of dupplicates of: ' . $distinct_set->count());
            // }

            /** 
             * Report & Execute purging duplicated WorkingTimestamp.
             * YOU HAVE TO USE THE SWITCH --force (-F) when using the command to excecute purging. If you don't use it,
             * The command still generate reports but WILL NOT purge any duplicated WorkingTimestamp (databases are unchanged).
             */

            // Report files prefix. Default is company_code_timestamp_
             $report_files_prefix = $company_code . '_' . (Carbon::today())->timestamp . '_';

            // Duplication WorkingTimestamps information
            $duplicated_timestamps_detail = fopen(base_path('storage/logs/' . $report_files_prefix . 'duplicated_details'), 'w+');
            fwrite($duplicated_timestamps_detail, 'Total duplicated WorkingTimestamp: ' . $distinct_set_of_working_timestamp->count() . "\n");

            foreach ($distinct_set_of_working_timestamp as $unique_key => $distinct_set) {
                if ($distinct_set->count() > 1)
                fwrite($duplicated_timestamps_detail, '    Duplicated set of: ' . $unique_key . ' has number of dupplicates of: ' . $distinct_set->count() . "\n");
            }
            fclose($duplicated_timestamps_detail);

            // Ids of duplicated WorkingTimestamps that WILL BE DELETED IF THE FORCE OPTION IS ON AND WILL NOT BE DELETED OTHERWISE.
            $to_be_deleted_ids = fopen(base_path('storage/logs/' . $report_files_prefix . 'to_be_deleted_working_timestamp_ids'), 'w+');
            fwrite($to_be_deleted_ids, "Total to be deleted WorkingTimestamp ids: " . $duplicated_working_timestamps_by_id->count() . "\n");
            fwrite($to_be_deleted_ids, "IDs: \n" . $duplicated_working_timestamps_by_id->implode(",\n") . '.');
            fclose($to_be_deleted_ids);

            // Execute Purging
            if ($this->option('force')) {
                WorkingTimestamp::whereIn('id', $duplicated_working_timestamps_by_id->toArray())->delete();
            }

            $this->info('Command completed.');
            
        } else {

            $this->info('Please do it again and provide company_code.');
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use Artisan;
use DB;

class UpdateCaeruDatabase extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The root folder of migration files
     */
    protected $migration_folder = "/database/migrations/update/";

    /**
     * The default place holder migration file for the update.
     */
    protected $migration_file_name = "update_sub_dbs";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_db:update {company_code=all : the code of a specific company that you want to update. Default is all companies.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the databases(add columns, change type, add tablesm, etc.). You can use the from option to point to the folder that contain the migration file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $company_code = $this->argument('company_code');

        if ($company_code == 'all') {
            $companies = DB::table('companies')->get();

            $this->info("Start migrating all sub databases!");

            foreach ($companies as $company) {
                $this->changeDatabaseConnectionTo($company->company_code);

                $migrations = DB::table('migrations')->where('migration', 'like', "%{$this->migration_file_name}%")->delete();

                Artisan::call('migrate', [
                    '--path' => $this->migration_folder,
                ]);

                $this->info("Migrate database of {$company->company_code} sucessfully!");
            }

            $this->info("Complete!");

        } else if ($company_code) {
            $this->changeDatabaseConnectionTo($company_code);

            $migrations = DB::table('migrations')->where('migration', 'like', "%{$this->migration_file_name}%")->delete();

            Artisan::call('migrate', [
                '--path' => $this->migration_folder,
            ]);

            $this->info("Migrate database of {$company_code} sucessfully!");

        } else {
            $this->error('Unknow company code');
        }
    }
}

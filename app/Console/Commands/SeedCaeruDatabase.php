<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use Artisan;

class SeedCaeruDatabase extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "caeru_db:seed {company_code=main}
                            {--develop : Use developer mode (use the DeveloperDatabaseSeeder class)}
                            {--attendance : Use a specific seeder class (either use 'class' option or 'develop' option, not both)}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed data into databases of caeru project. It can be the main database or sub (client) database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $company_code = $this->argument('company_code');
        $develop_mode = $this->option('develop');
        $attendance_mode = $this->option('attendance');
        $migration_folder = "";

        if ($company_code === 'main') {

            $this->info('Seeding main database of Caeru project...');

            $this->changeDatabaseConnectionTo();

            Artisan::call('db:seed', [
                '--class' => 'MainDatabaseSeeder'
            ]);

            $this->info('Seeding complete sucessfully!');

            return;

        } else {

            $this->info('Seeding database of company: ' . $company_code . '...');

            if ($this->changeDatabaseConnectionTo($company_code) === false) {
                $this->info('Sorry, the database: ' . $company_code . ' does not exists!');
                return;
            }

        }

        if ($develop_mode) {

            Artisan::call('db:seed', [
                '--class' => 'DevelopDatabaseSeeder',
            ]);

        } else if ($attendance_mode) {

            Artisan::call('db:seed', [
                '--class' => 'AttendanceDataSeeder',
            ]);

        } else {

            Artisan::call('db:seed');
            $this->info("ATTENTION: remember to override the the company code in the company table of this company's database. Or esle, it will be defaulted to 'itz'.");
        }

        $this->info('Seeding complete sucessfully!');
    }
}

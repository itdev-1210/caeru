<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Employee;
use App\ChangeEmployeeLocationTimer;
use Carbon\Carbon;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;

class EmployeeLocationChangeTimers extends Command
{
    use DatabaseRelatedTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_timer:employee-location-change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if there is any Employee Location Change in due time to actually take effect';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();
        $timers = ChangeEmployeeLocationTimer::where('change_date', $today->toDateString())->get();
        foreach ($timers as $timer) {
            // Change to the database of this timer record's company
            $this->changeDatabaseConnectionTo($timer->company_code);

            $employee = Employee::find($timer->employee_id);
            if ($employee) {
                $employee->work_location_id = $employee->new_work_location_id;
                $employee->change_date = null;
                $employee->new_work_location_id = null;
                $employee->save();
            }
            $timer->delete();
        }
    }
}

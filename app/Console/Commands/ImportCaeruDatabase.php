<?php

namespace App\Console\Commands;

use App\Company;
use App\WorkLocation;
use App\Employee;
use League\Csv\Reader;
use League\Csv\CharsetConverter;
use Illuminate\Console\Command;
use App\Services\TodofukenService;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use App\Http\Controllers\Reusables\GenerateNumberTrait;
use Constants;
use Config;
use DB;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;

class ImportCaeruDatabase extends Command
{
    use DatabaseRelatedTrait, GenerateNumberTrait;

    /**
     * The location to put the initial import files.
     */
    protected $data_file_location = 'database/init_import/';


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "caeru_db:import {company_code=main}
                                {--w|work_locations= : Import work_locations information from a csv file.}
                                {--e|employees= : Import employees information from a csv file.}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Import work locations or employees information into a company's database.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $company_code = $this->argument('company_code');
        $migration_folder = "";

        if ($company_code === 'main') {

            $this->info('This command need a company code. Please provide the code of the company, of which database you want to import data to.');

        } else {

            $this->info('Importing to database of company: ' . $company_code . '...');

            if (!$migration_folder = $this->changeDatabaseConnectionTo($company_code)) {
                $this->info('Sorry, the database: ' . $company_code . ' does not exists!');
                return;
            }

            // Initialize some variables
            $work_locations_data_file = $this->option('work_locations');
            $employees_data_file = $this->option('employees');
            $todofuken_service = resolve(TodofukenService::class);
            $company = Company::first();
            $departments_list = $company->departments->pluck('id', 'name');
            $work_locations_list = $company->workLocations->pluck('id', 'presentation_id');
            $schedule_types_list = array_flip(Constants::scheduleTypes());

            // This part is for providing variations of the name of some constants, due to humanly mistakes.
            // This is a common mistake. The text in the constants is '1ヶ月単位変形労働'(the number 1),
            // but users usually use '一ヶ月単位変形労働'(the kanji '一').
            $schedule_types_list['一ヶ月単位変形労働'] = Config::get('constants.monthly_based_schedule');
            // And sometimes they use the full width 1 instead of the half width １
            $schedule_types_list['1年単位変形労働'] = Config::get('constants.yearly_based_schedule');

            $employment_types_list = array_flip(Constants::employmentTypes());
            $salary_types_list = array_flip(Constants::salaryTypes());
            $work_statuses_list = array_flip(Constants::workStatuses());
            $holiday_bonus_types_list = array_flip(Constants::holidayBonusTypes());
            $new_departments = collect([]);
            $new_employees = collect([]);

            // ** Change these attributes according to the attributes in the csv file.
            $work_location_attributes = [
                'presentation_id',
                'name',
                'furigana',
                'enable',
                'postal_code',
                'todofuken',
                'address',
                'login_range',
                'latitude',
                'longitude',
                'telephone',
                'chief_last_name',
                'chief_first_name',
                'chief_last_name_furigana',
                'chief_first_name_furigana',
                'chief_email'
            ];

            // ** Change these attributes according to the attributes in the csv file.
            $employee_attributes = [
                'presentation_id',
                'password',
                'last_name',
                'first_name',
                'last_name_furigana',
                'first_name_furigana',
                'birthday',
                'gender',
                'postal_code',
                'todofuken',
                'address',
                'telephone',
                'email',
                'work_location_id',
                'joined_date',
                'department_id',
                'schedule_type',
                'employment_type',
                'salary_type',
                'work_status',
                'resigned_date',
                'chiefs',
                'paid_holiday_exception',
                'holidays_update_day',
                'work_time_per_day',
                'holiday_bonus_type',
                'all_timestamps_register_to_belong_work_location',
            ];

            if ($work_locations_data_file) {

                $file_path = $this->data_file_location . $work_locations_data_file;
                $reader = Reader::createFromPath($file_path, 'r');

                // Have header or not? If yes set the correct index of the header, comment out otherwise.
                $reader->setHeaderOffset(0);

                // Need to convert encoding or not? If yes set the correct encoding, comment out otherwise.
                $reader->addStreamFilter('convert.iconv.SJIS/UTF-8');

                $raw_work_locations = $reader->getRecords($work_location_attributes);

                foreach ($raw_work_locations as $work_location) {

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////// This is the part where you add the neccessary logic to process data from a csv file source. //////

                    // Only when the user specify the value 0 then this attribute will be false, it will be true otherwise.
                    $work_location['enable'] = $work_location['enable'] === 0 ? false : true;

                    // Assign id of todofuken
                    $todofuken_id = $todofuken_service->getId($work_location['todofuken']);
                    $work_location['todofuken'] = $todofuken_id ? $todofuken_id : null;

                    $work_location = $this->assignNullIfEmptyAndStandardizeOtherwise($work_location);
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////

                    /////////////////////////////////////////////////////////////////////////
                    ////// This is the part where you add the logic of the controller. //////

                    $new_work_location = new WorkLocation($work_location);
                    $new_work_location->company_id = $company->id;
                    $new_work_location->registration_number = $this->generateUniqueNumber(WorkLocation::class, 'registration_number');
                    /////////////////////////////////////////////////////////////////////////

                    // Save it, boys.
                    try {
                        $new_work_location->save();
                    } catch (QueryException $e) {
                        $this->error('Dupplicate entry. WorkLocation with presentation id: ' . $work_location['presentation_id'] . ' has already existed in the database.');
                    }
                }

                $this->info('Imported WorkLocations sucessfully!');

            } else if ($employees_data_file) {

                $file_path = $this->data_file_location . $employees_data_file;
                $reader = Reader::createFromPath($file_path, 'r');

                // Have header or not? If yes set the correct index of the header, comment out otherwise.
                $reader->setHeaderOffset(0);

                $raw_employees = $reader->getRecords($employee_attributes);

                // Need to convert encoding or not? If yes set the correct encoding, comment out otherwise.
                $encoder = (new CharsetConverter())->inputEncoding('SJIS-win');
                $raw_employees = $encoder->convert($raw_employees);

                $employee_chiefs_list = [];

                foreach ($raw_employees as $employee) {

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////// This is the part where you add the neccessary logic to process data from a csv file source. //////

                    try {
                        $employee['birthday'] = str_replace('/', '-', $employee['birthday']);

                        $employee['gender'] = $employee['gender'] == '男' ? Config::get('constants.male') : Config::get('constants.female');
                        if (strpos($employee['postal_code'], '-') !== false) $employee['postal_code'] = str_replace('-', '', $employee['postal_code']);

                        $todofuken_id = $todofuken_service->getId($employee['todofuken']);
                        $employee['todofuken'] = $todofuken_id ? $todofuken_id : null;

                        $employee['joined_date'] = str_replace('/', '-', $employee['joined_date']);

                        if (!$departments_list->has($employee['department_id'])) {
                            if (!$new_departments->contains($employee['department_id'])) {
                                $new_departments->push($employee['department_id']);
                            }
                        } else {
                            $employee['department_id'] = $employee['department_id'] ? $departments_list[$employee['department_id']] : null;
                        }

                        $employee['work_location_id'] = $work_locations_list[$employee['work_location_id']];

                        $employee['schedule_type'] = $employee['schedule_type'] ? $schedule_types_list[$employee['schedule_type']] : Config::get('constants.normal_schedule');
                        $employee['employment_type'] = $employee['employment_type'] ? $employment_types_list[$employee['employment_type']] : Config::get('constants.official_employee');
                        $employee['salary_type'] = $employee['salary_type'] ? $salary_types_list[$employee['salary_type']] : Config::get('constants.monthly_salary');
                        $employee['work_status'] = $employee['work_status'] ? $work_statuses_list[$employee['work_status']] : Config::get('constants.working');

                        $employee['resigned_date'] = str_replace('/', '-', $employee['resigned_date']);

                        $employee['paid_holiday_exception'] = $employee['paid_holiday_exception'] == 1 ? true : false;

                        // Default to 8. If the user use this type of string '7:45', then we need to convert it to float. For that, we can use the convertHourStringToRealNumber() function
                        $employee['work_time_per_day'] = $employee['work_time_per_day'] == '' ? 8 : (is_numeric($employee['work_time_per_day']) ? $employee['work_time_per_day'] : $this->convertHourStringToRealNumber($employee['work_time_per_day']));

                        $employee['holiday_bonus_type'] = $employee['holiday_bonus_type'] ? $holiday_bonus_types_list[$employee['holiday_bonus_type']] : Config::get('constants.normal_bonus');

                        $employee['all_timestamps_register_to_belong_work_location'] = $employee['all_timestamps_register_to_belong_work_location'] == 1 ? true : false;

                        // Get them chiefs and put in another array so that we can set the relation ship later after creating all the employee.
                        if ($employee['chiefs']) {
                            $employee_chiefs_list[$employee['presentation_id']] = explode('/', $employee['chiefs']);;
                        }
                        unset($employee['chiefs']);
                    } catch (\Exception $e) {
                        $this->error('Error! Can not import record for employee with presentation id: ' . $employee['presentation_id'] . '.');
                        $this->error('Script has been stopped at employee ' . $employee['presentation_id'] . "'s record. Please fix and retry.");
                        $this->error($e->getMessage());
                        die;
                    }

                    $employee = $this->assignNullIfEmptyAndStandardizeOtherwise($employee);
                    $employee['card_registration_number'] = $this->generateUniqueNumber(Employee::class, 'card_registration_number');
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////

                    // Add some necessary field to the records. If you choose the 'mass insert' method below, uncomment these.
                    // $employee['password'] = Hash::make($employee['password']);
                    // $employee['view_order'] = config('caeru.unused_view_order');
                    // $employee['created_at'] = (Carbon::now())->format('Y-m-d h:i:s');
                    // $employee['updated_at'] = (Carbon::now())->format('Y-m-d h:i:s');
                    //$employee['holidays_update_day'] = config('caeru.blind_year') . '/' . $employee['holidays_update_day'];


                    $new_employees->push($employee);
                }

                // 2019/03/22 Updated: Save the new departments first if any. And then save the employees
                if ($new_departments->isNotEmpty()) {
                    $new_departments = $new_departments->transform(function($new_department) use ($company){
                        return [
                            'company_id' => $company->id,
                            'name' => $new_department,
                        ];
                    });

                    DB::table('departments')->insert($new_departments->toArray());
                    $company = Company::first();
                    $new_departments_included_list = $company->departments->pluck('id', 'name')->toArray();
                    $new_employees = $new_employees->transform(function($new_employee) use ($new_departments_included_list) {
                        try {
                            $new_employee['department_id'] = isset($new_employee['department_id']) ? ($new_departments_included_list[$new_employee['department_id']] ?? $new_employee['department_id']) : null;
                        } catch (\Exception $e) {
                            $this->error('current department id: ' . $new_employee['department_id']);
                            dump($new_departments_included_list);
                            die;
                        }
                        return $new_employee;
                    });
                }

                // Choose one of the two method here:

                // 1.Mass insert. If you choose this make sure, each element of the new_employees array has necessary data.
                // For example: view_order, created_at, updated_at, etc. (In other words, uncomment those line)
                // DB::table('employees')->insert($new_employees->toArray());

                // 2.Loop and insert each employee through Eloquent. Use this method when the 'holiday_update_date' is null in the source data file.
                foreach ($new_employees as $employee) {
                    $new_employee = new Employee($employee);
                    try {
                        $new_employee->save();
                    } catch (QueryException $e) {
                        // $this->error('Dupplicate entry. Employee with presentation id: ' . $employee['presentation_id'] . ' has already existed in the database.');
                        $this->error($e->getMessage());
                    }
                }


                // Now we have to set the chief-subordinate relations ship
                $employees_list = Employee::all()->keyBy('presentation_id');

                foreach ($employee_chiefs_list as $employee_presentation_id => $chief_presentation_ids) {
                    $employee = $employees_list[$employee_presentation_id];
                    $chief_ids = [];
                    foreach ($chief_presentation_ids as $id) {
                        $chief_ids[] = $employees_list[$id]->id;
                    }

                    $employee->chiefs()->sync($chief_ids);
                }

                $this->info('Imported Employees sucessfully!');

            } else {
                $this->info('Finished, but nothing was imported. You need to specify a work_locations/employees data file.');
            }
        }
    }

    /**
     * All the attributes which have empty value("") will be assign null.
     * If the attribute has value, we will standardize the value. See the middleware CaeruStandardizeStringInput for more details.
     *
     * @param array
     * @return array
     */
    private function assignNullIfEmptyAndStandardizeOtherwise($attributes)
    {
        // The value of these keys will be ignored
        $exception_keys = collect([
            'department_id',
        ]);

        foreach ($attributes as $key => $value) {
            if (is_string($value) && $value === "")
                $attributes[$key] = null;
            else if (is_string($value) && !$exception_keys->contains($key)) {
                // Convert zen-kaku numbers and alphabet and space to han-kaku (the normal ones)
                $value = mb_convert_kana($value, 'a');
                $value = mb_convert_kana($value, 's');

                // Then convert the han-kaku katakana to zen-kaku (the normal katakana )
                $value = mb_convert_kana($value, 'K');

                $attributes[$key] = $value;
            }
        }

        return $attributes;
    }

    /**
     * Convert a hour string 'hh:mm' into a conresponding real number.
     *
     * @param   string      $hour_string
     * @return  float       if the hour_string is inappropriate it will return 0 instead.
     */
    private function convertHourStringToRealNumber($hour_string)
    {
        $result = 0;
        $data = explode(':', $hour_string);

        if (count($data) == 2) {
            $hour = $data[0];
            $minute = $data[1];
            $minute = round($minute/60, 2);
            $result = $hour + $minute;
        }

        return $result;
    }
}

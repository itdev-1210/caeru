<?php

namespace App\Console\Commands;

use App\Employee;
use App\CachedPaidHolidayInformation;
use Illuminate\Console\Command;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use DB;

class ResetCachedPaidHolidayInformation extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_cache:paid_holiday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset all CachedPaidHolidayInformations records to "old".';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = DB::table('companies')->get();

        foreach ($companies as $company) {

            $this->changeDatabaseConnectionTo($company->company_code);

            $employees = Employee::with('paidHolidayInformations')->get();

            // Get the latest paid holiday info of each employee
            $all_latest_paid_holiday_infos = $employees->filter(function($employee) {
                return $employee->paidHolidayInformations->isNotEmpty();
            })->map(function($employee) {
                return $employee->paidHolidayInformations->sortBy('period_end')->last();
            });
            
            CachedPaidHolidayInformation::whereIn('id', $all_latest_paid_holiday_infos->pluck('id')->toArray())->update([
                'old' => 1
            ]);
        }
    }
}

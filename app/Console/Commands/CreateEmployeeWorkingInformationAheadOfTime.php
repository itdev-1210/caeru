<?php

namespace App\Console\Commands;

use App\Company;
use App\Employee;
use App\PlannedSchedule;
use Illuminate\Console\Command;
use App\Services\ScheduleProcessingService;
use App\Console\Commands\Reusables\DatabaseRelatedTrait;
use DB;
use Config;

class CreateEmployeeWorkingInformationAheadOfTime extends Command
{
    use DatabaseRelatedTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caeru_create:employee_working_information {company_code=all}
                                {--e|employee=all : create new EmployeeWorkingDay and new EmployeeWorkingInformation for specific employee. Default: all}
                                {--u|until=null : create new EmployeeWorkingDay and new EmployeeWorkingInformation until a specific date. Default: last day of the third month ahead}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new EmployeeWorkingDay and EmployeeWorkingInformation for employee(s) of a company if they have PlannedSchedule without an end_date.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $company_code = $this->argument('company_code');

        if ($company_code === 'all') {
            $companies = DB::table('companies')->get();

            foreach ($companies as $company) {
                $this->createEmployeeWorkingInformationAheadOfTimeForThisCompany($company->company_code);
                $this->info('start waiting to cool down');
                sleep(300);
                $this->info('stop waiting.');
            }

        } else {
            $this->createEmployeeWorkingInformationAheadOfTimeForThisCompany($company_code);
        }
    }

    /**
     * Create EmployeeWorkingDay and EmployeeWorkingInformation ahead of time for a company.
     * Why do we need to do this ? Because in some PlannedSchedule, the end_date will be null. That means that PlannedSchedule does not have a
     * specific end_date limit, it will have effect untill the end of time (or at least until the relating employee retire).
     *
     * @param string    $company_code
     * @return void
     */
    private function createEmployeeWorkingInformationAheadOfTimeForThisCompany($company_code)
    {
        $this->changeDatabaseConnectionTo($company_code);

        $company = Company::first();

        $employees = $this->option('employee') == 'all' ? $company->employees()->where('work_status', '!=', Config::get('constants.retired'))->get() :
                                                        Employee::where('id', $this->option('employee'))->where('work_status', '!=', Config::get('constants.retired'))->get();

        if ($employees->isEmpty()) {
            $this->error('Employee not found or there is no employee in company: ' . $company_code);

        } else {
            $not_have_end_date_planned_schedules = PlannedSchedule::with('employee')->whereIn('employee_id', $employees->pluck('id')->toArray())->whereNull('end_date')->get();
            $schedule_service = $service = resolve(ScheduleProcessingService::class);

            foreach ($not_have_end_date_planned_schedules as $schedule) {

                // Check the date period to create some new EmployeeWorkingDays
                $period = $schedule_service->getDatePeriod($schedule);

                // Most likely this branch of the if wont be touch, because the WorkAddress feature is still underdeveloped.
                if ($schedule->work_address_id) {
                    $work_address = $schedule->workAddress;
                    $schedule_service->initializeWorkingDay($work_address, $period);
                    $schedule_service->initializeWorkAddressWorkingInformationsMatchedTimeRange($schedule, null, null, $company_code);
                } else {
                    $employee = $schedule->employee;
                    $schedule_service->initializeWorkingDay($employee, $period);
                    $schedule_service->initializeEmployeeWorkingInformationsMatchedTimeRange($schedule, null, null, $company_code);
                }
            }

            $this->info('Complete create schedule for company: ' . $company_code);

        }
    }
}

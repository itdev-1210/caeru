<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        // caeru_db
        Commands\MigrateCaeruDatabase::class,
        Commands\SeedCaeruDatabase::class,
        Commands\ResetCaeruDatabase::class,
        Commands\RefreshCaeruDatabase::class,
        Commands\ImportCaeruDatabase::class,
        Commands\UpdateCaeruDatabase::class,
        Commands\ImportPaidHoliday::class,

        // caeru_error
        Commands\CheckForgotErrors::class,
        Commands\CheckHaveScheduleButOfflineError::class,

        // caeru_cache
        Commands\RefreshOldEmployeeWorkingInformationCachedData::class,
        Commands\CleanUpEmployeeWorkingInformationCachedData::class,
        Commands\ResetCachedPaidHolidayInformation::class,

        // caeru_timer
        Commands\CheckChangeWorkTimePerDayTimers::class,
        Commands\EmployeeLocationChangeTimers::class,

        // caeru_create
        Commands\CreateEmployeeWorkingInformationAheadOfTime::class,

        // caeru_clear
        Commands\ClearOutDatedColorStatus::class,

        // caeru_fix
        Commands\FixDuplicateWorkingTimestamps::class,

        // retrieve and update national holidays
        Commands\RetrieveAndUpdateNationalHoliday::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Check the Forgot end_work/return errors for every minute
        $schedule->command('caeru_error:check-forgot-errors')->everyMinute();

        // Check the HAVE_SCHEDULE_BUT_OFFLINE every day at 00:05:00
        $schedule->command('caeru_error:check-have-schedule-but-offline')->dailyAt('00:05');

        // Refresh the EmployeeWorkingInformation's Cache every 5 minutes
        // CONSUME RAM A LOT FOR SOME REASON. FOR NOW, STOP THIS COMMAND
        // $schedule->command('caeru_cache:refresh')->everyFiveMinutes();

        // Clean up EmployeeWorkingInformation's cached junk data (the original model get deleted) every day
        $schedule->command('caeru_cache:clean-up')->dailyAt('01:00');

        // Check the ChangeWorkTimePerDayTimer everyday
        $schedule->command('caeru_timer:work-time-per-day')->dailyAt('01:30');

        // Reset the cache of paid holiday informations every day.
        $schedule->command('caeru_cache:paid_holiday')->dailyAt('01:45');

        // Create new EmployeeWorkingDay and EmployeeWorkingInformation for PlannedSchedule without an end_date every month
        $schedule->command('caeru_create:employee_working_information')->monthlyOn(1, '03:00');

        // Clear up all outdated color statuses on all databases every day.
        $schedule->command('caeru_clear:outdated_color_statuses')->dailyAt('02:00');

        // Employee Location Change Timer
        $schedule->command('caeru_timer:employee-location-change')->dailyAt('00:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
        $this->load(__DIR__.'/Commands');
    }
}

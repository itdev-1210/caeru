<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Reusables\PasswordTrait;
use App\Reusables\TodofukenTrait;
use App\Reusables\BelongsToWorkLocationTrait;
use Constants;
use Carbon\Carbon;
use App\Reusables\PaidHolidayInformationTrait;
use DB;
use App\EmployeeWorkingInformationSnapshot;

class Employee extends Model
{
    use SoftDeletes, PasswordTrait, TodofukenTrait, BelongsToWorkLocationTrait, PaidHolidayInformationTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the work location of this employee
     */
    public function workLocation()
    {
        return $this->belongsTo(WorkLocation::class);
    }

    /**
     * Get the schedules of this employee
     */
    public function schedules()
    {
        return $this->hasMany(PlannedSchedule::class);
    }

    /**
     * Get the name of the department of this employee, if he/she has one
     */
    public function departmentName()
    {
        $department = null;
        if ($this->department_id) {
            $department = $this->workLocation->activatingDepartments()->find($this->department_id);
        }
        return $department ? $department->name : null;
    }

    /**
     * Get full name of this employee
     */
    public function fullName()
    {
        return $this->last_name . $this->first_name;
    }

    /**
     * Get string of schedule type of this employee
     */
    public function scheduleType()
    {
        return $this->schedule_type ? Constants::scheduleTypes()[$this->schedule_type] : null;
    }

    /**
     * Get string of employment type of this employee
     */
    public function employmentType()
    {
        return $this->employment_type ? Constants::employmentTypes()[$this->employment_type] : null;
    }

    /**
     * Get string of work status of this employee
     */
    public function workStatus()
    {
        return $this->work_status ? Constants::workStatuses()[$this->work_status] : null;
    }

    /**
     * Get Japanese string of total worked time of this employee
     */
    public function workedTimeString()
    {
        if ($this->joined_date) {
            $joined_day = new Carbon($this->joined_date);
            $today = Carbon::today();

            $worked_months = $joined_day->diffInMonths($today);

            return floor($worked_months/12) . '年' . ($worked_months%12) . 'ヶ月';
        }
        return null;
    }

    /**
     * Get Japanese string of paid holiday bonus type of this employee
     */
    public function paidHolidayBonusType()
    {
        return $this->holiday_bonus_type ? Constants::holidayBonusTypes()[$this->holiday_bonus_type] : null;
    }

    /**
     * Get all the subordinates of this employee, of who he/she can approve.
     */
    public function subordinates()
    {
        return $this->belongsToMany(Employee::class, 'employees_approval', 'chief_id', 'subordinate_id');
    }

    /**
     * Get all the chiefs who have the right to approve this employee
     */
    public function chiefs()
    {
        return $this->belongsToMany(Employee::class, 'employees_approval', 'subordinate_id', 'chief_id');
    }

    /**
     * Mutator for the holidays_update_day
     *
     * @param string $value
     * @return void
     */
    public function setHolidaysUpdateDayAttribute($value)
    {
        $this->attributes['holidays_update_day'] = $value ? config('caeru.blind_year') . '/' . $value : null;
    }

    /**
     * Accessor for the holidays_update_day
     *
     * @param string $value
     * @return void
     */
    public function getHolidaysUpdateDayAttribute($value)
    {

        return ($value) ? date('m/d', strtotime($value)) : null;

    }

    /**
     * Scope a query to get the working employee.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWorking($query)
    {
        return $query->where('work_status', config('constants.working'));
    }

    //// From here on are the relationships of stage II ////
    ////////////////////////////////////////////////////////

    /**
     * Get all the working day instances of this employee
     */
    public function employeeWorkingDays()
    {
        return $this->hasMany(EmployeeWorkingDay::class);
    }

    /**
     * Get all the paid holiday information instances of this employee
     */
    public function paidHolidayInformations()
    {
        return $this->hasMany(PaidHolidayInformation::class);
    }

    /**
     * Get all the checklist items of this employee
     */
    public function checklistItems()
    {
        return $this->hasMany(ChecklistItem::class);
    }


    /**
     * Convert date to Carbon
     *
     * @param string $value
     *
     * @return Carbon
     */
    public function convertPaidHolidayUpdateToCarbon($value)
    {
        Carbon::setToStringFormat('m-d');
        return new Carbon($value);
    }

    /**
     * Get all the request of this employee
     */
    public function employeeWorkingInformationModifyRequests()
    {
        return $this->hasMany(EmployeeWorkingInformationModifyRequest::class);
    }

   /**
    * Counts the number of item type.
    *
    * @param int $itemtype
    *
    * @return int Number of item type.
    */
    public function countItemType($itemtype, $beginDate, $endDate) 
    {
        return $this->checklistItems()->itemTypeWithDate($itemtype, $beginDate, $endDate)->count();
    }

    /**
     * Counts the number of request status.
     *
     * @param int $status
     *
     * @return int Number of request status.
     */
    public function countRequestStatus($status)
    {
        return $this->employeeWorkingInformationModifyRequests()->statusType($status)->count();
    }

    /**
     * Gets true or false conclude one for employee.
     *
     * @return boolean
     */
    public function hasConcludedOne($beginDate, $endDate)
    {

        return $this->employeeWorkingDays()->workingDay($beginDate, $endDate)->count() && !in_array(0, $this->employeeWorkingDays()->workingDay($beginDate, $endDate)->get(['concluded_level_one'])->pluck('concluded_level_one')->toArray());

    }

    /**
     * Get true or false conclude two for employee.
     * 
     * @return boolean
     */
    public function hasConcludedTwo($beginDate, $endDate) 
    {
         return $this->employeeWorkingDays()->workingDay($beginDate, $endDate)->count() && !in_array(0, $this->employeeWorkingDays()->workingDay($beginDate, $endDate)->get(['concluded_level_two'])->pluck('concluded_level_two')->toArray());
    }
    
    /**
     * Search base on request status
     *
     * @param array $status
     *
     * @return Builder
     */
    public function scopeSearchBaseRequestStatus($query, $status=[])
    {
        if(!count($status))
        {
            return $query;
        }
        if(in_array(1, $status)) {
            $query = $query
            ->whereExists(
                function($q) {
                    $q
                    ->from('employee_working_information_modify_requests')
                    ->whereRaw(\DB::getTablePrefix() . 'employees.id = ' . \DB::getTablePrefix() . 'employee_working_information_modify_requests.employee_id')
                    ->where('employee_working_information_modify_requests.request_status', 1)
                    ->groupBy('employee_working_information_modify_requests.employee_id');
                }
            );
        }
        if(in_array(2, $status)) {
            $query = $query
            ->whereExists(
                function($q) {
                    $q
                    ->from('employee_working_information_modify_requests')
                    ->whereRaw(\DB::getTablePrefix() . 'employees.id = ' . \DB::getTablePrefix() . 'employee_working_information_modify_requests.employee_id')
                    ->where('employee_working_information_modify_requests.request_status', 2)
                    ->groupBy('employee_working_information_modify_requests.employee_id');
                }
            );
        }
        return $query;
    }

    /**
     * Search base on checklist type
     *
     * @param array $itemtype
     *
     * @return Builder
     */
    public function scopeSearchBaseChecklistType($query, $itemtype=[])
    {
        if(!count($itemtype))
        {
            return $query;
        }
        if(in_array(100, $itemtype))
        {
            $query = $query
            ->whereExists(
                function($q) {
                    $q->from('checklist_items')
                    ->whereRaw(\DB::getTablePrefix(). 'employees.id = ' . \DB::getTablePrefix() .  'checklist_items.employee_id')
                    ->where('checklist_items.item_type', 100)
                    ->groupBy('checklist_items.employee_id');
                }
            );
        }
        if(in_array(200, $itemtype))
        {
             $query = $query
            ->whereExists(
                function($q) {
                    $q->from('checklist_items')
                    ->whereRaw(\DB::getTablePrefix(). 'employees.id = ' . \DB::getTablePrefix() .  'checklist_items.employee_id')
                    ->where('checklist_items.item_type', 200)
                    ->groupBy('checklist_items.employee_id');
                }
            );
        }
        return $query;
    }

    /**
     *
     * @param array $concludedleveltwo
     *
     * @return Builder
     */
    public function scopeSearchBaseHasNotConcludedLevelTwo ($query, $concludedleveltwo=[])
    {
        if(!count($concludedleveltwo)) 
        {
            return $query;
        }  
        if(in_array(500, $concludedleveltwo))
        {
            $query = $query
            ->whereExists(
                function($q) {
                    $q
                    ->from('employee_working_days')
                    ->whereRaw(\DB::getTablePrefix() . 'employees.id = ' . \DB::getTablePrefix() . 'employee_working_days.employee_id')
                    ->where('employee_working_days.concluded_level_two',0)
                    ->groupBy('employee_working_days.employee_id');
                }
            );
        }
        return $query;
    }

    /**
     * Compute worked time.
     *
     * @param Carbon $joined_date
     *
     * @return string.
     */
    public function getEmployeeJoinedDate($joined_date)
    {   
        $joined_date = new \Carbon\Carbon($joined_date);
   
        $result = '';
        Carbon::setLocale('ja');
        $currYear = Carbon::now()->year;
        $currMonth = Carbon::now()->month;
    
        if(Carbon::now()->gt($joined_date))
        {   
            $employeeDate = Carbon::create($joined_date->year,$joined_date->month,$joined_date->day);
            $month = Carbon::now()->diffForHumans($joined_date,true,false,2);
            $result =$month;
        }

        return $result;

    }
    /**
     * Calculates the period end.
     *
     */
    public function computePeriodOriginStart()
    {
        return (new \Carbon\Carbon($this->joined_date))->addMonth($this->workLocation->setting->paid_holiday_after_joined_period??Setting::whereNull('work_location_id')->first()->paid_holiday_after_joined_period);
    }

    /**
     * Determines if it has first koushin.
     *
     * @return boolean.
     */
    public function hasFirstKoushin()
    {   
        return $this->computePeriodOriginStart()->lt(Carbon::now());

    }

    /**
     * Determines if it has not first koushin.
     *
     * @return boolean.
     */
    public function hasNotFirstKoushin()
    {

        $period = $this->paidHolidayInformations()->orderBy('period_end','desc')->get()->first();
        if(empty($period) || !$this->hasFirstKoushin()) return false;
        $periodEnd = Carbon::createFromFormat('Y-m-d', $period->period_end);
        $periodStart = Carbon::createFromFormat('Y-m-d', $period->period_start);

        return Carbon::now()->between($periodStart, $periodEnd);
    }

    /**
     * Display koushin button.
     *
     * @return boolean.
     */
    public function hasKouShin()
    {
        $koshinbi = $this->convertPaidHolidayUpdateToCarbon($this->holidays_update_day);
        $periodEnd = $this->paidHolidayInformations()->orderBy('period_end','desc')->first();

        if(empty($periodEnd))
        {
            return $this->hasFirstKouShin();
        }

        $periodEnd = Carbon::createFromFormat('Y-m-d', $periodEnd->period_end);
        $endCycle = Carbon::createFromFormat('Y-m-d',$periodEnd->copy()->year . '-' . $koshinbi);
        return $endCycle->lte(Carbon::now());
    }

    /**
     * Check period of current.
     * 
     * Example:
     * 
     * Joined date: 8/6/2016, paid_holiday_update: 27/4, now: 3/2/2018
     * $periodOriginStart : 8/9/2017 (period start)
     * year: get year of periodOriginStart then add for paid_holiday_update 
     * =>result periodOriginEnd: 27/4/2017 but less than $periodOriginStart 
     * =>periodOriginEnd : 27/4/2018
     * =>false
     * =>
     * 
     * 
     * Joined date: 8/6/2014, paid_holiday_update: 27/4, now: 3/2/2018
     * $periodOriginStart : 8/9/2014 (period start)
     * year: get year of periodOriginStart then add for paid_holiday_update 
     * =>result periodOriginEnd: 27/4/2014 but less than $periodOriginStart 
     * =>periodOriginEnd : 27/4/2014
     * =>true
     * 
     * @return boolean.
     */
    public function isSenior()
    {
        $koshinbi = $this->convertPaidHolidayUpdateToCarbon($this->holidays_update_day);

        $periodOriginStart = $this->computePeriodOriginStart()->copy()->addYear();
        $year = $periodOriginStart->year;

        $periodOriginEnd = Carbon::createFromFormat('Y-m-d',$year . '-' . $koshinbi->month . '-' . $koshinbi->day);
       
        if($periodOriginStart->gte($periodOriginEnd)) {
            $periodOriginEnd->addYear();
        }

        return $periodOriginEnd->lt(Carbon::now());
        
    }

    /**
     * Counts the period amount.
     *
     * @return integer.
     */
    public function countPeriod()
    {   
        $joined_date = new \Carbon\Carbon($this->joined_date);
        $start_period = $this->computePeriodOriginStart();
        $koshinbi = $this->convertPaidHolidayUpdateToCarbon($this->holidays_update_day);
        $end_period_temp = Carbon::createFromFormat('Y-m-d',$joined_date->year . '-' . $koshinbi->month . '-' . $koshinbi->day)->addYear();
        $month_temp = $end_period_temp->diffInMonths($start_period);
        $month = Carbon::now()->diffInMonths($start_period)/12;
         ($month);
        if($month_temp < 12){
            $year = ceil($month) - 1;
        }
        $year = ceil($month);
    
        return $year;

    }

    /**
     * Get Employees with default checkbox 更新必要者
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeNecessaryKoushin($query, $isCheck=true)
    {
        // Temporary way =.=
        $prefix = \DB::getTablePrefix();

        $fresherMonth = $this->workLocation->setting->paid_holiday_after_joined_period??Setting::whereNull('work_location_id')->first()->paid_holiday_after_joined_period;
        if($isCheck) {
            return $query
            ->whereRaw('(' . $prefix . 'paid_holiday_informations.period_end is null and DATE_ADD(' . $prefix . 'employees.joined_date, INTERVAL "' . $fresherMonth . '" MONTH) <= now()) or ' . $prefix . 'paid_holiday_informations.period_end < now()');
        }
        return $query
        ->whereRaw('(' . $prefix . 'paid_holiday_informations.period_end is null and DATE_ADD(' . $prefix . 'employees.joined_date, INTERVAL "' . $fresherMonth . '" MONTH) > now()) or ' . $prefix . 'paid_holiday_informations.period_end > now()');
        
    }


    /**
     * Calculates the attendance rate period.
     * 
     * If the first period beginDay of attentence rate is joined day, endDay of attendance rate is joinedate + setting.paid_holiday_after_joined_period.
     * If it's first period beginDay is period_start of previous period, and endDay is period_end of previous period.
     *
     * @return array|boolean 
     */
    public function computeAttendanceRatePeriod()
    {
        $PaidHolidayInformation = $this->paidHolidayInformations()->orderBy('period_end','desc')->first();

        if(count($PaidHolidayInformation) > 0 ) 
        {
            if($this->isSenior()) 
            {
                $koshinbi = $this->convertPaidHolidayUpdateToCarbon($this->holidays_update_day);
                $currentPeriodStartDay = Carbon::createFromFormat('Y-m-d', Carbon::now()->year . '-' . $koshinbi->month . '-' . $koshinbi->day);
                if($currentPeriodStartDay->gt(Carbon::now()))
                {
                    $currentPeriodStartDay = $currentPeriodStartDay->subYear(); 
                }
                if(Carbon::createFromFormat('Y-m-d', $PaidHolidayInformation->period_end)->eq($currentPeriodStartDay->subDay())){
                    return [$PaidHolidayInformation->period_start, $PaidHolidayInformation->period_end];
                }
                return false;
            }
        }
        else {
            if($this->isSenior())
            {
                return false;
            }
            else {
                return [$this->joined_date, $this->computePeriodOriginStart()];
            }
        }
        return false;
    }

    /**
     * Get employees list with presentation_id
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSearchId($query, $presentation_id=null)
    {
        if(empty($presentation_id)){
            return $query;
        }
        return $query
            ->where('employees.presentation_id','like', '%'.$presentation_id.'%' );
    }

    /**
     * Search employee base on first name
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSearchFirstName($query, $first_name=null)
    {
        if(empty($first_name)){
            return $query;
        }
        return $query
            ->where('employees.first_name', 'like', '%'.$first_name.'%');
    }

    /**
     * Search employee base on Employment type
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSearchEmploymentType($query, $employment_type=null)
    {
        if(empty($employment_type)){
            return $query;
        }
        return $query
            ->where('employees.employment_type', $employment_type);
    }

    /**
     * Search employee base on Department
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSearchDepartment($query, $departments=null)
    {
         if(empty($departments)){
            return $query;
        }
        return $query
            ->whereIn('employees.department_id', $departments);
    }

    /**
     * Search employee base work status
     *
     * @param Bulder $query
     *
     * @return Builder
     */
    public function scopeSearchWorkStatus($query, $work_status=null)
    {
        if(empty($work_status)){
            return $query;
        }
        return $query
            ->where('employees.work_status',$work_status);
    }

    /**
     * Search Employee base on HolidayBonusType
     *
     * @param  Builder $query
     *
     * @return Builder
     */
    public function scopeSearchHolidayBonusType($query, $holiday_bonus_type=null)
    {
        if(empty($holiday_bonus_type)){
            return $query;
        }
        return $query
            ->where('employees.holiday_bonus_type', $holiday_bonus_type);
    }

    /**
     * Search employee base on Joined day
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSearchJoinedDay($query, $joined_day_start=null, $joined_day_end=null)
    {
        if(empty($joined_day_start) && !empty($joined_day_end))
        {
            return $query
            ->where('employees.joined_date','<', $joined_day_end);
        }
        if(empty($joined_day_end) && !empty($joined_day_start))
        {
            return $query
            ->where('employees.joined_date','>', $joined_day_start);
        }
        if(empty($joined_day_start) && empty($joined_day_end)) {
            return $query;
        }
        return $query
            ->whereBetween('employees.joined_date',[$joined_day_start,$joined_day_end]);
    }

    /**
     * Search employee base on PaidHolidayException with value 0 or 1
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSearchPaidHolidayException($query, $paid_holiday_exception=null)
    {
        if(empty($paid_holiday_exception)) {
            return $query;
        }
        return $query
            ->where('employees.paid_holiday_exception','=', $paid_holiday_exception);
    }

    /**
     * Get Employee base on WorkedTime
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeSearchWorkedTime($query, $workedStart, $workedEnd)
    {
        // Temporary way =.=
        $prefix = \DB::getTablePrefix();

        if(empty($workedStart) and empty($workedEnd)) {
            return $query;
        }
        if(empty($workedStart) and !empty($workedEnd)) {
            return $query
            ->whereRaw('TIMESTAMPDIFF(MONTH, ' . $prefix . 'employees.joined_date, now()) <= ' .$workedEnd);
        }
        if(!empty($workedStart) and empty($workedEnd)) {
            return $query
            ->whereRaw('TIMESTAMPDIFF(MONTH, ' . $prefix . 'employees.joined_date, now()) <= ' .$workedStart);
        }
        return $query
        ->whereRaw('TIMESTAMPDIFF(MONTH, ' . $prefix . 'employees.joined_date, now()) >= ' .$workedStart)
        ->whereRaw('TIMESTAMPDIFF(MONTH, ' . $prefix . 'employees.joined_date, now()) <= ' .$workedEnd);
        
    }

    /**
     * Advance Search with Paid Holiday Update Day and Attendance Rate 
     *
     * Builder $query
     *
     * @return Builder
     */
    public function scopeAdvanceSearch($query, $holidays_update_day, $holidays_update_day_start_month, $holidays_update_day_start_day, $holidays_update_day_end_month, $holidays_update_day_end_day, $attendance_rate)
    {
        // Temporary way =.=
        $prefix = \DB::getTablePrefix();

        $holidays_update_date_start = date("Y").'-'.$holidays_update_day_start_month.'-'.$holidays_update_day_start_day;
        $holidays_update_date_end = date("Y").'-'.$holidays_update_day_end_month.'-'.$holidays_update_day_end_day;
        $holidays_update_day_compute = date("Y").'-'.$holidays_update_day;

        if(empty($holidays_update_day_start_month) || empty($holidays_update_day_start_day) || empty($holidays_update_day_end_month) || empty($holidays_update_day_end_day) )
        {
            if(empty($attendance_rate))
            {
                if(empty($holidays_update_day)) {
                    return $query;
                }
                return $query
                ->whereRaw("date(concat(year(now()), '-', month( ' . $prefix . 'employees.holidays_update_day), '-', day(' . $prefix . 'employees.holidays_update_day))) = date(\"$holidays_update_day_compute\")");
            }
            if(!empty($attendance_rate)) {
                if(empty($holidays_update_day)) {
                    return $query
                    ->where('paid_holiday_informations.attendance_rate','<',$attendance_rate);
                }
                return $query
                ->whereRaw("date(concat(year(now()), '-', month(' . $prefix . 'employees.holidays_update_day), '-', day( ' . $prefix . 'employees.holidays_update_day))) = date(\"$holidays_update_day_compute\")")
                ->where('paid_holiday_informations.attendance_rate','<',$attendance_rate);
            }

        }
        if(!empty($holidays_update_day_start_month) && !empty($holidays_update_day_start_day) && !empty($holidays_update_day_end_month) && !empty($holidays_update_day_end_day))
        {
            if(empty($attendance_rate)) {
                if(empty($holidays_update_day))
                {
                    return $query;     
                }
                return $query
                ->whereRaw("date(concat(year(now()), '-', month(' . $prefix . 'employees.holidays_update_day), '-', day(' . $prefix . 'employees.holidays_update_day))) = date(\"$holidays_update_day_compute\")");
            }
            if(!empty($attendance_rate))
            {
                if(empty($holidays_update_day))
                {
                    return $query
                    ->whereRaw("date(concat(year(now()), '-', month( ' . $prefix . 'employees.holidays_update_day), '-', day(' . $prefix . 'employees.holidays_update_day))) between date(\"$holidays_update_date_start\") and date(\"$holidays_update_date_end\")")
                    ->where('paid_holiday_informations.attendance_rate','<',$attendance_rate);
                }
                return $query
                    ->whereRaw("date(concat(year(now()), '-', month( ' . $prefix . 'employees.holidays_update_day), '-', day(' . $prefix . 'employees.holidays_update_day))) between date(\"$holidays_update_date_start\") and date(\"$holidays_update_date_end\")")
                    ->whereRaw("date(\"$holidays_update_day_compute\") between date(\"$holidays_update_date_start\") and date(\"$holidays_update_date_end\")")
                    ->whereRaw("date(concat(year(now()), '-', month(' . $prefix . 'employees.holidays_update_day), '-', day(' . $prefix . 'employees.holidays_update_day))) = date(\"$holidays_update_day_compute\")")
                    ->where('paid_holiday_informations.attendance_rate','<',$attendance_rate);
            }
        }
    }

    /**
     * Get paid holiday information
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeGetDataPaidHolidayInformation($query)
    {
        // Temporary way =.=
        $prefix = \DB::getTablePrefix();

        return $query
        ->leftJoin('work_locations', function($join) use ($prefix) {
            $join->on('work_locations.id','=','employees.work_location_id');
        })
        ->leftJoin('paid_holiday_informations', function($join)  use ($prefix)
            {
                $join->on('employees.id','=','paid_holiday_informations.employee_id')
                ->whereRaw($prefix . 'paid_holiday_informations.id in (select max(' . $prefix . 'paid_holiday_informations.id) from ' . $prefix . 'paid_holiday_informations group by ' . $prefix . 'paid_holiday_informations.employee_id)');
            }
        )
        ->select(
                'employees.id',
                'employees.presentation_id',
                'employees.last_name',
                'employees.first_name',
                'employees.holiday_bonus_type',
                'employees.joined_date',
                'employees.holidays_update_day',
                'employees.work_location_id',
                'employees.work_time_per_day as e_work_time_per_day',
                'work_locations.name',
                'paid_holiday_informations.period_start',
                'paid_holiday_informations.period_end',
                'paid_holiday_informations.work_time_per_day',
                'paid_holiday_informations.attendance_rate',
                'paid_holiday_informations.provided_paid_holidays',
                'paid_holiday_informations.carried_forward_paid_holidays',
                'paid_holiday_informations.carried_forward_paid_holidays_hour',
                'paid_holiday_informations.consumed_paid_holidays',
                'paid_holiday_informations.consumed_paid_holidays_hour',
                'paid_holiday_informations.available_paid_holidays',
                'paid_holiday_informations.available_paid_holidays_hour',
                'paid_holiday_informations.note',
                'paid_holiday_informations.last_modified_date',
                'paid_holiday_informations.last_modified_manager_id',
                'paid_holiday_informations.updated_at'
        )
        ->orderBy('employees.id', 'asc');
    }
  

    /**
     * Gets the amount paid holiday depend on 週勤務日数.
     *
     * @return integer.
     */
    public function getAmountPaidHoliday()
    {
        switch ($this->holiday_bonus_type) {
            case config('constants.normal_bonus'):
                return $this->workLocation->setting->paid_holiday_first_time_normal_type??Setting::whereNull('work_location_id')->first()->paid_holiday_first_time_normal_type;
                break;
            case config('constants.four_days_per_week_bonus'):
                return $this->workLocation->setting->paid_holiday_first_time_4wdpw_type??Setting::whereNull('work_location_id')->first()->paid_holiday_first_time_4wdpw_type;
                break;
            case config('constants.three_days_per_week_bonus'):
                return $this->workLocation->setting->paid_holiday_first_time_3wdpw_type??Setting::whereNull('work_location_id')->first()->paid_holiday_first_time_3wdpw_type;
                break;
            case config('constants.two_days_per_week_bonus'):
                return $this->workLocation->setting->paid_holiday_first_time_2wdpw_type??Setting::whereNull('work_location_id')->first()->paid_holiday_first_time_2wdpw_type;
                break;
            case config('constants.one_day_per_week_bonus'):
                return $this->workLocation->setting->paid_holiday_first_time_1wdpw_type??Setting::whereNull('work_location_id')->first()->paid_holiday_first_time_1wdpw_type;
                break;
            
            default:
                return 0;
                break;
        }
    }

    /**
     * Gets the amount paid holiday increase.
     *
     * @return string.
     */
    public function getAmountPaidHolidayIncrease()
    {
        switch ($this->holiday_bonus_type) {
            case config('constants.normal_bonus'):
                return $this->workLocation->setting->paid_holiday_increase_rate_normal_type??Setting::whereNull('work_location_id')->first()->paid_holiday_increase_rate_normal_type;
                break;
            case config('constants.four_days_per_week_bonus'):
                return $this->workLocation->setting->paid_holiday_increase_rate_4wdpw_type??Setting::whereNull('work_location_id')->first()->paid_holiday_increase_rate_4wdpw_type;
                break;
            case config('constants.three_days_per_week_bonus'):
                return $this->workLocation->setting->paid_holiday_increase_rate_3wdpw_type??Setting::whereNull('work_location_id')->first()->paid_holiday_increase_rate_3wdpw_type;
                break;
            case config('constants.two_days_per_week_bonus'):
                return $this->workLocation->setting->paid_holiday_increase_rate_2wdpw_type??Setting::whereNull('work_location_id')->first()->paid_holiday_increase_rate_2wdpw_type;
                break;
            case config('constants.one_day_per_week_bonus'):
                return $this->workLocation->setting->paid_holiday_increase_rate_1wdpw_type??Setting::whereNull('work_location_id')->first()->paid_holiday_increase_rate_1wdpw_type;
                break;
            
            default:
                return 0;
                break;
        }
    } 

    /**
     * Get number of requests this employee has made within a given period of time.
     *
     * @param string        $start_date(Y-m-d)
     * @param string        $end_date(Y-m-d)
     *
     * @return integer
     */
    public function numberOfRequests($start_date, $end_date)
    {  
        $count = EmployeeWorkingInformationSnapshot::whereHas('employeeWorkingDay', function($query) use ($start_date, $end_date) {
            return $query->where('employee_id', $this->id)->where('date', '>=', $start_date)->where('date', '<=', $end_date);
        })->where('left_status', EmployeeWorkingInformationSnapshot::CONSIDERING)->count();

        return $count;
    }

    /**
     * Get total number of requests this employee's subordinates have made within a given period of time.
     *
     * @param string        $start_date(Y-m-d)
     * @param string        $end_date(Y-m-d)
     *
     * @return integer
     */
    public function numberOfSubordinatesRequest($start_date, $end_date)
    {
        $count = EmployeeWorkingInformationSnapshot::whereHas('employeeWorkingDay', function($query) use ($start_date, $end_date) {
            return $query->where('date', '>=', $start_date)->where('date', '<=', $end_date)
                ->whereHas('employee', function($query) {
                    return $query->whereHas('chiefs', function($query) {
                        return $query->where('id', $this->id);
                    });
                });
        })->where('left_status', EmployeeWorkingInformationSnapshot::CONSIDERING)->count();
        return $count;
    }

    /**
     * Determine if this employee's paid holiday information can be updated or not.
     *
     * @return boolean
     */
    public function canUpdatePaidHolidayInformation()
    {
        if ($this->paidHolidayInformations->isEmpty()) {

            $soonest_update_date = $this->firstDayOfAfterJoinedPeriod();

        } else {

            $last_period = $this->paidHolidayInformations->sortBy('period_end')->last();

            $last_period_last_day_carbon = new Carbon($last_period->period_end);

            // Have to add one extra day, to match with Employee's holidays_update_day
            $soonest_update_date = $last_period_last_day_carbon->copy()->addDay();
        }

        $today = Carbon::today();

        return $today->gte($soonest_update_date);
    }

    /**
     * Get the first day after the probation period of an employee.
     *
     * @return Carbon
     */
    public function firstDayOfAfterJoinedPeriod()
    {
        $joined_date_carbon = new Carbon($this->joined_date);

        return $joined_date_carbon->copy()->addMonths($this->workLocation->currentSetting()->paid_holiday_after_joined_period);
    }

    /**
     * Get all the maebarai history of this employee
     */
    public function employeeMaebaraiHistory()
    {
        return $this->hasMany(MaebaraiHistory::class);
    }
}


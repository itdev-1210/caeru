<?php

namespace App;

class ConcludedEmployeeWorkingInformation extends Model
{
    /**
     * Get the original version of this working information
     */
    public function employeeWorkingInformation()
    {
        return $this->belongsTo(EmployeeWorkingInformation::class);
    }

    /**
     * Get the working day instance
     */
    public function employeeWorkingDay()
    {
        return $this->belongsTo(EmployeeWorkingDay::class);
    }
}

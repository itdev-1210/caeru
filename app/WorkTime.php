<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkTime extends Model
{
    use SoftDeletes;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * DEFAULT WORK STATUS CONSTANTS
     */
    const MORNING1  = 1;
    const MORNING2  = 2;    

    /**
     * get the default work statuses
     */
    public static function defaults()
    {
        return [
            self::MORNING1      =>      '早1',
            self::MORNING2      =>      '早2',
        ];
    }

    /**
     * Get the company of this work time
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the work location of this work time
     */
    public function workLocation()
    {
        return $this->belongsTo(WorkLocation::class);
    }
}

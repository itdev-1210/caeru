<?php

namespace App\Exceptions;

use Illuminate\Session\TokenMismatchException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Exception;
use Response;
use Caeru;
use App;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        // \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $e = $this->prepareException($exception);
        // dd(in_array($e->getStatusCode(), $pass_through_exceptions));

        if ($e instanceof AuthenticationException) {
            return $this->unauthenticated($request,$e);

        } else if ($e instanceof SinseiAuthenticateException) {
            return $this->sinseiUnauthenticated($request,$e);

	    } else if ($e instanceof ApproveAuthenticateException) {
            return $this->approveUnauthenticated($request,$e);

        } else if ($e instanceof UnauthenticatedCompanyException) {
            return $this->theSameAsPageNotFound($request,$e);

        } else if ($e instanceof UndefinedCompanyException) {
            return $this->theSameAsPageNotFound($request,$e);

        } else if ($e instanceof TokenMismatchException) {
            return $this->pageExpired($request,$e);

        } else if (!($e instanceof ValidationException) && App::environment('production')) {
            return Response::view('errors.500', [], 500);
        }

	    return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        $route_name = $request->route()->getName();

        if (strpos($route_name, 'admin') === false) {
            return Caeru::redirect('login');
        } else {
            return Caeru::redirect('admin_login');
        }

    }

	/**
	 * Convert an authentication exception into an unauthenticated response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Illuminate\Auth\SinseiAuthenticateException  $exception
	 * @return \Illuminate\Http\Response
	 */
	protected function sinseiUnauthenticated($request, SinseiAuthenticateException $exception)
	{
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return Caeru::redirect('ss_show_login');
	}

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\ApproveAuthenticateException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function approveUnauthenticated($request, ApproveAuthenticateException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return Caeru::redirect('ss_show_login');
    }

    /**
     * Show 404 Page Not Found Error Page
     *
     * @param  \Illuminate\Http\Request     $request
     * @param                               $exception
     * @return \Illuminate\Http\Response
     */
    protected function theSameAsPageNotFound($request, $exception)
    {
        if (App::environment('production')) {
            if ($request->expectsJson()) {
                return response()->json(['error' => 'Page not found.'], 404);
            }

            return Response::view('errors.404', [], 404);
        } else {
            return parent::render($request, $exception);
        }
    }

    /**
     * Show Page Expired error page
     *
     * @param  \Illuminate\Http\Request     $request
     * @param                               $exception
     * @return \Illuminate\Http\Response
     */
    protected function pageExpired($request, $exception)
    {
        if (App::environment('production')) {
            if ($request->expectsJson()) {
                return response()->json(['error' => 'Page expired.'], 512);
            }

            return Response::view('errors.512', [], 512);
        } else {
            return parent::render($request, $exception);
        }
    }

    /**
     * Convert a validation exception into a JSON response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Validation\ValidationException  $exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function invalidJson($request, ValidationException $exception)
    {
        return response()->json($exception->errors(), $exception->status);
    }
}

<?php

namespace App\Exceptions;

use Exception;

class ApproveAuthenticateException extends Exception
{
	
	/**
	 * Create a new authentication exception.
	 *
	 * @param  string  $message
	 * @param  array  $guards
	 * @return void
	 */
	public function __construct($message = 'You do not have permission to access this page.')
	{
		parent::__construct($message);
	}
	
}
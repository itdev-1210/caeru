<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use League\Csv\Reader;
use League\Csv\Writer;
use League\Csv\CharsetConverter;
use Validator;
use Illuminate\Validation\Rule;
use App\Employee;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use App\WorkLocation;
use App\EmployeeWorkingInformationSnapshot;
use App\Company;
use Carbon\Carbon;
use App\WorkStatus;
use App\RestStatus;
use App\WorkTime;
use Auth;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;
use Barryvdh\Debugbar\Facade as Debugbar;

class EmployeeWorkingInformationCsvRequest extends FormRequest
{
    use CanCreateWorkingDayOnTheFlyTrait;

    /**
     * Constants for error of record
     *
     */
    const DATA_ERROR                    = 1;
    const SINSEI_CONSIDERING_ERROR      = 2;
    const CONCLUDED_ERROR               = 3;

    /**
     * Define time limit
     *
     */
    protected $day_of_the_upper_limit      = null;
    protected $date_upper_limit             = null;

    /**
     * The attributes that should be add to header in file csv.
     *
     * @var array
     */
    protected $key_value = [
        '勤務先ID' => 'planned_work_location_id',
        '所定出勤時刻' => 'schedule_start_work_time',
        '所定退勤時刻' => 'schedule_end_work_time',
        '所定休憩時間' => 'schedule_break_time',
        '所定深夜休憩時間' => 'schedule_night_break_time',
        '所定労働時間' => 'schedule_working_hour',
        '勤務形態' => 'planned_work_status_id',
        '休暇形態' => 'planned_rest_status_id',
        '時間休暇開始時刻' => 'paid_rest_time_start',
        '時間休暇終了時刻' => 'paid_rest_time_end',
        '時間休暇時間（時間）' => 'paid_rest_time_period',
        '早出開始時刻' => 'planned_early_arrive_start',
        '早出終了時刻' => 'planned_early_arrive_end',
        '残業開始時刻' => 'planned_overtime_start',
        '残業終了時刻' => 'planned_overtime_end',
        '遅刻時間（分）' => 'planned_late_time',
        '早退時間（分）' => 'planned_early_leave_time',
        '予定休憩（分）' => 'planned_break_time',
        '予定深夜休憩（分）' => 'planned_night_break_time',
        '予定外出時間（分）' => 'planned_go_out_time',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'      => 'required|mimes:csv,txt',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $file = $this->file('file');
            if (!empty($file)) {
                $records = $true_data = $employee_working_day_with_rest_status_changed = [];
                $check_error = false;
                $message = '';
                $employees = Employee::all();
                $work_locations = WorkLocation::all();
                //===========================Updated optimize code since 2019-03-29================================//
                $employee_working_days = null;
                //=================================================================================================//
                $employee_working_informations = EmployeeWorkingInformation::with([
                    'employeeWorkingInformationSnapshot',
                    'cachedEmployeeWorkingInformation',
                    'concludedEmployeeWorkingInformation',
                    'plannedSchedule.workLocation.setting',
                    'plannedSchedule.workLocation.unusedWorkStatuses',
                    'plannedSchedule.workLocation.unusedRestStatuses',
                    'plannedSchedule.workLocation.company.setting',
                    'plannedSchedule.workLocation.company.workStatuses',
                    'plannedSchedule.workLocation.company.restStatuses',
                    'currentRealWorkLocation.setting',
                    'currentRealWorkLocation.unusedWorkStatuses',
                    'currentRealWorkLocation.unusedRestStatuses',
                    'currentRealWorkLocation.company.setting',
                    'currentRealWorkLocation.company.workStatuses',
                    'currentRealWorkLocation.company.restStatuses',
                    'employeeWorkingDay.employee.workLocation.company',
                ]);

                setlocale(LC_ALL, 'ja_JP.UTF-8');

                $work_times = WorkTime::where('visible', 1)
                    ->where('company_id', Auth::user()->company_id)
                    ->where('work_location_id', session()->get('current_work_location'))
                    ->get();

                $csvData = file_get_contents($file);
                if (!mb_check_encoding($csvData, 'UTF-8'))
                    $csvData = mb_convert_encoding($csvData, "UTF-8", "SJIS");

                $csv = Reader::createFromString($csvData);
                $csv->setHeaderOffset(0);
                $header = $csv->getHeader();
                $type = 0;
                $isCurrentLocation = true;
                if (count($header) < 4 || $header[count($header) - 1] !== 'エラー') {
                    $type = 0;
                } else {
                    if ($header[0] == '勤務ID（新規は空白）') {
                        $type = 1;
                    } else {
                        $type = 2;
                        try {
                            for ($i = 2; $i < count($header) - 1; $i++) {
                                $temp = Carbon::parse($header[$i])->format('Y-m-d');
                            }
                        } catch (\Exception $e) {
                            $type = 0;
                        }
                        if ($type != 0) {
                            $location = $work_locations->where('id', session()->get('current_work_location'))->first();
                            if ($header[1] !== $location->presentation_id && $header[1] != $location->name)
                                $isCurrentLocation = false;
                        }
                    }
                }

                if ($type == 1) {
                    $list_ewi_id = [];
                    //===========================Updated optimize code since 2019-03-29================================//
                    $check = false;
                    //=================================================================================================//
                    foreach ($csv->getRecords() as $record) {
                        array_push($list_ewi_id, $record['勤務ID（新規は空白）']);
                        //===========================Updated optimize code since 2019-03-29================================//
                        if ($record['勤務ID（新規は空白）'] === "" && $record['従業員ID'] !== "" && $record['勤務年月日'] !== "") {
                            $employee = $employees->whereStrict('presentation_id', $record['従業員ID'])->first();
                            if (isset($employee)) {
                                if ($check == false) {
                                    $employee_working_days = EmployeeWorkingDay::where([
                                        ['date', '=', $record['勤務年月日']],
                                        ['employee_id', '=', $employee->id],
                                    ]);
                                } else {
                                    $employee_working_days = $employee_working_days->orWhere([
                                        ['date', '=', $record['勤務年月日']],
                                        ['employee_id', '=', $employee->id],
                                    ]);
                                }
                                $check = true;
                            }
                        }
                        //=================================================================================================//
                    }
                    //=============================Updated optimize code since 2019-03-29==============================//
                    if ($check == true)
                        $employee_working_days = $employee_working_days->get();
                    else
                        $employee_working_days = collect([]);
                    //=================================================================================================//
                    $employee_working_informations = $employee_working_informations->whereIn('id', $list_ewi_id)->get();
                    foreach ($csv->getRecords() as $record) {
                        if ($record['勤務ID（新規は空白）'] !== "Sample") {
                            $record = array_map(function($value){
                                return ($value !== "") ? trim($value, " \t\n") : null;
                            }, $record);

                            $record['エラー'] = $employee = $work_location = $employee_working_day = $employee_working_information = $work_status
                                = $rest_status = null;

                            $sub_validator = $this->validationRecord($record, $employees, $work_locations, $employee_working_informations);

                            if ($sub_validator->fails()) $record['エラー'] = self::DATA_ERROR . '/' . head($sub_validator->errors()->keys());

                            if (!isset($record['エラー'])) {

                                if ($record['勤務ID（新規は空白）'] !== null) {
                                    $employee_working_information = $employee_working_informations->where('id', $record['勤務ID（新規は空白）'])->first();

                                    $employee_working_day = $employee_working_information->employeeWorkingDay;
                                    $record['勤務年月日'] = $employee_working_day->date;
                                    $employee = $employee_working_day->employee;
                                } else {
                                    $record['勤務年月日'] = Carbon::createFromFormat('Y-m-d', str_replace("/","-",$record['勤務年月日']))->toDateString();

                                    $employee = $employees->whereStrict('presentation_id', $record['従業員ID'])->first();
                                    $employee_working_day = $employee_working_days->where('date', $record['勤務年月日'])->where('employee_id', $employee->id)->first();
                                }

                                if ($record['勤務先ID'] !== null)
                                    $work_location = $work_locations->whereStrict('presentation_id', $record['勤務先ID'])->first();

                                $check_data_change = (isset($employee_working_information)) ?
                                    $this->compareDataWithEWI($record, $employee_working_information) : $this->checkAllItemEmpty($record);

                                if (!$check_data_change || $record['削除']) {
                                    $work_status = (isset($record['勤務形態']) && isset($work_location))
                                        ? $work_location->activatingWorkStatuses()->where('name', $record['勤務形態'])->first() : null ;
                                    $rest_status = (isset($record['休暇形態']) && isset($work_location))
                                        ? $work_location->activatingRestStatuses()->where('name', $record['休暇形態'])->first() : null ;

                                    if (!isset($record['エラー'])) {
                                        if (isset($record['勤務形態']) && !isset($work_status))
                                            $record['エラー'] = self::DATA_ERROR . '/勤務形態';
                                        else if (isset($record['休暇形態']) && !isset($rest_status))
                                            $record['エラー'] = self::DATA_ERROR . '/休暇形態';
                                    }

                                    if (!isset($record['エラー'])) {
                                        if (isset($employee_working_day)) {
                                            if ($employee_working_day->isConcluded()) $record['エラー'] = self::CONCLUDED_ERROR;
                                            if ($employee_working_day->isHavingConsideringRequest()) $record['エラー'] = self::SINSEI_CONSIDERING_ERROR;
                                        }
                                    }

                                    if (!isset($record['エラー'])) {
                                        $this->getDayOfTheUpperLimit($record['勤務年月日'], $employee);
                                        $this->getDateUpperLimit($record['勤務年月日'], $employee);

                                        $data = $record;
                                        $data['employee_id'] = isset($employee) ? $employee->id : null;
                                        $data['planned_work_location_id'] = isset($record['勤務先ID']) && isset($work_location) ? $work_location->id : null;
                                        $data['planned_work_status_id'] = isset($work_status) ? $work_status->id : null;
                                        $data['planned_rest_status_id'] = isset($rest_status) ? $rest_status->id : null;
                                        if (!isset($employee_working_day)) {
                                            $employee_working_day = $this->createEmployeeWorkingDayOnTheFly($employee->id, $record['勤務年月日']);
                                            $employee_working_days->push($employee_working_day);
                                        }
                                        $data['employee_working_day_id'] = $employee_working_day->id;

                                        if (isset($employee_working_information)) {
                                            if ($data['planned_rest_status_id'] != $employee_working_information->planned_rest_status_id) {
                                                array_push($employee_working_day_with_rest_status_changed, $data['employee_working_day_id']);
                                            }
                                        } else {
                                            if (isset($data['planned_rest_status_id'])) {
                                                array_push($employee_working_day_with_rest_status_changed, $data['employee_working_day_id']);
                                            }
                                        }
                                        $true_data[] = $this->createTrueDataFromRecord($data);
                                    }
                                }
                            }
                            if (isset($record['エラー']))
                                $check_error = true;

                            $records[] = $record;
                        }
                    }
                } else if ($type == 2) {
                    if ($isCurrentLocation) {
                        $work_location = $work_locations->where('id', session()->get('current_work_location'))->first();
                        foreach ($csv->getRecords() as $record) {
                            // if ($record['勤務地ID'] !== "Sample") {
                            $record = array_map(function($value){
                                return ($value !== "") ? trim($value, " \t\n") : null;
                            }, $record);

                            $record['エラー'] = $employee = $employee_working_day = null;

                            $sub_validator = $this->validationWorkTimeRecord($record, $work_times, $work_location, $header);
                            if ($sub_validator->fails()) $record['エラー'] = head($sub_validator->errors()->keys());
                            $employee = $employees->whereStrict('presentation_id', $record[$header[1]])->first();
                            if (!$employee) $record['エラー'] = '従業員IDが存在しません。';
                            if (!isset($record['エラー'])) {
                                $count = count($header);
                                for($i = 2; $i < $count - 1; $i++)
                                {
                                    if (isset($record[$header[$i]])) {
                                        $temp = Carbon::parse($header[$i])->format('Y-m-d');
                                        $this->getDayOfTheUpperLimit($temp, $employee);
                                        $this->getDateUpperLimit($temp, $employee);

                                        $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', '=', $temp)->first();

                                        // Make sure there is always a working_day, if that employee_id is valid.
                                        if (!$employee_working_day) {
                                            $employee_working_day = $this->createEmployeeWorkingDayOnTheFly($employee->id, $temp);
                                        }
                                        $data = [];
                                        if ($work_time = $work_times->where('name', $record[$header[$i]])->where('work_location_id', session()->get('current_work_location'))->first()) {
                                            $data['schedule_start_work_time'] = $work_time['start_work_time'];
                                            $data['schedule_end_work_time'] = $work_time['end_work_time'];
                                            $data['schedule_break_time'] = $work_time['break_time'];
                                            $data['schedule_night_break_time'] = $work_time['night_break_time'];
                                            $data['schedule_working_hour'] = $work_time['working_hour'];
                                            $data['work_time'] = $work_time['name'];
                                            $data['planned_work_location_id'] = $work_time['work_location_id'];
                                            $data['employee_id'] = isset($employee) ? $employee->id : null;
                                            $data['employee_working_day_id'] = $employee_working_day->id;
                                            $true_data[] = $this->createTrueWorkTimeDataFromRecord($data);
                                        } else if ($work_status = $work_location->activatingWorkStatuses()->where('name', $record[$header[$i]])->first()) {
                                            $data['planned_work_status_id'] = $work_status->id;
                                            $data['planned_work_location_id'] = session()->get('current_work_location');
                                            $data['employee_id'] = isset($employee) ? $employee->id : null;
                                            $data['employee_working_day_id'] = $employee_working_day->id;
                                            $true_data[] = $data;
                                        } else if ($rest_status = $work_location->activatingRestStatuses()->where('name', $record[$header[$i]])->first()) {
                                            $data['planned_rest_status_id'] = $rest_status->id;
                                            $data['planned_work_location_id'] = session()->get('current_work_location');
                                            $data['employee_id'] = isset($employee) ? $employee->id : null;
                                            $data['employee_working_day_id'] = $employee_working_day->id;
                                            $true_data[] = $data;
                                        }
                                    }
                                }
                            }
                            if (isset($record['エラー']))
                                $check_error = true;

                            $records[] = $record;
                            // }
                        }
                    } else {
                        $check_error = true;
                        $records = $csv->getRecords();
                        $message = '勤務地が一致しません';
                    }
                } else {
                    $check_error = true;
                    $records = $csv->getRecords();
                    $message = 'ファイルのフォーマットが正確ではありません。';
                }
                if ($check_error)
                    $this->writeCSV($csv->getHeader(), $records, $message);
                else {
                    if ($type == 1) {
                        $this->merge(['employee_working_informations' => $employee_working_informations]);
                        $this->merge(['employee_working_day_with_rest_status_changed' => $employee_working_day_with_rest_status_changed]);
                    }
                    if (!$isCurrentLocation) $this->merge(['isCurrentLocation' => $isCurrentLocation]);
                    $this->merge(['type' => $type]);
                    $this->merge(['true_data' => $true_data]);
                }
            }
        });
    }

    /**
     * validation record
     *
     * @param  array $record
     * @return Validator $sub_validator
     */
    protected function validationRecord($record, $employees, $work_locations, $employee_working_informations) {
        $employee_working_informations_id = $employee_working_informations->pluck('id')->toArray();
        $work_locations_presentation_id = $work_locations->pluck('presentation_id')->toArray();
        $employees_presentation_id = $employees->pluck('presentation_id')->toArray();

        $sub_validator = Validator::make($record, [
            '勤務ID（新規は空白）' => [
                'nullable',
                Rule::in($employee_working_informations_id),
            ],
            '従業員ID' => [
                'nullable',
                'required_without:勤務ID（新規は空白）',
                Rule::in($employees_presentation_id),
            ],
            '従業員名' => 'nullable',
            '勤務先ID' => [
                'required',
                Rule::in($work_locations_presentation_id),
            ],
            '勤務先名' => 'nullable',
            '勤務年月日' => 'nullable|required_without:勤務ID（新規は空白）|date',
            '所定出勤時刻' => 'nullable|time|required_with:所定退勤時刻',
            '所定退勤時刻' => 'nullable|time|required_with:所定出勤時刻',
            '所定休憩時間' => 'nullable|integer',
            '所定深夜休憩時間' => 'nullable|integer',
            '所定労働時間' => 'nullable|time',
            '勤務形態' => 'nullable',
            '休暇形態' => 'nullable',
            '時間休暇開始時刻' => 'nullable|time|required_with:時間休暇終了時刻',
            '時間休暇終了時刻' => 'nullable|time|required_with:時間休暇開始時刻',
            '時間休暇時間（時間）' => 'nullable|time',
            '早出開始時刻' => 'nullable|time|required_with:早出終了時刻',
            '早出終了時刻' => 'nullable|time|required_with:早出開始時刻',
            '残業開始時刻' => 'nullable|time|required_with:残業終了時刻',
            '残業終了時刻' => 'nullable|time|required_with:残業開始時刻',
            '遅刻時間（分）' => 'nullable|integer',
            '早退時間（分）' => 'nullable|integer',
            '予定休憩（分）' => 'nullable|integer',
            '予定深夜休憩（分）' => 'nullable|integer',
            '予定外出時間（分）' => 'nullable|integer',
            '削除' => 'nullable|boolean',
            'エラー' => 'nullable|integer',
        ]);

        return $sub_validator;
    }

    /**
     * validation work time record
     *
     * @param  array $record
     * @return Validator $sub_validator
     */
    protected function validationWorkTimeRecord($record, $work_times, $work_location, $headers) {
        $work_time_name = $work_times->pluck('name')->toArray();
        $work_status = $work_location->activatingWorkStatuses()->pluck('name')->toArray();
        $del_status = ['振出', '法出','休出','残業'];
        $intersect = array_intersect($work_status, $del_status);
        $add_status = array_diff($work_status, $intersect);
        $rest_status = $work_location->activatingRestStatuses()->where('unit_type', 1)->pluck('name')->toArray();
        $work_time_name = array_merge($work_time_name, $add_status);
        $work_time_name = array_merge($work_time_name, $rest_status);

        $validator = [];
        $validator['勤務地ID'] = 'nullable';
        $validator['エラー'] = 'nullable|integer';
        for($i = 2; $i < count($headers) - 1; $i++)
        {
            $validator[$headers[$i]] = ['nullable', Rule::in($work_time_name)];
        }
        $sub_validator = Validator::make($record, $validator);

        return $sub_validator;
    }

    /**
     * Export CSV
     *
     * @param  array $header
     * @param  array(array) $records
     */
    protected function writeCSV($header, $records, $message) {
        //load the CSV document from a string
        $writer = Writer::createFromString('');

        if ($message !== '') {
            if ($header[count($header) - 1] === 'エラー')
                $header[count($header) - 1] = $message;
            else
                $header[count($header)] = $message;
        }

        //insert the header
        $writer->insertOne($header);

        //insert all the records
        $writer->insertAll($records);

        $writer->output('schedules.csv');
        die;
    }

    /**
     * Check early_arrive_start and early_arrive_end and late_time
     *
     * @param  hh:mm $early_arrive_start
     * @param  hh:mm $early_arrive_end
     * @param  mm $late_time
     * @return boolean
     */
    protected function checkLateTimeAndEarlyArrive($early_arrive_start, $early_arrive_end, $late_time) {
        return $early_arrive_start !== null && $early_arrive_end !== null && $late_time !== null;
    }

    /**
     * Check over_time_start and over_time_end and leave_early
     *
     * @param  hh:mm $over_time_start
     * @param  hh:mm $over_time_end
     * @param  mm $leave_early
     * @return boolean
     */
    protected function checkLeaveEarlyAndOvertime($over_time_start, $over_time_end, $leave_early) {
        return $over_time_start !== null && $over_time_end !== null && $leave_early !== null;
    }

    /**
     * The function will compare data with employee working information, will return true, if it is same
     * Else return false
     *
     * @param Array $record
     * @param App/EmployeeWorkingInformation $employee_working_information
     * @return boolean
     */
    protected function compareDataWithEWI($record, $employee_working_information) {
        foreach ($this->key_value as $key => $value) {
            $data = $employee_working_information->$value;
            if (in_array($value, ['schedule_start_work_time', 'schedule_end_work_time', 'paid_rest_time_start', 'paid_rest_time_end',
                'planned_early_arrive_start', 'planned_early_arrive_end', 'planned_overtime_start', 'planned_overtime_end']))
                $data = $this->convertStringToMintues($data);
            else if (in_array($value, ['schedule_working_hour', 'paid_rest_time_period']))
                $data = $this->convertStringToMintues($data, 'H:i:s');
            else if ($value === 'planned_work_status_id' && isset($data))
                $data = WorkStatus::find($data)->name;
            else if ($value === 'planned_rest_status_id' && isset($data))
                $data = RestStatus::find($data)->name;
            else if ($value === 'planned_work_location_id' && isset($data))
                $data = WorkLocation::find($data)->presentation_id;

            if ($record[$key] != $data)
                return false;
        }
        return true;
    }

    /**
     * The function will return true, if all item of record is empty
     * Else return false
     *
     * @param Array $record
     * @return boolean
     */
    protected function checkAllItemEmpty($record) {
        foreach ($this->key_value as $key => $value) {
            if ($record[$key] !== null) return false;
        }
        return true;
    }

    /**
     * Check paid_rest_time_start and paid_rest_time_end and leave_early
     *
     * @param  hh:mm $paid_rest_time_start
     * @param  hh:mm $paid_rest_time_end
     * @param  hh:mm $paid_rest_time_period
     * @return boolean
     */
    protected function checkPaidRestTime($paid_rest_time_start, $paid_rest_time_end, $paid_rest_time_period) {
        return $paid_rest_time_start !== null && $paid_rest_time_end !== null && $paid_rest_time_period !== null;
    }

    /**
     * The function will check logic in the record
     *
     * @param  Array $record
     * @param  App\Employee $employee
     * @return boolean
     */
    protected function checkLogicOfRecord($record, $employee) {

        $schedule_start_work_time = $this->getCarbonInstance($record['所定出勤時刻']);
        $schedule_end_work_time = $this->getCarbonInstance($record['所定退勤時刻']);
        if (isset($schedule_start_work_time) && isset($schedule_end_work_time) && $schedule_start_work_time->gt($schedule_end_work_time))
            $schedule_end_work_time->addDay();
        $schedule_working_hour = $this->getCarbonInstance($record['所定労働時間']);

        $paid_rest_time_start = $this->getCarbonInstance($record['時間休暇開始時刻']);
        $paid_rest_time_end = $this->getCarbonInstance($record['時間休暇終了時刻']);
        if (isset($paid_rest_time_start) && isset($paid_rest_time_end) && $paid_rest_time_start->gt($paid_rest_time_end))
            $paid_rest_time_end->addDay();
        $paid_rest_time_period = $this->getCarbonInstance($record['時間休暇時間（時間）']);

        $planned_early_arrive_start = $this->getCarbonInstance($record['早出開始時刻']);
        $planned_early_arrive_end = $this->getCarbonInstance($record['早出終了時刻']);
        if (isset($planned_early_arrive_start) && isset($planned_early_arrive_end) && $planned_early_arrive_start->gt($planned_early_arrive_end))
            $planned_early_arrive_end->addDay();

        if ((isset($schedule_working_hour) && isset($paid_rest_time_period) && $paid_rest_time_period->gt($schedule_working_hour))
            || (isset($planned_early_arrive_start) && isset($schedule_start_work_time) && $schedule_start_work_time->between($planned_early_arrive_start, $planned_early_arrive_end, false))
            || (isset($paid_rest_time_start) && !$paid_rest_time_start->between($schedule_start_work_time, $schedule_end_work_time))
            || (isset($paid_rest_time_end) && !$paid_rest_time_end->between($schedule_start_work_time, $schedule_end_work_time)))
            return true;

    }

    /**
     * Get the day of the date_upper_limit above
     *
     */
    public function getDayOfTheUpperLimit($date, $employee)
    {
        if ($employee->workLocation->company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {
            $this->day_of_the_upper_limit = $date;
        } else {
            $this->day_of_the_upper_limit = Carbon::createFromFormat('Y-m-d', $date)->subDay()->format('Y-m-d');
        }
    }

    /**
     * Base on the date_separate setting of the company to determine the uppler limit of date attributes of this working day
     *
     */
    public function getDateUpperLimit($date, $employee)
    {
        $company = $employee->workLocation->company;

        if ($company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {
            $this->date_upper_limit = $date . ' ' . $company->date_separate_time . ':00';
        } else {
            $day = Carbon::createFromFormat('Y-m-d', $date);
            $day->subDay();
            $this->date_upper_limit = $day->format('Y-m-d') . ' ' . $company->date_separate_time . ':00';
        }
    }

    /**
     * Generate a Carbon instance from a time string with format 'hh:mm'
     *
     * @param string $time_string
     * @return Carbon
     */
    protected function getCarbonInstance($time_string)
    {
        if ($time_string != null) {

            // If the $time_string is a full-fledge time string with date parts
            if (strpos($time_string, ' ') !== false) {
                return new Carbon($time_string);
            }

            $instance = Carbon::createFromFormat('Y-m-d', $this->day_of_the_upper_limit);

            $time = explode(':', $time_string);

            $instance->hour = $time[0];
            $instance->minute = $time[1];
            $instance->second = (isset($time[2])) ? $time[2] : 0 ;

            $carbon_date_limit = Carbon::createFromFormat('Y-m-d H:i:s', $this->date_upper_limit);
            if ($instance->lt($carbon_date_limit))
                $instance->addDay();

            return $instance;

        } else {
            return null;
        }
    }

    /**
     * The function will transfer format (hh:mm) to format (yyyy-mm-dd hh:mm:ss)
     *
     * @param Array $record
     * @return Array $record
     */
    protected function createTrueDataFromRecord($record) {
        $schedule_start_work_time = $this->getCarbonInstance($record['所定出勤時刻']);
        $schedule_end_work_time = $this->getCarbonInstance($record['所定退勤時刻']);
        $schedule_break_time = $record['所定休憩時間'] ?? 0;

        if (isset($schedule_start_work_time) && isset($schedule_end_work_time) && $schedule_start_work_time->gt($schedule_end_work_time))
            $schedule_end_work_time->addDay();
        if (isset($schedule_start_work_time) && isset($schedule_end_work_time)) {
            $schedule_working_hour = $schedule_end_work_time->diffInMinutes($schedule_start_work_time) - $schedule_break_time;
            $schedule_working_hour = $this->getCarbonInstance($this->minutesToString($schedule_working_hour));
        } else {
            $schedule_working_hour = $this->getCarbonInstance($record['所定労働時間']);
        }

        $paid_rest_time_start = $this->getCarbonInstance($record['時間休暇開始時刻']);
        $paid_rest_time_end = $this->getCarbonInstance($record['時間休暇終了時刻']);
        if (isset($paid_rest_time_start) && isset($paid_rest_time_end) && $paid_rest_time_start->gt($paid_rest_time_end))
            $paid_rest_time_end->addDay();

        $planned_early_arrive_start = $this->getCarbonInstance($record['早出開始時刻']);
        $planned_early_arrive_end = $this->getCarbonInstance($record['早出終了時刻']);
        if (isset($planned_early_arrive_start) && isset($planned_early_arrive_end) && $planned_early_arrive_start->gt($planned_early_arrive_end))
            $planned_early_arrive_end->addDay();

        $planned_overtime_start = $this->getCarbonInstance($record['残業開始時刻']);
        if (isset($planned_overtime_start)) {
            if (isset($schedule_start_work_time) && isset($schedule_start_work_time) && $schedule_start_work_time->gt($planned_overtime_start))
                $planned_overtime_start->addDay();
            else if (!isset($schedule_start_work_time) && !isset($schedule_start_work_time)
                && isset($schedule_working_hour) && $schedule_working_hour->gt($planned_overtime_start))
                $planned_overtime_start->addDay();
        }

        $planned_overtime_end = $this->getCarbonInstance($record['残業終了時刻']);
        if (isset($planned_overtime_end) && $planned_overtime_start->gt($planned_overtime_end)) $planned_overtime_end->addDay();

        if (isset($schedule_start_work_time)) $record['所定出勤時刻'] = $schedule_start_work_time->format('Y-m-d H:i:s');
        if (isset($schedule_end_work_time)) $record['所定退勤時刻'] = $schedule_end_work_time->format('Y-m-d H:i:s');
        if (isset($schedule_working_hour)) $record['所定労働時間'] = $schedule_working_hour->format('H:i:s');
        if (isset($paid_rest_time_start)) $record['時間休暇開始時刻'] = $paid_rest_time_start->format('Y-m-d H:i:s');
        if (isset($paid_rest_time_end)) $record['時間休暇終了時刻'] = $paid_rest_time_end->format('Y-m-d H:i:s');
        if (isset($planned_early_arrive_start)) $record['早出開始時刻'] = $planned_early_arrive_start->format('Y-m-d H:i:s');
        if (isset($planned_early_arrive_end)) $record['早出終了時刻'] = $planned_early_arrive_end->format('Y-m-d H:i:s');
        if (isset($planned_overtime_start)) $record['残業開始時刻'] = $planned_overtime_start->format('Y-m-d H:i:s');
        if (isset($planned_overtime_end)) $record['残業終了時刻'] = $planned_overtime_end->format('Y-m-d H:i:s');
        if (isset($schedule_start_work_time) || isset($schedule_end_work_time) || isset($schedule_working_hour)) $record['所定休憩時間'] = $schedule_break_time;

        return $record;
    }

    /**
     * The function will transfer format (hh:mm) to format (yyyy-mm-dd hh:mm:ss)
     *
     * @param Array $record
     * @return Array $record
     */
    protected function createTrueWorkTimeDataFromRecord($record) {
        $start_work_time = $this->convertStringToMintues($record['schedule_start_work_time'], 'H:i:s');
        $end_work_time = $this->convertStringToMintues($record['schedule_end_work_time'], 'H:i:s');

        $schedule_start_work_time = $this->getCarbonInstance($start_work_time);
        $schedule_end_work_time = $this->getCarbonInstance($end_work_time);

        if (isset($schedule_start_work_time) && isset($schedule_end_work_time) && $schedule_start_work_time->gt($schedule_end_work_time))
            $schedule_end_work_time->addDay();

        if (isset($schedule_start_work_time)) $record['schedule_start_work_time'] = $schedule_start_work_time->format('Y-m-d H:i:s');
        if (isset($schedule_end_work_time)) $record['schedule_end_work_time'] = $schedule_end_work_time->format('Y-m-d H:i:s');
        return $record;
    }

    /**
     * The function will transfer string to minutes
     *
     * @param string    $data
     * @param string       $format
     * @return string     minute(hh:mm).
     */
    protected function convertStringToMintues($data, $format = 'Y-m-d H:i:s')
    {
        if (isset($data)) return Carbon::createFromFormat($format, $data)->format('H:i');
        return null;
    }

    /**
     * Convert a given number of minutes to a time string. Format: 'hh:mm'
     *
     * @param int       $minutes
     * @return string
     */
    protected function minutesToString($minutes)
    {
        if ($minutes <= 0) {
            return '00:00:00';
        } else {
            return str_pad(floor($minutes/60), 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes%60, 2, '0', STR_PAD_LEFT) . ':00';
        }
    }
}

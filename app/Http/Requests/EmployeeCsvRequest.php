<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use League\Csv\Reader;
use League\Csv\Writer;
use League\Csv\CharsetConverter;
use Validator;
use Illuminate\Validation\Rule;
use App\Employee;
use App\WorkLocation;
use App\Company;
use Carbon\Carbon;
use App\WorkStatus;
use App\RestStatus;
use App\Department;
use Constants;
use App\Services\TodofukenService;
use App\Http\Requests\Reusables\ExtraValidations;
use Auth;

class EmployeeCsvRequest extends FormRequest
{
    /**
     * Constants for error of record
     *
     */
    const DATA_ERROR                    = 1;
    const SINSEI_CONSIDERING_ERROR      = 2;
    const CONCLUDED_ERROR               = 3;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'      => 'required|mimes:csv,txt',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $file = $this->file('file');
            if (!empty($file)) {
                $records = $true_data = [];
                $check_error = false;
                $message = '';
                $employees = Employee::all();
                $work_locations = WorkLocation::all();

                setlocale(LC_ALL, 'ja_JP.UTF-8');

                $csvData = file_get_contents($file);
                if (!mb_check_encoding($csvData, 'UTF-8'))
                    $csvData = mb_convert_encoding($csvData, "UTF-8", "SJIS");

                $csv = Reader::createFromString($csvData);
                $csv->setHeaderOffset(0);
                $header = $csv->getHeader();
                $isCurrentLocation = true;
                $type = 1;
                if (count($header) < 27 || $header[count($header) - 1] !== 'エラー') {
                    $type = 0;
                } else {
                    if ($header[0] == '従業員ID(必須)' && count($header) == 27) {
                        $type = 1;
                    } else if ($header[0] == 'ICカード登録用番号(※削除・変更不可)' && count($header) == 29){
                        $type = 2;
                    }
                }

                if ($type == 1) {
                    $tmp_list = $employee_list = $employees->pluck('presentation_id')->toArray();
                    foreach ($csv->getRecords() as $record) {
                        if ($record['従業員ID(必須)'] !== "Sample") {
                            $record = array_map(function($value){
                                return ($value !== "") ? trim($value, " \t\n") : null;
                            }, $record);
                            $tmp_month = $record['更新日(月/日)'];
                            $record['更新日(月/日)'] = isset($record['更新日(月/日)']) ? config('caeru.blind_year') . '/' . $record['更新日(月/日)'] : $record['更新日(月/日)'];
                            $sub_validator = $this->validationRecord($record, $work_locations);
                            if ($sub_validator->fails()) $record['エラー'] = head($sub_validator->errors()->keys());

                            $work_location = WorkLocation::where('presentation_id', $record['所属先ID(必須)'])->first();
                            if (!isset($record['エラー']) && isset($record['部署'])) {
                                $departments = $work_location->activatingDepartments()->pluck('name')->toArray();
                                if (!in_array($record['部署'], $departments))
                                    $record['エラー'] = '部署';
                            }

                            if (!isset($record['エラー'])) {
                                if (in_array($record['従業員ID(必須)'], $tmp_list)) {
                                    $record['エラー'] = '従業員ID(必須)';
                                }
                            }

                            if (!isset($record['エラー']) && isset($record['承認者ID'])) {
                                $chiefs = explode(',', $record['承認者ID']);
                                $employees = Auth::user()->company->employees->pluck('id')->toArray();
                                foreach($chiefs as $chief) {
                                    if (!in_array($chief, $employees)) {
                                        $record['エラー'] = '承認者ID';
                                        break;
                                    }
                                }
                            }
                            $tmp_list[] = $record['従業員ID(必須)'];

                            if (!isset($record['エラー'])) {

                                $data = [];
                                $todofuken_service = resolve(TodofukenService::class);
                                if (isset($record['部署'])) {
                                    $department = Department::where('name', $record['部署'])->first();
                                    $data['department_id'] = $department->id;
                                } else {
                                    $data['department_id'] = null;
                                }

                                $data['presentation_id'] = $record['従業員ID(必須)'];
                                $data['last_name'] = $record['従業員名(必須) 姓'];
                                $data['first_name'] = $record['従業員名(必須) 名'];
                                $data['password'] = $record['パスワード(英数)'];
                                $data['last_name_furigana'] = $record['従業員名(カナ)(必須) セイ'];
                                $data['first_name_furigana'] = $record['従業員名(カナ)(必須) メイ'];
                                $data['birthday'] = $record['生年月日(必須) (年/月/日)'];
                                $data['gender'] = array_search($record['性別(必須)'], Constants::genders());
                                $data['postal_code'] = str_replace('-', '', $record['郵便番号']);
                                $data['todofuken'] = '';
                                if (isset($record['都道府県']))
                                    $data['todofuken'] = $todofuken_service->getId($record['都道府県']);
                                $data['address'] = $record['住所:'];
                                $data['telephone'] = $record['電話番号'];
                                $data['email'] = $record['メールアドレス'];
                                $data['work_location_id'] = $work_location->id;
                                $data['joined_date'] = $record['入社日(必須)(年/月/日)'];
                                $data['schedule_type'] = array_search($record['就労形態(必須)'], Constants::scheduleTypes());
                                $data['employment_type'] = array_search($record['採用形態(必須)'], Constants::employmentTypes());
                                $data['salary_type'] = array_search($record['給与形態(必須)'], Constants::salaryTypes());
                                $data['work_status'] = array_search($record['雇用状態(必須)'], Constants::workStatuses());
                                $data['resigned_date'] = $record['退職日(年/月/日)'];
                                $data['chiefs'] = isset($record['承認者ID']) ? explode(',', $record['承認者ID']) : [];
                                $data['paid_holiday_exception'] = $record['有給対象外(チェックを入れる場合は1)'];
                                $data['holidays_update_day'] = $tmp_month;
                                $data['work_time_per_day'] = $record['１日の労働時間'];
                                if (isset($record['有給休暇タイプ']))
                                    $data['holiday_bonus_type'] = array_search($record['有給休暇タイプ'], Constants::holidayBonusTypes());
                                else
                                    $data['holiday_bonus_type'] = 1;
                                $true_data[] = $data;
                            } else {
                                $check_error = true;
                            }
                            $record['更新日(月/日)'] = $tmp_month;
                            $records[] = $record;
                        }
                    }
                } else if ($type == 2) {
                    foreach ($csv->getRecords() as $record) {
                        $record = array_map(function($value){
                            return ($value !== "") ? trim($value, " \t\n") : null;
                        }, $record);
                        $tmp_password = $record['パスワード(英数)'];
                        if ($record['パスワード(英数)'] == '有')
                            $record['パスワード(英数)'] = null;
                        $tmp_month = $record['更新日(月/日)'];
                        $record['更新日(月/日)'] = isset($record['更新日(月/日)']) ? config('caeru.blind_year') . '/' . $record['更新日(月/日)'] : $record['更新日(月/日)'];
                        $sub_validator = $this->validationUpdateRecord($record, $work_locations, $employees);
                        if ($sub_validator->fails()) $record['エラー'] = head($sub_validator->errors()->keys());

                        $work_location = WorkLocation::where('presentation_id', $record['所属先ID(必須)'])->first();
                        if (!isset($record['エラー']) && isset($record['部署'])) {
                            $departments = $work_location->activatingDepartments()->pluck('name')->toArray();
                            if (!in_array($record['部署'], $departments))
                                $record['エラー'] = '部署';
                        }

                        if (!isset($record['エラー']) && isset($record['並び順'])) {
                            if (session('current_work_location') == 'all')
                                $record['エラー'] = '並び順';
                        }

                        if (!isset($record['エラー']) && isset($record['承認者ID'])) {
                            $chiefs = explode(',', $record['承認者ID']);
                            $employees = Auth::user()->company->employees->pluck('id')->toArray();
                            foreach($chiefs as $chief) {
                                if (!in_array($chief, $employees)) {
                                    $record['エラー'] = '承認者ID';
                                    break;
                                }
                            }
                        }

                        if (!isset($record['エラー'])) {

                            $data = [];
                            $todofuken_service = resolve(TodofukenService::class);
                            if (isset($record['部署'])) {
                                $department = Department::where('name', $record['部署'])->first();
                                $data['department_id'] = $department->id;
                            } else {
                                $data['department_id'] = null;
                            }

                            if (isset($record['並び順']))
                                $data['view_order'] = $record['並び順'] + 1;
                            else
                                $data['view_order'] = '';
                            $data['card_registration_number'] = $record['ICカード登録用番号(※削除・変更不可)'];
                            $data['presentation_id'] = $record['従業員ID(必須)'];
                            $data['password'] = $record['パスワード(英数)'];
                            $data['last_name'] = $record['従業員名(必須) 姓'];
                            $data['first_name'] = $record['従業員名(必須) 名'];
                            $data['last_name_furigana'] = $record['従業員名(カナ)(必須) セイ'];
                            $data['first_name_furigana'] = $record['従業員名(カナ)(必須) メイ'];
                            $data['birthday'] = $record['生年月日(必須) (年/月/日)'];
                            $data['gender'] = array_search($record['性別(必須)'], Constants::genders());
                            $data['postal_code'] = str_replace('-', '', $record['郵便番号']);
                            $data['todofuken'] = '';
                            if (isset($record['都道府県']))
                                $data['todofuken'] = $todofuken_service->getId($record['都道府県']);
                            $data['address'] = $record['住所:'];
                            $data['telephone'] = $record['電話番号'];
                            $data['email'] = $record['メールアドレス'];
                            $data['work_location_id'] = $work_location->id;
                            $data['joined_date'] = $record['入社日(必須)(年/月/日)'];
                            $data['schedule_type'] = array_search($record['就労形態(必須)'], Constants::scheduleTypes());
                            $data['employment_type'] = array_search($record['採用形態(必須)'], Constants::employmentTypes());
                            $data['salary_type'] = array_search($record['給与形態(必須)'], Constants::salaryTypes());
                            $data['work_status'] = array_search($record['雇用状態(必須)'], Constants::workStatuses());
                            $data['resigned_date'] = $record['退職日(年/月/日)'];
                            $data['chiefs'] = isset($record['承認者ID']) ? explode(',', $record['承認者ID']) : [];
                            $data['paid_holiday_exception'] = $record['有給対象外(チェックを入れる場合は1)'];
                            $data['holidays_update_day'] = $tmp_month;
                            $data['work_time_per_day'] = $record['１日の労働時間'];
                            if (isset($record['有給休暇タイプ']))
                                $data['holiday_bonus_type'] = array_search($record['有給休暇タイプ'], Constants::holidayBonusTypes());
                            else
                                $data['holiday_bonus_type'] = 1;
                            $true_data[] = $data;
                        } else {
                            $check_error = true;
                        }
                        $record['パスワード(英数)'] = $tmp_password;
                        $record['更新日(月/日)'] = $tmp_month;
                        $records[] = $record;
                    }
                } else {
                    $check_error = true;
                    $records = $csv->getRecords();
                    $message = 'ファイルのフォーマットが正確ではありません。';
                }
                if ($check_error) {
                    $this->writeCSV($csv->getHeader(), $records, $message, $type);
                }
                else {
                    $this->merge(['type' => $type]);
                    $this->merge(['true_data' => $true_data]);
                }
            }
        });
    }

    /**
     * validation record
     *
     * @param  array $record
     * @return Validator $sub_validator
     */
    protected function validationRecord($record, $work_locations) {

        $todofuken_service = resolve(TodofukenService::class);
        $todofuken_list = $todofuken_service->getNames();

        $chosen_work_location = session('current_work_location');
        $list = null;
        if ($chosen_work_location === 'all') {
            $list = $work_locations->pluck('presentation_id')->toArray();
        } else {
            $tmp = $work_locations->whereStrict('id', intval($chosen_work_location))->first();
            $list = [$tmp->presentation_id];
        }

        $sub_validator = Validator::make($record, [
            '従業員ID(必須)' => 'required|max:80',
            'パスワード(英数)' => 'nullable|regex:/^([0-9A-Za-z!@#$&*_])*$/u',
            '従業員名(必須) 姓' => 'required|max:80',
            '従業員名(必須) 名' => 'required|max:80',
            '従業員名(カナ)(必須) セイ' => 'required|max:240|furigana',
            '従業員名(カナ)(必須) メイ' => 'required|max:240|furigana',
            '生年月日(必須) (年/月/日)' => 'required|date|date_format:Y/m/d',
            '性別(必須)' => [
                'required',
                Rule::in(array_values(Constants::genders())),
            ],
            '郵便番号' => 'nullable|string|max:8|regex:/^[0-9]{3}-[0-9]{4}$/',
            '都道府県' => [
                'nullable',
                Rule::in($todofuken_list),
            ],
            '住所:' => 'nullable',
            '電話番号' => 'nullable|string|max:15|regex:/^[0-9]{0,4}-[0-9]{0,4}-[0-9]{0,5}$/',
            'メールアドレス' => 'nullable|email',
            '所属先ID(必須)' => [
                'required',
                Rule::in($list),
            ],
            '入社日(必須)(年/月/日)' => 'required|date|date_format:Y/m/d',
            '部署' => 'nullable',
            '就労形態(必須)' => [
                'required',
                Rule::in(array_values(Constants::scheduleTypes())),
            ],
            '採用形態(必須)' => [
                'required',
                Rule::in(array_values(Constants::employmentTypes())),
            ],
            '給与形態(必須)' => [
                'required',
                Rule::in(array_values(Constants::salaryTypes())),
            ],
            '雇用状態(必須)' => [
                'required',
                Rule::in(array_values(Constants::workStatuses())),
            ],
            '退職日(年/月/日)' => 'nullable|date|date_format:Y/m/d',
            '承認者ID' => 'nullable',
            '有給対象外(チェックを入れる場合は1)' => 'nullable|boolean',
            '更新日(月/日)' => 'nullable|date|date_format:Y/m/d',
            '１日の労働時間' => 'nullable|numeric|max:24|min:0',
            '有給休暇タイプ' => [
                'nullable',
                Rule::in(array_values(Constants::holidayBonusTypes())),
            ],
            'エラー' => 'nullable',
        ]);

        return $sub_validator;
    }

    /**
     * validation update record
     *
     * @param  array $record
     * @return Validator $sub_validator
     */
    protected function validationUpdateRecord($record, $work_locations, $employees) {

        $employee_presentation_ids = $employees->pluck('presentation_id')->toArray();
        $employee_register_cards = $employees->pluck('card_registration_number')->toArray();
        $todofuken_service = resolve(TodofukenService::class);
        $todofuken_list = $todofuken_service->getNames();

        $chosen_work_location = session('current_work_location');
        $list = null;
        if ($chosen_work_location === 'all') {
            $list = $work_locations->pluck('presentation_id')->toArray();
        } else {
            $tmp = $work_locations->whereStrict('id', intval($chosen_work_location))->first();
            $list = [$tmp->presentation_id];
        }

        $sub_validator = Validator::make($record, [
            'ICカード登録用番号(※削除・変更不可)' => [
                'required',
                'string',
                'min:8',
                'max:8',
                Rule::in($employee_register_cards),
            ],
            '従業員ID(必須)' => [
                'required',
                Rule::in($employee_presentation_ids),
            ],
            'パスワード(英数)' => 'nullable|regex:/^([0-9A-Za-z!@#$&*_])*$/u',
            '従業員名(必須) 姓' => 'required|max:80',
            '従業員名(必須) 名' => 'required|max:80',
            '従業員名(カナ)(必須) セイ' => 'required|max:240|furigana',
            '従業員名(カナ)(必須) メイ' => 'required|max:240|furigana',
            '生年月日(必須) (年/月/日)' => 'required|date|date_format:Y/m/d',
            '性別(必須)' => [
                'required',
                Rule::in(array_values(Constants::genders())),
            ],
            '郵便番号' => 'nullable|string|max:8|regex:/^[0-9]{3}-[0-9]{4}$/',
            '都道府県' => [
                'nullable',
                Rule::in($todofuken_list),
            ],
            '住所:' => 'nullable',
            '電話番号' => 'nullable|string|max:15|regex:/^[0-9]{0,4}-[0-9]{0,4}-[0-9]{0,5}$/',
            'メールアドレス' => 'nullable|email',
            '所属先ID(必須)' => [
                'required',
                Rule::in($list),
            ],
            '入社日(必須)(年/月/日)' => 'required|date|date_format:Y/m/d',
            '部署' => 'nullable',
            '就労形態(必須)' => [
                'required',
                Rule::in(array_values(Constants::scheduleTypes())),
            ],
            '採用形態(必須)' => [
                'required',
                Rule::in(array_values(Constants::employmentTypes())),
            ],
            '給与形態(必須)' => [
                'required',
                Rule::in(array_values(Constants::salaryTypes())),
            ],
            '雇用状態(必須)' => [
                'required',
                Rule::in(array_values(Constants::workStatuses())),
            ],
            '退職日(年/月/日)' => 'nullable|date|date_format:Y/m/d',
            '承認者ID' => 'nullable',
            '有給対象外(チェックを入れる場合は1)' => 'nullable|boolean',
            '更新日(月/日)' => 'nullable|date|date_format:Y/m/d',
            '１日の労働時間' => 'nullable|numeric|max:24|min:0',
            '有給休暇タイプ' => [
                'nullable',
                Rule::in(array_values(Constants::holidayBonusTypes())),
            ],
            'エラー' => 'nullable',
        ]);

        return $sub_validator;
    }

    /**
     * Export CSV
     *
     * @param  array $header
     * @param  array(array) $records
     */
    protected function writeCSV($header, $records, $message, $type) {
        //load the CSV document from a string
        $writer = Writer::createFromString('');

        if ($message !== '') {
            if ($header[count($header) - 1] === 'エラー')
                $header[count($header) - 1] = $message;
            else
                $header[count($header)] = $message;
        }

        //insert the header
        $writer->insertOne($header);

        //insert all the records
        $writer->insertAll($records);

        if ($type == 1)
            $writer->output('employee.csv');
        else
            $writer->output('employee_list.csv');
        die;
    }
}

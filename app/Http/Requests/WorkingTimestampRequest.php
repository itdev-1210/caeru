<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\WorkingTimestamp;
use App\Company;
use Carbon\Carbon;

class WorkingTimestampRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $valid_types = [
            WorkingTimestamp::START_WORK,
            WorkingTimestamp::END_WORK,
            WorkingTimestamp::GO_OUT,
            WorkingTimestamp::RETURN,
        ];

        return [
            'enable'                        => 'required|boolean',
            'processed_date_value'          => 'required|date',
            'processed_time_value'          => 'required|time',
            'timestamped_type'              => [
                'required',
                Rule::in($valid_types),
            ],
            'work_location_id'              => 'required|exists:work_locations,id',
            'work_address_id'               => 'nullable|exists:work_addresses,id',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            // Validate the time of the working timestamp with company's setting
            $working_day = $this->route('working_day');
            $company = $this->user()->company;

            $input_date_value = $this->input('processed_date_value');
            $input_time_value = $this->input('processed_time_value');

            if (isset($input_date_value) && isset($input_time_value)) {
                $date_from_the_request = Carbon::createFromFormat('Y-m-d H:i:s', $input_date_value . ' ' . $input_time_value . ':00');
                $start_limit = $company->date_separate_type == Company::APPLY_TO_THE_DAY_BEFORE ?
                                Carbon::createFromFormat('Y-m-d H:i:s', $working_day->date . ' ' . $company->date_separate_time . ':00') :
                                Carbon::createFromFormat('Y-m-d H:i:s', $working_day->date . ' ' . $company->date_separate_time . ':00')->subDay();


                if ($date_from_the_request->lt($start_limit)) {
                    $validator->errors()->add('processed_time_value', '正しい打刻時刻を入力してください');
                }

                // We have to check the end_limit if this WorkingTimestamp is START_WORK type.
                if ($this->input('timestamped_type') === WorkingTimestamp::START_WORK) {
                    $end_limit = $start_limit->addDay();
                    if ($date_from_the_request->gt($end_limit)) {
                        $validator->errors()->add('processed_time_value', '正しい打刻時刻を入力してください');
                    }
                }
            }
        });
    }
}

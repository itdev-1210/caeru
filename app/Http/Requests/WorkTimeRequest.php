<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\Reusables\PostValidationProcesses;
use Constants;

class WorkTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'list_work_time_default.*.start_work_time'               =>  'nullable|time',
            'list_work_time_default.*.end_work_time'                 =>  'nullable|time',
            'list_work_time_default.*.break_time'                    =>  'required|numeric',
            'list_work_time_default.*.night_break_time'              =>  'nullable|numeric',
            'list_work_time_default.*.working_hour'                  =>  'required|time',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        
        // Post validation processes
        $validator->after(function ($validator) {
            $this->list_work_time_default = $this->changeToTimeFormat($this->list_work_time_default, 'start_work_time');
            $this->list_work_time_default = $this->changeToTimeFormat($this->list_work_time_default, 'end_work_time');
            $this->list_work_time_default = $this->changeToTimeFormat($this->list_work_time_default, 'working_hour');
        });
    }

    public function changeToTimeFormat($datas, $field_name)
    {
        for($i = 0; $i < count($datas); $i++) {
            $time = explode(':', $datas[$i][$field_name]);

            if ((count($time) == 1) && (strlen($time[0]) == 4)) {
                $datas[$i][$field_name] = substr($time[0], 0, 2) . ':' . substr($time[0], -2);
            }
        }
        return $datas;
    }

}

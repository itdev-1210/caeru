<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Constants;

class PaidHolidayInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'attendance_rate'                     => 'numeric|max:100|min:0',
            'provided_paid_holidays'              => 'numeric',
            'carried_forward_paid_holidays'       => 'numeric',
            'carried_forward_paid_holidays_hour'  => 'date_format:H:i',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $carried_forward_time = $this->toMinutes($this->input('carried_forward_paid_holidays_hour'));

        $first_period = $this->route('employee')->paidHolidayInformations()->orderBy('period_start', 'des')->first();

        if ($carried_forward_time > $first_period->work_time_per_day) {
            $validator->after(function ($validator) {
                $validator->errors()->add('carried_forward_paid_holidays_hour', '繰り越し時間は大き過ぎました。');
            });
        }
    }

    /**
     * Convert a time string back to number of minutes.
     *
     * @param string        $time_string    format: HH:mm
     * @return int|null
     */
    protected function toMinutes($time_string)
    {
        $data = explode(':', $time_string);

        if (count($data) == 2) {
            return floatval($data[0]) * 60 + floatval($data[1]);
        } else {
            return null;
        }
    }

}

<?php

namespace App\Http\Requests\Sinsei\Requester;

use Illuminate\Foundation\Http\FormRequest;
use App\Employee;

class UpdateEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_email'          => 'nullable|email',
            'new_email'              => 'confirmed|email',
            'new_email_confirmation' => 'required|email'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->current_email !== Employee::find(session('sinsei_user')->id)->email)
                $validator->errors()->add('current_email', '現在メールアドレスが正しくありません。');
        });
    }
}

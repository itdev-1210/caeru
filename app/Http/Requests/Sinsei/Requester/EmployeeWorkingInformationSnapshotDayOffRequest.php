<?php

namespace App\Http\Requests\Sinsei\Requester;

use Illuminate\Foundation\Http\FormRequest;
use App\WorkLocation;
use Illuminate\Validation\Rule;
use App\RestStatus;

class EmployeeWorkingInformationSnapshotDayOffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'snapshots.*.employee_working_day_id'                                       =>  'required|exists:employee_working_days,id',
            'snapshots.*.employee_working_information_id'                               =>  'required|exists:employee_working_informations,id',

            'snapshots.*.left_timestamped_start_work_date'                              => 'nullable|date_format:Y-m-d',
            'snapshots.*.left_timestamped_start_work_time'                              => 'nullable|date_format:H:i:s',
            'snapshots.*.left_timestamped_end_work_date'                                => 'nullable|date_format:Y-m-d',
            'snapshots.*.left_timestamped_end_work_time'                                => 'nullable|date_format:H:i:s',
            'snapshots.*.left_timestamped_end_work_time_work_location_id'               =>  'nullable|exists:work_locations,id',
            'snapshots.*.left_switch_planned_schedule_target'                           => 'sometimes|nullable|date_format:Y-m-d',
            'snapshots.*.left_planned_work_location_id'                                 => 'required|exists:work_locations,id',

            'snapshots.*.left_paid_rest_time_start'                                     =>  'nullable|date_format:Y-m-d H:i:s',
            'snapshots.*.left_paid_rest_time_end'                                       =>  'nullable|date_format:Y-m-d H:i:s',
            'snapshots.*.left_paid_rest_time_period'                                    =>  'nullable|date_format:H:i:s',

            'snapshots.*.left_planned_early_arrive_start'                               => 'nullable|required_with:snapshots.*.left_planned_early_arrive_end|date_format:Y-m-d H:i:s',
            'snapshots.*.left_planned_early_arrive_end'                                 => 'nullable|required_with:snapshots.*.left_planned_early_arrive_start|date_format:Y-m-d H:i:s',
            'snapshots.*.left_planned_overtime_start'                                   => 'nullable|required_with:snapshots.*.left_planned_overtime_end|date_format:Y-m-d H:i:s',
            'snapshots.*.left_planned_overtime_end'                                     => 'nullable|required_with:snapshots.*.left_planned_overtime_start|date_format:Y-m-d H:i:s',

            'snapshots.*.left_planned_break_time'                                       =>  'nullable|integer',
            'snapshots.*.left_real_break_time'                                          =>  'nullable|integer',
            'snapshots.*.left_planned_night_break_time'                                 =>  'nullable|integer',
            'snapshots.*.left_real_night_break_time'                                    =>  'nullable|integer',
            'snapshots.*.left_planned_late_time'                                        =>  'nullable|integer',
            'snapshots.*.left_planned_early_leave_time'                                 =>  'nullable|integer',
            'snapshots.*.left_planned_go_out_time'                                      =>  'nullable|integer',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        foreach ($this->snapshots as $key => $snapshot) {
            $work_location = WorkLocation::find($snapshot['left_planned_work_location_id']);
            $available_work_statuses = $work_location->activatingWorkStatuses()->pluck('id')->toArray();
            $available_rest_statuses = $work_location->activatingRestStatuses()->pluck('id')->toArray();

            $validator->addRules([
                'snapshots.' . $key . '.left_work_status_id' => [
                    'sometimes',
                    'nullable',
                    Rule::in($available_work_statuses),
                ],
                'snapshots.' . $key . '.left_rest_status_id' => [
                    'sometimes',
                    'nullable',
                    Rule::in($available_rest_statuses),
                ],
            ]);

            if (isset($snapshot['left_rest_status_id'])) {
                if (!in_array($snapshot['left_rest_status_id'], [RestStatus::GOKYUU_1, RestStatus::GOKYUU_2, RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2])
                    && RestStatus::find($snapshot['left_rest_status_id'])->unit_type != true) {
                    if (isset($snapshot['left_schedule_start_work_time']) || isset($snapshot['left_schedule_end_work_time'])) {
                        $validator->addRules([
                            'snapshots.' . $key . '.left_paid_rest_time_start' => 'required|date_format:Y-m-d H:i:s',
                            'snapshots.' . $key . '.left_paid_rest_time_end' => 'required|date_format:Y-m-d H:i:s',
                        ]);
                    } else {
                        $validator->addRules([
                            'snapshots.' . $key . '.left_paid_rest_time_period' => 'required|date_format:H:i:s',
                        ]);
                    }
                }
            }
        }
    }
}

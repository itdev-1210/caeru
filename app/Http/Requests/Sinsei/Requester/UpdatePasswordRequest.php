<?php

namespace App\Http\Requests\Sinsei\Requester;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use App\Employee;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password'          => 'required|regex:/^([0-9A-Za-z!@#$&*_])*$/u',
            'new_password'              => 'confirmed|regex:/^([0-9A-Za-z!@#$&*_])*$/u',
            'new_password_confirmation' => 'required|regex:/^([0-9A-Za-z!@#$&*_])*$/u'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!Hash::check($this->current_password, Employee::find(session('sinsei_user')->id)->password))
                $validator->errors()->add('current_password', '現在のパスワードが正しくありません。');
            if ($this->current_password === $this->new_password)
                $validator->errors()->add('new_password', '同じパスワードを入力してください。');
        });
    }
}

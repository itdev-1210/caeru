<?php

namespace App\Http\Requests\Sinsei\Requester;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\Reusables\PostValidationProcesses;

class MaebaraiSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->input('maebarai_bank_name') != '' || $this->input('maebarai_bank_code') != '' ||
            $this->input('maebarai_branch_name') != '' || $this->input('maebarai_branch_code') != '' ||
            $this->input('maebarai_account_number') != '' || $this->input('maebarai_account_name') != '')
        {
            return [
                'maebarai_bank_name'        =>  'required',
                'maebarai_bank_code'        =>  'required | string | max: 4 | regex:/^[0-9]+$/',
                'maebarai_branch_name'      =>  'required',
                'maebarai_branch_code'      =>  'required | string | max: 3 | regex:/^[0-9]+$/',
                'maebarai_account_number'   =>  'required | string | max: 7 | regex:/^[0-9]+$/',
                'maebarai_account_name'     =>  'required',
            ];
        } else {
            return [];
        }
    }
}

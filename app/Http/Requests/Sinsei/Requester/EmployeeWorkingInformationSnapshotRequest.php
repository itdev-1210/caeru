<?php

namespace App\Http\Requests\Sinsei\Requester;

use Illuminate\Foundation\Http\FormRequest;
use App\WorkLocation;
use Illuminate\Validation\Rule;
use App\WorkStatus;
use App\RestStatus;

class EmployeeWorkingInformationSnapshotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'snapshot.employee_working_day_id'                          =>  'required|exists:employee_working_days,id',
            'snapshot.employee_working_information_id'                  =>  'sometimes|nullable|exists:employee_working_informations,id',

            'snapshot.left_timestamped_start_work_date'                 =>  'sometimes|nullable|date_format:Y-m-d|required_with:snapshot.left_timestamped_start_work_time',
            'snapshot.left_timestamped_start_work_time'                 =>  'sometimes|nullable|date_format:H:i:s|required_with:snapshot.left_timestamped_start_work_date',
            'snapshot.left_timestamped_end_work_date'                   =>  'sometimes|nullable|date_format:Y-m-d|required_with:snapshot.left_timestamped_end_work_time',
            'snapshot.left_timestamped_end_work_time'                   =>  'sometimes|nullable|date_format:H:i:s|required_with:snapshot.left_timestamped_end_work_date',
            'snapshot.left_timestamped_end_work_time_work_location_id'  =>  'nullable|exists:work_locations,id',
            'snapshot.left_switch_planned_schedule_target'              =>  'sometimes|nullable|date_format:Y-m-d',
            'snapshot.left_planned_work_location_id'                    =>  'required|exists:work_locations,id',

            'snapshot.left_paid_rest_time_start'                        =>  'nullable|date_format:Y-m-d H:i:s',
            'snapshot.left_paid_rest_time_end'                          =>  'nullable|date_format:Y-m-d H:i:s',
            'snapshot.left_paid_rest_time_period'                       =>  'nullable|date_format:H:i:s',

            'snapshot.left_planned_early_arrive_start'                  =>  'nullable|required_with:snapshot.left_planned_early_arrive_end|date_format:Y-m-d H:i:s',
            'snapshot.left_planned_early_arrive_end'                    =>  'nullable|required_with:snapshot.left_planned_early_arrive_start|date_format:Y-m-d H:i:s',
            'snapshot.left_planned_overtime_start'                      =>  'nullable|required_with:snapshot.left_planned_overtime_end|date_format:Y-m-d H:i:s',
            'snapshot.left_planned_overtime_end'                        =>  'nullable|required_with:snapshot.left_planned_overtime_start|date_format:Y-m-d H:i:s',

            'snapshot.left_planned_break_time'                          =>  'nullable|integer',
            'snapshot.left_real_break_time'                             =>  'nullable|integer',
            'snapshot.left_planned_night_break_time'                    =>  'nullable|integer',
            'snapshot.left_real_night_break_time'                       =>  'nullable|integer',
            'snapshot.left_planned_late_time'                           =>  'nullable|integer',
            'snapshot.left_planned_early_leave_time'                    =>  'nullable|integer',
            'snapshot.left_planned_go_out_time'                         =>  'nullable|integer',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if (isset($this->snapshot['left_planned_work_location_id'])) {
            $work_location = WorkLocation::find($this->snapshot['left_planned_work_location_id']);
            $available_work_statuses = $work_location->activatingWorkStatuses()->pluck('id')->toArray();
            $available_rest_statuses = $work_location->activatingRestStatuses()->pluck('id')->toArray();

            $validator->addRules([
                'snapshot.left_work_status_id' => [
                    'sometimes',
                    'nullable',
                    Rule::in($available_work_statuses),
                ],
                'snapshot.left_rest_status_id' => [
                    'sometimes',
                    'nullable',
                    Rule::in($available_rest_statuses),
                ],
            ]);
            if (isset($this->snapshot['left_rest_status_id'])) {
                if (!in_array($this->snapshot['left_rest_status_id'], [RestStatus::GOKYUU_1, RestStatus::GOKYUU_2, RestStatus::ZENKYUU_1, RestStatus::ZENKYUU_2])
                    && RestStatus::find($this->snapshot['left_rest_status_id'])->unit_type != true) {
                    if (isset($this->snapshot['left_schedule_start_work_time']) || isset($this->snapshot['left_schedule_end_work_time'])) {
                        $validator->addRules([
                            'snapshot.left_paid_rest_time_start' => 'required|date_format:Y-m-d H:i:s',
                            'snapshot.left_paid_rest_time_end' => 'required|date_format:Y-m-d H:i:s',
                        ]);
                    } else {
                        $validator->addRules([
                            'snapshot.left_paid_rest_time_period' => 'required|date_format:H:i:s',
                        ]);
                    }
                }
            }
        }
        $validator->after(function ($validator) {
            if (!isset($this->snapshot['left_schedule_break_time']) && !isset($this->snapshot['left_schedule_end_work_time'])
                && !isset($this->snapshot['left_schedule_night_break_time']) && !isset($this->snapshot['left_schedule_start_work_time'])
                && !isset($this->snapshot['left_schedule_working_hour'])) {
                if (!in_array($this->snapshot['left_work_status_id'], [WorkStatus::HOUDE, WorkStatus::KYUUDE, WorkStatus::ZANGYOU])) {
                    $check = false;
                    if (in_array('left_work_status_id', $this->array_attribute_name))
                        $check = true;
                    $this->array_attribute_name = [];
                    if ($check) $this->array_attribute_name[] = 'left_work_status_id';
                    else {
                        if (!isset($this->snapshot['id']) && !isset($this->snapshot['employee_working_information_id']))
                            $validator->errors()->add('error', 'error!');
                    }
                } else {
                    if (!isset($this->snapshot['left_planned_overtime_start']) || !isset($this->snapshot['left_planned_overtime_end'])) {
                        $validator->errors()->add('left_planned_overtime_start', 'left_planned_overtime_start is required');
                        $validator->errors()->add('left_planned_overtime_end', 'left_planned_overtime_end is required');
                    }
                }
            }
        });
    }
}

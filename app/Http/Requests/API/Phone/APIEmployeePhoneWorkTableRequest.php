<?php

namespace App\Http\Requests\API\Phone;

use Illuminate\Foundation\Http\FormRequest;

class APIEmployeePhoneWorkTableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_month' => 'nullable|date_format:Y-m'
        ];
    }
}

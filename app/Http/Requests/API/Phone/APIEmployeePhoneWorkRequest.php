<?php

namespace App\Http\Requests\API\Phone;

use Illuminate\Foundation\Http\FormRequest;
use App\WorkAddress;
use App\WorkingTimestamp;
use Illuminate\Validation\Rule;
use App\Employee;
use Coordinate;
use Carbon\Carbon;
use App\EmployeeWorkingDay;
use DB;

class APIEmployeePhoneWorkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $list_work_address = WorkAddress::enable()->pluck('id')->toArray();

        return [
            'work_address_id'   => [
                'required',
                'numeric',
                Rule::in($list_work_address),
            ],
            'lat'           => 'required|numeric|min:-90|max:90',
            'lon'           => 'required|numeric|min:-180|max:180',
            'type'   => [
                'required',
                Rule::in([WorkingTimestamp::START_WORK,
                         WorkingTimestamp::END_WORK]),
            ]
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $employee = Employee::where('device_id', $this->device_id)->first();

            $work_address = WorkAddress::find($this->work_address_id);

            if (!$this->checkDistance($work_address, $this->lat, $this->lon)) {
                $validator->errors()->add('location', '勤務地に移動してください');
            }

            if (!$this->check5mintues($work_address, $employee)) {
                $validator->errors()->add('time', '5分以上間隔を空けてください');
            }

            $this->employee = $employee;
            $this->work_address = $work_address;
        });
    }

    /**
     * The function would be check location
     *
     * @param  App\WorkAddress  $work_address
     * @param  numeric  $lat
     * @param  numeric  $lon
     * @return boolean
     */
    private function checkDistance($work_address, $lat, $lon) {
        $distance = Coordinate::distance($lat, $lon, $work_address->latitude, $work_address->longitude, "K");

        return $distance * 1000 <= $work_address->login_range;
    }

    /**
     * The function would be check dakoku must be 5 minutes later than previous dakoku
     *
     * @param  App\WorkAddress  $work_address
     * @param  numeric  $lat
     * @param  numeric  $lon
     * @return boolean
     */
    private function check5mintues($work_address, $employee) {
        $before_5_minutes_timestamp = Carbon::now()->subMinutes(5)->timestamp;

        if (DB::table('tablet_temporary_working_timestamps')
                ->where('work_address_id', $work_address->id)
                ->where('device_id', $employee->device_id)
                ->whereIn('timestamped_type', [WorkingTimestamp::START_WORK, WorkingTimestamp::END_WORK])
                ->where('timestamped_value', '>=', $before_5_minutes_timestamp)
                ->first()) {
            return false;
        }

        $employee_working_days = EmployeeWorkingDay::where('employee_id', $employee->id)
                                    ->whereIn('date', [Carbon::yesterday()->toDateString(), Carbon::today()->toDateString(), Carbon::tomorrow()->toDateString()])
                                    ->pluck('id')->toArray();

        if (WorkingTimestamp::where('work_address_id', $work_address->id)
                            ->where('registerer_type', WorkingTimestamp::MOBILE_APP)
                            ->whereIn('employee_working_day_id', $employee_working_days)
                            ->whereIn('timestamped_type', [WorkingTimestamp::START_WORK, WorkingTimestamp::END_WORK])
                            ->where('timestamped_value', '>=', $before_5_minutes_timestamp)
                            ->exists()) {
            return false;
        }

        return true;
    }
}

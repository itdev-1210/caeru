<?php

namespace App\Http\Requests\API\Phone;

use Illuminate\Foundation\Http\FormRequest;
use App\WorkAddress;
use Illuminate\Validation\Rule;

class APIEmployeePhoneChooseWorkAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $list_work_address = WorkAddress::enable()->pluck('id')->toArray();

        return [
            'work_address_id'   => [
                'required',
                'numeric',
                Rule::in($list_work_address),
            ]
        ];
    }
}

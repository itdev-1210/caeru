<?php

namespace App\Http\Requests\API\Phone;

use Illuminate\Foundation\Http\FormRequest;
use App\Employee;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class APIEmployeePhoneLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gps_number'                => 'required|max:255|exists:employees',
            'password'                  => 'required|max:255',
            'device_id'                 => 'required|max:255',
            'company_code'              => 'required|max:32',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->addRules([
                                'device_id' => [
                                                'required',
                                                'max:255',
                                                Rule::unique('employees')->ignore($this->input('gps_number'), 'gps_number'),
                                            ]
                            ]);

        $validator->after(function ($validator) {
            $employee = Employee::where('gps_number', $this->input('gps_number'))->first();

            if (isset($employee) && isset($employee->device_id) && $employee->device_id !== $this->input('device_id')) {
                $validator->errors()->add('device_id', __('validation.unique'));
            }

            if (isset($employee) && !Hash::check($this->input('password'), $employee->password))
                $validator->errors()->add('Error account', 'GPS端末番号またはパスワードが違います');
        });
    }
}

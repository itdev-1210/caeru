<?php

namespace App\Http\Requests\API\JupiterTablet;

use Illuminate\Foundation\Http\FormRequest;
use App\WorkLocation;
use App\WorkingTimestamp;
use Illuminate\Validation\Rule;
use App\Employee;

class APIJupiterOfflineDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $list_work_location = WorkLocation::enable()->pluck('id')->toArray();
        $list_work_address = WorkLocation::find($this->work_location_id)->workAddresses->pluck('id')->toArray();
        return [
            'work_location_id'   => [
                'required',
                'numeric',
                Rule::in($list_work_location),
            ],
            'work_address_id'   => [
                'nullable',
                'numeric',
                Rule::in($list_work_address),
            ],
            'data.*.timezone'   => 'required|timezone',
            'data.*.timestamp'   => 'required|numeric',
            'data.*.type'   => [
                'required',
                Rule::in([WorkingTimestamp::START_WORK,
                         WorkingTimestamp::END_WORK,
                         WorkingTimestamp::GO_OUT,
                         WorkingTimestamp::RETURN]),
            ],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $card_number = Employee::pluck('card_number')->filter()->toArray();
            $true_data = [];
            foreach ($this->data as $value) {
                if (in_array($value['card_code'], $card_number)) {
                    $true_data[] = $value;
                }
            }
            $this->data = $true_data;
        });
    }
}

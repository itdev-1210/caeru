<?php

namespace App\Http\Requests\API\Tablet;

use Illuminate\Foundation\Http\FormRequest;
use App\WorkLocation;
use App\Employee;
use Illuminate\Validation\Rule;

class APITabletGetEmployeeNameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $list_work_location = WorkLocation::enable()->pluck('id')->toArray();
        $list_work_address = WorkLocation::find($this->work_location_id)->workAddresses->pluck('id')->toArray();
        $card_registration_number = Employee::whereNull('card_number')->get()->pluck('card_registration_number')->toArray();
        return [
            'work_location_id'   => [
                'required',
                'numeric',
                Rule::in($list_work_location),
            ],
            'work_address_id'   => [
                'nullable',
                'numeric',
                Rule::in($list_work_address),
            ],
            'card_registration_number'   => [
                'required',
                Rule::in($card_registration_number),
            ]
        ];
    }
}

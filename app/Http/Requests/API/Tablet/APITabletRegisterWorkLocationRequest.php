<?php

namespace App\Http\Requests\API\Tablet;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\WorkLocation;

class APITabletRegisterWorkLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $work_location_ids = WorkLocation::enable()->pluck('id')->toArray();

        return [
            'work_location_id'   => [
                'required',
                'numeric',
                Rule::in($work_location_ids),
            ],
        ];
    }
}

<?php

namespace App\Http\Requests\API\Tablet;

use Illuminate\Foundation\Http\FormRequest;
use App\Employee;
use Illuminate\Validation\Rule;

class APITabletTimeTableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $card_number = Employee::all()->pluck('card_number')->toArray();
        return [
            'business_month' => 'nullable|date_format:Y-m',
            'card_code'   => [
                'required',
                Rule::in($card_number),
            ]
        ];
    }
}

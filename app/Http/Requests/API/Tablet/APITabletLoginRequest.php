<?php

namespace App\Http\Requests\API\Tablet;

use Illuminate\Foundation\Http\FormRequest;
use App\AdminUser;
use Illuminate\Support\Facades\Hash;

class APITabletLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'manager_presentation_id'   => 'required|max:60',
            'manager_password'          => 'required|max:255',
            'tablet_id'                 => 'required|max:255',
            'company_code'              => 'required|max:32',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $username = $this->manager_presentation_id;
            $account = AdminUser::where('username', $username)->first();
            if (isset($account)) {
                if (Hash::check($this->manager_password, $account->password)) return;
            }
            $validator->errors()->add('manager_presentation_id', 'ユーザーとパスワードが違います。');
        });
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Http\Requests\Reusables\ExtraValidations;
use App\Http\Requests\Reusables\PostValidationProcesses;
use App\Company;

class UpdateCompanyInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Available types for date_separate_type field
        $available_types = [
            Company::APPLY_TO_THE_DAY_BEFORE,
            Company::APPLY_TO_THE_DAY_AFTER,
        ];

        $rules = [
            'name'                                  => 'required|max:80',
            'furigana'                              => 'required|max:240|furigana',
            'postal_code_1'                         => 'nullable|digits:3|required_with:postal_code_2',
            'postal_code_2'                         => 'nullable|digits:4|required_with:postal_code_1',
            'todofuken'                             => 'nullable',
            'telephone_1'                           => 'digits_between:0,4|required_with:telephone_2,telephone_3',
            'telephone_2'                           => 'digits_between:0,4|required_with:telephone_1,telephone_3',
            'telephone_3'                           => 'digits_between:0,5|required_with:telephone_1,telephone_2',
            'fax_1'                                 => 'digits_between:0,4|required_with:fax_2,fax_3',
            'fax_2'                                 => 'digits_between:0,4|required_with:fax_1,fax_3',
            'fax_3'                                 => 'digits_between:0,5|required_with:fax_1,fax_2',
            'ceo_first_name'                        => 'max:80',
            'ceo_last_name'                         => 'max:80',
            'ceo_first_name_furigana'               => 'nullable|max:240|furigana',
            'ceo_last_name_furigana'                => 'nullable|max:240|furigana',
            'ceo_email'                             => 'nullable|email',
            'billing_person_first_name'             => 'max:80',
            'billing_person_last_name'              => 'max:80',
            'billing_person_first_name_furigana'    => 'nullable|max:240|furigana',
            'billing_person_last_name_furigana'     => 'nullable|max:240|furigana',
            'billing_person_email'                  => 'nullable|email',
            'date_separate_time'                    => 'required|time',
            'date_separate_type'                    => [
                'required',
                Rule::in($available_types),
            ],
            'use_address_system'                    => 'boolean',
            'use_night_shift'                       => 'boolean',
        ];

        if ($this->input('use_night_shift')) {
            $rules['pass_time'] ='required|time';
            $rules['over_time'] ='required|numeric|min:0';
            $rules['auto_end_timestamp'] ='required|time';
            $rules['auto_start_timestamp'] ='required|time';
        }

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        // Extra validations
        ExtraValidations::todofuken($validator);

        // Post validation processes
        $validator->after(function ($validator) {
            // Only process all the below if the input data pass the validation
            if ($validator->errors()->isEmpty()) {
                PostValidationProcesses::telephone($this);
                PostValidationProcesses::fax($this);
                PostValidationProcesses::postalCode($this);
                PostValidationProcesses::time($this, 'date_separate_time');
                PostValidationProcesses::time($this, 'pass_time');
                PostValidationProcesses::time($this, 'auto_end_timestamp');
                PostValidationProcesses::time($this, 'auto_start_timestamp');
            }
        });
    }
}

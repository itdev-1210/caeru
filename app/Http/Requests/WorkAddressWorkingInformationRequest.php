<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class WorkAddressWorkingInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'work_address_working_day_id'       => 'sometimes|required|exists:work_address_working_days,id',

            'schedule_start_work_time'          => 'nullable|date_format:Y-m-d H:i:s',
            'schedule_start_work_time_changed'  => 'nullable|date_format:Y-m-d H:i:s',
            'schedule_end_work_time'            => 'nullable|date_format:Y-m-d H:i:s',
            'schedule_end_work_time_changed'    => 'nullable|date_format:Y-m-d H:i:s',
            'candidate_number'                  => 'nullable|integer',
            'candidate_number_changed'          => 'nullable|integer',

        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $work_address_working_info = $this->route('work_address_working_info');
        
        if (!$work_address_working_info) {
            $validator->addRules([
                'schedule_start_work_time'  => 'required',
                'schedule_end_work_time'    => 'required',
            ]);
        }
    }
}
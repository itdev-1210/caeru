<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Http\Requests\Reusables\ExtraValidations;
use App\Http\Requests\Reusables\PostValidationProcesses;
use Constants;


class EmployeeWorkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'all_timestamps_register_to_belong_work_location' => 'boolean',
            'delete_card'               => 'boolean',
            'paid_holiday_exception'    => 'boolean',
            'holidays_update_day'       => 'sometimes|required',
            'work_time_per_day'         => 'required|numeric|max:24|min:0',
            'work_time_change_date'     => 'nullable|date|after:today|required_with:work_time_change_to',
            'work_time_change_to'       => 'nullable|numeric|max:24|min:0|required_with:work_time_change_date',
            'holiday_bonus_type_extra'  => 'boolean',
            'holiday_bonus_type'        => Rule::in(array_keys(Constants::holidayBonusTypes())),
            'gps_device_type'           => [
                'nullable',
                Rule::in(array_keys(Constants::gpsDeviceTypes())),
            ],
            'gps_number'              => [
                'nullable',
                Rule::unique('employees')->ignore($this->employee->id),
            ],
            'device_id'              => [
                'nullable',
                Rule::unique('employees')->ignore($this->employee->id),
            ],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($this->filled('holidays_update_day')) {
            ExtraValidations::dateMonthType($this, $validator, 'holidays_update_day');
        }

        // Post validation process
        $validator->after(function ($validator) {
            PostValidationProcesses::paidHolidayTypes($this);
        });
    }
}

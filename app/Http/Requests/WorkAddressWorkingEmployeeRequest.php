<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\WorkAddressWorkingInformation;
use App\Services\WorkLocationSettingService;

class WorkAddressWorkingEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->input('work_address_working_information_id')) {
            $working_info = WorkAddressWorkingInformation::find($this->input('work_address_working_information_id'));
        } else {
            $working_employee = $this->route('work_address_working_employee');
            $working_info = WorkAddressWorkingInformation::find($working_employee->work_address_working_information_id);
        }

        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $work_location = $working_info ? $work_location_setting_service->getWorkLocationById($working_info->workAddressWorkingDay->workAddress->work_location_id) : null;
        $available_work_status_ids = $work_location ? $work_location->activatingWorkStatuses()->pluck('id')->toArray() : [];
        $available_rest_status_ids = $work_location ? $work_location->activatingRestStatuses()->pluck('id')->toArray() : [];

        return [
            'work_address_working_information_id'   => 'sometimes|required|exists:work_address_working_informations,id',
            'employee_id'                           => 'sometimes|required|integer',
            'working_confirm'                       => 'sometimes|required|boolean',
            'schedule_break_time'                   => 'sometimes|required|integer',
            'schedule_night_break_time'             => 'nullable|integer',
            'schedule_break_time_changes'           => 'nullable|integer',
            'schedule_night_break_time_changes'     => 'nullable|integer',
            'planned_work_status_id'                => [
                'nullable',
                'integer',
                Rule::in($available_work_status_ids)
            ],
            'planned_rest_status_id'                => [
                'nullable',
                'integer',
                Rule::in($available_rest_status_ids)
            ],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $work_address_working_employee = $this->route('work_address_working_employee');
        
        if (!$work_address_working_employee) {
            $working_info = WorkAddressWorkingInformation::find($this->input('work_address_working_information_id'));
            $already_exist_working_employee_ids = $working_info->workAddressWorkingEmployees->pluck('employee_id')->toArray();

            $validator->addRules([
                'employee_id'           => Rule::notIn($already_exist_working_employee_ids),
            ]);
        }
    }
}
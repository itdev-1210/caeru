<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\EmployeeWorkingInformation;
use App\WorkStatus;
use App\RestStatus;
use App\ConcludedEmployeeWorkingInformation;
use App\EmployeeWorkingInformationSnapshot;

class OptionItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'list_work_status_default.*.name' => 'string|size:2',
            'list_work_status_default.*.status' => 'boolean',
            'list_work_status_customize.*.name' => 'string|size:2',
            'list_work_status_customize.*.status' => 'boolean',
            'list_rest_status_customize.*.name' => 'string|size:2',
            'list_rest_status_customize.*.paid_type' => 'boolean',
            'list_rest_status_customize.*.unit_type' => 'boolean',
            'list_rest_status_customize.*.status' => 'boolean',
            'list_rest_status_default.*.name' => 'string|size:2',
            'list_rest_status_default.*.status' => 'boolean',
            'list_department_status.*.status' => 'boolean',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            if(isset($this->list_work_status_default) || isset($this->list_work_status_customize)) {

                $array_work_status_delete = WorkStatus::whereNotIn('id', array_column(array_merge($this->list_work_status_default, $this->list_work_status_customize),                                                      'id'))->pluck('id')->toArray();

                if (EmployeeWorkingInformation::whereIn('planned_work_status_id', $array_work_status_delete)->exists() ||
                    ConcludedEmployeeWorkingInformation::whereIn('work_status_id', $array_work_status_delete)->exists() || EmployeeWorkingInformationSnapshot::whereIn('left_work_status_id', $array_work_status_delete)->orWhereIn('right_work_status_id', $array_work_status_delete)->exists()) {
                        $validator->errors()->add('error', '勤務形態 cannot remove');
                        session()->flash('error', '勤務形態 cannot remove');
                    }
            }

            if(isset($this->list_rest_status_default) || isset($this->list_rest_status_customize)) {

                $array_rest_status_delete = RestStatus::whereNotIn('id', array_column(array_merge($this->list_rest_status_default, $this->list_rest_status_customize), 'id'))->pluck('id')->toArray();

                if (EmployeeWorkingInformation::whereIn('planned_rest_status_id', $array_rest_status_delete)->exists() ||
                    ConcludedEmployeeWorkingInformation::whereIn('rest_status_id', $array_rest_status_delete)->exists() || EmployeeWorkingInformationSnapshot::whereIn('left_rest_status_id', $array_rest_status_delete)->orWhereIn('right_rest_status_id', $array_rest_status_delete)->exists()){
                        $validator->errors()->add('error', '休暇形態 cannot remove');
                        session()->flash('error', '休暇形態 cannot remove');
                    }
            }
        });
    }
}

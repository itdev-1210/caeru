<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\Reusables\PostValidationProcesses;
use Constants;

class MaebaraiSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'maebarai_enable'           =>  'required',
            'maebarai_auto_approve'     =>  'nullable',
            'maebarai_payment_method'   =>  'required',
        ];
        if ($this->input('maebarai_enable') == 1) {
            $rules['maebarai_payment_rate'] = 'required | integer | min:0 | max:100';
            $rules['maebarai_branch_code'] = 'required | string | max: 3 | regex:/^[0-9]+$/';
            $rules['maebarai_account_number'] = 'required | string | max: 7 | regex:/^[0-9]+$/';
            $rules['maebarai_account_name'] = 'required';
        }
        return $rules;
    }
}

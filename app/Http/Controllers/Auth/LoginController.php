<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Manager;
use Validator;
use Caeru;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('layouts.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  App\Http\Requests\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $login_result = $this->attemptLogin($request);

        if ($login_result === true) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request, $login_result);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        // At this point, there is no 'company_code' parameter in the rout. So we have to get this code from the session before it get flushed,
        // or else we can not redirect the user to the loggin page of the right company.
        $current_company_code = session('current_company_code');

        Auth::logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return Caeru::redirect('login', ['company_code' => $current_company_code]);
    }


    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request     $request
     * @return true|array                  it's either true or an error message
     */
    protected function attemptLogin(Request $request)
    {
        $manager = Manager::where('presentation_id', '=', $request->input('presentation_id'))->first();

        $result = false;

        if ($manager && $manager->enable) {
            $current_ip = $request->ip();

            $ip_addresses_of_manager = $manager->ipAddresses;

            $matching_ip = $ip_addresses_of_manager->first(function($ip) use ($current_ip) {
                return $ip->value === $current_ip;
            });

            if ($ip_addresses_of_manager->isEmpty() || $matching_ip) {
                $result = Auth::attempt($request->only('presentation_id', 'password'));
            } else {
                return ['auth_failed' => trans('auth.mismatched_ip')];
            }
        }

        return $result === true ? $result : ['auth_failed' => trans('auth.failed')];
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $current_company_code = $request->route('company_code');

        session(['current_company_code' => $current_company_code]);

        return Caeru::redirect('dashboard');
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request     $request
     * @param   string                      $error_message
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request, $error_message)
    {
        if ($request->expectsJson()) {
            return response()->json($error_message, 422);
        }

        return redirect()->back()
            ->withInput($request->only('presentation_id'))
            ->withErrors($error_message);
    }
}

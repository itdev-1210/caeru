<?php

namespace App\Http\Controllers\Sinsei\Requester;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use App\EmployeeWorkingDay;
use App\Employee;
use App\WorkLocation;
use App\WorkAddress;
use App\WorkingTimestamp;
use App\EmployeeWorkingInformationSnapshot;
use App\EmployeeWorkingInformation;
use App\WorkStatus;
use App\ColorStatus;
use App\Setting;
use App\SubstituteEmployeeWorkingInformation;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Http\Controllers\Reusables\UseTheEmployeeWorkingInfoVueComponentTrait;
use App\Http\Requests\Sinsei\Requester\EmployeeWorkingInformationSnapshotRequest;
use App\Http\Requests\Sinsei\Requester\EmployeeWorkingInformationSnapshotFurikaeRequest;
use App\Http\Requests\Sinsei\Requester\EmployeeWorkingInformationSnapshotDayOffRequest;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;
use App\Services\NationalHolidayService;
use App\Services\WorkLocationSettingService;
use mysql_xdevapi\Exception;
use Session;

class RequestFormController extends Controller
{
    use UseTheEmployeeWorkingInfoVueComponentTrait, BusinessMonthTrait, CanCreateWorkingDayOnTheFlyTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('sinsei_auth');
    }

    /**
     * Show request form
     *  $list_date will substring with charter '|' return array of string
     *  Call function checkDateList for check furikae's case
     *  Finally, get all snapshot with employee working day and some data of work location
     *
     * @param String      $list_date
     * @return \Illuminate\Http\Response
     */
    public function showRequestForm($list_date)
    {
        $snapshot_days = collect([]);
        $array_date = explode("|", str_replace(' ', '', $list_date));
        $array_date = $this->checkDateList($array_date);
        if (!$array_date) abort(404, 'Can not find snapshot information for this employee on that day!');
        foreach ($array_date as $date) {
            $snapshot_days->push($this->sendDataFollowWorkingDay($date));
        }
        session('sinsei_user')->load('worklocation.company');
        $company = session('sinsei_user')->worklocation->company;
        $this->getAllWorkLocationWithRestStatusAndWorkStatusAndTimezone();
        Javascript::put([
            'snapshot_days' => $snapshot_days,
            'company_separate_info' => [$company->date_separate_time, $company->date_separate_type],
            'list_date_sinsei_chu' => $this->getListSinseiChu(),
            'default_work_location_id' => session('sinsei_user')->work_location_id,
        ]);

        return view('sinsei.requester.request_form');
    }

    /**
     * First, will run remove all snapshot if it is furikae's case
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\Sinsei\Requester\EmployeeWorkingInformationSnapshotRequest      $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeWorkingInformationSnapshotRequest $request)
    {
        $column_name_data = [
            'employee_working_day_id',
            'employee_working_information_id',

            'left_timestamped_start_work_date',
            'left_timestamped_start_work_time',
            'left_timestamped_end_work_time_work_location_id',
            'left_timestamped_end_work_date',
            'left_timestamped_end_work_time',
            'left_work_status_id',
            'left_rest_status_id',
            'left_paid_rest_time_start',
            'left_paid_rest_time_end',
            'left_paid_rest_time_period',
            'left_not_include_break_time_when_display_planned_time',
            'left_switch_planned_schedule_target',
            'left_planned_work_location_id',
            'left_planned_early_arrive_start',
            'left_planned_early_arrive_end',
            'left_planned_overtime_start',
            'left_planned_overtime_end',
            'left_planned_break_time',
            'left_real_break_time',
            'left_planned_night_break_time',
            'left_real_night_break_time',
            'left_planned_late_time',
            'left_planned_early_leave_time',
            'left_planned_go_out_time',
            'left_requester_note',

            'right_timestamped_start_work_time_work_location_id',
            'right_timestamped_start_work_date',
            'right_timestamped_start_work_time',
            'right_timestamped_end_work_time_work_location_id',
            'right_timestamped_end_work_date',
            'right_timestamped_end_work_time',
            'right_work_status_id',
            'right_rest_status_id',
            'right_paid_rest_time_start',
            'right_paid_rest_time_end',
            'right_paid_rest_time_period',
            'right_not_include_break_time_when_display_planned_time',
            'right_switch_planned_schedule_target',
            'right_planned_work_location_id',
            'right_real_work_location_id',
            'right_planned_early_arrive_start',
            'right_real_early_arrive_start',
            'right_planned_early_arrive_end',
            'right_real_early_arrive_end',
            'right_planned_work_span_start',
            'right_real_work_span_start',
            'right_planned_work_span_end',
            'right_real_work_span_end',
            'right_planned_overtime_start',
            'right_real_overtime_start',
            'right_planned_overtime_end',
            'right_real_overtime_end',
            'right_planned_work_span',
            'right_real_work_span',
            'right_planned_break_time',
            'right_real_break_time',
            'right_planned_night_break_time',
            'right_real_night_break_time',
            'right_planned_late_time',
            'right_real_late_time',
            'right_planned_early_leave_time',
            'right_real_early_leave_time',
            'right_planned_go_out_time',
            'right_real_go_out_time',
        ];

        // force delete with snapshot is activing
        if (isset($request->snapshot["isTrashed"]) && $request->snapshot["isTrashed"]) {
            $employee_working_day = EmployeeWorkingDay::find($request->snapshot["employee_working_day_id"]);
            $date_furikyu = EmployeeWorkingInformationSnapshot::where("employee_working_day_id", $employee_working_day->id)->first();
            if ($date_furikyu) {
                $this->deleteSnapshot($employee_working_day);
                $this->restoreSnapshot($employee_working_day);
                $this->restoreSnapshot(EmployeeWorkingDay::where('employee_id', session('sinsei_user')->id)
                ->where('date', $date_furikyu->left_switch_planned_schedule_target)->first());
            }
        } else {
            // This rule is for preventing user from creating new snapshot for the same day.
            if ($request->input('snapshot.employee_working_day_id') && $request->input('snapshot.employee_working_information_id', null) === null) {
                $employee_working_day = EmployeeWorkingDay::with('employeeWorkingInformationSnapshots')->find($request->input('snapshot.employee_working_day_id'));

                if ($employee_working_day && $employee_working_day->employeeWorkingInformationSnapshots->isNotEmpty()) {
                    return response()->json(['error' => '申請中が存在しています。'], 401);
                }
            }
        }

        $data_snapshot = $request->snapshot;
        if (count($request->array_attribute_name) != 0) {

            $snapshot = new EmployeeWorkingInformationSnapshot(array_only($request->snapshot, $column_name_data));
            $snapshot->left_status = EmployeeWorkingInformationSnapshot::CONSIDERING;
            $snapshot->right_status = EmployeeWorkingInformationSnapshot::PREVIOUS;
            $snapshot->left_requester_modify_time = Carbon::now(session('sinsei_user')->workLocation->getTimezone()->name_id)->toDateTimeString();
            $snapshot->save();

            foreach ($request->array_attribute_name as $item) {
                $this->createColorStatusForSnapshot($snapshot->id, $item, ColorStatus::CONSIDERING_COLOR);
            }

            if ($snapshot->employee_working_information_id)
                $this->createColorStatusForSubstituteEmployeeWorkingInformationOrEmployeeWorkingInformation(
                    array_only($request->snapshot, $request->array_attribute_name), $request->snapshot, null, $snapshot->employee_working_information_id);
            else
                $this->createColorStatusForSubstituteEmployeeWorkingInformationOrEmployeeWorkingInformation(
                    array_only($request->snapshot, $request->array_attribute_name), $request->snapshot,
                    $this->createSubstituteEmployeeWorkingInformation($snapshot->employee_working_day_id)->id, null);

            $data_snapshot = ($snapshot->employee_working_information_id !== null) ?
                $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot() : $snapshot;
        }

        $request->session()->flash('success', '保存しました');
        return [
            'success'   => '保存しました',
            'snapshot'        => $data_snapshot,
            'list_date_sinsei_chu' => $this->getListSinseiChu(),
            'color_status'        => (isset($data_snapshot->id)) ? $data_snapshot->colorStatuses()->get() : [],
        ];
    }

    /**
     * First, will run remove all snapshot if it is furikae's case
     * Second, will restore the snapshot's card which was deleted
     * Update the specified resource in storage.
     *
     * @param App\Http\Requests\Sinsei\Requester\EmployeeWorkingInformationSnapshotRequest      $request
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeWorkingInformationSnapshotRequest $request, $id)
    {
        $snapshot = EmployeeWorkingInformationSnapshot::withTrashed()->find($id);

        if ($snapshot->trashed()) {
            $this->deleteSnapshot($snapshot->employeeWorkingDay);
            $this->restoreSnapshot($snapshot->employeeWorkingDay);
        }

        $column_name_data = [
            'left_timestamped_start_work_date',
            'left_timestamped_start_work_time',
            'left_timestamped_end_work_time_work_location_id',
            'left_timestamped_end_work_date',
            'left_timestamped_end_work_time',
            'left_work_status_id',
            'left_rest_status_id',
            'left_paid_rest_time_start',
            'left_paid_rest_time_end',
            'left_paid_rest_time_period',
            'left_not_include_break_time_when_display_planned_time',
            'left_switch_planned_schedule_target',
            'left_planned_work_location_id',
            'left_planned_early_arrive_start',
            'left_planned_early_arrive_end',
            'left_planned_overtime_start',
            'left_planned_overtime_end',
            'left_planned_break_time',
            'left_real_break_time',
            'left_planned_night_break_time',
            'left_real_night_break_time',
            'left_planned_late_time',
            'left_planned_early_leave_time',
            'left_planned_go_out_time',
            'left_requester_note',

            'right_timestamped_start_work_time_work_location_id',
            'right_timestamped_start_work_date',
            'right_timestamped_start_work_time',
            'right_timestamped_end_work_time_work_location_id',
            'right_timestamped_end_work_date',
            'right_timestamped_end_work_time',
            'right_work_status_id',
            'right_rest_status_id',
            'right_paid_rest_time_start',
            'right_paid_rest_time_end',
            'right_paid_rest_time_period',
            'right_not_include_break_time_when_display_planned_time',
            'right_switch_planned_schedule_target',
            'right_planned_work_location_id',
            'right_real_work_location_id',
            'right_planned_early_arrive_start',
            'right_real_early_arrive_start',
            'right_planned_early_arrive_end',
            'right_real_early_arrive_end',
            'right_planned_work_span_start',
            'right_real_work_span_start',
            'right_planned_work_span_end',
            'right_real_work_span_end',
            'right_planned_overtime_start',
            'right_real_overtime_start',
            'right_planned_overtime_end',
            'right_real_overtime_end',
            'right_planned_work_span',
            'right_real_work_span',
            'right_planned_break_time',
            'right_real_break_time',
            'right_planned_night_break_time',
            'right_real_night_break_time',
            'right_planned_late_time',
            'right_real_late_time',
            'right_planned_early_leave_time',
            'right_real_early_leave_time',
            'right_planned_go_out_time',
            'right_real_go_out_time',
        ];

        $array_attribute_name = $request->array_attribute_name;
        if (isset($snapshot->target_absorb_EWI_id)) {
            $target_absorb_EWI_new = EmployeeWorkingInformation::find($snapshot->target_absorb_EWI_id);

            $array_attribute_name = $this->checkTargetAbsorbEWI($array_attribute_name,
                                                                $snapshot->fill(array_only($request->snapshot, $column_name_data)),
                                                                $target_absorb_EWI_new);
        }

        if ($snapshot->employee_working_information_id)
            $this->createOrRemoveSubstituteOrWorkingInformationColor(array_only($request->snapshot, $array_attribute_name),
                $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot(), null, $snapshot->employeeWorkingInformation);
        else {

            $substitute_employee_working_information = null;

            if ($snapshot->soon_to_be_EWI_id)
                $substitute_employee_working_information = SubstituteEmployeeWorkingInformation::where('employee_working_day_id',
                    $snapshot->employee_working_day_id)->where('employee_working_information_id', $snapshot->soon_to_be_EWI_id)->first();
            else
                $substitute_employee_working_information = SubstituteEmployeeWorkingInformation::where('employee_working_day_id',
                    $snapshot->employee_working_day_id)->first();

            $this->createOrRemoveSubstituteOrWorkingInformationColor(array_only($request->snapshot, $array_attribute_name), $snapshot,
                $substitute_employee_working_information, null);
        }

        foreach ($snapshot->colorStatuses()->get() as $color) {
            if (!array_search($color->field_name, $array_attribute_name))
                $color->delete();
        }

        foreach ($array_attribute_name as $item) {
            $this->createColorStatusForSnapshot($snapshot->id, $item, ColorStatus::CONSIDERING_COLOR);
        }

        $data_snapshot = $snapshot;
        if (count($array_attribute_name) != 0) {
            $snapshot->left_status = EmployeeWorkingInformationSnapshot::CONSIDERING;
            $snapshot->right_status = EmployeeWorkingInformationSnapshot::PREVIOUS;
            $snapshot->right_approval_time = $snapshot->right_requester_modify_time =
            $snapshot->right_approver_note = $snapshot->right_requester_note = null;
            $snapshot->left_requester_modify_time = Carbon::now(session('sinsei_user')->workLocation->getTimezone()->name_id)->toDateTimeString();
            $snapshot->save();
            $snapshot->update(array_only($request->snapshot, $column_name_data));

            // $data_snapshot =
            //     ($snapshot->employeeWorkingInformation) ? $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot()
            //     : $snapshot->employeeWorkingDay->mergeSnapshotsAndEmployeeWorkingInformations()->where('id', $snapshot->id)->first();
            // 2019-05-04: Doing like above will lead to: old data will be used. So we need to add one step: refresh the necessary relationship before use the 'merge' functions.
            if ($snapshot->employeeWorkingInformation) {
                $snapshot->load('employeeWorkingInformation');
                $data_snapshot = $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot();
            } else {
                $snapshot->load('employeeWorkingDay');
                $data_snapshot = $snapshot->employeeWorkingDay->mergeSnapshotsAndEmployeeWorkingInformations()->where('id', $snapshot->id)->first();
            }

        } else {
            $snapshot->forceDelete();
            $data_snapshot = $snapshot->employee_working_information_id ?
                EmployeeWorkingInformation::find($snapshot->employee_working_information_id)->mergeWorkingInformationToSnapshot() :
                EmployeeWorkingDay::find($snapshot->employee_working_day_id)->mergeSnapshotsAndEmployeeWorkingInformations()->first();
            if (!isset($data_snapshot)) {
                $employee_working_day = EmployeeWorkingDay::find($snapshot->employee_working_day_id);
                if (!$employee_working_day)
                    $employee_working_day = $this->createEmployeeWorkingDayOnTheFly(session('sinsei_user')->id, $date);
                $substitute_employee_working_information = SubstituteEmployeeWorkingInformation::where('employee_working_day_id', $employee_working_day->id)->first();
                $substitute_employee_working_information->colorStatuses()->delete();
                $substitute_employee_working_information->delete();
                $data_snapshot = $this->createDefaultSnapshot($employee_working_day);
            }
        }

        $request->session()->flash('success', '保存しました');
        return [
            'success'   => '保存しました',
            'snapshot'        =>  $data_snapshot,
            'list_date_sinsei_chu' => $this->getListSinseiChu(),
            'color_status'        => $snapshot->colorStatuses()->get(),
        ];
    }

    /**
     * The function only run for furikae's case.
     *  Delete all snapshot is activing,
     *  Create new snapshot with the new data in the request
     *
     * @param App\Http\Requests\Sinsei\Requester\EmployeeWorkingInformationSnapshotFurikaeRequest   $request
     * @return \Illuminate\Http\Response
     */
    public function storeFuriKae(EmployeeWorkingInformationSnapshotFurikaeRequest $request)
    {
        $column_name_data = [
            'left_timestamped_start_work_date',
            'left_timestamped_start_work_time',
            'left_timestamped_end_work_time_work_location_id',
            'left_timestamped_end_work_date',
            'left_timestamped_end_work_time',
            'left_work_status_id',
            'left_rest_status_id',
            'left_switch_planned_schedule_target',
            'left_paid_rest_time_start',
            'left_paid_rest_time_end',
            'left_paid_rest_time_period',
            'left_not_include_break_time_when_display_planned_time',
            'left_planned_work_location_id',
            'left_planned_early_arrive_start',
            'left_planned_early_arrive_end',
            'left_planned_work_span_start',
            'left_planned_work_span_end',
            'left_planned_overtime_start',
            'left_planned_overtime_end',
            'left_planned_work_span',
            'left_planned_break_time',
            'left_real_break_time',
            'left_planned_night_break_time',
            'left_real_night_break_time',
            'left_planned_late_time',
            'left_planned_early_leave_time',
            'left_planned_go_out_time',
            'left_requester_note',
        ];

        $employee_working_day_from = EmployeeWorkingDay::where('employee_id', session('sinsei_user')->id)->where('date', $request->date_from)->first();
        $employee_working_day_to = EmployeeWorkingDay::where('employee_id', session('sinsei_user')->id)->where('date', $request->date_to)->first();
        if (!$employee_working_day_to)
            $employee_working_day_to = $this->createEmployeeWorkingDayOnTheFly(session('sinsei_user')->id, $request->date_to);

        $this->deleteSnapshot($employee_working_day_from);
        foreach ($request->snapshotTo as $index => $snapshot) {
            $note = null;
            if (isset($request->snapshotFrom[$index]['left_requester_note'])) $note = $request->snapshotFrom[$index]['left_requester_note'];
            $this->createAndSaveSnapshotFurikyu($employee_working_day_from, $snapshot["employee_working_information_id"], $request->date_to, $note);
            $this->createAndSaveSnapshotFuride($employee_working_day_to, $snapshot, $column_name_data);
        }

        $request->session()->flash('success', '保存しました');
        return [
            'success'   => '保存しました',
            'snapshotFrom'        => $this->sendDataFollowWorkingDay($request->date_from),
            'snapshotTo'        => $this->sendDataFollowWorkingDay($request->date_to),
            'list_date_sinsei_chu' => $this->getListSinseiChu(),
        ];
    }

    /**
     * The function will create or update snapshot of array in request
     *
     * @param App\Http\Requests\Sinsei\Requester\EmployeeWorkingInformationSnapshotDayOffRequest   $request
     * @return \Illuminate\Http\Response
     */
    public function storeDayOff(EmployeeWorkingInformationSnapshotDayOffRequest $request)
    {
        $column_name_data = [
            'left_timestamped_start_work_date',
            'left_timestamped_start_work_time',
            'left_timestamped_end_work_time_work_location_id',
            'left_timestamped_end_work_date',
            'left_timestamped_end_work_time',
            'left_work_status_id',
            'left_rest_status_id',
            'left_switch_planned_schedule_target',
            'left_paid_rest_time_start',
            'left_paid_rest_time_end',
            'left_paid_rest_time_period',
            'left_not_include_break_time_when_display_planned_time',
            'left_planned_work_location_id',
            'left_planned_early_arrive_start',
            'left_planned_early_arrive_end',
            'left_planned_work_span_start',
            'left_planned_work_span_end',
            'left_planned_overtime_start',
            'left_planned_overtime_end',
            'left_planned_work_span',
            'left_planned_break_time',
            'left_real_break_time',
            'left_planned_night_break_time',
            'left_real_night_break_time',
            'left_planned_late_time',
            'left_planned_early_leave_time',
            'left_planned_go_out_time',
            'left_requester_note',
        ];

        foreach ($request->snapshots as $snapshot) {
            $this->createOrUpdateSnapshot($snapshot, $column_name_data);
        }

        $request->session()->flash('success', '保存しました');
        return [
            'success'   => '保存しました',
            'snapshotDay'        => $this->sendDataFollowWorkingDay($request->date),
        ];
    }

    /**
     * The function will create or update snapshot of array in request
     *
     * @param App\Http\Requests\Sinsei\Requester\EmployeeWorkingInformationSnapshotDayOffRequest   $request
     * @return \Illuminate\Http\Response
     */
    public function storeAbsorb(EmployeeWorkingInformationSnapshotRequest $request)
    {
        $column_name_data = [
            'employee_working_day_id',
            'employee_working_information_id',
            'target_absorb_EWI_id',

            'left_timestamped_start_work_date',
            'left_timestamped_start_work_time',
            'left_timestamped_end_work_time_work_location_id',
            'left_timestamped_end_work_date',
            'left_timestamped_end_work_time',
            'left_work_status_id',
            'left_rest_status_id',
            'left_paid_rest_time_start',
            'left_paid_rest_time_end',
            'left_paid_rest_time_period',
            'left_not_include_break_time_when_display_planned_time',
            'left_switch_planned_schedule_target',
            'left_planned_work_location_id',
            'left_planned_early_arrive_start',
            'left_planned_early_arrive_end',
            'left_planned_overtime_start',
            'left_planned_overtime_end',
            'left_planned_break_time',
            'left_real_break_time',
            'left_planned_night_break_time',
            'left_real_night_break_time',
            'left_planned_late_time',
            'left_planned_early_leave_time',
            'left_planned_go_out_time',
            'left_requester_note',

            'right_timestamped_start_work_time_work_location_id',
            'right_timestamped_start_work_date',
            'right_timestamped_start_work_time',
            'right_timestamped_end_work_time_work_location_id',
            'right_timestamped_end_work_date',
            'right_timestamped_end_work_time',
            'right_work_status_id',
            'right_rest_status_id',
            'right_paid_rest_time_start',
            'right_paid_rest_time_end',
            'right_paid_rest_time_period',
            'right_not_include_break_time_when_display_planned_time',
            'right_switch_planned_schedule_target',
            'right_planned_work_location_id',
            'right_real_work_location_id',
            'right_planned_early_arrive_start',
            'right_real_early_arrive_start',
            'right_planned_early_arrive_end',
            'right_real_early_arrive_end',
            'right_planned_work_span_start',
            'right_real_work_span_start',
            'right_planned_work_span_end',
            'right_real_work_span_end',
            'right_planned_overtime_start',
            'right_real_overtime_start',
            'right_planned_overtime_end',
            'right_real_overtime_end',
            'right_planned_work_span',
            'right_real_work_span',
            'right_planned_break_time',
            'right_real_break_time',
            'right_planned_night_break_time',
            'right_real_night_break_time',
            'right_planned_late_time',
            'right_real_late_time',
            'right_planned_early_leave_time',
            'right_real_early_leave_time',
            'right_planned_go_out_time',
            'right_real_go_out_time',
        ];

        $target_absorb_EWI_old = $target_absorb_EWI_new = null;

        $snapshot = EmployeeWorkingInformationSnapshot::firstOrNew(['id' => $request->snapshot['id'] ?? null]);

        if (isset($snapshot->id)) {
            if ($snapshot->target_absorb_EWI_id != $request->snapshot['target_absorb_EWI_id']) {
                $target_absorb_EWI_old = EmployeeWorkingInformation::find($snapshot->target_absorb_EWI_id);
                if (isset($target_absorb_EWI_old)) {
                    $target_absorb_EWI_old->employeeWorkingInformationSnapshot->forceDelete();
                    $target_absorb_EWI_old->employeeWorkingInformationSnapshot->colorStatuses()->delete();
                }
            }
        }

        $array_diff_attribute = $request->array_diff_attribute;

        if (count($array_diff_attribute) != 0) {

            $snapshot->fill(array_only($request->snapshot, $column_name_data));
            $snapshot->left_status = EmployeeWorkingInformationSnapshot::CONSIDERING;
            $snapshot->right_status = EmployeeWorkingInformationSnapshot::PREVIOUS;
            $snapshot->left_requester_modify_time = Carbon::now(session('sinsei_user')->workLocation->getTimezone()->name_id)->toDateTimeString();
            $snapshot->save();

            if (isset($snapshot->target_absorb_EWI_id)) {
                $target_absorb_EWI_new = EmployeeWorkingInformation::find($snapshot->target_absorb_EWI_id);

                $array_diff_attribute = $this->checkTargetAbsorbEWI($array_diff_attribute, $snapshot, $target_absorb_EWI_new);

                $snapshot_soon_to_be_absorb = $target_absorb_EWI_new->employeeWorkingInformationSnapshot ?? new EmployeeWorkingInformationSnapshot();
                $snapshot_soon_to_be_absorb = $this->mergeEmployeeWorkingInformationToTheLeftAndRightSnapshot($snapshot_soon_to_be_absorb, $target_absorb_EWI_new, true);
                $snapshot_soon_to_be_absorb->left_soon_to_be_absorb = true;
                $snapshot_soon_to_be_absorb->left_status = EmployeeWorkingInformationSnapshot::CONSIDERING;
                $snapshot_soon_to_be_absorb->right_status = EmployeeWorkingInformationSnapshot::PREVIOUS;
                $snapshot_soon_to_be_absorb->employeeWorkingDay()->associate($target_absorb_EWI_new->employee_working_day_id);
                $snapshot_soon_to_be_absorb->employeeWorkingInformation()->associate($target_absorb_EWI_new);
                $snapshot_soon_to_be_absorb->left_requester_modify_time = Carbon::now(session('sinsei_user')->workLocation->getTimezone()->name_id)->toDateTimeString();
                $snapshot_soon_to_be_absorb->save();
            }

            foreach ($snapshot->colorStatuses()->get() as $color) {
                if (!array_search($color->field_name, $array_diff_attribute))
                    $color->delete();
            }

            foreach (array_only($request->snapshot, $array_diff_attribute) as $key => $name) {
                $this->createColorStatusForSnapshot($snapshot->id, $key, ColorStatus::CONSIDERING_COLOR);
            }
            $this->createColorStatusForSubstituteEmployeeWorkingInformationOrEmployeeWorkingInformation(array_only($request->snapshot, $array_diff_attribute), $snapshot,
                null, $snapshot->employee_working_information_id);
        } else {
            if (isset($snapshot->id)) {
                if (isset($snapshot->employee_working_information_id)) {
                    $snapshot->employeeWorkingInformation->colorStatuses()->where('field_css_class', ColorStatus::CONSIDERING_COLOR)->delete();
                }
                $snapshot->forceDelete();
                $snapshot->colorStatuses()->delete();
            }
        }

        $request->session()->flash('success', '保存しました');
        return [
            'success'   => '保存しました',
            'snapshot'        => $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot(),
            'color_status'        => (isset($snapshot->id)) ? $snapshot->colorStatuses()->get() : [],
            'snapshot_target_absorb_EWI_old'        => isset($target_absorb_EWI_old) ? $target_absorb_EWI_old->mergeWorkingInformationToSnapshot() : null,
            'snapshot_target_absorb_EWI_new'        => isset($target_absorb_EWI_new) ? $target_absorb_EWI_new->mergeWorkingInformationToSnapshot() : null,
        ];
    }

    /**
     * The function will get all snapshot previous if there is a snapshot
     * Default return current snasphot
     *
     * @param App\EmployeeWorkingDay   $employee_working_day
     * @return array $list_snapshot_previous
     */
    protected function getSnapshotPrevious($employee_working_day)
    {
        $list_snapshot_previous = collect([]);
        $employee_working_informations = $employee_working_day->employeeWorkingInformations()->get();

        foreach ($employee_working_informations as $employee_working_information) {
            $snapshot = $employee_working_information->employeeWorkingInformationSnapshot;
            if ($snapshot && $snapshot->left_switch_planned_schedule_target) {
                $snapshots = EmployeeWorkingInformationSnapshot::onlyTrashed()->where('employee_working_information_id',
                    $employee_working_information->id)->orderBy('deleted_at')->get();
                $list_snapshot_previous->push($employee_working_information->mergeWorkingInformationToSnapshot(true,
                    $this->searchSnapshotInTheTrashed($snapshots)));
            } else {
                $list_snapshot_previous->push($employee_working_information->mergeWorkingInformationToSnapshot());
            }
        }
        if (count($list_snapshot_previous) == 0) {
            $new_snapshot = $this->createDefaultSnapshot($employee_working_day);
            $new_snapshot->isTrashed = true;
            $list_snapshot_previous->push($new_snapshot);
        }
        return $list_snapshot_previous;
    }

    /**
     * The function will get all color of snapshot previous if there is a snapshot
     * Default return color of current snasphot, or array empty
     *
     * @param App\EmployeeWorkingDay   $employee_working_day
     * @return array $list_snapshot_previous_color
     */
    protected function getSnapshotPreviousColor($employee_working_day)
    {
        $list_snapshot_previous_color = collect([]);
        $employee_working_informations = $employee_working_day->employeeWorkingInformations()->get();

        foreach ($employee_working_informations as $employee_working_information) {
            $snapshot = $employee_working_information->employeeWorkingInformationSnapshot;
            if ($snapshot && $snapshot->left_switch_planned_schedule_target) {
                $snapshots = EmployeeWorkingInformationSnapshot::onlyTrashed()->where('employee_working_information_id',
                    $employee_working_information->id)->orderBy('deleted_at')->get();
                (count($snapshots) > 0) ? $list_snapshot_previous_color->push($this->searchSnapshotInTheTrashed($snapshots)->colorStatuses()->get()) :
                    $list_snapshot_previous_color->push([]);
            } else {
                ($snapshot) ? $list_snapshot_previous_color->push($snapshot->colorStatuses()->get()) : $list_snapshot_previous_color->push([]);
            }
        }
        if (count($list_snapshot_previous_color) == 0) {
            $list_snapshot_previous_color->push([]);
        }
        return $list_snapshot_previous_color;
    }


    /**
     * Validate the date string.
     *
     * @param string    $date
     * @param string    $format
     * @return boolean
     */
    protected function validateDateByFormat($date, $format = 'Y-m-d')
    {
        $result = Carbon::createFromFormat($format, $date);

        return $result && ($result->format($format) == $date);
    }

    /**
     * The function gets to all data for the vue component
     * All work location
     * Each work location gets all the rest status, the work status and separate time of it
     * 2019-04-22: now this function will also load WorkAddressName if this company use work_address_mode.
     *
     * @return data
     */
    protected function getAllWorkLocationWithRestStatusAndWorkStatusAndTimezone()
    {
        // $all_work_locations = session('sinsei_user')->workLocation->company->workLocations()->get();
        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $all_work_locations = $work_location_setting_service->getAllWorkLocations();
        // The list of work location with work statuses and rest statuses
        $work_locations = $all_work_locations->map(function($work_location) {
            return [
                'id'            => $work_location->id,
                'name'          => $work_location->name,
                'work_statuses' => $work_location->activatingWorkStatuses()->map(function($status) {
                    return [
                        'id'    => $status->id,
                        'name'  => $status->name,
                    ];
                })->values(),
                'rest_statuses' => $work_location->activatingRestStatuses()->map(function($status) {
                    return [
                        'id'        => $status->id,
                        'name'      => $status->name,
                        'day_based' => $status->unit_type == true,
                    ];
                })->values(),
                'utc_offset_number'          => $work_location->getTimezone()->utc_offset_number,
                'is_use_go_out_button'          => $work_location->currentSetting()->go_out_button_usage == Setting::USE_GO_OUT_BUTTON,
                'start_time_round_up'          => $work_location->currentSetting()->start_time_round_up,
                'end_time_round_down'          => $work_location->currentSetting()->end_time_round_down,
            ];
        });

        $timestamp_types = [
            WorkingTimestamp::START_WORK => "出勤",
            WorkingTimestamp::END_WORK => "退勤",
            WorkingTimestamp::GO_OUT => "外出",
            WorkingTimestamp::RETURN => "戻り",
        ];

        Javascript::put([
            'work_locations'                    => $work_locations,
            'timestamp_types'                   => $timestamp_types,
        ]);

        // 2019-04-22: Add the list of all WorkAddress name if this company use WorkAdress mode
        if ($work_location_setting_service->getCompany()->use_address_system) {
            $work_address_names = WorkAddress::pluck('name', 'id');
            Javascript::put([
                'work_address_names' => $work_address_names,
            ]);
        }
    }

    /**
     * Send data to the normal date picker
     *
     * @param integer       $employee_id,
     * @return void
     */
    protected function sendDatePickerData($employee_id)
    {
        $work_location = Employee::find($employee_id)->workLocation;

        if ($work_location) {

            $rest_days = $work_location->getRestDays();

            $national_holidays_service = resolve(NationalHolidayService::class);
            $national_holidays = $national_holidays_service->get();

            Javascript::put([
                'rest_days'             => $rest_days,
                'national_holidays'     => $national_holidays,
                'flip_color_day'        => $work_location->currentSetting()->salary_accounting_day,
            ]);
        }
    }

    /**
     * The function will send to data of working day
     * snapshots is an array of snapshot
     * snapshot_day_color is an array of color in a snapshot
     * date is client sent
     * timestampes is an array of timestamp with working day
     * schedule_transfer_data is data of calendar
     * previousSinseiCard and previousSinseiCardColor is snapshot previous of the working day and color of snapshot
     *
     * @param format(yyyy-mm-dd)    $date,
     * @return object $object
     */
    protected function sendDataFollowWorkingDay($date)
    {
        $employee_working_day = EmployeeWorkingDay::where('employee_id', session('sinsei_user')->id)->where('date', $date)->first();
        if (!$employee_working_day)
            $employee_working_day = $this->createEmployeeWorkingDayOnTheFly(session('sinsei_user')->id, $date);
        $merge_snapshots_and_employee_working_informations = $employee_working_day->mergeSnapshotsAndEmployeeWorkingInformations();
        $object = collect([
            'snapshots' => (count($merge_snapshots_and_employee_working_informations) != 0) ?
            $merge_snapshots_and_employee_working_informations : [$this->createDefaultSnapshot($employee_working_day)],
            'snapshot_day_color' => $this->getColorStatusWithEmployWorkingDay($employee_working_day),
            'date' => $employee_working_day->date,
            'isConcluded' =>$employee_working_day->isConcluded(),
            'timestampes' => $employee_working_day->workingTimestamps->sortBy('raw_date_time_value')->values(),
            'schedule_transfer_data' => $this->getScheduleTransferData($employee_working_day->employeeWorkingInformations, $employee_working_day->employee_id),
            'previousSinseiCard' => $this->getSnapshotPrevious($employee_working_day),
            'previousSinseiCardColor' => $this->getSnapshotPreviousColor($employee_working_day),
            'has_snapshot_with_work_address_field' => $this->hasSnapshotThatHasWorkAddressFields($merge_snapshots_and_employee_working_informations),
        ]);
        $this->sendDatePickerData($employee_working_day->employee_id);
        return $object;
    }

    /**
     * The function will force delete snapshot and color if furikae's case,
     * or delete snapshot if not
     *
     * @param App\EmployeeWorkingDay    $employee_working_day,
     * @return void
     */
    protected function deleteSnapshot($employee_working_day)
    {
        foreach ($employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {
            if ($snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING && $snapshot->left_switch_planned_schedule_target) {

                $employee_working_day_switch = EmployeeWorkingDay::where('employee_id', session('sinsei_user')->id)->where('date',
                    $snapshot->left_switch_planned_schedule_target)->first();
                foreach ($employee_working_day_switch->employeeWorkingInformationSnapshots as $employeeWorkingInformationSnapshot) {
                    $employeeWorkingInformationSnapshot->forceDelete();
                    $employeeWorkingInformationSnapshot->colorStatuses()->delete();

                    ($employeeWorkingInformationSnapshot->employeeWorkingInformation) ?
                        $employeeWorkingInformationSnapshot->employeeWorkingInformation->colorStatuses()
                        ->where('field_css_class', ColorStatus::CONSIDERING_COLOR)->delete() :
                            $this->deleteColorStatusWithSubstituteEmployeeWorkingInformation($employeeWorkingInformationSnapshot
                            ->employee_working_day_id, $employeeWorkingInformationSnapshot->soon_to_be_EWI_id);
                }

                foreach ($employee_working_day->employeeWorkingInformationSnapshots as $employeeWorkingInformationSnapshot) {
                    $employeeWorkingInformationSnapshot->forceDelete();
                    $employeeWorkingInformationSnapshot->colorStatuses()->delete();

                    ($employeeWorkingInformationSnapshot->employeeWorkingInformation) ?
                    $employeeWorkingInformationSnapshot->employeeWorkingInformation->colorStatuses()->where('field_css_class',
                        ColorStatus::CONSIDERING_COLOR)->delete() :
                    $this->deleteColorStatusWithSubstituteEmployeeWorkingInformation($employeeWorkingInformationSnapshot->employee_working_day_id,
                        $employeeWorkingInformationSnapshot->soon_to_be_EWI_id);
                }
            } else {
                foreach ((EmployeeWorkingInformationSnapshot::where('employee_working_day_id', $employee_working_day->id)->get()) as $snapshot) {
                    ColorStatus::where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION)
                        ->where('colorable_id', $snapshot->employee_working_information_id)->update(['enable' => false]);
                    ColorStatus::where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION_SNAPSHOT)
                        ->where('colorable_id', $snapshot->id)->update(['enable' => false]);
                    $snapshot->delete();
                }
            }
            break;
        }
    }

    /**
     * The function will force delete substitute employee working information and color status
     *
     * @param int    $employee_working_day_id,
     * @param int    $employee_working_information_id,
     * @return void
     */
    protected function deleteColorStatusWithSubstituteEmployeeWorkingInformation($employee_working_day_id, $employee_working_information_id)
    {
        $substitute_employee_working_information = SubstituteEmployeeWorkingInformation::where('employee_working_day_id', $employee_working_day_id)
            ->where('employee_working_information_id', $employee_working_information_id)->first();
        if ($substitute_employee_working_information) {
            foreach ($substitute_employee_working_information->colorStatuses()->get() as $color_status) {
                $color_status->delete();
            }
            $substitute_employee_working_information->delete();
        }
    }

    /**
     * restore snapshot from trashed for the working day
     *
     * @param App\EmployeeWorkingDay   $employee_working_day,
     * @return void
     */
    protected function restoreSnapshot($employee_working_day)
    {
        foreach ($employee_working_day->employeeWorkingInformations as $employeeWorkingInformation) {
            $snapshot = EmployeeWorkingInformationSnapshot::onlyTrashed()->where('employee_working_day_id', $employee_working_day->id)
                ->where('employee_working_information_id', $employeeWorkingInformation->id)->orderBy('deleted_at')->first();
            if ($snapshot) {
                $snapshot->restore();
                ColorStatus::where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION_SNAPSHOT)
                    ->where('colorable_id', $snapshot->id)->update(['enable' => true]);
                ColorStatus::where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION)
                    ->where('colorable_id', $snapshot->employee_working_information_id)->update(['enable' => true]);
            }
        }
    }

    /**
     * Create snapshot for furikyu's case
     *
     * @param App\EmployeeWorkingDay            $employee_working_day,
     * @param App\EmployeeWorkingInformation    $employee_working_information,
     * @param yyyy-mm-dd    $date_to
     * @param string    $note
     *
     */
    protected function createAndSaveSnapshotFurikyu($employee_working_day, $employee_working_information_id, $date_to, $note = null)
    {
        $employee_working_information = EmployeeWorkingInformation::find($employee_working_information_id);

        $snapshot = new EmployeeWorkingInformationSnapshot;
        $snapshot->employee_working_day_id = $employee_working_day->id;
        $snapshot->employee_working_information_id = $employee_working_information->id;
        $snapshot->left_planned_work_location_id = $employee_working_information->planned_work_location_id;
        $snapshot->left_status = EmployeeWorkingInformationSnapshot::CONSIDERING;
        $snapshot->left_work_status_id = WorkStatus::FURIKYUU;
        $snapshot->left_switch_planned_schedule_target = $date_to;
        $snapshot->left_requester_note = $note;
        $snapshot->left_requester_modify_time = Carbon::now(session('sinsei_user')->workLocation->getTimezone()->name_id)->toDateTimeString();

        $snapshot->right_status = EmployeeWorkingInformationSnapshot::PREVIOUS;

        $snapshot->save();

        $this->createColorStatusForSnapshot($snapshot->id, 'left_work_status_id', ColorStatus::CONSIDERING_COLOR);
        $this->createColorStatusForSnapshot($snapshot->id, 'left_switch_planned_schedule_target', ColorStatus::CONSIDERING_COLOR);

        $this->createColorStatusForEmployeeWorkingInformation($employee_working_information->id, 'work_status_id', ColorStatus::CONSIDERING_COLOR,
            WorkStatus::FURIKYUU);
    }

    /**
     * Create snapshot for default's case
     *
     * @param App\EmployeeWorkingDay            $employee_working_day,
     * @return $snapshot,
     *
     */
    protected function createDefaultSnapshot($employee_working_day)
    {
        $snapshot = new EmployeeWorkingInformationSnapshot([
            'left_timestamped_start_work_date'                  => null,
            'left_timestamped_start_work_time'                  => null,
            'left_timestamped_end_work_time_work_location_id'   => null,
            'left_timestamped_end_work_date'                    => null,
            'left_timestamped_end_work_time'                    => null,
            'left_work_status_id'                               => null,
            'left_rest_status_id'                               => null,
            'left_paid_rest_time_start'                         => null,
            'left_paid_rest_time_end'                           => null,
            'left_paid_rest_time_period'                        => null,
            'left_planned_work_location_id'                     => null,
            'left_planned_early_arrive_start'                   => null,
            'left_planned_early_arrive_end'                     => null,
            'left_planned_overtime_start'                       => null,
            'left_planned_overtime_end'                         => null,
            'left_planned_break_time'                           => null,
            'left_real_break_time'                              => null,
            'left_planned_night_break_time'                     => null,
            'left_real_night_break_time'                        => null,
            'left_planned_late_time'                            => null,
            'left_planned_early_leave_time'                     => null,
            'left_planned_go_out_time'                          => null,
            'schedule_working_hour'                             => null,
            'left_requester_note'                               => null,
        ]);
        $snapshot->employee_working_day_id = $employee_working_day->id;
        return $snapshot;
    }

    /**
     * Create snapshot for furide's case
     *
     * @param App\EmployeeWorkingDay            $employee_working_day_to,
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot,
     * @param array    $column_name_data,
     *
     */
    protected function createAndSaveSnapshotFuride($employee_working_day_to, $snapshot, $column_name_data)
    {
        $snapshot_new = new EmployeeWorkingInformationSnapshot(array_only($snapshot, $column_name_data));
        $snapshot_new->employee_working_day_id = $employee_working_day_to->id;
        $snapshot_new->soon_to_be_EWI_id = $snapshot['employee_working_information_id'];
        $snapshot_new->left_status = EmployeeWorkingInformationSnapshot::CONSIDERING;
        $snapshot_new->right_status = EmployeeWorkingInformationSnapshot::PREVIOUS;
        $snapshot_new->left_requester_modify_time = Carbon::now(session('sinsei_user')->workLocation->getTimezone()->name_id)->toDateTimeString();
        $snapshot_new->save();

        $employee_working_information = EmployeeWorkingInformation::find($snapshot_new->soon_to_be_EWI_id);
        $array_diff_attribute = [];
        foreach ($column_name_data as $name) {
            $origin_name = str_after($name, 'left_');
            if (in_array($origin_name, ['planned_break_time', 'real_break_time', 'planned_night_break_time', 'real_night_break_time'])) {
                if ($snapshot_new->$name !== $employee_working_information->getOriginal($origin_name)) {
                    $this->createColorStatusForSnapshot($snapshot_new->id, $name, ColorStatus::CONSIDERING_COLOR);
                    $array_diff_attribute[$name] = $snapshot_new->$name;
                }
            } else if (in_array($origin_name, ["timestamped_start_work_date", "timestamped_start_work_time"])
                && empty(array_intersect(["left_timestamped_start_work_date", "left_timestamped_start_work_time"], $array_diff_attribute))) {
                if (isset($employee_working_information->timestamped_start_work_time)) {
                    $timestamped_start_work = explode(" ", $employee_working_information->timestamped_start_work_time);
                    if (isset($snapshot_new['left_timestamped_start_work_date']) && $snapshot_new['left_timestamped_start_work_date'] !== $timestamped_start_work[0]) {
                        $array_diff_attribute['left_timestamped_start_work_date'] = $snapshot_new['left_timestamped_start_work_date'];
                    }
                    if (isset($snapshot_new['left_timestamped_start_work_time']) && $snapshot_new['left_timestamped_start_work_time'] !== $timestamped_start_work[1]) {
                        $array_diff_attribute['left_timestamped_start_work_time'] = $snapshot_new['left_timestamped_start_work_time'];
                    }
                } else {
                    if (isset($snapshot_new['left_timestamped_start_work_date']))
                        $array_diff_attribute['left_timestamped_start_work_date'] = $snapshot_new['left_timestamped_start_work_date'];
                    if (isset($snapshot_new['left_timestamped_start_work_time']))
                        $array_diff_attribute['left_timestamped_start_work_time'] = $snapshot_new['left_timestamped_start_work_time'];
                }
                if (isset($array_diff_attribute['left_timestamped_start_work_time']) || isset($array_diff_attribute['left_timestamped_start_work_date'])) {
                    $this->createColorStatusForSnapshot($snapshot_new->id, 'left_timestamped_start_work_date', ColorStatus::CONSIDERING_COLOR);
                    $this->createColorStatusForSnapshot($snapshot_new->id, 'left_timestamped_start_work_time', ColorStatus::CONSIDERING_COLOR);
                }
            } else if (in_array($origin_name, ["timestamped_end_work_date", "timestamped_end_work_time"])
                && empty(array_intersect(["left_timestamped_end_work_date", "left_timestamped_end_work_time"], $array_diff_attribute))) {
                if (isset($employee_working_information->timestamped_end_work_time)) {
                    $timestamped_end_work = explode(" ", $employee_working_information->timestamped_end_work_time);
                    if (isset($snapshot_new['left_timestamped_end_work_date']) && $snapshot_new['left_timestamped_end_work_date'] !== $timestamped_end_work[0]) {
                        $array_diff_attribute['left_timestamped_end_work_date'] = $snapshot_new['left_timestamped_end_work_date'];
                    }
                    if (isset($snapshot_new['left_timestamped_end_work_time']) && $snapshot_new['left_timestamped_end_work_time'] !== $timestamped_end_work[1]) {
                        $array_diff_attribute['left_timestamped_end_work_time'] = $snapshot_new['left_timestamped_end_work_time'];
                    }
                } else {
                    if (isset($snapshot_new['left_timestamped_end_work_date']))
                        $array_diff_attribute['left_timestamped_end_work_date'] = $snapshot_new['left_timestamped_end_work_date'];
                    if (isset($snapshot_new['left_timestamped_end_work_time']))
                        $array_diff_attribute['left_timestamped_end_work_time'] = $snapshot_new['left_timestamped_end_work_time'];
                }
                if (isset($array_diff_attribute['left_timestamped_end_work_time']) || isset($array_diff_attribute['left_timestamped_end_work_date'])) {
                    $this->createColorStatusForSnapshot($snapshot_new->id, 'left_timestamped_end_work_date', ColorStatus::CONSIDERING_COLOR);
                    $this->createColorStatusForSnapshot($snapshot_new->id, 'left_timestamped_end_work_time', ColorStatus::CONSIDERING_COLOR);
                }
            } elseif (in_array($origin_name, ["paid_rest_time_start", "paid_rest_time_end", "planned_early_arrive_start",
                                            "planned_early_arrive_end", "planned_overtime_start", "planned_overtime_end"])) {
                if (isset($snapshot_new->$name) || isset($employee_working_information->$origin_name)) {
                    if (!(isset($snapshot_new->$name)
                        && isset($employee_working_information->$origin_name)
                        && $this->checkSameDatetime($snapshot_new->left_switch_planned_schedule_target, $employee_working_information->$origin_name,
                        $employee_working_day_to->date, $snapshot_new->$name))) {

                        $this->createColorStatusForSnapshot($snapshot_new->id, $name, ColorStatus::CONSIDERING_COLOR);
                        $array_diff_attribute[$name] = $snapshot_new->$name;
                    }
                }
            } else {
                if (in_array($origin_name, ["rest_status_id", "work_status_id"])) $origin_name = "planned_" . $origin_name;
                if ((isset($snapshot_new->$name) || isset($employee_working_information->$origin_name))
                    && $snapshot_new->$name !== $employee_working_information->$origin_name
                    && !in_array($origin_name, ['planned_work_span_start', 'planned_work_span_end', 'requester_note'])) {

                    $this->createColorStatusForSnapshot($snapshot_new->id, $name, ColorStatus::CONSIDERING_COLOR);
                    $array_diff_attribute[$name] = $snapshot_new->$name;
                }
            }
        }
        // Will add work_status_id if it is not
        if (!isset($array_diff_attribute['left_work_status_id'])) {
            $array_diff_attribute['left_work_status_id'] = WorkStatus::FURIDE;
            $this->createColorStatusForSnapshot($snapshot_new->id, 'left_work_status_id', ColorStatus::CONSIDERING_COLOR);
        }
        $this->createColorStatusForSubstituteEmployeeWorkingInformationOrEmployeeWorkingInformation($array_diff_attribute, $snapshot_new,
            $this->createSubstituteEmployeeWorkingInformation($employee_working_day_to->id, $snapshot_new->soon_to_be_EWI_id)->id);
    }

    /**
     * Create or update snapshot with employee working information
     *
     * @param App\EmployeeWorkingInformationSnapshot    $employee_working_information_snapshot,
     * @param array    $column_name_data,
     * @return void
     */
    protected function createOrUpdateSnapshot($employee_working_information_snapshot, $column_name_data)
    {
        $snapshot_id = $employee_working_information_snapshot['id'] ?? null;
        $snapshot = EmployeeWorkingInformationSnapshot::firstOrNew(['id' => $snapshot_id]);
        $snapshot->fill(array_only($employee_working_information_snapshot, $column_name_data));
        $snapshot->left_status = EmployeeWorkingInformationSnapshot::CONSIDERING;
        $snapshot->right_status = EmployeeWorkingInformationSnapshot::PREVIOUS;
        $snapshot->employeeWorkingDay()->associate($employee_working_information_snapshot['employee_working_day_id']);
        $snapshot->employeeWorkingInformation()->associate($employee_working_information_snapshot['employee_working_information_id']);
        $snapshot->left_requester_modify_time = Carbon::now(session('sinsei_user')->workLocation->getTimezone()->name_id)->toDateTimeString();

        $snapshot->target_absorb_EWI_id = $employee_working_information_snapshot['target_absorb_EWI_id'] ?? null;
        $snapshot->left_soon_to_be_absorb = $employee_working_information_snapshot['left_soon_to_be_absorb'] ?? false;

        $employee_working_information = $snapshot->employeeWorkingInformation;
        $array_diff_attribute = [];
        foreach ($column_name_data as $name) {
            $origin_name = str_after($name, 'left_');
            if (in_array($origin_name, ['planned_break_time', 'real_break_time', 'planned_night_break_time', 'real_night_break_time'])) {
                if ($snapshot->$name !== $employee_working_information->getOriginal($origin_name)) {
                    $array_diff_attribute[$name] = $snapshot->$name;
                }
            } else if (in_array($origin_name, ["timestamped_start_work_date", "timestamped_start_work_time"])
                && empty(array_intersect(["left_timestamped_start_work_date", "left_timestamped_start_work_time"], $array_diff_attribute))) {
                if (isset($employee_working_information->timestamped_start_work_time)) {
                    $timestamped_start_work = explode(" ", $employee_working_information->timestamped_start_work_time);
                    if (isset($snapshot['left_timestamped_start_work_date']) && $snapshot['left_timestamped_start_work_date'] !== $timestamped_start_work[0]) {
                        $array_diff_attribute['left_timestamped_start_work_date'] = $snapshot['left_timestamped_start_work_date'];
                    }
                    if (isset($snapshot['left_timestamped_start_work_time']) && $snapshot['left_timestamped_start_work_time'] !== $timestamped_start_work[1]) {
                        $array_diff_attribute['left_timestamped_start_work_time'] = $snapshot['left_timestamped_start_work_time'];
                    }
                } else {
                    if (isset($snapshot['left_timestamped_start_work_date']))
                        $array_diff_attribute['left_timestamped_start_work_date'] = $snapshot['left_timestamped_start_work_date'];
                    if (isset($snapshot['left_timestamped_start_work_time']))
                        $array_diff_attribute['left_timestamped_start_work_time'] = $snapshot['left_timestamped_start_work_time'];
                }
            } else if (in_array($origin_name, ["timestamped_end_work_date", "timestamped_end_work_time"])
                && empty(array_intersect(["left_timestamped_end_work_date", "left_timestamped_end_work_time"], $array_diff_attribute))) {
                if (isset($employee_working_information->timestamped_end_work_time)) {
                    $timestamped_end_work = explode(" ", $employee_working_information->timestamped_end_work_time);
                    if (isset($snapshot['left_timestamped_end_work_date']) && $snapshot['left_timestamped_end_work_date'] !== $timestamped_end_work[0]) {
                        $array_diff_attribute['left_timestamped_end_work_date'] = $snapshot['left_timestamped_end_work_date'];
                    }
                    if (isset($snapshot['left_timestamped_end_work_time']) && $snapshot['left_timestamped_end_work_time'] !== $timestamped_end_work[1]) {
                        $array_diff_attribute['left_timestamped_end_work_time'] = $snapshot['left_timestamped_end_work_time'];
                    }
                } else {
                    if (isset($snapshot['left_timestamped_end_work_date']))
                        $array_diff_attribute['left_timestamped_end_work_date'] = $snapshot['left_timestamped_end_work_date'];
                    if (isset($snapshot['left_timestamped_end_work_time']))
                        $array_diff_attribute['left_timestamped_end_work_time'] = $snapshot['left_timestamped_end_work_time'];
                }
            } else {
                if (in_array($origin_name, ["rest_status_id", "work_status_id"])) $origin_name = "planned_" . $origin_name;
                if ((isset($snapshot->$name) || isset($employee_working_information->$origin_name)) && $snapshot->$name !== $employee_working_information->$origin_name
                    && !in_array($origin_name, ['planned_work_span_start', 'planned_work_span_end', 'requester_note'])) {
                    $array_diff_attribute[$name] = $snapshot->$name;
                }
            }
        }

        if (count($array_diff_attribute) > 0) {
            $snapshot = $this->mergeEmployeeWorkingInformationToTheLeftAndRightSnapshot($snapshot, $employee_working_information);
            $snapshot->save();

            if (isset($snapshot->target_absorb_EWI_id)) {
                $target_absorb_EWI_new = EmployeeWorkingInformation::find($snapshot->target_absorb_EWI_id);

                $array_attribute_need = $this->checkTargetAbsorbEWI(array_keys($array_diff_attribute), $snapshot, $target_absorb_EWI_new);
                $array_diff_attribute = array_only($array_diff_attribute, $array_attribute_need);
            }

            foreach ($snapshot->colorStatuses()->get() as $color) {
                if (!array_search($color->field_name, $array_diff_attribute))
                    $color->delete();
            }

            foreach ($array_diff_attribute as $key => $name) {
                $this->createColorStatusForSnapshot($snapshot->id, $key, ColorStatus::CONSIDERING_COLOR);
            }
            $this->createOrRemoveSubstituteOrWorkingInformationColor($array_diff_attribute,
                $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot(), null, $snapshot->employeeWorkingInformation);

        } else {
            if (isset($snapshot->id)) {
                if ($snapshot->left_soon_to_be_absorb == true) {
                    $snapshot = $this->mergeEmployeeWorkingInformationToTheLeftAndRightSnapshot($snapshot, $employee_working_information);
                    $employee_working_information->colorStatuses()->where('field_css_class', ColorStatus::CONSIDERING_COLOR)->delete();
                    $snapshot->colorStatuses()->delete();
                    $snapshot->save();
                } else {
                    if (isset($snapshot->employee_working_information_id))
                        $snapshot->employeeWorkingInformation->colorStatuses()->where('field_css_class', ColorStatus::CONSIDERING_COLOR)->delete();
                    $snapshot->forceDelete();
                    $snapshot->colorStatuses()->delete();
                }
            }
        }
    }

    /**
     * Default, the function will be merge the employee working information to the right of snapshot
     * Sometime, will merge to the left and right of snapshot
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot,
     * @param App\EmployeeWorkingInformation    $employee_working_information,
     * @return App\EmployeeWorkingInformationSnapshot    $snapshot,
     */
    protected function mergeEmployeeWorkingInformationToTheLeftAndRightSnapshot($snapshot, $employee_working_information, $both = false)
    {
        $column_name_data = [
            'timestamped_start_work_time_work_location_id' => 'timestamped_start_work_time_work_location_id',
            'timestamped_start_work_date' => 'timestamped_start_work_time',
            'timestamped_start_work_time' => 'timestamped_start_work_time',
            'timestamped_end_work_time_work_location_id' => 'timestamped_end_work_time_work_location_id',
            'timestamped_end_work_date' => 'timestamped_end_work_time',
            'timestamped_end_work_time' => 'timestamped_end_work_time',
            'work_status_id' => 'planned_work_status_id',
            'rest_status_id' => 'planned_rest_status_id',
            'switch_planned_schedule_target' => 'switch_planned_schedule_target',
            'paid_rest_time_start' => 'paid_rest_time_start',
            'paid_rest_time_end' => 'paid_rest_time_end',
            'paid_rest_time_period' => 'paid_rest_time_period',
            'planned_work_location_id' => 'planned_work_location_id',
            'planned_early_arrive_start' => 'planned_early_arrive_start',
            'planned_early_arrive_end' => 'planned_early_arrive_end',
            'planned_work_span_start' => 'planned_work_span_start',
            'planned_work_span_end' => 'planned_work_span_end',
            'planned_overtime_start' => 'planned_overtime_start',
            'planned_overtime_end' => 'planned_overtime_end',
            'planned_work_span' => 'planned_work_span',
            'planned_break_time' => 'planned_break_time',
            'real_break_time' => 'real_break_time',
            'planned_night_break_time' => 'planned_night_break_time',
            'real_night_break_time' => 'real_night_break_time',
            'planned_late_time' => 'planned_late_time',
            'planned_early_leave_time' => 'planned_early_leave_time',
            'planned_go_out_time' => 'planned_go_out_time',
        ];
        foreach ($column_name_data as $key => $value) {
            if (in_array($key, ['timestamped_start_work_date', 'timestamped_end_work_date'])) {
                if (isset($employee_working_information->$value))
                    $snapshot['right_' . $key] = Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->$value)->toDateString();
            } else if (in_array($key, ['timestamped_start_work_time', 'timestamped_end_work_time'])) {
                if (isset($employee_working_information->$value))
                    $snapshot['right_' . $key] = Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->$value)->format('H:i:s');
            } else {
                $snapshot['right_' . $key] = $employee_working_information->$value;
            }
        }

        if ($both) {
            foreach ($column_name_data as $key => $value) {
                if (in_array($key, ['timestamped_start_work_date', 'timestamped_end_work_date'])) {
                    if (isset($employee_working_information->$value))
                        $snapshot['left_' . $key] = Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->$value)->toDateString();
                } else if (in_array($key, ['timestamped_start_work_time', 'timestamped_end_work_time'])) {
                    if (isset($employee_working_information->$value))
                        $snapshot['left_' . $key] = Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->$value)->format('H:i:s');
                } else {
                    $snapshot['left_' . $key] = $employee_working_information->$value;
                }
            }
        }

        return $snapshot;
    }

    /**
     * Check list date
     *
     * @param array format(yyyy-mm-dd)    $date_list
     * @retrun array $date_list
     *
     */
    protected function checkDateList($date_list)
    {
        $count = 0;
        foreach ($date_list as $key => $date) {
            if ($this->validateDateByFormat($date)) {
                $employee_working_day = EmployeeWorkingDay::where('employee_id', session('sinsei_user')->id)->where('date', $date)->first();
                if ($employee_working_day) {
                    foreach ($employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {
                        if ($snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING && $snapshot->left_switch_planned_schedule_target
                            && $snapshot->colorStatuses()->get()->contains('field_name', 'left_switch_planned_schedule_target')) {
                            array_splice($date_list, $key + $count + 1, 0, $snapshot->left_switch_planned_schedule_target);
                            $count++;
                            break;
                        }
                    }
                }
            } else
                array_splice($date_list, $key, 1);
        }
        return array_unique($date_list);
    }

    /**
     * Search snapshot from the trash
     *
     * @param snapshot's array    $snapshots
     * @retrun snapshot  $snapshot or null
     *
     */
    protected function searchSnapshotInTheTrashed($snapshots)
    {
        foreach ($snapshots as $snapshot) {
            if ($snapshot->left_status != EmployeeWorkingInformationSnapshot::CONSIDERING || ($snapshot->left_status ==
                EmployeeWorkingInformationSnapshot::CONSIDERING && !$snapshot->left_switch_planned_schedule_target))
                return $snapshot;
        }
        return null;
    }

    /**
     * The function will return the list which it is include all sinseichu's day
     *
     * @retrun array(yyyy-mm-dd)  $list_sinsei_chu
     *
     */
    protected function getListSinseiChu()
    {
        $list_sinsei_chu = [];
        $today = Carbon::now(session('sinsei_user')->workLocation->getTimezone()->name_id);
        $start_date = $today->day(1)->format('Y-m-d');
        $end_date = $today->addMonths(3)->format('Y-m-t');
        $employee_working_days = EmployeeWorkingDay::with('employeeWorkingInformationSnapshots')->where('employee_id', session('sinsei_user')->id)->whereBetween('date', [$start_date, $end_date])->get();
        foreach ($employee_working_days as $employee_working_day) {
            foreach ($employee_working_day->employeeWorkingInformationSnapshots as $employeeWorkingInformationSnapshot) {
                if ($employeeWorkingInformationSnapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING) {
                    array_push($list_sinsei_chu, $employee_working_day->date);
                    break;
                }
            }
        }
        return $list_sinsei_chu;
    }

    /**
     * A shortcut to create a ColorStatus record for snapshot's case.
     *
     * @param int       $snapshot_id
     * @param string    $field_name
     * @param int       $field_css_class
     * @return void
     */
    protected function createColorStatusForSnapshot($snapshot_id, $field_name, $field_css_class)
    {
        ColorStatus::updateOrCreate(
            ['colorable_type' => ColorStatus::EMPLOYEE_WORKING_INFORMATION_SNAPSHOT,
            'field_name' => $field_name,
            'colorable_id' => $snapshot_id],
            ['field_css_class' => $field_css_class]
        );
    }

    /**
     * A shortcut to create a ColorStatus record for employee working information's case.
     *
     * @param int       $employee_working_information_id
     * @param string    $field_name
     * @param int       $field_css_class
     * @return void
     */
    protected function createColorStatusForEmployeeWorkingInformation($employee_working_information_id, $field_name, $field_css_class, $field_fake_value)
    {
        ColorStatus::create(
            ['colorable_type' => ColorStatus::EMPLOYEE_WORKING_INFORMATION,
            'field_name' => $field_name,
            'colorable_id' => $employee_working_information_id,
            'field_css_class' => $field_css_class,
            'field_fake_value' => $field_fake_value]
        );
    }

    /**
     * A shortcut to create a ColorStatus record for employee working day's case.
     *
     * @param int       $employee_working_information_id
     * @param string    $field_name
     * @param int       $field_css_class
     * @return void
     */
    protected function createColorStatusForSubstituteEmployeeWorkingInformation($substitute_working_information_id, $field_name,
        $field_css_class, $field_fake_value)
    {
        ColorStatus::updateOrCreate(
            ['colorable_type' => ColorStatus::SUBSTITUTE_EMPLOYEE_WORKING_INFORMATION,
            'field_name' => $field_name,
            'colorable_id' => $substitute_working_information_id],
            ['field_css_class' => $field_css_class,
            'field_fake_value' => $field_fake_value]
        );
    }

    /**
     * The function will return the list of color follow employee working day
     *
     * @param App\EmployWorkingDay       $employee_working_day
     * @return array $list_color_status_for_working_day
     */
    protected function getColorStatusWithEmployWorkingDay($employee_working_day)
    {
        $list_color_status_for_working_day= collect([]);
        foreach ($employee_working_day->employeeWorkingInformations()->get() as $employee_working_information) {
            if ($employee_working_information->employeeWorkingInformationSnapshot)
                $list_color_status_for_working_day->push($employee_working_information->employeeWorkingInformationSnapshot->colorStatuses()->get());
            else
                $list_color_status_for_working_day->push([]);
        }

        // UPDATE 2019-01-22: Now we will always query for transfer snapshots
        $transfer_snapshots = $employee_working_day->employeeWorkingInformationSnapshots()
            ->whereNull('employee_working_information_id')
            ->whereNotNull('soon_to_be_EWI_id')->get();
        foreach ($transfer_snapshots as $snapshot_for_furide_case) {
            $list_color_status_for_working_day->push($snapshot_for_furide_case->colorStatuses()->get());
        }

        // UPDATE 2019-01-22: Change for the new spec of furikae the 'if' below is for preparing data for HOUDE/KYUUDE cases. it will be kept for the moment.
        // The 'if' that has been commented out is for preparing data for FURIKAE feature. It has been refactored into the code above(with out the if).

        // if (count($list_color_status_for_working_day) == 0) {
        //     $snapshotForFurideCases = $employee_working_day->employeeWorkingInformationSnapshots()->whereNull('employee_working_information_id')
        //     ->whereNotNull('soon_to_be_EWI_id')->get();
        //     foreach ($snapshotForFurideCases as $snapshotForFurideCase) {
        //         $list_color_status_for_working_day->push($snapshotForFurideCase->colorStatuses()->get());
        //     }
            if(count($list_color_status_for_working_day) == 0) {
                $snapshot = $employee_working_day->employeeWorkingInformationSnapshots()->whereNull('employee_working_information_id')
                ->whereNull('soon_to_be_EWI_id')->first();
                if ($snapshot)
                    $list_color_status_for_working_day->push($snapshot->colorStatuses()->get());
            }
        // }
        return $list_color_status_for_working_day;
    }

    /**
     * The function will create color status of SubstituteEmployeeWorkingInformation or color status of EmployeeWorkingInformation
     *
     * @param array    $array_diff_attribute
     * @param App\EmployeeWorkingInformationSnapshot     $snapshot
     * @param int     $substitute_employee_working_information_id
     * @param int     $employee_working_information_id
     * @return void
     */
    protected function createColorStatusForSubstituteEmployeeWorkingInformationOrEmployeeWorkingInformation($array_diff_attribute, $snapshot,
        $substitute_employee_working_information_id = null, $employee_working_information_id = null)
    {
        $check = (isset($employee_working_information_id)) ? true : false ;
        $employee_working_information = (isset($employee_working_information_id)) ? EmployeeWorkingInformation::find($employee_working_information_id) : null;
        $check_late_and_leave_early = $check_zangyou = $check_timestamped_start_work = $check_timestamped_end_work = $check_rest_status_id = false;

        foreach ($array_diff_attribute as $key => $item) {
            $name = $value = null;
            if (in_array($key, ['left_planned_late_time', 'left_planned_early_leave_time'])) {
                if (!$check_late_and_leave_early) {

                    $left_planned_early_leave_time = 0;

                    if (array_key_exists("left_planned_early_leave_time", $array_diff_attribute))
                        $left_planned_early_leave_time = $array_diff_attribute["left_planned_early_leave_time"] ?? 0;
                    else if (isset($employee_working_information->planned_early_leave_time))
                        $left_planned_early_leave_time = $employee_working_information->planned_early_leave_time;

                    $left_planned_late_time = 0;

                    if (array_key_exists("left_planned_late_time", $array_diff_attribute))
                        $left_planned_late_time = $array_diff_attribute["left_planned_late_time"] ?? 0;
                    else if (isset($employee_working_information->planned_late_time))
                        $left_planned_late_time = $employee_working_information->planned_late_time;

                    $name = 'planned_total_late_and_leave_early';
                    $value = $left_planned_late_time + $left_planned_early_leave_time;
                    $check_late_and_leave_early = true;
                }
            } else if (in_array($key, ['left_planned_early_arrive_start', 'left_planned_early_arrive_end', 'left_planned_overtime_start', 'left_planned_overtime_end'])) {
                if (!$check_zangyou) {

                    $planned_total_early_arrive = isset($employee_working_information->planned_early_arrive_start) ?
                        Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->planned_early_arrive_start)
                        ->diffInMinutes(Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->planned_early_arrive_end)) : 0;
                    $planned_total_overtime = isset($employee_working_information->planned_overtime_start) ?
                        Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->planned_overtime_start)
                        ->diffInMinutes(Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->planned_overtime_end)) : 0;

                    if (array_key_exists("left_planned_early_arrive_start", $array_diff_attribute) || array_key_exists("left_planned_early_arrive_end",
                        $array_diff_attribute)) {
                        if (!isset($array_diff_attribute["left_planned_early_arrive_start"]) && !isset($array_diff_attribute["left_planned_early_arrive_end"]))
                            $planned_total_early_arrive = 0;
                        else {
                            $left_planned_early_arrive_start = isset($array_diff_attribute["left_planned_early_arrive_start"])
                                ? $array_diff_attribute["left_planned_early_arrive_start"] : $employee_working_information->planned_early_arrive_start;
                            $left_planned_early_arrive_end = isset($array_diff_attribute["left_planned_early_arrive_end"])
                                ? $array_diff_attribute["left_planned_early_arrive_end"] : $employee_working_information->planned_early_arrive_end;
                            $planned_total_early_arrive = Carbon::createFromFormat('Y-m-d H:i:s', $left_planned_early_arrive_start)->diffInMinutes(Carbon::createFromFormat('Y-m-d H:i:s',
                                    $left_planned_early_arrive_end));
                        }
                    }

                    if (array_key_exists("left_planned_overtime_start", $array_diff_attribute) || array_key_exists("left_planned_overtime_end",
                        $array_diff_attribute)) {
                        if (!isset($array_diff_attribute["left_planned_overtime_start"]) && !isset($array_diff_attribute["left_planned_overtime_end"]))
                            $planned_total_overtime = 0;
                        else {
                            $left_planned_overtime_start = isset($array_diff_attribute["left_planned_overtime_start"])
                                ? $array_diff_attribute["left_planned_overtime_start"] : $employee_working_information->planned_overtime_start;
                            $left_planned_overtime_end = isset($array_diff_attribute["left_planned_overtime_end"])
                                ? $array_diff_attribute["left_planned_overtime_end"] : $employee_working_information->planned_overtime_end;
                            $planned_total_overtime = Carbon::createFromFormat('Y-m-d H:i:s', $left_planned_overtime_start)->diffInMinutes(Carbon::createFromFormat('Y-m-d H:i:s',
                                    $left_planned_overtime_end));
                        }
                    }

                    $name = 'planned_total_early_arrive_and_overtime';

                    $work_status_id = array_key_exists("left_work_status_id", $array_diff_attribute) ?
                                        $array_diff_attribute["left_work_status_id"] : $employee_working_information->planned_work_status_id;
                    if (in_array($work_status_id, [WorkStatus::HOUDE, WorkStatus::KYUUDE, WorkStatus::ZANGYOU])) {
                        $planned_break_time = 0;

                        if (array_key_exists("left_planned_break_time", $array_diff_attribute)) {
                            $planned_break_time = $array_diff_attribute["left_planned_break_time"];
                        } else {
                            if ($check)
                                $planned_break_time = $employee_working_information->planned_break_time;
                        }

                        $value = $planned_total_early_arrive + $planned_total_overtime - $planned_break_time;
                    } else
                        $value = $planned_total_early_arrive + $planned_total_overtime;

                    if ($value < 0) $value = 0;
                    $check_zangyou = true;
                }
            } else if (in_array($key, ['left_timestamped_start_work_date', 'left_timestamped_start_work_time'])) {
                if (!$check_timestamped_start_work) {

                    $left_planned_work_location_id = $start_work_time = null;
                    if ($check) {
                        $left_planned_work_location_id = $employee_working_information->planned_work_location_id;
                        $start_work_time = ($employee_working_information->timestamped_start_work_time)
                            ? Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->getOriginal('timestamped_start_work_time')) : null;
                    }

                    $name = 'timestamped_start_work_time';
                    $date = $array_diff_attribute["left_timestamped_start_work_date"] ?? $start_work_time->toDateString();
                    $time = $array_diff_attribute["left_timestamped_start_work_time"] ?? $start_work_time->toTimeString();

                    if (isset($array_diff_attribute["left_planned_work_location_id"]))
                        $left_planned_work_location_id = $array_diff_attribute["left_planned_work_location_id"];

                    $time = $this->getTimeRounded($date, $time, $left_planned_work_location_id)->toTimeString();

                    $value = $date . " " . $time;
                    $check_timestamped_start_work = true;
                }
            } else if (in_array($key, ['left_timestamped_end_work_date', 'left_timestamped_end_work_time', 'left_timestamped_end_work_time_work_location_id'])) {
                if (!$check_timestamped_end_work) {

                    $left_planned_work_location_id = $end_work_time = null;
                    if ($check) {
                        $left_planned_work_location_id = $employee_working_information->planned_work_location_id;
                        $end_work_time = ($employee_working_information->timestamped_end_work_time)
                            ? Carbon::createFromFormat('Y-m-d H:i:s', $employee_working_information->getOriginal('timestamped_end_work_time')) : null;
                    }

                    $name = 'timestamped_end_work_time';
                    $date = $array_diff_attribute["left_timestamped_end_work_date"] ?? $end_work_time->toDateString();
                    $time = $array_diff_attribute["left_timestamped_end_work_time"] ?? $end_work_time->toTimeString();

                    if (isset($array_diff_attribute["left_planned_work_location_id"]))
                        $left_planned_work_location_id = $array_diff_attribute["left_planned_work_location_id"];

                    $time = $this->getTimeRounded($date, $time, $left_planned_work_location_id, false)->toTimeString();

                    $value = $date . " " . $time;
                    $check_timestamped_end_work = true;
                }
            } else if (in_array($key, ['left_rest_status_id', 'left_paid_rest_time_start', 'left_paid_rest_time_end', 'left_paid_rest_time_period'])) {
                if (!$check_rest_status_id) {

                    $name = 'rest_status_id';
                    $value = null;
                    if (array_key_exists("left_rest_status_id", $array_diff_attribute))
                        $value = $array_diff_attribute["left_rest_status_id"] ?? config('caeru.empty');
                    else
                        $value = $snapshot["left_rest_status_id"];

                    $check_rest_status_id = true;
                }
            } else {
                $name = str_after($key, 'left_');
                $value = $item ?? config('caeru.empty');
            }
            if($name)
                $this->createSubstituteColorOrEmployeeWorkingInformationColor($check ? $employee_working_information_id :
                    $substitute_employee_working_information_id, $check, $name, $value);
        }
    }

    /**
     * The function will remove the previous color of snapshot, then will create color status of SubstituteEmployeeWorkingInformation or color status of EmployeeWorkingInformation
     *
     * @param array    $array_diff_attribute
     * @param App\EmployeeWorkingInformationSnapshot     $snapshot
     * @param App\SubstituteEmployeeWorkingInformation     $substitute_employee_working_information
     * @param App\EmployeeWorkingInformation     $employee_working_information
     * @return void
     */
    protected function createOrRemoveSubstituteOrWorkingInformationColor($array_diff_attribute, $snapshot,
        SubstituteEmployeeWorkingInformation $substitute_employee_working_information = null, EmployeeWorkingInformation $employee_working_information = null)
    {
        $color_status = ($employee_working_information) ? $employee_working_information->colorStatuses()->get() :
            $substitute_employee_working_information->colorStatuses()->get() ;
        foreach ($color_status as $color) {
            if (!in_array($color->field_css_class, [ColorStatus::APPROVED_COLOR, ColorStatus::REJECTED_COLOR, ColorStatus::ERROR_COLOR, ColorStatus::UPDATED_COLOR, ColorStatus::BLANK_COLOR])) {
                if ($color->field_name === 'planned_total_early_arrive_and_overtime') {
                    if (!array_key_exists("left_planned_early_arrive_start", $array_diff_attribute) &&
                        !array_key_exists("left_planned_early_arrive_end", $array_diff_attribute) &&
                        !array_key_exists("left_planned_overtime_start", $array_diff_attribute) &&
                        !array_key_exists("left_planned_overtime_end", $array_diff_attribute))
                        $color->delete();
                } elseif ($color->field_name === 'planned_total_late_and_leave_early') {
                    if (!array_key_exists("left_planned_late_time", $array_diff_attribute) &&
                    !array_key_exists("left_planned_early_leave_time", $array_diff_attribute))
                        $color->delete();
                } elseif ($color->field_name === 'timestamped_start_work_time') {
                    if (!array_key_exists("left_timestamped_start_work_date", $array_diff_attribute) &&
                    !array_key_exists("left_timestamped_start_work_time", $array_diff_attribute))
                        $color->delete();
                } elseif ($color->field_name === 'timestamped_end_work_time') {
                    if (!array_key_exists("left_timestamped_end_work_date", $array_diff_attribute) &&
                    !array_key_exists("left_timestamped_end_work_time", $array_diff_attribute))
                        $color->delete();
                } else
                    if (!array_key_exists("left_" . $color->field_name, $array_diff_attribute))
                        $color->delete();
            }
        }
        $this->createColorStatusForSubstituteEmployeeWorkingInformationOrEmployeeWorkingInformation($array_diff_attribute,
            $snapshot, $substitute_employee_working_information->id ?? null, $employee_working_information->id ?? null);
    }

    /**
     * Create substitute employee working information
     *
     * @param int   $employee_working_day_id
     * @param int(default null)   $employee_working_information_id
     * @return SubstituteEmployeeWorkingInformation $new_substitute
     */
    protected function createSubstituteEmployeeWorkingInformation($employee_working_day_id, $employee_working_information_id = null)
    {
        $new_substitute = new SubstituteEmployeeWorkingInformation();

        $new_substitute->employee_working_day_id = $employee_working_day_id;
        $new_substitute->employee_working_information_id = $employee_working_information_id;
        $new_substitute->save();

        return $new_substitute;
    }

    /**
     * The function will create color status of EmployeeWorkingInformation or color status of SubstituteEmployeeWorkingInformation
     *
     * @param int   $id
     * @param boolean   $check
     * @param string   $name
     * @param string   $value
     * @param int   $color
     * @return void
     */
    protected function createSubstituteColorOrEmployeeWorkingInformationColor($id, $check, $name, $value, $color = ColorStatus::CONSIDERING_COLOR)
    {
        if ($check) {
            $this->createColorStatusForEmployeeWorkingInformation($id, $name, $color, $value);
        } else {
            $this->createColorStatusForSubstituteEmployeeWorkingInformation($id, $name, $color, $value);
        }
    }

    /**
     * The function will rounded of time
     *
     * @param date  $date
     * @param time   $time
     * @param int   $location_id
     * @param boolean   $type
     * @return time
     */
    protected function getTimeRounded($date, $time, $location_id, $type_start = true)
    {
        $work_location = WorkLocation::find($location_id);
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $date . " " . $time);

        if ($type_start) {
            $round_up = $work_location->currentSetting()->start_time_round_up;

            if (($round_up >= 1) && ($round_up <= 60)) {
                $minute = $carbon->minute;

                $carbon->minute = ceil($minute/$round_up) * $round_up;
            }
        } else {
            $round_down = $work_location->currentSetting()->end_time_round_down;

            if (($round_down >= 1) && ($round_down <= 60)) {
                $minute = $carbon->minute;

                $carbon->minute = floor($minute/$round_down) * $round_down;
            }
        }
        return $carbon;
    }

    /**
     * The function will check same time
     *
     * @param date   $date_from
     * @param datetime   $datetime_from
     * @param date   $date_to
     * @param datetime   $datetime_to
     * @return boolean
     */
    protected function checkSameDatetime($date_from, $datetime_from, $date_to, $datetime_to)
    {
        $carbon_date_from = Carbon::createFromFormat('Y-m-d', $date_from);
        $carbon_datetime_from = Carbon::createFromFormat('Y-m-d H:i:s', $datetime_from);
        $carbon_date_to = Carbon::createFromFormat('Y-m-d', $date_to);
        $carbon_datetime_to = Carbon::createFromFormat('Y-m-d H:i:s', $datetime_to);

        $diff_in_days = $carbon_date_to->diffInDays($carbon_date_from);

        $carbon_datetime_from_add_day = $carbon_datetime_from->addDays($diff_in_days);

        if ($carbon_datetime_to->toDateTimeString() === $carbon_datetime_from_add_day->toDateTimeString()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * The function will removed same time if target_absorb_EWI_new is exist
     *
     * @param array   $array_diff_attribute
     * @param App/EmployeeWorkingInformationSnapshot   $snapshot
     * @param App/EmployeeWorkingInformation   $target_absorb_EWI_new
     * @return array
     */
    protected function checkTargetAbsorbEWI($array_diff_attribute, $snapshot, $target_absorb_EWI_new)
    {
        if (in_array("left_timestamped_start_work_date", $array_diff_attribute)
            && in_array("left_timestamped_start_work_time", $array_diff_attribute)) {

            $left_timestamped_start_work =
                $this->getTimeRounded($snapshot->left_timestamped_start_work_date, $snapshot->left_timestamped_start_work_time,
                $snapshot->left_planned_work_location_id);

            if ($left_timestamped_start_work->toDateTimeString() === $target_absorb_EWI_new->timestamped_start_work_time) {
                if (($key = array_search("left_timestamped_start_work_date", $array_diff_attribute)) !== false)
                    unset($array_diff_attribute[$key]);

                if (($key = array_search("left_timestamped_start_work_time", $array_diff_attribute)) !== false)
                    unset($array_diff_attribute[$key]);
            }
        }

        if (in_array("left_timestamped_end_work_date", $array_diff_attribute)
            && in_array("left_timestamped_end_work_time", $array_diff_attribute)) {

            $left_timestamped_end_work =
                $this->getTimeRounded($snapshot->left_timestamped_end_work_date, $snapshot->left_timestamped_end_work_time,
                $snapshot->left_planned_work_location_id, false);

            if ($left_timestamped_end_work->toDateTimeString() === $target_absorb_EWI_new->timestamped_end_work_time) {
                if (($key = array_search("left_timestamped_end_work_date", $array_diff_attribute)) !== false)
                    unset($array_diff_attribute[$key]);

                if (($key = array_search("left_timestamped_end_work_time", $array_diff_attribute)) !== false)
                    unset($array_diff_attribute[$key]);

                // We also need remove left_timestamped_end_work_time_work_location_id, because it is also pushed in array different
                // This case also had been lacked while created this function
                if (($key = array_search("left_timestamped_end_work_time_work_location_id", $array_diff_attribute)) !== false)
                    unset($array_diff_attribute[$key]);
            }
        }

        return $array_diff_attribute;
    }

    /**
     * Check if there is any snapshot with WorkAddress fields in the given collection of snapshots.
     *
     * @param   Collection      $snapshot_collection
     * @return  Boolean
     */
    protected function hasSnapshotThatHasWorkAddressFields($snapshot_collection)
    {
        $exist_a_snapshot_with_work_address_fields = $snapshot_collection->first(function($snapshot) {
            return $this->isASnapshotWithWorkAddressFields($snapshot);
        });

        return $exist_a_snapshot_with_work_address_fields ? true : false;
    }

    /**
     * Check if the given snapshot has WorkAddress fields or not
     *
     * @param   Snapshot    $snapshot
     * @return  Boolean
     */
    protected function isASnapshotWithWorkAddressFields($snapshot)
    {
        return $snapshot->left_planned_work_address_id || $snapshot->left_real_work_address_id;
    }

    /**
     * @param Request $request
     * @author LamNguyen <lamnguyen080296@gmail.com>
     */
    public function saveSessionDisplay(Request $request)
    {
        try{
            $request->has('display_toggle_sensei') ? Session::put('display_toggle_sensei',$request->display_toggle_sensei) : null;
            return response()->json(['code' => 200,'msg' => 'success']);
        }
        catch (Exception $exception){
            return response()->json(['code' => 500,'msg' => 'failed']);
        }
    }
}
<?php

namespace App\Http\Controllers\Sinsei\Requester;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\Sinsei\Requester\MaebaraiSettingRequest;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use Caeru;
use CaeruJBA;

class MaebaraiSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('sinsei_auth');
    }

    /**
     * Show Maebarai setting page
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) 
    {
        $employee = session('sinsei_user');
        $employee->refresh();

        $maebarai_setting = [];
        $maebarai_setting['maebarai_bank_code'] = $employee['maebarai_bank_code'];
        $maebarai_setting['maebarai_bank_name'] = $employee['maebarai_bank_name'];
        $maebarai_setting['maebarai_branch_code'] = $employee['maebarai_branch_code'];
        $maebarai_setting['maebarai_branch_name'] = $employee['maebarai_branch_name'];
        $maebarai_setting['maebarai_account_number'] = $employee['maebarai_account_number'];
        $maebarai_setting['maebarai_account_name'] = $employee['maebarai_account_name'];
        $maebarai_setting['maebarai_salary_per_hour'] = $employee['maebarai_salary_per_hour'];
        
        JavaScript::put([
            'maebarai_setting' => $maebarai_setting,
        ]);

        return view('sinsei.maebarai.maebarai_setting');
    }

    /**
     * Update Maebarai Setting in storage
     *
     * @param  App\Http\Requests\Sinsei\Requester\MaebaraiSettingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(MaebaraiSettingRequest $request) 
    {
        $employee = session('sinsei_user');
        $employee->refresh();

        $employee->update($request->only(
            'maebarai_bank_code', 
            'maebarai_branch_code', 
            'maebarai_account_number'));

        $employee->maebarai_bank_name = CaeruJBA::CnvStr($request->input('maebarai_bank_name'));
        $employee->maebarai_branch_name = CaeruJBA::CnvStr($request->input('maebarai_branch_name'));
        $employee->maebarai_account_name = CaeruJBA::CnvStr($request->input('maebarai_account_name'));
        $employee->save();

        $maebarai_setting['maebarai_salary_per_hour'] = $employee['maebarai_salary_per_hour'];
        $maebarai_setting['maebarai_bank_code'] = $employee['maebarai_bank_code'];
        $maebarai_setting['maebarai_bank_name'] = $employee['maebarai_bank_name'];
        $maebarai_setting['maebarai_branch_code'] = $employee['maebarai_branch_code'];
        $maebarai_setting['maebarai_branch_name'] = $employee['maebarai_branch_name'];
        $maebarai_setting['maebarai_account_number'] = $employee['maebarai_account_number'];
        $maebarai_setting['maebarai_account_name'] = $employee['maebarai_account_name'];
        JavaScript::put([
            'maebarai_setting' => $maebarai_setting,
        ]);

        return [
            'success' => '保存しました',
            'settings' => $maebarai_setting,
        ];
    }
}

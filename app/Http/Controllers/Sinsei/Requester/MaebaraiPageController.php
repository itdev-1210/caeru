<?php

namespace App\Http\Controllers\Sinsei\Requester;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Http\Requests\Sinsei\Requester\MaebaraiPayRequest;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\MaebaraiHistory;
use App\MaebaraiTransactionResponse;
use Constants;


class MaebaraiPageController extends Controller
{
    use BusinessMonthTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('sinsei_auth');
    }

    /**
     * Show Maebarai page
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $employee = session('sinsei_user');
        $employee->refresh();

        $month = $this->getMonthThatContainASpecificDay($employee);
        $date =$this->getStartDateAndEndDateFromBusinessMonth($employee, $month);
        $start_date_range = $date[0];
        $end_date_range = $date[1];

        // Calculate Minus Maebarai Money
        $transactions = MaebaraiHistory::where('employee_id', $employee->id)
                                        ->where('transaction_status', '!=', MaebaraiHistory::TRANSACTION_API_FAIL)
                                        ->where('tran_date', '>=', $start_date_range)
                                        ->where('tran_date', '<=', $end_date_range)
                                        ->get();
        $transfer_amount = $transactions->sum(function($transaction) {
            if ($transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_SUCCESS) return $transaction->transfer_amount;
            else return 0;
        });
        $transfer_fee = $transactions->sum(function($transaction) {
            if ($transaction->transfer_fee == 54) {
                if ($transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_SUCCESS) return $transaction->transfer_fee;
                else return 0;
            } else {
                return $transaction->transfer_fee;
            }
        });
        $api_fee = $transactions->sum(function($transaction) {
            if ($transaction->transfer_fee == 54) {
                if ($transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_SUCCESS) return $transaction->api_usage_fee;
                else return 0;
            } else {
                return $transaction->api_usage_fee;
            }
        });
        // Calculate Working hour
        $working_hour = $this->getRealWorkingTime($employee, $start_date_range, $end_date_range);
        $month = $this->getMonthThatContainASpecificDay($employee);
        $date =$this->getStartDateAndEndDateFromBusinessMonth($employee, $month);
        $start_date_range = $date[0];
        $end_date_range = $date[1];
        if ($employee->workLocation->company->maebarai_auto_approve == 1) {
            $working_hour += $this->getRealWorkingTimeIncludeRequest($employee, $start_date_range, $end_date_range);
        }

        $maebarai_money = (int)($employee->maebarai_salary_per_hour * ($working_hour / 60) * $employee->workLocation->company->maebarai_payment_rate / 100);

        $maebarai_money = $maebarai_money - $transfer_amount - $transfer_fee - $api_fee;
        $maebarai_money = $maebarai_money > 0 ? $maebarai_money : 0;
        
        Javascript::put([
            'maebarai_money' => $maebarai_money,
        ]);

        return view('sinsei.maebarai.maebarai_page')->with([
            'maebarai_money'            =>  $maebarai_money,
        ]);
    }

    /** 
     * Show Maebarai History Page
     */
    public function history(Request $request)
    {
        $employee = session('sinsei_user');
        $employee->refresh();
        $conditions = $this->getDefaultConditions($request);
        if ($request->wantsJson()) {
            $conditions = $request->input('conditions');
        }
        $month = Carbon::create($conditions['year'], $conditions['month'], 1, 0, 0, 0);
        $date =$this->getStartDateAndEndDateFromBusinessMonth($employee, $month);
        $start_date_range = $date[0];
        $end_date_range = $date[1];

        // Calculate Minus Maebarai Money
        $transactions = MaebaraiHistory::where('employee_id', $employee->id)
                                        ->where('transaction_status', '!=', MaebaraiHistory::TRANSACTION_API_FAIL)
                                        ->where('tran_date', '>=', $start_date_range)
                                        ->where('tran_date', '<=', $end_date_range)
                                        ->orderBy('tran_date')->get();
        $transactions = $transactions->transform(function($transaction, $key) {
            $datas = explode(' ', $transaction->tran_date);
            $times = explode(':', $datas[1]);
            $time = implode(':', array($times[0], $times[1]));
            $error_datas = $error_time = null;
            if ($transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_FAIL) {
                $error_datas = explode(' ', $transaction->error_time);
                $times = explode(':', $error_datas[1]);
                $error_time = implode(':', array($times[0], $times[1]));
            }
            return [
                'date'              =>  $datas[0],
                'time'              =>  $time,
                'payment_format'    =>  Constants::payment_status()[$transaction->payment_format],
                'maebarai_amount'   =>  $transaction->maebarai_amount,
                'transfer_amount'   =>  $transaction->transfer_amount,
                'transfer_fee'      =>  $transaction->transfer_fee,
                'api_usage_fee'     =>  $transaction->api_usage_fee,
                'status'            =>  $transaction->transaction_status,
                'error_text'        =>  Constants::payment_errors()[$transaction->error_code],
                'error_date'        =>  $transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_FAIL ? $error_datas[0] : '',
                'error_time'        =>  $transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_FAIL ? $error_time : '',
            ];
        });
        if ($request->wantsJson()) {
            return [
                'datas' => $transactions,
                'conditions' => $conditions,
            ];
        }

        Javascript::put([
            'datas' => $transactions,
            'conditions' => $conditions,
        ]);
        return view('sinsei.maebarai.maebarai_history_page');
    }

    /** 
     * Show Maebarai Decision Page
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function maebarai_decision(Request $request)
    {
        $validator = $request->validate(
            [
            'can_pay_money' => 'required',
            'maebarai_pay_money' => 'required | integer | min: 0',
            ]
        );
        $employee = session('sinsei_user');
        $employee->refresh();

        if ($request->input('maebarai_pay_money') > $request->input('can_pay_money')) {
            $request->session()->flash('error', '前払可能金額をオーバーしています');
            return back();
        }
        if ($employee->maebarai_bank_code === MaebaraiHistory::SEVEN_BANK_CODE) {
            if ($request->input('maebarai_pay_money') < MaebaraiHistory::API_FEE * 1.08 + MaebaraiHistory::SEVEN_BANK_FEE * 1.08) {
                $request->session()->flash('error', '162円以上を入力してください');
                return back();
            }
        } else {
            if ($request->input('maebarai_pay_money') < MaebaraiHistory::API_FEE * 1.08 + MaebaraiHistory::OTHER_BANK_FEE * 1.08) {
                $request->session()->flash('error', '324円以上を入力してください');
                return back();
            }
        }

        $maebarai_pay_money = $request->input('maebarai_pay_money');
        $transaction_fee = ($employee->maebarai_bank_code === MaebaraiHistory::SEVEN_BANK_CODE) ? MaebaraiHistory::SEVEN_BANK_FEE * 1.08 : MaebaraiHistory::OTHER_BANK_FEE * 1.08;
        
        return view('sinsei.maebarai.maebarai_decision_page')->with([
            'maebarai_pay_money' => $maebarai_pay_money,
            "real_pay_money"    => $maebarai_pay_money - $transaction_fee - MaebaraiHistory::API_FEE * 1.08,
            'transaction_fee'   => $transaction_fee,
            'api_fee'           => MaebaraiHistory::API_FEE * 1.08,
        ]);
    }

    /** 
     * Show Maebarai Complete Page
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function complete(Request $request)
    {
        $employee = session('sinsei_user');
        $employee->refresh();

        // Bank Info
        $sender = array(
            'bankCode'      => '0034',
            'bankName'      => 'ｾﾌﾞﾝ',
            'branchCode'    => $employee->workLocation->company->maebarai_branch_code,
            'branchName'    => 'ｾﾌﾞﾝ',
            'accountType'   => '1',
            'accountNumber' => $employee->workLocation->company->maebarai_account_number,
            'accountName'   => $employee->workLocation->company->maebarai_account_name
        );

        // Employee Info
        $receiver = array(
            'bankCode'      => $employee->maebarai_bank_code,
            'bankName'      => $employee->maebarai_bank_name,
            'branchCode'    => $employee->maebarai_branch_code,
            'branchName'    => $employee->maebarai_branch_name,
            'accountType'   => '1',
            'accountNumber' => $employee->maebarai_account_number,
            'accountName'   => $employee->maebarai_account_name
        );
        $count = strval(MaebaraiHistory::count() + 1);
        $transactionId = '';
        
        for($i = strlen($count); $i < 10; $i++)
        {
            $transactionId .= '0';
        }
        $transactionId .= $count;
        $price = (int)($request->input('maebarai_pay_money'));
        $body = array(
            'transactionId'     => $transactionId,
            'sender'            => $sender,
            'receiver'          => $receiver,
            'transactionType'   => '21',
            'price'             => strval($price),
            'callId'            => env('PROJECT_ID', 'CAERU'),
            'companyCode'       => $employee->workLocation->company->code,
            'redirectUrl'       => env('APP_URL', 'http://caeru.biz') . '/updateTransactionResponse',
        );

        $client = new Client();

        $url = "http://172.31.21.45/api/send-money";
        $response = $client->post($url,  ['json'=>$body]);
        $response = $response->getBody()->getContents();
        $response = json_decode($response);
        $transaction_status = MaebaraiHistory::TRANSACTION_STATUS_SUCCESS;
        $error_code = $response->data->messageId;
        $transaction_fee = ($employee->maebarai_bank_code === MaebaraiHistory::SEVEN_BANK_CODE) ? MaebaraiHistory::SEVEN_BANK_FEE * 1.08 : MaebaraiHistory::OTHER_BANK_FEE * 1.08;
        $transfer_amount = $price - $transaction_fee - MaebaraiHistory::API_FEE * 1.08;
        
        $maebarai_history = new MaebaraiHistory();

        if ($response->data->statusCode != 200) {
            $transaction_status = MaebaraiHistory::TRANSACTION_API_FAIL;
            $maebarai_history->error_time = Carbon::now();        
        } else {
            if ($error_code != "00000000") {
                $transaction_status = MaebaraiHistory::TRANSACTION_API_FAIL;
                $maebarai_history->error_time = Carbon::now();
            }
        }
        
        $maebarai_history->employee_id = $employee->id;
        $maebarai_history->transaction_id = $transactionId;
        $maebarai_history->payment_format = 1;
        $maebarai_history->maebarai_amount = $price;
        $maebarai_history->transfer_amount = $transfer_amount;
        $maebarai_history->transfer_fee = $transaction_fee;
        $maebarai_history->api_usage_fee = MaebaraiHistory::API_FEE * 1.08;
        $maebarai_history->transaction_status = $transaction_status;
        $maebarai_history->tran_date = Carbon::now();
        $maebarai_history->error_code = $error_code;
        $maebarai_history->save();

        $this->maebaraiTransactionResponseSave($response->data, $maebarai_history->id, $error_code);
        $title = $text = '';
        if ($error_code == '00000000') {
            $title = '処理完了';
            $text = '処理を受け付けました。エラーにより入金されない場合があります。数日たっても入金が無い場合は、前払履歴でエラーの表示がないか確認ください。';
        } else if ($error_code == '880101S3001') {
            $title = '残高不足';
            $text = '支払い口座の残高が不足しています。管理者にお問い合わせください。';
        } else if ($error_code == '880101S5003' || $error_code == '880101S5015' || $error_code == '880101S5033' || $error_code == '880101S6002') {
            $title = '口座情報に不備があります';
            $text = '口座情報が正しいかご確認ください。';
        } else if ($error_code == '880101S2002') {
            $title = '振込限度額を超えています';
            $text = '金額を再設定のうえ支払申請を行なってください。';
        } else if ($error_code == '880101S6006' || $error_code == '880101S7009' || $error_code == '880101S8003' || $error_code == '880101S8005') {
            $title = 'メンテナンス中';
            $text = 'メンテナンス終了までお待ちください。';
        } else if ($error_code == 'AZ_C_CO_005001') {
            $title = '入力値エラー';
            $text = '管理者にお問い合わせください。';
        } else {
            $title = '北大湯';
            $text = '管理者にお問い合わせください。';
        }
        
        return view('sinsei.maebarai.maebarai_complete_page')->with([
            'title' => $title,
            'text'  => $text,
        ]);
    }

    /**
     * Get the default conditions for the search box in pre pay information page.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getDefaultConditions(Request $request)
    {
        $employee = session('sinsei_user');
        $employee->refresh();
        $month = $this->getMonthThatContainASpecificDay($employee);
        return [
            'year' => $month->year,
            'month' => $month->month,
        ];
    }

    /**
     * Save Maebarai Transaction Response
     */
    protected function maebaraiTransactionResponseSave($datas, $id, $error_code) {
        $maebarai_transaction_response = new MaebaraiTransactionResponse();
        $maebarai_transaction_response->maebarai_history_id = $id;
        $maebarai_transaction_response->status_code = $datas->statusCode;
        $maebarai_transaction_response->message = $datas->message;
        $maebarai_transaction_response->message_id = $datas->messageId;
        $data = $datas->data;
        if ($error_code == "00000000") {
            $maebarai_transaction_response->tran_date = $data->tranDate;
            $maebarai_transaction_response->tran_time = $data->tranTime;
            $maebarai_transaction_response->receipt_id = $data->receiptId;
            $maebarai_transaction_response->tran_id = $data->tranId;
            $maebarai_transaction_response->transfer_date = $data->transferDate;
            $maebarai_transaction_response->resrv_appt = $data->resrvAppt;
            $maebarai_transaction_response->c_bank_code = $data->cBankCode;
            $maebarai_transaction_response->c_bank_name = $data->cBankName;
            $maebarai_transaction_response->c_branch_code = $data->cBranchCode;
            $maebarai_transaction_response->c_branch_name = $data->cBranchName;
            $maebarai_transaction_response->c_account_type = $data->cAccountType;
            $maebarai_transaction_response->c_account_number = $data->cAccountNumber;
            $maebarai_transaction_response->c_account_name = $data->cAccountName;
            $maebarai_transaction_response->transfer_name = $data->transferName;
            $maebarai_transaction_response->r_bank_code = $data->rBankCode;
            $maebarai_transaction_response->r_bank_name = $data->rBankName;
            $maebarai_transaction_response->r_branch_code = $data->rBranchCode;
            $maebarai_transaction_response->r_branch_name = $data->rBranchName;
            $maebarai_transaction_response->r_account_type = $data->rAccountType;
            $maebarai_transaction_response->r_account_number = $data->rAccountNumber;
            $maebarai_transaction_response->r_account_name = $data->rAccountName;
            $maebarai_transaction_response->amount = $data->amount;
            $maebarai_transaction_response->transfer_type = $data->transferType;
            $maebarai_transaction_response->partner_code = $data->partnerCode;
        }
        $maebarai_transaction_response->save();
    }
}

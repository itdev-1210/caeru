<?php

namespace App\Http\Controllers\Sinsei\Requester;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\Sinsei\Requester\UpdatePasswordRequest;
use App\Http\Requests\Sinsei\Requester\UpdateEmailRequest;
use Caeru;

class AccountSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('sinsei_auth');
    }

    /**
     * Show form account setting
     *
     * @param Illuminate\Http\Request      $request
     * @return \Illuminate\Http\Response
     */
    public function showAccountSettingForm(Request $request)
    {
        return view('sinsei.requester.account_setting');
    }

    /**
     * Update password's account in storage.
     *
     * @param App\Http\Requests\Sinsei\Requester\UpdatePasswordRequest      $request
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        session('sinsei_user')->update(['password' => $request->input('new_password')]);
        $request->session()->flash('success', '保存しました');
        return Caeru::redirect('ss_show_account_setting');
    }

    /**
     * Update email's account in storage.
     *
     * @param App\Http\Requests\Sinsei\Requester\UpdateEmailRequest      $request
     * @return \Illuminate\Http\Response
     */
    public function updateEmail(UpdateEmailRequest $request)
    {
        session('sinsei_user')->update(['email' => $request->input('new_email')]);
        $request->session()->flash('success', '保存しました');
        return Caeru::redirect('ss_show_account_setting');
    }
}
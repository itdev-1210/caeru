<?php

namespace App\Http\Controllers\Sinsei\Requester;

use App\Services\WorkLocationSettingService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Employee;
use Carbon\Carbon;
use Session;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
class WorkingMonthController extends Controller
{
    use BusinessMonthTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('sinsei_auth');
    }

    /**
     * Show working month
     *
     * @param Request      $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $business_month = null)
    {
        $employee = session('sinsei_user');
        $employee->refresh();

        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $setting = $work_location_setting_service->getCurrentSettingOfWorkLocationById($employee->work_location_id);
 
        if ($business_month == null || $business_month == 'reset') {
            $business_month = $this->getBusinessMonthThatContainASpecificDay($employee->workLocation->currentSetting());
        }
        else
        {
            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');
        }  
        $business_month_need = $business_month;
        session(['business_month_need' => $business_month_need]); 

        $date =$this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);
        $start_date = $date[0]->toDateString();
        $start_date_range = $date[0];
        $end_date = $date[1]->toDateString();
        $end_date_range = $date[1];

        $data = $this->getWorkingDataFromDateRange($employee, $start_date_range, $end_date_range);

        $latest_paid_holiday_info = $employee->paidHolidayInformations->sortBy('period_end')->last();

        $sumtotal = $employee->numberOfSubordinatesRequest($start_date, $end_date);

        Javascript::put([
            'business_month'  => $business_month->format('Y-m'),
            'employee_id' => $employee->id,
            'toggle_display' => Session::has('display_toggle_sensei') ? session('display_toggle_sensei') : $setting->employee_working_month_pages_display_toggle,
        ]);
       
        return view('sinsei.requester.working_month', [
            'business_month' => $business_month->format('Y-m'),
            'sumtotal' => $sumtotal,
            'summarized_data' =>$data["summarized_data"],
            'working_days' => $data['working_days'],
            'work_locations' => $this->prepareTheWorkPlacesData($request),
            'real_available_paid_holidays' => $latest_paid_holiday_info ? $latest_paid_holiday_info->available_paid_holidays : null,
            'real_available_paid_holidays_hour' => $latest_paid_holiday_info ? $latest_paid_holiday_info->available_paid_holidays_hour : null,
            'planned_available_paid_holidays' => $latest_paid_holiday_info ? $latest_paid_holiday_info->plannedAvailablePaidHolidays()['days'] : null,
            'planned_available_paid_holidays_hour' => $latest_paid_holiday_info ? $latest_paid_holiday_info->plannedAvailablePaidHolidays()['time'] : null,
            'toggle_display' => Session::has('display_toggle_sensei') ? session('display_toggle_sensei') : $setting->employee_working_month_pages_display_toggle,
        ]);
    }

    /**
     * Prepare an array of WorkLocations, each have a name and an array of WorkAddress,
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    protected function prepareTheWorkPlacesData($request)
    {   
        $presentation_id = session('sinsei_user')->presentation_id;
        $employee = Employee::where('presentation_id', $presentation_id)->first();
        $all_work_locations = $employee->workLocation->company->workLocations;

        return $all_work_locations->keyBy('id')->map(function($work_location) {
            return [
                'name' => $work_location->name,
                'addresses' => $work_location->workAddresses->pluck('name', 'id')->toArray(),
            ];
        })->toArray();
    }
}

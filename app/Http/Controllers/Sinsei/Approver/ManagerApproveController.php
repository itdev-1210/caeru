<?php

namespace App\Http\Controllers\Sinsei\Approver;

use App\Services\WorkLocationSettingService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Employee;
use Carbon\Carbon;
use App\EmployeeWorkingDay;
use NationalHolidays;
use Session;
use App\WorkingTimestamp;
use App\EmployeeWorkingInformationSnapshot;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use App\Http\Controllers\Reusables\UseTheEmployeeWorkingInfoVueComponentTrait;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;

class ManagerApproveController extends Controller
{
    use BusinessMonthTrait, UseTheEmployeeWorkingInfoVueComponentTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('sinsei_auth');
        $this->middleware('approve_auth');
    }

    /**
     * Show manager approve list
     *
     * @param Request      $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $business_month = null)
    {
        $sinsei_user = session('sinsei_user');

        if ($business_month) {
            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');
        } else {
            $business_month = $this->calculateTheBusinessMonth($sinsei_user);
        }

        $date =$this->getStartDateAndEndDateFromBusinessMonth($sinsei_user, $business_month);
        $start_date = $date[0]->toDateString();
        $end_date = $date[1]->toDateString();

        $subordinates = $sinsei_user->subordinates()->paginate(20);
            
        // Considering requests by date of all subordinates of the sinsei_user
        $employee_working_days = EmployeeWorkingDay::with('employeeWorkingInformationSnapshots')
                                        ->whereIn('employee_id', $subordinates->pluck('id')->toArray())
                                        ->where('date', '>=', $start_date)->where('date', '<=', $end_date)->get();

        // Count the considering requests and add that to the result object
        $subordinates->transform(function($subordinate) use ($employee_working_days) {

            $working_days_of_this_subordinate = $employee_working_days->filter(function($day) use ($subordinate) {
                return $day->employee_id === $subordinate->id;
            });
            $subordinate['number_of_request'] = $working_days_of_this_subordinate->sum(function($day) {
                $considering_snapshots = $day->employeeWorkingInformationSnapshots->filter(function($snapshot) {
                    return $snapshot->left_status === EmployeeWorkingInformationSnapshot::CONSIDERING;
                });
                return $considering_snapshots->count();
            });
            return $subordinate;
        });

        Javascript::put([
            'business_month'  => $business_month->format('Y-m'),
        ]);

        return view('sinsei.approver.manager_list',['business_month' => $business_month->format('Y-m'),
            'employees_approval' =>$subordinates]);
    }

    /**
     * Show manager approve detail
     *
     * @param Request      $request
     * @return \Illuminate\Http\Response
     */
    public function showDetail(Request $request, $employee_id, $business_month = null)
    {
        $employee = Employee::where('id', $employee_id)->first();

        if ($business_month) {
            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');
           
        } else {
            $business_month = $this->calculateTheBusinessMonth($employee);
        }

        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $setting = $work_location_setting_service->getCurrentSettingOfWorkLocationById($employee->work_location_id);

        $date =$this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);
        $start_date = $date[0]->toDateString();
        $start_date_range = $date[0];
        $end_date = $date[1]->toDateString();
        $end_date_range = $date[1];

        $data = $this->getWorkingDataFromDateRange($employee, $start_date_range, $end_date_range);

        $latest_paid_holiday_info = $employee->paidHolidayInformations->sortBy('period_end')->last();

        $total = $employee->numberOfRequests($start_date, $end_date);

        Javascript::put([
            'business_month'  => $business_month->format('Y-m'),
            'employee_id' => $employee->id,
            'display_toggle' => Session::has('display_toggle_sensei') ? session('display_toggle_sensei') : $setting->employee_working_month_pages_display_toggle
        ]);

        return view('sinsei.approver.manager_detail', [
            'business_month' => $business_month->format('Y-m'),
            'employees_approval' =>$employee, 'total' => $total,
            'summarized_data' =>$data["summarized_data"],
            'working_days' => $data['working_days'],
            'work_locations' => $this->prepareTheWorkPlacesData($request),
            'real_available_paid_holidays' => $latest_paid_holiday_info ? $latest_paid_holiday_info->available_paid_holidays : null,
            'real_available_paid_holidays_hour' => $latest_paid_holiday_info ? $latest_paid_holiday_info->available_paid_holidays_hour : null,
            'planned_available_paid_holidays' => $latest_paid_holiday_info ? $latest_paid_holiday_info->plannedAvailablePaidHolidays()['days'] : null,
            'planned_available_paid_holidays_hour' => $latest_paid_holiday_info ? $latest_paid_holiday_info->plannedAvailablePaidHolidays()['time'] : null,
            'display_toggle' => Session::has('display_toggle_sensei') ? session('display_toggle_sensei') : $setting->employee_working_month_pages_display_toggle
        ]);
    }


     /**
     * Prepare an array of WorkLocations, each have a name and an array of WorkAddress,
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    protected function prepareTheWorkPlacesData($request)
    {   
        $presentation_id = session('sinsei_user')->presentation_id;
        $employee = Employee::where('presentation_id', $presentation_id)->first();
        $all_work_locations = $employee->workLocation->company->workLocations;

        return $all_work_locations->keyBy('id')->map(function($work_location) {
            return [
                'name' => $work_location->name,
                'addresses' => $work_location->workAddresses->pluck('name', 'id')->toArray(),
            ];
        })->toArray();
    }

}

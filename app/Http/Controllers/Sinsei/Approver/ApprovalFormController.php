<?php

namespace App\Http\Controllers\Sinsei\Approver;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Employee;
use Carbon\Carbon;
use App\EmployeeWorkingDay;
use App\Http\Controllers\Reusables\UseTheEmployeeWorkingInfoVueComponentTrait;
use NationalHolidays;
use App\EmployeeWorkingInformationSnapshot;
use App\WorkingTimestamp;
use App\ColorStatus;
use App\WorkStatus;
use App\RestStatus;
use App\WorkAddress;
use App\EmployeeWorkingInformation;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Http\Controllers\Reusables\ManageWorkingTimestampsTrait;
use App\SubstituteEmployeeWorkingInformation;
use App\Http\Controllers\Reusables\TransferEmployeeWorkingDayTrait;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;
use App\Http\Controllers\Reusables\UseTheSnapshotVueComponentTrait;
use App\Events\AbsorbTheMatchingTemporaryWorkingInformation;
use App\Events\CachedPaidHolidayInformationBecomeOld;
use App\Setting;
use DB;
use App\Events\WorkingTimestampChanged;
use App\Services\TimezonesService;
use App\Services\WorkLocationSettingService;

class ApprovalFormController extends Controller
{
    use UseTheEmployeeWorkingInfoVueComponentTrait, ManageWorkingTimestampsTrait, TransferEmployeeWorkingDayTrait,
        BusinessMonthTrait, UseTheSnapshotVueComponentTrait, CanCreateWorkingDayOnTheFlyTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('sinsei_auth');
        $this->middleware('approve_auth:allow_manager')->only(['acceptApprovalForm', 'declineApprovalForm']);
        $this->middleware('approve_auth')->except(['acceptApprovalForm', 'declineApprovalForm']);
    }

    /**
     * Show approval form
     *  $list_date will substring with charter '|' return array of string
     *  Call function checkDateList for check furikae's case
     *  Finally, get all snapshot with employee working day and some data of work location
     *
     * @param String    $list_date
     * @param App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function showApprovalForm(Employee $employee, $list_date)
    {
        $snapshot_days = collect([]);
        $array_date = explode("|", str_replace(' ', '', $list_date));
        $array_date = $this->checkDateList($array_date, $employee);
        if (!$array_date) abort(404, 'Can not find snapshot information for this employee on that day!');
        foreach ($array_date as $date) {
            $snapshot_days->push($this->sendDataFollowWorkingDay($date, $employee));
        }
        $this->getAllWorkLocation();
        Javascript::put([
            'snapshot_days' => $snapshot_days,
            'employee_id' => $employee->id,
        ]);
        return view('sinsei.approver.approval_form');
    }

    /**
     * Show approval form with month
     *  First, will get $business_month
     *  Second, will get employee working day follow business month
     *  Finally, get only snapshot which is questing with employee working day and some data of work location
     *
     * @param String    $business_month
     * @param App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function showApprovalFormMonth(Employee $employee, $business_month)
    {
        $snapshot_days = collect([]);
        $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');
        $date =$this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);
        $array_date = EmployeeWorkingDay::where('employee_id', $employee->id)->whereBetween('date', [$date[0]->toDateString(), $date[1]->toDateString()])->pluck('date');
        $array_date = $this->checkDateList($array_date->all(), $employee);
        if (count($array_date) != 0) {
            $employee_working_days = EmployeeWorkingDay::with('employeeWorkingInformationSnapshots')->where('employee_id', $employee->id)->whereIn('date', $array_date)->get();

            $sort_employee_working_days = collect([]);
            foreach ($array_date as $key => $value) {
                $sort_employee_working_days->push($employee_working_days->where('date', $value)->first());
            }

            $snapshot_days = $this->sendDataFollowWorkingDayOnlySinseiChu($sort_employee_working_days, $employee);

            $this->getAllWorkLocation();
        } else {
            Javascript::put([
                'work_locations' => [],
                'rest_days' => [],
                'national_holidays' => [],
                'flip_color_day' => 0,
                'timestamp_types' => null,
            ]);
        }

        Javascript::put([
            'snapshot_days' => $snapshot_days,
            'employee_id' => $employee->id,
        ]);
        return view('sinsei.approver.approval_form');
    }

    /**
     * The function will update the model snapshot with approver_note and call acceptForFuricaeCase or call acceptForNormalCase
     *
     * @param Illuminate\Http\Request   $request
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @return array
     */
    public function acceptApprovalForm(Request $request, EmployeeWorkingInformationSnapshot $snapshot)
    {
        $register_info = [];
        if ($request->isEmployeeApproval)
            $register_info = [
                "registerer_id" => session('sinsei_user')->id,
                "registerer_type" => WorkingTimestamp::EMPLOYEE,
            ];
        else
            $register_info = [
                "registerer_id" => $request->user()->id,
                "registerer_type" => WorkingTimestamp::MANAGER,
            ];

        $snapshot->update(["left_approver_note" => $request->approver_note]);
        $left_rest_status = RestStatus::find($snapshot->left_rest_status_id);
        $employee_working_information = $snapshot->employeeWorkingInformation;
        $planned_rest_status = (isset($employee_working_information)) ? RestStatus::find($employee_working_information->planned_rest_status_id) : null;
        $color_status_considering = $snapshot->colorStatuses()->where('field_css_class', ColorStatus::CONSIDERING_COLOR)->get();

        if ($color_status_considering->contains('field_name', 'left_switch_planned_schedule_target'))

            return $this->acceptForFuricaeCase($snapshot, $register_info);

        else if (($color_status_considering->contains('field_name', 'left_work_status_id')
            || $color_status_considering->contains('field_name', 'left_rest_status_id'))
            && (($snapshot->left_work_status_id == WorkStatus::KEKKIN || (isset($left_rest_status) && $left_rest_status->unit_type))
            || ((isset($employee_working_information) && $employee_working_information->planned_work_status_id == WorkStatus::KEKKIN)
            || (isset($planned_rest_status) && $planned_rest_status->unit_type))))

            return $this->acceptForDayOffCase($snapshot, $register_info);

        else if (isset($snapshot->target_absorb_EWI_id))
            return $this->acceptForAbsorbCase($snapshot, $register_info);

        else {
            $this->acceptForNormalCase($snapshot, $register_info);

            return [
                'success'               => '保存しました',
                'snapshot'              => $snapshot->employeeWorkingInformation()->first()->mergeWorkingInformationToSnapshot(),
                'color_status'          => $snapshot->colorStatuses()->get(),
            ];
        }
    }

    /**
     * The function will update the model snapshot with approver_note and and call declineForFuricaeCase or call declineForNormalCase
     *
     * @param Illuminate\Http\Request      $request
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @return array
     */
    public function declineApprovalForm(Request $request, EmployeeWorkingInformationSnapshot $snapshot)
    {
        $register_info = [];
        if ($request->isEmployeeApproval)
            $register_info = [
                "registerer_id" => session('sinsei_user')->id,
                "registerer_type" => EmployeeWorkingInformation::MODIFY_PERSON_TYPE_EMPLOYEE,
            ];
        else
            $register_info = [
                "registerer_id" => $request->user()->id,
                "registerer_type" => EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER,
            ];

        $snapshot->update(["left_approver_note" => $request->approver_note]);
        $left_rest_status = RestStatus::find($snapshot->left_rest_status_id);
        $employee_working_information = $snapshot->employeeWorkingInformation;
        $employee_working_day = $snapshot->employeeWorkingDay;
        $planned_rest_status = (isset($employee_working_information)) ? RestStatus::find($employee_working_information->planned_rest_status_id) : null;
        $color_status_considering = $snapshot->colorStatuses()->where('field_css_class', ColorStatus::CONSIDERING_COLOR)->get();

        // This flow is to delete the snapshot in the case of '「休出」申請ー＞承認・否決しなくてー＞打刻するー＞その後、否決する'
        if (!$employee_working_information && $snapshot->soon_to_be_EWI_id == null && $employee_working_day->employeeWorkingInformations->isNotEmpty()) {
            $substitute_employee_working_information = SubstituteEmployeeWorkingInformation::where('employee_working_day_id', $employee_working_day->id)->first();
            $substitute_employee_working_information->colorStatuses()->delete();
            $substitute_employee_working_information->delete();
            $snapshot->colorStatuses()->delete();
            $snapshot->delete();

            return [
                'success'               => '保存しました',
                // 'snapshot'              => ($snapshot->employeeWorkingInformation) ?
                //                                 $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot() :
                //                                 $snapshot->employeeWorkingDay->mergeSnapshotsAndEmployeeWorkingInformations()->first(),
                // 'color_status'          => $snapshot->colorStatuses()->get(),
                ];
        }

        if ($color_status_considering->contains('field_name', 'left_switch_planned_schedule_target')){

            return $this->declineForFuricaeCase($snapshot, $register_info);

        } else if (($color_status_considering->contains('field_name', 'left_work_status_id')
            || $color_status_considering->contains('field_name', 'left_rest_status_id'))
            && (($snapshot->left_work_status_id == WorkStatus::KEKKIN || (isset($left_rest_status) && $left_rest_status->unit_type))
            || ((isset($employee_working_information) && $employee_working_information->planned_work_status_id == WorkStatus::KEKKIN)
            || (isset($planned_rest_status) && $planned_rest_status->unit_type)))) {

            return $this->declineForDayOffCase($snapshot, $register_info);

        } else if (isset($snapshot->target_absorb_EWI_id))
            return $this->declineForAbsorbCase($snapshot, $register_info);

        else {
            $this->declineForNormalCase($snapshot, $register_info);

            return [
                'success'               => '保存しました',
                'snapshot'              => ($snapshot->employeeWorkingInformation) ?
                                                $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot() :
                                                $snapshot->employeeWorkingDay->mergeSnapshotsAndEmployeeWorkingInformations()->first(),
                'color_status'          => $snapshot->colorStatuses()->get(),
            ];
        }
    }

    /**
     * The function will update the snapshot to employee working information and sometime working timestamp
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @param Array   $register_info
     * @return void
     */
    protected function acceptForNormalCase($snapshot, $register_info)
    {
        $color_statuses = $snapshot->colorStatuses()->get();
        $check_working_timestamp_start_work_time_updated = $check_working_timestamp_end_work_time_updated = false;
        $check_start_work_time = empty($snapshot->employee_working_information_id)
            ? true : empty($snapshot->employeeWorkingInformation->timestamped_start_work_time_working_timestamp_id);
        $check_end_work_time = $color_statuses->contains('field_name', 'left_timestamped_end_work_time_work_location_id');
        foreach ($color_statuses as $color) {

            if (!$check_working_timestamp_start_work_time_updated) {
                if (!$check_start_work_time && ($color->field_name === 'left_timestamped_start_work_date' || $color->field_name === 'left_timestamped_start_work_time')) {
                    $working_timestamp = WorkingTimestamp::find($snapshot->employeeWorkingInformation->timestamped_start_work_time_working_timestamp_id);
                    $working_timestamp->enable = false;
                    $working_timestamp->save();
                    $check_working_timestamp_start_work_time_updated = true;
                }
            }

            if (!$check_working_timestamp_end_work_time_updated) {
                if (!$check_end_work_time && ($color->field_name === 'left_timestamped_end_work_date' || $color->field_name === 'left_timestamped_end_work_time')) {
                    $working_timestamp = WorkingTimestamp::find($snapshot->employeeWorkingInformation->timestamped_end_work_time_working_timestamp_id);
                    $working_timestamp->enable = false;
                    $working_timestamp->save();
                    $check_working_timestamp_end_work_time_updated = true;
                }
            }

            $color->field_css_class = ColorStatus::APPROVED_COLOR;
            $color->save();
        }

        $employee_working_information = $timestamps = null;

        if ($snapshot->employee_working_information_id) {
            $transferSnapshotColorAttributeToEmployeeWorkingInformationColor = $this->transferSnapshotColorAttributeToEmployeeWorkingInformationColor($snapshot, $snapshot->colorStatuses()->get(), $register_info);
            $timestamps = $transferSnapshotColorAttributeToEmployeeWorkingInformationColor['timestamps'];

            $employee_working_information = $snapshot->employeeWorkingInformation;

            $employee_working_information->fill($transferSnapshotColorAttributeToEmployeeWorkingInformationColor['array_after_transfer_key']);

            if($employee_working_information->temporary == true)
                $employee_working_information->temporary = false;

            if(isset($snapshot->target_absorb_EWI_id))
                event(new AbsorbTheMatchingTemporaryWorkingInformation($employee_working_information));

            $employee_working_information->save();

        } else {
            $transferSnapshotColorAttributeToEmployeeWorkingInformationColor =
                $this->transferSnapshotColorAttributeToEmployeeWorkingInformationColor(
                    $snapshot,
                    $snapshot->colorStatuses()->get(),
                    $register_info,
                    [
                        "employee_working_day_id" => $snapshot->employee_working_day_id,
                        "manually_modified" => true, "temporary" => false
                    ]
                );
            $timestamps = $transferSnapshotColorAttributeToEmployeeWorkingInformationColor['timestamps'];
            $employee_working_information = EmployeeWorkingInformation::create($transferSnapshotColorAttributeToEmployeeWorkingInformationColor['array_after_transfer_key']);
        }

        if (count($timestamps) != 0) {
            if (isset($snapshot->target_absorb_EWI_id)) {
                $start_work = WorkingTimestamp::find($employee_working_information->timestamped_start_work_time_working_timestamp_id);
                $end_work = WorkingTimestamp::find($employee_working_information->timestamped_end_work_time_working_timestamp_id);

                if (isset($start_work) && $start_work->enable == true) {

                    $start_work_key = array_search(WorkingTimestamp::START_WORK, array_column($timestamps, 'timestamped_type'));

                    if ($start_work_key !== false) {
                        $start_work->enable = false;
                        $start_work->save();
                    }
                }

                if (isset($end_work) && $end_work->enable == true) {

                    $end_work_key = array_search(WorkingTimestamp::END_WORK, array_column($timestamps, 'timestamped_type'));

                    if ($end_work_key !== false) {
                        $end_work->enable = false;
                        $end_work->save();
                    }
                }

                if (count($timestamps) != 0)
                    $this->addNewWorkingTimestampsForWorkingDay($timestamps, $snapshot->employeeWorkingDay);
                else
                    event(new WorkingTimestampChanged($snapshot->employeeWorkingDay));

            } else {
                $this->addNewWorkingTimestampsForWorkingDay($timestamps, $snapshot->employeeWorkingDay);
            }
        } else {
            event(new WorkingTimestampChanged($snapshot->employeeWorkingDay));
        }

        if (!$snapshot->employee_working_information_id) {
            $snapshot->employee_working_information_id = $employee_working_information->id;
            $substitute_employee_working_information = SubstituteEmployeeWorkingInformation::where('employee_working_day_id', $snapshot->employee_working_day_id)->first();
            foreach ($substitute_employee_working_information->colorStatuses()->get() as $color) {
                $color->colorable_type = ColorStatus::EMPLOYEE_WORKING_INFORMATION;
                $color->colorable_id = $employee_working_information->id;
                $color->field_fake_value = null;
                $color->field_css_class = ColorStatus::APPROVED_COLOR;
                $color->save();
            }
            $substitute_employee_working_information->delete();
        } else {
            foreach ($snapshot->employeeWorkingInformation->colorStatuses()->get() as $color) {
                if ($color->field_css_class == ColorStatus::CONSIDERING_COLOR) {
                    $new_snapshot_color = new ColorStatus;
                    $new_snapshot_color->colorable_type = $color->colorable_type;
                    $new_snapshot_color->colorable_id = $color->colorable_id;
                    $new_snapshot_color->field_name = $color->field_name;
                    $new_snapshot_color->field_css_class = ColorStatus::APPROVED_COLOR;
                    $new_snapshot_color->field_fake_value = null;
                    $new_snapshot_color->save();
                    $color->delete();
                }
            }
        }
        $snapshot->left_approval_time = Carbon::now(Employee::find($snapshot->employeeWorkingDay->employee_id)->workLocation->getTimezone()->name_id)->toDateTimeString();
        $snapshot->left_status = EmployeeWorkingInformationSnapshot::APPROVED;
        if (isset($snapshot->target_absorb_EWI_id)) $snapshot->target_absorb_EWI_id = null;
        $snapshot->save();
    }

    /**
     * The function will update the model snapshot with employee working day
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @param Array   $register_info
     * @return array
     */
    protected function acceptForFuricaeCase($snapshot, $register_info)
    {
        $employee = Employee::find($snapshot->employeeWorkingDay->employee_id);
        if ($snapshot->left_work_status_id == WorkStatus::FURIKYUU) {
            $from_employee_working_day = EmployeeWorkingDay::find($snapshot->employee_working_day_id);
            $to_employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $snapshot->left_switch_planned_schedule_target)->first();
        }else {
            $from_employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $snapshot->left_switch_planned_schedule_target)->first();
            $to_employee_working_day = EmployeeWorkingDay::find($snapshot->employee_working_day_id);
        }
        $this->transferWorkingDay(
            $employee->id,
            $from_employee_working_day->date,
            $to_employee_working_day->date,
            $employee
        );
        $this->updateSnapshotForFurikyu($from_employee_working_day, $register_info);
        $this->updateSnapshotForFuride($to_employee_working_day, $register_info);

        return [
            'success'               => '保存しました',
            'snapshotFrom'          => $this->sendDataFollowWorkingDay($from_employee_working_day->date, $employee),
            'snapshotTo'            => $this->sendDataFollowWorkingDay($to_employee_working_day->date, $employee),
            'isFurikae'             => true,
        ];
    }

    /**
     * The function will update all of snapshot in employee working day when requesting day off
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @param Array   $register_info
     * @return array
     */
    protected function acceptForDayOffCase($snapshot, $register_info)
    {
        $employee_working_day = $snapshot->employeeWorkingDay;
        $employee = $employee_working_day->employee;
        $check_rest_status = false;
        $snapshots = $employee_working_day->employeeWorkingInformationSnapshots->load("employeeWorkingInformation");

        foreach ($snapshots->sortByDesc('employeeWorkingInformation.temporary') as $employee_working_information_snapshot) {
            if (isset($employee_working_information_snapshot->employeeWorkingInformation)) {
                if (!$check_rest_status) {
                    if ($employee_working_information_snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING
                        && $employee_working_information_snapshot->left_rest_status_id != $employee_working_information_snapshot->employeeWorkingInformation->planned_rest_status_id) {
                            $check_rest_status = true;
                    }
                }
                $this->acceptForNormalCase($employee_working_information_snapshot, $register_info);
            }
        }

        if ($check_rest_status) event(new CachedPaidHolidayInformationBecomeOld($employee_working_day));

        return [
            'success'               => '保存しました',
            'snapshotDay'          => $this->sendDataFollowWorkingDay($employee_working_day->date, $employee),
            'isDayOff'             => true,
        ];
    }

    /**
     * The function will update the snapshot when requesting absorb, and remove soon to be absorb snapshot
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @param Array   $register_info
     * @return App\EmployeeWorkingInformationSnapshot    $snapshot
     */
    protected function acceptForAbsorbCase($snapshot, $register_info)
    {
        $this->acceptForNormalCase($snapshot, $register_info);

        return [
            'success'               => '保存しました',
            'snapshot'              => $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot(),
            'color_status'          => $snapshot->colorStatuses()->get(),
        ];
    }

    /**
     * The function will change the left of snapshot to the right of snapshot.
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @param Array    $register_info
     * @return void
     */
    protected function declineForNormalCase($snapshot, $register_info)
    {
        $array_transfer_key = [
            'left_work_status_id',
            'left_rest_status_id',
            'left_paid_rest_time_start',
            'left_paid_rest_time_end',
            'left_paid_rest_time_period',
            'left_planned_work_location_id',
            'left_planned_early_arrive_start',
            'left_planned_early_arrive_end',
            'left_planned_overtime_start',
            'left_planned_overtime_end',
            'left_planned_break_time',
            'left_real_break_time',
            'left_planned_night_break_time',
            'left_real_night_break_time',
            'left_planned_late_time',
            'left_planned_early_leave_time',
            'left_planned_go_out_time',
        ];

        $check_update_last_modify = false;

        foreach ($snapshot->colorStatuses()->get() as $color) {
            if (!$check_update_last_modify) {
                if (in_array($color->field_name, $array_transfer_key))
                    $check_update_last_modify = true;
            }
            $color->field_css_class = ColorStatus::REJECTED_COLOR;
            $snapshot[str_replace("left", "right", $color->field_name)] = $snapshot[$color->field_name];
            $snapshot[$color->field_name] = null;
            $color->field_name = str_replace("left", "right", $color->field_name);
            $color->save();
        }
        if ($snapshot->employee_working_information_id) {
            $employee_working_information = EmployeeWorkingInformation::find($snapshot->employee_working_information_id);

            $color_statuses = $employee_working_information->colorStatuses()->get();

            if ($check_update_last_modify) {
                $employee_working_information->last_modify_person_type = $register_info["registerer_type"];
                $employee_working_information->last_modify_person_id = $register_info["registerer_id"];
                $employee_working_information->save();
            }

        } else
            $color_statuses = SubstituteEmployeeWorkingInformation::where('employee_working_day_id', $snapshot->employee_working_day_id)->first()->colorStatuses()->get();

        foreach ($color_statuses as $color) {
            if ($color->field_css_class == ColorStatus::CONSIDERING_COLOR) {
                $new_snapshot_color = new ColorStatus;
                $new_snapshot_color->colorable_type = $color->colorable_type;
                $new_snapshot_color->colorable_id = $color->colorable_id;
                $new_snapshot_color->field_name = $color->field_name;
                $new_snapshot_color->field_css_class = ColorStatus::REJECTED_COLOR;
                $new_snapshot_color->field_fake_value = null;
                $new_snapshot_color->save();
                $color->delete();
            }
        }
        $snapshot->left_status = EmployeeWorkingInformationSnapshot::CURRENT;
        $snapshot->right_status = EmployeeWorkingInformationSnapshot::DENIED;
        $snapshot->right_approver_note = $snapshot->left_approver_note;
        $snapshot->right_requester_modify_time = $snapshot->left_requester_modify_time;
        $snapshot->left_requester_note = $snapshot->left_approver_note = $snapshot->left_requester_modify_time = null;
        $snapshot->right_approval_time = Carbon::now(Employee::find($snapshot->employeeWorkingDay->employee_id)->workLocation->getTimezone()->name_id)->toDateTimeString();
        $snapshot->save();
    }

    /**
     * The function will remove the snapshot with employee working day
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @param Array    $register_info
     * @return array
     */
    protected function declineForFuricaeCase($snapshot, $register_info)
    {
        $employee = Employee::find($snapshot->employeeWorkingDay->employee_id);
        if ($snapshot->left_work_status_id == WorkStatus::FURIKYUU) {
            $from_employee_working_day = EmployeeWorkingDay::find($snapshot->employee_working_day_id);
            $to_employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $snapshot->left_switch_planned_schedule_target)->first();
        }else {
            $from_employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $snapshot->left_switch_planned_schedule_target)->first();
            $to_employee_working_day = EmployeeWorkingDay::find($snapshot->employee_working_day_id);
        }
        $this->deleteSnapshotForFurikyu($from_employee_working_day, $register_info);
        $this->deleteSnapshotForFuride($to_employee_working_day);

        return [
            'success'               => '保存しました',
            'snapshotFrom'          => $this->sendDataFollowWorkingDay($from_employee_working_day->date, $employee),
            'snapshotTo'            => $this->sendDataFollowWorkingDay($to_employee_working_day->date, $employee),
            'isFurikae'             => true,
        ];
    }

    /**
     * The function will remove all of snapshot in employee working day when requesting day off
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @param Array   $register_info
     * @return array
     */
    protected function declineForDayOffCase($snapshot, $register_info)
    {
        $employee_working_day = $snapshot->employeeWorkingDay;
        $employee = $employee_working_day->employee;

        foreach ($employee_working_day->employeeWorkingInformationSnapshots as $employee_working_information_snapshot) {
            $this->declineForNormalCase($employee_working_information_snapshot, $register_info);
            $employee_working_information_snapshot->target_absorb_EWI_id = null;
            if ($employee_working_information_snapshot->left_soon_to_be_absorb == true) {
                $employee_working_information_snapshot->left_soon_to_be_absorb = false;
                $employee_working_information_snapshot->right_soon_to_be_absorb = true;
            }
            $employee_working_information_snapshot->save();
        }

        return [
            'success'              => '保存しました',
            'snapshotDay'          => $this->sendDataFollowWorkingDay($employee_working_day->date, $employee),
            'isDayOff'             => true,
        ];
    }

    /**
     * The function will update two snapshot follow decline's case
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot
     * @param App\EmployeeWorkingInformationSnapshot    $register_info
     * @return array
     */
    protected function declineForAbsorbCase($snapshot, $register_info)
    {
        $target_absorb_EWI_id = EmployeeWorkingInformation::find($snapshot->target_absorb_EWI_id);

        if (isset($target_absorb_EWI_id)) {
            $soon_to_be_absorb_snapshot = $target_absorb_EWI_id->employeeWorkingInformationSnapshot;
            $this->declineForNormalCase($soon_to_be_absorb_snapshot, $register_info);
            if ($soon_to_be_absorb_snapshot->left_soon_to_be_absorb == true) $soon_to_be_absorb_snapshot->left_soon_to_be_absorb = false;
            if ($soon_to_be_absorb_snapshot->right_soon_to_be_absorb == false) $soon_to_be_absorb_snapshot->right_soon_to_be_absorb = true;
            $soon_to_be_absorb_snapshot->save();
        }

        $this->declineForNormalCase($snapshot, $register_info);
        $snapshot->target_absorb_EWI_id = null;
        $snapshot->save();

        return [
            'success'                                   => '保存しました',
            'isAbsorb'                                  => true,
            'snapshot'                                  => $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot(),
            'color_status'                              => $snapshot->colorStatuses()->get(),
            'soon_to_be_absorb_snapshot'                => $soon_to_be_absorb_snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot(),
            'color_status_soon_to_be_absorb_snapshot'   => $soon_to_be_absorb_snapshot->colorStatuses()->get(),
        ];
    }

    /**
     * The function will update the furikyu's snapshot in the furikae's case.
     *
     * @param App\EmployeeWorkingDay    $from_employee_working_day
     * @param Array    $register_info
     * @return void
     */
    protected function updateSnapshotForFurikyu($from_employee_working_day, $register_info)
    {
        foreach ($from_employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {

            $employee_working_information = $snapshot->employeeWorkingInformation;
            $transfer_with_EWI_id = EmployeeWorkingInformation::find($employee_working_information->transfer_with_EWI_id);

            $transfer_with_EWI_id->last_modify_person_id = $register_info["registerer_id"];
            if ($register_info["registerer_type"] == WorkingTimestamp::EMPLOYEE)
                $transfer_with_EWI_id->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_EMPLOYEE;
            else if ($register_info["registerer_type"] == WorkingTimestamp::MANAGER)
                $transfer_with_EWI_id->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
            $transfer_with_EWI_id->save();

            foreach ($employee_working_information->colorStatuses()->get() as $color) {
                if ($color->field_css_class == ColorStatus::CONSIDERING_COLOR) {
                    $new_snapshot_color = new ColorStatus;
                    $new_snapshot_color->colorable_type = $color->colorable_type;
                    $new_snapshot_color->colorable_id = $transfer_with_EWI_id->id;
                    $new_snapshot_color->field_name = $color->field_name;
                    $new_snapshot_color->field_css_class = ColorStatus::APPROVED_COLOR;
                    $new_snapshot_color->field_fake_value = null;
                    $new_snapshot_color->save();
                    $color->delete();
                }
            }
            $deleted_snapshot = EmployeeWorkingInformationSnapshot::onlyTrashed()
                                    ->where('employee_working_day_id', $from_employee_working_day->id)
                                    ->where('employee_working_information_id', $snapshot->employee_working_information_id)
                                    ->first();
            if ($deleted_snapshot) {
                $deleted_snapshot->forceDelete();
                $deleted_snapshot->colorStatuses()->delete();
                $deleted_snapshot->employeeWorkingInformation->colorStatuses()->delete();
            }

            $snapshot = $this->saveEWIToRightOfSnapshot($snapshot, $employee_working_information);

            $snapshot->employee_working_information_id = $transfer_with_EWI_id->id;
            $snapshot->left_status = EmployeeWorkingInformationSnapshot::APPROVED;
            $snapshot->left_approval_time = Carbon::now(Employee::find($from_employee_working_day->employee_id)->workLocation->getTimezone()->name_id)->toDateTimeString();
            $snapshot->save();
            foreach ($snapshot->colorStatuses()->get() as $color) {
                $color->field_css_class = ColorStatus::APPROVED_COLOR;
                $color->save();
            }
        }
    }

    /**
     * The function will update the furide's snapshot in the furikae's case.
     *
     * @param App\EmployeeWorkingDay    $to_employee_working_day
     * @return void
     */
    protected function updateSnapshotForFuride($to_employee_working_day, $register_info)
    {
        foreach ($to_employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {
            $employee_working_information = EmployeeWorkingInformation::find($snapshot->soon_to_be_EWI_id);
            $transferSnapshotColorAttributeToEmployeeWorkingInformationColor = $this->transferSnapshotColorAttributeToEmployeeWorkingInformationColor($snapshot, $snapshot->colorStatuses()->get(), $register_info);
            $timestamps = $transferSnapshotColorAttributeToEmployeeWorkingInformationColor['timestamps'];
            $employee_working_information->update($transferSnapshotColorAttributeToEmployeeWorkingInformationColor['array_after_transfer_key']);
            $snapshot->employee_working_information_id =  $employee_working_information->id;
            $snapshot->soon_to_be_EWI_id = null;
            $snapshot->left_status = EmployeeWorkingInformationSnapshot::APPROVED;
            $snapshot->left_approval_time = Carbon::now(Employee::find($to_employee_working_day->employee_id)->workLocation->getTimezone()->name_id)->toDateTimeString();
            $snapshot->save();
            if (count($timestamps) != 0)
                $this->addNewWorkingTimestampsForWorkingDay($timestamps, $snapshot->employeeWorkingDay);
            foreach ($snapshot->colorStatuses()->get() as $color) {
                $color->field_css_class = ColorStatus::APPROVED_COLOR;
                $color->save();
            }
            $substitute_employee_working_information = SubstituteEmployeeWorkingInformation::where('employee_working_day_id', $to_employee_working_day->id)->where('employee_working_information_id', $employee_working_information->id)->first();
            foreach ($substitute_employee_working_information->colorStatuses()->get() as $color) {
                $color->colorable_type = ColorStatus::EMPLOYEE_WORKING_INFORMATION;
                $color->colorable_id = $employee_working_information->id;
                $color->field_fake_value = null;
                $color->field_css_class = ColorStatus::APPROVED_COLOR;
                $color->save();
            }
            $substitute_employee_working_information->delete();
        }
    }

    /**
     * The function will remove the furikyu's snapshot in the furikae's case.
     *
     * @param App\EmployeeWorkingDay    $from_employee_working_day
     * @param Array    $register_info
     * @return void
     */
    protected function deleteSnapshotForFurikyu($from_employee_working_day, $register_info)
    {
        foreach ($from_employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {
            $employee_working_information = $snapshot->employeeWorkingInformation;
            $employee_working_information->last_modify_person_type = $register_info["registerer_type"];
            $employee_working_information->last_modify_person_id = $register_info["registerer_id"];
            $employee_working_information->save();

            $deleted_snapshot = EmployeeWorkingInformationSnapshot::onlyTrashed()
                                    ->where('employee_working_day_id', $from_employee_working_day->id)
                                    ->where('employee_working_information_id', $snapshot->employee_working_information_id)
                                    ->first();
            if ($deleted_snapshot) {
                $deleted_snapshot->forceDelete();
                ColorStatus::where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION_SNAPSHOT)
                            ->where('colorable_id', $deleted_snapshot->id)->disable()->delete();
                ColorStatus::where('colorable_type', ColorStatus::EMPLOYEE_WORKING_INFORMATION)
                            ->where('colorable_id', $deleted_snapshot->employee_working_information_id)->disable()->delete();
            }

            foreach ($employee_working_information->colorStatuses()->get() as $color) {
                if ($color->field_css_class == ColorStatus::CONSIDERING_COLOR) {
                    $new_snapshot_color = new ColorStatus;
                    $new_snapshot_color->colorable_type = $color->colorable_type;
                    $new_snapshot_color->colorable_id = $color->colorable_id;
                    $new_snapshot_color->field_name = $color->field_name;
                    $new_snapshot_color->field_css_class = ColorStatus::REJECTED_COLOR;
                    $new_snapshot_color->field_fake_value = null;
                    $new_snapshot_color->save();
                    $color->delete();
                }
            }

            foreach ($snapshot->colorStatuses()->get() as $color) {
                $color->field_css_class = ColorStatus::REJECTED_COLOR;
                $snapshot[str_replace("left", "right", $color->field_name)] = $snapshot[$color->field_name];
                $snapshot[$color->field_name] = null;
                $color->field_name = str_replace("left", "right", $color->field_name);
                $color->save();
            }
            $snapshot->left_status = EmployeeWorkingInformationSnapshot::CURRENT;
            $snapshot->right_status = EmployeeWorkingInformationSnapshot::DENIED;
            $snapshot->right_approver_note = $snapshot->left_approver_note;
            $snapshot->right_requester_note = $snapshot->left_requester_note;
            $snapshot->right_requester_modify_time = $snapshot->left_requester_modify_time;
            $snapshot->left_requester_note = $snapshot->left_approver_note = $snapshot->left_requester_modify_time = null;
            $snapshot->right_approval_time = Carbon::now(Employee::find($from_employee_working_day->employee_id)->workLocation->getTimezone()->name_id)->toDateTimeString();
            $snapshot->save();
        }
    }

    /**
     * The function will remove the furide's snapshot in the furikae's case.
     *
     * @param App\EmployeeWorkingDay    $to_employee_working_day
     * @return void
     */
    protected function deleteSnapshotForFuride($to_employee_working_day)
    {
        foreach ($to_employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {
            $substitute_employee_working_information = SubstituteEmployeeWorkingInformation::where('employee_working_day_id', $to_employee_working_day->id)->where('employee_working_information_id', $snapshot->soon_to_be_EWI_id)->first();
            $substitute_employee_working_information->colorStatuses()->delete();
            $substitute_employee_working_information->delete();
            $snapshot->colorStatuses()->delete();
            $snapshot->forceDelete();
        }
    }

    /**
     * The function will send to data of working day
     * snapshots is a array which all of snapshot
     * snapshot_day_color is a array which all of color in a snapshot
     * date is client sent
     * timestampes is a array which all of timestamp with working day
     * schedule_transfer_data is data of calendar
     *
     * @param format(yyyy-mm-dd)    $date,
     * @return object $object
     */
    protected function sendDataFollowWorkingDayOnlySinseiChu($employee_working_days, $employee)
    {
        $snapshot_days = collect([]);
        foreach ($employee_working_days as $employee_working_day) {
        $list_sinsei_chu = $this->getSnapshotSinseiChuWithEmployWorkingDay($employee_working_day);
            if (isset($list_sinsei_chu)) {
                $employee_working_day->load(['workingTimestamps', 'employeeWorkingInformationSnapshots.colorStatuses' => function ($query) {
                    $query->where('enable', true);
                }]);
                $object = collect([
                    'snapshots' => $list_sinsei_chu,
                    'snapshot_day_color' => $this->getColorStatusOnlySinseiChuWithEmployWorkingDay($employee_working_day),
                    'date' => $employee_working_day->date,
                    'timestampes' => $employee_working_day->workingTimestamps,
                    'schedule_transfer_data' => [],
                ]);
                $snapshot_days->push($object);
            }
        }

        $this->sendDatePickerData($employee->id);

        return $snapshot_days;
    }

    /**
     * The function will return only the snapshot requesting follow employee working day
     *
     * @param App\EmployWorkingDay       $employee_working_day
     * @return array $list_snapshot
     */
    protected function getSnapshotSinseiChuWithEmployWorkingDay($employee_working_day)
    {
        $list_snapshot = collect([]);
        foreach ($employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {
            if ($snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING)
                $list_snapshot->push($snapshot->employeeWorkingInformation ? $snapshot->employeeWorkingInformation->mergeWorkingInformationToSnapshot() : $snapshot);
        }
        return $list_snapshot->isNotEmpty() ? $list_snapshot : null;
    }

    /**
     * The function will return only the color status of snapshot requesting follow employee working day
     *
     * @param App\EmployWorkingDay       $employee_working_day
     * @return array $list_color_status_for_working_day
     */
    protected function getColorStatusOnlySinseiChuWithEmployWorkingDay($employee_working_day)
    {
        $list_color_status_for_working_day = collect([]);
        foreach ($employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {
            if ($snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING)
                $list_color_status_for_working_day->push($snapshot->colorStatuses);
        }
        return $list_color_status_for_working_day->isNotEmpty() ? $list_color_status_for_working_day : null;
    }

    /**
     * The function gets to all data for the vue component
     * All work location
     *
     * @return data
     */
    protected function getAllWorkLocation()
    {
        // $all_work_locations = session('sinsei_user')->workLocation->company->workLocations()->get();
        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $all_work_locations = $work_location_setting_service->getAllWorkLocations();

        $timezones_service = resolve(TimezonesService::class);

        // The list of work location with work statuses and rest statuses
        $work_locations = $all_work_locations->map(function($work_location) use ($timezones_service) {

            $current_setting = $work_location->currentSetting();
            $current_timezone = $timezones_service->getTimezoneById($current_setting->timezone);

            return [
                'id'            => $work_location->id,
                'name'          => $work_location->name,
                'work_statuses' => $work_location->activatingWorkStatuses()->map(function($status) {
                    return [
                        'id'    => $status->id,
                        'name'  => $status->name,
                    ];
                }),
                'rest_statuses' => $work_location->activatingRestStatuses()->map(function($status) {
                    return [
                        'id'        => $status->id,
                        'name'      => $status->name,
                        'day_based' => $status->unit_type == true,
                    ];
                }),
                'utc_offset_number'          => $current_timezone ? $current_timezone->utc_offset_number : null,
                'is_use_go_out_button'          => $current_setting->go_out_button_usage == Setting::USE_GO_OUT_BUTTON,
                'start_time_round_up'          => $current_setting->start_time_round_up,
                'end_time_round_down'          => $current_setting->end_time_round_down,
            ];
        });

        $timestamp_types = [
            WorkingTimestamp::START_WORK => "出勤",
            WorkingTimestamp::END_WORK => "退勤",
            WorkingTimestamp::GO_OUT => "外出",
            WorkingTimestamp::RETURN => "戻り",
        ];

        Javascript::put([
            'work_locations'                    => $work_locations,
            'timestamp_types'                   => $timestamp_types,
        ]);

        // 2019-04-22: Add the list of all WorkAddress name if this company use WorkAdress mode
        if ($work_location_setting_service->getCompany()->use_address_system) {
            $work_address_names = WorkAddress::pluck('name', 'id');
            Javascript::put([
                'work_address_names' => $work_address_names,
            ]);
        }
    }

    /**
     * Validate the date string.
     *
     * @param string    $date
     * @param string    $format
     * @return boolean
     */
    protected function validateDateByFormat($date, $format = 'Y-m-d')
    {
        $result = Carbon::createFromFormat($format, $date);

        return $result && ($result->format($format) == $date);
    }

    /**
     * The function will convert from attribute of snapshot to attribute of employee working information
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot,
     * @param array    $array_color
     * @return array    $array_after_transfer_key,
     *
     */
    protected function transferSnapshotColorAttributeToEmployeeWorkingInformationColor($snapshot, $array_color, $register_info, $array_after_transfer_key = [])
    {
        $array_transfer_key = [
            'left_work_status_id'                                   =>      'planned_work_status_id',
            'left_rest_status_id'                                   =>      'planned_rest_status_id',
            'left_paid_rest_time_start'                             =>      'paid_rest_time_start',
            'left_paid_rest_time_end'                               =>      'paid_rest_time_end',
            'left_paid_rest_time_period'                            =>      'paid_rest_time_period',
            'left_not_include_break_time_when_display_planned_time' => 'not_include_break_time_when_display_planned_time',
            'left_planned_work_location_id'                         =>      'planned_work_location_id',
            'left_planned_early_arrive_start'                       =>      'planned_early_arrive_start',
            'left_planned_early_arrive_end'                         =>      'planned_early_arrive_end',
            'left_planned_overtime_start'                           =>      'planned_overtime_start',
            'left_planned_overtime_end'                             =>      'planned_overtime_end',
            'left_planned_break_time'                               =>      'planned_break_time',
            'left_real_break_time'                                  =>      'real_break_time',
            'left_planned_night_break_time'                         =>      'planned_night_break_time',
            'left_real_night_break_time'                            =>      'real_night_break_time',
            'left_planned_late_time'                                =>      'planned_late_time',
            'left_planned_early_leave_time'                         =>      'planned_early_leave_time',
            'left_planned_go_out_time'                              =>      'planned_go_out_time',
        ];

        $timestamps = [];

        $check_working_timestamp_start_work_time_created =
        $check_working_timestamp_end_work_time_created =
        $check_transfer_key_exists = false;

        $check_start_work_time = empty($snapshot->left_timestamped_start_work_time_work_location_id);
        $check_end_work_time = $array_color->contains('field_name', 'left_timestamped_end_work_time_work_location_id');

        foreach ($array_color as $color) {
            if (array_key_exists($color->field_name, $array_transfer_key)) {

                if (!$check_transfer_key_exists)
                    $check_transfer_key_exists = true;

                $array_after_transfer_key = array_add($array_after_transfer_key, $array_transfer_key[$color->field_name], $snapshot[$color->field_name]);

            } elseif ($color->field_name === 'left_timestamped_start_work_date' || $color->field_name === 'left_timestamped_start_work_time') {
                if (!$check_working_timestamp_start_work_time_created) {
                    array_push($timestamps, new WorkingTimestamp([
                        'enable'                => true,
                        'processed_date_value'  => $snapshot['left_timestamped_start_work_date'],
                        'processed_time_value'  => $snapshot['left_timestamped_start_work_time'],
                        'timestamped_type'      => WorkingTimestamp::START_WORK,
                        'work_location_id'      => !$check_start_work_time ? $snapshot->employeeWorkingInformation->timestamped_start_work_time_work_location_id : $snapshot->left_planned_work_location_id,
                        'registerer_type'       => $register_info["registerer_type"],
                        'registerer_id'         => $register_info["registerer_id"],
                        'approved'            => true,
                    ]));
                    $check_working_timestamp_start_work_time_created = true;
                }
            } elseif ($color->field_name === 'left_timestamped_end_work_date' || $color->field_name === 'left_timestamped_end_work_time' || $color->field_name === 'left_timestamped_end_work_time_work_location_id') {
                if (!$check_working_timestamp_end_work_time_created) {
                    array_push($timestamps, new WorkingTimestamp([
                        'enable'                => true,
                        'processed_date_value'  => $snapshot['left_timestamped_end_work_date'],
                        'processed_time_value'  => $snapshot['left_timestamped_end_work_time'],
                        'timestamped_type'      => WorkingTimestamp::END_WORK,
                        'work_location_id'      => !$check_end_work_time ? $snapshot->employeeWorkingInformation->timestamped_end_work_time_work_location_id : $snapshot->left_timestamped_end_work_time_work_location_id,
                        'registerer_type'       => $register_info["registerer_type"],
                        'registerer_id'         => $register_info["registerer_id"],
                        'approved'            => true,
                    ]));
                    $check_working_timestamp_end_work_time_created = true;
                }
            }
        }

        if ($check_transfer_key_exists) {
            $array_after_transfer_key = array_add($array_after_transfer_key, 'last_modify_person_id', $register_info["registerer_id"]);
            if ($register_info["registerer_type"] == WorkingTimestamp::EMPLOYEE)
                $array_after_transfer_key = array_add($array_after_transfer_key, 'last_modify_person_type', EmployeeWorkingInformation::MODIFY_PERSON_TYPE_EMPLOYEE);
            else if ($register_info["registerer_type"] == WorkingTimestamp::MANAGER)
                $array_after_transfer_key = array_add($array_after_transfer_key, 'last_modify_person_type', EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER);
        }

        return [
            'array_after_transfer_key' => $array_after_transfer_key,
            'timestamps' => $timestamps
        ];
    }

    /**
     * The function will get data from $employee_working_information to save the right of snapshot
     *
     * @param App\EmployeeWorkingInformationSnapshot    $snapshot,
     * @param App\EmployeeWorkingInformation    $employee_working_information
     * @return App\EmployeeWorkingInformationSnapshot    $snapshot,
     *
     */
    private function saveEWIToRightOfSnapshot($snapshot, $employee_working_information)
    {
        $array_transfer_key = [
            'right_rest_status_id'                   =>      'planned_rest_status_id',
            'right_planned_work_span_start'          =>      'planned_work_span_start',
            'right_planned_work_span_end'            =>      'planned_work_span_end',
            'right_planned_work_span'                =>      'planned_work_span',
            'right_paid_rest_time_start'             =>      'paid_rest_time_start',
            'right_paid_rest_time_end'               =>      'paid_rest_time_end',
            'right_paid_rest_time_period'            =>      'paid_rest_time_period',
            'right_planned_work_location_id'         =>      'planned_work_location_id',
            'right_planned_early_arrive_start'       =>      'planned_early_arrive_start',
            'right_planned_early_arrive_end'         =>      'planned_early_arrive_end',
            'right_planned_overtime_start'           =>      'planned_overtime_start',
            'right_planned_overtime_end'             =>      'planned_overtime_end',
            'right_planned_break_time'               =>      'planned_break_time',
            'right_planned_night_break_time'         =>      'planned_night_break_time',
            'right_planned_late_time'                =>      'planned_late_time',
            'right_planned_early_leave_time'         =>      'planned_early_leave_time',
            'right_planned_go_out_time'              =>      'planned_go_out_time',
        ];

        foreach ($array_transfer_key as $key => $value) {
            $snapshot->$key = $employee_working_information->$value;
        }

        return $snapshot;
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Http\Requests\MaebaraiSettingRequest;
use JavaScript;
use CaeruJBA;

class MaebaraiSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:view_maebarai_setting_page');
        $this->middleware('can:change_maebarai_setting_page')->only(['updateMaebaraiSetting']);
    }

    /**
     * Show Maebarai setting page
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
        $company = $request->user()->company;

        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        $breadcrumbs_service->resetToMaebaraiRoot();

        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::MAEBARAI_SETTING, $request->route()->getName(), $request->route());

        $maebarai_setting = [];
        $maebarai_setting['maebarai_enable'] = $company['maebarai_enable'];
        $maebarai_setting['maebarai_payment_rate'] = $company['maebarai_payment_rate'];
        $maebarai_setting['maebarai_auto_approve'] = $company['maebarai_auto_approve'];
        $maebarai_setting['maebarai_payment_method'] = $company['maebarai_payment_method'];
        $maebarai_setting['maebarai_branch_code'] = $company['maebarai_branch_code'];
        $maebarai_setting['maebarai_account_number'] = $company['maebarai_account_number'];
        $maebarai_setting['maebarai_account_name'] = $company['maebarai_account_name'];

        JavaScript::put([
            'maebarai_setting' => $maebarai_setting,
        ]);
        return view('maebarai.maebarai_setting');
    }

    /**
     * Update Maebarai Setting in storage
     *
     * @param  App\Http\Requests\MaebaraiSettingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(MaebaraiSettingRequest $request) 
    {
        $company = $request->user()->company;

        if ($request->maebarai_enable == 1) {
            $company->update($request->only(
                'maebarai_enable',
                'maebarai_payment_rate',
                'maebarai_auto_approve',
                'maebarai_payment_method',
                'maebarai_branch_code',
                'maebarai_account_number'
            ));
            $company->maebarai_account_name = CaeruJBA::CnvStr($request->input('maebarai_account_name'));
            $company->save();
        }
        else
            $company->update($request->only(
                'maebarai_enable'));
        $maebarai_setting = [];
        $maebarai_setting['maebarai_enable'] = $company['maebarai_enable'];
        $maebarai_setting['maebarai_payment_rate'] = $company['maebarai_payment_rate'];
        $maebarai_setting['maebarai_auto_approve'] = $company['maebarai_auto_approve'];
        $maebarai_setting['maebarai_payment_method'] = $company['maebarai_payment_method'];
        $maebarai_setting['maebarai_branch_code'] = $company['maebarai_branch_code'];
        $maebarai_setting['maebarai_account_number'] = $company['maebarai_account_number'];
        $maebarai_setting['maebarai_account_name'] = $company['maebarai_account_name'];

        return [
            'success' => '保存しました',
            'settings' => $maebarai_setting,
        ];
    }
}

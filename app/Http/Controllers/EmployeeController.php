<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Employee;
use App\WorkLocation;
use App\WorkTime;
use App\Department;
use App\Events\EmployeeInformationChanged;
use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\EmployeeWorkRequest;
use App\Http\Requests\EmployeeMaebaraiRequest;
use App\Http\Controllers\SearchController;
use App\Services\ChangeWorkTimePerDayService;
use App\Http\Controllers\Reusables\GetEmployeeBaseOnWorkLocationTrait as GetEmployeeTrait;
use App\Http\Controllers\Reusables\GenerateNumberTrait;
use App\Http\Requests\EmployeeCsvRequest;
use Caeru;
use CaeruJBA;
use Constants;
use League\Csv\Writer;
use App\Services\TodofukenService;

class EmployeeController extends Controller
{
    use GetEmployeeTrait, GenerateNumberTrait;

    // The search controller instance
    private $search_controller = null;

    /**
     * The sample record
     *
     * @var array
     */
    protected $sample_record = [
        'Sample','AAA','山田','太郎','ヤマダ','タロウ','1990/01/01','男','735-0017','広島県','安芸郡府中町青崎南6-24','082‑258‑2988','sample@it-z.biz','100','1990/01/01','営業','通常','正社員','月給','勤務中','1990/01/01','12345,67890','1','01/01','8.25','週4日勤務',''
    ];

    /**
     * The attributes that should be add to header in file csv.
     *
     * @var array
     */
    protected $key_value = [
        '従業員ID(必須)' => 'presentation_id',
        'パスワード(英数)' => 'password',
        '従業員名(必須) 姓' => 'last_name',
        '従業員名(必須) 名' => 'first_name',
        '従業員名(カナ)(必須) セイ' => 'last_name_furigana',
        '従業員名(カナ)(必須) メイ' => 'first_name_furigana',
        '生年月日(必須) (年/月/日)' => 'birthday',
        '性別(必須)' => 'gender',
        '郵便番号' => 'postal_code',
        '都道府県' => 'todofuken',
        '住所:' => 'address',
        '電話番号' => 'telephone',
        'メールアドレス' => 'email',
        '所属先ID(必須)' => 'work_location_id',
        '入社日(必須)(年/月/日)' => 'joined_date',
        '部署' => 'department_id',
        '就労形態(必須)' => 'schedule_type',
        '採用形態(必須)' => 'employment_type',
        '給与形態(必須)' => 'salary_type',
        '雇用状態(必須)' => 'work_status',
        '退職日(年/月/日)' => 'resigned_date',
        '承認者ID' => 'chiefs',
        '有給対象外(チェックを入れる場合は1)' => 'paid_holiday_exception',
        '更新日(月/日)' => 'holidays_update_day',
        '１日の労働時間' => 'work_time_per_day',
        '有給休暇タイプ' => 'holiday_bonus_type',
        'エラー' => 'error',
    ];

    /**
     * The attributes that should be add to header in file csv.
     *
     * @var array
     */
    protected $list_key_value = [
        'ICカード登録用番号(※削除・変更不可)' => 'card_registration_number',
        '従業員ID(必須)' => 'presentation_id',
        'パスワード(英数)' => 'password',
        '並び順' => 'view_order',
        '従業員名(必須) 姓' => 'last_name',
        '従業員名(必須) 名' => 'first_name',
        '従業員名(カナ)(必須) セイ' => 'last_name_furigana',
        '従業員名(カナ)(必須) メイ' => 'first_name_furigana',
        '生年月日(必須) (年/月/日)' => 'birthday',
        '性別(必須)' => 'gender',
        '郵便番号' => 'postal_code',
        '都道府県' => 'todofuken',
        '住所:' => 'address',
        '電話番号' => 'telephone',
        'メールアドレス' => 'email',
        '所属先ID(必須)' => 'work_location_id',
        '入社日(必須)(年/月/日)' => 'joined_date',
        '部署' => 'department_id',
        '就労形態(必須)' => 'schedule_type',
        '採用形態(必須)' => 'employment_type',
        '給与形態(必須)' => 'salary_type',
        '雇用状態(必須)' => 'work_status',
        '退職日(年/月/日)' => 'resigned_date',
        '承認者ID' => 'chiefs',
        '有給対象外(チェックを入れる場合は1)' => 'paid_holiday_exception',
        '更新日(月/日)' => 'holidays_update_day',
        '１日の労働時間' => 'work_time_per_day',
        '有給休暇タイプ' => 'holiday_bonus_type',
        'エラー' => 'error',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SearchController $controller)
    {
        $this->middleware('auth');
        $this->middleware('choose');
        $this->middleware('require_work_location')->only(['create', 'store']);
        $this->middleware('can:see_employee_tab');
        $this->middleware('can:view_employee_basic_info')->only(['edit', 'create', 'store', 'update']);
        $this->middleware('can:change_employee_basic_info')->only(['create', 'store', 'update']);
        $this->middleware('can:view_employee_work_info')->only(['editWork', 'updateWork']);
        $this->middleware('can:change_employee_work_info')->only('updateWork');
        $this->middleware('can:view_employee_maebarai_info')->only(['editMaebarai', 'updateMaebarai']);
        $this->middleware('can:change_employee_maebarai_info')->only('updateMaebarai');
        $this->search_controller = $controller;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (session('employee_search_history')) {
            $search_history_conditions = session('employee_search_history')['conditions'];

            $employees_list = $this->search_controller->getEmployeesApplyConditions($search_history_conditions);
        } else {
            // By default, the list page will only list employees with working status '勤務中'
            $default_conditions =  $this->search_controller->employee_default_conditions;

            $employees_list = $this->search_controller->getEmployeesApplyConditionsSaveResultToSession($default_conditions);
        }


        return view('employee.list')->with([
            'employees' => $employees_list->paginate(20),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\EmployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $employee = new Employee($request->only([
            'presentation_id',
            'password',
            'first_name',
            'first_name_furigana',
            'last_name',
            'last_name_furigana',
            'birthday',
            'gender',
            'postal_code',
            'todofuken',
            'address',
            'telephone',
            'email',
            'work_location_id',
            'change_date',
            'new_work_location_id',
            'joined_date',
            'department_id',
            'schedule_type',
            'employment_type',
            'salary_type',
            'work_status',
            'resigned_date',
        ]));

        $employee->card_registration_number = $this->generateUniqueNumber(Employee::class, 'card_registration_number');

        // work_time_per_day default to 8h/day
        $employee->work_time_per_day = 8;

        $employee->save();

        $employee->chiefs()->sync($request->input('chiefs'));
        if($request->input('new_work_location_id') != '') {
            $service = resolve(ChangeWorkTimePerDayService::class);
            $service->addLocationChangeTimer($employee);
        } else {
            $service = resolve(ChangeWorkTimePerDayService::class);
            $service->removeLocationChangeTimer($employee);
        }

        event(new EmployeeInformationChanged());

        $request->session()->flash('success', '保存しました');

        return Caeru::redirect('edit_employee', $employee->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee, $page = 1)
    {
        return view('employee.edit', [
            'employee'  => $employee,
            'page'      => $page,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\EmployeeRequest  $request
     * @param  Employee     $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee, $page = 1)
    {
        $employee->update($request->only([
            'presentation_id',
            'first_name',
            'first_name_furigana',
            'last_name',
            'last_name_furigana',
            'birthday',
            'gender',
            'postal_code',
            'todofuken',
            'address',
            'telephone',
            'email',
            'work_location_id',
            'change_date',
            'new_work_location_id',
            'joined_date',
            'department_id',
            'schedule_type',
            'employment_type',
            'salary_type',
            'work_status',
            'resigned_date',
        ]));

        $employee->chiefs()->sync($request->input('chiefs'));

        // If the password is changed (meaning this field is not null) then save it
        if ($request->input('password')) {
            $employee->update(['password' => $request->input('password')]);
        }

        if($request->input('new_work_location_id') != '') {
            $service = resolve(ChangeWorkTimePerDayService::class);
            $service->addLocationChangeTimer($employee);
        } else {
            $service = resolve(ChangeWorkTimePerDayService::class);
            $service->removeLocationChangeTimer($employee);
        }

        event(new EmployeeInformationChanged());

        $request->session()->flash('success', '保存しました');

        return Caeru::redirect('edit_employee', [$employee->id, $page]);
    }

    /**
     * Show the form for editing the employee's work.
     *
     * @param  Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function editWork(Employee $employee, $page = 1)
    {
        Javascript::put([
            'model_data' => $employee->schedules->toArray(),
            'default_work_location' => $employee->work_location_id,
        ]);
        return view('employee.edit_work', [
            'employee' => $employee,
            'page'      => $page,
            'can_edit_update_date' => $employee->paidHolidayInformations->isEmpty(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\EmployeeWorkRequest  $request
     * @param  Employee     $employee
     * @return \Illuminate\Http\Response
     */
    public function updateWork(EmployeeWorkRequest $request, Employee $employee, $page = 1)
    {
        $service = resolve(ChangeWorkTimePerDayService::class);
        $employee->holiday_bonus_type = $request->input('holiday_bonus_type');

        if ($employee->work_time_per_day != $request->input('work_time_per_day')) {
            $employee->work_time_per_day = $request->input('work_time_per_day');

            // Since this is immediate change, it will take effect immediately
            $service->changeWorkTimePerDayOfThisEmployee($employee, $request->input('work_time_per_day'));
        }

        if ($request->filled('holidays_update_day')) {
            $employee->holidays_update_day = $request->input('holidays_update_day');
        }

        if ($request->filled('work_time_change_date') && $request->filled('work_time_change_to')) {
            $employee->work_time_change_date = $request->input('work_time_change_date');
            $employee->work_time_change_to = $request->input('work_time_change_to');

            // This change on the other hand will not take effect immediately, but create a timer to take effect later
            $service->addChangeWorkTimePerDayTimer($employee, $request->input('work_time_change_to'), $request->input('work_time_change_date'));
        }

        $employee->all_timestamps_register_to_belong_work_location = $request->input('all_timestamps_register_to_belong_work_location') == true;
        $employee->paid_holiday_exception = $request->input('paid_holiday_exception') !== null;

        $employee->update($request->only([
            'gps_device_type',
            'gps_number',
            'device_id'
        ]));

        if ($request->input('delete_card')) {
            $employee->card_number = null;
        }
        $employee->save();

        $request->session()->flash('success', '保存しました');

        return Caeru::redirect('edit_employee_work', [$employee->id, $page]);;
    }

    /**
     * Show the form for editing the employee's work.
     *
     * @param  Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function editMaebarai(Employee $employee, $page = 1)
    {
        $maebarai_setting = [];
        $maebarai_setting['maebarai_enable'] = $employee['maebarai_enable'];
        $maebarai_setting['maebarai_salary_per_hour'] = $employee['maebarai_salary_per_hour'];
        $maebarai_setting['maebarai_bank_code'] = $employee['maebarai_bank_code'];
        $maebarai_setting['maebarai_bank_name'] = $employee['maebarai_bank_name'];
        $maebarai_setting['maebarai_branch_code'] = $employee['maebarai_branch_code'];
        $maebarai_setting['maebarai_branch_name'] = $employee['maebarai_branch_name'];
        $maebarai_setting['maebarai_account_number'] = $employee['maebarai_account_number'];
        $maebarai_setting['maebarai_account_name'] = $employee['maebarai_account_name'];

        JavaScript::put([
            'maebarai_setting' => $maebarai_setting,
            'employee' => $employee,
            'page' => $page,
        ]);

        return view('employee.edit_maebarai', [
            'employee'  => $employee,
            'page'      => $page,
        ]);
    }
    /**
     * Update Maebarai Setting in storage
     *
     * @param  App\Http\Requests\EmployeeMaebaraiRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function updateMaebarai(EmployeeMaebaraiRequest $request, Employee $employee, $page = 1) 
    {
        $employee->update($request->only(
            'maebarai_enable', 
            'maebarai_salary_per_hour', 
            'maebarai_bank_code', 
            'maebarai_branch_code', 
            'maebarai_account_number'));

        $employee->maebarai_bank_name = CaeruJBA::CnvStr($request->input('maebarai_bank_name'));
        $employee->maebarai_branch_name = CaeruJBA::CnvStr($request->input('maebarai_branch_name'));
        $employee->maebarai_account_name = CaeruJBA::CnvStr($request->input('maebarai_account_name'));
        $employee->save();

        
        $maebarai_setting = [];
        $maebarai_setting['maebarai_enable'] = $employee['maebarai_enable'];
        $maebarai_setting['maebarai_salary_per_hour'] = $employee['maebarai_salary_per_hour'];
        $maebarai_setting['maebarai_bank_code'] = $employee['maebarai_bank_code'];
        $maebarai_setting['maebarai_bank_name'] = $employee['maebarai_bank_name'];
        $maebarai_setting['maebarai_branch_code'] = $employee['maebarai_branch_code'];
        $maebarai_setting['maebarai_branch_name'] = $employee['maebarai_branch_name'];
        $maebarai_setting['maebarai_account_number'] = $employee['maebarai_account_number'];
        $maebarai_setting['maebarai_account_name'] = $employee['maebarai_account_name'];

        JavaScript::put([
            'maebarai_setting' => $maebarai_setting,
            'employee' => $employee,
            'page' => $page,
        ]);

        return [
            'success' => '保存しました',
            'settings' => $maebarai_setting,
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Download data with Format
     *
     * @param \Illuminate\Http\Request      $request
     * @return CSV
     */
    public function download(Request $request)
    {

        $records = [];

        array_push($records, $this->sample_record);

        //load the CSV document from a string
        $csv = Writer::createFromString('');

        //insert the header
        $csv->insertOne(array_keys($this->key_value));

        //insert all the records
        $csv->insertAll($records);

        $csv->output('employee.csv');
        die;
    }

    /**
     * Download employee list with Format
     *
     * @param \Illuminate\Http\Request      $request
     * @return CSV
     */
    public function download_list(Request $request)
    {

        $records = [];

        $chosen_work_location = session('current_work_location');
        if ($chosen_work_location === 'all') {
            $employees = Employee::all();
        } else {
            $employees = Employee::where('work_location_id', $chosen_work_location)->get();
        }
        foreach($employees as $employee) {
            $record = [];
            array_push($record, $employee->card_registration_number);
            array_push($record, $employee->presentation_id);
            if (isset($employee->password))
                array_push($record, '有');
            else
                array_push($record, '');
            if ($chosen_work_location === 'all') {
                array_push($record, '');
            } else {
                array_push($record, $employee->view_order - 1);
            }
            array_push($record, $employee->last_name);
            array_push($record, $employee->first_name);
            array_push($record, $employee->last_name_furigana);
            array_push($record, $employee->first_name_furigana);
            array_push($record, str_replace('-', '/', $employee->birthday));
            array_push($record, Constants::genders()[$employee->gender]);
            if (isset($employee->postal_code)) {
                array_push($record, substr($employee->postal_code, 0, 3) . '-' . substr($employee->postal_code, 3, 4));
            } else {
                array_push($record, '');
            }
            if (isset($employee->todofuken)) {
                $todofuken_service = resolve(TodofukenService::class);
                array_push($record, $todofuken_service->getName($employee->todofuken));
            } else {
                array_push($record, '');
            }
            array_push($record, $employee->address);
            array_push($record, $employee->telephone);
            array_push($record, $employee->email);
            array_push($record, WorkLocation::find($employee->work_location_id)->presentation_id);
            array_push($record, str_replace('-', '/', $employee->joined_date));
            if (isset($employee->department_id)) {
                $department = Department::find($employee->department_id)->name;
                array_push($record, $department);
            } else {
                array_push($record, '');
            }
            array_push($record, Constants::scheduleTypes()[$employee->schedule_type]);
            array_push($record, Constants::employmentTypes()[$employee->employment_type]);
            array_push($record, Constants::salaryTypes()[$employee->salary_type]);
            array_push($record, Constants::workStatuses()[$employee->work_status]);
            array_push($record, str_replace('-', '/', $employee->resigned_date));
            $chiefs = str_replace('[', '', $employee->chiefs);
            $chiefs = str_replace(']', '', $chiefs);
            array_push($record, $chiefs);
            array_push($record, $employee->paid_holiday_exception);
            array_push($record, $employee->holidays_update_day);
            array_push($record, $employee->work_time_per_day);
            if (isset($employee->holiday_bonus_type)) {
                array_push($record, Constants::holidayBonusTypes()[$employee->holiday_bonus_type]);
            } else {
                array_push($record, '');
            }
            array_push($record, '');

            array_push($records, $record);
        }


        //load the CSV document from a string
        $csv = Writer::createFromString('');

        //insert the header
        $csv->insertOne(array_keys($this->list_key_value));

        //insert all the records
        $csv->insertAll($records);

        $csv->output('employee_list.csv');
        die;
    }

    /**
     * Upload data with given conditions
     *
     * @param App\Http\Requests\EmployeeCsvRequest      $request
     * @return Array|JSON
     */
    public function upload(EmployeeCsvRequest $request)
    {
        if ($request->type == 1) {
            foreach ($request->true_data as $data) {
                $employee = new Employee(Arr::only($data, [
                    'presentation_id',
                    'password',
                    'first_name',
                    'first_name_furigana',
                    'last_name',
                    'last_name_furigana',
                    'birthday',
                    'gender',
                    'postal_code',
                    'todofuken',
                    'address',
                    'telephone',
                    'email',
                    'work_location_id',
                    'joined_date',
                    'department_id',
                    'schedule_type',
                    'employment_type',
                    'salary_type',
                    'work_status',
                    'resigned_date',
                    'paid_holiday_exception',
                    'holidays_update_day',
                    'holiday_bonus_type',
                    'work_time_per_day',
                ]));
                $employee->card_registration_number = $this->generateUniqueNumber(Employee::class, 'card_registration_number');

                $employee->save();
    
                $employee->chiefs()->sync($data['chiefs']);
    
                event(new EmployeeInformationChanged());
            }
        } else {
            foreach ($request->true_data as $data) {
                $employee = Employee::where('card_registration_number', $data['card_registration_number'])->first();
                $employee->update(Arr::only($data, [
                    'presentation_id',
                    'first_name',
                    'first_name_furigana',
                    'last_name',
                    'last_name_furigana',
                    'birthday',
                    'gender',
                    'postal_code',
                    'todofuken',
                    'address',
                    'telephone',
                    'email',
                    'work_location_id',
                    'joined_date',
                    'department_id',
                    'schedule_type',
                    'employment_type',
                    'salary_type',
                    'work_status',
                    'resigned_date',
                    'paid_holiday_exception',
                    'holidays_update_day',
                    'holiday_bonus_type',
                    'work_time_per_day',
                ]));

                if (isset($data['view_order']))
                    $employee->view_order = $data['view_order'];

                if (isset($data['password']))
                    $employee->update(['password' => $data['password']]);
                $employee->save();
    
                $employee->chiefs()->sync($data['chiefs']);
    
                event(new EmployeeInformationChanged());
            }
        }
        
        return [ 'success' => '保存しました'];
    }
}

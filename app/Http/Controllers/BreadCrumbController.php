<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use Caeru;

class BreadCrumbController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Go back one page in the breadcrumb tree
     */
    public function goBack()
    {
        $breadcrumbs = resolve(BreadCrumbService::class);

        $page = $breadcrumbs->goBackOneLevel();

        return Caeru::redirect($page->getRoute(), $page->getParameters());
    }

    /**
     * Go to the next element in the second dimension of the parent's page.
     *
     * @param mix       $pivot_value    the value of the important parameter (for example: employee_id)
     */
    public function goNext($pivot_value)
    {
        $breadcrumbs = resolve(BreadCrumbService::class);

        $page = $breadcrumbs->gotoNextElementInSecondDimensionOfParentNode($pivot_value);

        if (isset($page)) {
            return Caeru::redirect($page->getRoute(), $page->getParameters());
        } else {
            return back();
        } 
    }

    /**
     * Go to the previous element in the second dimension of the parent's page.
     *
     * @param mix       $pivot_value    the value of the important parameter (for example: employee_id)
     */
    public function goPrevious($pivot_value)
    {
        $breadcrumbs = resolve(BreadCrumbService::class);

        $page = $breadcrumbs->gotoPreviousElementInSecondDimensionOfParentNode($pivot_value);

        if (isset($page)) {
            return Caeru::redirect($page->getRoute(), $page->getParameters());
        } else {
            return back();
        }
    }

    /**
     * Go to a specific node, given a level.
     *
     * @param int       $level
     */
    public function goTo($level)
    {
        $breadcrumbs = resolve(BreadCrumbService::class);

        $page = $breadcrumbs->goToLevel($level);

        if (isset($page)) {
            return Caeru::redirect($page->getRoute(), $page->getParameters());
        } else {
            return back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WorkLocation;
use App\Employee;
use App\MaebaraiHistory;
use Illuminate\Routing\Controller;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Http\Controllers\Reusables\SendDatePickerDataBaseOnCurrentWorkLocationTrait as SendDatePickerDataTrait;
use App\Http\Controllers\Reusables\CustomizedPaginationTrait;
use Carbon\Carbon;
use Constants;

class MaebaraiInformationController extends Controller
{
    use SendDatePickerDataTrait, CustomizedPaginationTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose:singular');
        $this->middleware('can:view_maebarai_information_page');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request  $request
     * @param string                    $reset_search_conditions
     * @param int                       $page
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $reset_search_conditions = null, $page = 1) 
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        if ($reset_search_conditions == true) {
            session()->forget('maebarai_information');

            // Reset the breadcumbs if reset the search history
            $breadcrumbs_service->resetToMaebaraiRoot();
        }

        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::MAEBARAI_INFORMATION, $request->route()->getName(), $request->route());
        $conditions = session('maebarai_information') ? session('maebarai_information') : $this->getDefaultConditions($request);
        $result = $this->searchMaebaraiTranaction($conditions);
        $data = $this->paginateData($result, $page);
        $employee_names = $this->getAllEmployeeNames($request);

        Javascript::put([
            'datas' => $data['data'],
            'current_page' => $data['current_page'],
            'total' => $result->count(),
            'employee_names' => $employee_names,
            'per_page' => $this->ITEMS_PER_PAGE,
            'conditions' => $conditions,
        ]);

        return view('maebarai.maebarai_information');
    }

    /**
     * Calculate statistic data of some employees base on the given conditions.
     *
     * @param \Illuminate\Http\Request  $request
     * @param int                       $page
     * @return array|json
     */
    public function search(Request $request, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => 0 ]);

        $conditions = $request->input('conditions');
        $result = $this->searchMaebaraiTranaction($conditions);
        $data = $this->paginateData($result, $page);
        $employee_names = $this->getAllEmployeeNames($request);

        return [
            'datas' => $data['data'],
            'current_page' => $data['current_page'],
            'total' => $result->count(),
            'employee_names' => $employee_names,
            'per_page' => $this->ITEMS_PER_PAGE,
            'conditions' => $conditions,
        ];
    }

    /**
     * Display a details of transaction.
     *
     * @param \Illuminate\Http\Request  $request
     * @param Employee                  $employee
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, Employee $employee) 
    {
        $conditions = session('maebarai_information');
        $employees = $this->searchEmployeeWithConditions($this->getDefaultConditions($request));
        $can_go_previous = $can_go_next = true;
        $index = $employees->search(function($data) use ($employee){
            return $data->id === $employee->id;
        });

        if ($index == 0) {
            $can_go_previous = false;
        } else if ($index == count($employees) - 1) {
            $can_go_next = false;
        }
        
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::MAEBARAI_DETAIL, $request->route()->getName(), $request->route());

        if ($request->wantsJson()) {
            $conditions = $request->get('conditions');
        }

        $result = $this->searchMaebaraiDetailTranaction($employee, $conditions);

        if ($request->wantsJson()) {
            return [
                'datas' => $result,
                'conditions' => $conditions,
                'id'        => $employee->id,
                'can_go_previous'   => $can_go_previous,
                'can_go_next'   => $can_go_next,
            ];
        }

        Javascript::put([
            'datas' => $result,
            'conditions' => $conditions,
            'id'        => $employee->id,
            'can_go_previous'   => $can_go_previous,
            'can_go_next'   => $can_go_next,
        ]);

        return view('maebarai.maebarai_information_detail')->with([
            'presentation_id'        => $employee->presentation_id,
            'name'        => $employee->fullName(),
        ]);
    }

    public function findEmployee(Request $request, Employee $employee)
    {
        $employees = $this->searchEmployeeWithConditions($this->getDefaultConditions($request));
        $step = $request->input('step');
        $index = $employees->search(function($data) use ($employee){
            return $data->id === $employee->id;
        });
        $step == 1 ? $index-- : $index++;
        $employee = $employees[$index];

        $can_go_previous = $can_go_next = true;
        if ($index == 0) {
            $can_go_previous = false;
        } else if ($index == count($employees) - 1) {
            $can_go_next = false;
        }

        $conditions = $request->input('conditions');
        $result = $this->searchMaebaraiDetailTranaction($employee, $conditions);

        return [
            'datas' => $result,
            'id'        => $employee->id,
            'presentation_id'        => $employee->presentation_id,
            'name'        => $employee->fullName(),
            'can_go_previous'   => $can_go_previous,
            'can_go_next'   => $can_go_next,
        ];
    }
    /**
     * Get the default conditions for the search box in pre pay information page.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getDefaultConditions(Request $request)
    {
        $work_location = session('current_work_location');
        if ($work_location === "all") {
            $setting = $request->user()->company->setting;
        } else {
            $work_location = WorkLocation::find($work_location);
            $setting = $work_location->currentSetting();
        }

        return [
            'year' => Carbon::now()->year,
            'month' => Carbon::now()->month,
            'presentation_id' => null,
            'name' => null,
        ];
    }

    /**
     * Summary the salaray data for all employees of the current work location and apply the search conditions to filter.
     * This function is just a clean wrapper.
     *
     * @param array         $conditions
     * @return Collection
     */
    protected function searchMaebaraiTranaction($conditions)
    {
        // Save the search conditions history to session
        session(['maebarai_information' => $conditions]);

        // First search employees data base on the first conditions
        $employees = $this->searchEmployeeWithConditions($conditions);
        $today = Carbon::today();
        $month = $today->copy();
        $month->year = $conditions['year']; $month->month = $conditions['month'];
        $start_date = Carbon::create($month->year, $month->month, 1, 0, 0, 0);
        $end_date = Carbon::create($month->year, $month->month + 1, 1, 0, 0, 0);

        $result = collect([]);
        foreach($employees as $employee) {
            $transactions = MaebaraiHistory::where('employee_id', $employee->id)
                                            ->where('transaction_status', '!=', MaebaraiHistory::TRANSACTION_API_FAIL)
                                            ->where('tran_date', '>=', $start_date)
                                            ->where('tran_date', '<', $end_date)
                                            ->get();
            $maebarai_amount = $transactions->sum(function($transaction) {
                if ($transaction->transaction_status != MaebaraiHistory::TRANSACTION_STATUS_FAIL) return $transaction->maebarai_amount;
                else return 0;
            });
            $transfer_amount = $transactions->sum(function($transaction) {
                if ($transaction->transaction_status != MaebaraiHistory::TRANSACTION_STATUS_FAIL) return $transaction->transfer_amount;
                else return 0;
            });
            $transfer_fee = $transactions->sum(function($transaction) {
                if ($transaction->transaction_status != MaebaraiHistory::TRANSACTION_STATUS_FAIL) return $transaction->transfer_fee;
                else return 0;
            });
            $api_fee = $transactions->sum(function($transaction) {
                return $transaction->api_usage_fee;
            });
            $result->push([
                'id'                =>  $employee->id,
                'presentation_id'   =>  $employee->presentation_id,
                'name'              =>  $employee->fullName(),
                'prepay_money'      =>  $maebarai_amount,
                'realpay_money'     =>  $transfer_amount,
                'send_fee'          =>  $transfer_fee,
                'api_fee'           =>  $api_fee,
            ]);
        }
        return $result;
    }

    /**
     * Summary the salaray data for all employees of the current work location and apply the search conditions to filter.
     * This function is just a clean wrapper.
     *
     * @param Employee                  $employee
     * @param array         $conditions
     * @return Collection
     */
    protected function searchMaebaraiDetailTranaction($employee, $conditions)
    {
        // Save the search conditions history to session
        session(['maebarai_information' => $conditions]);
        $today = Carbon::today();
        $month = $today->copy();
        $month->year = $conditions['year']; $month->month = $conditions['month'];
        $start_date = Carbon::create($month->year, $month->month, 1, 0, 0, 0);
        $end_date = Carbon::create($month->year, $month->month + 1, 1, 0, 0, 0);

        $transactions = MaebaraiHistory::where('employee_id', $employee->id)
                                        ->where('transaction_status', '!=', MaebaraiHistory::TRANSACTION_API_FAIL)
                                        ->where('tran_date', '>=', $start_date)
                                        ->where('tran_date', '<', $end_date)
                                        ->orderBy('tran_date')->get();

        $transactions = $transactions->transform(function($transaction, $key) {
            $datas = explode(' ', $transaction->tran_date);
            $times = explode(':', $datas[1]);
            $time = implode(':', array($times[0], $times[1]));
            $error_datas = $error_time = null;
            if ($transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_FAIL) {
                $error_datas = explode(' ', $transaction->error_time);
                $times = explode(':', $error_datas[1]);
                $error_time = implode(':', array($times[0], $times[1]));
            }
            return [
                'date'              =>  $datas[0],
                'time'              =>  $time,
                'payment_format'    =>  Constants::payment_status()[$transaction->payment_format],
                'maebarai_amount'   =>  $transaction->maebarai_amount,
                'transfer_amount'   =>  $transaction->transfer_amount,
                'transfer_fee'      =>  $transaction->transfer_fee,
                'api_usage_fee'     =>  $transaction->api_usage_fee,
                'status'            =>  $transaction->transaction_status,
                'error_text'        =>  Constants::payment_errors()[$transaction->error_code],
                'error_date'        =>  $transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_FAIL ? $error_datas[0] : '',
                'error_time'        =>  $transaction->transaction_status == MaebaraiHistory::TRANSACTION_STATUS_FAIL ? $error_time : '',
            ];
        });
        return $transactions;
    }

    /**
     * Search Employees of the current work location base on some given conditions.
     *
     * @param array     $conditions
     * @return Collection
     */
    protected function searchEmployeeWithConditions($conditions)
    {
        $query = Employee::query();

        $work_location = session('current_work_location');

        if (is_numeric($work_location)) {
            $query = $query->where('work_location_id', $work_location)->orderBy('view_order');
        }

        if (isset($conditions['presentation_id'])) {
            $query = $query->where('presentation_id', '=', $conditions['presentation_id']);
        }

        if (isset($conditions['name'])) {
            $query = $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["name"] . '%');
        }

        return $query->get();
    }

    /**
     * Send name list of all employees of this company to the javascript side for the autocomplete feature in the search box
     *
     * @param Request       $request
     * @return void
     */
    private function getAllEmployeeNames(Request $request)
    {
        return $request->user()->company->employees->map(function($employee) {
            return ['name' => $employee->last_name . $employee->first_name];
        });
    }
}

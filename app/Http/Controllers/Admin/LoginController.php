<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\LoginRequest;
use App\AdminUser;
use Validator;
use Caeru;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('admin.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  App\Http\Requests\Admin\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $login_result = $this->attemptLogin($request);

        if ($login_result === true) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request, $login_result);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {

        Auth::guard('admin')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return Caeru::redirect('admin_login_page');
    }


    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request     $request
     * @return true|array                  it's either true or an error message
     */
    protected function attemptLogin(Request $request)
    {
        $login_info = $request->only('username', 'password');
        $result = Auth::guard('admin')->attempt(['username' => $login_info['username'], 'password' => $login_info['password'], 'enable' => 1]);

        return $result === true ? $result : ['auth_failed' => trans('auth.failed')];
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        return Caeru::redirect('admin_change_process_file_page');
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request     $request
     * @param   string                      $error_message
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request, $error_message)
    {
        if ($request->expectsJson()) {
            return response()->json($error_message, 422);
        }

        return redirect()->back()
            ->withInput($request->only('username'))
            ->withErrors($error_message);
    }
}

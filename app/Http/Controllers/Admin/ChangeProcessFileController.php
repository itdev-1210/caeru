<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use App\AttendanceDataDownload\DataDownloadService;
use App\Http\Requests\Admin\ChangeAttendanceProcessFileRequest;
use App\Http\Requests\Admin\ChangeSummaryProcessFileRequest;


class ChangeProcessFileController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the change process file form page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showChangeForm()
    {
        return view('admin.change_process_file');
    }

    /**
     * Change the Attendance Data Download process file of a specific company.
     *
     * @param ChangeAttendanceProcessFileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function changeAttendanceFile(ChangeAttendanceProcessFileRequest $request)
    {
        $file = $request->attendance_file;

        $file_name = $request->input('attendance_company_code') . 'Attendance.php';
        $attendance_files_directory = app_path() . '/AttendanceDataDownload/Attendance/';

        $file->move($attendance_files_directory, $file_name);

        $request->session()->flash('success', '保存しました');
        $request->session()->flash('last_attendance_company_code', $request->input('attendance_company_code'));
        return back();
    }

    /**
     * Download the Attendance Data Download process file of a specific company.
     *
     * @param string        $company_code
     * @return \Illuminate\Http\Response
     */
    public function downloadAttendanceFile($company_code = null)
    {
        $exists = \DB::table('companies')->where('company_code', $company_code)->exists();
        if ($company_code && $exists) {
            $download_service = resolve(DataDownloadService::class);
            list($file_path, $already_exists) = $download_service->downloadCurrentAttendanceCalculatingFile($company_code);
            if ($already_exists) {
                return response()->download($file_path);
            } else {
                return response()->download($file_path)->deleteFileAfterSend(true);
            }

        } else {
            return response()->json(['error' => '指定会社コードは存在していません!'], 404);
        }
    }

    /**
     * Change the Summary Data Download process file of a specific company.
     *
     * @param ChangeSummaryProcessFileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function changeSummaryFile(ChangeSummaryProcessFileRequest $request)
    {
        $file = $request->summary_file;

        $file_name = $request->input('summary_company_code') . 'Summary.php';
        $summary_files_directory = app_path() . '/AttendanceDataDownload/Summary/';

        $file->move($summary_files_directory, $file_name);

        $request->session()->flash('success', '保存しました');
        $request->session()->flash('last_summary_company_code', $request->input('summary_company_code'));
        return back();
    }

    /**
     * Download the Summary Data Download process file of a specific company.
     *
     * @param string        $company_code
     * @return \Illuminate\Http\Response
     */
    public function downloadSummaryFile($company_code = null)
    {
        $exists = \DB::table('companies')->where('company_code', $company_code)->exists();
        if ($company_code && $exists) {
            $download_service = resolve(DataDownloadService::class);
            list($file_path, $already_exists) = $download_service->downloadCurrentSummaryCalculatingFile($company_code);
            if ($already_exists) {
                return response()->download($file_path);
            } else {
                return response()->download($file_path)->deleteFileAfterSend(true);
            }

        } else {
            return response()->json(['error' => '指定会社コードは存在していません!'], 404);
        }
    }
}
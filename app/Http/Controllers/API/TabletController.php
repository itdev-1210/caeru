<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Reusables\BusinessMonthForAPITrait;
use App\Jobs\SendTabletTemporaryWorkingTimestampsToConnectorServiceJob;
use DB;
use Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\WorkLocation;
use App\Setting;
use App\Employee;
use App\Company;
use App\Http\Requests\API\Tablet\APITabletLoginRequest;
use App\Http\Requests\API\Tablet\APITabletRegisterWorkLocationRequest;
use App\Http\Requests\API\Tablet\APITabletChooseWorkAddressRequest;
use App\Http\Requests\API\Tablet\APITabletCheckRequest;
use App\Http\Requests\API\Tablet\APITabletTimeStampingRequest;
use App\Http\Requests\API\Tablet\APITabletTimeTableRequest;
use App\Http\Requests\API\Tablet\APITabletGetEmployeeNameRequest;
use App\Http\Requests\API\Tablet\APITabletRegisterCardRequest;
use App\WorkAddress;
use File;

class TabletController extends Controller
{
    use BusinessMonthForAPITrait;

    /**
     * Controller of API login
     *
     * @param  App\Http\Requests\API\APITabletLoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(APITabletLoginRequest $request)
    {
        $random = str_random(20);
        if (DB::table('api_auth_tokens')->where('device_id', $request->input('tablet_id'))->where('company_code', $request->input('company_code'))->exists()) 
            DB::table('api_auth_tokens')->where('device_id', $request->input('tablet_id'))->where('company_code', $request->input('company_code'))->update(['remember_token' => Hash::make($random)]);
        else
            DB::table('api_auth_tokens')->insert(
                ['device_id'        =>  $request->input('tablet_id'),
                 'company_code'     =>  $request->input('company_code'),
                 'remember_token'   =>  Hash::make($random)
             ]);

        $this->connectToSubDatabase($request->input('company_code'));

        return response()->json([
            'company_name' => Company::first()->name,
            'authenticated_token' => $random
        ], 200);
    }

    /**
     * Connect to the sub database with this company_code.
     *
     * @param  string  $company_code
     * @return void
     */
    protected function connectToSubDatabase($company_code)
    {
        Config::set('database.default', 'sub');
        Config::set('database.connections.sub.database', config('database.connections.sub.prefix') . $company_code);
        Config::set('database.connections.sub.prefix', config('database.connections.sub.prefix') . $company_code . '_');

        DB::reconnect('sub');
    }

    /**
     * Controller of API list work location
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function listWorkLocation(Request $request)
    {
        $work_locations = WorkLocation::enable()->get()->map(function ($work_location) {
            return collect($work_location)->only(['id', 'name']);
        });

        return response()->json([
            'work_locations'        => $work_locations,
        ], 200);
    }

    /**
     * Controller of API register work location
     *
     * @param  App\Http\Requests\API\APITabletRegisterWorkLocationRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function registerWorkLocation(APITabletRegisterWorkLocationRequest $request)
    {
        $work_location = WorkLocation::find($request->work_location_id);
        $work_addresses =  $work_location->workAddresses->map(function ($work_address) {
            return collect($work_address)->only(['id', 'name']);
        });
        return response()->json([
            'use_work_address'      => ($work_location->company->use_address_system) ? 1 : 0,
            'work_addresses'        => $work_addresses,
            'work_location_name'    => $work_location->name,
            'work_location_id'      => $work_location->id,
            'work_location_address' => $work_location->address,
            'lat'                   => $work_location->latitude,
            'lon'                   => $work_location->longitude,
            'radius'                => $work_location->login_range,
            'go_out_button'         => ($work_location->currentSetting()->go_out_button_usage == Setting::NOT_USE_GO_OUT_BUTTON) ? 0 : 1,
            'over_time_button'      => ($work_location->currentSetting()->use_overtime_button) ? 0 : 0,
        ], 200);
    }

    /**
     * Controller of API choose work address
     *
     * @param  App\Http\Requests\API\APITabletChooseWorkAddressRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function chooseWorkAddress(APITabletChooseWorkAddressRequest $request)
    {
        $work_address = WorkAddress::find($request->work_address_id);

        return response()->json([
            'work_address_name'     => $work_address->name,
            'work_address_id'       => $work_address->id,
            'work_address_address'  => $work_address->address,
            'lat'                   => $work_address->latitude,
            'lon'                   => $work_address->longitude,
            'radius'                => $work_address->login_range,
        ], 200);
    }

    /**
     * Controller of API check
     *
     * @param  App\Http\Requests\API\APITabletCheckRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function check(APITabletCheckRequest $request)
    {
        $work_location = WorkLocation::find($request->work_location_id);
        $work_address = ($work_location->company->use_address_system && isset($request->work_address_id) && !empty($request->work_address_id))
                        ? $work_location->workAddresses->where('id', $request->work_address_id)->first() : null;

        return response()->json([
            'company_name'          => Company::first()->name,
            'work_location_name'    => $work_location->name,
            'work_location_id'      => $work_location->id,
            'work_location_address' => $work_location->address,
            'work_location_lat'     => $work_location->latitude,
            'work_location_lon'     => $work_location->longitude,
            'work_location_radius'  => $work_location->login_range,
            'go_out_button'         => ($work_location->currentSetting()->go_out_button_usage == Setting::NOT_USE_GO_OUT_BUTTON) ? 0 : 1,
            'over_time_button'      => ($work_location->currentSetting()->use_overtime_button) ? 0 : 0,
            'work_address_id'       => isset($work_address) ? $work_address->id : null,
            'work_address_name'     => isset($work_address) ? $work_address->name : null,
            'work_address_address'  => isset($work_address) ? $work_address->address : null,
            'work_address_lat'      => isset($work_address) ? $work_address->latitude : null,
            'work_address_lon'      => isset($work_address) ? $work_address->longitude : null,
            'work_address_radius'   => isset($work_address) ? $work_address->login_range : null,
        ], 200);
    }

    /**
     *  Controller of API Time stamping
     *
     * @param  App\Http\Requests\API\Tablet\APITabletTimeStampingRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function timeStamping(APITabletTimeStampingRequest $request)
    {
        // UPDATED: This part has been replaced to use the new service
        ///////////////////////////////////////////////////////////////
        // $employee_working_day = $this->getEmployeeWorkingDayAvailable($request->type, $employee, $request->work_location_id);

        // $work_timestamps = new WorkingTimestamp;
        // if ($employee_working_day->isConcluded())
        //     $work_timestamps->enable = false;
        // else
        //     $work_timestamps->enable = true;
        // $work_timestamps->timestamped_value = Carbon::now()->timestamp;
        // $work_timestamps->name_id = WorkLocation::find($request->work_location_id)->getTimezone()->name_id;
        // $work_timestamps->timestamped_type = $request->type;
        // $work_timestamps->registerer_type = WorkingTimestamp::TABLET;
        // $work_timestamps->work_location_id = $request->work_location_id;
        // if (isset($request->work_address_id) && !empty($request->work_address_id))
        //     $work_timestamps->work_address_id = $request->work_address_id;
        // $work_timestamps->employee_working_day_id = $employee_working_day->id;
        // $work_timestamps->save();

        // event(new WorkingTimestampChanged($employee_working_day));
        ////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////
        // Code before 2018-12-21. newer than the above, older than the below

        // $employee = Employee::where('card_number', $request->card_code)->first();

        // $work_location_id = $request->input('work_location_id');

        // if ($employee->all_timestamps_register_to_belong_work_location && empty($request->input('work_address_id')))
        //     $work_location_id = $employee->work_location_id;

        // // To use the new service, create new timestamp
        // $new_timestamp = new WorkingTimestamp([
        //     'enable'                => true,
        //     'timestamped_value'     => Carbon::now()->timestamp,
        //     'name_id'               => WorkLocation::find($work_location_id)->getTimezone()->name_id,
        //     'timestamped_type'      => $request->input('type'),
        //     'registerer_type'       => WorkingTimestamp::TABLET,
        //     'work_location_id'      => $work_location_id,
        // ]);

        // if (!empty($request->input('work_address_id'))) {
        //     $new_timestamp->work_address_id = $request->input('work_address_id');
        // }

        // // Then resolve the service and call the function
        // $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);
        // $connector_service->connectTimestampsToWorkingDaysOfEmployee([$new_timestamp], $employee);

        // Code before 2018-12-21. newer than the above, older than the below
        ////////////////////////////////////////////////////////////////

        $request_key = $request->input('company_code') . '_' . $request->input('work_location_id') . '_' . $request->input('work_address_id') . '_' .
            $request->input('tablet_id') . '_' . (Carbon::now())->timestamp;

        DB::table('tablet_temporary_working_timestamps')->insert(
            [
                'company_code' => $request->input('company_code'),
                'device_id' => $request->input('tablet_id'),
                'work_location_id' => $request->input('work_location_id'),
                'work_address_id' => $request->input('work_address_id'),
                'timestamped_value' => (Carbon::now())->timestamp,
                'card_code' => $request->input('card_code'),
                'timestamped_type' => $request->input('type'),
                'name_id' => null,
                'request_key' => $request_key,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );

        $new_job = (new SendTabletTemporaryWorkingTimestampsToConnectorServiceJob(
            $request_key,
            $request->input('company_code'),
            $request->input('work_location_id'),
            $request->input('work_address_id')
        ))->onConnection('database')->onQueue('TemporaryWorkingTimestamps');

        dispatch($new_job);

        // Return the employee's full name for the tablet to display.
        $employee = Employee::where('card_number', $request->card_code)->first();

        return response()->json([
            'employee_name'    => $employee->last_name . " " .$employee->first_name,
        ], 200);
    }

    /**
     * Controller of API Time table
     *
     * @param  App\Http\Requests\API\APITabletTimeTableRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function timeTable(APITabletTimeTableRequest $request)
    {
        $employee = Employee::where('card_number', $request->input('card_code'))->first();

        $business_month = $request->input('business_month');

        $data = $this->getDataAccordingBusinessMonthForAPI($employee, $business_month);

        return response()->json($data, 200);
    }

    /**
     * Controller of API get employee name
     *
     * @param  App\Http\Requests\API\Tablet\APITabletGetEmployeeNameRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function getEmployeeName(APITabletGetEmployeeNameRequest $request)
    {
        $employee = Employee::where('card_registration_number', $request->card_registration_number)->first();
        return response()->json([
            'employee_id'       => $employee->id ,
            'employee_name'     => $employee->last_name . " " .$employee->first_name ,
        ], 200);
    }

    /**
     * Controller of API register card for employee
     *
     * @param  App\Http\Requests\API\Tablet\APITabletRegisterCardRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function registerCard(APITabletRegisterCardRequest $request)
    {
        $employee = Employee::find($request->employee_id);
        $employee->card_number = $request->card_code;
        $employee->save();
        return response()->json([
            'sucesses'      => 'sucesses'
        ], 200);
    }

    /**
     * Controller of API check version
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function checkVersion(Request $request)
    {
        $version = File::get(public_path("file/version/tablet/real_server/version.txt"));

        if ($request->input('app_test') == "true")
            $version = File::get(public_path("file/version/tablet/test_server/version.txt"));

        return response()->json([
            'version'    => $version,
        ], 200);
    }

    /**
     * Controller of API download file apk
     *
     * @param  App\Http\Requests  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function downloadInstallerForTablet(Request $request)
    {
        $pathToFile = null;

        if ($request->input('app_test') == "true") {
            $pathToFile = public_path("file/apk/tablet/test_server/caeru_tablet_pasori.apk");
        } else {
            $pathToFile = public_path("file/apk/tablet/real_server/caeru_tablet_pasori.apk");
        }

        return response()->file($pathToFile ,[
            'Content-Type'=>'application/vnd.android.package-archive',
            'Content-Disposition'=> 'attachment; filename="android.apk"',
        ]) ;
    }
}

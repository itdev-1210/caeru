<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Carbon\Carbon;
use App\MaebaraiHistory;
use Constants;

class MaebaraiTransactionController extends Controller
{
    /**
     * Update Maebarai Transaction
     * @param Illuminate\Http\Request $request
     */
    public function updateTransaction(Request $request)
    {
        $company_code = $request->company_code;
        DB::purge('sub');

        Config::set('database.default', 'sub');

        Config::set('database.connections.sub.database', config('database.connections.sub.default_prefix') . $company_code);
        Config::set('database.connections.sub.prefix', config('database.connections.sub.default_prefix') . $company_code . '_');

        DB::reconnect('sub');

        $id = $requet->input('transaction_id');
        $maebaraiHistory = MaebaraiHistory::where('transaction_id', $id)->get();
        $maebaraiHistory->transaction_status = MaebaraiHistory::TRANSACTION_STATUS_FAIL;
        $maebaraiHistory->error_time = Carbon::today();
        // update after discussing
        $maebaraiHistory->save();
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\API\JupiterTablet\APIJupiterLoginRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterRegisterWorkLocationRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterChooseWorkAddressRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterCheckRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterCheckCardRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterGetEmployeeNameRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterRegisterCardRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterTimeStampingRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterTimeTableRequest;
use App\Http\Requests\API\JupiterTablet\APIJupiterOfflineDataRequest;
use Illuminate\Support\Facades\Hash;
use App\Events\WorkingTimestampChanged;
use App\Services\TimestampesAndWorkingDayConnectorService;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;
use App\Jobs\SendTabletTemporaryWorkingTimestampsToConnectorServiceJob;
use Carbon\Carbon;
use App\WorkLocation;
use App\Setting;
use App\Employee;
use App\WorkingTimestamp;
use App\Company;
use App\EmployeeWorkingDay;
use App\TabletTemporaryWorkingTimestamp;
use Illuminate\Support\Facades\Log;
use Constants;
use Config;
use DB;
use File;

class JupiterTabletController extends Controller
{
    use CanCreateWorkingDayOnTheFlyTrait;

    /**
     * Controller of API connection
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function connection(Request $request)
    {
        return response()->json([
            'sucesses'      => 'sucesses'
        ], 200);
    }

    /**
     * Controller of API login
     *
     * @param  App\Http\Requests\API\APIJupiterLoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(APIJupiterLoginRequest $request)
    {
        $random = str_random(20);
        if (DB::table('api_auth_tokens')->where('device_id', $request->input('tablet_id'))->where('company_code', $request->input('company_code'))->exists()) 
            DB::table('api_auth_tokens')->where('device_id', $request->input('tablet_id'))->where('company_code', $request->input('company_code'))->update(['remember_token' => Hash::make($random)]);
        else
            DB::table('api_auth_tokens')->insert(
                ['device_id'        =>  $request->input('tablet_id'),
                 'company_code'     =>  $request->input('company_code'),
                 'remember_token'   =>  Hash::make($random)
             ]);
        return response()->json([
            'company_name' => '会社',
            'authenticated_token' => $random
        ], 200);
    }

    /**
     * Controller of API register work location
     *
     * @param  App\Http\Requests\API\APIJupiterRegisterWorkLocationRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function registerWorkLocation(APIJupiterRegisterWorkLocationRequest $request)
    {
        $work_location = WorkLocation::where('registration_number', $request->work_location_number)->first();
        $work_addresses =  $work_location->workAddresses->map(function ($work_address) {
            return collect($work_address)->only(['id', 'name']);
        });
        return response()->json([
            'use_work_address'      => ($work_location->company->use_address_system) ? true : false,
            'work_addresses'        => $work_addresses,
            'work_location_name'    => $work_location->name,
            'work_location_id'      => $work_location->id,
            'time_zone'             => $work_location->getTimezone()->name_id,
            'go_out_button'         => ($work_location->currentSetting()->go_out_button_usage == Setting::NOT_USE_GO_OUT_BUTTON) ? false : true,
            'over_time_button'      => ($work_location->currentSetting()->use_overtime_button) ? false : false,
            'bentou_button'         => false,
        ], 200);
    }

    /**
     * Controller of API choose work address
     *
     * @param  App\Http\Requests\API\APIJupiterChooseWorkAddressRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function chooseWorkAddress(APIJupiterChooseWorkAddressRequest $request)
    {
        return response()->json([
            'sucesses'      => 'sucesses'
        ], 200);
    }

    /**
     * Controller of API check
     *
     * @param  App\Http\Requests\API\APIJupiterCheckRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function check(APIJupiterCheckRequest $request)
    {
        $work_location = WorkLocation::find($request->work_location_id);

        $version = File::get(public_path("file/version/jupiter/real_server/version.txt"));

        if ($request->input('app_test') == "true")
            $version = File::get(public_path("file/version/jupiter/test_server/version.txt"));

        return response()->json([
            'work_location_name'    => $work_location->name,
            'work_location_id'      => $work_location->id,
            'time_zone'             => $work_location->getTimezone()->name_id,
            'go_out_button'         => ($work_location->currentSetting()->go_out_button_usage == Setting::NOT_USE_GO_OUT_BUTTON) ? false : true,
            'over_time_button'      => ($work_location->currentSetting()->use_overtime_button) ? false : false,
            'bentou_button'         => false,
            'expected_time'         => Carbon::now()->timestamp,
            'version'               => $version,
            'use_work_address'      => ($work_location->company->use_address_system) ? true : false,
            'work_address_id'       => ($work_location->company->use_address_system && isset($request->work_address_id) && !empty($request->work_address_id)) ?  $work_location->workAddresses->where('id', $request->work_address_id)->first()->id : null,
            'work_address_name'       => ($work_location->company->use_address_system && isset($request->work_address_id) && !empty($request->work_address_id)) ?  $work_location->workAddresses->where('id', $request->work_address_id)->first()->name : null,
        ], 200);
    }

    /**
     * The API would like to checking for new tablet
     *
     * @param  App\Http\Requests\API\APIJupiterCheckRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function checkForNewTablet(APIJupiterCheckRequest $request)
    {
        $work_location = WorkLocation::find($request->work_location_id);

        $version = File::get(public_path("file/version/new_jupiter/real_server/version.txt"));

        if ($request->input('app_test') == "true")
            $version = File::get(public_path("file/version/new_jupiter/test_server/version.txt"));

        return response()->json([
            'work_location_name'    => $work_location->name,
            'work_location_id'      => $work_location->id,
            'time_zone'             => $work_location->getTimezone()->name_id,
            'go_out_button'         => ($work_location->currentSetting()->go_out_button_usage == Setting::NOT_USE_GO_OUT_BUTTON) ? false : true,
            'over_time_button'      => ($work_location->currentSetting()->use_overtime_button) ? false : false,
            'bentou_button'         => false,
            'expected_time'         => Carbon::now()->timestamp,
            'version'               => $version,
            'use_work_address'      => ($work_location->company->use_address_system) ? true : false,
            'work_address_id'       => ($work_location->company->use_address_system && isset($request->work_address_id) && !empty($request->work_address_id)) ?  $work_location->workAddresses->where('id', $request->work_address_id)->first()->id : null,
            'work_address_name'       => ($work_location->company->use_address_system && isset($request->work_address_id) && !empty($request->work_address_id)) ?  $work_location->workAddresses->where('id', $request->work_address_id)->first()->name : null,
        ], 200);
    }

    /**
     *  Controller of API Time stamping
     *
     * @param  App\Http\Requests\API\APIJupiterTimeStampingRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function timeStamping(APIJupiterTimeStampingRequest $request)
    {
        // UPDATED: This part has been replaced to use the new service
        ///////////////////////////////////////////////////////////////
        // $employee_working_day = $this->getEmployeeWorkingDayAvailable($request->type, $employee, $request->work_location_id);

        // $work_timestamps = new WorkingTimestamp;
        // if ($employee_working_day->isConcluded())
        //     $work_timestamps->enable = false;
        // else
        //     $work_timestamps->enable = true;
        // $work_timestamps->timestamped_value = Carbon::now()->timestamp;
        // $work_timestamps->name_id = WorkLocation::find($request->work_location_id)->getTimezone()->name_id;
        // $work_timestamps->timestamped_type = $request->type;
        // $work_timestamps->registerer_type = WorkingTimestamp::TABLET;
        // $work_timestamps->work_location_id = $request->work_location_id;
        // if (isset($request->work_address_id) && !empty($request->work_address_id))
        //     $work_timestamps->work_address_id = $request->work_address_id;
        // $work_timestamps->employee_working_day_id = $employee_working_day->id;
        // $work_timestamps->save();

        // event(new WorkingTimestampChanged($employee_working_day));
        ////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////
        // Code before 2018-12-21. newer than the above, older than the below

        // $employee = Employee::where('card_number', $request->card_code)->first();

        // $work_location_id = $request->input('work_location_id');

        // if ($employee->all_timestamps_register_to_belong_work_location && empty($request->input('work_address_id')))
        //     $work_location_id = $employee->work_location_id;

        // // To use the new service, create new timestamp
        // $new_timestamp = new WorkingTimestamp([
        //     'enable'                => true,
        //     'timestamped_value'     => Carbon::now()->timestamp,
        //     'name_id'               => WorkLocation::find($work_location_id)->getTimezone()->name_id,
        //     'timestamped_type'      => $request->input('type'),
        //     'registerer_type'       => WorkingTimestamp::TABLET,
        //     'work_location_id'      => $work_location_id,
        // ]);

        // if (!empty($request->input('work_address_id'))) {
        //     $new_timestamp->work_address_id = $request->input('work_address_id');
        // }

        // // Then resolve the service and call the function
        // $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);
        // $connector_service->connectTimestampsToWorkingDaysOfEmployee([$new_timestamp], $employee);

        // Code before 2018-12-21. newer than the above, older than the below
        ////////////////////////////////////////////////////////////////

        $request_key = $request->input('company_code') . '_' . $request->input('work_location_id') . '_' . $request->input('work_address_id') . '_' .
            $request->input('tablet_id') . '_' . (Carbon::now())->timestamp;

        DB::table('tablet_temporary_working_timestamps')->insert(
            [
                'company_code' => $request->input('company_code'),
                'device_id' => $request->input('tablet_id'),
                'work_location_id' => $request->input('work_location_id'),
                'work_address_id' => $request->input('work_address_id'),
                'timestamped_value' => (Carbon::now())->timestamp,
                'card_code' => $request->input('card_code'),
                'timestamped_type' => $request->input('type'),
                'name_id' => null,
                'request_key' => $request_key,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );

        $new_job = (new SendTabletTemporaryWorkingTimestampsToConnectorServiceJob(
            $request_key,
            $request->input('company_code'),
            $request->input('work_location_id'),
            $request->input('work_address_id')
        ))->onConnection('database')->onQueue('TemporaryWorkingTimestamps');

        dispatch($new_job);

        // Return the employee's full name for the tablet to display.
        $employee = Employee::where('card_number', $request->card_code)->first();

        return response()->json([
            'employee_name'    => $employee->last_name . " " .$employee->first_name,
        ], 200);
    }

    /**
     * Controller of API Time table
     *
     * @param  App\Http\Requests\API\APIJupiterTimeTableRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function timeTable(APIJupiterTimeTableRequest $request)
    {
        $work_location = WorkLocation::find($request->work_location_id);
        $start_day = $this->getStartDay(Carbon::createFromFormat('Y-m-d', $request->date), ($work_location->currentSetting()->start_day_of_week != Config::get('constants.sunday')) ? $work_location->currentSetting()->start_day_of_week : 0);
        return response()->json([
            'data'    =>$this->getTimestampWithWeek($start_day, $request->card_code),
        ],  200);
    }

    /**
     * Controller of API office data
     *
     * @param  App\Http\Requests\API\APIJupiterOfflineDataRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function offlineData(APIJupiterOfflineDataRequest $request)
    {
        // UPDATED: This part has been replaced to use the new service
        ///////////////////////////////////////////////////////////////

        // $work_location = WorkLocation::find($request->work_location_id);
        // foreach ($request->data as $value) {
        //     $datetime = Carbon::createFromTimestamp($value["timestamp"]);
        //     $time_setting_company = Carbon::createFromTimestamp($value["timestamp"])
        //                             ->hour(Carbon::createFromFormat('H:i', $work_location->company->date_separate_time)->hour)
        //                             ->minute(Carbon::createFromFormat('H:i', $work_location->company->date_separate_time)->minute);
        //     $employee = Employee::where('card_number', $value["card_code"])->first();
        //     $employee_working_day = $this->getEmployeeWorkingDayAvailable($value["type"], $employee, $request->work_location_id, $datetime, $time_setting_company, $value["timestamp"]);

        //     $work_timestamps = new WorkingTimestamp;
        //     if ($employee_working_day->isConcluded())
        //         $work_timestamps->enable = false;
        //     else
        //         $work_timestamps->enable = true;
        //     $work_timestamps->timestamped_value =$value["timestamp"];
        //     $work_timestamps->name_id = $work_location->getTimezone()->name_id;
        //     $work_timestamps->timestamped_type = $value["type"];
        //     $work_timestamps->registerer_type = WorkingTimestamp::TABLET;
        //     $work_timestamps->work_location_id = $request->work_location_id;
        //     if (isset($request->work_address_id) && !empty($request->work_address_id))
        //         $work_timestamps->work_address_id = $request->work_address_id;
        //     $work_timestamps->employee_working_day_id = $employee_working_day->id;
        //     $work_timestamps->save();

        //     event(new WorkingTimestampChanged($employee_working_day));
        // }
        ////////////////////////////////////////////////////////////////

        // if (count($request->data) != 0) {
        //     $requested_timestamps = collect($request->data);

        //     $requested_timestamps = $requested_timestamps->sortBy('card_code');

        //     $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);
        //     $current_employee = Employee::where('card_number', $requested_timestamps->first()["card_code"])->first();

        //     $new_timestamps = [];
        //     foreach ($requested_timestamps as $value) {

        //         if ($value['card_code'] != $current_employee->card_number) {
        //             $connector_service->connectTimestampsToWorkingDaysOfEmployee($new_timestamps, $current_employee);
        //             $new_timestamps = [];
        //             $current_employee = Employee::where('card_number', $value["card_code"])->first();
        //         }

        //         $work_location_id = $request->input('work_location_id');

        //         if ($current_employee->all_timestamps_register_to_belong_work_location && empty($request->input('work_address_id')))
        //             $work_location_id = $current_employee->work_location_id;

        //         $new_timestamp = new WorkingTimestamp([
        //             'enable'                => true,
        //             'timestamped_value'     => $value["timestamp"],
        //             'name_id'               => WorkLocation::find($work_location_id)->getTimezone()->name_id,
        //             'timestamped_type'      => $value["type"],
        //             'registerer_type'       => WorkingTimestamp::TABLET,
        //             'work_location_id'      => $work_location_id,
        //         ]);

        //         if (!empty($request->input('work_address_id'))) {
        //             $new_timestamp->work_address_id = $request->input('work_address_id');
        //         }

        //         $new_timestamps[] = $new_timestamp;
        //     }

        //     // Need to do it for the last guy
        //     $connector_service->connectTimestampsToWorkingDaysOfEmployee($new_timestamps, $current_employee);
        // }

        if (count($request->data) != 0) {
            $total_data = $request->data;

            // This is for the case when the offline data set is too big: split them into smaller chunk of 200
            $data_in_chunks = array_chunk($total_data, 200);

            foreach ($data_in_chunks as $one_chunk_data) {
                $request_key = $request->input('company_code') . '_' . $request->input('work_location_id') . '_' . $request->input('work_address_id') . '_' .
                    $request->input('tablet_id') . '_' . (Carbon::now())->timestamp;

                $new_temporary_timestamps = [];

                foreach ($one_chunk_data as $timestamp) {
                    $new_temporary_timestamps[] = [
                        'company_code' => $request->input('company_code'),
                        'device_id' => $request->input('tablet_id'),
                        'work_location_id' => $request->input('work_location_id'),
                        'work_address_id' => $request->input('work_address_id'),
                        'timestamped_value' => $timestamp['timestamp'],
                        'card_code' => $timestamp['card_code'],
                        'timestamped_type' => $timestamp['type'],
                        'name_id' => null,
                        'request_key' => $request_key,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                }

                try {
                    DB::table('tablet_temporary_working_timestamps')->insert($new_temporary_timestamps);

                } catch (\Exception $e) {
                    $new_temporary_timestamps_as_string = [];
                    foreach($new_temporary_timestamps as $temporary_timestamp) {
                        $new_temporary_timestamps_as_string[] = '(' . implode(', ', $temporary_timestamp) . ')';
                    }
                    $total_string = implode(",\n", $new_temporary_timestamps_as_string);
                    Log::error("||++++++++++");
                    Log::error("Can not insert all the offline data. The data set that made the exception: \n" . $total_string);
                    Log::error("++++++++++||");
                    exit();
                }

                $new_job = (new SendTabletTemporaryWorkingTimestampsToConnectorServiceJob(
                    $request_key,
                    $request->input('company_code'),
                    $request->input('work_location_id'),
                    $request->input('work_address_id')
                ))->onConnection('database')->onQueue('TemporaryWorkingTimestamps');

                dispatch($new_job);

                // We need to sleep one second here to ensure the request key is different for each chunk of data
                sleep(1);
            }

        }

        return response()->json([
            'sucesses'      => 'sucesses'
        ], 200);
    }

    /**
     * Controller of API check card
     *
     * @param  App\Http\Requests\API\APIJupiterCheckCardRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function checkCard(APIJupiterCheckCardRequest $request)
    {
        return response()->json([
            'usable'    => (Employee::where('card_number', $request->card_code)->exists()) ? false : true ,
        ], 200);
    }

    /**
     * Controller of API get employee name
     *
     * @param  App\Http\Requests\API\APIJupiterGetEmployeeNameRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function getEmployeeName(APIJupiterGetEmployeeNameRequest $request)
    {
        $employee_presentation_id = Employee::where('card_registration_number', $request->employee_presentation_id)->first();
        return response()->json([
            'employee_id'       => $employee_presentation_id->id ,
            'employee_name'     => $employee_presentation_id->last_name . " " .$employee_presentation_id->first_name ,
        ], 200);
    }

    /**
     * Controller of API register card for employee
     *
     * @param  App\Http\Requests\API\APIJupiterRegisterCardRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function registerCard(APIJupiterRegisterCardRequest $request)
    {
        $employee = Employee::find($request->employee_id);
        $employee->card_number = $request->card_code;
        $employee->save();
        return response()->json([
            'sucesses'      => 'sucesses'
        ], 200);
    }

    /**
     * Controller of API download file apk
     *
     * @param  App\Http\Requests  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function downloadInstaller(Request $request)
    {
        $pathToFile = null;

        if ($request->input('app_test') == "true") {
            $pathToFile = public_path("file/apk/jupiter/test_server/caeru_tablet_apk.apk");
        } else {
            $pathToFile = public_path("file/apk/jupiter/real_server/caeru_tablet_apk.apk");
        }

        return response()->file($pathToFile ,[
            'Content-Type'=>'application/vnd.android.package-archive',
            'Content-Disposition'=> 'attachment; filename="android.apk"',
        ]) ;
    }

    /**
     * Controller of API download file apk
     *
     * @param  App\Http\Requests  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function downloadInstallerForNewTablet(Request $request)
    {
        $pathToFile = null;

        if ($request->input('app_test') == "true") {
            $pathToFile = public_path("file/apk/new_jupiter/test_server/caeru_tablet_apk.apk");
        } else {
            $pathToFile = public_path("file/apk/new_jupiter/real_server/caeru_tablet_apk.apk");
        }

        return response()->file($pathToFile ,[
            'Content-Type'=>'application/vnd.android.package-archive',
            'Content-Disposition'=> 'attachment; filename="android.apk"',
        ]) ;
    }

    /**
     * The function search and get in employee working day
     *
     * @param  timestamped_type(integer)  $type
     * @param  app/Employee  $employee
     * @param  work_location(integer)  $work_location_id
     * @return app/EmployyeWorkingDay $employee_working_day
     */
    private function getEmployeeWorkingDayAvailable($type, $employee, $work_location_id, $datetime = null, $time_setting_company = null, $timestamp_working = null)
    {
        $work_location = WorkLocation::find($work_location_id);
        if (is_null($datetime)) {
            $datetime = Carbon::now();
            $datetime->second = 0;
        }
        if (is_null($time_setting_company))
            $time_setting_company = Carbon::createFromFormat('H:i',$work_location->company->date_separate_time);

        $employee_working_day = $this->getEmployeeWorkingDayToday($work_location, $datetime, $time_setting_company, $employee);

        if ($type == WorkingTimestamp::END_WORK || $type == WorkingTimestamp::GO_OUT)
            $employee_working_day = $this->sreachDataWorkingTimestampForEndWorkAndGoOut($work_location, $employee_working_day, $timestamp_working);
        elseif ($type == WorkingTimestamp::RETURN)
            $employee_working_day = $this->sreachDataWorkingTimestampForComeBack($work_location, $employee_working_day, $timestamp_working);

        return $employee_working_day;
    }

    /**
     * The function get the today's employee working day
     *
     * @param  app/WorkLocation  $work_location
     * @param  Carbon  $datetime
     * @param  app/Setting(Carbon)  $time_setting_company
     * @param  app/Employee  $employee
     * @return app/EmployyeWorkingDay $employee_working_day
     */
    private function getEmployeeWorkingDayToday($work_location, $datetime, $time_setting_company, $employee)
    {
        if ($work_location->company->date_separate_type == Company::APPLY_TO_THE_DAY_BEFORE) {
            if ($datetime->gte($time_setting_company))
                $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $datetime->toDateString())->first();
            else
                $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $datetime->subDay()->toDateString())->first();

        }elseif ($work_location->company->date_separate_type == Company::APPLY_TO_THE_DAY_AFTER) {
            if ($datetime->gte($time_setting_company))
                $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $datetime->addDay()->toDateString())->first();
            else
                $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $datetime->toDateString())->first();

        }
        if (!$employee_working_day)
            $employee_working_day = $this->createEmployeeWorkingDayOnTheFly($employee->id, $datetime->toDateString());

        return $employee_working_day;
    }

    /**
     * Validate the date string.
     *
     * @param string    $date
     * @param string    $format
     * @return boolean
     */
    protected function validateDateByFormat($date, $format = 'Y-m-d')
    {
        $result = Carbon::createFromFormat($format, $date);
        return $result && ($result->format($format) == $date);
    }

    /**
     * Search all available in working timestamp for type start work and end work
     * After return the trust employee working day
     *
     * @param App/EmployeeWorkingDay       $employee_working_day
     * @return App/EmployeeWorkingDay
     */
    protected function sreachDataWorkingTimestampForEndWorkAndGoOut($work_location, $employee_working_day, $timestamp_working = null)
    {
        for ($i=0; $i <= 2; $i++) {
            $set_employee_working_day = EmployeeWorkingDay::where('employee_id', $employee_working_day->employee_id)->where('date', Carbon::createFromFormat('Y-m-d', $employee_working_day->date)->subDays($i)->toDateString())->first();

            if (!empty($set_employee_working_day) && $set_employee_working_day->workingTimestamps()->enable()->startwork()->exists()) {

                if ($i == 0) {
                    if (!is_null($timestamp_working)) {
                        if ($set_employee_working_day->workingTimestamps()->enable()->startwork()->get()->where('raw_date_time_value', '<=', Carbon::createFromTimestamp($timestamp_working)->setTimeZone($work_location->getTimezone()->name_id)->toDateTimeString())->isNotEmpty())
                            return $set_employee_working_day;
                    } else
                        return $set_employee_working_day;
                }else
                    if ($set_employee_working_day->workingTimestamps()->enable()
                    ->whereIn('timestamped_type', [WorkingTimestamp::START_WORK, WorkingTimestamp::END_WORK])->get()
                    ->sortByDesc('raw_date_time_value')
                    ->first()->timestamped_type == WorkingTimestamp::START_WORK)
                        return $set_employee_working_day;
            }

        }
        return $employee_working_day;
    }

    /**
     * Search all available in working timestamp for type go out and go back
     * After return the trust employee working day
     *
     * @param App/EmployeeWorkingDay       $employee_working_day
     * @return App/EmployeeWorkingDay
     */
    protected function sreachDataWorkingTimestampForComeBack($work_location, $employee_working_day, $timestamp_working = null)
    {
        for ($i=0; $i <= 2; $i++) {
            $set_employee_working_day = EmployeeWorkingDay::where('employee_id', $employee_working_day->employee_id)->where('date', Carbon::createFromFormat('Y-m-d', $employee_working_day->date)->subDays($i)->toDateString())->first();

            if (!empty($set_employee_working_day) && $set_employee_working_day->workingTimestamps()->enable()->startwork()->exists()) {

                if ($i == 0) {
                    if (!is_null($timestamp_working)) {
                        if ($set_employee_working_day->workingTimestamps()->enable()->startwork()->get()->where('raw_date_time_value', '<=', Carbon::createFromTimestamp($timestamp_working)->setTimeZone($work_location->getTimezone()->name_id)->toDateTimeString())->isNotEmpty())
                            return $set_employee_working_day;
                    } else
                        return $set_employee_working_day;
                } else
                    if ($set_employee_working_day->workingTimestamps()->enable()
                        ->whereIn('timestamped_type', [WorkingTimestamp::GO_OUT, WorkingTimestamp::RETURN])->get()
                        ->sortByDesc('raw_date_time_value')
                        ->first()->timestamped_type == WorkingTimestamp::GO_OUT)
                        return $set_employee_working_day;
            }
        }
        return $employee_working_day;
    }

    /**
     * Get all time stamp for the week
     * Format: date, start_time, end_time, arrived_early, arrived_late, go_out_time
     *
     * @param Carbon      $start_date
     * @param integer      $card_code
     * @return array($time_stamp)
     */
    protected function getTimestampWithWeek($start_date, $card_code)
    {
        $week_array = array();
        $employee = Employee::where('card_number', $card_code)->first();
        $end_date = (new Carbon($start_date))->addDay(count(Constants::dayOfTheWeek())-1);

        for($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $day_array = array();
            $day_array['date'] = $date->toDateString();
            $day_array['start_time'] = $day_array['end_time'] = null;
            $day_array['arrived_early'] = $day_array['arrived_late'] = false;
            $day_array['go_out_time'] = 0;

            $employeeWorkingDay = $employee->employeeWorkingDays->where('date', $date->toDateString())->first();
            if (!empty($employeeWorkingDay)) {

                $start_time = $employeeWorkingDay->workingTimestamps()->enable()->startwork()->get()->sortBy('raw_date_time_value')->first();
                $end_time = $employeeWorkingDay->workingTimestamps()->enable()->endwork()->get()->sortBy('raw_date_time_value')->first();

                if (!empty($start_time)) {
                    $day_array['start_time'] = $start_time->processed_time_value;
                    $get_all_go_out = $employeeWorkingDay->workingTimestamps()->enable()
                        ->whereIn('timestamped_type', [WorkingTimestamp::GO_OUT, WorkingTimestamp::RETURN])->get()
                        ->where('raw_date_time_value', '>=', $start_time->raw_date_time_value)->sortBy('raw_date_time_value');

                    $end_time = $employeeWorkingDay->workingTimestamps()->enable()->endwork()->get()->where('raw_date_time_value', '>=', $start_time->raw_date_time_value)->sortBy('raw_date_time_value')->first();

                    if (!empty($end_time))
                        $get_all_go_out = $get_all_go_out->where('raw_date_time_value', '<=', $end_time->raw_date_time_value);

                    $day_array['go_out_time'] = $this->getGoOutTime($get_all_go_out, $day_array['go_out_time'], WorkLocation::find($start_time->work_location_id)->currentSetting()->break_time_round_up);
                }

                if (!empty($end_time))
                    $day_array['end_time'] = $end_time->processed_time_value;
            }

            array_push($week_array,$day_array);
        }

        return $week_array;
    }

    /**
     * Find start week from $date
     *
     * @param Carbon      $date
     * @param integer      $target
     * @return Carbon $date
     */
    protected function getStartDay($date, $target)
    {
        while ($date->dayOfWeek != $target) {
            $date->subDay();
        }
        return $date;
    }

    /**
     * Count all go out time on that day
     *
     * @param App/WorkingTimestamp      $get_all_go_out
     * @param integer      $sum
     * @return integer $sum
     */
    protected function getGoOutTime($get_all_go_out, $sum, $round_up)
    {
        $check = WorkingTimestamp::RETURN;
        $array = array();

        foreach ($get_all_go_out as $get_go_out) {
            ($get_go_out->timestamped_type == $check) ? array_push($array, $get_go_out->id) : $check = $get_go_out->timestamped_type;
        }

        $filtered = $get_all_go_out->whereNotIn('id', $array)->pluck('id');

        for ($i=1; $i < $filtered->count(); $i+=2) {
            $return = WorkingTimestamp::find($filtered[$i]);
            $go_out = WorkingTimestamp::find($filtered[$i-1]);
            $sum += Carbon::createFromFormat('Y-m-d H:i:s', $return->raw_date_time_value)->diffInMinutes(Carbon::createFromFormat('Y-m-d H:i:s', $go_out->raw_date_time_value));
        }

        return (ceil($sum/$round_up) * $round_up);
    }
}

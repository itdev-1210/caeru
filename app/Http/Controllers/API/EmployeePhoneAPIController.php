<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Coordinate;
use DB;
use App\Employee;
use App\Company;
use App\WorkAddress;
use App\WorkLocation;
use App\WorkingTimestamp;
use App\EmployeeWorkingDay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\API\Phone\APIEmployeePhoneLoginRequest;
use App\Http\Requests\API\Phone\APIEmployeePhoneCheckRequest;
use App\Http\Requests\API\Phone\APIEmployeePhoneRequestWorkAddressRequest;
use App\Http\Requests\API\Phone\APIEmployeePhoneChooseWorkAddressRequest;
use App\Http\Requests\API\Phone\APIEmployeePhoneWorkRequest;
use App\Http\Requests\API\Phone\APIEmployeePhoneWorkTableRequest;
use App\Jobs\SendTabletTemporaryWorkingTimestampsToConnectorServiceJob;
use App\Http\Controllers\Reusables\BusinessMonthForAPITrait;

class EmployeePhoneAPIController extends Controller
{
    use BusinessMonthForAPITrait;

    /**
     * Controller of API check company
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function checkCompany(Request $request)
    {
        return response()->json([
            'success'      => 'success'
        ], 200);
    }

    /**
     * Controller of API login
     *
     * @param  App\Http\Requests\API\Phone\APIEmployeePhoneLoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(APIEmployeePhoneLoginRequest $request)
    {
        $random = str_random(20);

        $company = Company::first();
        $employee = Employee::where('gps_number', $request->input('gps_number'))->first();

        $employee->device_id = $request->input('device_id');
        $employee->remember_token = Hash::make($random);

        $employee->save();

        return response()->json([
            'company_name' => $company->name,
            'employee_name' => $employee->fullName(),
            'authenticated_token' => $random
        ], 200);
    }

    /**
     * Controller of API check
     *
     * @param  App\Http\Requests\API\Phone\APIEmployeePhoneCheckRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function check(APIEmployeePhoneCheckRequest $request)
    {
        $company = Company::first();
        $employee = Employee::where('device_id', $request->input('device_id'))->first();

        return response()->json([
            'company_name' => $company->name,
            'employee_name' => $employee->fullName(),
        ], 200);
    }

    /**
     * Controller of API request work address
     *
     * @param  App\Http\Requests\API\Phone\APIEmployeePhoneRequestWorkAddressRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function requestWorkAddress(APIEmployeePhoneRequestWorkAddressRequest $request)
    {
        $filled_work_addresses = $this->searchAllOfWordAddressWithLocation($request->input('lat'), $request->input('lon'));

        switch ($filled_work_addresses->count()) {
            case 0:
                return response()->json(['location' => ['勤務地に移動してください']], 422);
            case 1:
                $employee = Employee::where('device_id', $request->input('device_id'))->first();

                return response()->json([
                    'mutil_work_address' => false,
                    'id' => $filled_work_addresses->first()->get("id"),
                    'name' => $filled_work_addresses->first()->get("name"),
                    'address' => $filled_work_addresses->first()->get("address"),
                    'type' => $this->searchTypeOfTimestampWithWorkAddressID($employee, $filled_work_addresses->first()->get("id"))
                ], 200);
            default:
                return response()->json([
                    'mutil_work_address' => true,
                    'data' => $filled_work_addresses->toArray(),
                ], 200);
        }
    }

    /**
     * Controller of API choose work address
     *
     * @param  App\Http\Requests\API\Phone\APIEmployeePhoneChooseWorkAddressRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function chooseWorkAddress(APIEmployeePhoneChooseWorkAddressRequest $request)
    {
        $employee = Employee::where('device_id', $request->input('device_id'))->first();

        $work_address = WorkAddress::find($request->input('work_address_id'));

        return response()->json([
            'id' => $work_address->id,
            'name' => $work_address->name,
            'address' => $work_address->address,
            'type' => $this->searchTypeOfTimestampWithWorkAddressID($employee, $work_address->id),
        ], 200);
    }

    /**
     * Controller of API work
     *
     * @param  App\Http\Requests\API\Phone\APIEmployeePhoneWorkRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function work(APIEmployeePhoneWorkRequest $request)
    {
       $request_key = $request->input('company_code') . '_' . $request->work_address->work_location_id . '_' . $request->input('work_address_id') . '_' .
            $request->input('device_id') . '_' . (Carbon::now())->timestamp;

        DB::table('tablet_temporary_working_timestamps')->insert(
            [
                'company_code' => $request->input('company_code'),
                'device_id' => $request->input('device_id'),
                'work_location_id' => $request->work_address->work_location_id,
                'work_address_id' => $request->input('work_address_id'),
                'timestamped_value' => (Carbon::now())->timestamp,
                'card_code' => null,
                'timestamped_type' => $request->input('type'),
                'name_id' => null,
                'latitude' => $request->input('lat'),
                'longitude' => $request->input('lon'),
                'request_key' => $request_key,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );

        $new_job = (new SendTabletTemporaryWorkingTimestampsToConnectorServiceJob(
            $request_key,
            $request->input('company_code'),
            $request->work_address->work_location_id,
            $request->input('work_address_id')
        ))->onConnection('database')->onQueue('TemporaryWorkingTimestamps');

        dispatch($new_job);

        return response()->json([
            'employee_name'    => $request->employee->fullName(),
        ], 200);
    }

    /**
     * Controller of API work table
     *
     * @param  App\Http\Requests\API\Phone\APIEmployeePhoneWorkTableRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function workTable(APIEmployeePhoneWorkTableRequest $request)
    {
        $employee = Employee::where('device_id', $request->input('device_id'))->first();

        $business_month = $request->input('business_month');

        $data = $this->getDataAccordingBusinessMonthForAPI($employee, $business_month);

        return response()->json($data, 200);
    }

    /**
     * Controller of API check version
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function checkVersion(Request $request)
    {
        $version = "1.0.0";

        if ($request->input('app_test') == "true") {
            if ($request->input('app_name') === "google_play_app") {
                $version = "1.1.0";
            } else if ($request->input('app_name') === "apple_store_app") {
                $version = "1.3.0";
            }
        } else {
            if ($request->input('app_name') === "google_play_app") {
                $version = "1.1.0";
            } else if ($request->input('app_name') === "apple_store_app") {
                $version = "1.3.0";
            }
        }

        return response()->json([
            'version'    => $version,
        ], 200);
    }

    /**
     * Controller of API logout
     *
     * @param  App\Http\Requests\API\Phone\APIEmployeePhoneWorkTableRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $employee = Employee::where('device_id', $request->input('device_id'))->first();

        $employee->device_id = null;
        $employee->remember_token = null;
        $employee->save();

        return response()->json([
            'success'      => 'success'
        ], 200);
    }

    /**
     * Controller of API download file apk
     *
     * @param  App\Http\Requests  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function downloadInstallerAndroid(Request $request)
    {
        $pathToFile = null;

        if ($request->input('app_test') == "true") {
            if ($request->input('app_name') === "google_play_app") {
                $pathToFile = public_path("file/apk/employee_phone/test_server/google_play_app.apk");
            }
        } else {
            if ($request->input('app_name') === "google_play_app") {
                $pathToFile = public_path("file/apk/employee_phone/real_server/google_play_app.apk");
            }
        }

        return response()->file($pathToFile ,[
            'Content-Type'=>'application/vnd.android.package-archive',
            'Content-Disposition'=> 'attachment; filename="android.apk"',
        ]) ;
    }

    /**
     * The function would be search work address with lat, lon
     *
     * @param latitude $lat
     * @param longitude  $lon
     * @return array $work_address_ids
     */
    private function searchAllOfWordAddressWithLocation($lat, $lon)
    {
        $work_addresses = WorkAddress::get();
        $filled_work_addresses = collect([]);

        foreach ($work_addresses as $work_address) {
            if (isset($work_address->latitude) && isset($work_address->longitude) && isset($work_address->login_range)) {
                $distance = Coordinate::distance($lat, $lon, $work_address->latitude, $work_address->longitude, "K");

                if ($distance * 1000 <= $work_address->login_range)
                    $filled_work_addresses->push(collect($work_address)->only(['id', 'name', 'address']));
            }
        }

        return $filled_work_addresses;
    }

    /**
     * The function would be find type of timestamp with work address id
     *
     * @param App/Employee $employee
     * @param integer $work_address_id
     * @return integer $type
     */
    private function searchTypeOfTimestampWithWorkAddressID($employee, $work_address_id)
    {
        $now = Carbon::now();
        $work_address = WorkAddress::find($work_address_id);

        $period = $this->getPeriodDay($work_address, $now);

        $date_start = $this->getTrueDateWithCurrentTime($employee, $period[0]);

        $date_end = $this->getTrueDateWithCurrentTime($employee, $period[1]);

        $type = $this->findTypeOfTimestampWithDateAndWorkAddress($employee, $date_start, $date_end, $work_address->id);

        return $type;
    }

    /**
     * The function would be get a period of date
     *
     * @param App/WorkAddress $work_address
     * @param Carbon $now
     * @return array[start_date,end_date]
     */
    private function getPeriodDay($work_address, $now)
    {
        $date_time_point = Carbon::createFromFormat('Y-m-d H:i:s', $now->toDateString() . ' ' . $work_address->date_separate_time . ':00');

        if ($now->lt($date_time_point))
            return [$date_time_point->copy()->subDay(), $date_time_point];

        return [$date_time_point, $date_time_point->copy()->addDay()];
    }

    /**
     * The function would be find true date follow $date parameter
     *
     * @param App/Employee $employee
     * @param Carbon $now
     * @return Carbon
     */
    private function getTrueDateWithCurrentTime($employee, $now)
    {
        $company = $employee->workLocation->company;

        $date_limit = Carbon::createFromFormat('Y-m-d H:i:s', $now->toDateString() . ' ' . $company->date_separate_time . ':00');
        if ($company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {

            if ($now->lt($date_limit))
                return $now->subDay();

        } else {

            if ($now->gt($date_limit))
                return $now->addDay();
        }

        return $now;
    }

    /**
     * The function would be find type of timestamp on work address in the date
     *
     * @param App/Employee $employee
     * @param Carbon $date_start
     * @param Carbon $date_end
     * @param Integer $work_address_id
     * @return $type
     */
    private function findTypeOfTimestampWithDateAndWorkAddress($employee, $date_start, $date_end, $work_address_id)
    {
        $type = WorkingTimestamp::START_WORK;

        $employee_working_day_ids = EmployeeWorkingDay::where('employee_id', $employee->id)->whereIn('date', [$date_start->toDateString(), $date_end->toDateString()])->pluck('id');

        if (isset($employee_working_day_ids)) {
            $working_timestamps = WorkingTimestamp::whereIn('employee_working_day_id', $employee_working_day_ids)
                                    ->where('work_address_id', $work_address_id)
                                    ->whereIn('timestamped_type', [WorkingTimestamp::START_WORK, WorkingTimestamp::END_WORK])
                                    ->enable()->get();

            $working_timestamp = $working_timestamps->filter(function ($working_timestamp) use ($date_start, $date_end) {
                                    return ($working_timestamp->raw_date_time_value >= $date_start->toDateTimeString())
                                            && ($working_timestamp->raw_date_time_value <= $date_end->toDateTimeString());
                                })->sortByDesc('raw_date_time_value')->first();

            if (isset($working_timestamp) && $working_timestamp->timestamped_type == WorkingTimestamp::START_WORK)
                $type = WorkingTimestamp::END_WORK; 
        }

        return $type;
    }
}

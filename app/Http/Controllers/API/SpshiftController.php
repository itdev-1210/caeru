<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use AWS;

class SpshiftController extends Controller
{
    /**
     * Controller of API addFace
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function addFace(Request $request)
    {
        $result = $this->addFaceToAWS($request->input('collectionId'), $request->file('image'));

        foreach ($result['FaceRecords'] as $face_record) {
            return response()->json([
                'FaceId' => $face_record["Face"]["FaceId"],
            ], 200);
        }

        return response()->json(['image' => ["Couldn't find the face in image."]], 403);
    }

    /**
     * Controller of API deleteFace
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function deleteFace(Request $request)
    {
        $result = $this->deleteFaceToAWS($request->input('collectionId'), $request->input('face_id'));

        foreach ($result['DeletedFaces'] as $delete_face) {
            return response()->json([
                'success' => 'success',
            ], 200);
        }

        return response()->json(['error' => ["Face id wasn't found"]], 403);
    }

    /**
     * Controller of API editFace
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function editFace(Request $request)
    {
        $result = $this->deleteFaceToAWS($request->input('collectionId'), $request->input('face_id'));

        foreach ($result['DeletedFaces'] as $delete_face) {
            $result = $this->addFaceToAWS($request->input('collectionId'), $request->file('image'));

            foreach ($result['FaceRecords'] as $face_record) {
                return response()->json([
                    'FaceId' => $face_record["Face"]["FaceId"],
                ], 200);
            }

            return response()->json(['image' => ["Couldn't find the face in image."]], 403);
        }

        return response()->json(['error' => ["Some things was wrong"]], 403);
    }

    /**
     * The function would be add faces to amazon rekonition
     *
     * @param  String  $collectionId
     * @param  File  $file
     * @return Array
     */
    private function addFaceToAWS($collectionId, $file)
    {
        $rekognition = AWS::createClient('rekognition');

        $result = $rekognition->indexFaces([
            'CollectionId' => $collectionId,
            'ExternalImageId' => $file->getClientOriginalName(),
            'Image' => [
                'Bytes' => file_get_contents($file),
            ],
        ]);

        return $result;
    }

    /**
     * The function would be remove only one face in amazon rekonition
     *
     * @param  String  $collectionId
     * @param  String  $face_id
     * @return Array
     */
    private function deleteFaceToAWS($collectionId, $face_id)
    {
        $rekognition = AWS::createClient('rekognition');

        $result = $rekognition->deleteFaces([
            'CollectionId' => $collectionId,
            'FaceIds' => [$face_id],
        ]);

        return $result;
    }
}

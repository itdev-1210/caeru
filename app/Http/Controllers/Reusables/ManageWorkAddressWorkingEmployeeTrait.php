<?php

namespace App\Http\Controllers\Reusables;

use App\WorkStatus;
use App\RestStatus;
use App\EmployeeWorkingInformation;
use App\WorkAddressWorkingInformation;
use App\WorkAddressWorkingEmployee;


trait ManageWorkAddressWorkingEmployeeTrait
{
    /**
     * A function to check if we can add more WorkAddressWorkingEmployee to a given WorkAddressWorkingInformation.
     *
     * @param int|WorkAddressWorkingInformation     $work_address_working_info
     * @param boolean                               $assuming_will_add_one      if we're assuming to add one more WAWE to this WAWI, then the operand is '>='(because the current WAWI is already full of WAWEs).
     *                                                                          Otherwise the operand is '>'.
     * @return boolean
     */
    protected function canAddMoreWorkingEmployeeTo($work_address_working_info, $assuming_will_add_one = false)
    {
        if (is_int($work_address_working_info)) {
            $work_address_working_info = WorkAddressWorkingInformation::find($work_address_working_info);
        }

        if ($work_address_working_info) {

            $currently_working_employees = $work_address_working_info->currentActualWorkingEmployees();

            if ($assuming_will_add_one) {
                return !($currently_working_employees->count() >= $work_address_working_info->candidate_number);
            } else {
                return !($currently_working_employees->count() > $work_address_working_info->candidate_number);
            }
        }

        return false;
    }

    /**
     * Create a new EmployeeWorkingInformation and connect it to a given WorkAddressWorkingEmployee.
     * Remember: this given WorkAddressWorkingEmployee must have working_confirm = true.
     *
     * @param WorkAddressWorkingEmployee    $working_employee,
     * @param array                         $additional_data
     * @return void
     */
    protected function createNewEmployeeWorkingInformation(WorkAddressWorkingEmployee $working_employee, $additional_data = [])
    {
        $new_employee_working_info = new EmployeeWorkingInformation([
            'planned_work_location_id'  => $working_employee->workAddressWorkingInformation->workAddressWorkingDay->workAddress->work_location_id,
            'planned_work_address_id'   => $working_employee->workAddressWorkingInformation->workAddressWorkingDay->workAddress->id,
            'employee_working_day_id'   => $working_employee->employee_working_day_id,
            'work_address_working_employee_id'  => $working_employee->id,
        ]);

        // If there is additional data, we need to assign that data correctly
        if ($additional_data) {
            $new_employee_working_info->planned_work_status_id = (isset($additional_data['planned_work_status_id'])) ? $additional_data['planned_work_status_id'] : null;
            $new_employee_working_info->planned_rest_status_id = (isset($additional_data['planned_rest_status_id'])) ? $additional_data['planned_rest_status_id'] : null;

            // If the planned_work_status is HOUDE or KYUUDE then these attributes have to be saved like this
            if ((isset($additional_data['planned_work_status_id']) && $additional_data['planned_work_status_id'] == WorkStatus::HOUDE) || (isset($additional_data['planned_rest_status_id']) && $additional_data['planned_work_status_id'] == WorkStatus::KYUUDE)) {
                // Save data to the planned_ attributes
                $new_employee_working_info->planned_overtime_start = $working_employee->workAddressWorkingInformation->schedule_start_work_time;
                $new_employee_working_info->planned_overtime_end = $working_employee->workAddressWorkingInformation->schedule_end_work_time;

                // Now we also need to save to the pocket break timne of work_address_working_employee instantces as well
                if ($additional_data['schedule_break_time_changes'] !== null) {
                    $new_employee_working_info->planned_break_time = $additional_data['schedule_break_time_changes'];
                    $working_employee->pocket_break_time = $additional_data['schedule_break_time_changes'];
                } else {
                    $new_employee_working_info->planned_break_time = config('caeru.empty');
                    $working_employee->pocket_break_time = config('caeru.empty');
                }

                if ($additional_data['schedule_night_break_time_changes'] !== null) {
                    $new_employee_working_info->planned_night_break_time = $additional_data['schedule_night_break_time_changes'];
                    $working_employee->pocket_night_break_time = $additional_data['schedule_night_break_time_changes'];
                } else {
                    $new_employee_working_info->planned_night_break_time = config('caeru.empty');
                    $working_employee->pocket_night_break_time = config('caeru.empty');
                }

                // Clear the schedule_ attributes
                $new_employee_working_info->schedule_start_work_time = config('caeru.empty_date');
                $new_employee_working_info->schedule_end_work_time = config('caeru.empty_date');
                $new_employee_working_info->schedule_break_time = config('caeru.empty');
                $new_employee_working_info->schedule_night_break_time = config('caeru.empty');
                $new_employee_working_info->schedule_working_hour = config('caeru.empty_time');

            } else {

                // Now we also need to save to the pocket break timne of work_address_working_employee instantces as well
                if ($additional_data['schedule_break_time_changes'] !== null) {
                    $new_employee_working_info->schedule_break_time = $additional_data['schedule_break_time_changes'];
                    $working_employee->pocket_break_time = $additional_data['schedule_break_time_changes'];
                } else {
                    $new_employee_working_info->schedule_break_time = config('caeru.empty');
                    $working_employee->pocket_break_time = config('caeru.empty');
                }

                if ($additional_data['schedule_night_break_time_changes'] !== null) {
                    $new_employee_working_info->schedule_night_break_time = $additional_data['schedule_night_break_time_changes'];
                    $working_employee->pocket_night_break_time = $additional_data['schedule_night_break_time_changes'];
                } else {
                    $new_employee_working_info->schedule_night_break_time = config('caeru.empty');
                    $working_employee->pocket_night_break_time = config('caeru.empty');
                }
            }

        // If there is no additional data, that mean all those attributes are assigned to null
        } else {
            $new_employee_working_info->planned_work_status_id = null;
            $new_employee_working_info->planned_rest_status_id = null;
            $new_employee_working_info->schedule_break_time = config('caeru.empty');
            $new_employee_working_info->schedule_night_break_time = config('caeru.empty');
        }

        $working_employee->save();
        $new_employee_working_info->save();
    }
}
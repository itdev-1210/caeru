<?php

namespace App\Http\Controllers\Reusables;

use App\WorkLocation;
use App\EmployeeWorkingDay;
use App\Timezone;
use App\Services\TimezonesService;
use App\Services\TimestampesAndWorkingDayConnectorService;

trait ManageWorkingTimestampsTrait
{

    /**
     * Add new WorkingTimestamps for the given EmployeeWorkingDay
     *
     * @param array|collection      $working_timestamps
     * @param EmployeeWorkingDay    $working_day
     * @return void
     */
    protected function addNewWorkingTimestampsForWorkingDay($working_timestamps, EmployeeWorkingDay $working_day)
    {
        foreach ($working_timestamps as $working_timestamp) {
            // Get timezone from work location and set to this timestamp
            $time_zone_id = WorkLocation::find($working_timestamp->work_location_id)->currentSetting()->timezone;

            $timezones_service = resolve(TimezonesService::class);
            $timezone_instance = $timezones_service->getTimezoneById($time_zone_id);

            $working_timestamp->name_id = $timezone_instance ? $timezone_instance->name_id : null;

            // Set the relationship for them
            // $working_timestamp->employeeWorkingDay()->associate($working_day);
            // $working_timestamp->save();
        }

        // Use the connector service instead of setting the relation ship like above
        $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);
        $connector_service->connectTimestampsToWorkingDaysOfEmployee($working_timestamps, $working_day->employee);

        // event(new WorkingTimestampChanged($working_day));
    }

}
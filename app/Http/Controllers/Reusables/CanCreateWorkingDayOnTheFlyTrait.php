<?php

namespace App\Http\Controllers\Reusables;

use App\Employee;
use App\EmployeeWorkingDay;
use App\WorkAddress;
use App\WorkAddressWorkingDay;
use Carbon\Carbon;

trait CanCreateWorkingDayOnTheFlyTrait
{
    /**
     * If on that day, this employee still does not have any working day instance, create one on the fly for him/her.
     *
     * @param int       $employee_id
     * @param string    $date
     * @return void
     */
    protected function createEmployeeWorkingDayOnTheFly($employee_id, $date)
    {
        // Validate again. Just in case
        $employee = Employee::find($employee_id);

        if ($employee && $this->validateDateByFormat($date)) {

            $new_working_day_instance = new EmployeeWorkingDay();

            $new_working_day_instance->date = $date;
            $new_working_day_instance->employee()->associate($employee_id);
            $new_working_day_instance->save();

            return $new_working_day_instance;

        } else {
            abort(500, 'Can not find attendance information for this employee on that day!');
        }
    }

    /**
     * If on that day, this work address still does not have any working day instance, create one on the fly for it.
     *
     * @param int       $work_address_id
     * @param string    $date
     * @return void
     */
    protected function createWorkAddressWorkingDayOnTheFly($work_address_id, $date)
    {
        // Validate again. Just in case
        $work_address = WorkAddress::find($work_address_id);

        if ($work_address && $this->validateDateByFormat($date)) {

            $new_working_day_instance = new WorkAddressWorkingDay();

            $new_working_day_instance->date = $date;
            $new_working_day_instance->WorkAddress()->associate($work_address_id);
            $new_working_day_instance->save();

            return $new_working_day_instance;

        } else {
            abort(500, 'Can not find attendance information for this employee on that day!');
        }
    }

    /**
     * Validate the date string.
     *
     * @param string    $date
     * @param string    $format
     * @return boolean
     */
    protected function validateDateByFormat($date, $format = 'Y-m-d')
    {
        $result = Carbon::createFromFormat($format, $date);

        return $result;
    }
}
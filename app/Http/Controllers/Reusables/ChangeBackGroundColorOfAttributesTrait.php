<?php

namespace App\Http\Controllers\Reusables;

use App\ColorStatus;

trait ChangeBackGroundColorOfAttributesTrait
{
    /**
     * Change the ColorStatus record of the snapshot of the given EmployeeWorkingInfo
     *
     * @param EmployeeWorkingInformation    $working_info,
     * @param string                        $field_name
     * @return void
     */
    protected function changeColorOfSnapshot($working_info, $field_name)
    {
        $snapshot = $working_info->employeeWorkingInformationSnapshot;

        if ($snapshot) {
            $snapshot_field_name = 'left_' . $field_name;
            $color_status = $snapshot->colorStatuses()->where('field_name', $snapshot_field_name)->first();
            if ($color_status) {
                $color_status->field_css_class = ColorStatus::UPDATED_COLOR;
                $color_status->save();
            }
        }
    }

    /**
     * A shortcut to create a ColorStatus record.
     *
     * @param int       $working_info_id
     * @param string    $field_name
     * @return void
     */
    protected function createColorStatus($working_info_id, $field_name)
    {
        ColorStatus::create([
            'colorable_type' => ColorStatus::EMPLOYEE_WORKING_INFORMATION,
            'colorable_id' => $working_info_id,
            'field_name' => $field_name,
            'field_css_class' => ColorStatus::UPDATED_COLOR,
        ]);
    }
}
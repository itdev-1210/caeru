<?php

namespace App\Http\Controllers\Reusables;

trait UseScheduleTypeIconTrait
{
    /**
     * Get the icon of the schedule type. (It's the small character before employee's name)
     *
     * @param integer   $schedule_type
     * @return string|null
     */
    protected function getScheduleTypeIcon($schedule_type)
    {
        $icons = [
            config('constants.flexible_schedule') => 'フ',
            config('constants.monthly_based_schedule') => '変',
            config('constants.yearly_based_schedule') => '変',
            config('constants.normal_schedule') => '',
        ];

        return isset($icons[$schedule_type]) ? $icons[$schedule_type] : null;
    }
}
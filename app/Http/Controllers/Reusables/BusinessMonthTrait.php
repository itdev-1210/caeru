<?php

namespace App\Http\Controllers\Reusables;

use App\Employee;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use App\EmployeeWorkingInformationSnapshot;
use App\WorkAddress;
use App\WorkAddressWorkingDay;
use App\Setting;
use App\CalendarRestDay;
use Carbon\Carbon;
use App\Services\NationalHolidayService;

trait BusinessMonthTrait
{
    /**
     * Just like the function below, only a wrapper to use take an Employee instead of a Setting
     *
     * @param Employee      $employee
     * @return Carbon
     */
    protected function calculateTheBusinessMonth(Employee $employee)
    {
        return $this->calculateBusinessMonthBaseOnASetting($employee->workLocation->currentSetting());
    }


    /**
     * Calculate the current business month to show, in the case of none provided.
     *
     * @param Setting   a setting instance
     * @return Carbon
     */
    protected function calculateBusinessMonthBaseOnASetting(Setting $setting)
    {
        $today = Carbon::today();

        $business_month = $today->copy();
        $business_month->day = 1;

        // Dont pay too much attention to this
        $this_month_offset = Setting::THIS_MONTH;

        // Base business month

        $business_month->month = $business_month->month - $setting->pay_month + $this_month_offset;

        // business month's pay day at this moment
        if ($setting->pay_day !== 0) {
            $current_pay_day = Carbon::createFromDate($today->year, $business_month->month, $setting->pay_day);

        } else {
            $current_pay_day = Carbon::createFromDate($today->year, $business_month->month + 1, 1);
            $current_pay_day->subDay();
        }

        // After this we have the business_month to show or to send to the front end.
        $business_month->month = ($today->day <= $current_pay_day->day) ? $business_month->month : $business_month->month + 1;

        return $business_month;
    }


    /**
     * Just a wrapper for the function below, to take an Employee instead of a Setting
     *
     * @param Employee      $employee
     * @param Carbon        $business_month
     * @return array        with the first([0]) element is the start_date and the second element([1]) is the end_date. Both are Carbon instances.
     */
    protected function getStartDateAndEndDateFromBusinessMonth($employee, $business_month)
    {
        return $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($employee->workLocation->currentSetting(), $business_month);
    }

    /**
     * Get the business month that contain a specific day. If there is no specific day, use today.
     *
     * @param Employee      $setting        the setting use to calculate the business month
     * @param string|null   $day            the specific day, default to null.
     * @return Carbon
     */
    protected function getBusinessMonthThatContainASpecificDay($setting, $day = null)
    {
        // If there is no specific day, use today
        if (!$day) {
            $day = Carbon::today();
        } else {
            $day = new Carbon($day);
        }

        $business_month = $day->copy();

        // Use the candidate business month to get the start/end_date
        $start_and_end_date = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($setting, $business_month);
        $start_date = $start_and_end_date[0];
        $end_date = $start_and_end_date[1];

        // Verify if today is in between start_date and end_date, if not change to the next business month
        if (!($start_date->lte($day) && $day->lte($end_date))) {
            $business_month->addMonth();
        }

        return $business_month;
    }

    /**
     * Get the month.
     *
     * @param Employee      $employee       the employee to get the setting
     * @return Carbon
     */
    protected function getMonthThatContainASpecificDay($employee)
    {
        $day = Carbon::today();

        $month = $day->copy();

        // Use the candidate business month to get the start/end_date
        $start_and_end_date = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($employee->workLocation->currentSetting(), $month);
        $start_date = $start_and_end_date[0];
        $end_date = $start_and_end_date[1];

        // Verify if today is in between start_date and end_date, if not change to the next business month
        if (!($start_date->lte($day) && $day->lte($end_date))) {
            $month->addMonth();
        }

        return $month;
    }


    /**
     * Create two carbon instances for start_date and end_date from a given business_month (also a Carbon instance)
     *
     * @param Setting       $setting
     * @param Carbon        $business_month
     * @return array        with the first([0]) element is the start_date and the second element([1]) is the end_date. Both are Carbon instances.
     */
    protected function getStartDateAndEndDateFromBusinessMonthBaseOnASetting(Setting $setting, $business_month)
    {
        $salary_accounting_day = $setting->salary_accounting_day;

        if ($salary_accounting_day !== 0) {
            $start_date = Carbon::create($business_month->year, $business_month->month -1, $salary_accounting_day + 1, 0, 0, 0);

            $end_date = Carbon::create($business_month->year, $business_month->month, $salary_accounting_day, 23, 59, 59);

        } else {
            $start_date = Carbon::create($business_month->year, $business_month->month, 1, 0, 0, 0);

            $end_date = Carbon::create($business_month->year, $business_month->month + 1, 1, 23, 59, 59);
            $end_date->subDay();
        }

        return [$start_date, $end_date];
    }


    /**
     * Calculate the summarized data and prepare the compact working data for each day in this business month
     *
     * @param Employee  $employee
     * @param Carbon    $start_date
     * @param Carbon    $end_date
     * @return array
     */
    protected function getWorkingDataFromDateRange($employee, $start_date, $end_date)
    {
        // Load employee working day data
        $working_days = EmployeeWorkingDay::with([
                                                'concludedLevelOneManager',
                                                'concludedLevelTwoManager',
                                                'concludedEmployeeWorkingDay',
                                                'substituteEmployeeWorkingInformations',
                                                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                                                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                                                'employeeWorkingInformations.colorStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.setting',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
                                                'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
                                                'employeeWorkingInformations.currentRealWorkLocation.setting',
                                                'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
                                                'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
                                                'employeeWorkingInformations.currentRealWorkLocation.company.setting',
                                                'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
                                                'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
                                                'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
                                            ])
                                            ->where('employee_id', $employee->id)
                                            ->where('date', '>=', $start_date->toDateString())
                                            ->where('date', '<=', $end_date->toDateString())
                                            ->get();

        // Initialize summary data
        $summary_working_data = [
            'have_schedule_days' => 0,
            'planned_work_days' => 0,
            'real_work_days' => 0,
            'planned_working_time' => 0,
            'real_working_time' => 0,
            'schedule_working_time' => 0,
            'planned_work_span_time' => 0,
            'real_work_span_time' => 0,
            'planned_paid_rest_time' => 0,
            'real_paid_rest_time' => 0,
            'planned_unpaid_rest_time' => 0,
            'real_unpaid_rest_time' => 0,
            'planned_non_work_time' => 0,
            'real_non_work_time' => 0,
            'planned_overtime_work_time' => 0,
            'real_overtime_work_time' => 0,
            'planned_night_work_time' => 0,
            'real_night_work_time' => 0,
            'planned_kekkin_days' => 0,
            'real_kekkin_days' => 0,
            'planned_taken_paid_rest_days' => 0,
            'real_taken_paid_rest_days' => 0,
            'planned_taken_paid_rest_time' => 0,
            'real_taken_paid_rest_time' => 0,
        ];

        // Initialize working data for each day
        $working_data_for_each_day = [];

        // Load calendar rest day data
        $rest_days = $employee->workLocation->getRestDays($start_date->toDateString(), $end_date->toDateString());

        // Load national holidays
        $national_holidays_service = resolve(NationalHolidayService::class);
        $national_holidays = $national_holidays_service->get($start_date->toDateString(), $end_date->toDateString());

        // Some variables to check the concluded status
        $everyday_with_data_is_level_one_concluded = true;
        $everyday_with_data_is_level_two_concluded = true;
        $zero_day_without_data = true;
        $everyday_can_be_concluded = true;

        // Increase the end_date by one so that we can include the end_date in the loop
        $end_date->addDay();
        $first_date = $start_date->copy();
        while ($start_date->toDateString() !== $end_date->toDateString()) {

            $working_day_with_data = $working_days->first(function ($working_day) use ($start_date) {
                return $working_day->date === $start_date->toDateString();
            });

            if ($working_day_with_data) {

                $data = $working_day_with_data->getSummaryInformation();

                // Calculate the summary data
                $summary_working_data['have_schedule_days'] += ($data['have_schedule'] == true) ? 1 : 0;
                $summary_working_data['planned_work_days'] += ($data['planned_work_day']) ? 1 : 0;
                $summary_working_data['real_work_days'] += ($data['real_work_day']) ? 1 : 0;
                $summary_working_data['planned_working_time'] += $data['planned_working_time'];
                $summary_working_data['real_working_time'] += $data['real_working_time'];
                $summary_working_data['schedule_working_time'] += $data['schedule_working_time'];
                $summary_working_data['planned_work_span_time'] += $data['planned_work_span_time'];
                $summary_working_data['real_work_span_time'] += $data['real_work_span_time'];
                $summary_working_data['planned_paid_rest_time'] += $data['planned_paid_rest_time'];
                $summary_working_data['real_paid_rest_time'] += $data['real_paid_rest_time'];
                $summary_working_data['planned_unpaid_rest_time'] += $data['planned_unpaid_rest_time'];
                $summary_working_data['real_unpaid_rest_time'] += $data['real_unpaid_rest_time'];
                $summary_working_data['planned_non_work_time'] += $data['planned_non_work_time'];
                $summary_working_data['real_non_work_time'] += $data['real_non_work_time'];
                $summary_working_data['planned_overtime_work_time'] += $data['planned_overtime_work_time'];
                $summary_working_data['real_overtime_work_time'] += $data['real_overtime_work_time'];
                $summary_working_data['planned_night_work_time'] += $data['planned_night_work_time'];
                $summary_working_data['real_night_work_time'] += $data['real_night_work_time'];
                $summary_working_data['planned_kekkin_days'] += ($data['planned_is_kekkin'] == true) ? 1 : 0;
                $summary_working_data['real_kekkin_days'] += ($data['real_is_kekkin'] == true) ? 1 : 0;
                $summary_working_data['planned_taken_paid_rest_days'] += $data['planned_taken_paid_rest_days'];
                $summary_working_data['real_taken_paid_rest_days'] += $data['real_taken_paid_rest_days'];
                $summary_working_data['planned_taken_paid_rest_time'] += $data['planned_taken_paid_rest_time'];
                $summary_working_data['real_taken_paid_rest_time'] += $data['real_taken_paid_rest_time'];

                // And prepare the working informations
                $working_data_for_each_day[$start_date->toDateString()] = [
                    'data' => $working_day_with_data->getCompactedWorkingInformations()->values(),
                ];
                $working_data_for_each_day[$start_date->toDateString()]['concluded_level_one'] = $working_day_with_data->concluded_level_one == true;
                $working_data_for_each_day[$start_date->toDateString()]['concluded_level_two'] = $working_day_with_data->concluded_level_two == true;
                $working_data_for_each_day[$start_date->toDateString()]['concluded_level_one_manager_name'] = $working_day_with_data->concludedLevelOneManager !== null ? $working_day_with_data->concludedLevelOneManager->last_name : null;
                $working_data_for_each_day[$start_date->toDateString()]['concluded_level_two_manager_name'] = $working_day_with_data->concludedLevelTwoManager !== null ? $working_day_with_data->concludedLevelTwoManager->fullName() : null;
                $working_data_for_each_day[$start_date->toDateString()]['working_day_id'] = $working_day_with_data->id;

                $working_data_for_each_day[$start_date->toDateString()]['can_be_concluded'] = $this->checkIfCanConcludeThisDayToday($start_date->toDateString());

                // This is for concluded status checking
                $everyday_can_be_concluded = $working_data_for_each_day[$start_date->toDateString()]['can_be_concluded'] ? $everyday_can_be_concluded : false;
                $everyday_with_data_is_level_one_concluded = $working_day_with_data->concluded_level_one == true ? $everyday_with_data_is_level_one_concluded : false;
                $everyday_with_data_is_level_two_concluded = $working_day_with_data->concluded_level_two == true ? $everyday_with_data_is_level_two_concluded : false;

            } else {
                $working_data_for_each_day[$start_date->toDateString()] = [
                    'data' => [],
                    'concluded_level_one' => false,
                    'concluded_level_two' => false,
                    'concluded_level_one_manager_name' => null,
                    'concluded_level_two_manager_name' => null,
                    'can_be_concluded' => $this->checkIfCanConcludeThisDayToday($start_date->toDateString()),
                ];

                // This is for concluded status checking
                $everyday_can_be_concluded = $working_data_for_each_day[$start_date->toDateString()]['can_be_concluded'] ? $everyday_can_be_concluded : false;
                $zero_day_without_data = false;
            }

            // Put some more data into the working_data_for_each_day. Colors relating stuffs. (I.e Law rest day, normal rest day, sunday, saturday)
            $working_data_for_each_day[$start_date->toDateString()]['day_of_week'] = $start_date->dayOfWeek;
            $working_data_for_each_day[$start_date->toDateString()]['law_rest_day'] = $this->isLawRestDay($start_date, $rest_days);
            $working_data_for_each_day[$start_date->toDateString()]['normal_rest_day'] = $this->isNormalRestDay($start_date, $rest_days);
            $working_data_for_each_day[$start_date->toDateString()]['national_holiday'] = $this->isNationalHoliday($start_date, $national_holidays);

            $start_date->addDay();
        }

        // Need a step to convert all the exceed paid rest time to paid rest day
        $summary_working_data['planned_taken_paid_rest_days'] = ($summary_working_data['planned_taken_paid_rest_time'] >= $employee->work_time_per_day * 60) ?
                                                                $summary_working_data['planned_taken_paid_rest_days'] + floor($summary_working_data['planned_taken_paid_rest_time']/($employee->work_time_per_day * 60)) :
                                                                $summary_working_data['planned_taken_paid_rest_days'];

        $summary_working_data['planned_taken_paid_rest_time'] = ($summary_working_data['planned_taken_paid_rest_time'] >= $employee->work_time_per_day * 60) ?
                                                                $summary_working_data['planned_taken_paid_rest_time'] % ($employee->work_time_per_day * 60) :
                                                                $summary_working_data['planned_taken_paid_rest_time'];

        $summary_working_data['real_taken_paid_rest_days'] = ($summary_working_data['real_taken_paid_rest_time'] >= $employee->work_time_per_day * 60) ?
                                                            $summary_working_data['real_taken_paid_rest_days'] + floor($summary_working_data['real_taken_paid_rest_time']/($employee->work_time_per_day * 60)) :
                                                            $summary_working_data['real_taken_paid_rest_days'];

        $summary_working_data['real_taken_paid_rest_time'] = ($summary_working_data['real_taken_paid_rest_time'] >= $employee->work_time_per_day * 60) ?
                                                                $summary_working_data['real_taken_paid_rest_time'] % ($employee->work_time_per_day * 60) :
                                                                $summary_working_data['real_taken_paid_rest_time'];

        return [
            'summarized_data' => $summary_working_data,
            'working_days' => $working_data_for_each_day,
            'everyday_can_be_concluded' => $everyday_can_be_concluded,
            'level_one_concluded_all' => $zero_day_without_data && $everyday_with_data_is_level_one_concluded,
            'level_two_concluded' => $zero_day_without_data && $everyday_with_data_is_level_two_concluded,
            'level_two_concluded_anchor_manager_name' => $working_data_for_each_day[$first_date->toDateString()]['concluded_level_two_manager_name']
        ];
    }


    /**
     * Get the working data of a WorkAddress from a given date range
     *
     * @param WorkAddress   $working_address
     * @param Carbon        $start_date
     * @param Carbon        $end_date
     * @return array
     */
    protected function getWorkAddressWorkingDataFromDateRange($work_address, $start_date, $end_date)
    {
        $working_days = WorkAddressWorkingDay::where('date', '>=', $start_date->toDateString())->where('date', '<=', $end_date->toDateString())->where('work_address_id', $work_address->id)->with([
            'workAddressWorkingInformations.workAddressWorkingEmployees.employee'
        ])->get()->map(function($working_day) {

            $result = [
                'id' => $working_day->id,
                'date' => $working_day->date,
                'working_infos' => $working_day->getCompactedWorkingInformations(),
            ];
            return $result;
        })
        ->keyBy('date');

        // Initialize working data for each day
        $working_data_for_each_day = [];

        // Load calendar rest day data
        $rest_days = $work_address->workLocation->getRestDays($start_date->toDateString(), $end_date->toDateString());

        // Load national holidays
        $national_holidays_service = resolve(NationalHolidayService::class);
        $national_holidays = $national_holidays_service->get($start_date->toDateString(), $end_date->toDateString());

        $pivot_date = $start_date->copy();
        $end_date->addDay();
        while ($pivot_date->toDateString() !== $end_date->toDateString()) {

            $working_day_with_data = isset($working_days[$pivot_date->toDateString()]) ? $working_days[$pivot_date->toDateString()] : null;

            if ($working_day_with_data) {
                $working_data_for_each_day[$pivot_date->toDateString()] = [
                    'id' => $working_day_with_data['id'],
                    'data' => $working_day_with_data['working_infos'],
                ];

            } else {
                $working_data_for_each_day[$pivot_date->toDateString()] = [
                    'data' => [],
                ];
            }

            // Put some more data into the working_data_for_each_day. Colors relating stuffs. (I.e Law rest day, normal rest day, sunday, saturday)
            $working_data_for_each_day[$pivot_date->toDateString()]['day_of_week'] = $pivot_date->dayOfWeek;
            $working_data_for_each_day[$pivot_date->toDateString()]['law_rest_day'] = $this->isLawRestDay($pivot_date, $rest_days);
            $working_data_for_each_day[$pivot_date->toDateString()]['normal_rest_day'] = $this->isNormalRestDay($pivot_date, $rest_days);
            $working_data_for_each_day[$pivot_date->toDateString()]['national_holiday'] = $this->isNationalHoliday($pivot_date, $national_holidays);

            $pivot_date->addDay();
        }

        return [
            'working_days' => $working_data_for_each_day,
        ];
    }

    /**
     * Calculate the total working hour
     *
     * @param Employee  $employee
     * @param Carbon    $start_date
     * @param Carbon    $end_date
     * @return $real_working_time
     */
    protected function getRealWorkingTime($employee, $start_date, $end_date)
    {
        // Load employee working day data
        $working_days = EmployeeWorkingDay::with([
                                                'substituteEmployeeWorkingInformations',
                                                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                                                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                                            ])
                                            ->where('employee_id', $employee->id)
                                            ->where('date', '>=', $start_date->toDateString())
                                            ->where('date', '<=', $end_date->toDateString())
                                            ->get();

        // Initialize summary data
        $real_working_time = 0;

        // Increase the end_date by one so that we can include the end_date in the loop
        $end_date->addDay();
        while ($start_date->toDateString() !== $end_date->toDateString()) {

            $working_day_with_data = $working_days->first(function ($working_day) use ($start_date) {
                return $working_day->date === $start_date->toDateString();
            });

            if ($working_day_with_data) {

                $data = $working_day_with_data->getSummaryInformation();

                $real_working_time += $data['real_working_time'];
            }

            $start_date->addDay();
        }

        return $real_working_time;
    }

    /**
     * Calculate the total working hour including request day
     *
     * @param Employee  $employee
     * @param Carbon    $start_date
     * @param Carbon    $end_date
     * @return $real_working_time
     */
    protected function getRealWorkingTimeIncludeRequest($employee, $start_date, $end_date)
    {
        // Load employee working day data
        $working_days = EmployeeWorkingDay::with([
                                                'employeeWorkingInformations',
                                                'employeeWorkingInformationSnapshots'
                                            ])
                                            ->where('employee_id', $employee->id)
                                            ->where('date', '>=', $start_date->toDateString())
                                            ->where('date', '<=', $end_date->toDateString())
                                            ->get();

        // Initialize summary data
        $real_working_time = 0;

        // Increase the end_date by one so that we can include the end_date in the loop
        $end_date->addDay();
        while ($start_date->toDateString() !== $end_date->toDateString()) {

            $working_day_with_data = $working_days->first(function ($working_day) use ($start_date) {
                return $working_day->date === $start_date->toDateString();
            });

            if ($working_day_with_data) {
                if (isset($working_day_with_data->employeeWorkingInformationSnapshots[0])) {
                    if($working_day_with_data->employeeWorkingInformationSnapshots[0]->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING) {
                        // var_dump($working_day_with_data->employeeWorkingInformations->toArray());exit;
                        $employeeWorkingInformationSnapshot = EmployeeWorkingInformationSnapshot::find($working_day_with_data->employeeWorkingInformationSnapshots[0]->id);
                        $employeeWorkingInformation = EmployeeWorkingInformation::find($working_day_with_data->employeeWorkingInformations[0]->id);
                        $real_working_time += $employeeWorkingInformationSnapshot->calcWorkingTime($employeeWorkingInformation);
                    }
                }
            }

            $start_date->addDay();
        }

        return $real_working_time;
    }

    /**
     * Check if the given day is a law rest day
     *
     * @param Carbon    $day
     * @param array     $rest_days
     * @return boolean
     */
    protected function isLawRestDay($day, $rest_days)
    {
        $index = $day->year . '-' . $day->month . '-' . $day->day;

        return isset($rest_days[$index]) && $rest_days[$index]['type'] == CalendarRestDay::LAW_BASED_REST_DAY;
    }

    /**
     * Check if the given day is a normal rest day
     *
     * @param Carbon    $day
     * @param array     $rest_days
     * @return boolean
     */
    protected function isNormalRestDay($day, $rest_days)
    {
        $index = $day->year . '-' . $day->month . '-' . $day->day;

        return isset($rest_days[$index]) && $rest_days[$index]['type'] == CalendarRestDay::NORMAL_REST_DAY;
    }

    /**
     * Check if the given day is a national holiday
     *
     * @param Carbon    $day
     * @param array     $national_holidays
     * @return boolean
     */
    protected function isNationalHoliday($day, $national_holidays)
    {
        $index = $day->format('Y-m-d');

        return $national_holidays->contains($index);
    }

    /**
     * Check if today the user can conclude this EmployeeWorkingDay (Because there's a spec about not allow concluding future days)
     *
     * @param string        $date   the day to check
     * @return boolean
     */
    protected function checkIfCanConcludeThisDayToday($date)
    {
        $current_date = new Carbon($date);
        $today = Carbon::today()->hour(23)->minute(59)->second(59);
        return $current_date->lt($today);
    }
}
<?php

namespace App\Http\Controllers\Reusables;

trait CustomizedPaginationTrait
{
    /**
     * This controller is using a small customize pagination function (paginateData). This constant is for that function.
     */
    protected $ITEMS_PER_PAGE = 20;
    
    /**
     * A small customized pagination function. It just simply chunks the result colection up.
     *
     * @param Collection    $data
     * @param integer       $page
     * @return array        an array include a collection(the result) and the current page.
     */
    protected function paginateData($data, $page)
    {
        if ($data->isNotEmpty()) {
            $pages = $data->chunk($this->ITEMS_PER_PAGE);

            $page = intval($page);

            $current_page = isset($pages[$page-1]) ? $page - 1 : 1;
            $result_page =  $pages[$current_page];

            return [
                'data' => $result_page,
                'current_page' => $current_page + 1,
            ];

        } else {
            return [
                'data' => $data,
                'current_page' => 1,
            ];
        }

    }
}
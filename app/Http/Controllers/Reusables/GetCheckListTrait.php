<?php
namespace App\Http\Controllers\Reusables;

use App\Setting;
use App\ChecklistItem;
use App\WorkLocation;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformationSnapshot;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

trait GetCheckListTrait
{
    use BusinessMonthTrait;

    /**
     * Gets the session.
     *
     * @param mixed $search_history_conditions
     */
    private function getSession() 
    {
        if(session()->has('checklist_search_history')) {
            return session()->get('checklist_search_history');
        }
        return null;
    }

    /**
     * Sets the session.
     *
     * @param  $search_history_conditions
     */
    private function setSession($search_history_conditions)
     {
        session(['checklist_search_history' => $search_history_conditions]);
    }

    /**
     * Gets the check list.
     *
     * @param array $search_conditions
     */
    private function getCheckList(array $search_conditions)
    {        
        $current_work_location = request()->session()->get('current_work_location');

        if ($current_work_location === "all") {
            $result = ChecklistItem::with('employee');
        } else {
            $work_location_ids = is_array($current_work_location) ? $current_work_location : [$current_work_location];

            // searchTarget 1: find checklist errors of employee who belongs to the current work location(s)
            // searchTarget 2: find checklist errors of employee who have EmployeeWorkingInformation of the current work location(s)
            $result = $search_conditions['searchTarget'] == 1 ? $this->getChecklistErrorsOfEmployeesWhoAreRegisteredToTheseWorkLocations($work_location_ids) :
                                                                $this->getChecklistErrorsOfEmployeesWhoHaveEWIOfTheseWorkLocations($work_location_ids);
        }

        $result = $this->applyConditions($result, $search_conditions);

        // Get the list of EmployeeWorkingDays that have considering request(s) and key it by date + # + employee_id, then use it to filter our the day that have request/
        $days_that_have_requests = EmployeeWorkingDay::where('date', '>=', $search_conditions['beginDate'])->where('date', '<=', $search_conditions['endDate'])
                                                        ->whereHas('employeeWorkingInformationSnapshots', function($query) {
                                                            return $query->where('left_status', EmployeeWorkingInformationSnapshot::CONSIDERING);
                                                        })->get()->keyBy(function($day) {
                                                            return $day->date . '#' . $day->employee_id;
                                                        });

        $checklists_timestamp_error  = $this->filterDaysThatHaveRequests(with(clone $result)->itemType(ChecklistItem::TIMESTAMP_ERROR)->get(), $days_that_have_requests);

        $checklists_confirm_needed  = $this->filterDaysThatHaveRequests(with(clone $result)->itemType(ChecklistItem::CONFIRM_NEEDED)->get(), $days_that_have_requests);

        return [
            'checklists_confirm_needed'=>$checklists_confirm_needed,
            'checklists_timestamp_error'=>$checklists_timestamp_error,
        ];
    }

    /**
     * Search for ChecklistItem swhich relate to Employees who belong to the given WorkLocation(s) (所属している従業員)
     *
     * @param array         $work_location_ids
     * @return Builder
     */
    private function getChecklistErrorsOfEmployeesWhoAreRegisteredToTheseWorkLocations($work_location_ids)
    {
        return ChecklistItem::with('employee')->whereHas('employee', function($query) use ($work_location_ids) {
            $query->whereIn('work_location_id', $work_location_ids);
        });
    }

    /**
     * Search for ChecklistItems which belong to EWIs which have planned_work_location_id in the given $work_location_ids
     *
     * @param array         $work_location_ids
     * @return Builder
     */
    private function getChecklistErrorsOfEmployeesWhoHaveEWIOfTheseWorkLocations($work_location_ids)
    {
        return ChecklistItem::with('employee')->whereHas('employeeWorkingInformation', function($query) use ($work_location_ids) {
            $query->whereIn('planned_work_location_id', $work_location_ids)
                ->orWhereHas('plannedSchedule', function($query) use ($work_location_ids) {
                    $query->whereIn('work_location_id', $work_location_ids);
                })
                ->orWhereHas('workAddressWorkingEmployee', function($query) use ($work_location_ids) {
                    $query->whereHas('plannedSchedule', function($query) use ($work_location_ids) {
                        $query->whereIn('work_location_id', $work_location_ids);
                    });
                });
        });
    }

    /**
     * Apply the search conditions to the query.
     * The seach conditions include the mandatory 'beginDate' and 'endDate', and the optional 'employeeId', 'employeeName', 'errList'.
     *
     * @param Builder       $query
     * @param array         $conditions
     *
     * @return Builder
     */
    private function applyConditions($query, $conditions)
    {
        $query->where('date', '>=', $conditions['beginDate'])->where('date', '<=', $conditions['endDate']);

        if (isset($conditions["employeeId"])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                $query->where('presentation_id', '=', $conditions['employeeId']);
            });
        }

        if (isset($conditions["employeeName"])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employeeName"] . '%');
            });
        }

        if (isset($conditions['errlist']) && !empty($conditions['errlist'])) {
            $query->whereIn('error_type', $conditions['errlist']);
        }

        if (isset($conditions["departments"]) && !empty($conditions['departments'])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                $query->whereIn('department_id', $conditions['departments']);
            });
        }

        return $query->orderBy('date', 'des')->orderBy('employee_id');
    }

    /**
     * Gets the beginDate, endDate follow salary_accounting_day.
     *
     * @link http://carbon.nesbot.com/docs/
     * @throws \Exception if salary_accounting_day not exitst on settings table
     */
    private function getDate() {
        $current_work_location = request()->session()->get('current_work_location');

        //$currentDay = Carbon::now()->day;
        $currentMonth = Carbon::now()->month;
        $currentYear = Carbon::now()->year;

        $history = $this->getSession();

        $year = $history ? $history['year'] : null;
        $month = $history ? $history['month'] : null;

        $year = request()->year ? request()->year : $year;
        $month = request()->month ? request()->month : $month;

        $setting = ($current_work_location === 'all') ? request()->user()->company->setting : WorkLocation::find($current_work_location)->currentSetting();
        $business_month = ($year == null || $month == null) ?  $this->calculateBusinessMonthBaseOnASetting($setting) : Carbon::create($year, $month, 1, 0, 0, 0);
        $start_and_end_date = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($setting, $business_month);

        return [$start_and_end_date[0]->format('Y-m-d'), $start_and_end_date[1]->format('Y-m-d'), $business_month->year, $business_month->month, $currentYear, $currentMonth];
    }

	/**
     * Searches checklist.
     *
     * @param boolean $refreshSession
     *
     * @return array
     */
    public function search() {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'refresh_hitory' => false ]);

        list($beginDate, $endDate, $year, $month, $currentYear, $currentMonth) = $this->getDate(); 

        $conditions = [
            'beginDate' => $beginDate,
            'endDate' => $endDate,
            'year' => $year,
            'month' => $month,
            'searchTarget' => 2,
        ];

        if (request()->exists('errlist') || request()->exists('employeeName') || request()->exists('employeeId') || request()->exists('searchTarget')) {
            $conditions['errlist'] = request()->errlist;
            $conditions['employeeId'] = request()->employeeId;
            $conditions['employeeName'] = request()->employeeName;
            $conditions['searchTarget'] = request()->searchTarget;
            $conditions['departments'] = request()->departments;

            // If the user search for something, save the history to session
            $this->setSession($conditions);

        } else {
            $conditions = $this->getSession() ? $this->getSession() : $conditions;
        }

        // And then we add the time parameters to the search conditions, then conduct the search.
        // After the search, remove the time parameters.
        $checklists = $this->getCheckList($conditions);

        $total_dakoku = $checklists['checklists_timestamp_error']->count();
        $total_hyou = $checklists['checklists_confirm_needed']->count();

        $timestamp_errors = collect($this->unifyRecordsByDate($checklists['checklists_timestamp_error']));
        $confirm_needed_errors = collect($this->unifyRecordsByDate($checklists['checklists_confirm_needed']));

        // Per request of Mrs.Yamauchi, we now will check the confirm_needed_errors list, and filter out all the day-employee that already exist
        // in the timestamp_errors list. So that, when a day-employee has both timestamp_error and confirm_needed_error, it will display erros in the
        // timestamp_errors list first, not all errors from both list at the same time.
        $confirm_needed_errors = $confirm_needed_errors->filter(function($value, $key) use ($timestamp_errors) {
            return !$timestamp_errors->has($key);
        });

        $checklistsJson = [
            'beginDate' => $beginDate,
            'endDate' => $endDate,
            'year' => $year,
            'month' => $month,
            'checklists_timestamp_error'=> $timestamp_errors->toArray(),
            'checklists_confirm_needed'=> $confirm_needed_errors->toArray(),
            'totaldakoku'=>$total_dakoku,
            'totalhyou'=>$total_hyou
        ];

        $result = [
            'checklistsJson' => $checklistsJson,
            'currentMonth'=>$currentMonth,
            'currentYear'=>$currentYear,
            'checklistsHistory' => $conditions,
        ];

		return $result;
    }

    /**
     * Unify all the checklists by date, and when doing so, enlarge the error_name
     *
     * @param collections   $checklists
     * @return array
     */
    protected function unifyRecordsByDate($checklists)
    {
        $result = [];

        foreach($checklists as $checklist) {

            // Check the unique by date AND employee_id
            $key = $checklist->date . '-' . $checklist->employee_id;

            if (!array_key_exists($key, $result)) {
                $result[$key] = $checklist->toArray();

            } else {
                if (strpos($result[$key]['error_name'], $checklist->error_name) === false) {
                    $result[$key]['error_name'] .= "/" . $checklist->error_name;
                }
            }
        }

        return $result;
    }

    /**
     * Filter out from the original set by the days_that_have_requests (the result will not contain the days_that_have_requests)
     *
     * @param Collection    $original_set
     * @param Collection    $days_that_have_requests
     * @return Collection
     */
    private function filterDaysThatHaveRequests($original_set, $days_that_have_requests)
    {
        return $original_set->filter(function($day) use ($days_that_have_requests) {
            $key = $day->date . '#' . $day->employee_id;
            return !isset($days_that_have_requests[$key]);
        });
    }
}
<?php

namespace App\Http\Controllers\Reusables;

use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use App\WorkStatus;
use App\ColorStatus;
use App\ChecklistItem;
use App\Events\WorkingTimestampChanged;
use App\Events\ResetWorkStatusColor;
use Carbon\Carbon;

trait TransferEmployeeWorkingDayTrait
{

    /**
     * Transfer the working information data from an instance of EmployeeWorkingDay to another instance.
     *
     * @param int       $employee_id
     * @param string    $from_date - format: YYYY-MM-DD
     * @param string    $to_date   - format: YYYY-MM-DD
     * @param int       $changer    the person who make this transfer (can be an instance of Manafer or Employee)
     * @return void
     */
    protected function transferWorkingDay($employee_id, $from_date, $to_date, $changer)
    {
        $from_working_day = EmployeeWorkingDay::where('employee_id', $employee_id)->where('date', $from_date)->notConcluded()->first();

        $to_working_day = EmployeeWorkingDay::where('employee_id', $employee_id)->where('date', $to_date)->notConcluded()->first();

        // If that day is not existing, create it
        if (!$to_working_day) {
            $to_working_day = $this->createEmployeeWorkingDayOnTheFly($employee_id, $to_date);
        }


        // Transfer all the  working_informations of this 'from_date' to the 'to_date'
        foreach ($from_working_day->employeeWorkingInformations as $working_info) {

            // And create the replaced working information for the 'from_date'
            $replaced_working_info = new EmployeeWorkingInformation();
            $replaced_working_info->planned_work_status_id = WorkStatus::FURIKYUU;
            $replaced_working_info->planned_work_location_id = $working_info->planned_work_location_id;
            $replaced_working_info->manually_modified = true;

            // Assign the the person who make this transfer as the last modify person of this instance.
            if (is_a($changer, Manager::class)) {
                $replaced_working_info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
            } else {
                $replaced_working_info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_EMPLOYEE;
            }
            $replaced_working_info->last_modify_person_id = $changer->id;

            $replaced_working_info->employeeWorkingDay()->associate($from_working_day);
            $replaced_working_info->save();

            // Before move the working info to the new working day, remove all the current checklist errors of this working info
            $this->removeChecklistErrorFromWorkingInfo($working_info);

            // Only change the work status to FURIDE if the current work status is not ZANGYOU
            if ($working_info->planned_work_status_id != WorkStatus::ZANGYOU) {
                $working_info->planned_work_status_id = WorkStatus::FURIDE;
            }
            $working_info->manually_modified = true;
            $working_info->employeeWorkingDay()->associate($to_working_day);
            $working_info = $this->makeTheWorkingInfoIndependent($working_info, $from_date, $to_date);
            $working_info = $this->removeRealDataFromWorkingInfo($working_info);
            $working_info->transfer_with_EWI_id = $replaced_working_info->id;
            $working_info->save();

            // Also reset the color of work_status if that color is not CONSIDERING's color
            $colors_of_this_info = $working_info->colorStatuses()->where('field_name', 'work_status_id')->orderBy('created_at')->get();
            if ($colors_of_this_info->isNotEmpty() && $colors_of_this_info->first()->field_css_class !== ColorStatus::CONSIDERING_COLOR) {
                event(new ResetWorkStatusColor($working_info));
            }
        }

        // Do this just in case. Maybe we don't nee to do this after all. Well, I'm not sure.
        event(new WorkingTimestampChanged($from_working_day));
        event(new WorkingTimestampChanged($to_working_day));

    }


    /**
     * Make the working information independent from the planned schedule, also recalibrate all the editable date time fields.
     *
     * @param  EmployeeWorkingInformation    $working_info
     * @return EmployeeWorkingInformation    $working_info
     */
    protected function makeTheWorkingInfoIndependent($working_info, $from_date, $to_date)
    {
        // Make the schedule part become independent( no longer be effect by the planned schedule). Also adjust them with the new working day
        $working_info->schedule_start_work_time = ($working_info->schedule_start_work_time !== null) ? $working_info->schedule_start_work_time : config('caeru.empty_date');
        $working_info->schedule_end_work_time = ($working_info->schedule_end_work_time !== null) ? $working_info->schedule_end_work_time : config('caeru.empty_date');
        $working_info->schedule_break_time = ($working_info->schedule_break_time !== null) ? $working_info->schedule_break_time : config('caeru.empty');
        $working_info->schedule_night_break_time = ($working_info->schedule_night_break_time !== null) ? $working_info->schedule_night_break_time : config('caeru.empty');
        $working_info->schedule_working_hour = ($working_info->schedule_working_hour !== null) ? $working_info->schedule_working_hour : config('caeru.empty_time');

        // Make these salaries fields independent from the planned schedule
        $working_info->basic_salary    = ($working_info->basic_salary !== null) ? $working_info->basic_salary : config('caeru.empty');
        $working_info->night_salary    = ($working_info->night_salary !== null) ? $working_info->night_salary : config('caeru.empty');
        $working_info->overtime_salary    = ($working_info->overtime_salary !== null) ? $working_info->overtime_salary : config('caeru.empty');
        $working_info->deduction_salary    = ($working_info->deduction_salary !== null) ? $working_info->deduction_salary : config('caeru.empty');
        $working_info->night_deduction_salary    = ($working_info->night_deduction_salary !== null) ? $working_info->night_deduction_salary : config('caeru.empty');
        $working_info->monthly_traffic_expense    = ($working_info->monthly_traffic_expense !== null) ? $working_info->monthly_traffic_expense : config('caeru.empty');
        $working_info->daily_traffic_expense    = ($working_info->daily_traffic_expense !== null) ? $working_info->daily_traffic_expense : config('caeru.empty');


        // Also we have to recalibrate all these editable 'planned_' date time fields, because they are still stick to the old working day
        $carbon_from_date = Carbon::createFromFormat('Y-m-d', $from_date);
        $carbon_to_date = Carbon::createFromFormat('Y-m-d', $to_date);

        $working_info->schedule_start_work_time         = $this->adjustTheDateOfThisAttribute($working_info->schedule_start_work_time, $carbon_from_date, $carbon_to_date);
        $working_info->schedule_end_work_time           = $this->adjustTheDateOfThisAttribute($working_info->schedule_end_work_time, $carbon_from_date, $carbon_to_date);
        $working_info->paid_rest_time_start             = $this->adjustTheDateOfThisAttribute($working_info->paid_rest_time_start, $carbon_from_date, $carbon_to_date);
        $working_info->paid_rest_time_end               = $this->adjustTheDateOfThisAttribute($working_info->paid_rest_time_end, $carbon_from_date, $carbon_to_date);
        $working_info->planned_early_arrive_start       = $this->adjustTheDateOfThisAttribute($working_info->planned_early_arrive_start, $carbon_from_date, $carbon_to_date);
        $working_info->planned_early_arrive_end         = $this->adjustTheDateOfThisAttribute($working_info->planned_early_arrive_end, $carbon_from_date, $carbon_to_date);
        $working_info->planned_overtime_start           = $this->adjustTheDateOfThisAttribute($working_info->planned_overtime_start, $carbon_from_date, $carbon_to_date);
        $working_info->planned_overtime_end             = $this->adjustTheDateOfThisAttribute($working_info->planned_overtime_end, $carbon_from_date, $carbon_to_date);

        return $working_info;
    }

    /**
     * Adjust the date of a given attribute's value (by add or subtract the number of days between the from_date and the end_date)
     *
     * @param string        $attribute_value
     * @param Carbon        $carbon_from_date
     * @param Carbon        $carbon_to_date
     * @return string
     */
    protected function adjustTheDateOfThisAttribute($attribute_value, $carbon_from_date, $carbon_to_date)
    {
        if ($attribute_value !== null) {

            $carbon_instance = Carbon::createFromFormat('Y-m-d H:i:s', $attribute_value);
            $diff = $carbon_to_date->diffInDays($carbon_from_date);

            if ($carbon_from_date->lt($carbon_to_date)) {
                $carbon_instance->addDays($diff);

            // The to_date must be smaller than the from_date in this case, there can't be an equal case.
            } else {
                $carbon_instance->subDays($diff);
            }
            return $carbon_instance->format('Y-m-d H:i:s');

        } else {
            return null;
        }
    }

    /**
     * Remove all the real data that has been assigned for this EmployeeWorkingInformation instance
     *
     * @param EmployeeWorkingInformation    $working_info
     * @return EmployeeWorkingInformation
     */
    protected function removeRealDataFromWorkingInfo($working_info)
    {
        $working_info->timestamped_start_work_time = null;
        $working_info->timestamped_start_work_time_work_location_id = null;
        $working_info->timestamped_end_work_time = null;
        $working_info->timestamped_end_work_time_work_location_id = null;
        $working_info->real_work_location_id = null;
        $working_info->real_work_address_id = null;
        $working_info->real_go_out_time = null;

        return $working_info;
    }

    /**
     * Delete any Checklist error currently associate with this working information.
     *
     * @param EmployeeWorkingInformation    $working_info
     * @return void
     */
    protected function removeChecklistErrorFromWorkingInfo($working_info)
    {
        ChecklistItem::where('employee_working_information_id', $working_info->id)->delete();
    }
}
<?php

namespace App\Http\Controllers\Reusables;

use App\EmployeeWorkingInformation;
use App\Events\CachedEmployeeWorkingInformationBecomeOld;

trait RefreshCachedEmployeeWorkingInformationTrait
{
    /**
     * Mark all the EmployeeWorkingInformations that relate to given WorkLocations (either one id or an array of ids)
     *
     * @param array|integer     $ids_or_id      array of ids or just one id of WorkLocation
     * @return void
     */
    protected function markRelatedEmployeeWorkingInformationAsOld($ids_or_id)
    {
        if ($ids_or_id === 'all') {
            event(new CachedEmployeeWorkingInformationBecomeOld([], true));
        } else if (is_array($ids_or_id)) {
            $related_employee_working_information_ids = EmployeeWorkingInformation::whereIn('planned_work_location_id', $ids_or_id)
                                                                                    ->orWhereHas('plannedSchedule', function($query) use ($ids_or_id) {
                                                                                        $query->whereIn('work_location_id', $ids_or_id);
                                                                                    })
                                                                                    ->orWhereHas('workAddressWorkingEmployee', function($query) use ($ids_or_id) {
                                                                                        $query->whereHas('plannedSchedule', function($query) use ($ids_or_id) {
                                                                                            $query->whereIn('work_location_id', $ids_or_id);
                                                                                        });
                                                                                    })
                                                                                    ->orWhereIn('real_work_location_id', $ids_or_id)
                                                                                    ->get()->pluck('id')->toArray();

            event(new CachedEmployeeWorkingInformationBecomeOld($related_employee_working_information_ids));
        } else {
            $related_employee_working_information_ids = EmployeeWorkingInformation::where('planned_work_location_id', '=', $ids_or_id)
                                                                                    ->orWhereHas('plannedSchedule', function($query) use ($ids_or_id) {
                                                                                        $query->where('work_location_id', '=', $ids_or_id);
                                                                                    })
                                                                                    ->orWhereHas('workAddressWorkingEmployee', function($query) use ($ids_or_id) {
                                                                                        $query->whereHas('plannedSchedule', function($query) use ($ids_or_id) {
                                                                                            $query->where('work_location_id', '=', $ids_or_id);
                                                                                        });
                                                                                    })
                                                                                    ->orWhere('real_work_location_id', '=', $ids_or_id)
                                                                                    ->get()->pluck('id')->toArray();

            event(new CachedEmployeeWorkingInformationBecomeOld($related_employee_working_information_ids));
        }
    }
}
<?php

namespace App\Http\Controllers\Reusables;

use App\Employee;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformationSnapshot;
use App\ChecklistItem;
use App\ChecklistErrorTimer;

trait CanCheckIfWorkingDayCanBeConcluded
{
    /**
     * Check if the given working day can be concluded. (It must not have any timestamp error)
     *
     * @param EmployeeWorkingDay        $working_day
     * @return -2|-1|0|1
     *          + -2:       this working day currently has considering request(s).
     *          + -1:       this working day has been concluded level two already
     *          + 0:        either have an timestamp error or the timer for a timestamp error
     *          + 1:        OK
     */
    protected function checkIfAWorkingDayCanBeConcluded(EmployeeWorkingDay $working_day)
    {
        if ($working_day->concluded_level_two == true) return -1;

        if ($working_day->isHavingConsideringRequest()) return -2;

        $has_timestamp_error = ChecklistItem::where('item_type', ChecklistItem::TIMESTAMP_ERROR)
                                                ->where('date', $working_day->date)
                                                ->where('employee_id', $working_day->employee_id)->exists();
        $has_checklist_timer = ChecklistErrorTimer::where('company_code', session('current_company_code'))
                                                    ->where('date', $working_day->date)
                                                    ->where('employee_id', $working_day->employee_id)->exists();

        return (!$has_timestamp_error && !$has_checklist_timer) ? 1 : 0;
    }

    /**
     * Check if the given working month can be concluded level one. (It must not have any timestamp error)
     *
     * @param Employee      $employee
     * @param Carbon        $business_month
     * @param Carbon        $start_date
     * @param Carbon        $end_date
     * @return -2|-1|0|1
     *          + -2:       this business_month has some days that currently have considering request(s)
     *          + -1:       this business_month has been concluded level two already
     *          + 0:        either have an timestamp error or the timer for a timestamp error
     *          + 1:        OK
     */
    protected function checkIfAWorkingMonthCanBeConcludedLevelOne(Employee $employee, $start_date, $end_date)
    {
        $already_concluded_level_two = EmployeeWorkingDay::where('employee_id', $employee->id)
                                                            ->where('date', '>=', $start_date->toDateString())->where('date', '<=', $end_date->toDateString())
                                                            ->where('concluded_level_two', true)
                                                            ->exists();

        if ($already_concluded_level_two) return -1;

        return $this->checkIfAWorkingMonthCanBeConcluded($employee, $start_date, $end_date);

    }

    /**
     * Check if a given working month can be concluded.
     *
     * @param Employee      $employee
     * @param Carbon        $start_date
     * @param Carbon        $end_date
     * @return -2|0|1
     *          + -2:       this business_month has some days that currently have considering request(s)
     *          + 0:        either have an timestamp error or the timer for a timestamp error
     *          + 1:        OK
     */
    protected function checkIfAWorkingMonthCanBeConcluded(Employee $employee, $start_date, $end_date)
    {
        $has_considering_request = EmployeeWorkingDay::where('employee_id', $employee->id)
                                                        ->where('date', '>=', $start_date->toDateString())->where('date', '<=', $end_date->toDateString())
                                                        ->whereHas('employeeWorkingInformationSnapshots', function($query) {
                                                            return $query->where('left_status', EmployeeWorkingInformationSnapshot::CONSIDERING);
                                                        })->exists();

        if ($has_considering_request) return -2;

        $has_timestamp_error = ChecklistItem::where('employee_id', $employee->id)
                                                ->where('date', '>=', $start_date->toDateString())->where('date', '<=', $end_date->toDateString())
                                                ->where('item_type', ChecklistItem::TIMESTAMP_ERROR)->exists();
        $has_checklist_timer = ChecklistErrorTimer::where('company_code', session('current_company_code'))
                                                    ->where('date', '>=', $start_date->toDateString())->where('date', '<=', $end_date->toDateString())
                                                    ->where('employee_id', $employee->id)->exists();

        return (!$has_timestamp_error && !$has_checklist_timer && !$has_considering_request) ? 1 : 0;
    }

    /**
     * Check if a given working day can be unconcluded level one. (It must not be concluded level two)
     *
     * @param EmployeeWorkingDay    $working_day
     * @return boolean
     */
    protected function checkIfAWorkingDayCanBeUnconcluded(EmployeeWorkingDay $working_day)
    {
        return !$working_day->concluded_level_two;
    }

    /**
     * Check if a working month can be unconcluded level one.(It must not contain any working day with concluded_level_two == true)
     *
     * @param Employee      $employee
     * @param Carbon        $start_date
     * @param Carbon        $end_date
     * @return boolean
     */
    protected function checkIfAWorkingMonthCanBeUnconcludedLevelOne(Employee $employee, $start_date, $end_date)
    {
        $has_concluded_level_two = EmployeeWorkingDay::where('employee_id', $employee->id)
                                                        ->where('date', '>=', $start_date->toDateString())->where('date', '<=', $end_date->toDateString())
                                                        ->where('concluded_level_two', true)
                                                        ->exists();

        return !$has_concluded_level_two;
    }
}
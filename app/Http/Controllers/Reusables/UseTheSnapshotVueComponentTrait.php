<?php

namespace App\Http\Controllers\Reusables;

use App\Employee;
use App\WorkLocation;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformationSnapshot;
use App\Setting;
use Carbon\Carbon;
use App\Services\NationalHolidayService;
use App\Services\WorkLocationSettingService;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;

trait UseTheSnapshotVueComponentTrait
{
    /**
     * The function will send to data of working day
     * snapshots is a array which all of snapshot
     * snapshot_day_color is a array which all of color in a snapshot
     * date is client sent
     * timestampes is a array which all of timestamp with working day
     * schedule_transfer_data is data of calendar
     *
     * @param format(yyyy-mm-dd)    $date,
     * @return object $object
     */
    protected function sendDataFollowWorkingDay($date, $employee)
    {
        $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee->id)->where('date', $date)->first();
        if (!$employee_working_day)
            $employee_working_day = $this->createEmployeeWorkingDayOnTheFly($employee->id, $date);

        $snapshots = $employee_working_day->mergeSnapshotsAndEmployeeWorkingInformations();
        $object = collect([
            'snapshots' => (count($snapshots) != 0) ? $snapshots : [$this->createDefaultSnapshot($employee_working_day)],
            'snapshot_day_color' => $this->getColorStatusWithEmployWorkingDay($employee_working_day),
            'date' => $employee_working_day->date,
            'timestampes' => $employee_working_day->workingTimestamps->sortBy('raw_date_time_value')->values(),
            'schedule_transfer_data' => [],
        ]);
        $this->sendDatePickerData($employee_working_day->employee_id);
        return $object;
    }

    /**
     * Another version of the function above. But without the loading data process.
     * Reason: so that we can leave the loading data to whoever call this function (so that they can eager loading from the outside).
     *
     * @param   EmployeeWorkingDay      $employee_working_day
     * @param   Employee                $employee
     * @return  Object                  $object
     */
    protected function sendDataFollowWorkingDayWithoutLoadingDataVersion($employee_working_day, $employee)
    {
        if (!$employee_working_day) {
            $employee_working_day = $this->createEmployeeWorkingDayOnTheFly($employee->id, $date);
        }

        $snapshots = $employee_working_day->mergeSnapshotsAndEmployeeWorkingInformationsWithoutLoadingDataVersion();
        $object = collect([
            'snapshots' => (count($snapshots) != 0) ? $snapshots : [$this->createDefaultSnapshot($employee_working_day)],
            'snapshot_day_color' => $this->getColorStatusWithEmployWorkingDay($employee_working_day),
            'date' => $employee_working_day->date,
            'timestampes' => $employee_working_day->workingTimestamps->sortBy('raw_date_time_value')->values(),
            'schedule_transfer_data' => [],
        ]);

        $this->sendDatePickerDataButReceiveWorkLocationIdVersion($employee->work_location_id);
        return $object;
    }

    /**
     * Check list date
     *
     * @param array format(yyyy-mm-dd)    $date_list
     * @retrun array $date_list
     */
    protected function checkDateList($date_list, $employee)
    {
        foreach ($date_list as $key => $date) {
            if (!$this->validateDateByFormat($date)) {
                array_splice($date_list, $key, 1);
            }
        }
        $employee_working_days = EmployeeWorkingDay::with(['employeeWorkingInformationSnapshots', 'employeeWorkingInformationSnapshots.colorStatuses' => function ($query) {
            $query->where('enable', true);
        }])->where('employee_id', $employee->id)->whereIn('date', $date_list)->get();

        $count = 0;
        foreach ($employee_working_days as $employee_working_day) {
            foreach ($employee_working_day->employeeWorkingInformationSnapshots as $snapshot) {
                if ($snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING
                    && $snapshot->left_switch_planned_schedule_target
                    && $snapshot->colorStatuses->contains('field_name', 'left_switch_planned_schedule_target')) {

                    $key = array_search($employee_working_day->date, $date_list);
                    array_splice($date_list, $key + $count + 1, 0, $snapshot->left_switch_planned_schedule_target);
                    $count++;
                }
                break;
            }
        }
        return array_unique($date_list);
    }

    /**
     * This function is an alternative way for the checkDateList. Because the logic flow which uses that function
     * has low efficiency (poorly designed).
     * Use this function to get all the extra EmployeeWorkingDays that is the target of the transfering request that exists in a given set of EmployeeWorkingDay
     * Then load those extra working days outside, instead of "put the extra day in an array, then reload all data again in another function (namely
     * 'sendDataFollowWorkingDay')".
     *
     * @param   Collection      $employee_working_days
     * @return  Collection
     */
    public function getTargetsOfWorkingDayTransfering($employee_working_days)
    {
        // Get the days that is the target of every transfering request that exists in the current in-range-working-days
        $extra_working_days = $employee_working_days->reduce(function($set, $working_day) {
            if ($working_day->employeeWorkingInformationSnapshots->isNotEmpty()) {
                foreach ($working_day->employeeWorkingInformationSnapshots as $snapshot) {
                    if ($snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING &&
                        $snapshot->left_switch_planned_schedule_target &&
                        $snapshot->colorStatuses->contains('field_name', 'left_switch_planned_schedule_target')) {
                            $set->push($snapshot->left_switch_planned_schedule_target);
                    }
                }
            }
            return $set;
        }, collect([]))->unique();

        return $extra_working_days;
    }

    /**
     * Use this function to extract the loaded target employee working day from the loaded collection.
     *
     * @param   EmployeeWorkingDay      $employee_working_day
     * @param   Collection              $loaded_target_working_day
     * @return  EmployeeWorkingDay|null
     */
    public function extractTheTargetWorkingDayFromALoadedCollectionOfTargetWorkingDays($employee_working_day, $loaded_target_working_day)
    {
        $transfer_request_snapshot = $employee_working_day->employeeWorkingInformationSnapshots->first(function($snapshot) {
            return $snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING &&
                $snapshot->left_switch_planned_schedule_target &&
                $snapshot->colorStatuses->contains('field_name', 'left_switch_planned_schedule_target');
        });

        if ($transfer_request_snapshot) {
            return $loaded_target_working_day[$transfer_request_snapshot->left_switch_planned_schedule_target];
        }

        return null;
    }

    /**
     * Create snapshot for default's case
     *
     * @param App\EmployeeWorkingDay            $employee_working_day,
     * @return $snapshot,
     *
     */
    protected function createDefaultSnapshot($employee_working_day)
    {
        $snapshot = new EmployeeWorkingInformationSnapshot([
            'left_timestamped_start_work_date'      => null,
            'left_timestamped_start_work_time'      => null,
            'left_timestamped_end_work_date'        => null,
            'left_timestamped_end_work_time'        => null,
            'left_planned_early_arrive_start'       => null,
            'left_planned_early_arrive_end'         => null,
            'left_planned_overtime_start'           => null,
            'left_planned_overtime_end'             => null,
            'left_planned_break_time'               => null,
            'left_real_break_time'                  => null,
            'left_planned_night_break_time'         => null,
            'left_real_night_break_time'            => null,
            'left_planned_late_time'                => null,
            'left_planned_early_leave_time'         => null,
            'left_planned_go_out_time'              => null,
            'schedule_working_hour'                 => null,
        ]);
        $snapshot->employee_working_day_id = $employee_working_day->id;
        return $snapshot;
    }

    /**
     * The function will return the list of color follow employee working day
     *
     * @param App\EmployWorkingDay       $employee_working_day
     * @return array $list_color_status_for_working_day
     */
    protected function getColorStatusWithEmployWorkingDay($employee_working_day)
    {
        $list_color_status_for_working_day= collect([]);
        foreach ($employee_working_day->employeeWorkingInformations as $employee_working_information) {
            if ($employee_working_information->employeeWorkingInformationSnapshot)
                $list_color_status_for_working_day->push($employee_working_information->employeeWorkingInformationSnapshot->colorStatuses);
            else
                $list_color_status_for_working_day->push([]);
        }
        if (count($list_color_status_for_working_day) == 0) {
            $snapshotForFurideCases = $employee_working_day->employeeWorkingInformationSnapshots->filter(function($snapshot) {
                return $snapshot->employee_working_information_id == null && $snapshot->soon_to_be_EWI_id != null;
            });
            foreach ($snapshotForFurideCases as $snapshotForFurideCase) {
                $list_color_status_for_working_day->push($snapshotForFurideCase->colorStatuses()->get());
            }
            if(count($list_color_status_for_working_day) == 0) {
                $snapshot = $employee_working_day->employeeWorkingInformationSnapshots->first(function($snapshot) {
                    return $snapshot->employee_working_information_id == null && $snapshot->soon_to_be_EWI_id == null;
                });
                if ($snapshot)
                    $list_color_status_for_working_day->push($snapshot->colorStatuses()->get());
            }
        }
        return $list_color_status_for_working_day;
    }

    /**
     * Send data to the normal date picker
     *
     * @param integer       $employee_id,
     * @return void
     */
    protected function sendDatePickerData($employee_id)
    {
        $work_location = Employee::find($employee_id)->workLocation;

        if ($work_location) {

            $rest_days = $work_location->getRestDays();

            $national_holidays_service = resolve(NationalHolidayService::class);
            $national_holidays = $national_holidays_service->get();

            Javascript::put([
                'rest_days'             => $rest_days,
                'national_holidays'     => $national_holidays,
                'flip_color_day'        => $work_location->currentSetting()->salary_accounting_day,
            ]);
        }
    }

    /**
     * The same as the above function but instead of an employee_id, receive a work_location_id.
     * And then use the Server to load that WorkLocation.
     *
     * @param integer       $work_location_id,
     * @return void
     */
    protected function sendDatePickerDataButReceiveWorkLocationIdVersion($work_location_id)
    {
        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $work_location = $work_location_setting_service->getWorkLocationById($work_location_id);

        if ($work_location) {

            $rest_days = $work_location->getRestDays();

            $national_holidays_service = resolve(NationalHolidayService::class);
            $national_holidays = $national_holidays_service->get();

            Javascript::put([
                'rest_days'             => $rest_days,
                'national_holidays'     => $national_holidays,
                'flip_color_day'        => $work_location->currentSetting()->salary_accounting_day,
            ]);
        }
    }
}
<?php

namespace App\Http\Controllers\Reusables;

use Auth;
use App\WorkLocation;
use App\Services\NationalHolidayService;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;

trait SendDatePickerDataBaseOnCurrentWorkLocationTrait
{
    /**
     * Send data to the normal date picker.
     *
     * Updated: now can take optional parameters: eager loaded work location OR eager loaded company.
     * If the work location/company has been eager loaded somewhere, we can take it from the parameter instead.
     *
     * @return void
     */
    protected function sendDatePickerData($eager_loaded_work_location = null, $eager_loaded_company = null)
    {
        if ($eager_loaded_work_location) {
            $setting = $eager_loaded_work_location->currentSetting();
            $rest_days = $eager_loaded_work_location->getRestDays();

        } else if ($eager_loaded_company) {
            $setting = $eager_loaded_company->setting;
            $rest_days = $eager_loaded_company->calendarRestDays()->get(['assigned_date', 'type'])->keyBy('assigned_date')->toArray();

        } else {
            $work_location_id = session('current_work_location');

            if (is_numeric($work_location_id)) {
                $work_location = WorkLocation::find($work_location_id);

                $setting = $work_location->currentSetting();
                $rest_days = $work_location->getRestDays();

            } elseif ($work_location_id === 'all') {
                $company = Auth::user()->company;

                $setting = $company->setting;
                $rest_days = $company->calendarRestDays()->get(['assigned_date', 'type'])->keyBy('assigned_date')->toArray();
            }
        }

        $national_holiday_service = resolve(NationalHolidayService::class);
        $national_holidays = $national_holiday_service->get();

        Javascript::put([
            'date_picker_data' => [
                'rest_days'             => isset($rest_days) ? $rest_days : [],
                'national_holidays'     => $national_holidays,
                'flip_color_day'        => isset($setting) ? $setting->salary_accounting_day : null,
            ],
        ]);

    }
}
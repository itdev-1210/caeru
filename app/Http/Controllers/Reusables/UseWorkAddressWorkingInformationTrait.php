<?php

namespace App\Http\Controllers\Reusables;

use App\WorkAddressWorkingInformation;


trait UseWorkAddressWorkingInformationTrait
{
    /**
     * Hide some relationship of WorkAddressWorkingInformation to avoid endless looping while loading relationships.
     *
     * @param WorkAddressWorkingInformation     $working_info
     * @return void
     */
    protected function hideSomeRelationshipsOfWorkAddressWorkingInformation(WorkAddressWorkingInformation $working_info)
    {
        foreach($working_info->workAddressWorkingEmployees as $employee) {
            $employee->makeHidden('employeeWorkingInformation');
        }
    }
}
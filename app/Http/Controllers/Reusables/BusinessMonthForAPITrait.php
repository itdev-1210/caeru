<?php

namespace App\Http\Controllers\Reusables;

use App\Http\Controllers\Reusables\BusinessMonthTrait;
use Carbon\Carbon;
use App\Employee;
use App\EmployeeWorkingDay;
use App\WorkAddress;
use App\WorkLocation;

trait BusinessMonthForAPITrait
{
    use BusinessMonthTrait;

    /**
     * This function would be fetch data necessary according to business month for api
     * But this data is simpler than the data in BusinessMonthTrait
     *
     * @param Employee      $employee
     * @param Employee      $business_month
     * @return Array
     */
    protected function getDataAccordingBusinessMonthForAPI(Employee $employee, $business_month)
    {
        if (!isset($business_month))
            $business_month = $this->getBusinessMonthThatContainASpecificDay($employee->workLocation->currentSetting());
        else
            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');

        $data = $this->getWorkingDataFromBusinessMonth($employee, $business_month);

        return [
            'data'    => $data->toArray(),
            'business_month'  => $business_month->format('Y-m'),
        ];
    }

    /**
     * Get the working data from a business month. This is basically a function to translate the business_month into start_date and end_date.
     *
     * @param Employee  $employee
     * @param Carbon    $business_month
     * @param array     that have two keys: summarized_data, and working_days
     */
    private function getWorkingDataFromBusinessMonth(Employee $employee, $business_month)
    {
        $range = $this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);

        return $this->getWorkingDataFromDateRangeOnlyStartTimeAndEndTime($employee, $range[0], $range[1]);
    }

    /**
     * The function only get real start time, real end time and real break time each day in this business month
     *
     * @param Employee  $employee
     * @param Carbon    $start_date
     * @param Carbon    $end_date
     * @return Array
     */
    private function getWorkingDataFromDateRangeOnlyStartTimeAndEndTime($employee, $start_date, $end_date)
    {
        $data = collect([]);

        // Load employee working day data
        $working_days = EmployeeWorkingDay::with([
            'concludedLevelOneManager',
            'concludedLevelTwoManager',
            'concludedEmployeeWorkingDay',
            'substituteEmployeeWorkingInformations',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            'employeeWorkingInformations.colorStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.setting',
            'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.setting',
            'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
        ])
        ->where('employee_id', $employee->id)
        ->where('date', '>=', $start_date->toDateString())
        ->where('date', '<=', $end_date->toDateString())
        ->get();

        // Increase the end_date by one so that we can include the end_date in the loop
        $end_date->addDay();

        while ($start_date->toDateString() !== $end_date->toDateString()) {

            $working_day_with_data = $working_days->first(function ($working_day) use ($start_date) {
                return $working_day->date === $start_date->toDateString();
            });

            if ($working_day_with_data) {
                $check = false;

                $working_infos = $working_day_with_data->employeeWorkingInformations;

                foreach ($working_infos as $working_info) {
                    if (!empty($working_info->timestamped_start_work_time) || !empty($working_info->timestamped_end_work_time)) {

                        if (!$check)
                            $check = true;

                        $red_number_setting = $working_info->getRedNumberSetting();

                        $location = !empty($working_info->real_work_address_id) ? WorkAddress::find($working_info->real_work_address_id)->name
                                        : WorkLocation::find($working_info->real_work_location_id)->name;

                        $data->push(collect([
                            'date' => $start_date->toDateString(),
                            'location' => $location,
                            'start_time' => !empty($working_info->timestamped_start_work_time) ? $working_info->timestamped_start_work_time : null,
                            'start_time_red_number' => isset($red_number_setting['start_time_red_number']) ? $red_number_setting['start_time_red_number'] : false,
                            'end_time' => !empty($working_info->timestamped_end_work_time) ? $working_info->timestamped_end_work_time : null,
                            'end_time_red_number' => isset($red_number_setting['end_time_red_number']) ? $red_number_setting['end_time_red_number'] : false,
                            'real_go_out_time' => !empty($working_info->real_go_out_time) ? $working_info->real_go_out_time : null,
                            'go_out_red_number' => isset($red_number_setting['go_out_red_number']) ? $red_number_setting['go_out_red_number'] : false,
                        ]));
                    }
                }

                if (!$check)
                    $data->push($this->createDefaultValue($start_date->toDateString()));

            } else {
                $data->push($this->createDefaultValue($start_date->toDateString()));
            }

            $start_date->addDay();
        }

        return $data;
    }

    /**
     * The function will create default value
     *
     * @param yyyy-mm-dd  $date
     * @return collection
     */
    private function createDefaultValue($date)
    {
        return collect([
            'date' => $date,
            'location' => null,
            'start_time' => null,
            'start_time_red_number' => false,
            'end_time' => null,
            'end_time_red_number' => false,
            'real_go_out_time' => null,
            'go_out_red_number' => false,
        ]);
    }
}
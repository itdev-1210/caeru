<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\WorkLocation;
use App\Employee;
use App\Setting;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformationModifyRequest;
use App\ChecklistItem;

class TotalizationController extends Controller
{
	/**
	 * Create a new controller instance
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('choose');

		// $this->middleware('can:view_work_data_calculation');
		//$this->middleware('can:change_work_data_calculation');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response.
	 */

      /**
     * Gets the session.
     *
     * @param mixed $search_history_conditions
     */
    private function getSession() 
    {
        if(session()->has('checklist_search_history')) {
            return session()->get('checklist_search_history');
        }
        return null;
    }

    /**
     * Sets the session.
     *
     * @param  $search_history_conditions
     */
    private function setSession($search_history_conditions)
    {
        session(['checklist_search_history' => $search_history_conditions]);
    }

	/**
     * Show totalization
     */
    public function index(Request $request)
	{
		$current_work_location = request()->session()->get('current_work_location');
		list($beginDate, $endDate, $year, $month, $currentYear, $currentMonth) = $this->getDate(); 
		//$employees = Employee::getCountError()->paginate(20);
		// $test = Employee::find(4)->hasConcludedOne();
		$employees = Employee::leftJoin('work_locations', function($join) {
            $join->on('work_locations.id', '=', 'employees.work_location_id');
             })
        	->workLocations($current_work_location)
        	//->workingDay($beginDate, $beginDate)
        	->groupBy('employees.id')
        	->select('employees.*')
        	//->get();
        	
			 ->paginate(20);
    	collect($employees->items())
    	->each(function($employee) use ($beginDate,$endDate) {
    		$employee['totalRequestStatus'] = $employee->countRequestStatus(EmployeeWorkingInformationModifyRequest::APPROVED) + $employee->countRequestStatus(EmployeeWorkingInformationModifyRequest::CONSIDERING);
    		$employee['approvedRequestStatus'] = $employee->countRequestStatus(EmployeeWorkingInformationModifyRequest::APPROVED);
    		$employee['consideringRequeststatus'] = $employee->countRequestStatus(EmployeeWorkingInformationModifyRequest::CONSIDERING);
    		$employee['totalConfirmNeeded'] = $employee->countItemType(ChecklistItem::CONFIRM_NEEDED, $beginDate,$endDate);
    		$employee['totalTimestampError'] = $employee->countItemType(ChecklistItem::TIMESTAMP_ERROR, $beginDate,$endDate);
    		$employee['hasConcludedOne'] = $employee->hasConcludedOne($beginDate,$endDate);
    		$employee['hasConcludedTwo'] = $employee->hasConcludedTwo($beginDate,$endDate);
    	});
		$totalizationsJson = [
        'beginDate' => $beginDate,
        'endDate' => $endDate,
        'employees'=> $employees,
        ];
        		
			return view('totalization.list')->with([
			'totalizationsJson'=> json_encode($totalizationsJson),
			'employees'=>$employees,
			'currentMonth'=>$currentMonth,
			'currentYear'=>$currentYear,
			'beginDate'=>$beginDate,
			'endDate'=>$endDate,
		]);
	}

	/**
	 * Searches for the first match.
	 */
	public function search($refreshSession=false) {
        $current_work_location = request()->session()->get('current_work_location');
		list($beginDate, $endDate, $year, $month, $currentYear, $currentMonth) = $this->getDate(); 
		//$employees = Employee::getCountError()->paginate(20);
		// $test = Employee::find(4)->hasConcludedOne();
		
        $conditions = [
            'beginDate' => $beginDate,
            'endDate' => $endDate,
            'cklist' => request()->cklist,
            'employeeId' => request()->employeeId,
            'employeeName' => request()->employeeName,
            'yearHistory'=>$year,
            'monthHistory'=>$month
        ];
        //$search_history_conditions = $conditions;
        $search_history_conditions = $refreshSession?($this->getSession()?:$conditions):$conditions;
        $this->setSession($search_history_conditions);

         $totalizations = $this->getTotalLizations($search_history_conditions);
        // $totaldakoku = $checklists['checklists_timestamp_error']->count();
        // $totalhyou = $checklists['checklists_confirm_needed']->count();

        $totalizationsJson = [
        'beginDate' => $beginDate,
        'endDate' => $endDate,
        'employees'=> $totalizations,
        ];
        $result = [
            'totalizationsJson' => $totalizationsJson,
            // 'currentMonth'=>$currentMonth,
            // 'currentYear'=>$currentYear,
            'totalizationsHistory' => $search_history_conditions,
        ];

		return $result;
	}
	/**
     * Gets the totalizations with conditions.
     *
     * @param array $search_history_conditions
     *
     */
    private function getTotalLizations(array $search_history_conditions)
	{	
		$current_work_location = request()->session()->get('current_work_location');
		$beginDate = $search_history_conditions['beginDate'];
		$endDate = $search_history_conditions['endDate'];
		$employees = Employee::leftJoin('work_locations', function($join) {
            $join->on('work_locations.id', '=', 'employees.work_location_id');
             })
        	->workLocations($current_work_location)
        	//->workingDay($beginDate, $beginDate)
        	->groupBy('employees.id')
        	->select('employees.*')
        	//->get();
        	->searchBaseRequestStatus($search_history_conditions['cklist'])
            ->searchBaseChecklistType($search_history_conditions['cklist'])
            ->searchBaseHasNotConcludedLevelTwo($search_history_conditions['cklist'])
            ->searchEmployeeName($search_history_conditions['employeeName'])
            ->searchEmployeeId($search_history_conditions['employeeId'])
			->paginate(20);
    	collect($employees->items())
    	->each(function($employee) use ($beginDate,$endDate) {
    		$employee['totalRequestStatus'] = $employee->countRequestStatus(EmployeeWorkingInformationModifyRequest::APPROVED) + $employee->countRequestStatus(EmployeeWorkingInformationModifyRequest::CONSIDERING);
    		$employee['approvedRequestStatus'] = $employee->countRequestStatus(EmployeeWorkingInformationModifyRequest::APPROVED);
    		$employee['consideringRequeststatus'] = $employee->countRequestStatus(EmployeeWorkingInformationModifyRequest::CONSIDERING);
    		$employee['totalConfirmNeeded'] = $employee->countItemType(ChecklistItem::CONFIRM_NEEDED, $beginDate,$endDate);
    		$employee['totalTimestampError'] = $employee->countItemType(ChecklistItem::TIMESTAMP_ERROR, $beginDate,$endDate);
    		$employee['hasConcludedOne'] = $employee->hasConcludedOne($beginDate,$endDate);
    		$employee['hasConcludedTwo'] = $employee->hasConcludedTwo($beginDate,$endDate);
    	});
    	return $employees;
	}
    /**
     * Gets the date.
     *
     * @return array
     */
    private function getDate() {
        $current_work_location = request()->session()->get('current_work_location');
        $settingDate = (new Setting)->getSalaryDay($current_work_location);

        if(!$settingDate) throw new \Exception('Query invalid, cause settingDate not exists');
        $currentMonth = Carbon::now()->month;
        $currentYear = Carbon::now()->year;

        $year = request()->year?:$currentYear;
        $month = request()->month?:$currentMonth;

        $dateCompute = Carbon::createFromDate($year, $month, 1);
        $beginCompute = $dateCompute->copy()->subMonth();
        $endCompute = $dateCompute->copy();

        $yearBeginCompute = $beginCompute->year;
        $monthBeginCompute = $beginCompute->month;
        $dayBeginCompute = $settingDate;

        $beginDate = Carbon::create($yearBeginCompute, $monthBeginCompute, $dayBeginCompute, 0, 0, 0);
        $endDate = $beginDate->copy()->addMonth()->subDay();

        $dayOfWeekBegin = $beginDate->dayOfWeek;
        $dayOfWeekEnd = $endDate->dayOfWeek;
        
        return [$beginDate, $endDate, $year, $month, $currentYear, $currentMonth];
    }
}
<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Routing\Controller;
use App\WorkStatus;
use App\RestStatus;
use App\EmployeeWorkingDay;
use App\WorkAddressWorkingEmployee;
use App\EmployeeWorkingInformation;
use App\WorkAddressWorkingInformation;
use App\Events\WorkingTimestampChanged;
use App\Events\CachedEmployeeWorkingInformationBecomeOld;
use App\Http\Requests\WorkAddressWorkingEmployeeRequest;
use App\Http\Controllers\Reusables\ManageWorkAddressWorkingEmployeeTrait;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;
use App\Http\Controllers\Reusables\ChangeBackGroundColorOfAttributesTrait;

class WorkAddressWorkingEmployeeController extends Controller
{
    use CanCreateWorkingDayOnTheFlyTrait, ManageWorkAddressWorkingEmployeeTrait, ChangeBackGroundColorOfAttributesTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose');
        $this->middleware('can:change_attendance_work_address_working_day_page');
    }

    /**
     * Store the new WorkAddressWorkingEmployee instance
     *
     * @param WorkAddressWorkingEmployeeRequest      $request
     * @return array|json
     */
    public function store(WorkAddressWorkingEmployeeRequest $request)
    {
        $new_working_employee = new WorkAddressWorkingEmployee($request->only([
            'work_address_working_information_id',
            'working_confirm',
            'employee_id',
        ]));

        // Check if this WorkAddressWorkingInformation can add more WorkAddressWorkingEmployee
        if (!$this->canAddMoreWorkingEmployeeTo($new_working_employee->work_address_working_information_id, true)) {
            return response()->json(['error' => '必要人数を満たしています。'], 401);
        }

        $new_working_employee->schedule_modified = true;

        $new_working_employee->modified_manager_id = $request->user()->id;

        // Find the suitable employee_working_day_id
        if (!$new_working_employee->employee_working_day_id) {
            $employee_id = $new_working_employee->employee_id;

            $work_address_working_info = WorkAddressWorkingInformation::find($new_working_employee->work_address_working_information_id);
            $date = $work_address_working_info->workAddressWorkingDay->date;

            $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee_id)->where('date', $date)->first();

            if (!$employee_working_day) {
                $employee_working_day =$this->createEmployeeWorkingDayOnTheFly($employee_id, $date);
            }

            $new_working_employee->employee_working_day_id = $employee_working_day->id;
        }

        $new_working_employee->save();
        $this->saveTheCurrentAttributesValueOfTheWorkAddressWorkingInformation($new_working_employee->work_address_working_information_id);

        // Create new EWI if this WAWE has working_confirm = true
        if ($new_working_employee->working_confirm == true) {
            $additional_data = $request->only([
                'schedule_break_time_changes',
                'schedule_night_break_time_changes',
                'planned_work_status_id',
                'planned_rest_status_id',
            ]);
            $this->createNewEmployeeWorkingInformation($new_working_employee, $additional_data);

            // Re-distribute WorkingTimestamps and re-check ChecklistError
            event(new WorkingTimestampChanged($employee_working_day));
        }

        $new_working_employee->makeHidden('workAddressWorkingInformation');
        if ($new_working_employee->working_confirm) $new_working_employee->makeHidden('employeeWorkingInformation');

        return [
            'success' => '保存しました',
            'data' => $new_working_employee,
        ];
    }

    /**
     * Update a WordAddressWorkingEmployee model
     *
     * @param WorkAddressWorkingEmployeeRequest         $request
     * @param WorkAddressWorkingEmployee                $work_address_working_employee
     * @return array|json
     */
    public function update(WorkAddressWorkingEmployeeRequest $request, WorkAddressWorkingEmployee $work_address_working_employee)
    {
        if ($work_address_working_employee->working_confirm) {
            $employee_working_info = $work_address_working_employee->employeeWorkingInformation;

            $employee_working_info->fill($request->only([
                'planned_work_status_id',
                'planned_rest_status_id',
            ]));

            // Change the time data from 'schedule_' attributes to the designated 'planned_' attributes and vice versa.
            if ($request->input('planned_work_status_id') !== $employee_working_info->getOriginal('planned_work_status_id')) {
                if (($request->input('planned_work_status_id') == WorkStatus::HOUDE || $request->input('planned_work_status_id') == WorkStatus::KYUUDE) &&
                    ($employee_working_info->getOriginal('planned_work_status_id') != WorkStatus::HOUDE && $employee_working_info->getOriginal('planned_work_status_id') != WorkStatus::KYUUDE)) {
                    $this->moveTimeDataToPlannedAttributes($employee_working_info);
                } else if (($employee_working_info->getOriginal('planned_work_status_id') == WorkStatus::HOUDE || $employee_working_info->getOriginal('planned_work_status_id') == WorkStatus::KYUUDE) &&
                    ($request->input('planned_work_status_id') != WorkStatus::HOUDE && $request->input('planned_work_status_id') != WorkStatus::KYUUDE)) {
                    $this->moveTimeDataToScheduleAttributes($employee_working_info);
                }
            }

            // Assign the break_time and night_break_time and change the color respectively
            if ($request->input('schedule_break_time_changes') !== null) {
                if ($request->input('planned_work_status_id') != null && ($request->input('planned_work_status_id') == WorkStatus::HOUDE || $request->input('planned_work_status_id') == WorkStatus::KYUUDE)) {
                    $employee_working_info->planned_break_time = $request->input('schedule_break_time_changes');
                    $this->createColorStatus($employee_working_info->id, 'planned_break_time');
                    $this->changeColorOfSnapshot($employee_working_info, 'planned_break_time');
                } else {
                    $employee_working_info->schedule_break_time = $request->input('schedule_break_time_changes');
                    $employee_working_info->manually_inputed_schedule_break_time = true;
                }

                // Update the pocket break time of this WAWE instance as well
                $work_address_working_employee->pocket_break_time = $request->input('schedule_break_time_changes');
            }

            if ($request->input('schedule_night_break_time_changes') !== null) {
                if ($request->input('planned_work_status_id') != null && ($request->input('planned_work_status_id') == WorkStatus::HOUDE || $request->input('planned_work_status_id') == WorkStatus::KYUUDE)) {
                    $employee_working_info->planned_night_break_time = $request->input('schedule_night_break_time_changes');
                    $this->createColorStatus($employee_working_info->id, 'planned_night_break_time');
                    $this->changeColorOfSnapshot($employee_working_info, 'planned_night_break_time');
                } else {
                    $employee_working_info->schedule_night_break_time = $request->input('schedule_night_break_time_changes');
                    $employee_working_info->manually_inputed_schedule_night_break_time = true;
                }

                // Update the pocket night break time of this WAWE instance as well
                $work_address_working_employee->pocket_night_break_time = $request->input('schedule_night_break_time_changes');
            }

            // Check if number of WorkingEmployee exceed candidate number
            if (!$this->checkWorkingEmployeeNumber($work_address_working_employee, $request->input('planned_work_status_id'), $request->input('planned_rest_status_id'))) {
                return response()->json(['error' => '必要人数を満たしています。'], 401);
            }

            $employee_working_info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
            $employee_working_info->last_modify_person_id = $request->user()->id;
            $employee_working_info->manually_modified = true;
            $employee_working_info->temporary = false;
            $employee_working_info->save();

            // Re-distribute WorkingTimestamps and re-check ChecklistError
            event(new WorkingTimestampChanged($work_address_working_employee->employeeWorkingDay));

            $work_address_working_employee->schedule_modified = true;
            $work_address_working_employee->modified_manager_id = $request->user()->id;
            $work_address_working_employee->save();
            $this->saveTheCurrentAttributesValueOfTheWorkAddressWorkingInformation($work_address_working_employee->work_address_working_information_id);


            // If this WorkAddressWorkingEmployee has an EmployeeWorkingInformation, then the EmployeeWorkingInformation's cache need to be refreshed.
            if ($work_address_working_employee->employeeWorkingInformation) {
                event(new CachedEmployeeWorkingInformationBecomeOld([$work_address_working_employee->employeeWorkingInformation->id]));
            }
        }

        $work_address_working_employee->makeHidden('employeeWorkingInformation');
        $work_address_working_employee->makeHidden('workAddressWorkingInformation');

        return [
            'success' => '保存しました',
            'data' => $work_address_working_employee,
        ];
    }

    /**
     * Delete a WorkAddressWorkingEmployee instance.
     *
     * @param WorkAddressWorkingEmployee    $work_address_working_employee
     * @return array|json
     */
    public function destroy(WorkAddressWorkingEmployee $work_address_working_employee)
    {
        // Not delete WorkAddressWorkingInformation that has working confirm true
        if ($work_address_working_employee->working_confirm == true) {
            return response()->json(['error' => '削除は出来ませんでした。'], 401);
        }

        $work_address_working_employee->delete();
        return [ 'success' => '削除しました' ];
    }

    /**
     * Toggle the working_confirm on a WorkAddressWorkingEmployee instance.
     *
     * @param WorkAddressWorkingEmployee    $work_address_working_employee
     * @return array|json
     */
    public function toggle(WorkAddressWorkingEmployee $work_address_working_employee)
    {
        $work_address_working_employee->working_confirm = !$work_address_working_employee->working_confirm;

        // Check if this WorkAddressWorkingInformation can add more WorkAddressWorkingEmployee
        if ($work_address_working_employee->working_confirm == true && !$this->canAddMoreWorkingEmployeeTo($work_address_working_employee->work_address_working_information_id, true)) {
            return response()->json(['error' => '必要人数を満たしています。'], 401);
        }

        // Not allow to toggle off the WorkAddressWorkingInformation that has:
        //      - EmployeeWorkingInformation that has timestamps
        //      - EmployeeWorkingInformation that has work status or rest status
        //      - EmployeeWorkingDay concluded
        if ($work_address_working_employee->working_confirm == false &&
                ($work_address_working_employee->timestamps_exist_or_not == true ||
                $work_address_working_employee->employeeWorkingDay->isConcluded() ||
                $work_address_working_employee->employeeWorkingInformation->planned_work_status_id ||
                $work_address_working_employee->employeeWorkingInformation->planned_rest_status_id)
            ) {
            return response()->json(['error' => '削除は出来ませんでした。'], 401);
        }

        // If the working_confirm is turn to true while this instace has no EmployeeWorkingInformation, then create one and opposite.
        if ($work_address_working_employee->working_confirm && !$work_address_working_employee->employeeWorkingInformation) {

            // Now we have to make the new EWI have the break time from the pocket of the WAWE
            $additional_data = [
                'schedule_break_time_changes' => $work_address_working_employee->pocket_break_time,
                'schedule_night_break_time_changes' => $work_address_working_employee->pocket_night_break_time,
            ];

            $this->createNewEmployeeWorkingInformation($work_address_working_employee, $additional_data);

            // Re-distribute WorkingTimestamps and re-check ChecklistError
            event(new WorkingTimestampChanged($work_address_working_employee->employeeWorkingDay));

        } else if (!$work_address_working_employee->working_confirm && $work_address_working_employee->employeeWorkingInformation) {

            // If this working employee still doesn't have pocket_break/night_break_time, set it using EmployeeWorkingInformation's attributes
            if (!$work_address_working_employee->pocket_break_time) {
                if ($work_address_working_employee->employeeWorkingInformation->schedule_break_time) {
                    $work_address_working_employee->pocket_break_time = $work_address_working_employee->employeeWorkingInformation->schedule_break_time;
                } else {
                    $work_address_working_employee->pocket_break_time = $work_address_working_employee->employeeWorkingInformation->planned_break_time;
                }
            }
            if (!$work_address_working_employee->pocket_night_break_time) {
                if ($work_address_working_employee->employeeWorkingInformation->schedule_night_break_time) {
                    $work_address_working_employee->pocket_night_break_time = $work_address_working_employee->employeeWorkingInformation->schedule_night_break_time;
                } else {
                    $work_address_working_employee->pocket_night_break_time = $work_address_working_employee->employeeWorkingInformation->planned_night_break_time;
                }
            }

            // Delete the EmployeeWorkingInformation
            $work_address_working_employee->employeeWorkingInformation->delete();
        }

        $work_address_working_employee->save();
        $work_address_working_employee->load('employeeWorkingInformation');

        // Hidden employeeWorkingInformation and anyway that lead to that model, to avoid infinite loop
        $work_address_working_employee->makeHidden('workAddressWorkingInformation');
        if ($work_address_working_employee->working_confirm) $work_address_working_employee->makeHidden('employeeWorkingInformation');

        return [
            'success' => '保存しました',
            'data' => $work_address_working_employee,
        ];
    }

    /**
     * Check the if the real (going-to-be) working employee number is exceeding the parent working info's candidate number
     * We have to use this function because we can't use the function canAddMoreWorkingEmployeeTo in the trait (various reasons,
     * mainly because of the EmployeeWorkingInformation still hasn't been persisted to database)
     *
     * @param   \App\WorkAddressWorkingEmployee         $work_address_working_employee
     * @param   integer                                 $new_work_status
     * @param   integer                                 $new_rest_status
     * @return  boolean
     */
    protected function checkWorkingEmployeeNumber($work_address_working_employee, $new_work_status, $new_rest_status)
    {
        // Check if this WorkAddressWorkingInformation has number of workAddressWorkingEmployees exceeding candidate number
        $working_info = $work_address_working_employee->workAddressWorkingInformation;
        $wawe_with_minimal_data = $working_info->workAddressWorkingEmployees->keyBy('id')->map(function($working_employee) {
            if ($working_employee->working_confirm == true) {
                return [
                    'working_confirm' => $working_employee->working_confirm,
                    'work_status' => $working_employee->employeeWorkingInformation->planned_work_status_id,
                    'rest_status' => $working_employee->employeeWorkingInformation->planned_rest_status_id,
                ];
            }
        });
        $wawe_with_minimal_data->forget($work_address_working_employee->id);
        $wawe_with_minimal_data->push([
            'working_confirm'   => $work_address_working_employee->working_confirm,
            'work_status'       => $new_work_status,
            'rest_status'       => $new_rest_status,
        ]);
        $rest_statuses = RestStatus::all()->keyBy('id');

        $real_working_employee = $wawe_with_minimal_data->filter(function($wawe) use ($rest_statuses) {
            return $wawe['working_confirm'] == true &&
                $wawe['work_status'] != WorkStatus::KEKKIN &&
                $wawe['work_status'] != WorkStatus::FURIKYUU &&
                !($wawe['rest_status'] != null && $rest_statuses[$wawe['rest_status']]->unit_type == 1);
        });

        return !($real_working_employee->count() > $working_info->candidate_number);
    }

    /**
     * Save the current value of the attributes of the given WorkAddressWorkingInformation.
     *
     * @param int|WorkAddressWorkingInformation     $work_address_working_info
     * @return void
     */
    protected function saveTheCurrentAttributesValueOfTheWorkAddressWorkingInformation($work_address_working_info)
    {
        if (is_int($work_address_working_info)) {
            $work_address_working_info = WorkAddressWorkingInformation::find($work_address_working_info);
        }

        if ($work_address_working_info) {
            $work_address_working_info->schedule_start_work_time = $work_address_working_info->schedule_start_work_time;
            $work_address_working_info->schedule_end_work_time = $work_address_working_info->schedule_end_work_time;
            $work_address_working_info->candidate_number = $work_address_working_info->candidate_number;
            $work_address_working_info->save();
        }
    }

    /**
     * Move time data from 'schedule_' attributes to designated 'planned_' attributes.
     *
     * @param EmployeeWorkingInformation    $employee_working_info
     * @return void
     */
    protected function moveTimeDataToPlannedAttributes($employee_working_info) {
        // First, move the time data to planned_ attributes
        $employee_working_info->planned_overtime_start = $employee_working_info->schedule_start_work_time ? $employee_working_info->schedule_start_work_time : $employee_working_info->planned_overtime_start;
        $employee_working_info->planned_overtime_end = $employee_working_info->schedule_end_work_time ? $employee_working_info->schedule_end_work_time : $employee_working_info->planned_overtime_end;
        $employee_working_info->planned_break_time = $employee_working_info->schedule_break_time ? $employee_working_info->schedule_break_time : $employee_working_info->planned_break_time;
        $employee_working_info->planned_night_break_time = $employee_working_info->schedule_night_break_time ? $employee_working_info->schedule_night_break_time : $employee_working_info->planned_night_break_time;

        // Then clear the schedule_ attributes
        $employee_working_info->schedule_start_work_time = config('caeru.empty_date');
        $employee_working_info->schedule_end_work_time = config('caeru.empty_date');
        $employee_working_info->schedule_break_time = config('caeru.empty');
        $employee_working_info->schedule_night_break_time = config('caeru.empty');
        $employee_working_info->schedule_working_hour = config('caeru.empty_time');
    }

    /**
     * Move time data from 'planned_' attributes to designated 'schedule_' attributes.
     *
     * @param EmployeeWorkingInformation    $employee_working_info
     * @return void
     */
    protected function moveTimeDataToScheduleAttributes($employee_working_info) {
        // First, move the time data to schedule_ attribute, remember to reset the schedule_working_hour
        $employee_working_info->schedule_start_work_time = $employee_working_info->planned_overtime_start ? $employee_working_info->planned_overtime_start : $employee_working_info->schedule_start_work_time;
        $employee_working_info->schedule_end_work_time = $employee_working_info->planned_overtime_end ? $employee_working_info->planned_overtime_end : $employee_working_info->schedule_end_work_time;
        $employee_working_info->schedule_break_time = $employee_working_info->planned_break_time ? $employee_working_info->planned_break_time : $employee_working_info->schedule_break_time;
        $employee_working_info->schedule_night_break_time = $employee_working_info->planned_night_break_time ? $employee_working_info->planned_night_break_time : $employee_working_info->schedule_night_break_time;
        $employee_working_info->schedule_working_hour = null;

        // Then clear the designated planned_ attributes
        $employee_working_info->planned_overtime_start = null;
        $employee_working_info->planned_overtime_end = null;
        // $employee_working_info->planned_break_time = null;
        // $employee_working_info->planned_night_break_time = null;
    }

}
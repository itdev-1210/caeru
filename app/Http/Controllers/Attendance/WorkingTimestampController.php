<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\WorkingTimestampRequest;
use App\WorkAddress;
use App\EmployeeWorkingDay;
use App\WorkingTimestamp;
use App\Events\WorkingTimestampChanged;
use App\Services\TimestampesAndWorkingDayConnectorService;
use App\Http\Controllers\Reusables\ManageWorkingTimestampsTrait;

class WorkingTimestampController extends Controller
{
    use ManageWorkingTimestampsTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:change_attendance_data');
    }    


    /**
     * Store a WorkingTimestamp
     *
     * @param \App\Http\Requests\WorkingTimestampRequest        $request
     * @param \App\EmployeeWorkingDay                           $working_day
     * @return \Illuminate\Http\Response
     */
    public function store(WorkingTimestampRequest $request, EmployeeWorkingDay $working_day)
    {
        $new_timestamp = new WorkingTimestamp($request->only([
            'enable',
            'processed_date_value',
            'processed_time_value',
            'timestamped_type',
            'work_location_id',
            'work_address_id',
        ]));

        // If the new working timestamp belong to a WorkAddress then assign that WorkAddress's position to this timestamp
        // if ($request->input('work_address_id') != null) {
        //     $work_address = WorkAddress::find($request->input('work_address_id'));

        //     if ($work_address) {
        //         $new_timestamp->latitude = $work_address->latitude;
        //         $new_timestamp->longitude = $work_address->longitude;
        //     }
        // }

        $new_timestamp->registerer_type = WorkingTimestamp::MANAGER;
        $new_timestamp->registerer_id = $request->user()->id;

        $this->addNewWorkingTimestampsForWorkingDay([$new_timestamp], $working_day);

        $working_day->load('workingTimestamps');
        return [
            'success' => '保存しました',
            'timestamps' => $working_day->workingTimestamps()->orderBy('raw_date_time_value')->get(),
        ];
    }

    /**
     * Toggle the enable of a WorkingTimestamp
     *
     * @param \Illuminate\Http\Request      $request
     * @param \App\WorkingTimestamp         $working_timestamp
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus(Request $request, WorkingTimestamp $working_timestamp)
    {
        $working_timestamp->enable = $request->input('enable');

        $working_timestamp->save();

        $working_day = $working_timestamp->employeeWorkingDay;

        $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);
        $connector_service->rearrangeAndConnectTheTimestampsToWorkingDays([$working_day]);

        // event(new WorkingTimestampChanged($working_timestamp->employeeWorkingDay));

        $working_day->load('workingTimestamps');

        return [
            'success' => '保存しました',
            'timestamps' => $working_day->workingTimestamps->sortBy('raw_date_time_value')->values(),
        ];
    }
}

<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\WorkLocation;
use App\AttendanceDataDownload\DataDownloadService;
use Carbon\Carbon;

class AttendanceDataDownloadController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Download Attendance Data
     *
     * @param Illuminate\Http\Request       $request
     * @return BinaryFileResponse
     */
    public function downloadAttendanceData(Request $request)
    {
        $conditions = [
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'presentation_id' => $request->input('presentation_id'),
            'name' => $request->input('name'),
            'work_status' => $request->input('work_status'),
        ];

        $work_location_ids = $this->getCurrentWorkLocationIds();
        $current_company_code = session('current_company_code');

        $download_service = resolve(DataDownloadService::class);

        $file_name = $download_service->downloadAttendanceData($conditions, $work_location_ids, $current_company_code);

        $to_be_downloaded_file_name = $current_company_code . '_' . Carbon::now()->format('Ymd_his') . '.csv';
        $headers = [
            'Content-Type' => 'text/csv; charset=UTF-8'
        ];

        return response()->download($file_name, $to_be_downloaded_file_name, $headers)->deleteFileAfterSend(true);
    }

    /**
     * Download Summary Data
     *
     * @param Illuminate\Http\Request       $request
     * @return BinaryFileResponse
     */
    public function downloadSummaryData(Request $request)
    {
        $conditions = [
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'presentation_id' => $request->input('presentation_id'),
            'name' => $request->input('name'),
            'work_status' => $request->input('work_status'),
        ];

        $current_company_code = session('current_company_code');
        $work_location_ids = $this->getCurrentWorkLocationIds();

        $download_service = resolve(DataDownloadService::class);

        $file_name = $download_service->downloadSummaryData($conditions, $work_location_ids, $current_company_code);

        $to_be_downloaded_file_name = $current_company_code . '_' . Carbon::now()->format('Ymd_his') . '.csv';
        $headers = [
            'Content-Type' => 'text/csv; charset=UTF-8'
        ];

        return response()->download($file_name, $to_be_downloaded_file_name, $headers)->deleteFileAfterSend(true);
    }

    /**
     * Get the id of the current work location(s) in the session.
     *
     * @return array
     */
    private function getCurrentWorkLocationIds()
    {
        $current_work_location = session('current_work_location');

        if ($current_work_location == "all") {
            $current_work_location = WorkLocation::all()->pluck('id')->toArray();
        } else if (is_numeric($current_work_location)) {
            $current_work_location = [$current_work_location];
        }
        return $current_work_location;
    }
}

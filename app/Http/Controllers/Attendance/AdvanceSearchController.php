<?php

namespace App\Http\Controllers\Attendance;

use Auth;
use Constants;
use App\Setting;
use App\WorkLocation;
use App\Employee;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use App\Http\Controllers\Reusables\UseScheduleTypeIconTrait;
use App\Http\Controllers\Reusables\CustomizedPaginationTrait;
use App\Http\Controllers\Reusables\SendDatePickerDataBaseOnCurrentWorkLocationTrait as SendDatePickerDataTrait;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use League\Csv\Reader;
use League\Csv\Writer;
use App\WorkStatus;
use App\RestStatus;
use App\Http\Requests\EmployeeWorkingInformationCsvRequest;
use App\Events\WorkingTimestampChanged;
use App\Events\WorkingTimestampChangedOnManyWorkingDays;
use App\Events\DistributeTimestampsAndReevaluateChecklistErrors;
use App\Events\CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld;
use App\Events\DeleteManyEmployeeWorkingInformations;

class AdvanceSearchController extends Controller
{
    use BusinessMonthTrait, SendDatePickerDataTrait, CustomizedPaginationTrait, UseScheduleTypeIconTrait;

    /**
     * The attributes that should be add to header in file csv.
     *
     * @var array
     */
    protected $key_value = [
        '勤務ID（新規は空白）' => 'id',
        '従業員ID' => 'employee_presentation_id',
        '従業員名' => 'employee_name',
        '勤務先ID' => 'planned_work_location_presentation_id',
        '勤務先名' => 'planned_work_location_name',
        '勤務年月日' => 'date',
        '所定出勤時刻' => 'schedule_start_work_time',
        '所定退勤時刻' => 'schedule_end_work_time',
        '所定休憩時間' => 'schedule_break_time',
        '所定深夜休憩時間' => 'schedule_night_break_time',
        '所定労働時間' => 'schedule_working_hour',
        '勤務形態' => 'planned_work_status_name',
        '休暇形態' => 'planned_rest_status_name',
        '時間休暇開始時刻' => 'paid_rest_time_start',
        '時間休暇終了時刻' => 'paid_rest_time_end',
        '時間休暇時間（時間）' => 'paid_rest_time_period',
        '早出開始時刻' => 'planned_early_arrive_start',
        '早出終了時刻' => 'planned_early_arrive_end',
        '残業開始時刻' => 'planned_overtime_start',
        '残業終了時刻' => 'planned_overtime_end',
        '遅刻時間（分）' => 'planned_late_time',
        '早退時間（分）' => 'planned_early_leave_time',
        '予定休憩（分）' => 'planned_break_time',
        '予定深夜休憩（分）' => 'planned_night_break_time',
        '予定外出時間（分）' => 'planned_go_out_time',
        '削除' => 'delete',
        'エラー' => 'error',
    ];

    protected $key_work_time_value = [
        '勤務地ID' => 'employee_name',
        '10001' => 'employee_presentation_id',
        '2019/01/01' => 'date',
        '2019/01/02' => 'date',
        '2019/01/03' => 'date',
        '2019/01/04' => 'date',
        '2019/01/05' => 'date',
        '2019/01/06' => 'date',
        'エラー' => 'error',
    ];

    /**
     * The attributes that should be update to employee working information
     *
     * @var array
     */
    protected $array_update_key = [
        'schedule_start_work_time',
        'schedule_end_work_time',
        'schedule_break_time',
        'schedule_night_break_time',
        'schedule_working_hour',

        'planned_work_location_id',
        'planned_work_status_id',
        'planned_rest_status_id',
        'paid_rest_time_start',
        'paid_rest_time_end',
        'paid_rest_time_period',
        'planned_early_arrive_start',
        'planned_early_arrive_end',
        'planned_late_time',
        'planned_break_time',
        'planned_night_break_time',
        'planned_go_out_time',
        'planned_early_leave_time',
        'planned_overtime_start',
        'planned_overtime_end',
        'work_time',
    ];

    /**
     * The sample record
     *
     * @var array
     */
    protected $sample_record = [
        'Sample','10001','サンプル太郎','100','東京本社','2018/01/01','9:00','18:00','60','30','8:00','欠勤','時有','10:00','12:00','4:00','7:00','9:00','19:00','23:00','60','60','60','60','120','1',''
    ];
    protected $sample_work_time_record = [
        'Sample','12345', '公休','早番','欠勤', '有給', '中番', '早番', ''
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose');
        $this->middleware('can:view_attendance_advance_search_page');
    }

    /**
     * Show Employee Working Informations's advance search page
     *
     * @param \Illuminate\Http\Request      $request
     * @param boolean                       $reset_search_condition
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $reset_search_condition = 0, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        $condition = [];

        if ($reset_search_condition == true) {
            session()->forget('attendance_advance_search_history');
            $conditions = $this->getDefaultSearchConditions();

            // Reset the breadcumbs if reset the search history
            $breadcrumbs_service->resetToAttendanceRoot();

        // If that option is false, we need to retrieve the search conditions history from the session and then proceed to search with it
        } else {

            $conditions = session('attendance_advance_search_history');

            if ($conditions) {
                $data = $this->advanceSearch($conditions, $page);

                JavaScript::put([
                    'search_history' => $data['data'],
                    'current_page' => $data['current_page'],
                    'total' => $data['total'],
                    'per_page' => $data['per_page'],
                ]);

            } else {
                $conditions = $this->getDefaultSearchConditions();
            }
        }

        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::ADVANCE_SEARCH, $request->route()->getName(), $request->route());

        // Prepare some presentation data for the search box
        $this->prepareDataForSearchBox();

        // Send data for the date pickers
        $this->sendDatePickerData();

        JavaScript::put([
            'conditions' => $conditions,
            'can_go_to_working_month_page' => $request->user()->can('view_attendance_employee_working_month_page'),
            'can_go_to_working_day_page' => $request->user()->can('view_attendance_employee_working_day'),
        ]);

        return view('attendance.employee.advance_search');
    }

    /**
     * Search with given conditions
     *
     * @param \Illuminate\Http\Request      $request
     * @return Array|JSON
     */
    public function search(Request $request)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => 0 ]);

        $conditions = $request->input('conditions');

        // Save the search conditions to session
        session(['attendance_advance_search_history' => $conditions]);

        $data = $this->advanceSearch($conditions);

        return [
            'working_days' => $data['data'],
            'current_page' => $data['current_page'],
            'total' => $data['total'],
            'per_page' => $data['per_page'],
        ];
    }

    /**
     * Change page of advance search's result. We need to change the parameter 'page' in the breadcrumb of this page. So that we can return to the
     * correct page later.
     *
     * @param \Illuminate\Http\Request      $request
     * @param integer                       $page
     * @return array|json
     */
    public function changePage(Request $request, $page = 1)
    {

        $conditions = session('attendance_advance_search_history');
        $data = $this->advanceSearch($conditions, $page);

        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'page' => $page ]);

        return [
            'working_days' => $data['data'],
            'current_page' => $data['current_page'],
            'total' => $data['total'],
            'per_page' => $data['per_page'],
        ];
    }

    /**
     * Download data with Format
     *
     * @param \Illuminate\Http\Request      $request
     * @return CSV
     */
    public function download(Request $request)
    {

        $records = [];
        $csvType = $request->csvType;

        if ($csvType == 1) array_push($records, $this->sample_work_time_record);
        else array_push($records, $this->sample_record);

        //load the CSV document from a string
        $csv = Writer::createFromString('');

        //insert the header
        if ($csvType == 1) $csv->insertOne(array_keys($this->key_work_time_value));
        else $csv->insertOne(array_keys($this->key_value));

        //insert all the records
        $csv->insertAll($records);

        $csv->output('schedules.csv');
        die;
    }

    /**
     * Download data with given conditions
     *
     * @param \Illuminate\Http\Request      $request
     * @return CSV
     */
    public function searchDownload(Request $request)
    {
        $conditions = session('attendance_advance_search_history');

        $records = [];
        $csvType = $request->csvType;

        if (isset($conditions)) {
            $data = $this->searchEmployeeWorkingInformationForDownload($conditions);

            foreach ($data['data'] as $day) {
                if (isset($day['working_infos'])) {
                    foreach ($day['working_infos'] as $working_info) {
                        $record = [];
                        foreach ($this->key_value as $key => $value) {
                            if (in_array($key, ['従業員ID', '従業員名', '勤務年月日'])) {
                                array_push($record, $day[$value]);
                            } else {
                                array_push($record, $working_info[$value] ?? null);
                            }
                        }
                        array_push($records, $record);
                    }
                } else {
                    $record = [];
                    foreach ($this->key_value as $key => $value) {
                        if (in_array($key, ['従業員ID', '従業員名', '勤務年月日'])) {
                            array_push($record, $day[$value]);
                        } else {
                            array_push($record, null);
                        }
                    }
                    array_push($records, $record);
                }
            }
        }

        //load the CSV document from a string
        $csv = Writer::createFromString('');

        //insert the header
        $csv->insertOne(array_keys($this->key_value));

        //insert all the records
        $csv->insertAll($records);

        $csv->output('schedules.csv');
        die;
    }

    /**
     * Download data with given conditions
     *
     * @param App\Http\Requests\EmployeeWorkingInformationCsvRequest      $request
     * @return Array|JSON
     */
    public function upload(EmployeeWorkingInformationCsvRequest $request)
    {
        $employee_working_day_changes = [];
        $type = $request->type;
        if ($type == 1) {
            $to_be_deleted_employee_working_information_ids = [];
            $employee_id_list_of_deleted_employee_working_informations = [];
            foreach ($request->true_data as $data) {
                foreach ($this->key_value as $key => $value) {
                    $data[$value] = $data[$key];
                    unset($data[$key]);
                }
                array_push($employee_working_day_changes, $data['employee_working_day_id']);

                if ($data['delete']) {
                    $to_be_deleted_ewi = $request->employee_working_informations->where('id', $data['id'])->first();
                    if ($to_be_deleted_ewi) {
                        $to_be_deleted_employee_working_information_ids[] = $to_be_deleted_ewi->id;
                        $employee_id_list_of_deleted_employee_working_informations[] = $to_be_deleted_ewi->employeeWorkingDay->employee_id;
                    }
                }

                $this->createOrUpdateEmployeeWorkingInformation($data, $request->user()->id, $request->employee_working_informations->where('id', $data['id'])->first());
            }

            if (count($request->employee_working_day_with_rest_status_changed) != 0) {
                $employee_ids = EmployeeWorkingDay::whereIn('id', array_unique($request->employee_working_day_with_rest_status_changed))
                                                    ->pluck('employee_id')->toArray();

                event(new CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld(array_unique($employee_ids)));
            }

            // Delete
            if (count($to_be_deleted_employee_working_information_ids) != 0) {
                event(new DeleteManyEmployeeWorkingInformations($to_be_deleted_employee_working_information_ids, $employee_id_list_of_deleted_employee_working_informations));
            }

            // UPDATE 2019/03/07: Won't distribute WorkingTimestamp after create new EWI anymore. To make this function faster.
            // event(new WorkingTimestampChangedOnManyWorkingDays(array_unique($employee_working_day_changes)));

            event(new DistributeTimestampsAndReevaluateChecklistErrors(array_unique($employee_working_day_changes)));


        // Update 2019/03/11: Add the event to create Checklist error mechanism's Jobs and event to check PaidHolidayInformation to Mr.Panda's shift import data flow.
        } else {
            $employee_working_day_with_rest_status_changed = [];

            foreach($request->true_data as $data) {

                array_push($employee_working_day_changes, $data['employee_working_day_id']);

                if ($this->checkIfTheNewEmployeeWorkingInformationHasPlannedRestStatusOrNot($data)) {
                    array_push($employee_working_day_with_rest_status_changed, $data['employee_working_day_id']);
                }
                $this->createEmployeeWorkingInformation($data, $request->user()->id);
            }

            if (count($employee_working_day_with_rest_status_changed) != 0) {
                $employee_ids = EmployeeWorkingDay::whereIn('id', array_unique($employee_working_day_with_rest_status_changed))
                                                    ->pluck('employee_id')->toArray();

                event(new CachedPaidHolidayInformationOfSeveralEmployeesBecomeOld(array_unique($employee_ids)));
            }

            // UPDATE 2019/03/07: Won't distribute WorkingTimestamp after create new EWI anymore. To make this function faster.
            // event(new WorkingTimestampChangedOnManyWorkingDays(array_unique($employee_working_day_changes)));

            event(new DistributeTimestampsAndReevaluateChecklistErrors(array_unique($employee_working_day_changes)));
        }

        return [ 'success' => '保存しました'];
    }

    /**
     * Check if the new EmployeeWorkingInformation has set PlannedRestStatusId or not
     *
     * @param       array       $data
     * @return      boolean
     */
    protected function checkIfTheNewEmployeeWorkingInformationHasPlannedRestStatusOrNot($data)
    {
        return isset($data['planned_rest_status_id']);
    }

    /**
     * The function will create or update employee working information.
     * 2019-04-11: move the delete part out of this function, so that it will better fit with the name.
     *
     * @param Array    $data
     * @return void
     */
    protected function createOrUpdateEmployeeWorkingInformation($data, $user_id, $employee_working_information = null)
    {
        if (!isset($employee_working_information))
            $this->createEmployeeWorkingInformation($data, $user_id);
        else
            $this->updateEmployeeWorkingInformation($data, $employee_working_information, $user_id);
    }

    /**
     * The function will create employee working information
     *
     * @param Array    $data
     * @param App\Manager    $user_id
     * @return void
     */
    protected function createEmployeeWorkingInformation($data, $user_id)
    {
        $info = new EmployeeWorkingInformation(array_only($data, $this->array_update_key));

        // Set the last modified person name
        $info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
        $info->last_modify_person_id = $user_id;

        // All EmployeeWorkingInformation instance created by this way are considered 'manually_modified' AND 'schedule_modified'
        $info->manually_modified = true;
        $info->schedule_modified = true;

        // Set the relationship with the correct EmployeeWorkingDay
        $info->employeeWorkingDay()->associate($data['employee_working_day_id']);

        $info->save();
    }

    /**
     * The function will update employee working information
     *
     * @param Array    $data
     * @param App\EmployeeWorkingInformation    $employee_working_information
     * @return void
     */
    protected function updateEmployeeWorkingInformation($data, $employee_working_information, $user_id)
    {
        foreach ($this->array_update_key as $key => $value) {
            if ($value !== 'work_time') {
                if ($data[$value] != $employee_working_information->$value) {
                    if (in_array($value, ['schedule_start_work_time', 'schedule_end_work_time', 'schedule_break_time', 'schedule_night_break_time',
                        'schedule_working_hour', 'planned_work_location_id',])) {
                        if ($value !== 'planned_work_location_id') $employee_working_information['manually_inputed_' . $value] = 1;
                        if (!$employee_working_information->schedule_modified) $employee_working_information->schedule_modified = 1;

                        if (!isset($data[$value])) {
                            if (in_array($value, ['schedule_start_work_time', 'schedule_end_work_time'])) $data[$value] = config('caeru.empty_date');
                            else if (in_array($value, ['schedule_break_time', 'schedule_night_break_time'])) $data[$value] = config('caeru.empty');
                            else if (in_array($value, ['schedule_working_hour'])) $data[$value] = config('caeru.empty_time');
                        }
                    }
                    if (in_array($value, ['planned_break_time']) && !isset($data[$value])) $data[$value] = 0;
                    if (in_array($value, ['planned_night_break_time']) && !isset($data[$value])) $data[$value] = config('caeru.empty');
                    $employee_working_information->$value = $data[$value];
                }
            }
        }
        // Set the last modified person name
        $employee_working_information->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
        $employee_working_information->last_modify_person_id = $user_id;

        $employee_working_information->manually_modified = true;
        $employee_working_information->temporary = false;

        $employee_working_information->save();
    }

    /**
     * Make the query and apply search conditions
     *
     * @param Array         $conditions
     * @return Array
     */
    protected function advanceSearch($conditions, $page = 1)
    {
        // First, apply the date conditions
        $working_day_query = EmployeeWorkingDay::where('date', '>=', $conditions['start_date'])->where('date', '<=', $conditions['end_date']);

        $employee_number = Employee::whereIn('work_location_id', $this->getCurrentWorkLocationIds())->count();

        /**
         * NOTE: It's actually very interesting here.
         * How is that: it become slower when using eager loading ?!
         *
         * I don't know exactly why, but when the number of employees > 100, using eager loading is actually slower than not using it.
         * So, we'll add the eager loading only when the number of employees less than or equal to 100.
         */
        if ($employee_number <= 100) {
            $working_day_query->with([
                'employee',
                'concludedEmployeeWorkingDay',
                'substituteEmployeeWorkingInformations',
                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                'employeeWorkingInformations.colorStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.setting',
                'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.company.setting',
                'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
                'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
            ]);
        }

        // This '勤務予定のない従業員を表示' condition is a pain in the a**. Have to make a freaking new flow just to search with this condition.
        if ($conditions['employee_with_no_schedule'] == false) {

            // Apply all the employee-related conditions
            $working_day_query = $this->applyEmployeeConditionsToEmployeeWorkingDay($working_day_query, $conditions);

            // Apply the 締め未対応 conditions
            if ($conditions['unconcluded'] == true) {
                $working_day_query->where('concluded_level_one', '><', true)->where('concluded_level_two', '><', true);
            }

            // Get the data
            $paginated_working_days = $working_day_query->orderBy('date')->orderBy('employee_id')->get();

            // Then get the id list of EmployeeWorkingDay that have at least an EmployeeWorkingInformation that is not temporary
            $having_working_info_working_day_ids = EmployeeWorkingInformation::where('temporary', false)->pluck('employee_working_day_id')->unique();

            // And then, we apply the opposite of '勤務予定のない従業員を表示' condition, (because we're in the 'false' clause of the if )
            $working_days = $paginated_working_days->whereIn('id', $having_working_info_working_day_ids);

            // This is the part where the EmployeeWorkingInformation's conditions are applied
            // To be more specific: planned_work_status_id, planned_rest_status_id, planned_work_location_id and potentially planned_work_address_id as well
            if (isset($conditions['planned_work_status_id'])) {
                $working_days = $working_days->filter(function($day) use ($conditions) {
                    return $day->employeeWorkingInformations->contains(function($working_info) use ($conditions) {
                        return $working_info->planned_work_status_id == $conditions['planned_work_status_id'];
                    });
                });
            }
            if (isset($conditions['planned_rest_status_id'])) {
                $working_days = $working_days->filter(function($day) use ($conditions) {
                    return $day->employeeWorkingInformations->contains(function($working_info) use ($conditions) {
                        return $working_info->planned_rest_status_id == $conditions['planned_rest_status_id'];
                    });
                });
            }
            if (isset($conditions['planned_work_location_id'])) {
                $working_days = $working_days->filter(function($day) use ($conditions) {
                    return $day->employeeWorkingInformations->contains(function($working_info) use ($conditions) {
                        return $working_info->planned_work_location_id == $conditions['planned_work_location_id'];
                    });
                });
            }

            $pagination = $this->paginateData($working_days, $page);

            // Transform Data into the correct format to be displayed in the front end
            $data = isset($pagination['data']) ? $pagination['data']->map(function($working_day) {

                $working_infos_data = $working_day->getCompactedWorkingInformations()->toArray();
                return [
                    'date' => $working_day->date,
                    'employee_presentation_id' => $working_day->employee->presentation_id,
                    'employee_name' => $working_day->employee->fullName(),
                    'department' => $working_day->employee->department_id,
                    'employee_id' => $working_day->employee->id,
                    'schedule_type_icon' => $this->getScheduleTypeIcon($working_day->employee->schedule_type),
                    'working_infos' => empty($working_infos_data) ? null : $working_infos_data,
                    'max_line' => empty($working_infos_data) ? 1 : count($working_infos_data),
                ];
            }) : null;

            return [
                'data' => isset($data) ? $data->values() : null,
                'current_page' => $pagination['current_page'],
                'total' => $working_days->count(),
                'per_page' => $this->ITEMS_PER_PAGE,
            ];

        // if the condition '勤務予定のない従業員を表示' is true, we have to:
        //      1. Find the coresponding EmployeeWorkingDay (ez part), put them in an associate array with distinct key made from date and employee id
        //      2. For all day within start-end of the conditions, and for each sastifed-the-conditions-employee, check if he/she doesnt has PlannedSchedule
        //          or have a record in the above mentioned array.
        } else {

            // Part 1

            // Apply the 締め未対応 conditions
            if ($conditions['unconcluded'] == true) {
                $working_day_query = $working_day_query->where('concluded_level_one', '><', true)->where('concluded_level_two', '><', true);
            }

            // Get the EmployeeWorkingDay sastifying the all the other conditions except the condition '勤務予定のない従業員を表示'
            $all_working_days = $this->applyEmployeeConditionsToEmployeeWorkingDay($working_day_query, $conditions)->get();

            // Then get the id list of EmployeeWorkingDay that have at least an EmployeeWorkingInformation that is not temporary
            $having_working_info_working_day_ids = EmployeeWorkingInformation::where('temporary', false)->pluck('employee_working_day_id')->unique();

            // And then, filter those out using the function whereNotIn() of the Collection. Doing this instead of put it in the query imrpove the performance of the querry very much.
            $no_work_working_days = $all_working_days->whereNotIn('id', $having_working_info_working_day_ids);

            // Turn the result into a hash table with the key made of date#employee_id (guarantee to be unique)
            $easy_access_no_work_working_days = $no_work_working_days->keyBy(function($working_day) {
                return $working_day->date . '#' . $working_day->employee_id;
            });

            // Part 2
            $employees = $this->getEmployeesWhoSastifyTheConditions($conditions)->orderBy('id')->get();

            $pivot_day = Carbon::createFromFormat('Y-m-d', $conditions['start_date']);
            $end_date = Carbon::createFromFormat('Y-m-d', $conditions['end_date'])->addDay()->hour(0)->minute(0)->second(0);

            $data = collect([]);
            while($pivot_day->lt($end_date)) {

                foreach($employees as $employee) {
                    $unique_key = $pivot_day->format('Y-m-d') . '#' . $employee->id;

                    if (isset($easy_access_no_work_working_days[$unique_key]) || $employee->schedules->isEmpty()) {
                        $data[] = [
                            'date' => $pivot_day->format('Y-m-d'),
                            'employee_presentation_id' => $employee->presentation_id,
                            'employee_name' => $employee->fullName(),
                            'department' => $employee->department_id,
                            'employee_id' => $employee->id,
                            'schedule_type_icon' => $this->getScheduleTypeIcon($employee->schedule_type),
                            'working_infos' => null,
                            'max_line' => 1,
                        ];
                    }
                }

                $pivot_day->addDay();
            }

            $pagination = $this->paginateData($data, $page);

            return [
                'data' => $pagination['data']->values(),
                'current_page' => $pagination['current_page'],
                'total' => count($data),
                'per_page' => $this->ITEMS_PER_PAGE,
            ];

        }

    }

    /**
     * Make the query and apply search conditions for function download
     *
     * @param Array         $conditions
     * @return Array
     */
    protected function searchEmployeeWorkingInformationForDownload($conditions)
    {
        // First, apply the date conditions
        $working_day_query = EmployeeWorkingDay::where('date', '>=', $conditions['start_date'])->where('date', '<=', $conditions['end_date']);

        $employee_number = Employee::whereIn('work_location_id', $this->getCurrentWorkLocationIds())->count();

        $working_day_query->with([
            'employee',
            'concludedEmployeeWorkingDay',
            'substituteEmployeeWorkingInformations',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            'employeeWorkingInformations.colorStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
            'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.company.workStatuses',
            'employeeWorkingInformations.currentPlannedWorkLocation.company.restStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.setting',
            'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.setting',
            'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
            'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
        ]);

        // This '勤務予定のない従業員を表示' condition is a pain in the a**. Have to make a freaking new flow just to search with this condition.
        if ($conditions['employee_with_no_schedule'] == false) {

            // Apply all the employee-related conditions
            $working_day_query = $this->applyEmployeeConditionsToEmployeeWorkingDay($working_day_query, $conditions);

            // Apply the 締め未対応 conditions
            if ($conditions['unconcluded'] == true) {
                $working_day_query->where('concluded_level_one', '><', true)->where('concluded_level_two', '><', true);
            }

            // Get the data
            $paginated_working_days = $working_day_query->orderBy('employee_id')->orderBy('date')->get();

            // Then get the id list of EmployeeWorkingDay that have at least an EmployeeWorkingInformation that is not temporary
            $having_working_info_working_day_ids = EmployeeWorkingInformation::where('temporary', false)->pluck('employee_working_day_id')->unique();

            // And then, we apply the opposite of '勤務予定のない従業員を表示' condition, (because we're in the 'false' clause of the if )
            $working_days = $paginated_working_days->whereIn('id', $having_working_info_working_day_ids);

            // This is the part where the EmployeeWorkingInformation's conditions are applied
            // To be more specific: planned_work_status_id, planned_rest_status_id, planned_work_location_id and potentially planned_work_address_id as well
            if (isset($conditions['planned_work_status_id'])) {
                $working_days = $working_days->filter(function($day) use ($conditions) {
                    return $day->employeeWorkingInformations->contains(function($working_info) use ($conditions) {
                        return $working_info->planned_work_status_id == $conditions['planned_work_status_id'];
                    });
                });
            }
            if (isset($conditions['planned_rest_status_id'])) {
                $working_days = $working_days->filter(function($day) use ($conditions) {
                    return $day->employeeWorkingInformations->contains(function($working_info) use ($conditions) {
                        return $working_info->planned_rest_status_id == $conditions['planned_rest_status_id'];
                    });
                });
            }
            if (isset($conditions['planned_work_location_id'])) {
                $working_days = $working_days->filter(function($day) use ($conditions) {
                    return $day->employeeWorkingInformations->contains(function($working_info) use ($conditions) {
                        return $working_info->planned_work_location_id == $conditions['planned_work_location_id'];
                    });
                });
            }

            // Transform Data into the correct format to be displayed in the front end
            $data = $working_days->map(function($working_day) {
                $working_infos = $working_day->employeeWorkingInformations->sortBy('id');
                $working_infos_data = $working_infos->map(function($working_info) {
                    $data = [];
                    $work_location = isset($working_info->currentPlannedWorkLocation) ? $working_info->currentPlannedWorkLocation : $working_info->plannedSchedule->workLocation;
                    $planned_work_status_id = $work_location->company->workStatuses->where('id', $working_info->planned_work_status_id)->first();
                    $planned_rest_status_id = $work_location->company->restStatuses->where('id', $working_info->planned_rest_status_id)->first();

                    $data['id'] = $working_info->id;
                    $data['planned_work_location_presentation_id'] = $work_location->presentation_id;
                    $data['planned_work_location_name'] = $work_location->name;
                    $data['schedule_start_work_time'] = $this->convertStringToMintues($working_info->schedule_start_work_time);
                    $data['schedule_end_work_time'] = $this->convertStringToMintues($working_info->schedule_end_work_time);
                    $data['schedule_break_time'] = $working_info->schedule_break_time;
                    $data['schedule_night_break_time'] = $working_info->schedule_night_break_time;
                    $data['schedule_working_hour'] = $this->convertStringToMintues($working_info->schedule_working_hour, 'H:i:s');
                    $data['planned_work_status_name'] = (isset($planned_work_status_id)) ? $planned_work_status_id->name : null;
                    $data['planned_rest_status_name'] = (isset($planned_rest_status_id)) ? $planned_rest_status_id->name : null;
                    $data['paid_rest_time_start'] = $this->convertStringToMintues($working_info->paid_rest_time_start);
                    $data['paid_rest_time_end'] = $this->convertStringToMintues($working_info->paid_rest_time_end);
                    $data['paid_rest_time_period'] = $this->convertStringToMintues($working_info->paid_rest_time_period, 'H:i:s');
                    $data['planned_early_arrive_start'] = $this->convertStringToMintues($working_info->planned_early_arrive_start);
                    $data['planned_early_arrive_end'] = $this->convertStringToMintues($working_info->planned_early_arrive_end);
                    $data['planned_overtime_start'] = $this->convertStringToMintues($working_info->planned_overtime_start);
                    $data['planned_overtime_end'] = $this->convertStringToMintues($working_info->planned_overtime_end);
                    $data['planned_late_time'] = $working_info->planned_late_time;
                    $data['planned_early_leave_time'] = $working_info->planned_early_leave_time;
                    $data['planned_break_time'] = $working_info->planned_break_time;
                    $data['planned_night_break_time'] = $working_info->planned_night_break_time;
                    $data['planned_go_out_time'] = $working_info->planned_go_out_time;

                    return $data;
                });
                return [
                    'date' => $working_day->date,
                    'employee_presentation_id' => $working_day->employee->presentation_id,
                    'employee_name' => $working_day->employee->fullName(),
                    'working_infos' => empty($working_infos_data) ? null : $working_infos_data,
                ];
            });

            return [
                'data' => $data->values(),
            ];

        // if the condition '勤務予定のない従業員を表示' is true, we have to:
        //      1. Find the coresponding EmployeeWorkingDay (ez part), put them in an associate array with distinct key made from date and employee id
        //      2. For all day within start-end of the conditions, and for each sastifed-the-conditions-employee, check if he/she doesnt has PlannedSchedule
        //          or have a record in the above mentioned array.
        } else {

            // Part 1

            // Apply the 締め未対応 conditions
            if ($conditions['unconcluded'] == true) {
                $working_day_query = $working_day_query->where('concluded_level_one', '><', true)->where('concluded_level_two', '><', true);
            }

            // Get the EmployeeWorkingDay sastifying the all the other conditions except the condition '勤務予定のない従業員を表示'
            $all_working_days = $this->applyEmployeeConditionsToEmployeeWorkingDay($working_day_query, $conditions)->get();

            // Then get the id list of EmployeeWorkingDay that have at least an EmployeeWorkingInformation that is not temporary
            $having_working_info_working_day_ids = EmployeeWorkingInformation::where('temporary', false)->pluck('employee_working_day_id')->unique();

            // And then, filter those out using the function whereNotIn() of the Collection. Doing this instead of put it in the query imrpove the performance of the querry very much.
            $no_work_working_days = $all_working_days->whereNotIn('id', $having_working_info_working_day_ids);

            // Turn the result into a hash table with the key made of date#employee_id (guarantee to be unique)
            $easy_access_no_work_working_days = $no_work_working_days->keyBy(function($working_day) {
                return $working_day->date . '#' . $working_day->employee_id;
            });

            // Part 2
            $employees = $this->getEmployeesWhoSastifyTheConditions($conditions)->orderBy('id')->get();

            $pivot_day = Carbon::createFromFormat('Y-m-d', $conditions['start_date']);
            $end_date = Carbon::createFromFormat('Y-m-d', $conditions['end_date'])->addDay()->hour(0)->minute(0)->second(0);

            $data = collect([]);

            foreach($employees as $employee) {

                $start_date = $pivot_day->copy();
                while($start_date->lt($end_date)) {
                    $unique_key = $start_date->format('Y-m-d') . '#' . $employee->id;

                    if (isset($easy_access_no_work_working_days[$unique_key]) || $employee->schedules->isEmpty()) {
                        $data[] = [
                            'date' => $start_date->format('Y-m-d'),
                            'employee_presentation_id' => $employee->presentation_id,
                            'employee_name' => $employee->fullName(),
                            'working_infos' => null,
                        ];
                    }
                    $start_date->addDay();
                }
            }

            return [
                'data' => $data->values(),
            ];

        }

    }

    /**
     * The function will transfer string to minutes
     *
     * @param string    $data
     * @param string       $format
     * @return string     minute(hh:mm).
     */
    protected function convertStringToMintues($data, $format = 'Y-m-d H:i:s')
    {
        if (isset($data)) return Carbon::createFromFormat($format, $data)->format('H:i');
        return null;
    }

    /**
     * A small customized pagination function. It just simply chunks the result colection up.
     *
     * @param Collection    $data
     * @param integer       $page
     * @return array        an array include a collection(the result) and the current page.
     */
    protected function paginateData($data, $page)
    {
        if ($data->isNotEmpty()) {
            $pages = $data->chunk($this->ITEMS_PER_PAGE);

            $page = intval($page);

            $current_page = isset($pages[$page-1]) ? $page - 1 : 1;
            $result_page =  $pages[$current_page];

            return [
                'data' => $result_page,
                'current_page' => $current_page + 1,
            ];

        }

    }

    /**
     * Apply the employee-related conditions to a query of EmployeeWorkingDay
     *
     * @param Builder   $query
     * @param Array     $conditions
     * @return Builder
     */
    protected function applyEmployeeConditionsToEmployeeWorkingDay($query, $conditions)
    {
        // The default condition
        $query->whereHas('employee', function($query) use ($conditions) {
            $query->whereIn('work_location_id', $this->getCurrentWorkLocationIds());
        });

        if (isset($conditions['employee_presentation_id'])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                return $query->where('presentation_id', $conditions['employee_presentation_id']);
            });
        }

        if (isset($conditions['employee_name'])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                return $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
            });
        }

        if (isset($conditions['employee_work_status'])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                return $query->where('work_status', $conditions['employee_work_status']);
            });
        }

        if (!empty($conditions['departments'])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                return $query->whereIn('department_id', $conditions['departments']);
            });
        }

        return $query;
    }

    /**
     * Get employees who sastify the search conditions(with eager loaded PlannedSchedule).
     *
     * @param Builder       $query
     * @param Array         $conditions
     * @return Builder
     */
    protected function getEmployeesWhoSastifyTheConditions($conditions)
    {
        $query = Employee::with('schedules')->whereIn('work_location_id', $this->getCurrentWorkLocationIds());

        if (isset($conditions['employee_presentation_id'])) {
            $query->where('presentation_id', $conditions['employee_presentation_id']);
        }

        if (isset($conditions['employee_name'])) {
            $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
        }

        if (isset($conditions['employee_work_status'])) {
            $query->where('work_status', $conditions['employee_work_status']);
        }

        if (!empty($conditions['departments'])) {
            $query->whereIn('department_id', $conditions['departments']);
        }

        return $query;
    }

    /**
     * Get the default search conditions for the search box.
     *
     * @return array
     */
    protected function getDefaultSearchConditions()
    {
        return [
            'start_date' => null,
            'end_date' => null,
            'employee_presentation_id' => null,
            'employee_name' => null,
            'employee_work_status'  => config('constants.working'),
            'planned_work_status_id' => null,
            'planned_rest_status_id' => null,
            'departments' => [],
            'planned_work_location_id' => null,
            // 'work_address_name' => null,
            'employee_with_no_schedule' => false,
            'unconcluded' => false,
        ];
    }

    /**
     * Prepare presentation data for the search box.
     *
     * @return void
     */
    protected function prepareDataForSearchBox()
    {
        $current_work_location = session('current_work_location');

        // Get the list of department and employee's name, base on the current work location
        $company = Auth::user()->company;

        $all_departments = $company->departments->pluck('name', 'id')->toArray();
        $all_work_statuses = $company->workStatuses->pluck('name', 'id')->toArray();
        $all_rest_statuses = $company->restStatuses->pluck('name', 'id')->toArray();

        if ($current_work_location === 'all') {

            $employee_names = $company->employees->map(function($item) {
                return ['name' => $item->last_name . $item->first_name];
            })->toArray();

            $setting = $company->setting;

        } elseif (is_array($current_work_location)) {

            $employee_names = Employee::whereIn('work_location_id', $current_work_location)->get()->map(function($item) {
                return ['name' => $item->last_name . $item->first_name];
            })->toArray();

            $setting = $company->setting;

        } else {

            $work_location = WorkLocation::with('employees')->find($current_work_location);

            $departments = $work_location->activatingDepartments()->pluck('name', 'id')->toArray();
            $work_statuses = $work_location->activatingWorkStatuses()->pluck('name', 'id')->toArray();
            $rest_statuses = $work_location->activatingRestStatuses()->pluck('name', 'id')->toArray();

            $employee_names = $work_location->employees->map(function($item) {
                return ['name' => $item->last_name . $item->first_name];
            })->toArray();

            $setting = $work_location->currentSetting();

        }

        $today = Carbon::today();
        $pairOfDaysOfTheWeek = $this->getStartDateAndEndDateOfWeekBaseOnASetting($setting);
        $business_month = $this->calculateBusinessMonthBaseOnASetting($setting);
        $pairOfDaysOfTheMonth = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($setting, $business_month);

        $employee_work_statuses = Constants::workStatuses();
        $work_locations = Auth::user()->company->workLocations->pluck('name', 'id')->toArray();

        // Send the list of departments and employee names to the javascript side
        Javascript::put([
            'all_department'            => $all_departments,
            'all_work_statuses'         => $all_work_statuses,
            'all_rest_statuses'         => $all_rest_statuses,
            'search_box_departments'    => is_numeric($current_work_location) ? $departments : $all_departments,
            'search_box_work_statuses'  => $all_work_statuses, //is_numeric($current_work_location) ? $work_statuses : $all_work_statuses,
            'search_box_rest_statuses'  => $all_rest_statuses, //is_numeric($current_work_location) ? $rest_statuses : $all_rest_statuses,
            'employee_names'            => $employee_names,
            'employee_work_statuses'    => $employee_work_statuses,
            'work_locations'            => $work_locations,
            'today'                     => $today->format('Y-m-d'),
            'start_date_of_week'        => $pairOfDaysOfTheWeek[0]->format('Y-m-d'),
            'end_date_of_week'          => $pairOfDaysOfTheWeek[1]->format('Y-m-d'),
            'start_date_of_month'       => $pairOfDaysOfTheMonth[0]->format('Y-m-d'),
            'end_date_of_month'         => $pairOfDaysOfTheMonth[1]->format('Y-m-d'),
            'current_business_month'    => $business_month->format('Y-m'),
            'business_month_separate_day' => $setting->salary_accounting_day,
        ]);
    }

    /**
     * Get the start/end date of the current week base on the given setting. This setting can be belong to a WorkLocation or a Company.
     *
     * @param \App\Setting
     * @return array
     */
    protected function getStartDateAndEndDateOfWeekBaseOnASetting(Setting $setting)
    {
        $pivot = Carbon::today();
        $setting_start_date = $setting->start_day_of_week;

        $gap = $pivot->dayOfWeek >= $setting_start_date ? $pivot->dayOfWeek - $setting_start_date : 7 - ($setting_start_date - $pivot->dayOfWeek);
        $start_date = $pivot->copy()->subDays($gap);
        $end_date = $start_date->copy()->addDays(6);

        return [$start_date, $end_date];
    }

    /**
     * Get the current work locations's ids.
     * Reminder: the current_work_location variable in the session can be:
     *      - a string:     'all' - meaning the current chosen work location is the whole 'company'
     *      - an array:     contain some work location ids
     *      - an integer:   the id of a work location
     *
     * @return array
     */
    protected function getCurrentWorkLocationIds()
    {
        $current_work_location_ids = [];

        if (session('current_work_location') === 'all') {
            $current_work_location_ids = \Auth::user()->company->workLocations->pluck('id')->toArray();
        } else if (is_array(session('current_work_location'))) {
            $current_work_location_ids = session('current_work_location');
        } else {
            $current_work_location_ids = [session('current_work_location')];
        }

        return $current_work_location_ids;
    }

}

<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use App\Http\Requests\WorkingInformationTransferRequest;
use Illuminate\Routing\Controller;
use App\Company;
use App\Employee;
use App\WorkAddress;
use App\EmployeeWorkingDay;
use App\WorkingTimestamp;
use App\WorkTime;
use App\ChecklistItem;
use App\Events\EmployeeWorkingDayConcluded;
use App\Events\EmployeeWorkingDayUnconcluded;
use App\Http\Controllers\Reusables\UseTheEmployeeWorkingInfoVueComponentTrait;
use App\Http\Controllers\Reusables\TransferEmployeeWorkingDayTrait;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;
use App\Http\Controllers\Reusables\CanCheckIfWorkingDayCanBeConcluded;
use App\Http\Controllers\Reusables\UseTheSnapshotVueComponentTrait;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Events\PaidHolidayChanged;
use App\Services\WorkLocationSettingService;
use Carbon\Carbon;
use NationalHolidays;
use Auth;
use App\Setting;

class EmployeeWorkingDayController extends Controller
{
    use UseTheEmployeeWorkingInfoVueComponentTrait, TransferEmployeeWorkingDayTrait,
        CanCreateWorkingDayOnTheFlyTrait, CanCheckIfWorkingDayCanBeConcluded,
        UseTheSnapshotVueComponentTrait, BusinessMonthTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:view_attendance_employee_working_day');
        $this->middleware('can:change_attendance_data')->only(['scheduleTransfer']);
        $this->middleware('can:conclude_level_one')->only(['concludeLevelOne', 'unconcludedLevelOne', 'concludeNonExistWorkingDay']);
    }

    /**
     * Show an EmployeeWorkingDay page
     *
     * @param \Illuminate\Http\Request  $request        the request instance
     * @param int                       $employee_id    the id of the employee
     * @param string                    $date           the working day
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, $employee_id, $date)
    {
        // Validate the date by format 'yyyy-mm-dd'
        if ($this->validateDateByFormat($date)) {

            // Get the breadcrumb service.
            $breadcrumbs_service = resolve(BreadCrumbService::class);
            $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::WORKING_DAY, $request->route()->getName(), $request->route());

            // UPDATE 2018-01-18: now we have to change parent node(namely employee working month page)'s parameters
            // so that we can return back to the suitable business month
            if ($breadcrumbs_service->isParentOfCurrentNode(BreadCrumb::WORKING_MONTH)) {
                $employee = Employee::find($employee_id);
                $suitable_business_month = $this->getBusinessMonthThatContainASpecificDay($employee->workLocation->currentSetting(), $date);
                $parameter_to_overwrite = [
                    'business_month'=> $suitable_business_month->format('Y-m'),
                ];
                $breadcrumbs_service->changeParametersOfTheParentOfTheCurrentNode($parameter_to_overwrite);
            }

            $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee_id)->where('date', '=', $date)->first();

            // Make sure there is always a working_day, if that employee_id is valid.
            if (!$employee_working_day) {
                $employee_working_day = $this->createEmployeeWorkingDayOnTheFly($employee_id, $date);
            }

            Javascript::put([
                'working_day_id' => $employee_working_day->id,
                'current_date' => $employee_working_day->date,
                'can_make_yesterday_timestamp' => $this->checkIfCanMakeYesterdayTimestamp($request),
                'concluded_level_one' => $employee_working_day->concluded_level_one,
                'concluded_level_two' => $employee_working_day->concluded_level_two,
                'current_employee' => [
                    'id' => $employee_working_day->employee->id,
                    'name' => $employee_working_day->employee->fullName(),
                    'presentation_id' => $employee_working_day->employee->presentation_id,
                    'schedule_type' => $employee_working_day->employee->scheduleType(),
                ],
                'default_work_location_id' => is_numeric(session('current_work_location')) ? session('current_work_location') : $employee_working_day->employee->work_location_id,
                'working_infos' => $this->extractNecessaryData($employee_working_day->employeeWorkingInformations->sortBy('planned_start_work_time')->values()),
                'working_timestamps' => $employee_working_day->workingTimestamps->sortBy('raw_date_time_value')->values(),
                'timezone' => (Carbon::now()->tz->getOffset(Carbon::now('utc'))/60),
                'can_change_data' => $request->user()->can('change_attendance_data') && !$employee_working_day->isConcluded() && !$employee_working_day->isHavingConsideringRequest(),
                'concluded' => $employee_working_day->isConcluded(),
                'having_considering_request' => $employee_working_day->isHavingConsideringRequest(),
                // 'only_have_temporary_working_infos' => $this->haveOnlyTemporaryEWI($employee_working_day), No need to do this anymore, since this logic has been move to the javascript side
            ]);

            // Send presentation data for employee working day components
            $this->sendPresentationalDataOfEmployeeWorkingDayJsComponent($request);

            // Send presentation data for work time components
            $work_time_presentation = WorkTime::where('visible', 1)
                    ->where('company_id', Auth::user()->company_id)
                    ->get()->transform(function($item, $key) {
                        $data = $item->name;
                        $type = false;
                        if ($item->start_work_time == null && $item->end_work_time == null) {
                            $item->working_hour = substr($item->working_hour, 0, -3);
                            $data .= ' ' . $item->working_hour;
                        } else {
                            if ($item->start_work_time != null)
                                $item->start_work_time = substr($item->start_work_time, 0, -3);
                            if ($item->end_work_time != null)
                                $item->end_work_time = substr($item->end_work_time, 0, -3);
                            $data .= ' ' . $item->start_work_time . '~' . $item->end_work_time;
                            $type = true;
                        }
                        return [
                            'id'    => $item->id,
                            'work_location_id' => $item->work_location_id,
                            'name'  => $item->name,
                            'data'  => $data,
                            'type'  => $type,
                        ];
                    })->toArray();
            $work_times = WorkTime::where('visible', 1)
                    ->where('company_id', Auth::user()->company_id)
                    ->get()->toArray();

            Javascript::put([
                'autocomplete_work_time_data' => $work_time_presentation,
                'display_autocomplete_work_time' => $work_times,
            ]);

            // Send data for the date picker components. This part is a normal date picker(located at the date navigation at the top, not the schedule-transfer-purposed calendars)
            $this->sendDatePickerData($employee_working_day->employee_id);

            // Send data related to snapshot component
            $this->getSinseiData($employee_working_day);

            // Send data for the schedule-transfer-purposed calendars. These date pickers are inside of each employee_working_information component
            $schedule_transfer_data = $this->getScheduleTransferData($employee_working_day->employeeWorkingInformations, $employee_working_day->employee_id);
            Javascript::put([
                'schedule_transfer_data' => $schedule_transfer_data,
            ]);

            // Send data for the alert-when-work-time-does-not-match function(the numbers turn red).
            // $work_locations_settings = $this->extractDataFromWorkLocationSetting($employee_working_day->employeeWorkingInformations);
            // Javascript::put([
            //     'alert_setting_data' => $work_locations_settings,
            // ]);

            return view('attendance.employee.working_day', [
                'working_day'   =>  $employee_working_day,
                'can_conclude' => $this->checkIfCanConcludeThisDayToday($employee_working_day->date) && Auth::user()->can('conclude_level_one'),
            ]);

        } else {
            abort(404, 'Can not find attendance information for this employee on that day!');
        }

    }

    /**
     * Retrieve the EmployeeWorkingInformation list (and their coresponding schedule_transfer_data) of an EmployeeWorkingDay
     *
     * @param EmployeeWorkingDay        $employee_working_day
     * @return array
     */
    public function retrieve(EmployeeWorkingDay $employee_working_day)
    {
        return [
            'working_infos' => $this->extractNecessaryData($employee_working_day->employeeWorkingInformations->sortBy('planned_start_work_time')->values()),
            'schedule_transfer_data' => $this->getScheduleTransferData($employee_working_day->employeeWorkingInformations, $employee_working_day->employee_id),
        ];
    }

    /**
     * Transfer all the EmployeeWorkingInformations for an EmployeeWorkingDay to another.
     *
     * @param WorkingInformationTransferRequest     $request
     * @return \Illuminate\Http\Response
     */
    public function scheduleTransfer(WorkingInformationTransferRequest $request)
    {
        $this->transferWorkingDay(
            $request->input('employee_id'),
            $request->input('from_date'),
            $request->input('to_date'),
            $request->user()
        );

        $request->session()->flash('success', '保存しました');
        return [
            'success' => '保存しました'
        ];
    }


    /**
     * Mark this instance of EmployeeWorkingDay as concluded level one. And export summary data (if any).
     *
     * @param \Illuminate\Http\Request   $request
     * @param EmployeeWorkingDay    $working_day
     * @return \Illuminate\Http\Response
     */
    public function concludeLevelOne(Request $request, EmployeeWorkingDay $employee_working_day)
    {
        $check = $this->checkIfAWorkingDayCanBeConcluded($employee_working_day);
        if ($check === 1) {

            event(new EmployeeWorkingDayConcluded($employee_working_day, $request->user()));
            event(new PaidHolidayChanged($employee_working_day->employee));
            $request->session()->flash('success', '保存しました');
            return back();

        } else if ($check === 0) {
            $request->session()->flash('error', '打刻エラーがあるため、締める事ができません。');
            return back();
        } else if ($check === -1) {
            $request->session()->flash('error', '管理者２がすでに、締めました！');
            return back();
        } else {
            $request->session()->flash('error', '申請中は締める事ができません。');
            return back();
        }

    }


    /**
     * Mark this instance of EmployeeWorkingDay as NOT concluded level one.
     *
     * @param \Illuminate\Http\Request   $request
     * @param EmployeeWorkingDay    $working_day
     * @return \Illuminate\Http\Response
     */
    public function unconcludedLevelOne(Request $request, EmployeeWorkingDay $employee_working_day)
    {
        if ($this->checkIfAWorkingDayCanBeUnconcluded($employee_working_day) == true) {

            event(new EmployeeWorkingDayUnconcluded($employee_working_day));
            event(new PaidHolidayChanged($employee_working_day->employee));

            $request->session()->flash('success', '保存しました');
            return back();

        } else {
            $request->session()->flash('error', '管理者２がすでに、締めました！');
            return back();
        }
    }


    /**
     * Check if the user are allowed to make a WorkingTimestamp for yesterday
     *
     * @param \Illuminate\Http\Request   $request
     * @return string
     */
    protected function checkIfCanMakeYesterdayTimestamp($request)
    {
        return $request->user()->company->date_separate_type === Company::APPLY_TO_THE_DAY_AFTER;
    }

    /**
     * Check if the given EmployeeWorkingDay only has temporary EmployeeWorkingInformation
     *
     * @param EmployeeWorkingDay    $working_day
     * @return string
     */
    protected function haveOnlyTemporaryEWI($working_day)
    {
        return $working_day->employeeWorkingInformations->every(function($working_info) {
            return $working_info->temporary == true;
        });
    }

    /**
     * Check if today the user can conclude this EmployeeWorkingDay (Because there's a spec about not allow concluding future days)
     *
     * @param string        $date   the day to check
     * @return boolean
     */
    protected function checkIfCanConcludeThisDayToday($date)
    {
        $current_date = new Carbon($date);
        $today = Carbon::today()->hour(23)->minute(59)->second(59);
        return $current_date->lt($today);
    }


    /**
     * Extract the necessary data from a collection of working informations
     *
     * @param Collection      $working_infos
     * @param Collection
     */
    protected function extractNecessaryData($working_infos)
    {
        return $working_infos->map(function($info) {
            $data = $info->necessaryDataForTheVueComponent();

            // $start_work_error = $info->checklistItems->first(function($error) {
            //     return $error->error_type === ChecklistItem::START_WORK_ERROR;
            // });
            // $end_work_error = $info->checklistItems->first(function($error) {
            //     return $error->error_type === ChecklistItem::END_WORK_ERROR;
            // });
            // $data['has_start_work_error'] = isset($start_work_error) ? true : false;
            // $data['has_end_work_error'] = isset($end_work_error) ? true : false;

            return $data;
        });
    }

    /**
     * Prepare the data that support the presentational logic (list of work_location, list of work_status,... etc.) for the javascript side
     *
     * @param Illuminate\Http\Request   $request        the request instance
     * @return void
     */
    protected function sendPresentationalDataOfEmployeeWorkingDayJsComponent(Request $request)
    {

        $all_work_locations = $request->user()->company->workLocations()->with([
                                                                        'unusedWorkStatuses',
                                                                        'unusedRestStatuses',
                                                                        'company.workStatuses',
                                                                        'company.restStatuses',
                                                                        'workAddresses'
                                                                        ])->get();

        // The list of work location with (if any) work address use for the timestamps section
        $places = [];
        foreach ($all_work_locations as $work_location) {
            $places[] = [
                'work_location_id'  => $work_location->id,
                'work_address_id'   => null,
                'name'              => $work_location->name,
            ];
            foreach ($work_location->workAddresses as $work_address) {
                $places[] = [
                    'work_location_id'      => $work_location->id,
                    'work_address_id'       => $work_address->id,
                    'name'                  => $work_location->name . ' ' . $work_address->name,
                    'work_location_name'    => $work_location->name,
                    'work_address_name'     => $work_address->name,
                ];
            }
        }

        // The list of work location with work statuses and rest statuses
        $work_locations = $all_work_locations->map(function($work_location) {
            return [
                'id'            => $work_location->id,
                'name'          => $work_location->name,
                'work_statuses' => $work_location->activatingWorkStatuses()->map(function($status) {
                    return [
                        'id'    => $status->id,
                        'name'  => $status->name,
                    ];
                }),
                'rest_statuses' => $work_location->activatingRestStatuses()->map(function($status) {
                    return [
                        'id'        => $status->id,
                        'name'      => $status->name,
                        'day_based' => $status->unit_type == true,
                    ];
                }),
                'utc_offset_number'          => $work_location->getTimezone()->utc_offset_number,
                'start_time_round_up'          => $work_location->currentSetting()->start_time_round_up,
                'end_time_round_down'          => $work_location->currentSetting()->end_time_round_down,
                'is_use_go_out_button'          => $work_location->currentSetting()->go_out_button_usage == Setting::USE_GO_OUT_BUTTON,
            ];
        });

        // work statuses of company
        $all_work_statuses_list = $request->user()->company->workStatuses->map(function($status) {
            return [
                'id'    => $status->id,
                'name'  => $status->name,
            ];
        });

        // rest statuses list of company
        $all_rest_statuses_list = $request->user()->company->restStatuses->map(function($status) {
            return [
                'id'        => $status->id,
                'name'      => $status->name,
                'day_based' => $status->unit_type == true,
            ];
        });

        $timestamp_types = [
            WorkingTimestamp::START_WORK => "出勤",
            WorkingTimestamp::END_WORK => "退勤",
            WorkingTimestamp::GO_OUT => "外出",
            WorkingTimestamp::RETURN => "戻り",
        ];

        Javascript::put([
            'work_locations'        => $work_locations,
            'timestamp_types'       => $timestamp_types,
            'timestamp_places'      => $places,
            'all_work_statuses'     => $all_work_statuses_list,
            'all_rest_statuses'     => $all_rest_statuses_list,
        ]);
    }

    /**
     * Prepare data for the approver form pop-up
     *
     * @param EmployeeWorkingDay        $employee_working_day
     * @return void
     */
    protected function getSinseiData($employee_working_day)
    {
        if ($employee_working_day->employeeWorkingInformationSnapshots->isNotEmpty()) {
            $data = collect([]);

            $date_array = $this->checkDateList([$employee_working_day->date], $employee_working_day->employee);

            foreach ($date_array as $date) {
                $data->push($this->sendDataFollowWorkingDay($date, $employee_working_day->employee));
            }

            Javascript::put([
                'snapshot_days' => $data,
                // 'employee_id' => $employee_working_day->employee_id,
            ]);

            $work_location_setting_service = resolve(WorkLocationSettingService::class);
            if ($work_location_setting_service->getCompany()->use_address_system) {
                $work_address_names = WorkAddress::pluck('name', 'id');
                Javascript::put([
                    'work_address_names' => $work_address_names,
                ]);
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\WorkAddress;
use App\WorkLocation;
use App\WorkAddressWorkingDay;
use App\WorkAddressWorkingInformation;
use Carbon\Carbon;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Services\NationalHolidayService;
use App\Services\WorkLocationSettingService;
use Auth;

class WorkAddressAttendanceController extends Controller
{
    /**
     * Number of rows per paginated data's page.
     *
     * @var integer
     */
    protected $rows_per_page = 25;

    /**
     * The key to save the paginated data to the session.
     * @var string
     */
    protected $paginated_data_session_key = 'work_address_pagination';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose');
        $this->middleware('can:view_work_address_attendance_page');
    }

    /**
     * Show table of work addresses's working informations
     *
     * @param \Illuminate\Http\Request      $request
     * @param boolean                       $reset_search_condition
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $reset_search_condition = false)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        if ($reset_search_condition == true) {
            // Reset the search conditions that was saved in session
            session()->forget('work_address_attendance_search_history');
            
            // Reset the breadcumbs if reset the search history
            $breadcrumbs_service->resetToAttendanceRoot();
        }
        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::WORK_ADDRESS_ATTENDANCE, $request->route()->getName(), $request->route());

        // Get the search conditions if the session if there are saved search conditions
        if (session()->has('work_address_attendance_search_history')) {
            $conditions = session('work_address_attendance_search_history');

        // Or if not, we're just gonna get the default conditions
        } else {
            $conditions = $this->getDefaultSearchConditions();
        }

        // Send date picker data
        $this->sendDatePickerData();

        // $data = $this->getAttendanceData($conditions);
        $day_list = $this->getDayList($conditions['anchor_date']);

        $this->prepareDataForSearchBox();

        Javascript::put([
            'anchor_date' => $conditions['anchor_date'],
            'search_hitory' => session('work_address_attendance_search_history'),
            'day_list' => $day_list,
        ]);

        return view('attendance.work_address.attendance');
    }

    /**
     * Search for employees's working informations base on given conditions
     *
     * @param \Illuminate\Http\Request  $request
     * @return array|json
     */
    public function search(Request $request, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => false ]);

        $conditions = $request->only([
            'anchor_date',
            'employee_presentation_id',
            'employee_name',
            'work_address_name',
        ]);

        // Save the search history to session
        session(['work_address_attendance_search_history' => $conditions]);

        $data = $this->getAttendanceData($conditions, $page);

        return [
            'table_set' => view('attendance.work_address.attendance_table', [
                'work_address_data' => $data['work_addresses'],
                'most_left_day' => $data['day_list']->first()['date'],
            ])->render(),
            'scroll_pagination_info' => $this->extractPaginationInformation($data['paginator']),
            'day_list' => $data['day_list'],
            'anchor_date'     => $conditions['anchor_date'],
            'search_hitory' => session('work_address_attendance_search_history'),
        ];
    }

    /**
     * Retrieve work addresses's working information base on the anchor date.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array|json
     */
    public function retrieveDataByDate(Request $request, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => false ]);

        // Get the search conditions if the session if there are saved search conditions
        if (session()->has('work_address_attendance_search_history')) {
            $conditions = session('work_address_attendance_search_history');

        // Or if not, we're just gonna get the default conditions
        } else {
            $conditions = $this->getDefaultSearchConditions();
        }

        // Then we override the anchor date base on this request
        $conditions['anchor_date'] = $request->input('anchor_date');

        // And save the new search history to session
        session(['work_address_attendance_search_history' => $conditions]);

        $data = $this->getAttendanceData($conditions, $page);

        return [
            'table_set' => view('attendance.work_address.attendance_table', [
                'work_address_data' => $data['work_addresses'],
                'most_left_day' => $data['day_list']->first()['date'],
            ])->render(),
            'scroll_pagination_info' => $this->extractPaginationInformation($data['paginator']),
            'day_list' => $data['day_list'],
            'anchor_date'     => $conditions['anchor_date'],
            'search_hitory' => session('work_address_attendance_search_history'),
        ];
    }

    /**
     * Get a page of the paginated data set.
     *
     *
     * @param integer  $page
     * @return array|json
     */
    public function scroll($page = 1)
    {
        // With this function, there should definitely be a work_address_attendance_search_history in the session.
        // If there isn't, there should be something wrong
        $conditions = session('work_address_attendance_search_history');

        $data = $this->getAttendanceData($conditions, $page);

        return [
            'table_set' => view('attendance.work_address.attendance_table', [
                'work_address_data' => $data['work_addresses'],
                'most_left_day' => $data['day_list']->first()['date'],
            ])->render(),
            'scroll_pagination_info' => $this->extractPaginationInformation($data['paginator']),
        ];
    }

    /**
     * Retrieve data from database, build the data set to display.
     *
     * @param array     $conditions     seach conditions
     * @return array
     */
    protected function getAttendanceData($conditions, $page = 1)
    {
        $day_list = $this->getDayList($conditions['anchor_date']);

        // Retrieve data
        list($work_addresses, $work_address_working_days) = $this->retrieveTwoDataSetsInOptimalWay($conditions, $page, $day_list);

        //////////////////////////////////////////////////////////////////////////////////
        // Now that we have those two lists, next step is to build the data set to display

        // First is convert WorkAddressWorkingDay list into a hash table for easy lookup
        $work_address_working_days = $work_address_working_days->keyBy(function($working_day) {
            return $working_day->date . '-' . $working_day->work_address_id;
        });

        $data_for_all_work_addresses = [];

        foreach($work_addresses as $work_address) {
            $data_for_this_work_address = [];
            $maximum_number_of_lines = 1;

            foreach ($day_list as $day) {
                $unique_key = $day['date'] . '-' . $work_address->id;

                if (!$work_address_working_days->has($unique_key)) {
                    $data_for_this_work_address[$day['date']] = [];

                } else {
                    $working_infos = $work_address_working_days[$unique_key]->getCompactedWorkingInformations()->values();

                    $data_for_this_work_address[$day['date']] = $working_infos;

                    $maximum_number_of_lines = count($working_infos) > $maximum_number_of_lines ? count($working_infos) : $maximum_number_of_lines;
                }
            }
            $data_for_all_work_addresses[] = [
                'id' => $work_address->id,
                'presentation_id' => $work_address->presentation_id,
                'name' => $work_address->name,
                'data' => $data_for_this_work_address,
                'max_lines' => $maximum_number_of_lines,
                'work_location_id' => $work_address->work_location_id,
                'furigana' => $work_address->furigana,
            ];
        }

        $data_for_all_work_addresses = collect($data_for_all_work_addresses);
        // Now that we have the data set, we will proceed to add entries serve as a work_location label, and also sort the data in specific order.
        $transformed_data = collect([]);

        $single_work_location_id = session('current_work_location');

        if (is_numeric($single_work_location_id)) {
            $transformed_data = $transformed_data->concat($data_for_all_work_addresses);

        } else {
            $work_location_setting_service = resolve(WorkLocationSettingService::class);

            $current_labeled_work_location_id = null;
            foreach ($data_for_all_work_addresses as $work_address) {
                if ($work_address['work_location_id'] != $current_labeled_work_location_id) {
                    $transformed_data->push([
                        'work_location_label' => true,
                        'work_location_name' => $work_location_setting_service->getWorkLocationById($work_address['work_location_id'])->name,
                        'work_location_id' => $work_address['work_location_id'],
                    ]);
                    $current_labeled_work_location_id = $work_address['work_location_id'];
                }
                $transformed_data->push($work_address);
            }
        }

        return [
            'work_addresses' => $transformed_data,
            'day_list' => $day_list,
            'paginator' => $work_addresses, // this variable is also a paginator, which hold the pagination info.
        ];
    }

    /**
     * Retrieve the two data set of WorkAddress and WorkAddressWorkingInformation. In an, supposedly, optimal way.
     * Base on the condition of the search, we can perform this query before that query, or the other way around, etc. to
     * make the whole process fasters.
     *
     * @param   array       $conditions     the conditions from the search box
     * @param   integer     $page           the page of the pagination
     * @param   array       $day_list       the day list
     * @return  array       consist of two elements: first is work_addresses, second is work_address_working_days
     */
    protected function retrieveTwoDataSetsInOptimalWay($conditions, $page, $day_list)
    {

        // If there is no condition for employee, then the result's row number is depend on the WorkAddress data set
        // So we can paginate the WorkAddress data set to paginate the desired result data
        if (!isset($conditions['employee_presentation_id']) && !isset($conditions['employee_name'])) {

            // Paginate the result data set of WorkAddresses
            $work_addresses = $this->createWorkAddressesQuery($conditions, true);
            $work_addresses = $work_addresses->paginate($this->rows_per_page, ['work_addresses.*'], 'page', $page);

            // Then use those id to query WorkAddressWorkingDay, it will reduce the query time.
            $work_address_working_days = $this->createWorkAddressWorkingDaysQuery($conditions, $day_list, true, $work_addresses->pluck('id'))->get();

        // If there is even one condition for employee, then the result is no longer a UNION, but instead a INTERSECTION between the WorkAddressWorkingDay
        // data set and WorkAddress data set, and furthermore the result's row number is depend on the WorkAddressWorkingDay data set
        // So we have to, somehow, paginate the WorkAddressWorkingDay data set in order to paginate the desired result data
        } else {

            // In this flow it will go opposite, first query the WorkAddressWorkingDay(no need to eager loading or paginate) to get their ids.
            $work_address_working_days = $this->createWorkAddressWorkingDaysQuery($conditions, $day_list, false, null);
            $work_address_ids = $work_address_working_days->pluck('work_address_id');

            // Use those ids to apply one more condition to query WorkAddresses, then paginate the result set.
            $work_addresses = $this->createWorkAddressesQuery($conditions, true, $work_address_ids);
            $work_addresses = $work_addresses->paginate($this->rows_per_page, ['work_addresses.*'], 'page', $page);

            // Finally, use those WorkAddress ids above to query WorkAddressWorkingDay again, this time, we won't need $conditions from the search box,
            // Only need the WorkAddress ids from above. Also, we will eagerloading this time
            $work_address_working_days = $this->createWorkAddressWorkingDaysQuery(null, $day_list, true, $work_addresses->pluck('id'))->get();
        }

        return [ $work_addresses, $work_address_working_days ];
    }

    /**
     * Create query for WorkAddress model
     *
     * @param   array       $conditions             from the search box, mandatory, can not be null
     * @param   boolean     $sort                   determine top sort or not
     * @param   array|null  $work_address_ids       an array of ids to further filter the result
     * @return  \Illuminate\Database\Query\Builder the query
     */
    protected function createWorkAddressesQuery($conditions, $sort = false, $work_address_ids = null)
    {
        $current_work_location_ids = $this->getCurrentWorkLocationIds();

        $query = WorkAddress::with('workLocation')->whereIn('work_location_id', $current_work_location_ids);
        if (isset($conditions['work_address_name'])) {
            $query->where('name', 'like', '%' . $conditions['work_address_name'] . '%');
        }

        if ($work_address_ids) {
            $query->whereIn('work_addresses.id', $work_address_ids);
        }

        if ($sort) {
            // We have to sort the data in order to paginate
            $query->join('work_locations', 'work_locations.id', '=', 'work_location_id')->orderBy('work_locations.view_order')->orderBy('work_addresses.furigana');
        }

        return $query;
    }

    /**
     * Create query for WorkAddressWorkingDay model.
     * $conditions and $day_list are mandatory parameter, but $conditions can be null.
     *
     * @param   array|null      $conditions         the search conditions from the search box. In this fucntion, this parameter can be null.
     * @param   array           $day_list
     * @param   boolean         $eager_loading      flag to eager loading or not.
     * @param   array|null      $work_address_ids   additional work_address_ids to filter down the result
     * @return  \Illuminate\Database\Query\Builder  the query
     */
    protected function createWorkAddressWorkingDaysQuery($conditions, $day_list, $eager_loading = true, $work_address_ids = null)
    {
        $query = WorkAddressWorkingDay::where('date', '>=', $day_list->first()['date'])->where('date', '<=', $day_list->last()['date']);

        if  ($conditions && (isset($conditions['employee_presentation_id']) || isset($conditions['employee_name']))) {
            $temp_query = clone $query;
            // Temporary create some query so that we can get the ids of WorkAddressWorkingDay that sastify the conditions
            // and then use that ids to apply to the main query. Doing it this way, we can avoid those two inefficient queries.
            $temp_query->join('work_address_working_informations', 'work_address_working_informations.work_address_working_day_id', '=', 'work_address_working_days.id')
            ->join('work_address_working_employees', 'work_address_working_employees.work_address_working_information_id', '=', 'work_address_working_informations.id')
            ->join('employees', 'employees.id', '=', 'work_address_working_employees.employee_id');

            if (isset($conditions['employee_presentation_id'])) {

                // This query will KILL US ALL if the WorkAddress or Employee number get really big, which DEFINITELY WILL.
                // With 300 Employees and 300 WorkAddresses (all have PlannedSchedule), this query takes 44 seconds, dear Lord!
                // $query->whereHas('workAddressWorkingInformations.workAddressWorkingEmployees.employee', function ($sub_query) use ($conditions) {
                //     $sub_query->where('presentation_id', $conditions['employee_presentation_id']);
                // });

                // Alternative way
                $temp_query->where('employees.presentation_id', $conditions['employee_presentation_id']);
                $filtering_ids = $temp_query->pluck('work_address_working_days.id');
                $query->whereIn('id', $filtering_ids);
            }

            if (isset($conditions['employee_name'])) {

                // This query will KILL US ALL if the WorkAddress or Employee number get really big, which DEFINITELY WILL.
                // With 300 Employees and 300 WorkAddresses (all have PlannedSchedule), this query takes 44 seconds, dear Lord!
                // $query->whereHas('workAddressWorkingInformations.workAddressWorkingEmployees.employee', function ($sub_query) use ($conditions) {
                //     $sub_query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
                // });

                // Alternative way
                $temp_query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
                $filtering_ids = $temp_query->pluck('work_address_working_days.id');
                $query->whereIn('id', $filtering_ids);

            }
        }

        if (isset($work_address_ids)) {
            $query->whereIn('work_address_id', $work_address_ids);
        }

        if ($eager_loading) {
            $query->with([
                'workAddress.workLocation',
                'workAddressWorkingInformations.workAddressWorkingDay',
                'workAddressWorkingInformations.workAddressWorkingEmployees.employee',
                'workAddressWorkingInformations.workAddressWorkingEmployees.plannedSchedule',
                'workAddressWorkingInformations.workAddressWorkingEmployees.employeeWorkingInformation.plannedSchedule',
                'workAddressWorkingInformations.workAddressWorkingEmployees.employeeWorkingInformation.employeeWorkingDay.employee.workLocation.company',
                'workAddressWorkingInformations.workAddressWorkingEmployees.employeeWorkingInformation.workAddressWorkingEmployee.workAddressWorkingInformation.workAddressWorkingEmployees.plannedSchedule',
                'workAddressWorkingInformations.workAddressWorkingEmployees.employeeWorkingInformation.workAddressWorkingEmployee.plannedSchedule',
                'workAddressWorkingInformations.workAddressWorkingEmployees.employeeWorkingInformation.cachedEmployeeWorkingInformation',
                'workAddressWorkingInformations.workAddressWorkingEmployees.employeeWorkingInformation.concludedEmployeeWorkingInformation',
            ]);
        }

        return $query;
    }

    /**
     * Extract necessary information from the paginator to send to javascript
     *
     * @param  \Illuminate\Contracts\Pagination\LengthAwarePaginator    $paginator
     * @return array|json
     */
    protected function extractPaginationInformation($paginator)
    {
        return [
            'current_page' => $paginator->currentPage(),
            'number_of_pages' => ceil($paginator->total()/$paginator->perPage()),
            'can_load_previous_data' => $paginator->currentPage() > 1,
            'can_load_next_data' => $paginator->currentPage() !== $paginator->lastPage(),
        ];
    }

    /**
     * Prepare the employee names list and workAddress names list
     *
     * @return void
     */
    protected function prepareDataForSearchBox()
    {
        $employee_names = Auth::user()->company->employees->map(function($item) {
            return ['name' => $item->last_name . $item->first_name];
        })->toArray();

        $current_work_location = session('current_work_location');
        if ($current_work_location === 'all') {
            $work_address_names = Auth::user()->company->workAddresses->map(function($item) {
                return ['name' => $item->name];
            })->toArray();

        } else if (is_array($current_work_location)) {
            $work_address_names = WorkAddress::whereIn('work_location_id', $current_work_location)->get()->map(function($item) {
                return ['name' => $item->name];
            })->toArray();

        } else {
            $work_address_names = WorkAddress::where('work_location_id', $current_work_location)->get()->map(function($item) {
                return ['name' => $item->name];
            })->toArray();
        }

        Javascript::put([
            'employee_names'    => $employee_names,
            'work_address_names'    => $work_address_names,
        ]);
    }

    /**
     * Get the default search conditions.
     *
     * @return array
     */
    protected function getDefaultSearchConditions()
    {
        return [
            'anchor_date' => Carbon::today()->hour(1)->minute(0)->second(0)->format('Y-m-d'),
            'employee_presentation_id' => null,
            'employee_name' => null,
            'work_address_name' => null,
        ];
    }

    /**
     * Get the current work locations's ids.
     * Reminder: the current_work_location variable in the session can be:
     *      - a string:     'all' - meaning the current chosen work location is the whole 'company'
     *      - an array:     contain some work location ids
     *      - an integer:   the id of a work location
     *
     * @return array
     */
    protected function getCurrentWorkLocationIds()
    {
        $work_location_setting_service = resolve(WorkLocationSettingService::class);

        $current_work_location_in_the_session = session('current_work_location');

        if ($current_work_location_in_the_session === 'all') {
            $current_work_location_ids = $work_location_setting_service->getAllWorkLocations()->pluck('id');
        } else if (is_array($current_work_location_in_the_session)) {
            $current_work_location_ids = $current_work_location_in_the_session;
        } else {
            $current_work_location_ids = [$current_work_location_in_the_session];
        }

        return $current_work_location_ids;
    }

    /**
     * Get list of 7 days, base on a given date.
     * Edit: right now, the given day will be the start day of that week.
     *
     * @param string    $anchor_date  format: Y-m-d
     * @return collection
     */
    protected function getDayList($anchor_date)
    {
        // Carbonize
        $anchor_date = Carbon::createFromFormat('Y-m-d', $anchor_date);

        $start_date = $anchor_date->copy();
        $end_date = $anchor_date->copy()->addDays(6);
        $date_pivot = $start_date->copy();
        $days = [];
        while ($date_pivot->lte($end_date)) {
            $days[] = [
                'date' => $date_pivot->toDateString(),
                // Carbon's day of weeks: 0 => sunday, 6 => saturday
                'dow' => $date_pivot->dayOfWeek,
            ];
            $date_pivot->addDay();
        }

        return collect($days);
    }

    /**
     * Send data to normal date picker.
     *
     * @return void
     */
    protected function sendDatePickerData()
    {
        $current_work_location = session('current_work_location');

        $work_location_setting_service = resolve(WorkLocationSettingService::class);

        if ($current_work_location === "all") {
            $setting = Auth::user()->company->setting;
            $rest_days = Auth::user()->company->calendarRestDays()->get(['assigned_date', 'type'])->keyBy('assigned_date')->toArray();
        } else if (is_int($current_work_location)) {
            $setting = $work_location_setting_service->getCurrentSettingOfWorkLocationById($current_work_location);
            $rest_days = $work_location_setting_service->getWorkLocationById($current_work_location)->getRestDays();
        }

        $national_holiday_service = resolve(NationalHolidayService::class);
        $national_holidays = $national_holiday_service->get();

        Javascript::put([
            'date_picker_data' => [
                'rest_days'             => isset($rest_days) ? $rest_days : [],
                'national_holidays'     => $national_holidays,
                'flip_color_day'        => isset($setting) ? $setting->salary_accounting_day : null,
            ],
        ]);
    }
}
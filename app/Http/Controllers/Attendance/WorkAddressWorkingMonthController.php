<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use App\WorkAddress;
use App\WorkAddressWorkingDay;
use App\WorkAddressWorkingEmployee;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use App\Http\Controllers\Reusables\ManageWorkAddressWorkingEmployeeTrait;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;

class WorkAddressWorkingMonthController extends Controller
{
    use BusinessMonthTrait, ManageWorkAddressWorkingEmployeeTrait;
    // CanCreateWorkingDayOnTheFlyTrait, UseWorkAddressWorkingInformationTrait;

    /**
     * Controller of WorkAddressWorkingEmployee model
     *
     * Need to borrow that controller to toggle the working employee
     */
    protected $work_address_working_employee_controller;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(WorkAddressWorkingEmployeeController $working_employee_controller)
    {
        $this->middleware('auth');
        $this->middleware('choose');
        $this->middleware('can:view_attendance_work_address_working_month_page');
        $this->middleware('can:change_attendance_work_address_working_month_page')->only(['toggleAWorkingEmployee']);
        $this->work_address_working_employee_controller = $working_employee_controller;
    }

    /**
     * Show all working days of an work address of a business month
     *
     * @param \Illuminate\Http\Request  $request            the request instance
     * @param WorkAddress               $work_address       the work address
     * @param string                    $business_month     the business month
     * @param string                    $day     the day to determine business month, if this parameter presents, ignore the $business_month
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, WorkAddress $work_address, $business_month = null, $day = null)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::WORK_ADDRESS_WORKING_MONTH, $request->route()->getName(), $request->route());

        if ($day) {
            $business_month = $this->getBusinessMonthThatContainASpecificDay($work_address->workLocation->currentSetting(), $day);
        } else if ($business_month) {
            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');

        } else {
            $business_month = $this->calculateBusinessMonthBaseOnASetting($work_address->workLocation->currentSetting());
        }

        $start_and_end_date = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($work_address->workLocation->currentSetting(), $business_month);

        $data = $this->getWorkAddressWorkingDataFromDateRange($work_address, $start_and_end_date[0], $start_and_end_date[1]);

        Javascript::put([
            'business_month'  => $business_month->format('Y-m'),
            'working_days' => $data['working_days'],
            'work_address' => $work_address,
            'consts_sunday' => Carbon::SUNDAY,
            'consts_saturday' => Carbon::SATURDAY,
            'can_view_attendance_work_address_working_day_page' => $request->user()->can('view_attendance_work_address_working_day_page'),
            'can_change_attendance_work_address_working_month_page' => $request->user()->can('change_attendance_work_address_working_month_page'),
        ]);

        return view('attendance.work_address.working_month', [
            'work_address' => $work_address,
            'business_month' => $business_month->format('Y-m'),
        ]);
    }

    /**
     * Toggle a WorkAddressWorkingEmployee's working confirm in the working month page. Essentially this toggle and the toggle function in the
     * WorkAddressWorkingEmployeeController do the same thing. But they return different things.
     *
     * @param integer                       $working_day_id
     * @param WorkAddressWorkingEmployee    $working_employee
     * @return json|array
     */
    public function toggleAWorkingEmployee($working_day_id, WorkAddressWorkingEmployee $working_employee)
    {

        // Use the borrowed working employee controller to toggle the given working employee.
        $response = $this->work_address_working_employee_controller->toggle($working_employee);

        if (is_array($response) && isset($response['success'])) {

            // refresh the WorkingAddressWorkingDay
            $working_day = WorkAddressWorkingDay::find($working_day_id);

            return [
                'success' => '保存しました',
                'data' => $working_day->getCompactedWorkingInformations(),
            ];
        } else {
            return $response;
        }
    }
}

<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Manager;
use App\Employee;
use App\WorkAddress;
use App\WorkStatus;
use App\RestStatus;
use App\WorkAddressWorkingDay;
use App\EmployeeWorkingInformation;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Services\NationalHolidayService;
use App\Services\WorkLocationSettingService;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;
use App\Http\Controllers\Reusables\UseWorkAddressWorkingInformationTrait;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;

class WorkAddressWorkingDayController extends Controller
{
    use CanCreateWorkingDayOnTheFlyTrait, UseWorkAddressWorkingInformationTrait, BusinessMonthTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose');
        $this->middleware('can:view_attendance_work_address_working_day_page');
    }

    /**
     * Show an WorkAddressWorkingDay page
     *
     * @param \Illuminate\Http\Request  $request                the request instance
     * @param int                       $work_address_id        the id of the work address
     * @param string                    $date                   the working day
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, $work_address_id, $date)
    {
        // Validate the date by format 'yyyy-mm-dd'
        if ($this->validateDateByFormat($date)) {

            // Get the breadcrumb service
            $breadcrumbs_service = resolve(BreadCrumbService::class);
            $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::WORK_ADDRESS_WORKING_DAY, $request->route()->getName(), $request->route());

            // UPDATE 2019-01-31: now we have to change parent node(namely employee working month page)'s parameters
            // so that we can return back to the suitable business month
            if ($breadcrumbs_service->isParentOfCurrentNode(BreadCrumb::WORK_ADDRESS_WORKING_MONTH)) {
                $work_address = WorkAddress::find($work_address_id);
                $suitable_business_month = $this->getBusinessMonthThatContainASpecificDay($work_address->workLocation->currentSetting(), $date);
                $parameter_to_overwrite = [
                    'business_month'=> $suitable_business_month->format('Y-m'),
                    'day' => null,
                ];
                $breadcrumbs_service->changeParametersOfTheParentOfTheCurrentNode($parameter_to_overwrite);
            }

            $working_day = WorkAddressWorkingDay::where('work_address_id', $work_address_id)->where('date', '=', $date)->first();

            // Make sure there is always a working_day, if that work_address_id is valid.
            if (!$working_day) {
                $working_day = $this->createWorkAddressWorkingDayOnTheFly($work_address_id, $date);
            }

            // We need to makeHidden on employeeWorkingInformation for each workAddressWorkingEmployee, or else, it will cause an infinite loop.
            // And, for some reason, makeHidden('workAddressWorkingEmployees.employeeWorkingInformation') didn't work.
            $working_infos = $working_day->workAddressWorkingInformations()->with('workAddressWorkingEmployees.employeeWorkingDay')->get();
            foreach($working_infos as $working_info) {
                $this->hideSomeRelationshipsOfWorkAddressWorkingInformation($working_info);
            }

            // Create WorkStatus list and RestStatus list, these lists are a little special. They have to come from a specific WorkLocation's Setting and
            // then, have to go through a specific filter. This filter is specific made for WorkAddressWorkingDay page to displayed.
            $work_location_setting_service = resolve(WorkLocationSettingService::class);
            $work_location = $work_location_setting_service->getWorkLocationById($working_day->workAddress->work_location_id);
            $work_stastuses_specific_for_work_address = $work_location->activatingWorkStatuses()->filter(function($status) {
                return $status->id !== WorkStatus::ZANGYOU && $status->id !== WorkStatus::FURIKYUU;
            })->pluck('name', 'id');
            $rest_statuses_specific_for_work_address = $work_location->activatingRestStatuses()->filter(function($status) {
                return $status->unit_type == true;
            })->pluck('name', 'id');

            // Get all temporary employee working information that related to this work address today
            $list_of_employees_without_ewi_but_have_timestamps = $this->getTemporaryEmployeeWorkingInformationOfThisWorkAddressWorkingDay($working_day->work_address_id, $working_day->date);

            Javascript::put([
                'current_date'   => $working_day->date,
                'working_day_id' => $working_day->id,
                'work_address_working_infos' => $working_infos,
                'date_upper_limit' => $working_day->date_upper_limit,
                'day_of_the_upper_limit' => $working_day->day_of_the_upper_limit,
                'manager_names' => Manager::all()->map(function($manager) {return ['id' => $manager->id, 'name' => $manager->fullName()];})->pluck('name', 'id'),
                'employee_names' => Employee::all()->map(function($employee) {return ['id' => $employee->id, 'name' => $employee->fullName(), 'presentation_id' => $employee->presentation_id];}),
                'work_status_names' => $work_stastuses_specific_for_work_address,
                'rest_status_names' => $rest_statuses_specific_for_work_address,
                'can_change_data' => $request->user()->can('change_attendance_work_address_working_day_page'),
                'can_view_employee_working_day' => $request->user()->can('view_attendance_employee_working_day'),
                'employees_without_ewi_but_have_timestamps' => $list_of_employees_without_ewi_but_have_timestamps,
            ]);

            $this->sendDatePickerData($working_day->workAddress->workLocation);

            return view('attendance.work_address.working_day', [
                'working_day'   =>      $working_day,
                'work_address'  =>      $working_day->WorkAddress,
                'work_location_name' =>      $working_day->WorkAddress->WorkLocation->name,
            ]);

        } else {
            abort(404, 'Can not find attendance information for this work address on that day!');
        }
    }

    /**
     * Get all the temporary employee working information that related to this work address today.
     *
     * @param   int     $work_address_id
     * @param   string  $date
     * @return  collection
     */
    protected function getTemporaryEmployeeWorkingInformationOfThisWorkAddressWorkingDay($work_address_id, $date)
    {
        $temporary_employee_working_informations = EmployeeWorkingInformation::from('employee_working_informations as working_info')
                                                    ->select('working_info.*')
                                                    ->join('employee_working_days as working_day', 'working_info.employee_working_day_id', '=', 'working_day.id')
                                                    ->where('working_day.date', '=', $date)
                                                    ->where('working_day.concluded_level_one', false)
                                                    ->where('working_day.concluded_level_two', false)
                                                    ->where('working_info.temporary', true)
                                                    ->where('working_info.not_show_when_temporary', '!=', true)
                                                    ->where('working_info.planned_work_address_id', $work_address_id)
                                                    ->with([
                                                        'employeeWorkingDay.employee',
                                                        'plannedSchedule',
                                                        'workAddressWorkingEmployee.plannedSchedule'
                                                    ])
                                                    ->get();

        $list_of_employees_without_ewi_but_have_timestamps = $temporary_employee_working_informations->reduce(function ($result, $working_info) use ($work_address_id) {
            if (!$result->has($working_info->employeeWorkingDay->employee->id)) {
                $result->put($working_info->employeeWorkingDay->employee->id, [
                    'id'                            => $working_info->employeeWorkingDay->employee->id,
                    'presentation_id'               => $working_info->employeeWorkingDay->employee->presentation_id,
                    'name'                          => $working_info->employeeWorkingDay->employee->fullName(),
                    'timestamps'                    => collect([]),
                ]);
            };

            $result[$working_info->employeeWorkingDay->employee->id]['timestamps']->push([
                'timestamped_start_work_time'   => $working_info->timestamped_start_work_time,
                'timestamped_end_work_time'     => $working_info->timestamped_end_work_time,
            ]);

            return $result;
        }, collect([]))->values();

        return $list_of_employees_without_ewi_but_have_timestamps;
    }

    /**
     * Send data to the normal date picker
     *
     * @param WorkLocation       $work_location,
     * @return void
     */
    protected function sendDatePickerData($work_location)
    {
        if ($work_location) {

            $rest_days = $work_location->getRestDays();

            $national_holidays_service = resolve(NationalHolidayService::class);
            $national_holidays = $national_holidays_service->get();

            Javascript::put([
                'rest_days'             => $rest_days,
                'national_holidays'     => $national_holidays,
                'flip_color_day'        => $work_location->currentSetting()->salary_accounting_day,
            ]);
        }
    }
}

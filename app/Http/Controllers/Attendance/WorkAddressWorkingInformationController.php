<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Routing\Controller;
use App\WorkStatus;
use App\WorkAddressWorkingEmployee;
use App\WorkAddressWorkingInformation;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use App\Events\WorkingTimestampChanged;
use App\Events\CachedEmployeeWorkingInformationBecomeOld;
use App\Http\Controllers\Reusables\ManageWorkAddressWorkingEmployeeTrait;
use App\Http\Controllers\Reusables\UseWorkAddressWorkingInformationTrait;
use App\Http\Controllers\Reusables\ChangeBackGroundColorOfAttributesTrait;
use Illuminate\Http\Request;
use App\Http\Requests\WorkAddressWorkingInformationRequest;

class WorkAddressWorkingInformationController extends Controller
{
    use ManageWorkAddressWorkingEmployeeTrait, UseWorkAddressWorkingInformationTrait, ChangeBackGroundColorOfAttributesTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose');
        $this->middleware('can:change_attendance_work_address_working_day_page');
    }

    /**
     * Store the new working information instance
     *
     * @param WorkAddressWorkingInformationRequest      $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkAddressWorkingInformationRequest $request)
    {
        $new_working_info = new WorkAddressWorkingInformation($request->only([
            'work_address_working_day_id',
            'schedule_start_work_time',
            'schedule_end_work_time',
            'candidate_number',
            'note',
        ]));

        $new_working_info->schedule_start_work_time_modified = true;
        $new_working_info->schedule_end_work_time_modified = true;
        $new_working_info->candidate_number_modified = true;
        $new_working_info->candidate_number = $new_working_info->candidate_number ? $new_working_info->candidate_number : 0;

        $new_working_info->modified_manager_id = $request->user()->id;

        $new_working_info->save();
        $new_working_info->fresh();

        return [
            'success' => '保存しました',
            'data' => $new_working_info,
        ];
    }

    /**
     * Update a WordAddressWorkingInformation model
     *
     * @param WorkAddressWorkingInformationRequest         $request
     * @param WorkAddressWorkingInformation                $employee_working_info
     * @return \Illuminate\Http\Response
     */
    public function update(WorkAddressWorkingInformationRequest $request, WorkAddressWorkingInformation $work_address_working_info)
    {
        $work_address_working_info->fill($request->only([
            'schedule_start_work_time',
            'schedule_end_work_time',
            'candidate_number',
            'note',
        ]));
        if ($request->input('schedule_start_work_time_changed')) {
            $work_address_working_info->schedule_start_work_time_modified = true;
        }
        if ($request->input('schedule_end_work_time_changed')) {
            $work_address_working_info->schedule_end_work_time_modified = true;
        }
        if ($request->input('candidate_number_changed')) {
            $work_address_working_info->candidate_number_modified = true;
        }

        // Check if this WorkAddressWorkingInformation can add more WorkAddressWorkingEmployee
        if (!$this->canAddMoreWorkingEmployeeTo($work_address_working_info)) {
            return response()->json([
                'error' => '必要人数を満たしています。',
                'candidate_number' => [
                    '必要人数を満たしています。',
                ],
            ], 401);
        }

        $this->changeScheduleModifiedOnAllWorkingEmployees($work_address_working_info, $request->only([ 'schedule_start_work_time', 'schedule_start_work_time_changed', 'schedule_end_work_time', 'schedule_end_work_time_changed']));

        // $work_address_working_info->note = $request->input('note') ? $request->input('note') : $work_address_working_info->note;

        $work_address_working_info->modified_manager_id = $request->user()->id;

        $work_address_working_info->save();

        $this->hideSomeRelationshipsOfWorkAddressWorkingInformation($work_address_working_info);

        return [
            'success' => '保存しました',
            'data' => $work_address_working_info,
        ];
    }

    /**
     * Destroy an WorkAddressWorkingInformation instance
     *
     * @param WorkAddressWorkingInformation        $employee_working_info
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkAddressWorkingInformation $work_address_working_info)
    {
        // Not delete WorkAddressWorkingInformation that contain employee who has timestamps
        $has_working_employee_with_timestamps = $work_address_working_info->workAddressWorkingEmployees->first(function($working_employee) {
            return $working_employee->timestamps_exist_or_not == true;
        });

        if (!$has_working_employee_with_timestamps) {
            $work_address_working_info->delete();
        } else {
            return response()->json(['error' => '打刻した従業員がいます。'], 401);
        }

        return [
            'success' => '削除しました',
        ];
    }

    /**
     * From the given Employees create (or toggle) WorkAddressWorkingEmployee (with proper EmployeeWorkingInformation) for the given WorkAddressWorkingInformation.
     *
     * @param   \Illuminate\Http\Request        $request
     * @param   WorkAddressWorkingInformation   $work_address_working_info
     * @return  array|json
     */
    public function addEmployee(Request $request, WorkAddressWorkingInformation $work_address_working_info)
    {

        $new_employee_ids = $request->input('employee_ids');

        if (is_array($new_employee_ids)) {

            foreach ($new_employee_ids as $employee_id) {
                $already_existing_working_employee = $work_address_working_info->workAddressWorkingEmployees->first(function($working_employee) use ($employee_id) {
                    return $working_employee->employee_id == $employee_id;
                });

                // Check if this WorkAddressWorkingInformation can add more WorkAddressWorkingEmployee
                if (!$this->canAddMoreWorkingEmployeeTo($work_address_working_info->id, true)) {
                    return response()->json(['error' => '必要人数を満たしています。'], 401);
                }

                $employee_working_day = null;
                if ($already_existing_working_employee) {
                    if ($already_existing_working_employee->working_confirm == false) {
                        $already_existing_working_employee->working_confirm = true;
                        $already_existing_working_employee->save();

                        $this->createNewEmployeeWorkingInformation($already_existing_working_employee);

                    }
                    $employee_working_day = $already_existing_working_employee->employeeWorkingDay;

                } else {
                    $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee_id)->where('date', $work_address_working_info->workAddressWorkingDay->date)->first();

                    if ($employee_working_day) {
                        $new_work_address_working_employee = new WorkAddressWorkingEmployee([
                            'employee_id' => $employee_id,
                            'employee_working_day_id' => $employee_working_day->id,
                            'work_address_working_information_id' => $work_address_working_info->id,
                            'working_confirm' => true,
                        ]);
                        $new_work_address_working_employee->save();

                        $this->createNewEmployeeWorkingInformation($new_work_address_working_employee);

                    } else {
                        return response()->json(['error' => 'エラーがあります。'], 401);
                    }
                }

                // Re-distribute WorkingTimestamps and re-check ChecklistError
                event(new WorkingTimestampChanged($employee_working_day));
            }

            return [
                'success' => '保存しました',
            ];

        } else {
            return response()->json(['error' => 'データの形式が正しくないです。'], 401);
        }
    }

    /**
     * Change the schedule_modified attribute of all relating WorkAddressWorkingEmployees of the given WorkAddressWorkingInformation
     *
     * @param WorkAddressWorkingInformation         $working_info
     * @param array                                 $data_for_start_work_and_end_work      use this data to determine whether or not to change to color of schedule_start/end_work_time OR to update the planned_overtime_start/end
     * @return void
     */
    protected function changeScheduleModifiedOnAllWorkingEmployees(WorkAddressWorkingInformation $working_info, $data_for_start_work_and_end_work)
    {
        $working_employees = $working_info->workAddressWorkingEmployees()->with('employeeWorkingInformation')->get();

        foreach($working_employees as $employee) {
            $employee->schedule_modified = true;
            $employee->save();

            // Also set these attribute on the relating EmployeeWorkingInformation to make sure these attributes will become "special"
            $employee_working_info = $employee->employeeWorkingInformation;
            if ($employee_working_info) {
                $employee_working_info->planned_work_status_id = $employee_working_info->planned_work_status_id;
                $employee_working_info->planned_rest_status_id = $employee_working_info->planned_rest_status_id;
                $employee_working_info->manually_modified = true;

                if ($employee_working_info->planned_work_status_id == WorkStatus::HOUDE || $employee_working_info->planned_work_status_id == WorkStatus::KYUUDE) {
                    $employee_working_info->planned_break_time = $employee_working_info->planned_break_time;
                    $employee_working_info->planned_night_break_time = $employee_working_info->planned_night_break_time;

                    // Check to update value of planned_overtime_start/end AND ALSO change the color respectively
                    if ($data_for_start_work_and_end_work['schedule_start_work_time_changed']) {
                        $employee_working_info->planned_overtime_start = $data_for_start_work_and_end_work['schedule_start_work_time'];
                        $this->changeColorOfSnapshot($employee_working_info, 'planned_overtime_start');
                    }
                    if ($data_for_start_work_and_end_work['schedule_end_work_time_changed']) {
                        $employee_working_info->planned_overtime_end = $data_for_start_work_and_end_work['schedule_end_work_time'];
                        $this->changeColorOfSnapshot($employee_working_info, 'planned_overtime_end');
                    }
                    if ($data_for_start_work_and_end_work['schedule_start_work_time_changed'] || $data_for_start_work_and_end_work['schedule_end_work_time_changed']) {
                        $employee_working_info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
                        $employee_working_info->last_modify_person_id = \Auth::id();
                        $employee_working_info->temporary = false;
                        $this->createColorStatus($employee_working_info->id, 'planned_total_early_arrive_and_overtime');
                    }
                } else {
                    $employee_working_info->schedule_break_time = $employee_working_info->schedule_break_time;
                    $employee_working_info->schedule_night_break_time = $employee_working_info->schedule_night_break_time;

                    // Check to change color of schedule_start/end_work_time here
                    if ($data_for_start_work_and_end_work['schedule_start_work_time_changed']) {
                        $employee_working_info->manually_inputed_schedule_start_work_time = true;
                    }
                    if ($data_for_start_work_and_end_work['schedule_end_work_time_changed']) {
                        $employee_working_info->manually_inputed_schedule_end_work_time = true;
                    }
                    if ($data_for_start_work_and_end_work['schedule_start_work_time_changed'] || $data_for_start_work_and_end_work['schedule_end_work_time_changed']) {
                        $employee_working_info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
                        $employee_working_info->last_modify_person_id = \Auth::id();
                        $employee_working_info->temporary = false;
                    }
                }

                $employee_working_info->save();

                // Re-distribute WorkingTimestamps and re-check ChecklistError
                event(new WorkingTimestampChanged($employee->employeeWorkingDay));
            }

        }

        // Also need to refresh the cache of these EmployeeWorkingInformation
        $effected_employee_working_information_ids = $working_employees->pluck('employeeWorkingInformation.id')->toArray();
        event(new CachedEmployeeWorkingInformationBecomeOld($effected_employee_working_information_ids));
    }
}
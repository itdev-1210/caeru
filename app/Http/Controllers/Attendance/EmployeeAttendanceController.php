<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Employee;
use App\WorkLocation;
use App\EmployeeWorkingDay;
use App\EmployeeWorkingInformation;
use Carbon\Carbon;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Http\Controllers\Reusables\GetEmployeeBaseOnWorkLocationTrait as GetEmployeeTrait;
use App\Http\Controllers\Reusables\SendDatePickerDataBaseOnCurrentWorkLocationTrait as SendDatePickerDataTrait;
use App\Http\Controllers\Reusables\UseScheduleTypeIconTrait;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use App\Services\TimezonesService;
use App\Services\TimestampesAndWorkingDayConnectorService;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Services\WorkLocationSettingService;
use App\WorkingTimestamp;
use Auth;
use Constants;
use DB;

class EmployeeAttendanceController extends Controller
{
    use GetEmployeeTrait, SendDatePickerDataTrait, UseScheduleTypeIconTrait, BusinessMonthTrait;

    /**
     * Number of rows per paginated data's page.
     *
     * @var integer
     */
    protected $rows_per_page = 25;

    /**
     * Cache the current WorkLocation from session here.
     */
    private $eager_loaded_work_location = null;

    /**
     * Cache the company from session if the 'current_work_location' == all.
     */
    private $eager_loaded_company = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose');
        $this->middleware('can:view_attendance_management_page');
    }

    /**
     * Show table of emplopyees's working informations relate to a WorkLocation
     *
     * @param \Illuminate\Http\Request      $request
     * @param boolean                       $reset_search_condition
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $reset_search_condition = false)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        if ($reset_search_condition == true) {
            // Reset the search conditions that was saved in session
            session()->forget('employee_attendance_search_history');

            // Reset the breadcumbs if reset the search history
            $breadcrumbs_service->resetToAttendanceRoot();
        }
        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::EMPLOYEE_ATTENDANCE, $request->route()->getName(), $request->route());

        // Get the search conditions if the session if there are saved search conditions
        if (session()->has('employee_attendance_search_history')) {
            $conditions = session('employee_attendance_search_history');

        // Or if not, we're just gonna get the default conditions
        } else {
            $conditions = $this->getDefaultSearchConditions();
        }

        // Update 2019-02-07: to improve visual feedback, now the page will first appear with a blank table and then send a search request right after finish loading.
        // $data = $this->getAttendanceData($conditions);
        $day_list = $this->getDayList($conditions['anchor_date']);

        // If possible, eager load the current work location or company from the session
        $eager_loaded_work_location = $this->getEagerLoadedCurrentWorkLocation();
        $eager_loaded_company = $this->getEagerLoadedCompany();

        // Send date picker data
        $this->sendDatePickerData($eager_loaded_work_location, $eager_loaded_company);

        // Send search box data
        $this->prepareDataForSearchBox();

        // Night Shift (Start/End Date)
        $current_work_location = session('current_work_location');
        $company = Auth::user()->company;
        if ($current_work_location === 'all') {
            $setting = $company->setting;

        } elseif (is_array($current_work_location)) {
            $setting = $company->setting;
        } else {
            $work_location = WorkLocation::find($current_work_location);
            $setting = $work_location->currentSetting();
        }
        $business_month = $this->calculateBusinessMonthBaseOnASetting($setting);
        $pairOfDaysOfTheMonth = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($setting, $business_month);

        Javascript::put([
            // 'center_date' => $conditions['center_date'],
            'day_list' => $day_list,
            'display_go_out' => $this->getCurrentWorkLocationSetting()->display_go_out_time,
            'anchor_date' => $conditions['anchor_date'],
            'start_date'       => $pairOfDaysOfTheMonth[0]->format('Y-m-d'),
            'end_date'         => $pairOfDaysOfTheMonth[1]->format('Y-m-d'),
            'search_hitory' => session('employee_attendance_search_history'),
            'setting_data' => $this->getSettingData($request),
            'current_work_location_name' => $this->getCurrentWorkLocationName(),
        ]);

        return view('attendance.employee.attendance', [
            // 'employees_data' => [],     // $data['employees']
            'work_locations' => request()->user()->company->workLocations->keyBy('id'),
            'company'   => $company
        ]);
    }

    /**
     * Search for employees's working informations base on given conditions
     *
     * @param \Illuminate\Http\Request  $request
     * @return array|json
     */
    public function search(Request $request, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => false ]);

        $conditions = $request->only([
            // 'center_date',
            'anchor_date',
            'employee_presentation_id',
            'employee_name',
            'work_status',
            'departments',
            'belong_to_current_work_location',
        ]);

        // Save the search history to session
        session(['employee_attendance_search_history' => $conditions]);

        $data = $this->getAttendanceData($conditions, $page);

        return [
            'table_set'     => view('attendance.employee.attendance_table', [
                'employees_data' => $data['employees'],
                'work_locations' => request()->user()->company->workLocations->keyBy('id'),
                'display_go_out' => $this->getCurrentWorkLocationSetting()->display_go_out_time,
            ])->render(),
            'anchor_date'     => $conditions['anchor_date'],
            'day_list' => $data['day_list'],
            'scroll_pagination_info' => $this->extractPaginationInformation($data['paginator']),
            // 'search_hitory' => session('employee_attendance_search_history'),
            // 'display_go_out' => $this->getCurrentWorkLocationSetting()->display_go_out_time,
        ];
    }

    /**
     * Add Night Shift
     *
     * @param \Illuminate\Http\Request  $request
     * @return array|json
     */
    public function addTimestampsForNightshift(Request $request)
    {
        // Needed services
        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);

        $current_manager = Auth::user();
        $company = $current_manager->company;
        if (!$company->use_night_shift)
            return [
                'error' => 'Not Allow to change'
            ];

        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => false ]);

        $conditions = $request->only([
            'start_date',
            'end_date',
        ]);

        $day_list = $this->getDayListforNightShift($conditions);
        list($employees, $working_days) = $this->retrieveTwoDataSetsInOptimalWayForNightShift($day_list);

        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '-' . $working_day->employee_id;
        });

        $now = Carbon::now();
        foreach($employees as $employee) {
            $timestamps_for_this_employee = [];
            $redistribution_needed_working_days_for_this_employee = [];
            foreach($day_list as $day) {

                if ($day['date'] == $day_list->last()['date']) continue;

                $this_day_carbon = new Carbon($day['date']);
                $this_day_unique_key = $this_day_carbon->toDateString() . '-' . $employee->id;
                $next_day_carbon = $this_day_carbon->copy()->addDay();
                $next_day_unique_key = $next_day_carbon->toDateString() . '-' . $employee->id;
                $this_day = $working_days->has($this_day_unique_key) ? $working_days[$this_day_unique_key] : null;
                $next_day = $working_days->has($next_day_unique_key) ? $working_days[$next_day_unique_key] : null;

                if (isset($this_day) && !$this_day->isConcluded() && !$this_day->isHavingConsideringRequest() &&
                    isset($next_day) && !$next_day->isConcluded() && !$next_day->isHavingConsideringRequest())
                {
                    $redistribution_needed_working_days_for_this_employee[] = $this_day;

                    $working_infos = $this_day->employeeWorkingInformations;
                    $timestamps_of_this_day = $this_day->workingTimestamps;
                    $timestamps_of_this_day = $timestamps_of_this_day->sortBy(function($timestamp) {
                        return $timestamp->raw_date_time_value . '#' . $timestamp->created_at;
                    })->values();

                    for ($loop_index = 0; $loop_index < count($timestamps_of_this_day) - 1; $loop_index++) {
                        $current_timestamp = $timestamps_of_this_day[$loop_index];

                        if ($current_timestamp->timestamped_type == WorkingTimestamp::START_WORK) {
                            $next_timestamp = null;
                            $loop_index_to_search_for_next_end_work_timestamp = $loop_index + 1;
                            while ($next_timestamp == null && $loop_index_to_search_for_next_end_work_timestamp < count($timestamps_of_this_day)) {
                                $small_pivot_timestamp = $timestamps_of_this_day[$loop_index_to_search_for_next_end_work_timestamp];
                                if ($small_pivot_timestamp && $small_pivot_timestamp->timestamped_type == WorkingTimestamp::END_WORK && $small_pivot_timestamp->enable == true) {
                                    $next_timestamp = $small_pivot_timestamp;
                                }
                                $loop_index_to_search_for_next_end_work_timestamp++;
                            }

                            if ($next_timestamp) {
                                $start_work_timestamp = $current_timestamp;
                                $end_work_timestamp = $next_timestamp;

                                $start_work_carbon = Carbon::createFromFormat('Y-m-d H:i:s', $start_work_timestamp->raw_date_time_value);
                                $end_work_carbon = Carbon::createFromFormat('Y-m-d H:i:s', $end_work_timestamp->raw_date_time_value);

                                $pass_time_carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this_day->date . $company->pass_time . ':00');
                                if ($pass_time_carbon->lt($start_work_carbon)) $pass_time_carbon->addDay();
                                $difference_limit = $company->over_time;

                                if ($start_work_carbon->lt($pass_time_carbon) && $end_work_carbon->gt($pass_time_carbon) &&
                                    $start_work_carbon->diffInMinutes($end_work_carbon) >= $difference_limit*60)
                                {
                                    $work_location = $work_location_setting_service->getWorkLocationById($start_work_timestamp->work_location_id);
                                    $timezone = $work_location->getTimezone();

                                    // Create the new end work timestamp
                                    $the_end_timestamp_in_between_carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this_day->date . $company->auto_end_timestamp . ':00');
                                    if ($the_end_timestamp_in_between_carbon->lt($start_work_carbon)) $the_end_timestamp_in_between_carbon->addDay();
                                    if ($the_end_timestamp_in_between_carbon->gt($end_work_carbon)) $the_end_timestamp_in_between_carbon->subDay();
                                    $unique_key_of_the_end_timestamp_in_between = $the_end_timestamp_in_between_carbon->toDateString() . '-' . $employee->id;

                                    $to_be_inserted_end_work['enable'] = true;
                                    $to_be_inserted_end_work['processed_date_value'] = $the_end_timestamp_in_between_carbon->toDateString();
                                    $to_be_inserted_end_work['processed_time_value'] = $the_end_timestamp_in_between_carbon->toTimeString();
                                    $to_be_inserted_end_work['timestamped_type'] = WorkingTimestamp::END_WORK;
                                    $to_be_inserted_end_work['work_location_id'] = $start_work_timestamp->work_location_id;
                                    $to_be_inserted_end_work['work_address_id'] = $start_work_timestamp->work_address_id;
                                    $to_be_inserted_end_work['registerer_type'] = WorkingTimestamp::MANAGER;
                                    $to_be_inserted_end_work['registerer_id'] = $current_manager->id;
                                    $to_be_inserted_end_work['raw_date_time_value'] = $the_end_timestamp_in_between_carbon->format('Y-m-d H:i:s');
                                    $to_be_inserted_end_work['name_id'] = $timezone->name_id;
                                    $to_be_inserted_end_work['employee_working_day_id'] = $working_days[$unique_key_of_the_end_timestamp_in_between]->id;
                                    $to_be_inserted_end_work['created_at'] = $now;
                                    $to_be_inserted_end_work['updated_at'] = $now;
                                    $timestamps_for_this_employee[] = $to_be_inserted_end_work;

                                    // Create the new start work timestamp
                                    $the_start_timestamp_in_between_carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this_day->date . $company->auto_start_timestamp . ':00');
                                    if ($the_start_timestamp_in_between_carbon->lt($start_work_carbon)) $the_start_timestamp_in_between_carbon->addDay();
                                    if ($the_start_timestamp_in_between_carbon->gt($end_work_carbon)) $the_start_timestamp_in_between_carbon->subDay();
                                    $unique_key_of_the_start_timestamp_in_between = $the_start_timestamp_in_between_carbon->toDateString() . '-' . $employee->id;

                                    $to_be_inserted_start_work['enable'] = true;
                                    $to_be_inserted_start_work['processed_date_value'] = $the_start_timestamp_in_between_carbon->toDateString();
                                    $to_be_inserted_start_work['processed_time_value'] = $the_start_timestamp_in_between_carbon->toTimeString();
                                    $to_be_inserted_start_work['timestamped_type'] = WorkingTimestamp::START_WORK;
                                    $to_be_inserted_start_work['work_location_id'] = $start_work_timestamp->work_location_id;
                                    $to_be_inserted_start_work['work_address_id'] = $start_work_timestamp->work_address_id;
                                    $to_be_inserted_start_work['registerer_type'] = WorkingTimestamp::MANAGER;
                                    $to_be_inserted_start_work['registerer_id'] = $current_manager->id;
                                    $to_be_inserted_start_work['raw_date_time_value'] = $the_start_timestamp_in_between_carbon->format('Y-m-d H:i:s');
                                    $to_be_inserted_start_work['name_id'] = $timezone->name_id;
                                    $to_be_inserted_start_work['employee_working_day_id'] = $working_days[$unique_key_of_the_start_timestamp_in_between]->id;
                                    $to_be_inserted_start_work['created_at'] = $now->copy()->addSecond();
                                    $to_be_inserted_start_work['updated_at'] = $now->copy()->addSecond();
                                    $timestamps_for_this_employee[] = $to_be_inserted_start_work;
                                }
                            }
                        }

                    }
                }
            }

            if (count($timestamps_for_this_employee) > 0) {
                DB::table('working_timestamps')->insert($timestamps_for_this_employee);
                $connector_service->rearrangeAndConnectTheTimestampsToWorkingDaysWithoutImmediatelyEvaluateChecklistError($redistribution_needed_working_days_for_this_employee);
            }
        }

        return [
            'success' => '正確に追加できました！',
        ];
    }

    /**
     * Add Night Shift
     *
     * @param \Illuminate\Http\Request  $request
     * @return array|json
     */
    public function addNightShift(Request $request) {
        ini_set("memory_limit", "-1");
        $company = Auth::user()->company;
        if (!$company->use_night_shift)
            return [
                'error' => 'Not Allow to change'
            ];

        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => false ]);

        $conditions = $request->only([
            'start_date',
            'end_date',
        ]);

        $day_list = $this->getDayListforNightShift($conditions);
        list($employees, $working_days) = $this->retrieveTwoDataSetsInOptimalWayForNightShift($day_list);

        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '-' . $working_day->employee_id;
        });

        $now = Carbon::now();
        foreach($employees as $employee) {
            $workingTimestamps = array(); $ids = array();
            foreach($day_list as $index => $day) {
                if ($day['date'] == $day_list->last()['date']) continue;
                $unique_key = $day['date'] . '-' . $employee->id;
                $next_unique_key = $day_list[(int)$index+1]['date'] . '-' . $employee->id;
                if (!$working_days->has($unique_key)) continue;
                if ($working_days[$unique_key]->concluded_level_one) continue;
                if ($working_days->has($next_unique_key))
                    if ($working_days[$next_unique_key]->concluded_level_one) continue;
                $working_infos = $working_days[$unique_key]->employeeWorkingInformations;
                foreach($working_infos as $working_info) {
                    if (!$working_info->timestamped_start_work_time || !$working_info->timestamped_end_work_time) continue;
                    $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $working_info->timestamped_start_work_time);
                    $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $working_info->timestamped_end_work_time);
                    $nextDay = $day_list[(int)$index+1]['date'];

                    $pass_time = "";
                    $pass_time_start = Carbon::createFromFormat('Y-m-d H:i:s', $day['date'].' '.$company->pass_time.':00');
                    $pass_time_end = Carbon::createFromFormat('Y-m-d H:i:s', $nextDay.' '.$company->pass_time.':00');
                    if ($pass_time_start->gt($start_date) && $pass_time_start->lt($end_date)) {
                        $pass_time = $pass_time_start;
                    }
                    if ($pass_time_end->gt($start_date) && $pass_time_end->lt($end_date)) {
                        $pass_time = $pass_time_end;
                    }

                    if ($pass_time == "") break;

                    $is_considering_request = $this->isHavingConsideringRequest($working_info->getColorStatusData());
                    $diffHours = $end_date->diffInHours($start_date);

                    if ($company->over_time > $diffHours || $is_considering_request) continue;

                    $working_time_stamp = $working_info->endWorkingTimestamp;
                    
                    $tmp_date = Carbon::createFromFormat('Y-m-d H:i:s', $day['date'].' '.$company->auto_end_timestamp.':00');
                    $tmp_date = $tmp_date->gt($start_date) ? $day['date'].' '.$company->auto_end_timestamp.':00' : $nextDay.' '.$company->auto_end_timestamp.':00';
                    $workingTimestamp['enable'] = $working_time_stamp->enable;
                    $workingTimestamp['processed_date_value'] = $working_time_stamp->processed_date_value;
                    $workingTimestamp['processed_time_value'] = $working_time_stamp->processed_time_value;
                    $workingTimestamp['timestamped_type'] = WorkingTimestamp::END_WORK;
                    $workingTimestamp['work_location_id'] = $working_time_stamp->work_location_id;
                    $workingTimestamp['work_address_id'] = $working_time_stamp->work_address_id;
                    $workingTimestamp['registerer_type'] = WorkingTimestamp::MANAGER;
                    $workingTimestamp['registerer_id'] = $request->user()->id;
                    $workingTimestamp['raw_date_time_value'] = Carbon::createFromFormat('Y-m-d H:i:s', $tmp_date)->toDateTimeString();
                    $workingTimestamp['name_id'] = $working_time_stamp->name_id;
                    $workingTimestamp['employee_working_day_id'] = $working_days[$unique_key]->id;
                    $workingTimestamp['created_at'] = $now;
                    $workingTimestamp['updated_at'] = $now;
                    $workingTimestamps[] = $workingTimestamp;

                    /* Add Timestamp for Next Day */
                    $temp_start = Carbon::createFromFormat('Y-m-d H:i:s', $nextDay . ' ' . $company->auto_start_timestamp.':00');

                    $workingTimestamp['timestamped_type'] = WorkingTimestamp::START_WORK;
                    $workingTimestamp['employee_working_day_id'] = null;
                    if ($temp_start->gt($end_date)) {
                        $workingTimestamp['raw_date_time_value'] = Carbon::createFromFormat('Y-m-d H:i:s', $day['date'] . ' ' . $company->auto_start_timestamp.':00')->toDateTimeString();
                    } else {
                        $workingTimestamp['raw_date_time_value'] = Carbon::createFromFormat('Y-m-d H:i:s', $nextDay . ' ' . $company->auto_start_timestamp.':00')->toDateTimeString();
                    }
                    $workingTimestamps[] = $workingTimestamp;

                    $workingTimestamp['timestamped_type'] = WorkingTimestamp::END_WORK;
                    $workingTimestamp['registerer_type'] = $working_time_stamp->registerer_type;
                    $workingTimestamp['registerer_id'] = $working_time_stamp->registerer_id;
                    $workingTimestamp['raw_date_time_value'] = $end_date->toDateTimeString();
                    $workingTimestamps[] = $workingTimestamp;

                    $ids[] = $working_time_stamp->id;
                }
            }
            if (count($workingTimestamps) == 0 || count($ids) == 0) continue;
            WorkingTimestamp::whereIn('id', $ids)->delete();

            // Use the connector service instead of setting the relation ship like above
            $connector_service = resolve(TimestampesAndWorkingDayConnectorService::class);
            $connector_service->connectAutoTimestampsToWorkingDaysOfEmployee($workingTimestamps, $employee);
        }

        return;
    }

    /**
     * Retrieve employees's working information base on the center date.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array|json
     */
    public function retrieveDataByDate(Request $request, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => false ]);

        // Get the search conditions if the session if there are saved search conditions
        if (session()->has('employee_attendance_search_history')) {
            $conditions = session('employee_attendance_search_history');

        // Or if not, we're just gonna get the default conditions
        } else {
            $conditions = $this->getDefaultSearchConditions();
        }

        // Then we override the center date base on this request
        // $conditions['center_date'] = $request->input('center_date');
        $conditions['anchor_date'] = $request->input('anchor_date');

        // And save the new search history to session
        session(['employee_attendance_search_history' => $conditions]);

        $data = $this->getAttendanceData($conditions, $page);

        return [
            'table_set'     => view('attendance.employee.attendance_table', [
                'employees_data' => $data['employees'],
                'work_locations' => request()->user()->company->workLocations->keyBy('id'),
                'display_go_out' => $this->getCurrentWorkLocationSetting()->display_go_out_time,
            ])->render(),
            'anchor_date'   => $conditions['anchor_date'],
            'day_list' => $data['day_list'],
            'scroll_pagination_info' => $this->extractPaginationInformation($data['paginator']),
            // 'search_hitory' => session('employee_attendance_search_history'),
            // 'display_go_out' => $this->getCurrentWorkLocationSetting()->display_go_out_time,
        ];
    }

    /**
     * Get a page of the paginated data set.
     *
     *
     * @param integer  $page
     * @return array|json
     */
    public function scroll($page = 1)
    {
        // With this function, there should definitely be a work_address_attendance_search_history in the session.
        // If there isn't, there should be something wrong
        $conditions = session('employee_attendance_search_history');

        $data = $this->getAttendanceData($conditions, $page);

        return [
            'table_set' => view('attendance.employee.attendance_table', [
                'employees_data' => $data['employees'],
                'work_locations' => request()->user()->company->workLocations->keyBy('id'),
                'display_go_out' => $this->getCurrentWorkLocationSetting()->display_go_out_time,
            ])->render(),
            'scroll_pagination_info' => $this->extractPaginationInformation($data['paginator']),
        ];
    }

    /**
     * Extract necessary information from the paginator to send to javascript
     *
     * @param  \Illuminate\Contracts\Pagination\LengthAwarePaginator    $paginator
     * @return array|json
     */
    protected function extractPaginationInformation($paginator)
    {
        return [
            'current_page' => $paginator->currentPage(),
            'number_of_pages' => ceil($paginator->total()/$paginator->perPage()),
            'can_load_previous_data' => $paginator->currentPage() > 1,
            'can_load_next_data' => $paginator->currentPage() !== $paginator->lastPage(),
        ];
    }


    /**
     * Get the default search conditions for employee attendance page's search box.
     *
     * @return array
     */
    protected function getDefaultSearchConditions()
    {
        return [
            // 'center_date' => Carbon::today()->hour(1)->minute(0)->second(0)->addDays(2)->format('Y-m-d'),
            'anchor_date' => Carbon::today()->hour(1)->minute(0)->second(0)->subDay()->format('Y-m-d'),
            'employee_presentation_id' => null,
            'employee_name' => null,
            'departments' => [],
            'work_status' => config('constants.working'),
            'belong_to_current_work_location' => true,
        ];
    }

    /**
     * Retrieve the WorkingData, in a pre-defined format, base on given conditions
     *
     * @param array     $conditions
     * @return array
     */
    protected function getAttendanceData($conditions, $page = 1)
    {
        // $day_list = $this->getDayList($conditions['center_date']);
        $day_list = $this->getDayList($conditions['anchor_date']);

        $employees = [];

        list($employees, $working_days) = $this->retrieveTwoDataSetsInOptimalWay($conditions, $page, $day_list);

        /**
         * At this point, we need two things:
         *  - an array of Employee
         *  - an array of EmployeeWorkingDay
         *
         * to build the result array($data_for_all_employees).
         */
        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '-' . $working_day->employee_id;
        });
        $data_for_all_employees = [];
        $unique_work_locations = collect([]);

        foreach ($employees as $employee) {
            $data_for_this_employee = [];
            $concluded_info = [];
            $maximum_number_of_lines = 1;

            foreach ($day_list as $day) {

                $unique_key = $day['date'] . '-' . $employee->id;

                if (!$working_days->has($unique_key)) {
                    $data_for_this_employee[$day['date']] = [];

                    // Add concluded information
                    $concluded_info[$day['date']] = [];
                    $concluded_info[$day['date']]['concluded_level_one'] = false;
                    $concluded_info[$day['date']]['concluded_level_two'] = false;

                } else {
                    $working_infos = $working_days[$unique_key]->getCompactedWorkingInformations()->values();
                    $data_for_this_employee[$day['date']] = $working_infos;

                    // Add concluded information
                    $concluded_info[$day['date']] = [];
                    $concluded_info[$day['date']]['concluded_level_one'] = $working_days[$unique_key]->concluded_level_one;
                    $concluded_info[$day['date']]['concluded_level_two'] = $working_days[$unique_key]->concluded_level_two;

                    $maximum_number_of_lines = count($working_infos) > $maximum_number_of_lines ? count($working_infos) : $maximum_number_of_lines;
                }
            }
            $data_for_all_employees[] = [
                'id' => $employee->id,
                'presentation_id' => $employee->presentation_id,
                'name' => $employee->fullName(),
                'schedule_type_icon' => $this->getScheduleTypeIcon($employee->schedule_type),
                'data' => $data_for_this_employee,
                'concluded_info' => $concluded_info,
                'max_lines' => $maximum_number_of_lines,
                'work_location_id' => $employee->work_location_id,
                'view_order' => $employee->view_order,
            ];

            if (!$unique_work_locations->has($employee->work_location_id)) {
                $unique_work_locations->put($employee->work_location_id, $employee->workLocation->view_order);
            }
        }

        $data_for_all_employees = collect($data_for_all_employees);
        // These steps will add entries serve as a work_location label
        $transformed_data = collect([]);
        // Transform the data so that it can display like the request of Ms.Yamauchi

        // Normally, one work_location label entry is above all the entries of employees who belong to that work_location
        // But if the user choose the option '出勤した従業員', and the current work location is one specific work location,
        // then that work location's employees will always be displayed first prior to the others.
        $single_work_location_id = session('current_work_location');

        if (is_numeric($single_work_location_id) && $conditions['belong_to_current_work_location'] == true) {
            $transformed_data = $transformed_data->concat($data_for_all_employees); // ->sortBy('view_order')

        } else {
            if ($conditions['belong_to_current_work_location'] != true) {

                if (is_numeric($single_work_location_id)) {
                    // Get rid of the current working location's id in the unique array
                    $unique_work_locations->pull($single_work_location_id);

                    // So that we can manually add the entries of the current work location's employees first
                    $transformed_data->push([
                        'work_location_label' => true,
                        'work_location_id' => $single_work_location_id,
                    ]);

                    // Prioritize this WorkLocation's record and put them to the list first
                    $data_for_current_work_location = $data_for_all_employees->filter(function($row) use ($single_work_location_id) {
                        return $row['work_location_id'] == $single_work_location_id;
                    });

                    $transformed_data = $transformed_data->concat($data_for_current_work_location);

                    // Remove those records above from the data set
                    $data_for_all_employees = $data_for_all_employees->reject(function($row) use ($single_work_location_id) {
                        return $row['work_location_id'] == $single_work_location_id;
                    });
                    // After this we will sort and loop on the collection $unique_work_locations like normal
                }
            }

            // $unique_work_locations = $unique_work_locations->sort();
            // foreach ($unique_work_locations as $work_location_id => $work_location_view_order) {
            //     $transformed_data->push([
            //         'work_location_label' => true,
            //         'work_location_id' => $work_location_id,
            //     ]);

            //     $data_for_current_work_location = $data_for_all_employees->filter(function($row) use ($work_location_id) {
            //         return $row['work_location_id'] == $work_location_id;
            //     })->sortBy('view_order');

            //     $transformed_data = $transformed_data->concat($data_for_current_work_location);
            // }
            $current_labeled_work_location_id = null;
            foreach ($data_for_all_employees as $employee) {
                if ($employee['work_location_id'] != $current_labeled_work_location_id) {
                    $transformed_data->push([
                        'work_location_label' => true,
                        'work_location_id' => $employee['work_location_id'],
                    ]);
                    $current_labeled_work_location_id = $employee['work_location_id'];
                }
                $transformed_data->push($employee);
            }
        }

        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        // Set the 2nd dimension of this breadcrumb
        $breadcrumbs_service->setTheSecondDimensionForCurrentNode(collect($data_for_all_employees)->pluck('id'), 'employee');

        return [
            'employees' => $transformed_data,
            'day_list' => $day_list,
            'paginator' => $employees, // this variable is also a paginator, which hold the pagination info.
        ];
    }

    /**
     * Create query to retrieve Employee and EmployeeWorkingDay data in an, supposedly, optimal way.
     * The oder to execute queries is important, because it effects the performance time.
     *
     * @param   array       $day_list       the day list
     * @return  array       consist of two elements: first is employees, second is employee_working_days
     */
    protected function retrieveTwoDataSetsInOptimalWayForNightShift($day_list)
    {
        $current_work_location = $this->getCurrentWorkLocationIds();

        $query = Employee::with('workLocation');

        if ($current_work_location) {
            $query->whereIn('work_location_id', $current_work_location);
        }
        $employees = $query->get();

        $working_days = $this->createNightShiftEmployeeWorkingDaysQuery($day_list, true, $employees->pluck('id'))->get(['employee_working_days.*']);

        return [$employees, $working_days];
    }

    /**
     * Create query to retrieve Employee and EmployeeWorkingDay data in an, supposedly, optimal way.
     * The oder to execute queries is important, because it effects the performance time.
     *
     * @param   array       $conditions     the conditions from the search box
     * @param   integer     $page           the page of the pagination
     * @param   array       $day_list       the day list
     * @return  array       consist of two elements: first is employees, second is employee_working_days
     */
    protected function retrieveTwoDataSetsInOptimalWay($conditions, $page, $day_list)
    {
        $current_work_location = $this->getCurrentWorkLocationIds();

        if ($conditions['belong_to_current_work_location'] == true) {
            $employees = $this->createEmployeesQuery($conditions, $current_work_location, true);
            $employees = $employees->paginate($this->rows_per_page, ['employees.*'], 'page', $page);

            $working_days = $this->createEmployeeWorkingDaysQuery($conditions, $day_list, null, true, $employees->pluck('id'))->get(['employee_working_days.*']);

        } else {
            $working_days = $this->createEmployeeWorkingDaysQuery($conditions, $day_list, $current_work_location, false, null);
            $employee_ids = $working_days->pluck('employee_id');

            $employees = $this->createEmployeesQuery($conditions, null, true, $employee_ids);
            $employees = $employees->paginate($this->rows_per_page, ['employees.*'], 'page', $page);

            $working_days = $this->createEmployeeWorkingDaysQuery(null, $day_list, null, true, $employees->pluck('id'))->get(['employee_working_days.*']);
        }

        return [$employees, $working_days];
    }

    /**
     * Create query for Employee model.
     *
     * @param   array       $conditions                 conditions from the search box
     * @param   array|null  $current_work_location      current work location in session, used to filter, won't do anything if null
     * @param   boolean     $sort                       to sort or not to sort
     * @param   array|null  $employee_ids               an array of employee ids to filter, won't do anything if null
     * @return  \Illuminate\Database\Query\Builder      the query
     */
    protected function createEmployeesQuery($conditions, $current_work_location = null, $sort = false, $employee_ids = null)
    {
        $query = Employee::with('workLocation');

        if ($current_work_location) {
            $query->whereIn('work_location_id', $current_work_location);
        }

        if (isset($conditions['employee_presentation_id'])) {
            $query->where('employees.presentation_id', $conditions['employee_presentation_id']);
        }

        if (isset($conditions['employee_name'])) {
            $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
        }

        if (isset($conditions['work_status'])) {
            $query->where('work_status', $conditions['work_status']);
        }

        if (!empty($conditions['departments'])) {
            $query->whereIn('department_id', $conditions['departments']);
        }

        if ($employee_ids) {
            $query->whereIn('employees.id', $employee_ids);
        }

        if ($sort) {
            $query->join('work_locations', 'work_locations.id', '=', 'employees.work_location_id')->orderBy('work_locations.view_order')->orderBy('employees.view_order')->orderBy('employees.id');
        }

        return $query;
    }

    /**
     * Create query for EmployeeWorkingDay model.
     *
     * @param   array       $conditions                     conditions from the search box
     * @param   array       $day_list                       an array of days
     * @param   array|null  $work_location_ids_to_filter    an array of work location ids to filter, won't do anything if null
     * @param   boolean     $eager_loading                  to eager loading or not to eager loading
     * @param   array|null  $employee_ids                   an array of employee ids to filter, won't do anything if null
     * @return  \Illuminate\Database\Query\Builder          the query
     */
    protected function createEmployeeWorkingDaysQuery($conditions, $day_list, $work_location_ids_to_filter = null, $eager_loading = true, $employee_ids = null)
    {
        $query = EmployeeWorkingDay::where('date', '>=', $day_list->first()['date'])->where('date', '<=', $day_list->last()['date']);

        if ($conditions && (isset($conditions['employee_presentation_id']) || isset($conditions['employee_name']) || isset($conditions['work_status']) || isset($conditions['departments']))) {

            $query->join('employees', 'employees.id', '=', 'employee_working_days.employee_id');

            if (isset($conditions['employee_presentation_id'])) {
                $query->where('employees.presentation_id', $conditions['employee_presentation_id']);
            }

            if (isset($conditions['employee_name'])) {
                $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
            }

            if (isset($conditions['work_status'])) {
                $query->where('employees.work_status', $conditions['work_status']);
            }

            if (!empty($conditions['departments'])) {
                $query->whereIn('employees.department_id', $conditions['departments']);
            }
        }

        if ($employee_ids) {
            $query->whereIn('employee_id', $employee_ids);
        }

        if ($work_location_ids_to_filter) {
            $temp_query_for_planned_work_location_id = clone $query;
            $temp_query_for_real_work_location_id = clone $query;
            $temp_query_for_planned_work_location_id_through_planned_schedule = clone $query;
            $temp_query_for_planned_work_location_id_through_wawe = clone $query;

            $temp_query_for_planned_work_location_id = $temp_query_for_planned_work_location_id
            ->join('employee_working_informations', 'employee_working_informations.employee_working_day_id', '=', 'employee_working_days.id')
            ->whereIn('employee_working_informations.planned_work_location_id', $work_location_ids_to_filter)
            ->pluck('employee_working_days.id');

            // dump($temp_query_for_planned_work_location_id);

            // Right now, it is ensured that "when there is real_work_location_id, there is always planned_work_location as well", so we can skip checking the real_work_location_id
            // $temp_query_for_real_work_location_id = $temp_query_for_real_work_location_id->join('employee_working_informations', 'employee_working_informations.employee_working_day_id', '=', 'employee_working_days.id')
            // ->whereIn('employee_working_informations.real_work_location_id', $work_location_ids_to_filter)
            // ->pluck('employee_working_days.id');

            // dump($temp_query_for_real_work_location_id);

            $temp_query_for_planned_work_location_id_through_planned_schedule = $temp_query_for_planned_work_location_id_through_planned_schedule
            ->join('employee_working_informations', 'employee_working_informations.employee_working_day_id', '=', 'employee_working_days.id')
            ->join('planned_schedules', 'planned_schedules.id', '=', 'employee_working_informations.planned_schedule_id')
            ->whereIn('planned_schedules.work_location_id', $work_location_ids_to_filter)
            ->pluck('employee_working_days.id');

            // dump($temp_query_for_planned_work_location_id_through_planned_schedule);

            $temp_query_for_planned_work_location_id_through_wawe = $temp_query_for_planned_work_location_id_through_wawe
            ->join('employee_working_informations', 'employee_working_informations.employee_working_day_id', '=', 'employee_working_days.id')
            ->join('work_address_working_employees', 'work_address_working_employees.id', '=', 'employee_working_informations.work_address_working_employee_id')
            ->join('planned_schedules', 'planned_schedules.id', '=', 'work_address_working_employees.planned_schedule_id')
            ->whereIn('planned_schedules.work_location_id', $work_location_ids_to_filter)
            ->pluck('employee_working_days.id');

            // dump($temp_query_for_planned_work_location_id_through_wawe);

            $filtered_ids = $temp_query_for_planned_work_location_id->concat($temp_query_for_planned_work_location_id_through_planned_schedule)->concat($temp_query_for_planned_work_location_id_through_wawe)->unique();

            $query->whereIn('employee_working_days.id', $filtered_ids);
        }

        if ($eager_loading) {
            $query->with([
                'concludedEmployeeWorkingDay',
                'substituteEmployeeWorkingInformations',
                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                'employeeWorkingInformations.colorStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
                // 'employeeWorkingInformations.currentRealWorkLocation.setting',
                // 'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
                // 'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
                // 'employeeWorkingInformations.currentRealWorkLocation.company.setting',
                // 'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
                // 'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
                'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
                'employeeWorkingInformations.workAddressWorkingEmployee.plannedSchedule',
                'employeeWorkingInformations.workAddressWorkingEmployee.workAddressWorkingInformation.workAddressWorkingEmployees.plannedSchedule',
            ]);
        }

        return $query;
    }

    /**
     * Create query for Night Shift EmployeeWorkingDay model.
     *
     * @param   array       $day_list                       an array of days
     * @param   boolean     $eager_loading                  to eager loading or not to eager loading
     * @param   array|null  $employee_ids                   an array of employee ids to filter, won't do anything if null
     * @return  \Illuminate\Database\Query\Builder          the query
     */
    protected function createNightShiftEmployeeWorkingDaysQuery($day_list, $eager_loading = true, $employee_ids = null)
    {
        $query = EmployeeWorkingDay::where('date', '>=', $day_list->first()['date'])->where('date', '<=', $day_list->last()['date']);

        if ($employee_ids) {
            $query->whereIn('employee_id', $employee_ids);
        }

        if ($eager_loading) {
            $query->with([
                'employee',
                'concludedEmployeeWorkingDay',
                'employeeWorkingInformationSnapshots',
                'substituteEmployeeWorkingInformations',
                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                'employeeWorkingInformations.colorStatuses',
                'employeeWorkingInformations.endWorkingTimestamp',
                'workingTimestamps',
            ]);
        }

        return $query;
    }

    /**
     * Get the current work locations's ids.
     * Reminder: the current_work_location variable in the session can be:
     *      - a string:     'all' - meaning the current chosen work location is the whole 'company'
     *      - an array:     contain some work location ids
     *      - an integer:   the id of a work location
     *
     * @return array
     */
    protected function getCurrentWorkLocationIds()
    {
        $work_location_setting_service = resolve(WorkLocationSettingService::class);

        $current_work_location_in_the_session = session('current_work_location');

        if ($current_work_location_in_the_session === 'all') {
            $current_work_location_ids = $work_location_setting_service->getAllWorkLocations()->pluck('id');
        } else if (is_array($current_work_location_in_the_session)) {
            $current_work_location_ids = $current_work_location_in_the_session;
        } else {
            $current_work_location_ids = [$current_work_location_in_the_session];
        }

        return $current_work_location_ids;
    }

    /**
     * The same as the function above, except it get the name or at least the string represent the current work location(s)
     *
     * @return string
     */
    protected function getCurrentWorkLocationName()
    {
        // Try to eager loaded the current work location or the company
        $eager_loaded_work_location = $this->getEagerLoadedCurrentWorkLocation();
        $eager_loaded_company = $this->getEagerLoadedCompany();

        $current_work_location_name = null;

        if ($eager_loaded_company) {
            $current_work_location_name = '会社';
        } else if ($eager_loaded_work_location) {
            $current_work_location_name = $eager_loaded_work_location->name;
        } else {
            $current_work_location_name = '全勤務地';
        }

        return $current_work_location_name;
    }

    /**
     * The same as the two functions above, except it get the setting of the current work location(s)
     *
     * @return string
     */
    protected function getCurrentWorkLocationSetting()
    {
        // Try to eager loaded the current work location or the company
        $eager_loaded_work_location = $this->getEagerLoadedCurrentWorkLocation();
        $current_setting = null;

        if ($eager_loaded_work_location) {
            $current_setting = $eager_loaded_work_location->currentSetting();
        } else {
            $current_setting = request()->user()->company->setting;
        }

        return $current_setting;
    }

    /**
     * Get list of 7 days, base on a given date.
     * Edit: right now, the given day will be the start day of that week.
     *
     * @param string    $anchor_date  format: Y-m-d
     * @return collection
     */
    protected function getDayList($anchor_date)
    {
        // Carbonize
        $anchor_date = Carbon::createFromFormat('Y-m-d', $anchor_date);

        $start_date = $anchor_date->copy();
        $end_date = $anchor_date->copy()->addDays(6);
        $date_pivot = $start_date->copy();
        $days = [];
        while ($date_pivot->lte($end_date)) {
            $days[] = [
                'date' => $date_pivot->toDateString(),
                // Carbon's day of weeks: 0 => sunday, 6 => saturday
                'dow' => $date_pivot->dayOfWeek,
            ];
            $date_pivot->addDay();
        }

        return collect($days);
    }

    /**
     * Get list of days, base on a given date.
     * Edit: right now, the given day will be the start day of that week.
     *
     * @param array $conditions      format: Y-m-d
     * @return collection
     */
    protected function getDayListforNightShift($conditions)
    {
        // Carbonize
        $start_date = Carbon::createFromFormat('Y-m-d', $conditions['start_date']);
        $end_date = Carbon::createFromFormat('Y-m-d', $conditions['end_date'])->addDay();

        $date_pivot = $start_date->copy();
        $days = [];
        while ($date_pivot->lte($end_date)) {
            $days[] = [
                'date' => $date_pivot->toDateString(),
                // Carbon's day of weeks: 0 => sunday, 6 => saturday
                'dow' => $date_pivot->dayOfWeek,
            ];
            $date_pivot->addDay();
        }

        return collect($days);
    }

    /**
     * Get the setting data of the current work location
     * The data is about:
     *      - date_separate_time
     *      - start_time_round_up
     *      - end_time_round_down
     *      - break_time_round_up
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getSettingData(Request $request)
    {
        // Try to eager loaded the current work location or the company
        $eager_loaded_work_location = $this->getEagerLoadedCurrentWorkLocation();
        $eager_loaded_company = $this->getEagerLoadedCompany();

        if ($eager_loaded_company) {
            $setting['date_separate_time_hour'] = explode(':', $eager_loaded_company->date_separate_time)[0];
            $setting['date_separate_time_minute'] = explode(':', $eager_loaded_company->date_separate_time)[1];
            $setting['start_time_round_up'] = $eager_loaded_company->setting->start_time_round_up;
            $setting['end_time_round_down'] = $eager_loaded_company->setting->end_time_round_down;
            $setting['break_time_round_up'] = $eager_loaded_company->Setting->break_time_round_up;

        } else if ($eager_loaded_work_location) {
            $setting['date_separate_time_hour'] = explode(':', $eager_loaded_work_location->company->date_separate_time)[0];
            $setting['date_separate_time_minute'] = explode(':', $eager_loaded_work_location->company->date_separate_time)[1];
            $setting['start_time_round_up'] = $eager_loaded_work_location->currentSetting()->start_time_round_up;
            $setting['end_time_round_down'] = $eager_loaded_work_location->currentSetting()->end_time_round_down;
            $setting['break_time_round_up'] = $eager_loaded_work_location->currentSetting()->break_time_round_up;

        // The fall back (old code)
        } else {
            if (session('current_work_location') === 'all') {
                $setting['start_time_round_up'] = $company->setting->start_time_round_up;
                $setting['end_time_round_down'] = $company->setting->end_time_round_down;
                $setting['break_time_round_up'] = $company->Setting->break_time_round_up;

            } else if (is_numeric(session('current_work_location'))) {
                $work_location = WorkLocation::find(session('current_work_location'));

                $setting['start_time_round_up'] = $work_location->currentSetting()->start_time_round_up;
                $setting['end_time_round_down'] = $work_location->currentSetting()->end_time_round_down;
                $setting['break_time_round_up'] = $work_location->currentSetting()->break_time_round_up;
            }
        }

        return isset($setting) ? $setting : null;
    }

    /**
     * Prepare the department list and employees's name list for the search box
     *
     * @return void
     */
    protected function prepareDataForSearchBox()
    {
        // Try to eager loaded the current work location or the company
        $eager_loaded_work_location = $this->getEagerLoadedCurrentWorkLocation();
        $eager_loaded_company = $this->getEagerLoadedCompany();
        $employee_names = request()->user()->company->employees->map(function($item) {
            return ['name' => $item->last_name . $item->first_name];
        })->toArray();

        if ($eager_loaded_work_location) {
            $departments = $eager_loaded_work_location->activatingDepartments()->pluck('name', 'id')->toArray();
            // $employee_names = $eager_loaded_work_location->employees->map(function($item) {
            //     return ['name' => $item->last_name . $item->first_name];
            // })->toArray();

        } else if ($eager_loaded_company) {
            $departments = $eager_loaded_company->departments->pluck('name', 'id')->toArray();
            // $employee_names = $eager_loaded_company->employees->map(function($item) {
            //     return ['name' => $item->last_name . $item->first_name];
            // })->toArray();

        // Fall back - the elseif is still being used though
        } else {
            $current_work_location = session('current_work_location');

            // Get the list of department and employee's name, base on the current work location
            if ($current_work_location === 'all') {

                $departments = Auth::user()->company->departments->pluck('name', 'id')->toArray();

                // $employee_names = Auth::user()->company->employees->map(function($item) {
                //     return ['name' => $item->last_name . $item->first_name];
                // })->toArray();

            } elseif (is_array($current_work_location)) {

                $departments = Auth::user()->company->departments->pluck('name', 'id')->toArray();

                // $employee_names = Employee::whereIn('work_location_id', $current_work_location)->get()->map(function($item) {
                //     return ['name' => $item->last_name . $item->first_name];
                // })->toArray();

            } else {

                $work_location = WorkLocation::with('employees')->find($current_work_location);

                $departments = $work_location->activatingDepartments()->pluck('name', 'id')->toArray();

                $employee_names = request()->user()->company->employees->map(function($item) {
                    return ['name' => $item->last_name . $item->first_name];
                })->toArray();

            }
        }

        $employee_work_statuses = Constants::workStatuses();

        // Add variable department_companys when choose '出勤した従業員' from current work location select
        $departmentCompanys = Auth::user()->company->departments->pluck('name', 'id')->toArray();

        // Send the list of departments and employee names to the javascript side
        Javascript::put([
            'departments'       => $departments,
            'employee_names'    => $employee_names,
            'employee_work_statuses'    => $employee_work_statuses,
            'department_companys' => $departmentCompanys,
        ]);
    }

    /**
     * Apply some query conditions for Employee model
     * This function is use for searching for working information of Employees who belong to the current WorkLocation(s)
     *
     * @param Builder       $query
     * @param array         $conditions
     * @param boolean       $sort_by_view_order
     * @return collection
     */
    protected function applyEmployeeSearchConditions($query, $conditions, $sort_by_view_order = false)
    {
        if (isset($conditions['employee_presentation_id'])) {
            $query->where('presentation_id', $conditions['employee_presentation_id']);
        }

        if (isset($conditions['employee_name'])) {
            $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
        }

        if (isset($conditions['work_status'])) {
            $query->where('work_status', $conditions['work_status']);
        }

        if (!empty($conditions['departments'])) {
            $query->whereIn('department_id', $conditions['departments']);
        }

        if ($sort_by_view_order) {
            $query->orderBy('view_order');
        }

        return $query->get();
    }

    /**
     * Apply some query conditions for EmployeeWorkingDay model
     * This function is use for searching for working information of Employees who worked at or have plans to work at the current WorkLocation(s)
     *
     * @param Builder       $query
     * @param array         $conditions
     * @param array         $work_location_ids
     * @return collection
     */
    protected function applyEmployeeWorkingDaySearchCondition($query, $conditions, $work_location_ids)
    {
        if (isset($conditions['employee_presentation_id'])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                return $query->where('presentation_id', $conditions['employee_presentation_id']);
            });
        }

        if (isset($conditions['employee_name'])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                return $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
            });
        }

        if (!empty($conditions['departments'])) {
            $query->whereHas('employee', function($query) use ($conditions) {
                return $query->whereIn('department_id', $conditions['departments']);
            });
        }

        // This phrase make the query become so slow, it's unbearable. So instead of doing this, we query with the all the conditions first, and then
        // proceed to filter the result by laravel's collection later
        // $query->whereHas('employeeWorkingInformations', function($query) use ($work_location_ids) {
        //     return $query->whereIn('planned_work_location_id', $work_location_ids)
        //                ->orWhereHas('plannedSchedule', function($query) use ($work_location_ids) {
        //                    $query->whereIn('work_location_id', $work_location_ids);
        //                })
        //                ->orWhereHas('workAddressWorkingEmployee', function($query) use ($work_location_ids) {
        //                    $query->whereHas('plannedSchedule', function($query) use ($work_location_ids) {
        //                        $query->whereIn('work_location_id', $work_location_ids);
        //                    });
        //                })
        //                ->orWhereIn('real_work_location_id', $work_location_ids);
        // });

        // query with all the conditions except the condition 'searching for working information of Employees who worked at or have plans to work at the current WorkLocation(s)'
        $data = $query->get();

        // Now, filter the result with that condition
        $filtered_data = $data->filter(function($working_day) use ($work_location_ids) {
            $working_infos = $working_day->employeeWorkingInformations;

            $has_planned = $working_infos->whereIn('planned_work_location_id', $work_location_ids)->isNotEmpty();
            $or_real = $working_infos->whereIn('real_work_locaiton_id', $work_location_ids)->isNotEmpty();

            return $has_planned || $or_real;
        });

        return $filtered_data;
    }

    /**
     * Since the current work location is used pretty widely, so it's kinda good to create a mechanism to eager load the current work location
     * object-wise, so that we dont have to query the same work location again and again.
     *
     * @return null|WorkLocation
     */
    private function getEagerLoadedCurrentWorkLocation()
    {
        if (!$this->eager_loaded_work_location) {
            $current_work_location_id = session('current_work_location');
            $this->eager_loaded_work_location = is_numeric($current_work_location_id) ? WorkLocation::find($current_work_location_id) : null;
        }
        return $this->eager_loaded_work_location;
    }

    /**
     * The same as above, try to eager load the company if the 'current_work_location' is 'all'.
     *
     * @return null|Company
     */
    private function getEagerLoadedCompany()
    {
        if (!$this->eager_loaded_company) {
            $this->eager_loaded_company = session('current_work_location') === 'all' ? request()->user()->company : null;
        }
        return $this->eager_loaded_company;
    }

    /** Checking having considering request for each employee
     * @param array color_status
     * @return boolean
     */
    private function isHavingConsideringRequest($color_statuses) {
        foreach($color_statuses as $color_status) {
            if ($color_status['color'] == 'bg_apply') return true;
        }
        
        return false;
    }
}

<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Employee;
use App\WorkLocation;
use App\ChecklistItem;
use App\EmployeeWorkingDay;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\EmployeeWorkingInformationSnapshot;
use App\Events\AllEmployeeDataInThisWorkingMonthConcluded;
use App\Events\AllEmployeeDataInThisWorkingMonthUnConcluded;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use App\Http\Controllers\Reusables\UseScheduleTypeIconTrait;
use App\Http\Controllers\Reusables\CustomizedPaginationTrait;
use App\Http\Controllers\Reusables\CanCheckIfWorkingDayCanBeConcluded;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use Carbon\Carbon;
use Caeru;
use Constants;
use Auth;

class MonthSummaryController extends Controller
{
    use BusinessMonthTrait, CustomizedPaginationTrait, CanCheckIfWorkingDayCanBeConcluded, UseScheduleTypeIconTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose:singular');
        $this->middleware('can:view_attendance_month_summary_page');
        $this->middleware('can:conclude_level_two')->only(['concludeAll', 'unconcludeAll']);
    }

    /**
     * Show the summary of a given business month (or the default business month if none given).
     *
     * @param \Illuminate\Http\Request  $request
     * @param string                    $reset_search_conditions
     * @param int                       $page
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $reset_search_conditions = null, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        if ($reset_search_conditions == true) {
            session()->forget('month_summary_search_history');

            // Reset the breadcumbs if reset the search history
            $breadcrumbs_service->resetToAttendanceRoot();

        }

        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::MONTH_SUMMARY, $request->route()->getName(), $request->route());

        $conditions = session('month_summary_search_history') ? session('month_summary_search_history') : $this->getDefaultConditions($request);

        $temp_result = $this->searchMonthSummary($conditions);

        $result = $temp_result['result'];

        $all_employees_data_is_concluded = $temp_result['all_employees_data_is_concluded'];

        $data = $this->paginateData($result, $page);

        // Check if the current business month include future day or not
        $end_date = new Carbon($conditions['end_date']);
        $today = Carbon::today()->hour(23)->minute(59)->second(59);

        // Also send all employee work statuses
        $employee_work_statuses = Constants::workStatuses();

        Javascript::put([
            'employees_data' => $data['data'],
            'current_page' => $data['current_page'],
            'total' => $result->count(),
            'per_page' => $this->ITEMS_PER_PAGE,
            'conditions' => $conditions,
            'employee_names' => $this->getAllEmployeeNames($request),
            'can_conclude' => $end_date->lt($today) && Auth::user()->can('conclude_level_two'),
            'employee_work_statuses' => $employee_work_statuses,
        ]);

        return view('attendance.month_summary.show', [
            'all_concluded' => $all_employees_data_is_concluded,
            'can_conclude' => $end_date->lt($today) && Auth::user()->can('conclude_level_two'),
        ]);
    }

    /**
     * Calculate statistic data of some employees base on the given conditions.
     *
     * @param \Illuminate\Http\Request  $request
     * @param int                       $page
     * @return array|json
     */
    public function search(Request $request, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'reset_search_conditions' => 0 ]);

        $conditions = $request->input('conditions');

        $temp_result = $this->searchMonthSummary($conditions);

        $result = $temp_result['result'];

        // Dont know what to do with this yet.
        $all_employees_data_is_concluded = $temp_result['all_employees_data_is_concluded'];

        $data = $this->paginateData($result, $page);

        // Check if the current business month include future day or not
        $end_date = new Carbon($conditions['end_date']);
        $today = Carbon::today()->hour(23)->minute(59)->second(59);

        return [
            'employees_data' => $data['data'],
            'current_page' => $data['current_page'],
            'total' => $result->count(),
            'per_page' => $this->ITEMS_PER_PAGE,
            'all_concluded' => $all_employees_data_is_concluded,
            'can_conclude' => $end_date->lt($today) && Auth::user()->can('conclude_level_two'),
        ];
    }

    /**
     * Conclude all working data in the given business month of all employees of the current work location.
     *
     * @param \Illuminate\Http\Request  $request
     * @param string                    $business_month
     * @return \Illuminate\Http\Response
     */
    public function concludeAll(Request $request, $business_month)
    {
        $work_location = session('current_work_location');
        if ($work_location === "all") {
            $setting = $request->user()->company->setting;
            $employees = $request->user()->company->employees;
        } else {
            $work_location = WorkLocation::find($work_location);
            $setting = $work_location->currentSetting();
            $employees = $work_location->employees;
        }

        $business_month = ($business_month) ? Carbon::createFromFormat('Y-m-d', $business_month . '-1') : $this->calculateBusinessMonthBaseOnASetting($setting);

        $start_and_end_date = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($setting, $business_month);

        $can_conclude_all = true;
        foreach ($employees as $employee) {
            if ($this->checkIfAWorkingMonthCanBeConcluded($employee, $start_and_end_date[0], $start_and_end_date[1]) != 1) {
                $can_conclude_all = false;
                break;
            }
        }

        if ($can_conclude_all == true) {
            event(new AllEmployeeDataInThisWorkingMonthConcluded($employees, $start_and_end_date[0], $start_and_end_date[1], $request->user()));

            $request->session()->flash('success', '保存しました');

            // If the previous page is month summary then we have to do some small things before return to that page
            $previous_url = url()->previous();
            return $this->redirectToMonthSummaryPageConditionally($previous_url);

        } else {
            $request->session()->flash('error', '全て勤怠データを正確に締められませんでした。');

            // If the previous page is month summary then we have to do some small things before return to that page
            $previous_url = url()->previous();
            return $this->redirectToMonthSummaryPageConditionally($previous_url);
        }
    }

    /**
     * Unconclude all working data in the given business month of all employees of the current work location
     *
     * @param \Illuminate\Http\Request  $request
     * @param string                    $business_month
     * @return \Illuminate\Http\Response
     */
    public function unconcludeAll(Request $request, $business_month)
    {
        $work_location = session('current_work_location');
        if ($work_location === "all") {
            $setting = $request->user()->company->setting;
            $employees = $request->user()->company->employees;
        } else {
            $work_location = WorkLocation::find($work_location);
            $setting = $work_location->currentSetting();
            $employees = $work_location->employees;
        }

        $business_month = ($business_month) ? Carbon::createFromFormat('Y-m-d', $business_month . '-1') : $this->calculateBusinessMonthBaseOnASetting($setting);

        $start_and_end_date = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($setting, $business_month);

        event(new AllEmployeeDataInThisWorkingMonthUnConcluded($employees, $start_and_end_date[0], $start_and_end_date[1]));

        $request->session()->flash('success', '保存しました');

        // If the previous page is month summary then we have to do some small things before return to that page
        $previous_url = url()->previous();
        return $this->redirectToMonthSummaryPageConditionally($previous_url);
    }

    /**
     * The purpose of this function is to turn the paramater 'reset_search_conditions' to false/0 if the previous page is month_summary's page.
     * To be honest, I dont like this way at all. But for quick result, let's just bear with this dirty way for now.
     *
     * @param string    $previous_url
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectToMonthSummaryPageConditionally($previous_url)
    {
        $parts = explode('/', $previous_url);

        $next_to_last_part = $parts[count($parts)-2];
        $last_part = $parts[count($parts)-1];

        if (is_numeric($next_to_last_part)) {
            return Caeru::redirect('show_month_summary', ['reset_search_conditions' => 0, 'page' => $last_part]);
        } else {
            return Caeru::redirect('show_month_summary', ['reset_search_conditions' => false]);
        }
    }


    /**
     * Summary the statistic data for all employees of the current work location and apply the search conditions to filter.
     * This function is just a clean wrapper.
     *
     * @param array         $conditions
     * @return Collection
     */
    protected function searchMonthSummary($conditions)
    {
        $data = $this->summaryOneBusinessMonthStatsForManyEmployees($conditions);

        $result = $data['result'];

        $all_employees_data_is_concluded = $data['all_employees_data_is_concluded'];

        // Save the search conditions history to session
        session(['month_summary_search_history' => $conditions]);

        $result = $this->filterLevelTwo($result, $conditions);

        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        // Set the 2nd dimension of this breadcrumb
        $breadcrumbs_service->setTheSecondDimensionForCurrentNode(collect($result)->pluck('id'), 'employee');

        return [
            'result' => $result,
            'all_employees_data_is_concluded' => $all_employees_data_is_concluded,
        ];
    }

    /**
     * Apply the secondary conditions i.e: only_show_considering, only_show_timestamp_error, etc.
     * At this point, we already have the data set with all the statistic data of all employee. All we need to do is filtering the
     * data set base on the given confitions.
     *
     * @param Collection    $employees
     * @param array         $conditions
     * @return Collection
     */
    protected function filterLevelTwo($employees, $conditions)
    {
        if (isset($conditions['only_show_considering']) && $conditions['only_show_considering'] == true) {
            $employees = $employees->filter(function($employee) {
                return $employee['considering_snapshot_number'] !== 0;
            });
        }

        if (isset($conditions['only_show_timestamp_error']) && $conditions['only_show_timestamp_error'] == true) {
            $employees = $employees->filter(function($employee) {
                return $employee['timestamp_errors_number'] !== 0;
            });
        }

        if (isset($conditions['only_show_confirm_needed_error']) && $conditions['only_show_confirm_needed_error'] == true) {
            $employees = $employees->filter(function($employee) {
                return $employee['confirm_needed_errors_number'] !== 0;
            });
        }

        if (isset($conditions['only_show_month_unconcluded']) && $conditions['only_show_month_unconcluded'] == true) {
            $employees = $employees->filter(function($employee) {
                return $employee['is_all_day_concluded_level_two'] !== true;
            });
        }

        return $employees;
    }

    /**
     * First search employees data base on the first conditions i.e: presentation_id, name.
     * Then proceed to process data. Summary the statistic data of each employee for all working days of a business month.
     * Arrange data and output base on the design of the page month_summary.
     *
     * @param array   $conditions
     * @return Collection
     */
    protected function summaryOneBusinessMonthStatsForManyEmployees($conditions)
    {
        // First search employees data base on the first conditions
        $employees = $this->searchEmployeeWithConditions($conditions);

        $working_days = EmployeeWorkingDay::with([
                                'employeeWorkingInformationSnapshots',
                                'concludedLevelOneManager',
                                'concludedLevelTwoManager',
                            ])
                            ->whereIn('employee_id', $employees->pluck('id')->toArray())
                            ->where('date', '>=', $conditions['start_date'])
                            ->where('date', '<=', $conditions['end_date'])->get();

        $checklist_errors = ChecklistItem::where('date', '>=', $conditions['start_date'])
                                ->where('date', '<=', $conditions['end_date'])
                                ->whereIn('employee_id', $employees->pluck('id')->toArray())->get();

        // Key the items by date#employee_id, effectively turn the collections into hash tables, make it easier to search in the collections
        $working_days = $working_days->keyBy(function($working_day) {
            return $working_day->date . '#' . $working_day->employee_id;
        });
        $checklist_errors = $checklist_errors->groupBy(function($error) {
            return $error->date . '#' . $error->employee_id;
        });

        $result = collect([]);
        // This variable is to determine if all employees's data in the given business month of the current work location is concluded(level two)
        // Agree, this logic is completely out of scope for this function, but it's convenient to put it here. And also it reduces a lot of load.
        $all_employees_data_is_concluded = true;

        foreach ($employees as $employee) {
            $current_snapshot_number = 0;
            $current_considering_snapshot_number = 0;
            $current_approved_snapshot_number = 0;
            $current_timestamp_errors_number = 0;
            $current_confirm_needed_errors_number = 0;
            $is_all_day_concluded_level_one = true;
            $is_all_day_concluded_level_two = true;
            $current_concluded_level_two_manager_name = null;

            $start_date = new Carbon($conditions['start_date']);
            $end_date = new Carbon($conditions['end_date']);

            while ($start_date->lte($end_date)) {
                $unique_key = $start_date->format('Y-m-d') . '#' . $employee->id;
                $working_day = isset($working_days[$unique_key]) ? $working_days[$unique_key] : null;

                if ($working_day) {
                     // Count the stats relate to snapshots
                    $snapshots_of_current_day = $working_day->employeeWorkingInformationSnapshots;
                    $current_snapshot_number += $snapshots_of_current_day->count();
                    $current_considering_snapshot_number += $snapshots_of_current_day->filter(function($snapshot) {
                        return $snapshot->left_status == EmployeeWorkingInformationSnapshot::CONSIDERING;
                    })->count();
                    $current_approved_snapshot_number += $snapshots_of_current_day->filter(function($snapshot) {
                        return $snapshot->left_status == EmployeeWorkingInformationSnapshot::APPROVED;
                    })->count();

                    // Count the stats relate to checklist errors
                    $checklist_items_of_current_day = isset($checklist_errors[$unique_key]) ? $checklist_errors[$unique_key] : collect([]);
                    $current_timestamp_errors_number += $checklist_items_of_current_day->filter(function($error) {
                        return $error->item_type == ChecklistItem::TIMESTAMP_ERROR;
                    })->count();
                    $current_confirm_needed_errors_number += $checklist_items_of_current_day->filter(function($error) {
                        return $error->item_type == ChecklistItem::CONFIRM_NEEDED;
                    })->count();

                    // Relate to concluding status
                    $is_all_day_concluded_level_one = $working_day->concluded_level_one ? $is_all_day_concluded_level_one : false;
                    $is_all_day_concluded_level_two = $working_day->concluded_level_two ? $is_all_day_concluded_level_two : false;
                    $all_employees_data_is_concluded = $working_day->concluded_level_two ? $all_employees_data_is_concluded : false;
                    $current_concluded_level_two_manager_name = $is_all_day_concluded_level_two ? $working_day->getConcludedLevelTwoManagerName() : null;

                } else {
                    $is_all_day_concluded_level_one = false;
                    $is_all_day_concluded_level_two = false;
                    $all_employees_data_is_concluded = false;
                    $current_concluded_level_two_manager_name = null;
                }

                $start_date->addDay();
            }

            $result->push([
                'id' => $employee->id,
                'schedule_type_icon' => $this->getScheduleTypeIcon($employee->schedule_type),
                'presentation_id' => $employee->presentation_id,
                'name' => $employee->fullName(),
                'snapshot_number' => $current_snapshot_number,
                'considering_snapshot_number' => $current_considering_snapshot_number,
                'approved_snapshot_number' => $current_approved_snapshot_number,
                'timestamp_errors_number' => $current_timestamp_errors_number,
                'confirm_needed_errors_number' => $current_confirm_needed_errors_number,
                'is_all_day_concluded_level_one' => $is_all_day_concluded_level_one,
                'is_all_day_concluded_level_two' => $is_all_day_concluded_level_two,
                'concluded_level_two_manager_name' => $current_concluded_level_two_manager_name,
            ]);
        }

        return [
            'result' => $result,
            'all_employees_data_is_concluded' => $all_employees_data_is_concluded,
        ];
    }

    /**
     * Search Employees of the current work location base on some given conditions.
     *
     * @param array     $conditions
     * @return Collection
     */
    protected function searchEmployeeWithConditions($conditions)
    {
        $query = Employee::query();

        $work_location = session('current_work_location');

        if (is_numeric($work_location)) {
            $query = $query->where('work_location_id', $work_location)->orderBy('view_order');
        }

        if (isset($conditions['presentation_id'])) {
            $query = $query->where('presentation_id', '=', $conditions['presentation_id']);
        }

        if (isset($conditions['name'])) {
            $query = $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["name"] . '%');
        }

        if (isset($conditions['work_status'])) {
            $query = $query->where('work_status', '=', $conditions['work_status']);
        }

        return $query->get();
    }

    /**
     * Get the default conditions for the search box in month summary page.
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getDefaultConditions(Request $request)
    {
        $work_location = session('current_work_location');
        if ($work_location === "all") {
            $setting = $request->user()->company->setting;
        } else {
            $work_location = WorkLocation::find($work_location);
            $setting = $work_location->currentSetting();
        }

        $business_month = $this->calculateBusinessMonthBaseOnASetting($setting);

        $start_and_end_date = $this->getStartDateAndEndDateFromBusinessMonthBaseOnASetting($setting, $business_month);

        return [
            'year' => $business_month->year,
            'month' => $business_month->month,
            'start_date' => $start_and_end_date[0]->format('Y-m-d'),
            'end_date' => $start_and_end_date[1]->format('Y-m-d'),
            'presentation_id' => null,
            'name' => null,
            'work_status' => config('constants.working'),
            'only_show_considering' => false,
            'only_show_timestamp_error' => false,
            'only_show_confirm_needed_error' => false,
            'only_show_month_unconcluded' => false,
            'salary_accounting_day' => $setting->salary_accounting_day,
        ];
    }

    /**
     * Send name list of all employees of this company to the javascript side for the autocomplete feature in the search box
     *
     * @param Request       $request
     * @return void
     */
    private function getAllEmployeeNames(Request $request)
    {
        return $request->user()->company->employees->map(function($employee) {
            return ['name' => $employee->last_name . $employee->first_name];
        });
    }
}

<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Routing\Controller;
use App\EmployeeWorkingInformation;
use App\ColorStatus;
use App\RestStatus;
use App\Http\Requests\EmployeeWorkingInformationRequest;
use App\Http\Controllers\Reusables\UseTheEmployeeWorkingInfoVueComponentTrait;
use App\Http\Controllers\Reusables\ChangeBackGroundColorOfAttributesTrait;
use App\Events\EmployeeWorkingInformationWorkStatusChanged;
use App\Events\EmployeeWorkingInformationRestStatusChanged;
use App\Events\AbsorbTheMatchingTemporaryWorkingInformation;
use App\Events\WorkingTimestampChanged;
use App\Events\ResetWorkStatusColor;
use App\Events\ResetRestStatusColor;

class EmployeeWorkingInformationController extends Controller
{
    use UseTheEmployeeWorkingInfoVueComponentTrait, ChangeBackGroundColorOfAttributesTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:change_attendance_data');
    }

    /**
     * Store the new working information instance
     *
     * @param EmployeeWorkingInformationRequest      $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeWorkingInformationRequest $request)
    {
        $info = new EmployeeWorkingInformation($request->only([
            'schedule_start_work_time',
            'schedule_end_work_time',
            'schedule_break_time',
            'schedule_night_break_time',
            'schedule_working_hour',
            'manually_inputed_schedule_start_work_time',
            'manually_inputed_schedule_end_work_time',
            'manually_inputed_schedule_break_time',
            'manually_inputed_schedule_night_break_time',
            'manually_inputed_schedule_working_hour',

            'planned_work_location_id',
            'planned_work_status_id',
            'planned_rest_status_id',
            'paid_rest_time_start',
            'paid_rest_time_end',
            'paid_rest_time_period',
            'not_include_break_time_when_display_planned_time',
            'note',
            'planned_early_arrive_start',
            'planned_early_arrive_end',
            'planned_late_time',
            'planned_break_time',
            'real_break_time',
            'planned_night_break_time',
            'real_night_break_time',
            'planned_go_out_time',
            'planned_early_leave_time',
            'planned_overtime_start',
            'planned_overtime_end',

            'work_time',

            'basic_salary',
            'night_salary',
            'overtime_salary',
            'deduction_salary',
            'night_deduction_salary',
            'monthly_traffic_expense',
            'daily_traffic_expense',
        ]));

        // Set the last modified person name
        $info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
        $info->last_modify_person_id = $request->user()->id;

        // All EmployeeWorkingInformation instance created by this way are considered 'manually_modified' AND 'schedule_modified'
        $info->manually_modified = true;
        $info->schedule_modified = true;

        // Set the relationship with the correct EmployeeWorkingDay
        $info->employeeWorkingDay()->associate($request->input('employee_working_day_id'));

        $info->save();

        event(new WorkingTimestampChanged($info->employeeWorkingDay));

        // Reload model
        $info = $info->fresh();

        return [
            'new_data'  => $info->necessaryDataForTheVueComponent(),
            'schedule_transfer_data' => $this->getScheduleTransferData([$info], $info->employeeWorkingDay->employee_id),
            // 'alert_setting_data' => $this->extractDataFromWorkLocationSetting([$info]),
            'success' => '保存しました',
        ];
    }

    /**
     * Update an EmployeeWorkingInformation instance
     *
     * @param EmployeeWorkingInformationRequest         $request
     * @param EmployeeWorkingInformation                $employee_working_info
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeWorkingInformationRequest $request, EmployeeWorkingInformation $employee_working_info)
    {
        $this->checkAttributesToCreateColorStatus($request, $employee_working_info);

        // Check if planned_rest/work_status_id has been changed or not to fire events.
        if ($employee_working_info->planned_rest_status_id !== $request->input('planned_rest_status_id')) {
            event(new EmployeeWorkingInformationRestStatusChanged($employee_working_info, $request->input('planned_rest_status_id')));
        }
        if ($employee_working_info->planned_work_status_id !== $request->input('planned_work_status_id')) {
            event(new EmployeeWorkingInformationWorkStatusChanged($employee_working_info, $request->input('planned_work_status_id')));
        }

        $employee_working_info->fill($request->only([
            'planned_work_status_id',
            'planned_rest_status_id',
            'paid_rest_time_start',
            'paid_rest_time_end',
            'paid_rest_time_period',
            'not_include_break_time_when_display_planned_time',
            'note',
            'planned_early_arrive_start',
            'planned_early_arrive_end',
            'planned_late_time',
            'planned_go_out_time',
            'planned_early_leave_time',
            'planned_overtime_start',
            'planned_overtime_end',
            'work_time',
        ]));

        // For these 4 attributes, we only save when there is change
        if ($request->input('planned_break_time') !== null) {
            $employee_working_info->planned_break_time = $request->input('planned_break_time');

            // If these attributes get changed, the associated WorkAddressWorkingEmployee's schedule_modified flag will be turn on
            if ($employee_working_info->planned_work_address_id && $employee_working_info->workAddressWorkingEmployee) {
                $employee_working_info->workAddressWorkingEmployee->schedule_modified = true;
                $employee_working_info->workAddressWorkingEmployee->save();
            }
        }
        if ($request->input('planned_night_break_time') !== null) {
            $employee_working_info->planned_night_break_time = $request->input('planned_night_break_time');

            // If these attributes get changed, the associated WorkAddressWorkingEmployee's schedule_modified flag will be turn on
            if ($employee_working_info->planned_work_address_id && $employee_working_info->workAddressWorkingEmployee) {
                $employee_working_info->workAddressWorkingEmployee->schedule_modified = true;
                $employee_working_info->workAddressWorkingEmployee->save();
            }
        }
        if ($request->input('real_break_time') !== null) {
            $employee_working_info->real_break_time = $request->input('real_break_time');
        }
        if ($request->input('real_night_break_time') !== null) {
            $employee_working_info->real_night_break_time = $request->input('real_night_break_time');
        }

        // If the user manually modified something in the schedule at the front end, then we save all those values and turn the manually_modified flag ON
        if ($request->input('schedule_modified') === true) {

            $employee_working_info->fill($request->only([
                'schedule_start_work_time',
                'schedule_end_work_time',
                'schedule_break_time',
                'schedule_night_break_time',
                'schedule_working_hour',
                'manually_inputed_schedule_start_work_time',
                'manually_inputed_schedule_end_work_time',
                'manually_inputed_schedule_break_time',
                'manually_inputed_schedule_night_break_time',
                'manually_inputed_schedule_working_hour',
                'planned_work_location_id',
            ]));

            $employee_working_info->schedule_modified = true;
        }

        // If the user manually modified something in the salaries attributes at the front end, then we save all those values
        if ($request->input('salaries_modified') === true) {

            $employee_working_info->fill($request->only([
                'basic_salary',
                'night_salary',
                'overtime_salary',
                'deduction_salary',
                'night_deduction_salary',
                'monthly_traffic_expense',
                'daily_traffic_expense',
            ]));
        }

        // Set the last modified person name
        $employee_working_info->last_modify_person_type = EmployeeWorkingInformation::MODIFY_PERSON_TYPE_MANAGER;
        $employee_working_info->last_modify_person_id = $request->user()->id;

        $employee_working_info->manually_modified = true;
        $employee_working_info->temporary = false;

        // If there is a temporary employee_working_information with the same planned_work_location with the new planned_work_location of this instance
        // Absorb it, then refresh the page
        event(new AbsorbTheMatchingTemporaryWorkingInformation($employee_working_info));

        $employee_working_info->save();

        $employee_working_info->load('employeeWorkingDay');

        event(new WorkingTimestampChanged($employee_working_info->employeeWorkingDay));

        return [
            'new_data'  => $employee_working_info->necessaryDataForTheVueComponent(),
            'schedule_transfer_data' => $this->getScheduleTransferData([$employee_working_info], $employee_working_info->employeeWorkingDay->employee_id),
            // 'alert_setting_data' => $this->extractDataFromWorkLocationSetting([$employee_working_info]),
            'success' => '保存しました',
        ];
    }

    /**
     * Destroy an EmployeeWorkingInformation instance
     *
     * @param EmployeeWorkingInformation        $employee_working_info
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeWorkingInformation $employee_working_info)
    {
        $employee_working_info->delete();

        return [
            'success' => '削除しました',
        ];
    }


    /**
     * If there are any change with these attributes (it means they have been changed), create color status for those attributes. Some of the 'real_' attribute's changes can not
     * be detect here. Those attribute will be checked later in the model's updated event.
     *
     * @param EmployeeWorkingInformationRequest     $request
     * @param EmployeeWorkingInformation            $working_info
     * @return void
     */
    protected function checkAttributesToCreateColorStatus(EmployeeWorkingInformationRequest $request, EmployeeWorkingInformation $working_info)
    {
        if ($request->input('planned_work_location_id') !== $working_info->planned_work_location_id) {
            $this->createColorStatus($working_info->id, 'planned_work_location_id');
            $this->changeColorOfSnapshot($working_info, 'planned_work_location_id');
        }

        if ($request->input('planned_break_time') !== null ) {
            $this->createColorStatus($working_info->id, 'planned_break_time');
            $this->changeColorOfSnapshot($working_info, 'planned_break_time');
        }

        if ($request->input('real_break_time') !== null ) {
            $this->createColorStatus($working_info->id, 'real_break_time');
            $this->changeColorOfSnapshot($working_info, 'real_break_time');
        }

        if ($request->input('planned_night_break_time') !== null ) {
            $this->createColorStatus($working_info->id, 'planned_night_break_time');
            $this->changeColorOfSnapshot($working_info, 'planned_night_break_time');
        }

        if ($request->input('real_night_break_time') !== null ) {
            $this->createColorStatus($working_info->id, 'real_night_break_time');
            $this->changeColorOfSnapshot($working_info, 'real_night_break_time');
        }

        if ($request->input('planned_go_out_time') !== $working_info->planned_go_out_time ) {
            $this->createColorStatus($working_info->id, 'planned_go_out_time');
            $this->changeColorOfSnapshot($working_info, 'planned_go_out_time');
        }

        if ($request->input('planned_late_time') !== $working_info->planned_late_time || $request->input('planned_early_leave_time') !== $working_info->planned_early_leave_time) {
            $this->createColorStatus($working_info->id, 'planned_total_late_and_leave_early');

            if ($request->input('planned_late_time') !== $working_info->planned_late_time) {
                $this->changeColorOfSnapshot($working_info, 'planned_late_time');
            }

            if ($request->input('planned_early_leave_time') !== $working_info->planned_early_leave_time) {
                $this->changeColorOfSnapshot($working_info, 'planned_early_leave_time');
            }
        }

        if ($request->input('planned_early_arrive_start') !== $working_info->planned_early_arrive_start || $request->input('planned_early_arrive_end') !== $working_info->planned_early_arrive_end ||
            $request->input('planned_overtime_start') !== $working_info->planned_overtime_start || $request->input('planned_overtime_end') !== $working_info->planned_overtime_end) {
            $this->createColorStatus($working_info->id, 'planned_total_early_arrive_and_overtime');

            // These things below are for snapshots
            if ($request->input('planned_early_arrive_start') !== $working_info->planned_early_arrive_start) {
                $this->changeColorOfSnapshot($working_info, 'planned_early_arrive_start');
            }

            if ($request->input('planned_early_arrive_end') !== $working_info->planned_early_arrive_end) {
                $this->changeColorOfSnapshot($working_info, 'planned_early_arrive_end');
            }

            if ($request->input('planned_overtime_start') !== $working_info->planned_overtime_start) {
                $this->changeColorOfSnapshot($working_info, 'planned_overtime_start');
            }

            if ($request->input('planned_overtime_end') !== $working_info->planned_overtime_end) {
                $this->changeColorOfSnapshot($working_info, 'planned_overtime_end');
            }
        }

        // 2018-07-09: When manager change these fields in the form, remove any color statuses made from the sinsei's side
        if ($request->input('planned_work_status_id') !== $working_info->planned_work_status_id) {
            event(new ResetWorkStatusColor($working_info));
        }

        if (($request->input('planned_rest_status_id') !== $working_info->planned_rest_status_id) ||
           ($request->input('paid_rest_time_start') != $working_info->paid_rest_time_start) ||
           ($request->input('paid_rest_time_end') != $working_info->paid_rest_time_end)) {
            event(new ResetRestStatusColor($working_info));
        }

    }

}

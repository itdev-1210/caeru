<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\WorkLocation;
use App\WorkingTimestamp;
use App\EmployeeWorkingDay;
use Caeru;
use Carbon\Carbon;
use App\Events\WorkingTimestampChanged;
use App\Http\Controllers\Reusables\ManageWorkingTimestampsTrait;

class TabletWorkingTimestampController extends Controller
{
    use ManageWorkingTimestampsTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create a WorkingTimestamp with tablet type for testing
     *
     * @param \Illuminate\Http\Request       $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, EmployeeWorkingDay $working_day)
    {
        $timestamp_types = $this->getTimestampTypes();



        return view('attendance.tablet.form', [
            'timestamp_types' => $timestamp_types,
            'working_day'  => $working_day,
        ]);
    }

    /**
     * Store a WorkingTimestamp with tablet type for testing
     *
     * @param \Illuminate\Http\Request       $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $working_day = EmployeeWorkingDay::where('employee_id', $request->input('employee_id'))->where('date', $request->input('date'))->first();

        if ($working_day) {

            $timestamp = new Carbon($request->input('timestamp'));

            $work_location = WorkLocation::find($request->input('work_location_id'));

            if ($timestamp && $work_location) {
                
                $working_timestamp = new WorkingTimestamp([
                    'enable' => true,
                    'created_at' => $timestamp->format('Y-m-d h:m:s'),
                    'timestamped_value' => $timestamp->timestamp,
                    'work_location_id' => $request->input('work_location_id'),
                    'registerer_type' => WorkingTimestamp::TABLET,
                    'timestamped_type' => intval($request->input('timestamped_type')),
                ]);

                $this->addNewWorkingTimestampsForWorkingDay([$working_timestamp], $working_day);

                return Caeru::redirect('tablet_list_timestamp', [
                    'employee_id' => $request->input('employee_id'),
                    'date' => $request->input('date'),
                ]);

            } else {
                $request->session()->flash('error', 'timestamp、又は勤務地が違いました！');
                return back();
            }

        } else {
            $request->session()->flash('error', '従業員idと日付が違いました！');
            return back();
        }
    }

    /**
     * Show an EmployeeWorkingDay's list of WorkingTimestamps
     *
     * @param \Illuminate\Http\Request   $request        the request instance
     * @param int                       $employee_id    the id of the employee
     * @param string                    $date           the working day
     */
    public function list(Request $request, $employee_id, $date)
    {
        $working_day = EmployeeWorkingDay::where('employee_id', $employee_id)->where('date', $date)->first();

        if ($working_day) {
            return view('attendance.tablet.timestamps', [
                'timestamps' => $working_day->workingTimestamps,
                'timestamp_types' => $this->getTimestampTypes(),
                'working_day'  => $working_day,
            ]);

        } else {
            $request->session()->flash('error', '従業員idと日付が違いました！');
            return back();
        }
    }

    /**
     * Delete a WorkingTimestamp
     *
     * @param \Illuminate\Http\Request   $request               the request instance
     * @param WorkingTimestasmp          $working_timestamp     the WorkingTimestamp
     */
    public function delete(Request $request, WorkingTimestamp $working_timestamp)
    {
        $working_timestamp->enable = false;
        $working_timestamp->save();

        event(new WorkingTimestampChanged($working_timestamp->employeeWorkingDay));

        $working_timestamp->delete();

        return back();
    }

    /**
     * Array of WorkingTimestamp types
     *
     * @return array
     */
    protected function getTimestampTypes()
    {
        return [
            WorkingTimestamp::START_WORK => "出勤",
            WorkingTimestamp::END_WORK => "退勤",
            WorkingTimestamp::GO_OUT => "外出",
            WorkingTimestamp::RETURN => "戻り",
        ];
    }
}

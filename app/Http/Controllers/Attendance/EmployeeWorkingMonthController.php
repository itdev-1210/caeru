<?php

namespace App\Http\Controllers\Attendance;

use mysql_xdevapi\Exception;
use Session;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Employee;
use App\CalendarRestDay;
use App\EmployeeWorkingDay;
use App\ChecklistItem;
use App\ChecklistErrorTimer;
use App\WorkingTimestamp;
use App\Setting;
use Carbon\Carbon;
use App\Events\EmployeeWorkingDayConcluded;
use App\Events\ManyEmployeeWorkingDaysConcluded;
use App\Events\ManyEmployeeWorkingDaysUnconcluded;
use App\Events\EmployeeWorkingDayUnconcluded;
use App\Events\EmployeeWorkingMonthConcluded;
use App\Events\EmployeeWorkingMonthUnconcluded;
use App\Http\Controllers\Reusables\BusinessMonthTrait;
use App\Http\Controllers\Reusables\CanCreateWorkingDayOnTheFlyTrait;
use App\Http\Controllers\Reusables\UseTheSnapshotVueComponentTrait;
use App\Http\Controllers\Reusables\CanCheckIfWorkingDayCanBeConcluded;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Services\WorkLocationSettingService;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use Caeru;
use Auth;
use App\WorkLocation;

class EmployeeWorkingMonthController extends Controller
{
    use BusinessMonthTrait, CanCreateWorkingDayOnTheFlyTrait, CanCheckIfWorkingDayCanBeConcluded, UseTheSnapshotVueComponentTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:view_attendance_employee_working_month_page')->only(['list, preview']);
        $this->middleware('can:conclude_level_one')->only(['concludeLevelOne', 'unconcludedLevelOne', 'concludeNonExistWorkingDay', 'concludeLevelOneOnAllDays', 'unConcludedLevelOneOnAllDays']);
        $this->middleware('can:conclude_level_two')->only(['concludeLevelTwo', 'unconcludedLevelTwo']);
    }

    /**
     * Show all working days of an emplopyee of a business month
     *
     * @param \Illuminate\Http\Request  $request            the request instance
     * @param Employee                  $employee           the employee
     * @param string                    $business_month     the business month
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, Employee $employee, $business_month = null)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::WORKING_MONTH, $request->route()->getName(), $request->route());

        if ($business_month) {
            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');

        } else {
            $business_month = $this->calculateTheBusinessMonth($employee);
        }
        $data = $this->getWorkingDataFromBusinessMonth($employee, $business_month);

        $latest_paid_holiday_info = $employee->paidHolidayInformations->sortBy('period_end')->last();

        $current_work_location = session('current_work_location');
        $company = Auth::user()->company;
        if ($current_work_location === 'all') {
            $setting = $company->setting;

        } elseif (is_array($current_work_location)) {
            $setting = $company->setting;
        } else {
            $work_location = WorkLocation::find($current_work_location);
            $setting = $work_location->currentSetting();
        }

        Javascript::put([
            'business_month'  => $business_month->format('Y-m'),
            'employee_id' => $employee->id,
            'working_days' => $data['working_days'],
            'consts_sunday' => Carbon::SUNDAY,
            'consts_saturday' => Carbon::SATURDAY,
            'work_locations' => $this->prepareTheWorkPlacesData($request),
            'everyday_can_be_concluded' => $data['everyday_can_be_concluded'],
            'level_one_concluded_all' => $data['level_one_concluded_all'],
            'level_two_concluded' => $data['level_two_concluded'],
            'level_two_concluded_manager_name' => $data['level_two_concluded_anchor_manager_name'],
            'can_go_to_working_day_page' => $request->user()->can('view_attendance_employee_working_day'),
            'manager_can_conclude_level_one' => Auth::user()->can('conclude_level_one'),
            'default_work_location_id' => is_numeric(session('current_work_location')) ? session('current_work_location') : $employee->work_location_id,

            // Prepare sinsei data for the whole business month
            'sinsei_data' => $this->prepareSinseiDataForAWholeMonth($employee, $business_month),
            'display_toggle' => Session::has('display_toggle') ? session('display_toggle') :$setting->employee_working_month_pages_display_toggle,
        ]);

        // Send some data for presentational purpose to javascript side
        $this->sendPresentationalDataOfEmployeeWorkingMonthJsComponent($request);

        return view('attendance.employee.working_month', [
            'employee' => $employee,
            'business_month' => $business_month->format('Y-m'),
            'summarized_data' => $data['summarized_data'],
            'real_available_paid_holidays' => $latest_paid_holiday_info ? $latest_paid_holiday_info->available_paid_holidays : null,
            'real_available_paid_holidays_hour' => $latest_paid_holiday_info ? $latest_paid_holiday_info->available_paid_holidays_hour : null,
            'planned_available_paid_holidays' => $latest_paid_holiday_info ? $latest_paid_holiday_info->plannedAvailablePaidHolidays()['days'] : null,
            'planned_available_paid_holidays_hour' => $latest_paid_holiday_info ? $latest_paid_holiday_info->plannedAvailablePaidHolidays()['time'] : null,
            'display_toggle' => Session::has('display_toggle') ? session('display_toggle') :$setting->employee_working_month_pages_display_toggle,
        ]);
    }

    /**
     * Show preview all working days of an emplopyee of a business month
     *
     * @param \Illuminate\Http\Request  $request            the request instance
     * @param Employee                  $employee           the employee
     * @param string                    $business_month     the business month
     * @return \Illuminate\Http\Response
     */
    public function preview(Request $request, Employee $employee, $business_month = null, $type)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::EMPLOYEE_PRINT, $request->route()->getName(), $request->route());

        if ($business_month) {
            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');
        } else {
            $business_month = $this->calculateTheBusinessMonth($employee);
        }

        $data = $this->getWorkingDataFromBusinessMonth($employee, $business_month);

        $latest_paid_holiday_info = $employee->paidHolidayInformations->sortBy('period_end')->last();

        Javascript::put([
            'business_month'  => $business_month->format('Y-m'),
            'employee_id' => $employee->id,
            'working_days' => $data['working_days'],
            'displayWorkPlace' => $type == "1" ? true : false,
            'consts_sunday' => Carbon::SUNDAY,
            'consts_saturday' => Carbon::SATURDAY,
            'work_locations' => $this->prepareTheWorkPlacesData($request),

            // Prepare sinsei data for the whole business month
            'sinsei_data' => $this->prepareSinseiDataForAWholeMonth($employee, $business_month),
        ]);

        // Send some data for presentational purpose to javascript side
        $this->sendPresentationalDataOfEmployeeWorkingMonthJsComponent($request);

        return view('attendance.employee.preview_print', [
            'employee' => $employee,
            'business_month' => $business_month->format('Y-m'),
            'summarized_data' => $data['summarized_data'],
            'real_available_paid_holidays' => $latest_paid_holiday_info ? $latest_paid_holiday_info->available_paid_holidays : null,
            'real_available_paid_holidays_hour' => $latest_paid_holiday_info ? $latest_paid_holiday_info->available_paid_holidays_hour : null,
            'planned_available_paid_holidays' => $latest_paid_holiday_info ? $latest_paid_holiday_info->plannedAvailablePaidHolidays()['days'] : null,
            'planned_available_paid_holidays_hour' => $latest_paid_holiday_info ? $latest_paid_holiday_info->plannedAvailablePaidHolidays()['time'] : null,
        ]);
    }

    /**
     * Mark this instance of EmployeeWorkingDay as concluded level one. And export summary data (if any).
     *
     * @param \Illuminate\Http\Request      $request
     * @param EmployeeWorkingDay            $employee_working_day
     * @param string                        $business_month
     * @return Array|JSON
     */
    public function concludeLevelOne(Request $request, EmployeeWorkingDay $employee_working_day, $business_month)
    {
        $check = $this->checkIfAWorkingDayCanBeConcluded($employee_working_day);
        if ($check === 1) {

            event(new EmployeeWorkingDayConcluded($employee_working_day, $request->user()));

            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');

            $data = $this->getWorkingDataFromBusinessMonth($employee_working_day->employee, $business_month);

            return [
                'success' => '保存しました',
                'working_days' => $data['working_days'],
                'everyday_can_be_concluded' => $data['everyday_can_be_concluded'],
                'level_one_concluded_all' => $data['level_one_concluded_all'],
            ];

        } else if ($check === 0) {
            return response()->json(['error' => '打刻エラーがあるため、締める事ができません。'], 401);
        } else if ($check === -1) {
            return response()->json(['error' => '管理者２がすでに、締めました！'], 401);
        } else {
            return response()->json(['error' => '申請中は締める事ができません。'], 401);
        }
    }


    /**
     * Mark this instance of EmployeeWorkingDay as NOT concluded level one.
     *
     * @param \Illuminate\Http\Request      $request
     * @param EmployeeWorkingDay            $employee_working_day
     * @param string                        $business_month
     * @return Array|JSON
     */
    public function unconcludedLevelOne(Request $request, EmployeeWorkingDay $employee_working_day, $business_month)
    {
        if ($this->checkIfAWorkingDayCanBeUnconcluded($employee_working_day) == true) {
            event(new EmployeeWorkingDayUnconcluded($employee_working_day));

            $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');

            $data = $this->getWorkingDataFromBusinessMonth($employee_working_day->employee, $business_month);

            return [
                'success' => '保存しました',
                'working_days' => $data['working_days'],
                'everyday_can_be_concluded' => $data['everyday_can_be_concluded'],
                'level_one_concluded_all' => $data['level_one_concluded_all'],
                'level_two_concluded' => $data['level_two_concluded'],
                'level_two_concluded_manager_name' => $data['level_two_concluded_anchor_manager_name'],
            ];
        } else {
            return response()->json(['error' => '管理者２がすでに、締めました！'], 401);
        }
    }


    /**
     * Conclude on a non-exist working day. In this function we receive a date and an employee_id instead of a employee_working_day_id.
     * Create a new EmployeeWorkingDay instance and process like normal.
     *
     * @param \Illuminate\Http\Request      $request
     * @param integer                       $employee_id
     * @param string                        $date
     * @param string                        $business_month
     * @return \Illuminate\Http\Response
     */
    public function concludeNonExistWorkingDay(Request $request, $employee_id, $date, $business_month)
    {
        // Try to find that working day first
        $employee_working_day = EmployeeWorkingDay::where('employee_id', $employee_id)->where('date', '=', $date)->first();

        // If there's really none, create a new one.
        if (!$employee_working_day) {
            $employee_working_day = $this->createEmployeeWorkingDayOnTheFly($employee_id, $date);
        }

        return $this->concludeLevelOne($request, $employee_working_day, $business_month);
    }

    /**
     * Conclude level one all working days of a given business month
     *
     * @param \Illuminate\Http\Request      $request
     * @param integer                       $employee_id
     * @param string                        $business_month
     */
    public function concludeLevelOneOnAllDays(Request $request, Employee $employee, $business_month)
    {
        $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');
        $range = $this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);

        $check = $this->checkIfAWorkingMonthCanBeConcludedLevelOne($employee, $range[0], $range[1]);
        if ($check === 1) {

            event(new ManyEmployeeWorkingDaysConcluded($employee, $range[0], $range[1], $request->user()));

            $request->session()->flash('success', '保存しました');
            return back();
        } else if ($check === 0) {
            $request->session()->flash('error', '打刻エラーがあるため、締める事ができません。');
            return back();
        } else if ($check === -1) {
            $request->session()->flash('error', '管理者２がすでに、締めました！');
            return back();
        } else {
            $request->session()->flash('error', '申請中は締める事ができません。');
            return back();
        }
    }

    /**
     * Unconcluded level one all working days of a given business month
     *
     * @param \Illuminate\Http\Request      $request
     * @param integer                       $employee_id
     * @param string                        $business_month
     */
    public function unConcludedLevelOneOnAllDays(Request $request, Employee $employee, $business_month)
    {
        $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');
        $range = $this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);

        if ($this->checkIfAWorkingMonthCanBeUnconcludedLevelOne($employee, $range[0], $range[1])) {
            event(new ManyEmployeeWorkingDaysUnconcluded($employee, $range[0], $range[1]));

            $request->session()->flash('success', '保存しました');
            return back();

        } else {
            $request->session()->flash('error', '管理者２がすでに、締めました！');
            return back();
        }
    }


    /**
     * Mark EmployeeWorkingDay instances of this working month as concluded level two. And export summary data.
     *
     * @param \Illuminate\Http\Request  $request
     * @param Employee                  $employee
     * @param string                    $business_month
     * @return \Illuminate\Http\Response
     */
    public function concludeLevelTwo(Request $request, Employee $employee, $business_month)
    {
        $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');
        $range = $this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);

        $check = $this->checkIfAWorkingMonthCanBeConcluded($employee, $range[0], $range[1]);
        if ($check == 1) {

            event(new EmployeeWorkingMonthConcluded($employee, $range[0], $range[1], $request->user()));

            $request->session()->flash('success', '保存しました');

            // If the previous page is month summary then we have to do some small things before return to that page
            $previous_url = url()->previous();
            if (strpos($previous_url, 'month_summary') !== false) {
                return $this->redirectToMonthSummaryPageConditionally($previous_url);
            }
            return back();

        } else if ($check == 0) {
            $request->session()->flash('error', '打刻エラーがあるため、締める事ができません。');

            // If the previous page is month summary then we have to do some small things before return to that page
            $previous_url = url()->previous();
            if (strpos($previous_url, 'month_summary') !== false) {
                return $this->redirectToMonthSummaryPageConditionally($previous_url);
            }
            return back();
        } else {
            $request->session()->flash('error', '申請中は締める事ができません。');

            // If the previous page is month summary then we have to do some small things before return to that page
            $previous_url = url()->previous();
            if (strpos($previous_url, 'month_summary') !== false) {
                return $this->redirectToMonthSummaryPageConditionally($previous_url);
            }
            return back();
        }
    }

    /**
     * Unmark EmployeeWorkingDay instances of this given working month, (both level one and two) and delete the 'concluded_' data.
     *
     * @param \Illuminate\Http\Request  $request
     * @param Employee                  $employee
     * @param string                    $business_month
     * @return \Illuminate\Http\Response
     */
    public function unconcludedLevelTwo(Request $request, Employee $employee, $business_month)
    {
        $business_month = Carbon::createFromFormat('Y-m-d', $business_month . '-1');

        $range = $this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);

        event(new EmployeeWorkingMonthUnconcluded($employee, $range[0], $range[1]));

        $request->session()->flash('success', '保存しました');

        // If the previous page is month summary then we have to do some small things before return to that page
        $previous_url = url()->previous();
        if (strpos($previous_url, 'month_summary') !== false) {
            return $this->redirectToMonthSummaryPageConditionally($previous_url);
        }
        return back();
    }

    /**
     * The purpose of this function is to turn the paramater 'reset_search_conditions' to false/0 if the previous page is month_summary's page.
     * To be honest, I dont like this way at all. But for quick result, let's just bear with this dirty way for now.
     *
     * @param string    $previous_url
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectToMonthSummaryPageConditionally($previous_url)
    {
        $parts = explode('/', $previous_url);

        $next_to_last_part = $parts[count($parts)-2];
        $last_part = $parts[count($parts)-1];

        if (is_numeric($next_to_last_part)) {
            return Caeru::redirect('show_month_summary', ['reset_search_conditions' => 0, 'page' => $last_part]);
        } else {
            return Caeru::redirect('show_month_summary', ['reset_search_conditions' => false]);
        }
    }

    /**
     * Prepare an array of WorkLocations, each have a name and an array of WorkAddress,
     *
     * @param \Illuminate\Http\Request  $request
     * @return array
     */
    protected function prepareTheWorkPlacesData($request)
    {
        $all_work_locations = $request->user()->company->workLocations()->with('workAddresses')->get();

        return $all_work_locations->keyBy('id')->map(function($work_location) {
            return [
                'name' => $work_location->name,
                'addresses' => $work_location->workAddresses->pluck('name', 'id')->toArray(),
            ];
        })->toArray();
    }


    /**
     * Get the working data from a business month. This is basically a function to translate the business_month into start_date and end_date.
     *
     * @param Employee  $employee
     * @param Carbon    $business_month
     * @param array     that have two keys: summarized_data, and working_days
     */
    protected function getWorkingDataFromBusinessMonth(Employee $employee, $business_month)
    {
        $range = $this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);
        return $this->getWorkingDataFromDateRange($employee, $range[0], $range[1]);
    }


    /**
     * Check if the given day is a law rest day
     *
     * @param Carbon    $day
     * @param array     $rest_days
     * @return boolean
     */
    protected function isLawRestDay($day, $rest_days)
    {
        $index = $day->year . '-' . $day->month . '-' . $day->day;

        return isset($rest_days[$index]) && $rest_days[$index]['type'] == CalendarRestDay::LAW_BASED_REST_DAY;
    }

    /**
     * Check if the given day is a normal rest day
     *
     * @param Carbon    $day
     * @param array     $rest_days
     * @return boolean
     */
    protected function isNormalRestDay($day, $rest_days)
    {
        $index = $day->year . '-' . $day->month . '-' . $day->day;

        return isset($rest_days[$index]) && $rest_days[$index]['type'] == CalendarRestDay::NORMAL_REST_DAY;
    }

    /**
     * Prepare sinsei data for the whole business month
     *
     * @param Employee          $employee
     * @param string            $business_month
     * @return array|json
     */
    protected function prepareSinseiDataForAWholeMonth(Employee $employee, $business_month)
    {
        $range = $this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month);
        $start_date = $range[0];
        $end_date = $range[1]->addDay(); // Add one day to the end_date so that the array will conlude the last day
        $dates_in_range = [];

        $pivot_date = $start_date->copy();
        while ($pivot_date->toDateString() !== $end_date->toDateString()) {
            $dates_in_range[] = $pivot_date->toDateString();
            $pivot_date->addDay();
        }

        $employee_working_days_in_range = EmployeeWorkingDay::with([
            'workingTimestamps',
            'employeeWorkingInformationSnapshots.colorStatuses',
            'employeeWorkingInformations.employeeWorkingInformationSnapshot.colorStatuses',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            // 'employeeWorkingInformations.plannedSchedule.workLocation.setting',
            // 'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
            // 'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
            // 'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
            // 'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
            // 'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.setting',
            // 'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.setting',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
            // 'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
        ])->whereIn('date', $dates_in_range)->where('employee_id', $employee->id)->get()->keyBy('date');

        $target_working_days = $this->getTargetsOfWorkingDayTransfering($employee_working_days_in_range);

        // Load the actual data for those target working days
        $target_working_days = EmployeeWorkingDay::with([
            'workingTimestamps',
            'employeeWorkingInformationSnapshots.colorStatuses',
            'employeeWorkingInformations.employeeWorkingInformationSnapshot.colorStatuses',
            'employeeWorkingInformations.cachedEmployeeWorkingInformation',
            'employeeWorkingInformations.concludedEmployeeWorkingInformation',
            'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
        ])->whereIn('date', $target_working_days)->where('employee_id', $employee->id)->get()->keyBy('date');

        $data_for_one_month = collect([]);

        $pivot_date = $start_date->copy();
        while ($pivot_date->toDateString() !== $end_date->toDateString()) {
            if ($employee_working_days_in_range->has($pivot_date->toDateString())) {
                $working_day = $employee_working_days_in_range[$pivot_date->toDateString()];

                if ($working_day->employeeWorkingInformationSnapshots->isNotEmpty()) {
                    $data_for_one_day = collect([]);

                    $data_for_one_day->push($this->sendDataFollowWorkingDayWithoutLoadingDataVersion($working_day, $employee));

                    $extra_day = $this->extractTheTargetWorkingDayFromALoadedCollectionOfTargetWorkingDays($working_day, $target_working_days);

                    if ($extra_day) {
                        $data_for_one_day->push($this->sendDataFollowWorkingDayWithoutLoadingDataVersion($extra_day, $employee));
                    }

                    // $date_array = $this->checkDateList([$working_day->date], $employee);

                    // foreach ($working_days as $day) {
                    //     $data_for_one_day->push($this->sendDataFollowWorkingDayWithoutLoadingDataVersion($day, $employee));
                    // }

                    $data_for_one_month->put($working_day->date, [
                        'data' => $data_for_one_day,
                        'isHavingConsideringRequest' => $working_day->isHavingConsideringRequest(),
                    ]);
                } else {
                    $data_for_one_month->put($pivot_date->toDateString(), [
                        'data' => null,
                        'isHavingConsideringRequest' => false,
                    ]);
                }

            } else {
                $data_for_one_month->put($pivot_date->toDateString(), [
                    'data' => null,
                    'isHavingConsideringRequest' => false,
                ]);
            }

            $pivot_date->addDay();
        }

        return $data_for_one_month;
    }

    /**
     * Prepare the data that support the presentational logic (list of work_location, etc.) for the javascript side
     *
     * @param Illuminate\Http\Request   $request        the request instance
     * @return void
     */
    protected function sendPresentationalDataOfEmployeeWorkingMonthJsComponent(Request $request)
    {
        $work_location_setting_service = resolve(WorkLocationSettingService::class);
        // NOTE: When merge the branch dinh_work_address_pages, we can use the WorkLocationSettingService to get these, instead of doing this.
        // $all_work_locations = $request->user()->company->workLocations()->with([
        //     'unusedWorkStatuses',
        //     'unusedRestStatuses',
        //     'company.workStatuses',
        //     'company.restStatuses',
        //     'workAddresses'
        //     ])->get();

        $all_work_locations = $work_location_setting_service->getAllWorkLocations();

            // The list of work location with work statuses and rest statuses
        $work_locations = $all_work_locations->map(function($work_location) {
            return [
                'id'            => $work_location->id,
                'name'          => $work_location->name,
                'work_statuses' => $work_location->activatingWorkStatuses()->map(function($status) {
                    return [
                        'id'    => $status->id,
                        'name'  => $status->name,
                    ];
                }),
                'rest_statuses' => $work_location->activatingRestStatuses()->map(function($status) {
                    return [
                        'id'        => $status->id,
                        'name'      => $status->name,
                        'day_based' => $status->unit_type == true,
                    ];
                }),
                'utc_offset_number'          => $work_location->getTimezone()->utc_offset_number,
                'start_time_round_up'          => $work_location->currentSetting()->start_time_round_up,
                'end_time_round_down'          => $work_location->currentSetting()->end_time_round_down,
                'is_use_go_out_button'          => $work_location->currentSetting()->go_out_button_usage == Setting::USE_GO_OUT_BUTTON,
            ];
        });

        $timestamp_types = [
            WorkingTimestamp::START_WORK => "出勤",
            WorkingTimestamp::END_WORK => "退勤",
            WorkingTimestamp::GO_OUT => "外出",
            WorkingTimestamp::RETURN => "戻り",
        ];

        Javascript::put([
            'list_of_work_locations'        => $work_locations,
            'timestamp_types'       => $timestamp_types,
        ]);
    }

    /**
     * save session display schedule time
     * @param Request $request
     * @author LamNguyen <lamnguyen080296@gmail.com>
     */
    public function saveSessionDisplayScheduleTime(Request $request)
    {
        try{
            $request->has('display_toggle') ? Session::put('display_toggle',$request->display_toggle) : null;
            return response()->json(['code' => 200,'msg' => 'success']);
        }
        catch (Exception $exception){
            return response()->json(['code' => 500,'msg' => 'failed']);
        }
    }
}

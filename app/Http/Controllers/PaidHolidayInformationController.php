<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PaidHolidayInformationRequest;
use App\Employee;
use App\WorkLocation;
use App\PaidHolidayInformation;
use App\Department;
use App\EmployeeWorkingDay;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use App\Http\Controllers\Reusables\CustomizedPaginationTrait;
use App\Http\Controllers\Reusables\UseScheduleTypeIconTrait;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use Carbon\Carbon;
use InvalidArgumentException;
use Constants;
use Config;
use Auth;

class PaidHolidayInformationController extends Controller
{

	use CustomizedPaginationTrait, UseScheduleTypeIconTrait;

	/**
	 * Create a new controller instance.
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('choose');

		
		$this->middleware('can:view_work_data_paid_holiday_management');
		$this->middleware('can:change_work_data_paid_holiday_management')->only(['updatePaidHoliday', 'updatePaidHolidayForCheckedEmployees', 'updatePaidHolidayForAllEmployeesInSearchResult']);

		$this->middleware('can:view_work_data_paid_holiday_detail')->only(['showPaidHolidayInformation', 'updatePaidHolidayInformation']);
		$this->middleware('can:change_work_data_paid_holiday_detail')->only('updatePaidHolidayInformation');
    }

    /**
     * List the paid holiday information
     *
     * @param Request   $request
     * @param boolean   $refresh_hitory
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $refresh_hitory = false, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        if ($refresh_hitory == true) {
            session()->forget('paid_holiday_search_history');
            session()->forget('paid_holiday_result_history');

            // Reset the breadcumbs if reset the search history
            $breadcrumbs_service->resetToAttendanceRoot();
        }

        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::PAID_HOLIDAY, $request->route()->getName(), $request->route());


        $conditions = session()->has('paid_holiday_search_history') ? session('paid_holiday_search_history') : $this->getDefaultSearchConditions();

        $data = $this->searchPaidHolidayInformationWithConditions($conditions, $page);

        Javascript::put([
            'conditions' => $conditions,
            'result' => $data['result'],
            'current_page' => $data['current_page'],
            'total' => $data['total'],
            'per_page' => $data['per_page'],
            'show_work_location_name' => !is_numeric(session('current_work_location')),
        ]);

        return view('attendance.paid_holiday.list', [
            'presentation_data' => $this->prepareDataForSearchBox(),
        ]);
    }

    /**
     * Search the paid holiday information with some specific conditions
     *
     * @param Request   $request
     * @return array|json
     */
    public function search(Request $request, $page = 1)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'refresh_hitory' => 0 ]);

        $conditions = $request->input('conditions');

        $data = $this->searchPaidHolidayInformationWithConditions($conditions, $page);

        // If user choose a page other than the default page, we need to save that page, so that we can return correctly to that page later
        if ($page != 1) {
            $breadcrumbs_service->changeParametersOfTheCurrentNode([ 'page' => $page ]);
        }

        return [
            'result' => $data['result'],
            'current_page' => $data['current_page'],
            'total' => $data['total'],
            'per_page' => $data['per_page'],
        ];
	}

    /**
     * Show details and paid holiday information of the given employee
     *
     * @param Request   $request
     * @param Employee 	$employee
     * @return \Illuminate\Http\Response
     */
	public function showPaidHolidayInformation(Request $request, Employee $employee)
	{
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::PAID_HOLIDAY_DETAIL, $request->route()->getName(), $request->route());

        $paid_holiday_infos = $employee->paidHolidayInformations->sortBy('period_start', SORT_REGULAR, true);
        $paid_holiday_infos = $this->preprocessPaidHolidayInformations($paid_holiday_infos->values());

        return view('attendance.paid_holiday.edit', [
            'employee' => $employee,
            'paid_holiday_infos' => $paid_holiday_infos,
		]);
    }

    /**
     * Update the PaidHolidayInformations of this employee
     *
     * @param PaidHolidayInformationRequest   $request
     * @param Employee 	$employee
     * @return \Illuminate\Http\Response
     */
	public function updatePaidHolidayInformation(PaidHolidayInformationRequest $request, Employee $employee)
	{
        $paid_holiday_infos = $employee->paidHolidayInformations->sortBy('period_start', SORT_REGULAR, true);

        $first_period = $paid_holiday_infos->first();

        // Check for differences in these four attributes
        if ($first_period->attendance_rate != $request->input('attendance_rate')) $first_period->attendance_rate = $request->input('attendance_rate');
        if ($first_period->provided_paid_holidays != $request->input('provided_paid_holidays')) $first_period->provided_paid_holidays = $request->input('provided_paid_holidays');
        if ($first_period->carried_forward_paid_holidays != $request->input('carried_forward_paid_holidays')) $first_period->carried_forward_paid_holidays = $request->input('carried_forward_paid_holidays');
        if ($first_period->carried_forward_paid_holidays_hour != $this->toMinutes($request->input('carried_forward_paid_holidays_hour'))) $first_period->carried_forward_paid_holidays_hour = $this->toMinutes($request->input('carried_forward_paid_holidays_hour'));

        if ($first_period->isDirty()) {
            $first_period->last_modified_date = Carbon::today()->toDateString();
            $first_period->last_modified_manager_id = $request->user()->id;
        }

        for ($i = 0; $i < count($paid_holiday_infos); $i++) {
            $paid_holiday_infos[$i]->note = $request->input('notes')[$i];
            $paid_holiday_infos[$i]->save();
        };

        $request->session()->flash('success', '正確に保存できました。');
        return back();
    }

    /**
     * Pre-process the collection of PaidHolidayInformation, add some variables those infos.
     * If an instance is the start of a chain then it will have row_span bigger than 1, any others will have row_span = null.
     * Any instance that is the tail (or the middle) part of a chain link will have skip = true, any others will have skip = false.
     *
     * @param Collection    $paid_holiday_infos
     * @return Collection
     */
    protected function preprocessPaidHolidayInformations($paid_holiday_infos)
    {
        $skip = null;
        for ($i = 0; $i<count($paid_holiday_infos); $i++) {
            if ($skip != null && $skip > 1) {
                $paid_holiday_infos[$i]->skip = true;
                $skip--;
            } else {
                $skip = ($paid_holiday_infos[$i]->linkedPaidHolidayInformation) ? $paid_holiday_infos[$i]->linkDepth() : null;
                $paid_holiday_infos[$i]->row_span = $skip;
                $paid_holiday_infos[$i]->skip = false;
            }
        }

        return $paid_holiday_infos;
    }

    /**
     * Convert a time string back to number of minutes.
     *
     * @param string        $time_string    format: HH:mm
     * @return int|null
     */
    protected function toMinutes($time_string)
    {
        $data = explode(':', $time_string);

        if (count($data) == 2) {
            return floatval($data[0]) * 60 + floatval($data[1]);
        } else {
            return null;
        }
    }

    /**
     * Update paid holiday information of a given Employee
     *
     * @param Request   $request
     * @param Employee  $employee
     * @return array|json
     */
    public function updatePaidHoliday(Request $request, Employee $employee)
    {
        if ($employee->canUpdatePaidHolidayInformation()) {
            $this->createNewPaidHolidayInformationRecordForTheseEmployee(collect([$employee]));

            $request->session()->flash('success', '正確に更新しました');
            return [
                'success' => '正確に更新しました',
            ];
        }
        return response()->json(['error' => '正確に更新できませんでした'], 400);
    }

    /**
     * Update paid holiday information for checked employees (only the updatable ones)
     *
     * @param Request   $request
     * @return array|json
     */
    public function updatePaidHolidayForCheckedEmployees(Request $request)
    {
		$this->updatePaidHolidayForMultipleEmployees($request->input('employee_ids'));

        $request->session()->flash('success', '正確に更新しました');
        return [
            'success' => '正確に更新しました',
        ];
	}

	/**
     * Update paid holiday information for all updatable employees in the search result.
     *
     * @param Request   $request
     * @return array|json
     */
	public function updatePaidHolidayForAllEmployeesInSearchResult(Request $request)
	{
		$search_result = session('paid_holiday_result_history');

		if ($search_result) {
			$this->updatePaidHolidayForMultipleEmployees($search_result->pluck('id')->toArray());

			$request->session()->flash('success', '正確に更新しました');
			return [
				'success' => '正確に更新しました',
			];
		}
	}

	/**
	 * Update paid holiday information for multiple employees
	 * The above two functions are just wrapper of this function.
	 *
	 * @param array 	$employee_ids
	 * @return void
	 */
	protected function updatePaidHolidayForMultipleEmployees($employee_ids)
	{
		$employees = Employee::with([
            'paidHolidayInformations',
            'workLocation.setting',
            'workLocation.company.setting',
        ])
        ->whereIn('id', $employee_ids)->get();

		// Filter out any employee who can not be updated
		$updatable_employees = $employees->filter(function($employee) {
            return $employee->canUpdatePaidHolidayInformation();
        });

        $this->createNewPaidHolidayInformationRecordForTheseEmployee($updatable_employees);
	}

    /**
     * Create a new PaidHolidayInformation record for the given employees(that's right, a collection of employee)
     *
     * @param collection    $employees
     * @return void
     */
    protected function createNewPaidHolidayInformationRecordForTheseEmployee($employees)
    {
        $today = Carbon::today();

        foreach($employees as $employee) {
            $update_date_array = explode('/', $employee->holidays_update_day);

            $first_day_after_joined = $employee->firstDayOfAfterJoinedPeriod();
            $first_update_day = Carbon::createFromDate($first_day_after_joined->year, $update_date_array[0], $update_date_array[1], $first_day_after_joined->tz);
            if ($first_update_day->lt($first_day_after_joined)) $first_update_day->addYear();

            if ($first_day_after_joined->ne($first_update_day) && $first_day_after_joined->lte($today) && $today->lt($first_update_day)) {
                $start_day = $first_day_after_joined->copy();
                $end_day = $first_update_day->copy()->subDay();

            } else {

                $update_day = Carbon::createFromDate($today->year, $update_date_array[0], $update_date_array[1], $today->tz);
                if ($update_day->lte($today)) {
                    $start_day = $update_day->copy();
                    $end_day = $update_day->copy()->addYear()->subDay();
                } else {
                    $end_day = $update_day->copy()->subDay();
                    $start_day = $update_day->copy()->subYear();
                }
            }

            $check = $employee->paidHolidayInformations()->where('period_start', $start_day->format('Y-m-d'))->where('period_end', $end_day->format('Y-m-d'))->first();

            if (!$check) {
                $new_paid_holiday_info = new PaidHolidayInformation([
                    'employee_id' => $employee->id,
                    'work_time_per_day' => $employee->work_time_per_day * 60,
                    'period_start' => $start_day->format('Y-m-d'),
                    'period_end' => $end_day->format('Y-m-d'),
                    'attendance_rate' => $this->calculateAttendanceRate($employee, $start_day),
                    'provided_paid_holidays' => $this->calculateNumberOfPaidHolidayToProvide($employee),
                    'last_modified_date' => $today->format('Y-m-d'),
                ]);

                $new_paid_holiday_info->save();
            }
        }
    }

    /**
     * Calculate the attendance rate for the newly create PaidHolidayInformation instance.
     * This function need the employee to who the above instance connects and the start_day of that instance.
     *
     * @param Employee  $employee
     * @param Carbon    $start_day
     * @return float|null
     */
    protected function calculateAttendanceRate(Employee $employee, Carbon $start_day)
    {
        $one_year_ago_working_days = $query = EmployeeWorkingDay::with([
                'concludedEmployeeWorkingDay',
                'employeeWorkingInformations.cachedEmployeeWorkingInformation',
                'employeeWorkingInformations.concludedEmployeeWorkingInformation',
                'employeeWorkingInformations.plannedSchedule.workLocation.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedWorkStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.unusedRestStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.setting',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.workStatuses',
                'employeeWorkingInformations.plannedSchedule.workLocation.company.restStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.setting',
                'employeeWorkingInformations.currentRealWorkLocation.unusedWorkStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.unusedRestStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.company.setting',
                'employeeWorkingInformations.currentRealWorkLocation.company.workStatuses',
                'employeeWorkingInformations.currentRealWorkLocation.company.restStatuses',
                'employeeWorkingInformations.employeeWorkingDay.employee.workLocation.company',
            ])->where('employee_id', $employee->id)
            ->where('date', '>=', $start_day->copy()->subYear()->format('Y-m-d'))
            ->where('date', '<=', $start_day->copy()->subDay()->format('Y-m-d'))->get();

        $have_schedule_days = 0;
        $kekkin_days = 0;

        foreach ($one_year_ago_working_days as $working_day) {
            $data = $working_day->getSummaryInformation();
            $have_schedule_days = $data['have_schedule'] ? $have_schedule_days + 1 : $have_schedule_days;
            $kekkin_days = $data['real_is_kekkin'] ? $kekkin_days + 1 : $kekkin_days;
        }

        if ($have_schedule_days != 0) {
            return (($have_schedule_days - $kekkin_days) * 100)/$have_schedule_days;
        } else {
            return null;
        }
    }

    /**
     * Calculate the soon to be provided_paid_holidays for the given employee.
     *
     * @param Employee    $employees
     * @return integer|null
     */
    protected function calculateNumberOfPaidHolidayToProvide(Employee $employee)
    {
        $first_day_after_joined = $employee->firstDayOfAfterJoinedPeriod();

        $update_date_array = explode('/', $employee->holidays_update_day);
        $first_update_day = $first_day_after_joined->copy()->month($update_date_array[0])->day($update_date_array[1]);
        if ($first_update_day->lt($first_day_after_joined)) $first_update_day->addYear();

        $today = Carbon::today();

        $numbers_of_provide_days = $this->getTheProvidePaidHolidaySetting($employee);

        // Manager has changed employee's holidays_update_day once and today is between the original holidays_update_day and the new one
        if ($first_day_after_joined->ne($first_update_day) && $first_day_after_joined->lte($today) && $today->lt($first_update_day)) {
            $offset = 0;

        // Manager has changed employee's holidays_update_day once and today is NOT between the original holidays_update_day and the new one
        } else if ($first_day_after_joined->ne($first_update_day)) {
            $offset = $today->diffInYears($first_update_day) + 1;

        // The employee's holidays_update_day hasnt been changed
        } else {
            $offset = $today->diffInYears($first_update_day);
        }

        // If the provide paid holiday setting is not still not set, then provide 0 paid holiday
        if ($numbers_of_provide_days == null || count($numbers_of_provide_days) == 0 ) {
            return null;
        }

        return isset($numbers_of_provide_days[$offset]) ? $numbers_of_provide_days[$offset] : $numbers_of_provide_days[count($numbers_of_provide_days) -1];
    }

    /**
     * Get the provided_paid_holidays in the setting of a WorkLocation(also include the first time and the increase rate).
     * It will be something like [10, 11, 12, 13, 14, 15].
     *
     * @param Employee    $employees
     * @return array|null
     */
    protected function getTheProvidePaidHolidaySetting(Employee $employee)
    {
        $numbers_of_provide_days = [];

        $first_time_bonus_attributes = [
            config('constants.normal_bonus') => 'paid_holiday_first_time_normal_type',
            config('constants.four_days_per_week_bonus') => 'paid_holiday_first_time_4wdpw_type',
            config('constants.three_days_per_week_bonus') => 'paid_holiday_first_time_3wdpw_type',
            config('constants.two_days_per_week_bonus') => 'paid_holiday_first_time_2wdpw_type',
            config('constants.one_day_per_week_bonus') => 'paid_holiday_first_time_1wdpw_type',
        ];
        $bonus_increase_rate_attributes = [
            config('constants.normal_bonus') => 'paid_holiday_increase_rate_normal_type',
            config('constants.four_days_per_week_bonus') => 'paid_holiday_increase_rate_4wdpw_type',
            config('constants.three_days_per_week_bonus') => 'paid_holiday_increase_rate_3wdpw_type',
            config('constants.two_days_per_week_bonus') => 'paid_holiday_increase_rate_2wdpw_type',
            config('constants.one_day_per_week_bonus') => 'paid_holiday_increase_rate_1wdpw_type',
        ];

        $setting = $employee->workLocation->currentSetting();

        if ($employee->holiday_bonus_type != Config::get('constants.manually_input_bonus')) {
            $numbers_of_provide_days[] = $setting->getAttribute($first_time_bonus_attributes[$employee->holiday_bonus_type]);
            $increase_rate_array = $setting->getAttribute($bonus_increase_rate_attributes[$employee->holiday_bonus_type]) !== null ? explode(',', $setting->getAttribute($bonus_increase_rate_attributes[$employee->holiday_bonus_type])) : null;
            $numbers_of_provide_days = $increase_rate_array !== null ? array_merge($numbers_of_provide_days, $increase_rate_array) : $numbers_of_provide_days;
        } else {
            $numbers_of_provide_days = null;
        }

        return $numbers_of_provide_days;
    }

    /**
     * Get the default search conditions.
     *
     * @return array
     */
    protected function getDefaultSearchConditions()
    {
        return [
            'only_updateable_employee' => true,
            'presentation_id' => null,
            'employee_name' => null,
            'employment_type' => null,
            'departments' => [],
            'work_status' => config('constants.working'),
            'holiday_bonus_type' => null,
            'joined_date_start' => null,
            'joined_date_end' => null,
            'start_worked_years' => null,
            'start_worked_months' => null,
            'end_worked_years' => null,
            'end_worked_months' => null,
            'holidays_update_day' => null,
            'start_holidays_update_date' => null,
            'end_holidays_update_date' => null,
            'attendance_rate' => null,
            'paid_holiday_exception' => false,
        ];
    }

    /**
     * Search paid holiday informations with the given conditions.
     *
     * @param array
     * @return collection
     */
    protected function searchPaidHolidayInformationWithConditions($conditions, $page = 1)
    {
        $matching_employees = $this->searchEmployeeWithConditions($conditions);

        $data = $this->processAndTransformData($matching_employees);

        // Second filtering: this time filter with those special conditions
        $result = $this->secondFiltering($data, $conditions);

        // Save the search conditions and result to session
        session(['paid_holiday_search_history' => $conditions]);
        session(['paid_holiday_result_history' => $result]);

        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        // Set the 2nd dimension of this breadcrumb
        $breadcrumbs_service->setTheSecondDimensionForCurrentNode($result->pluck('id'), 'employee');

        $data = $this->paginateData($result, $page);

        return [
            'result' => $data['data'],
            'current_page' => $data['current_page'],
            'total' => $result->count(),
            'per_page' => $this->ITEMS_PER_PAGE,
        ];
    }

    /**
     * Filter the given data set with these conditions:
     *     - start_worked_time < x < end_worked_time
     *     - start holiday_update_date < x < end_holiday_update date and attendance_rate < y
     *
     * Why do we need to do this ? Because these conditions can't be applied when query data from database.
     *
     * @param collection     $employee
     * @param array     $conditions
     * @return collection
     */
    protected function secondFiltering($employees, $conditions)
    {

        // 更新必要者のみ
        if (isset($conditions['only_updateable_employee']) && $conditions['only_updateable_employee'] == true) {
            $employees = $employees->filter(function($employee) {
                return $employee['can_be_updated'] == true;
            });
        }

        // 勤続年数 A 年 B ヶ月　から
        $start_sum_worked_months = isset($conditions['start_worked_years']) || isset($conditions['start_worked_months']) ?
            intval($conditions['start_worked_years']) * 12 + intval($conditions['start_worked_months']) : null;

        if (isset($start_sum_worked_months)) {
            $employees = $employees->filter(function($employee) use ($start_sum_worked_months) {
                return $employee['sum_worked_months'] >= $start_sum_worked_months;
            });
        }

        // 勤続年数 C 年 D ヶ月　まで
        $end_sum_worked_months = isset($conditions['end_worked_years']) || isset($conditions['end_worked_months']) ?
            intval($conditions['end_worked_years']) * 12 + intval($conditions['end_worked_months']) : null;

        if (isset($end_sum_worked_months)) {
            $employees = $employees->filter(function($employee) use ($end_sum_worked_months) {
                return $employee['sum_worked_months'] <= $end_sum_worked_months;
            });
        }

        // 有給更新日が X から
        if (isset($conditions['start_holidays_update_date'])) {

            try {
                $carbon_start_holidays_update_date = Carbon::createFromFormat('m/d', $conditions['start_holidays_update_date']);
            } catch (InvalidArgumentException $e) {
                $carbon_start_holidays_update_date = null;
            }

            if (isset($carbon_start_holidays_update_date)) {

                $employees = $employees->filter(function($employee) use ($carbon_start_holidays_update_date) {
                    $carbon_holidays_update_date = Carbon::createFromFormat('m/d', $employee['holiday_update_day']);
                    return $carbon_holidays_update_date->gte($carbon_start_holidays_update_date);
                });
            }
        }

        // 有給更新日が Y まで
        if (isset($conditions['end_holidays_update_date'])) {

            try {
                $carbon_end_holidays_update_date = Carbon::createFromFormat('m/d', $conditions['end_holidays_update_date']);
            } catch (InvalidArgumentException $e) {
                $carbon_end_holidays_update_date = null;
            }

            if (isset($carbon_end_holidays_update_date)) {

                $employees = $employees->filter(function($employee) use ($carbon_end_holidays_update_date) {
                    $carbon_holidays_update_date = Carbon::createFromFormat('m/d', $employee['holiday_update_day']);
                    return $carbon_holidays_update_date->lte($carbon_end_holidays_update_date);
                });
            }
        }

        // のうち従業員の出勤率が Z ％未満
        if (isset($conditions['attendance_rate'])) {
            $employees = $employees->filter(function($employee) use ($conditions) {
                return $employee['last_period_attendance_rate'] < floatval($conditions['attendance_rate']);
            });
        }

        return $employees;
    }

    /**
     * Process and transform queried data form database into a suitable format for the second filtering.
     *
     * @param collection    $employees
     * @return collection
     */
    protected function processAndTransformData($employees)
    {
        $now = Carbon::now();

        $data = $employees->map(function($employee) use ($now) {
            $joined_date_carbon = new Carbon($employee->joined_date);

            $sum_worked_months = $now->diffInMonths($joined_date_carbon);
            $worked_years = floor($sum_worked_months / 12);
            $worked_months = $sum_worked_months % 12;

            $last_period_paid_holiday_information = $employee->paidHolidayInformations->sortBy('period_start')->last();

            return [
                'id'                        => $employee->id,
                'presentation_id'           => $employee->presentation_id,
                'employee_name'             => $employee->fullName(),
                'schedule_type_icon'        => $this->getScheduleTypeIcon($employee->schedule_type),
                'holiday_bonus_type'        => Constants::holidayBonusTypes()[$employee->holiday_bonus_type],
                'work_location_id'          => $employee->work_location_id,
                'joined_date'               => $employee->joined_date,
                'sum_worked_months'         => $sum_worked_months,
                'worked_years'              => $worked_years,
                'worked_months'             => $worked_months,
                'holiday_update_day'        => $employee->holidays_update_day,
                'last_period_attendance_rate'       => isset($last_period_paid_holiday_information) ? $last_period_paid_holiday_information->attendance_rate : null,
                'last_period_provided_days'         => isset($last_period_paid_holiday_information) ? $last_period_paid_holiday_information->provided_paid_holidays : null,
                'last_period_carried_forward_days'  => isset($last_period_paid_holiday_information) ? $last_period_paid_holiday_information->carried_forward_paid_holidays : null,
                'last_period_carried_forward_hour'  => isset($last_period_paid_holiday_information) ? $last_period_paid_holiday_information->carried_forward_paid_holidays_hour : null,
                'last_period_available_days'        => isset($last_period_paid_holiday_information) ? $last_period_paid_holiday_information->available_paid_holidays : null,
                'last_period_available_hour'        => isset($last_period_paid_holiday_information) ? $last_period_paid_holiday_information->available_paid_holidays_hour : null,
                'last_period_last_update'           => isset($last_period_paid_holiday_information) ? $last_period_paid_holiday_information->last_modified_date : null,
                'last_period_attendance_rate'       => isset($last_period_paid_holiday_information) ? $last_period_paid_holiday_information->attendance_rate : 0.0,
                'can_be_updated'                        => $employee->canUpdatePaidHolidayInformation(),
            ];
        });

        return $data;
    }

    /**
     * Query data with the given conditions(only some of the conditions will be applied, some other will not due to their nature)
     *
     * @param array     $conditions
     * @return collection
     */
    protected function searchEmployeeWithConditions($conditions)
    {
        $query = Employee::query();

        if (is_numeric(session('current_work_location'))) {
            $query = $query->where('work_location_id', session('current_work_location'));
        } else if (is_array(session('current_work_location'))) {
            $query = $query->whereIn('work_location_id', session('current_work_location'));
        }

        if (isset($conditions['presentation_id'])) {
            $query = $query->where('presentation_id', '=', $conditions['presentation_id']);
        }

        if (isset($conditions['employee_name'])) {
            $query = $query->where(\DB::raw('CONCAT_WS("", last_name, first_name)'), 'like', '%' . $conditions["employee_name"] . '%');
        }

        if (isset($conditions['employment_type'])) {
            $query = $query->where('employment_type', $conditions['employment_type']);
        }

        if (!empty($conditions['departments'])) {
            $query = $query->whereIn('department_id', $conditions['departments']);
        }

        if (isset($conditions['work_status'])) {
            $query = $query->where('work_status', '=', $conditions['work_status']);
        }

        if (isset($conditions['holiday_bonus_type'])) {
            $query = $query->where('holiday_bonus_type', '=', $conditions['holiday_bonus_type']);
        }

        if (isset($conditions['joined_date_start'])) {
            $query = $query->where('joined_date', '>=', $conditions['joined_date_start']);
        }

        if (isset($conditions['joined_date_end'])) {
            $query = $query->where('joined_date', '<=', $conditions['joined_date_end']);
        }

        if (isset($conditions['holidays_update_day'])) {

            try {
                $carbon = Carbon::createFromFormat('Y/m/d', config('caeru.blind_year') . '/' . $conditions['holidays_update_day']);
            } catch (InvalidArgumentException $e) {
                $carbon = null;
            }

            if (isset($carbon)) {
                $query = $query->where('holidays_update_day', $carbon->format('Y-m-d'));
            }
        }

        $query = $query->where('paid_holiday_exception', isset($conditions['paid_holiday_exception']) ? $conditions['paid_holiday_exception'] : false);

        return $query->with('paidHolidayInformations')->get();
    }

    /**
     * Prepare presentation data for the search box.
     *
     * @return array
     * and also directly put an array to javascrip side.
     */
    protected function prepareDataForSearchBox()
    {
        $current_work_location = session('current_work_location');

        $company = Auth::user()->company;

        $work_location_names = $company->workLocations->pluck('name', 'id')->toArray();

        $departments = $company->departments->pluck('name', 'id')->toArray();

        $employment_types = Constants::employmentTypes();

        $work_statuses = Constants::workStatuses();

        $holiday_bonus_types = Constants::holidayBonusTypes();

        if ($current_work_location === 'all') {

            $employee_names = $company->employees->map(function($item) {
                return ['name' => $item->last_name . $item->first_name];
            })->toArray();

        } elseif (is_array($current_work_location)) {

            $employee_names = Employee::whereIn('work_location_id', $current_work_location)->get()->map(function($item) {
                return ['name' => $item->last_name . $item->first_name];
            })->toArray();

        } else {

            $work_location = WorkLocation::find($current_work_location);

            $employee_names = $work_location->employees->map(function($item) {
                return ['name' => $item->last_name . $item->first_name];
            })->toArray();

            $departments = $work_location->activatingDepartments()->pluck('name', 'id')->toArray();
        }

        Javascript::put([
            'work_location_names' => $work_location_names,
            'employee_names' => $employee_names,
        ]);

        return [
            'departments' => $departments,
            'employment_types' => $employment_types,
            'work_statuses' => $work_statuses,
            'holiday_bonus_types' => $holiday_bonus_types,
        ];
    }
}
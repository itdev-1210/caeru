<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PaidHolidayInformationRequest;
use App\Employee;
use App\PaidHolidayInformation;
use App\Department;
use App\Reusables\PaidHolidayInformationTrait;
use Carbon\Carbon;
use App\EmployeeWorkingDay;

class PaidHolidayInformationController extends Controller
{

	use PaidHolidayInformationTrait;

	/**
	 * Create a new controller instance.
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('choose');

		
		$this->middleware('can:view_work_data_paid_holiday_management');
		// $this->middleware('can:change_work_data_paid_holiday_management');

		$this->middleware('can:view_work_data_paid_holiday_detail');
		$this->middleware('can:change_work_data_paid_holiday_detail')->only('update');
	}

	/**
	 * 
	 * Display a listing of the resource.
	 * 
	 */
	public function index(Request $request)
	{
		
		$departments = Department::all();
		$current_work_location = request()->session()->get('current_work_location');
		if($request->session()->has('searchDefault')){
			return $this->search($request);
		}

		$employees = Employee::getDataPaidHolidayInformation()
		->workLocations($current_work_location)
		->paginate(20);
		return view('paidholidayinformation.list')
		->with([
			'employees'=>$employees,
			'current_work_location'=>$current_work_location,
			'departments' =>$departments,
		]);
		
	}

	/**
	 *
	 * Basic search
	 *
	 */
	public function search(Request $request)
	{
		$request->session()->put('searchDefault', 1);
		if(!$request->input('isSearch')) {
			$request->session()->put('searchDefault', 2);
			$isCheck = $request->input('isCheck', $request->session()->get('isCheck_old_input'));
			$presentation_id = $request->input('presentation_id', $request->session()->get('presentation_id_old_input'));
			$first_name = $request->input('first_name', $request->session()->get('first_name_old_input'));

			$employment_type = $request->input('employment_type', $request->session()->get('employment_type_old_input'));

			$department = $request->input('department', $request->session()->get('department_old_input'));
	 	
			$work_status = $request->input('work_status', $request->session()->get('work_status_old_input'));
			$holiday_bonus_type = $request->input('holiday_bonus_type', $request->session()->get('holiday_bonus_type_old_input'));

			$joined_day_start = $request->input('joined_day_start', $request->session()->get('joined_day_start_old_input'));
			$joined_day_end = $request->input('joined_day_end', $request->session()->get('joined_day_end_old_input'));

			$joined_year_start = $request->input('joined_year_start', $request->session()->get('joined_year_start_old_input'));
			$joined_month_start = $request->input('joined_month_start', $request->session()->get('joined_month_start_old_input'));

			$joined_year_end = $request->input('joined_year_end', $request->session()->get('joined_year_end_old_input'));
			$joined_month_end = $request->input('joined_month_end', $request->session()->get('joined_month_end_old_input'));

			$holidays_update_day = $request->input('holidays_update_day', $request->session()->get('holidays_update_day_old_input'));
			$holidays_update_day_start_month = $request->input('holidays_update_day_start_month', $request->session()->get('holidays_update_day_start_month_old_input'));
			$holidays_update_day_start_day = $request->input('holidays_update_day_start_day', $request->session()->get('holidays_update_day_start_day_old_input'));
			$holidays_update_day_end_month = $request->input('holidays_update_day_end_month', $request->session()->get('holidays_update_day_end_month_old_input'));
			$holidays_update_day_end_day = $request->input('holidays_update_day_end_day', $request->session()->get('holidays_update_day_end_day_old_input'));

			$attendance_rate = $request->input('attendance_rate', $request->session()->get('attendance_rate_old_input'));

			$paid_holiday_exception = $request->input('paid_holiday_exception', $request->session()->get('paid_holiday_exception_old_input'));
		}
		else {
			$isCheck = $request->input('isCheck', 0);
			$presentation_id = $request->input('presentation_id');
			$first_name = $request->input('first_name');

			$employment_type = $request->input('employment_type');

			$department = $request->input('department');
	 	
			$work_status = $request->input('work_status');
			$holiday_bonus_type = $request->input('holiday_bonus_type');

			$joined_day_start = $request->input('joined_day_start');
			$joined_day_end = $request->input('joined_day_end');

			$joined_year_start = $request->input('joined_year_start');
			$joined_month_start = $request->input('joined_month_start');

			$joined_year_end = $request->input('joined_year_end');
			$joined_month_end = $request->input('joined_month_end');

			$holidays_update_day = $request->input('holidays_update_day');
			$holidays_update_day_start_month = $request->input('holidays_update_day_start_month');
			$holidays_update_day_start_day = $request->input('holidays_update_day_start_day');
			$holidays_update_day_end_month = $request->input('holidays_update_day_end_month');
			$holidays_update_day_end_day = $request->input('holidays_update_day_end_day');

			$attendance_rate = $request->input('attendance_rate');

			$paid_holiday_exception = $request->input('paid_holiday_exception');
		}

		if(empty($presentation_id)) {
			$request->session()->forget('presentation_id_old_input');
		}
		
		if(empty($first_name)) {
			$request->session()->forget('first_name_old_input');
		}
		if(empty($employment_type)) {
			$request->session()->forget('employment_type_old_input');
		}
		
		if(empty($department)) {
			$request->session()->forget('department_old_input');
		}
		if(empty($work_status)) {
			$request->session()->forget('work_status_old_input');
		}
		
		if(empty($holiday_bonus_type)) {
			$request->session()->forget('holiday_bonus_type_old_input');
		}
		if(empty($joined_day_start)) {
			$request->session()->forget('joined_day_start_old_input');
		}
		
		if(empty($joined_day_end)) {
			$request->session()->forget('joined_day_end_old_input');
		}
		if(empty($joined_year_start)) {
			$request->session()->forget('joined_year_start_old_input');
		}
		
		if(empty($joined_month_start)) {
			$request->session()->forget('joined_month_start_old_input');
		}
		if(empty($joined_year_end)) {
			$request->session()->forget('joined_year_end_old_input');
		}
		
		if(empty($joined_month_end)) {
			$request->session()->forget('joined_month_end_old_input');
		}
		if(empty($holidays_update_day)) {
			$request->session()->forget('holidays_update_day_old_input');
		}
		
		if(empty($holidays_update_day_start_month)) {
			$request->session()->forget('holidays_update_day_start_month_old_input');
		}
		if(empty($holidays_update_day_start_day)) {
			$request->session()->forget('holidays_update_day_start_day_old_input');
		}
		
		if(empty($holidays_update_day_end_month)) {
			$request->session()->forget('holidays_update_day_end_month_old_input');
		}
		
		if(empty($holidays_update_day_end_day)) {
			$request->session()->forget('holidays_update_day_end_day_old_input');
		}
		if(empty($attendance_rate)) {
			$request->session()->forget('attendance_rate_old_input');
		}
		
		if(empty($paid_holiday_exception)) {
			$request->session()->forget('paid_holiday_exception_old_input');
		}

		$departments = Department::all();
		$current_work_location = request()->session()->get('current_work_location');

		$workedStart = ($joined_year_start*12) + $joined_month_start;
		$workedEnd = ($joined_year_end*12) + $joined_month_end;
		$employees = Employee::getDataPaidHolidayInformation()
		->workLocations($current_work_location)
		->searchId($presentation_id)
		->searchFirstName($first_name)
		->searchEmploymentType($employment_type)
		->searchDepartment($department)
		->searchWorkStatus($work_status)
		->searchHolidayBonusType($holiday_bonus_type)
		->searchJoinedDay($joined_day_start, $joined_day_end)
		
		->searchWorkedTime($workedStart, $workedEnd)
		->advanceSearch($holidays_update_day, $holidays_update_day_start_month, $holidays_update_day_start_day, $holidays_update_day_end_month, $holidays_update_day_end_day, $attendance_rate)
		->searchPaidHolidayException($paid_holiday_exception)
		->necessaryKoushin($isCheck)
		->paginate(20);
		session([
			'isCheck_old_input'			=>$isCheck,
			'presentation_id_old_input' =>$presentation_id,
			'first_name_old_input'      =>$first_name,
			'employment_type_old_input'	=>$employment_type,
			'department_old_input'    =>$department,
			'work_status_old_input' =>$work_status,
			'holiday_bonus_type_old_input'      =>$holiday_bonus_type,
			'joined_day_start_old_input'	=>$joined_day_start,
			'joined_day_end_old_input'    =>$joined_day_end,
			'joined_year_start_old_input' =>$joined_year_start,
			'joined_month_start_old_input'      =>$joined_month_start,
			'joined_year_end_old_input'	=>$joined_year_end,
			'joined_month_end_old_input'    =>$joined_month_end,
			'holidays_update_day_old_input' =>$holidays_update_day,
			'holidays_update_day_start_month_old_input'      =>$holidays_update_day_start_month,
			'holidays_update_day_start_day_old_input'	=>$holidays_update_day_start_day,
			'holidays_update_day_end_month_old_input'    =>$holidays_update_day_end_month,
			'holidays_update_day_end_day_old_input' =>$holidays_update_day_end_day,
			'attendance_rate_old_input'      =>$attendance_rate,
			'paid_holiday_exception_old_input'	=>$paid_holiday_exception

		]);
		return view('paidholidayinformation.list')
		->with([
			'employees'=>$employees,
			'current_work_location'=>$current_work_location,
			'departments' =>$departments,
		]);
	}

	/**
	 *
	 * Show employee information and paidholiday list of current employee.
	 *  
	 */
	public function edit(Employee $employee, $page =1)
	{
		$paidholidayinformations = PaidHolidayInformation::where('employee_id','=', $employee->id)
		->orderBy('id','desc')
		->get();
		$current_work_location = request()->session()->get('current_work_location');
		$getEmployeeJoinedDate = $this->getEmployeeJoinedDate($employee->joined_date);


		return view('paidholidayinformation.edit')
		->with([
			'employee'=>$employee,
			'page'=>$page,
			'current_work_location'=>$current_work_location,
			'getEmployeeJoinedDate'=>$getEmployeeJoinedDate,
			'paidholidayinformations'=>$paidholidayinformations,

		]);
	}

	/**
	 *
	 * Get day of carried_work_paid_holidays without '日'
	 *
	 */
	private function getCarriedForwardDay($carriedForwardDay)
	{
		return preg_replace('/日/','', $carriedForwardDay);
	}

	/**
	 *
	 * get carried_work_paid_holidays without ':'
	 *
	 */
	private function getCarriedForwardTime($carrtedForwardTime)
	{
		return preg_split('/:/', $carrtedForwardTime);
	}

	/**
	 * 
	 * get carried_forward_paid_holidays from Form and revert to float
	 * 
	 */
	private function getCarriedForwardPaidHolidays($paidholidayinformation, $carriedForwardDay, $carrtedForwardTime)
	{
		return $paidholidayinformation->revertDate(
			$this->getCarriedForwardDay($carriedForwardDay),
			$this->getCarriedForwardTime($carrtedForwardTime)
		);
	}

	/**
	 *
	 * update paid holiday information of current employee
	 *
	 */
	public function update(PaidHolidayInformationRequest $request, Employee $employee, $page =1)
	{
		$paidholidayinformation = PaidHolidayInformation::find($request->presentation_id);
		$paidholidayinformation->attendance_rate = $request->attendance_rate;
		$paidholidayinformation->provided_paid_holidays = $request->provided_paid_holidays;
		$paidholidayinformation->note = $request->note;
		$paidholidayinformation->last_modified_date = Carbon::now();

		list($hour, $dayPlus) = $paidholidayinformation->revertTime($this->getCarriedForwardTime($request->carried_forward_paid_holidays_hour));
		$paidholidayinformation->carried_forward_paid_holidays = $dayPlus + $this->getCarriedForwardDay($request->carried_forward_paid_holidays);
		$paidholidayinformation->carried_forward_paid_holidays_hour = $hour;
		///////////////
		$available_paid_holidays_computed = $request->provided_paid_holidays + ($dayPlus + $this->getCarriedForwardDay($request->carried_forward_paid_holidays)) - $paidholidayinformation->consumed_paid_holidays;
        $available_paid_holidays_hour_computed = $hour*60 - $paidholidayinformation->consumed_paid_holidays_hour*60;
        list($available_day, $available_hour) = $this->convertTimeToDay($available_paid_holidays_hour_computed, $paidholidayinformation->work_time_per_day);
      	$available_paid_holidays_hour = 0;
        $paidholidayinformation->available_paid_holidays = $available_paid_holidays_computed + $available_day;
       	if($available_hour < 0){
           $available_paid_holidays_hour = $paidholidayinformation->work_time_per_day + $available_hour;
        }else {
             $available_paid_holidays_hour = $available_hour;
        }
        $paidholidayinformation->available_paid_holidays_hour = $available_paid_holidays_hour;
		//////////////
		$paidholidayinformation->last_modified_manager_id = auth()->user()->id;
		$paidholidayinformation->save();

		return back()->with('success', '保存しました');

	}

	/**
	 * Compute worked time.
	 *
	 * @param Carbon $joined_date
	 *
	 * @return string.
	 */
	private function getEmployeeJoinedDate($joined_date)
	{
		$joined_date = new \Carbon\Carbon($joined_date);
		$result = '';
		Carbon::setLocale('ja');
		$currYear = Carbon::now()->year;
		$currMonth = Carbon::now()->month;
		if($currYear !== $joined_date->year)
		{
			$year = Carbon::now()->diffForHumans($joined_date, true, false);
			$result .= $year;

		}
		if($currMonth !== $joined_date->month)
		{
			$employeeDate = Carbon::create(null,$joined_date->month, null);
			$month = Carbon::now()->diffForHumans($employeeDate, true, false);
			$result .= $month;
		}
		return $result;

	}


	/**
	 * Get multi koushin
	 *
	 * @param array $employeeIds
	 *
	 * 
	 */
	public function getPaidHolidayInformationApart($employeeIds)
	{
		$try = true;
		$isApartKoushin = true;
		foreach($employeeIds as $employeeId)
		{
			$employee = Employee::findOrFail($employeeId);

			if($employee->hasKoushin())
			{
				$isApartKoushin = false;
				$try = $try && $this->addPaidHolidayInformation($employee);
				if(!$try) break;
			}
			else {
				$isApartKoushin = $isApartKoushin && true;
			}
		}			
		return $try && !$isApartKoushin;
	}


	/**
	 * Add paid holiday information for one employee.
	 * 
	 * @param integer $employeeId
	 *
	 */
	public function store(Request $request, $employeeId)
	{
		try {
			$employee = Employee::find($employeeId);
			if($this->addPaidHolidayInformation($employee))
			{
				return back()->with('success','保存しました');
			}
			else 
			{
				return back()->withErrors('入力に誤りがあります');
			}
		} catch (\Exception $e) {
			return back()->withErrors('入力に誤りがあります');
		}
	}

	/**
	 * 
	 * Add paid holiday information for employees with multichecked.
	 * 
	 */
	public function apartStore(Request $request)
	{
		try {
			$employeeIds = explode(',',$request->employeeIdArray);
			if($this->getPaidHolidayInformationApart($employeeIds)) {
				return back()->with('success','保存しました');
			}
			else 
			{
				return back()->withErrors('入力に誤りがあります');
			}
		} catch (\Exception $e) {
			return back()->withErrors('入力に誤りがあります');
		}
	}


	/**
	 * 
	 * Add paid holiday information for all of employees has koushin button
	 * 
	 */
	public function allStore(Request $request)
	{
		$employees = Employee::all();

		$try = true;
		$isApartKoushin = true;
		try {
			foreach($employees as $employee)
			{
				if($employee->hasKoushin())
				{
					$isApartKoushin = false;
					$try = $try && $this->addPaidHolidayInformation($employee);
					if(!$try) break;
				}
				else {
					$isApartKoushin = $isApartKoushin && true;
				}
			}
			if($try && !$isApartKoushin) {
				return back()->with('success','保存しました');
			}
			else 
			{
				return back()->withErrors('入力に誤りがあります');
			}
		} catch (\Exception $e) {
			return back()->withErrors('入力に誤りがあります');
		}
	}
}
<?php
namespace App\Http\Controllers;

use App\WorkLocation;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Services\BreadCrumb;
use App\Services\BreadCrumbService;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Http\Controllers\Reusables\GetCheckListTrait;

class CheckListController extends Controller
{
    use GetCheckListTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('choose:singular');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request           $request
     * @param boolean           $refresh_hitory
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $refresh_hitory = false)
    {
        // Get the breadcrumb service
        $breadcrumbs_service = resolve(BreadCrumbService::class);

        if ($refresh_hitory == true) {
            session()->forget('checklist_search_history');

            // Reset the breadcumbs if reset the search history
            $breadcrumbs_service->resetToAttendanceRoot();
        }

        // These are for the breadcumbs feature
        $breadcrumbs_service->goOneLevelDeeperIfDoesNotExistOrJustGoToThisNodeIfExist(BreadCrumb::CHECKLIST, $request->route()->getName(), $request->route());

        $results = $this->search();

        $currentMonth = $results['currentMonth'];
        $currentYear = $results['currentYear'];

        $current_work_location = session('current_work_location');
        $company = \Auth::user()->company;
        $departments = $company->departments->pluck('name', 'id')->toArray();

        $department_work_locations = $departments;
        if ($current_work_location !== 'all') {
            $work_location = WorkLocation::with('employees')->find($current_work_location);
            $department_work_locations = $work_location->activatingDepartments()->pluck('name', 'id')->toArray();
        }

        Javascript::put([
            'checklists' => $results['checklistsJson'],
            'checklistsHistory' => $results['checklistsHistory'],
            'departments' => $departments,
            'departmentWorkLocations' => $department_work_locations,
        ]);

        // send some presentation data
        $this->sendEmployeeNamesToJavascriptSide($request);

        return view('checklist.list', compact('currentMonth', 'currentYear'));
    }

    /**
     * Send name list of all employees of this company to the javascript side for the autocomplete feature in the search box
     *
     * @param Request       $request
     * @return void
     */
    private function sendEmployeeNamesToJavascriptSide(Request $request)
    {
        $employee_names = $request->user()->company->employees->map(function($employee) {
            return ['name' => $employee->last_name . $employee->first_name];
        });

        Javascript::put([
            'employee_names' => $employee_names,
        ]);
    }
}
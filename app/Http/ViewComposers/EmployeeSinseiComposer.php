<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\Employee;
use Carbon\Carbon;
use Constants;
use App\Http\Controllers\Reusables\BusinessMonthTrait;

class EmployeeSinseiComposer
{
    use BusinessMonthTrait;
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {   
        $employee = session('sinsei_user');

        $business_month_need = null;
        $sumtotal = null;
        $can_view_payment_page = $employee->maebarai_enable == 1 && $employee->workLocation->company->maebarai_enable == 1;
        $can_view_maebarai_page = $employee->maebarai_enable == 1 && $employee->workLocation->company->maebarai_enable == 1 && isset($employee->maebarai_bank_code);

        if ($employee->subordinates->isNotEmpty()) {
            $business_month_need = session('business_month_need');

            if(!$business_month_need){
                $business_month_need = $this->calculateTheBusinessMonth($employee);
            }

            $date =$this->getStartDateAndEndDateFromBusinessMonth($employee, $business_month_need);
            $start_date = $date[0]->toDateString();
            $end_date = $date[1]->toDateString();

            $sumtotal = $employee->numberOfSubordinatesRequest($start_date, $end_date);
        }

        $view->with([
            'business_month'    =>  isset($business_month_need) ? $business_month_need->format('Y-m') : null,
            'sumtotal'       =>  isset($sumtotal) ? $sumtotal : null,
            'can_view_payment_page'       =>  $can_view_payment_page,
            'can_view_maebarai_page'       =>  $can_view_maebarai_page,
        ]);
    }
}
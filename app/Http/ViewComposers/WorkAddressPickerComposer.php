<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as Javascript;
use App\WorkAddress;


class WorkAddressPickerComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $current_work_location = session()->get('current_work_location');

        if ($current_work_location) {
            
            if ($current_work_location == 'all') {
                $work_addresses = WorkAddress::with('workLocation')->orderBy('work_location_id')->orderBy('furigana')->get();
            } else if (is_array($current_work_location)) {
                $work_addresses = WorkAddress::with('workLocation')->whereIn('work_location_id', $current_work_location)->orderBy('work_location_id')->orderBy('furigana')->get();
            } else {
                $work_addresses = WorkAddress::with('workLocation')->where('work_location_id', $current_work_location)->orderBy('furigana')->get();
            }
            
            $work_addresses = $work_addresses->map(function($work_address) {
                return [
                    'presentation_id' => $work_address->presentation_id,
                    'name' => $work_address->name,
                    'enable' => $work_address->workLocation->enable && $work_address->enable,
                    'id' => $work_address->id,
                ];
            });

            $view->with([
                'work_address_picker_list'   => $work_addresses,
            ]);
        }
    }
}
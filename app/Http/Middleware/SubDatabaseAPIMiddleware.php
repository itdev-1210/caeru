<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;
use Illuminate\Support\Facades\Hash;

class SubDatabaseAPIMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company_code = $request->input('company_code');
        if (DB::table('companies')->where('company_code', 'like binary', $company_code)->exists()) {
            if ($request->route()->named('login') || $request->route()->named('connection'))
                return $next($request);

            $auth_tokens = DB::table('api_auth_tokens')->where('company_code', $company_code)->where('device_id', $request->tablet_id)->get();
            if (isset($auth_tokens) && !empty($auth_tokens)) {
                foreach ($auth_tokens as $auth_token) {
                    if (Hash::check($request->token, $auth_token->remember_token)) {
                        $this->connectToSubDatabase($company_code);
                        return $next($request);
                    }
                }
            }
            return response()->json(['token' => ['認証情報が無効です。']], 403);
        }
        return response()->json(['company_code' => ['会社コードは無効です。']], 403);
    }

    /**
     * Connect to the sub database with this company_code.
     *
     * @param  string  $company_code
     * @return void
     */
    protected function connectToSubDatabase($company_code)
    {
        Config::set('database.default', 'sub');
        Config::set('database.connections.sub.database', config('database.connections.sub.prefix') . $company_code);
        Config::set('database.connections.sub.prefix', config('database.connections.sub.prefix') . $company_code . '_');

        DB::reconnect('sub');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Exceptions\ApproveAuthenticateException;

class ApproveAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $allow_manager = null)
    {
        $sinsei_user = session('sinsei_user');
        if ($allow_manager === "allow_manager") {
            if ((isset($sinsei_user) && $sinsei_user->subordinates->isNotEmpty()) || Auth::check()) {
                return $next($request);
            }
            throw new ApproveAuthenticateException();

        } else if (isset($sinsei_user) && $sinsei_user->subordinates->isNotEmpty()){
            return $next($request);
        }
        throw new ApproveAuthenticateException();
    }
}

<?php

namespace App\Http\Middleware;

use Caeru;
use Closure;
use Config;
use DB;
use App\Exceptions\UndefinedCompanyException;
use App\Exceptions\UnauthenticatedCompanyException;

class SubDatabaseConfiguration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company_code = $request->route('company_code');

        if ($this->checkCompany($company_code)) {

            $this->connectToSubDatabase($company_code);

            $prefix = $request->route()->getPrefix();
            // Only if the user is already authenticated, will the 'company_code' route parameter be removed.
            // These 3 conditions are:
            //      * If the route is sinsei's side (not caeru's side), and the user has already logged in
            //      * If the route is caeru's side (not sinsei's side), and the user has already logged in
            //      * Third case is a little bit special, among caeru's use cases, there is a case where we need to send a request to a route of sinsei's side
            //          this route must not be 'ss_login'.
            if (($prefix && $prefix === '/{company_code}/sinsei' && session('sinsei_user')) ||
                ($prefix == null && $request->user() !== null) ||
                ($request->user() !== null && $prefix != null && $request->route()->getName() !== 'ss_login')) {

                $request->route()->forgetParameter('company_code');
            }

            return $next($request);
        }

    }

    /**
     * Check if there is a company with this company code in the caeru_main database.
     *
     * @param  string  $company_code
     * @return boolean
     */
    protected function checkCompany($company_code)
    {
        $current_company_code = session('current_company_code');

        if (!DB::table('companies')->where('company_code', $company_code)->exists()) {
            throw new UndefinedCompanyException();
        } elseif ($current_company_code && $current_company_code != $company_code) {
            // throw new UnauthenticatedCompanyException();
            session()->flush();

            return redirect()->route('to_login', ['company_code' => $company_code]);

        } else
            return true;
    }

    /**
     * Connect to the sub database with this company_code.
     *
     * @param  string  $company_code
     * @return void
     */
    protected function connectToSubDatabase($company_code)
    {
        Config::set('database.default', 'sub');

        Config::set('database.connections.sub.database', config('database.connections.sub.prefix') . $company_code);
        Config::set('database.connections.sub.prefix', config('database.connections.sub.prefix') . $company_code . '_');

        DB::reconnect('sub');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

class SpshiftAPIMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->input('key');
        $default_key = '$2y$10$FsZ8yd1CuBRoBKAwe1vnOefE0ujM211ckyUd3x852o1j6aZSZKAsO';

        if (Hash::check($key, $default_key)) {
            return $next($request);
        }

        return response()->json(['auth_token' => ['認証情報が無効です。']], 403);
    }
}

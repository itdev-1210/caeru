<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;
use Illuminate\Support\Facades\Hash;
use App\Employee;
use App\Company;

class SubDatabaseEmployeePhoneAPIMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company_code = $request->input('company_code');
        if (DB::table('companies')->where('company_code', 'like binary', $company_code)->exists()) {

            $this->connectToSubDatabase($company_code);

            $company = Company::first();

            if (!$company->use_address_system) return response()->json(['error' => ["The company isn't using work address system"]], 403);

            if ($request->route()->named('employee_phone_login') || $request->route()->named('employee_phone_check_company'))
                return $next($request);

            $employee = Employee::where('device_id', $request->device_id)->first();
            if (isset($employee) && !empty($employee)) {
                if (Hash::check($request->auth, $employee->remember_token)) {
                    return $next($request);
                }
            }
            return response()->json(['auth' => ['認証情報が無効です。']], 403);
        }
        return response()->json(['company_code' => ['会社IDは無効です。']], 403);
    }

    /**
     * Connect to the sub database with this company_code.
     *
     * @param  string  $company_code
     * @return void
     */
    protected function connectToSubDatabase($company_code)
    {
        Config::set('database.default', 'sub');
        Config::set('database.connections.sub.database', config('database.connections.sub.prefix') . $company_code);
        Config::set('database.connections.sub.prefix', config('database.connections.sub.prefix') . $company_code . '_');

        DB::reconnect('sub');
    }
}

<?php

namespace App\Policies;

use App\Manager;
use App\ManagerAuthority;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuthorityPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the current manager can change managers information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeManagerInformation(Manager $manager)
    {
        return $manager->super;
    }

    /**
     * Determine if the current manager can change company information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeCompanyInformation(Manager $manager)
    {
        return $manager->managerAuthority->company_information == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view company information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewCompanyInformation(Manager $manager)
    {
        return $manager->managerAuthority->company_information != ManagerAuthority::NOTHING;
    }


    /**
     * Determine if the current manager can change paid holiday information detail
     *
     * @param Manager $manager
     * @return boolean
     * 
     */
    public function changePaidHolidayInformationDetail(Manager $manager)
    {
        return $manager->managerAuthority->work_data_paid_holiday_detail == ManagerAuthority::CHANGE;
    }


    /**
     * Determine if the current manager can view paid holiday information detail
     *
     * @param Manager $manager
     * @return boolean
     * 
     */
    public function viewPaidHolidayInformationDetail(Manager $manager)
    {
        return $manager->managerAuthority->work_data_paid_holiday_detail != ManagerAuthority::NOTHING;
    }


    /**
     * Determine if the current manager can change paid holiday information list 
     *
     * @param Manager $manager
     * @return boolean
     * 
     */
    public function changePaidHolidayInformationManagement(Manager $manager)
    {
        return $manager->managerAuthority->work_data_paid_holiday_management == ManagerAuthority::CHANGE;
    }


    /**
     * Determine if the current manager can view paid holiday information list
     *
     * @param Manager $manager
     * @return boolean
     * 
     */
    public function viewPaidHolidayInformationManagement(Manager $manager)
    {
        return $manager->managerAuthority->work_data_paid_holiday_management != ManagerAuthority::NOTHING;
    }

    /**
     * Determine if the current manager can change/add work location
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeWorkLocationInformation(Manager $manager)
    {
        return $manager->managerAuthority->work_location_information == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view work location
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewWorkLocationInformation(Manager $manager)
    {
        return $manager->managerAuthority->work_location_information != ManagerAuthority::NOTHING;
    }

    /**
     * Determine if the current manager can change/add work address
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeWorkAddressInformation(Manager $manager)
    {
        return $manager->managerAuthority->work_address_information == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view work address
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewWorkAddressInformation(Manager $manager)
    {
        return $manager->managerAuthority->work_address_information != ManagerAuthority::NOTHING && $manager->company->use_address_system == true;
    }

    /**
     * Determine if the current manager can change this work address's work location at that moment
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeWorkAddressWorkLocationInformation(Manager $manager)
    {
        return ($this->changeWorkAddressInformation($manager)) && !is_numeric(session('current_work_location'));
    }

    /**
     * Determine if the current manager can change/add employee's basic information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeEmployeeBasicInformation(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->employee_basic_information == ManagerAuthority::CHANGE) :
        ($manager->managerAuthority->employee_basic_information == ManagerAuthority::CHANGE &&
            ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
        );
    }

    /**
     * Determine if the current manager can view employee's basic information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewEmployeeBasicInformation(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->employee_basic_information != ManagerAuthority::NOTHING) :
        ($manager->managerAuthority->employee_basic_information != ManagerAuthority::NOTHING &&
            ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
        );
    }

    /**
     * Determine if the current manager can change employee's work location at that moment
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeEmployeeWorkLocationInformation(Manager $manager)
    {
        return ($this->changeEmployeeBasicInformation($manager)) && !is_numeric(session('current_work_location'));
    }

    /**
     * Determine if the current manager can change/add employee's work information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeEmployeeWorkInformation(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->employee_work_information == ManagerAuthority::CHANGE) :
        ($manager->managerAuthority->employee_work_information == ManagerAuthority::CHANGE &&
            ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
        );
    }

    /**
     * Determine if the current manager can view employee's work information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewEmployeeWorkInformation(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->employee_work_information != ManagerAuthority::NOTHING) :
        ($manager->managerAuthority->employee_work_information != ManagerAuthority::NOTHING &&
            ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
        );
    }

    /**
     * Determine if the current manager can change/add employee's maebarai information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeEmployeeMaebaraiInformation(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->employee_maebarai_information == ManagerAuthority::CHANGE) :
        ($manager->managerAuthority->employee_maebarai_information == ManagerAuthority::CHANGE &&
            ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
        );
    }

    /**
     * Determine if the current manager can view employee's maebarai information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewEmployeeMaebaraiInformation(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->employee_maebarai_information != ManagerAuthority::NOTHING) :
        ($manager->managerAuthority->employee_maebarai_information != ManagerAuthority::NOTHING &&
            ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
        );
    }

    /**
     * Determine if the current manager can see the tab employee on the navigation
     *
     * @param Manager $manager
     * @return boolean
     */
    public function seeEmployeeTabInNavigationBar(Manager $manager)
    {
        return (($manager->managerAuthority->employee_basic_information != ManagerAuthority::NOTHING) ||
            ($manager->managerAuthority->employee_work_information != ManagerAuthority::NOTHING));
    }

    /**
     * Determine if the current manager can change calendar information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeCalendarInformation(Manager $manager)
    {
        return $manager->managerAuthority->calendar_setting == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view calendar information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewCalendarInformation(Manager $manager)
    {
        return $manager->managerAuthority->calendar_setting != ManagerAuthority::NOTHING;
    }

    /**
     * Determine if the current manager can change setting information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeSettingInformation(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->setting == ManagerAuthority::CHANGE) :
        ($manager->managerAuthority->setting == ManagerAuthority::CHANGE &&
            ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
        );
    }

    /**
     * Determine if the current manager can view setting information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewSettingInformation(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->setting != ManagerAuthority::NOTHING) :
        ($manager->managerAuthority->setting != ManagerAuthority::NOTHING &&
            ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
        );
    }

    /**
     * Determine if the current manager can change statuses setting information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeStatusesSettingInformation(Manager $manager)
    {
        return $manager->managerAuthority->statuses_setting == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view statuses setting information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewStatusesSettingInformation(Manager $manager)
    {
        return $manager->managerAuthority->statuses_setting != ManagerAuthority::NOTHING;
    }

    /**
     * Determine if the current manager can change statuses setting information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeDepartmentTypeSettingInformation(Manager $manager)
    {
        return $manager->managerAuthority->department_type_setting == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view statuses setting information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewDepartmentTypeSettingInformation(Manager $manager)
    {
        return $manager->managerAuthority->department_type_setting != ManagerAuthority::NOTHING;
    }

    /**
     * Determine if the current manager can change statuses setting information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeWorkTimeSettingInformation(Manager $manager)
    {
        return $manager->managerAuthority->worktime_setting == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view statuses setting information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewWorkTimeSettingInformation(Manager $manager)
    {
        return $manager->managerAuthority->worktime_setting != ManagerAuthority::NOTHING;
    }

    /**
     * Determine if the current manager can view option item's page
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewOptionItemInformation(Manager $manager)
    {
        return ($manager->managerAuthority->worktime_setting != ManagerAuthority::NOTHING || $manager->managerAuthority->department_type_setting != ManagerAuthority::NOTHING || $manager->managerAuthority->statuses_setting != ManagerAuthority::NOTHING);
    }

    /**
     * Determine if the current manager can view attendance_management (or employee_attendance, or attendance table) page of the attendance tab
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewAttendanceManagementPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_management == ManagerAuthority::BROWSE;
    }

    /**
     * Determine if the current manager can view attendance_advance_search page of the attendance tab
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewAttendanceAdvanceSearchPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_search == ManagerAuthority::BROWSE;
    }

    /**
     * Determine if the current manager can view work data calculation or month summary page of the attendance tab
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewAttendanceMonthSummaryPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_calculation == ManagerAuthority::BROWSE;
    }

    /**
     * Determine if the current manager can view employee working month page of the attendance tab
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewAttendanceEmployeeWorkingMonthPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_personal_detail == ManagerAuthority::BROWSE;
    }

    /**
     * Determine if the current manager can view employee_working_day page of the attendance part
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewAttendanceEmployeeWorkingDayPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_detail == ManagerAuthority::BROWSE;
    }

    /**
     * Determine if the current manager can change employee's working data
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeAttendanceData(Manager $manager)
    {
        return $manager->managerAuthority->work_data_modify == true;
    }

    /**
     * Determine if the current manager can approve employee's requests (申請)
     *
     * @param Manager $manager
     * @return boolean
     */
    public function approveWorkDataModifyRequest(Manager $manager)
    {
        return $manager->managerAuthority->work_data_modify_request_confirm == true;
    }

    /**
     * Determine if the current manager can view WorkAddress attendance page
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewWorkAddressAttendancePage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_addresses == ManagerAuthority::BROWSE;
    }

    /**
     * Determine if the current manager can change WorkAddress working month page
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeAttendanceWorkAddressWorkingMonthPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_address_detail == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view WorkAddress working month page
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewAttendanceWorkAddressWorkingMonthPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_address_detail != ManagerAuthority::NOTHING;
    }

    /**
     * Determine if the current manager can change WorkAddress working day page
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeAttendanceWorkAddressWorkingDayPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_address_work_detail == ManagerAuthority::CHANGE;
    }

    /**
     * Determine if the current manager can view WorkAddress working day page
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewAttendanceWorkAddressWorkingDayPage(Manager $manager)
    {
        return $manager->managerAuthority->work_data_address_work_detail != ManagerAuthority::NOTHING;
    }

    /**
     * Determine if the current manager can conclude level one on employee's working information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function concludeWorkingInforLevelOne(Manager $manager)
    {
        return $manager->managerAuthority->approval_level_one == true;
    }

    /**
     * Determine if the current manager can conclude level two on employee's working information
     *
     * @param Manager $manager
     * @return boolean
     */
    public function concludeWorkingInforLevelTwo(Manager $manager)
    {
        return $manager->managerAuthority->approval_level_two == true;
    }

    /**
     * An utility function to determine if all the work locations in the list is in the manager's authority
     *
     * @param Manager   $manager
     * @param array     $work_locations
     * @return boolean
     */
    private function containsAllWorklocations($manager, $work_locations)
    {
        return collect($work_locations)->every(function($work_location) use ($manager) {
            return $manager->workLocations->contains('id', $work_location);
        });
    }

    /**
     * Determine if the current manager can view maebarai page of the maebarai
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewMaebaraiPage(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->maebarai_setting != ManagerAuthority::NOTHING || $manager->managerAuthority->maebarai_information == ManagerAuthority::BROWSE) :
            (($manager->managerAuthority->maebarai_setting != ManagerAuthority::NOTHING || $manager->managerAuthority->maebarai_information == ManagerAuthority::BROWSE) &&
                ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
            );
    }

    /**
     * Determine if the current manager can view maebarai setting page of the maebarai
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewMaebaraiSettingPage(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->maebarai_setting != ManagerAuthority::NOTHING) :
            ($manager->managerAuthority->maebarai_setting != ManagerAuthority::NOTHING &&
                ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
            );
    }

    /**
     * Determine if the current manager can change maebarai setting page of the maebarai
     *
     * @param Manager $manager
     * @return boolean
     */
    public function changeMaebaraiSettingPage(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->maebarai_setting == ManagerAuthority::CHANGE) :
            ($manager->managerAuthority->maebarai_setting == ManagerAuthority::CHANGE &&
                ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
            );
    }

    /**
     * Determine if the current manager can view maebarai information page of the maebarai
     *
     * @param Manager $manager
     * @return boolean
     */
    public function viewMaebaraiInformationPage(Manager $manager)
    {
        return ($manager->company_wide_authority) ? ($manager->managerAuthority->maebarai_information == ManagerAuthority::BROWSE) :
            ($manager->managerAuthority->maebarai_information == ManagerAuthority::BROWSE &&
                ($manager->workLocations->contains('id', session('current_work_location')) || $this->containsAllWorklocations($manager, session('current_work_location')))
            );
    }
}

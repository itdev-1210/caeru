<?php

namespace App;

use DB;
use App\Reusables\BelongsToWorkLocationTrait;

class ChecklistItem extends Model
{
    use BelongsToWorkLocationTrait;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['error_name', 'employee_name', 'employee_presentation_id'];

    /**
     * Constants for types of checklist item
     * 100 打刻エラー
     * 200 要チェックリスト
     */
    const TIMESTAMP_ERROR = 100;
    const CONFIRM_NEEDED = 200;

    /**
     * Constants for types of timestamp errors
     * 101 出勤
     * 102 退勤
     * 103 外出・戻り
     * 104 退勤
     * 105 外出・戻り
     */
    const START_WORK_ERROR      = 101;
    const END_WORK_ERROR        = 102;
    const GO_OUT_RETURN_ERROR   = 103;
    const FORGOT_END_WORK_ERROR = 104;
    const FORGOT_RETURN_ERROR   = 105;

    /**
     * Constants for types of situation where confirmation is needed
     * 201 遅刻.早退
     * 202 時間外
     * 203 形態
     * 204 休憩・外出
     * 205 休出
     * 206 欠勤
     * 207 予定外
     * 208 休出
     */
    const LATE_OR_LEAVE_EARLY_TYPE              = 201;
    const OFF_SCHEDULE_TIME                     = 202;
    const STATUS_MISTAKEN                       = 203;
    const OVERLIMIT_BREAK_TIME                  = 204;
    const WORK_WITHOUT_SCHEDULE                 = 205;
    const HAVE_SCHEDULE_BUT_OFFLINE             = 206;
    const DIFFERENCE_FROM_SCHEDULE              = 207;
    const EMPTY_EMPLOYEE_WORKING_INFORMATION    = 208;

    /**
     * Return the list of error names in japanese. This function can be use out of object context.
     *
     * @return array
     */
    public static function errorNames()
    {
        return [
            self::START_WORK_ERROR                      => '出勤',
            self::END_WORK_ERROR                        => '退勤',
            self::GO_OUT_RETURN_ERROR                   => '外出・戻り',
            self::FORGOT_END_WORK_ERROR                 => '退勤',
            self::FORGOT_RETURN_ERROR                   => '外出・戻り',
            self::LATE_OR_LEAVE_EARLY_TYPE              => '遅刻・早退',
            self::OFF_SCHEDULE_TIME                     => '時間外',
            self::STATUS_MISTAKEN                       => '形態',
            self::OVERLIMIT_BREAK_TIME                  => '休憩・外出',
            self::WORK_WITHOUT_SCHEDULE                 => '休出',
            self::HAVE_SCHEDULE_BUT_OFFLINE             => '欠勤',
            self::DIFFERENCE_FROM_SCHEDULE              => '予定外',
            self::EMPTY_EMPLOYEE_WORKING_INFORMATION    => '休出',
        ];
    }

    /**
     * Get the employee working information instance of this checklist item
     */
    public function employeeWorkingInformation()
    {
        return $this->belongsTo(EmployeeWorkingInformation::class);
    }

    /**
     * Get the employee of this checklist item
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * A fake attribute to access the name of the employee to who this checklist item relates.
     */
    public function getEmployeeNameAttribute()
    {
        return $this->employee->fullName();
    }

    /**
     * A fake attribute to access the presentation_id of the employee to who this checklist item relates.
     */
    public function getEmployeePresentationIdAttribute()
    {
        return $this->employee->presentation_id;
    }

    /**
     * Get check list base with error type ( 打刻エラーと要チェックリスト)
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeErrList($query, $errtype=[]) {
        if(!count($errtype)) 
        {
            return $query;  
        }
        return $query
        ->whereIn('checklist_items.error_type', $errtype);
    }

    /**
     * Get checklist items of a specific type
     *
     * @param Builder $query
     * @param int   $item_type:  one of the item type defined in the constants section
     * @return Builder
     */
    public function scopeItemType($query, $item_type)
    {
        return $query->where('item_type', $item_type);
    }

    /**
     * get checklist base item type
     *
     * @param Builder $query
     * @param int $itemtype
     *
     * @return Builder
     */
    public function scopeItemTypeWithDate($query, $itemtype, $beginDate, $endDate)
    {
        return $query
        ->where('item_type',$itemtype)
        ->checklistDay($beginDate, $endDate);
    }

    /**
     * Get checklist base on selected month
     *
     * @param date $beginDate
     * @param date $endDate
     *
     * @return Builder
     */
    public function scopeChecklistDay($query, $beginDate, $endDate)
    {
        return $query
        ->whereBetween('checklist_items.date',[$beginDate,$endDate]);
    }

    /**
     * Accessor of the fake attribute: error name
     */
    public function getErrorNameAttribute()
    {
        return self::errorNames()[$this->error_type];
    }

    /**
     * Scope a query to search checklist. Condition is employeeName
     *
     * @param Builder $query
     * @param string $employeeName The employee name
     *
     * @return Builder
     */
    public function scopeSearchEmployeeName($query,$employeeName=null)
    {
        if(empty($employeeName)) {
            return $query;
        }
        $query = $query
                // ->whereRaw('concat('.\DB::getTablePrefix().'employees.first_name, '.\DB::getTablePrefix().'employees.last_name) like "%?%"',[$employeeName]);
                ->where(function($q) use ($employeeName){
                    $q->where('employees.last_name', 'like', '%'. $employeeName.'%')
                    ->orWhere('employees.first_name', 'like', '%'. $employeeName.'%');
                });
    }

    /**
     * Scope a query to search checklist. Conditon is employeeId
     *
     * @param Builder $query
     * @param string $employeeId The employee identifier
     *
     * @return Builder
     */
    public function scopeSearchEmployeeId($query,$employeeId=null){
        if(empty($employeeId)) {
            return $query;
        }
        return $query
         ->where('employees.presentation_id', 'like','%' . $employeeId . '%');
    }

    /**
     * scope aquery to get a all checklist with worklocation
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeCheckList($query, $item_type, $work_location_id, $beginDate,$endDate, $errtype=[], $employeeId=null, $employeeName=null)
    {
        return $query
        ->select(['checklist_items.date', 'checklist_items.error_type', 'checklist_items.employee_id', 'employees.presentation_id', 'employees.first_name', 'employees.last_name'])
        ->join('employees', function($join) {
            $join->on('employees.id', '=', 'checklist_items.employee_id');
        })
        ->join('work_locations', function($join) {
            $join->on('work_locations.id', '=', 'employees.work_location_id');
        })
        ->join('settings', function($join) {
            $join->on('settings.work_location_id', '=', 'work_locations.id')
            ->orWhereNull('settings.work_location_id');
        })
        ->where(function($query) {
            $query
            ->where(function($q) {
                $q
                ->whereNull('settings.work_location_id')
                ->whereNotIn('work_locations.id', function($q) {
                    $q
                    ->select('work_location_id')
                    ->from('settings')
                    ->whereNotNull('work_location_id');
                });
            })
            ->orWhere(function($q) {
                $q
                ->whereNotNull('settings.work_location_id')
                ->whereIn('work_locations.id', function($q) {
                    $q
                    ->select('work_location_id')
                    ->from('settings')
                    ->whereNotNull('work_location_id');
                });
            });
        })
        ->whereBetween('checklist_items.date',[$beginDate,$endDate])
        ->where('item_type', $item_type)
        ->workLocations($work_location_id)
        ->errList($errtype)
        ->searchEmployeeId($employeeId)
        ->searchEmployeeName($employeeName);
    }

    /**
     * get checklist base on worklocation
     *
     * @param Builder $query
     * @param string $work_location_id
     *
     * @return Builder
     */
    public function scopeWorkLocations($query, $work_location_id){
        if($work_location_id == 'all') {
            return $query;
        } else {
            return $query
            ->whereIn('work_locations.id',[$work_location_id]);
        }
    }
}

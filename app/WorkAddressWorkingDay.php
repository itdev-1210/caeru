<?php

namespace App;

use Constants;

class WorkAddressWorkingDay extends Model
{

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['date_upper_limit', 'day_of_the_upper_limit'];

    /**
     * Get the work address of this working day instance
     */
    public function workAddress()
    {
        return $this->belongsTo(WorkAddress::class);
    }


    /**
     * Get all the working information instances of this working day instance
     */
    public function workAddressWorkingInformations()
    {
        return $this->hasMany(WorkAddressWorkingInformation::class);
    }

    /**
     * Get all the candidate information instances of this working day instance
     */
    public function workAddressCandidateInformations()
    {
        return $this->hasMany(WorkAddressCandidateInformation::class);
    }

    // Some Virtual utility attributes
    /**
     * Base on the date_separate setting of the company to determine the uppler limit of date attributes of this working day
     *
     * @return string       a date time string with format 'Y-m-d H:i:s'
     */
    public function getDateUpperLimitAttribute()
    {
        $company = $this->workAddress->workLocation->company;

        if ($company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {
            return $this->date . ' ' . $company->date_separate_time . ':00';
        } else {
            $day = Carbon::createFromFormat('Y-m-d', $this->date);
            $day->subDay();
            return $day->format('Y-m-d') . ' ' . $company->date_separate_time . ':00';
        }
    }

    /**
     * Get the day of the date_upper_limit above
     *
     * @return string    a date string with format 'Y-m-d'
     */
    public function getDayOfTheUpperLimitAttribute()
    {
        if ($this->workAddress->workLocation->company->date_separate_type === Company::APPLY_TO_THE_DAY_BEFORE) {
            return $this->date;
        } else {
            return Carbon::createFromFormat('Y-m-d', $this->date)->subDay()->format('Y-m-d');
        }
    }

    /**
     * Make a compacted version of EmployeeWorkingInformation of this EmployeeWorkingDay. This compacted data is used in employee_working_month page and
     * employee_working_information_management page
     *
     * @return array
     */
    public function getCompactedWorkingInformations()
    {
        $working_infos = $this->workAddressWorkingInformations->sortBy('schedule_start_work_time');

        $compacted_data = $working_infos->map(function($working_info) {

            return [
                'start_work_time' => $working_info->schedule_start_work_time,
                'end_work_time' => $working_info->schedule_end_work_time,
                'candidate_number' => $working_info->candidate_number,
                'employees' => $working_info->workAddressWorkingEmployees->sortBy('id')->map( function($working_employee) {
                    return [
                        'id' => $working_employee->id,
                        'working_confirm' => $working_employee->working_confirm,
                        'name' => $working_employee->employee->fullName() . '(' . Constants::genders()[$working_employee->employee->gender] . ')',
                        'disable_toggle_working_confirm' => $working_employee->timestamps_exist_or_not == true ||
                                                        $working_employee->planned_work_status_id != null ||
                                                        $working_employee->planned_rest_status_id != null,
                    ];
                }),
                'start_work_time_modified' => $working_info->schedule_start_work_time_modified == true,
                'end_work_time_modified' => $working_info->schedule_end_work_time_modified == true,
                'candidate_number_modified' => $working_info->candidate_number_modified == true,
                'candidate_number_mismatched' => $working_info->actualWorkingEmployeesMismatchOrNot(),
            ];
        });

        return $compacted_data->values();
    }
}

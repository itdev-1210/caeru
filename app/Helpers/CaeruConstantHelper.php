<?php

namespace App\Helpers;

use Config;

class CaeruConstantHelper
{
    /**
     * List of all genders
     */
    public static function genders()
    {
        return [
            Config::get('constants.male')           =>      '男',
            Config::get('constants.female')         =>      '女',
        ];
    }

    /**
     * List of all maebarai settings
     */
    public static function maebarai_settings()
    {
        return [
            Config::get('constants.maebarai_enable')          =>      '利用する',
            Config::get('constants.maebarai_disable')         =>      '利用しない',
        ];
    }

    /**
     * List of all payment status
     */
    public static function payment_status()
    {
        return [
            Config::get('constants.direct')           =>      '直接',
            Config::get('constants.indirect')         =>      '間接',
        ];
    }

    /**
     * List of all payment erorrs
     */
    public static function payment_errors()
    {
        return [
            '00000000'          =>  '',
            'AZ_C_CO_014001'    =>  'サーバーエラー',
            '82109999'          =>  'ホスト利用時間外エラー',
            'AZ_AFR_001001'     =>  '取引IDエラー',
            'AZ_AFR_001002'     =>  '依頼人名エラー',
            'AZ_AFR_001003'     =>  '受取人名エラー',
            'AZ_AFR_001004'     =>  '依頼人名エラー',
            'AZ_AFR_001005'     =>  '受取人名エラー',
            'AZ_AFR_001006'     =>  '仕向口座エラー',
            'AZ_AFR_001007'     =>  '振込先口座エラー',
            'AZ_AFR_001008'     =>  '振込最小可能金額エラー',
            'AZ_AFR_001009'     =>  '振込最大可能金額エラー',
            'AZ_AFR_001010'     =>  '振込元・振込先同一エラー',
            'AZ_AFR_001011'     =>  '仕向預金種目エラー',
            'AZ_AFR_001012'     =>  '自行宛振込振込先預金種目エラー',
            'AZ_AFR_001013'     =>  '振込先預金種目エラー',
            'AZ_AFR_001014'     =>  '種別コードエラー',
            'AZ_AFR_001015'     =>  '取引ID重複エラー',
            'AZ_AFR_001016'     =>  '提携先コードエラー',
            'AZ_AFR_001017'     =>  '契約者エラー',
            'AZ_AFR_001018'     =>  '仕向銀行コードエラー',
            'AZ_AFR_001019'     =>  '仕向支店コードエラー',
            'AZ_AFR_001020'     =>  '振込先銀行コードエラー',
            'AZ_AFR_001021'     =>  '振込先支店コードエラー',
            'AZ_AFR_001022'     =>  '営業日エラー',
            'AZ_AFR_001023'     =>  '取引ID重複エラー',
            'AZ_AFR_001024'     =>  '取引ID重複エラー',
            'AZ_C_CO_005001'    =>  '入力値エラー',
            'AZ_99990001'       =>  'その他エラー',
            '880101S2002'       =>  '入金限度超過',
            '880101S3001'       =>  '残高不足',
            '880101S5003'       =>  '口座なし',
            '880101S5006'       =>  'サーバーエラー',
            '880101S5011'       =>  '口座なし',
            '880101S5015'       =>  '取引なし',
            '880101S5033'       =>  '取引不能口座',
            '880101S6001'       =>  'サーバーエラー',
            '880101S6002'       =>  '金融機関コードエラー',
            '880101S6006'       =>  '取扱対象外',
            '880101S6014'       =>  'サーバーエラー',
            '880101S7009'       =>  '対外障害中',
            '880101S7043'       =>  'サーバーエラー',
            '880101S8003'       =>  '取引閉塞中',
            '880101S8005'       =>  'メンテナンス中',
        ];
    }

    /**
     * List of all schedule types
     */
    public static function scheduleTypes()
    {
        return [
            Config::get('constants.normal_schedule')               =>      '通常',
            Config::get('constants.monthly_based_schedule')        =>      '1ヶ月単位変形労働',
            Config::get('constants.yearly_based_schedule')         =>      '１年単位変形労働',
            Config::get('constants.flexible_schedule')             =>       'フレックス',
        ];
    }

    /**
     * List of all employment types
     */
    public static function employmentTypes()
    {
        return [
            Config::get('constants.official_employee')          =>      '正社員',
            Config::get('constants.contracted_employee')        =>      '契約社員',
            Config::get('constants.part_time_1_employee')       =>      'パート',
            Config::get('constants.part_time_2_employee')       =>      'アルバイト',
            Config::get('constants.dispatched_employee')        =>      '派遣',
        ];
    }

    /**
     * List of all salary types
     */
    public static function salaryTypes()
    {
        return [
            Config::get('constants.monthly_salary')         =>      '月給',
            Config::get('constants.hourly_salary')          =>      '時給',
            Config::get('constants.daily_salary')           =>      '日給',
        ];
    }

    /**
     * List of all work statuses
     */
    public static function workStatuses()
    {
        return [
            Config::get('constants.working')                =>      '勤務中',
            Config::get('constants.on_vacation')            =>      '休職中',
            Config::get('constants.retired')                =>      '退職済',
        ];
    }

    /**
     * List of all work statuses + 1 more
     * This list is used in employee working information page, and for some reason, it has that last one.
     */
    public static function holidayBonusTypes()
    {
        return [
            Config::get('constants.normal_bonus')                   =>      '一般',
            Config::get('constants.four_days_per_week_bonus')       =>      '週4日勤務',
            Config::get('constants.three_days_per_week_bonus')      =>      '週3日勤務',
            Config::get('constants.two_days_per_week_bonus')        =>      '週2日勤務',
            Config::get('constants.one_day_per_week_bonus')         =>      '週1日勤務',
            Config::get('constants.manually_input_bonus')           =>      '手動入力',
        ];
    }


    /**
     * The list default day of week
     */
    public static function dayOfTheWeek()
    {
        return [
            Config::get('constants.monday')                     =>      '月曜日',
            Config::get('constants.tuesday')                    =>      '火曜日',
            Config::get('constants.wednesday')                  =>      '水曜日',
            Config::get('constants.thursday')                   =>      '木曜日',
            Config::get('constants.friday')                     =>      '金曜日',
            Config::get('constants.saturday')                   =>      '土曜日',
            Config::get('constants.sunday')                     =>      '日曜日',
        ];
    }

    /**
     * The list of gps device types
     */
    public static function gpsDeviceTypes()
    {
        return [
            Config::get('constants.gps_android')        =>  'Android',
            Config::get('constants.gps_iphone')         =>  'Iphone',
            Config::get('constants.gps_docomo_device')  =>  'GPS端末',
        ];
    }
}

<?php
namespace App\Helpers;

use Carbon\Carbon;

/**
 * Some helper functions for blade.
 */
class CaeruBladeHelper
{
    /**
     * An array of Japanese abbreviation for days of the week.
     */
    protected static $day_of_week = [
        Carbon::SUNDAY => '日',
        Carbon::MONDAY => '月',
        Carbon::TUESDAY => '火',
        Carbon::WEDNESDAY => '水',
        Carbon::THURSDAY => '木',
        Carbon::FRIDAY => '金',
        Carbon::SATURDAY => '土',
    ];
    
    /**
     * Convert an number of minutes into an HH:mm string. The hour number can exceed 24 hours.
     * 
     * @param int       $total_minutes
     * @param boolean   $filter_empty  : if this option is true and the number of minutes is zero, show blank (instead of '00:00')
     * @return string
     */
    public static function toHourMinute($total_minutes, $filter_empty = false)
    {
        if ($filter_empty === false || $total_minutes !== 0) {
            return is_numeric($total_minutes) ? str_pad(floor($total_minutes/60), 2, '0', STR_PAD_LEFT) . ':' . str_pad($total_minutes%60, 2, '0', STR_PAD_LEFT) : $total_minutes;
        } else {
            return null;
        }
    }

    /**
     * Convert an day-of-week index into a Japanese abbreviation.
     * 
     * @param int   $day_of_week_index
     * @return string
     */
    public static function toJPDayOfWeek($day_of_week_index)
    {
        return self::$day_of_week[$day_of_week_index];
    }

    /**
     * Convert a full date time string into time format 'H:i' (in other words: hh:mm)
     * 
     * @param string    $date_time_string
     * @return string
     */
    public static function toHms($date_time_string)
    {
        try {
            $carbon_instance = Carbon::createFromFormat('Y-m-d H:i:s', $date_time_string);
        } catch (\Exception $e) {
            return null;
        }

        return $carbon_instance->format('H:i');
    }

    /**
     * Convert a full time string into time format 'H:i' (in other words: hh:mm)
     * 
     * @param string    $time_string
     * @return string
     */
    public static function toHm($time_string)
    {
        try {
            $carbon_instance = Carbon::createFromFormat('H:i:s', $time_string);
        } catch (\Exception $e) {
            return null;
        }

        return $carbon_instance->format('H:i');
    }

    /**
     * Convert a date string into a Japanese date string.
     *
     * @param string    $date_string
     * @return string
     */
    public static function toJpDateString($date_string)
    {
        try {
            $carbon_instance = Carbon::createFromFormat('Y-m-d', $date_string);
        } catch (\Exception $e) {
            return null;
        }

        return $carbon_instance->format('m月d日') . '（' . self::toJPDayOfWeek($carbon_instance->dayOfWeek) . '）';
    }
}
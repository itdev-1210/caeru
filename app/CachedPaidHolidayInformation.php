<?php

namespace App;

class CachedPaidHolidayInformation extends Model
{
    /**
     * Get the paid holiday information instance of this cached instance
     */
    public function paidHolidayInformation()
    {
        return $this->belongsTo(PaidHolidayInformation::class);
    }
}

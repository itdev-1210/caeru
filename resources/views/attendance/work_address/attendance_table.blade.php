@foreach ($work_address_data as $work_address)
    @if (isset($work_address['work_location_label']) && $work_address['work_location_label'] == true)
        <tr class='dynamic_row work_location_label' title="{{ $work_address['work_location_id'] }}">
            <td class="visit_place bg_white white_label" >{{ $work_address['work_location_name'] }}</td>
            <td colspan="5" class="bold_line"></td>
            <td colspan="5" class="bold_line"></td>
            <td colspan="5" class="bold_line"></td>
            <td colspan="5" class="bold_line"></td>
            <td colspan="5" class="bold_line"></td>
            <td colspan="5" class="bold_line"></td>
            <td colspan="5" class="bold_line"></td>
        </tr>
    @else
        @for ($line_index = 0; $line_index < $work_address['max_lines']; $line_index++)
            <tr class='dynamic_row'>
                @if ($line_index == 0)
                    <td class="bg_gray" rowspan="{{ $work_address['max_lines'] }}">
                        @can('view_attendance_work_address_working_month_page')
                            <a href="{{ Caeru::route('work_address_working_month_list', [ 'work_address' => $work_address['id'], 'business_month' => '_', 'day' => $most_left_day ]) }}"><p>{{ $work_address['presentation_id'] }}</p><p>{{ $work_address['name'] }}</p></a>
                        @else
                            <a class="no_underline"><p>{{ $work_address['presentation_id'] }}</p><p>{{ $work_address['name'] }}</p></a>
                        @endcan
                    </td>
                @endif
                @foreach ($work_address['data'] as $day_string => $working_day)
                    @if (count($working_day) == 0 || !isset($working_day[$line_index]))
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        @if ($line_index == 0)
                            <td class="bold_line" rowspan="{{ $work_address['max_lines'] }}">
                                @can('view_attendance_work_address_working_day_page')
                                    <p class="button"><a class="ss_size s_height btn_gray" href="{{ Caeru::route('work_address_working_day_detail', [ 'work_address_id' => $work_address['id'], 'date' => $day_string ]) }}">訪問</a></p>
                                @endcan
                            </td>
                        @endif
                    @else
                        <td class="{{ $working_day[$line_index]['start_work_time_modified'] ? 'bg_revised' : '' }}">{{ CaeruBlade::toHms($working_day[$line_index]['start_work_time']) }}</td>
                        <td class="{{ $working_day[$line_index]['end_work_time_modified'] ? 'bg_revised' : '' }}">{{ CaeruBlade::toHms($working_day[$line_index]['end_work_time']) }}</td>
                        <td class="{{ $working_day[$line_index]['candidate_number_mismatched'] ? 'bg_no_assignment' : ($working_day[$line_index]['candidate_number_modified'] ? 'bg_revised' : '') }}">{{ $working_day[$line_index]['candidate_number'] }}</td>
                        <td>
                            @foreach ($working_day[$line_index]['employees'] as $employee)
                                @if ($employee['working_confirm'] == true)
                                    <p class="right_10 left_10 side_input_block">{{ $employee['name'] }}</p>
                                @endif
                            @endforeach
                        </td>
                        @if ($line_index == 0)
                            <td class="bold_line" rowspan="{{ $work_address['max_lines'] }}">
                                @can('view_attendance_work_address_working_day_page')
                                    <p class="button"><a class="ss_size s_height btn_gray" href="{{ Caeru::route('work_address_working_day_detail', [ 'work_address_id' => $work_address['id'], 'date' => $day_string ]) }}">訪問</a></p>
                                @endcan
                            </td>
                        @endif
                    @endif
                @endforeach
            </tr>
        @endfor
    @endif
@endforeach
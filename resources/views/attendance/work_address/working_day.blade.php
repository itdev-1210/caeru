@extends('layouts.master')

@section('title', '訪問勤務詳細')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/work_address_working_day.js') }}"></script>
@endpush

@section('content')
    <main id="attendance_detail">
		<section class="title">
            @include('layouts.attendance_breadcrumbs')
			<div class="title_wrapper">
				<h1>訪問勤務詳細</h1>
				<div class="worklocation">
					<div class="worklocation_inner">
						<span class="right_30">{{ $work_location_name }}</span><span class="right_10">{{ $work_address->presentation_id }}</span><span>{{ $work_address->name }}</span>
					</div>
				</div>
			</div>
		</section>
		<section class="select_one bg_light_green bottom_10">
			<section class="select_one_inner">
				<section class="right_30 ico_position">
					<a :href="previousDay()"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg')  }}"></a>
				</section>
				<section class="right_10 ll_font" v-cloak>@{{ formatDate(currentDate) }}
					<section class="right_10 ico_position"><a class="modal-open" @click="toggleDatePicker"><img class="ico_ico_arrow" src="{{ asset('images/ico_calendar.svg') }}"></a></section>
				</section>
				<section class=" ico_position"> 
					<a :href="nextDay()"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
                </section>
                <section class="caeru_date_picker_wrapper" v-show="showDatePicker">
                    <calendar class="normal_date_picker" v-bind="datePickerOptions" :editable="false"
                        @change-current-time="datePickerChangeTime"
                        @date-picker-cancel="toggleDatePicker"
                        @day-chose="goToThisDay">
                    </calendar>
                    <div class="modal-overlay" v-cloak @click="toggleDatePicker"></div>
                </section>
            </section>
		</section>
        <section class="select_one2">
			<section class="right_position">
			<a class="red attendance_shift_alert modal-open" v-if="withoutPlanEmployees.length > 0" @click="showWithoutPlanPopUp">予定のない打刻データが@{{withoutPlanEmployees.length}}件あります</a>
				@can('change_attendance_work_address_working_day_page')
					<p class="button"><a class="m_size s_height btn_blue left_10" @click="addWorkingInfo">勤務追加</a></p>
				@endcan
			</section>
		</section>
		<without-plan :list="withoutPlanEmployees" :display="withoutPlannedPopupDisplay" @close-pop-up="hideWithoutPlanPopUp"></without-plan>
		<working-info v-for="info in workingInfos" :key="info['id']" :root="info"
			:success-request="successResult"
			:without-plan-employees="withoutPlanEmployees"
			@save-working-info="saveWorkingInfo($event, info)"
			@delete-working-info="deleteWorkingInfo(info)"
		>
		</working-info>
        <section class="btn">
			<p class="button"><a single-click class="m_size l_height btn_gray l_font" href="{{ Caeru::route('breadcrumb_go_back') }}">戻る</a></p>
		</section>
	</main>
@endsection
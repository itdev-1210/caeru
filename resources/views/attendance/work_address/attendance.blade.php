@extends('layouts.master')

@section('title', '訪問先別勤務情報')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/work_addresses_attendance.js') }}"></script>
	<script defer src="{{ asset('/js/components/work_location_picker.js') }}"></script>
	<script>history.scrollRestoration = "manual"</script>
@endpush

@section('content')
    <main id="attendance">
		<section class="title">
            @include('layouts.attendance_breadcrumbs')
			<div class="title_wrapper">
				<h1>訪問先別勤務情報</h1>
				<div class="worklocation">
					<div class="worklocation_inner">
						<span class="right_10">{{ $current_work_location }}</span>
						@if (isset($picker_list))
                            <p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
                        @endif
					</div>
					@include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'work_address_attendance', 'singular' => false])
				</div>
			</div>
        </section>
        <section id="employee_attendance">
			<work-address-attendance></work-address-attendance>
		</section>
	</main>
@endsection
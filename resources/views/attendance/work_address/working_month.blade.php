@extends('layouts.master')

@section('title', '訪問先別詳細')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/work_address_working_month.js') }}"></script>
    <script defer src="{{ asset('/js/components/work_location_picker.js') }}"></script>
    <script defer src="{{ asset('/js/components/work_address_picker.js') }}"></script>
@endpush

@section('content')
    <main>
        <section class="title">
            @include('layouts.attendance_breadcrumbs')
            <div class="title_wrapper">
                <h1>訪問先別詳細</h1>
                <div class="worklocation">
					<div class="worklocation_inner">
						<span class="right_10">{{ $current_work_location }}</span>
						@if (isset($picker_list))
                            <p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
                        @endif
					</div>
					@include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'work_address_attendance', 'singular' => false])
				</div>
            </div>
        </section>
        <section class="select_one">
            <section class="select_one_inner workAddress">
                <section class="right_10">
                    <span class="right_10">{{ $work_address->presentation_id }}</span>
                    <span class="right_10">{{ $work_address->name }}</span>
                    @if (isset($picker_list))
                        <p class="button"><a class="ss_size s_height btn_gray" @click="open">変更</a></p>
                    @endif
                </section>
                @include('layouts.work_address_picker', ['list' => $work_address_picker_list])
            </section>
        </section>
        <section id="attendance_detail">
            <section class="select_one bg_light_green">
                <section class="select_one_inner">
                    <section class="right_30">
                        <a @click="previousMonth"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}"></a>
                    </section>
                    <div class="selectbox side_input_block right_10">
                        <select class="s_size" v-model.number="selectedYear">
                            <option v-for="year in yearRange" :value="year">@{{ year }}年</option>
                        </select>
                    </div>
                    <div class="selectbox side_input_block right_30">
                        <select class="s_size" v-model.number="selectedMonth">
                            <option v-for="month in monthRange" :value="month">@{{ month }}月度</option>
                        </select>
                    </div>
                    <section>
                        <a @click="nextMonth"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
                    </section>
                </section>
            </section>
            <section class="select_one2">
                <section class="left_position">
                    <div class="left right_10 description_correction description_text">修正</div>
                    <div class="left right_10 description_no_assignment description_text">未配属</div>
                </section>
            </section>
            <section class="have_table_with_fixed_header">
                <working-month></working-month>
            </section>
            <section class="btn">
                <p class="button"><a single-click class="m_size l_height btn_gray l_font" href="{{ Caeru::route('breadcrumb_go_back') }}">一覧に戻る</a></p>
            </section>
        </section>
    </main>
@endsection
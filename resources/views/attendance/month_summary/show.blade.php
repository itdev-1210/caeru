@extends('layouts.master')

@section('title', '勤怠集計')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/month_summary_page.js') }}"></script>
	<script defer src="{{ asset('/js/components/work_location_picker.js') }}"></script>
@endpush

@section('content')
    <main id="attendance_detail">
		<section class="title">
			@include('layouts.attendance_breadcrumbs')
			<div class="title_wrapper">
				<h1>勤怠集計</h1>
				<div class="worklocation">
                    <div class="worklocation_inner">
                        <span class="right_10">{{ $current_work_location }}</span>
                        @if (isset($picker_list))
                            <p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
                        @endif
                    </div>
                    @include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'show_month_summary', 'singular' => true])
                </div>
				<section class="right_position2">
					<table class="close_btn_inner right">
						<tr>
							<td class="text_bold bg_gray">管理者2</td>
							<td>
								<p class="button">
									<a id="unconclude_all_button" class="m_size s_height btn_red {{ (!$can_conclude || !$all_concluded) ? 'hidden' : '' }}"><img class="ico_ico_arrow2" src="{{ asset('images/ico_check_red.svg') }}"></a>
									<a id="conclude_all_button" class="m_size s_height btn_white {{ (!$can_conclude || $all_concluded) ? 'hidden' : '' }}" href="#">全てを締める</a>
									<img class="check_red ico_ico_arrow4 {{ ($can_conclude || !$all_concluded) ? 'hidden' : '' }}" src="{{ asset('images/ico_check_red.svg') }}">
								</p>
							</td>
						</tr>
					</table>
				</section>
			</div>
		</section>
		<section id="month_summary_page">
			<input type='hidden' name='current_month' :value="currentMonth"></input>
			<input type='hidden' name='current_year' :value="currentYear"></input>
			<section class="select_one bg_light_green bottom_10">
				<section class="select_one_inner">
					<section @click="preMonth" class="right_30 ico_position">
						<a><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}"></a>
					</section>
					<div class="selectbox side_input_block right_10">
						<select class="s_size" v-model="currentYear" @change="yearChanged" v-cloak>
							<option v-for="year in yearRange" :value="year">@{{ year }}年</option>
						</select>
					</div>
					<div class="selectbox side_input_block right_10">
						<select class="s_size" v-model="currentMonth" @change="monthChanged" v-cloak>
							<option v-for="month in 12" :value="month">@{{ month }}月度</option>
						</select>
					</div>
					<p class="term right_30" v-cloak>@{{ startDateString }}<span class="right_10 left_10">〜</span v-cloak>@{{ endDateString }}</p>
					<section  @click ="nextMonth" class="ico_position">
						<a><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
					</section>
				</section>
			</section>
			<section class="search_box_wrapper">
				<div class="search_box_innner2">
					<div>
						<div class="search_setting">
							<span class="right_10">従業員ID</span><input class="s_size" v-model="conditions['presentation_id']" type="text">
						</div>
						<div class="search_setting">
							<span class="right_10">従業員名</span>
							<input type="text" class="vue_place_holder m_size" v-cloak>
							<autocomplete :suggestions = "employeeNames" custom-class="m_size" :initial-value="conditions['name']" :allow-approx ="true" @selected="employeeNameSelected" @changed="employeeNameChanged">
                            </autocomplete>
						</div>
						<div class="search_setting">
							<span class="right_10 left">雇用状態</span>
							<div class="selectbox left">
								<select class="s_size" v-model="conditions['work_status']">
									<option value=""></option>
									<option v-for="(status, id) in employeeWorkStatuses" :value="id">@{{ status }}</option>
								</select>
							</div>
						</div>
						<div class="check_box_wrap search_setting">
							<label class="checkbox_text"><input class="left" type="checkbox" v-model="conditions['only_show_considering']">申請中</label>
						</div>
						<div class="check_box_wrap search_setting">
							<label class="checkbox_text"><input class="left" type="checkbox" v-model="conditions['only_show_timestamp_error']">打刻エラー</label>
						</div>
						<div class="check_box_wrap search_setting">
							<label class="checkbox_text"><input class="left" type="checkbox" v-model="conditions['only_show_confirm_needed_error']">要確認エラー</label>
						</div>
						<div class="check_box_wrap search_setting">
							<label class="checkbox_text"><input class="left" type="checkbox" v-model="conditions['only_show_month_unconcluded']">締め未対応</label>
						</div>
						<div class="button bottom_10 search_setting right_10">
							<a class="s_size s_height btn_greeen" @click="submit(1)">検索</a>
						</div>
						<div class="button bottom_10 search_setting">
							<a class="s_size s_height btn_gray" @click="resetConditions">リセット</a>
						</div>
					</div>
				</div>
			</section>
			<paginator :total="total" :current-page="currentPage" :per-page="perPage" @changed="changePage"></paginator>
			<section class="pager">
				<section class="right_position">
					<p class="button left_10"><a class="mm_size s_height btn_gray" @click="downloadSummaryData()">勤怠集計ダウンロード</a></p>
				</section>
				<form id="downloadSummaryForm" :action="summaryDownloadLink" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="start_date" :value="calculatedStartDate.format('YYYY-MM-DD')">
					<input type="hidden" name="end_date" :value="calculatedEndDate.format('YYYY-MM-DD')">
					<input type="hidden" name="presentation_id" :value="conditions['presentation_id']">
					<input type="hidden" name="name" :value="conditions['name']">
					<input type="hidden" name="work_status" :value="conditions['work_status']">
				</form>
			</section>
			<section class="approval_table have_table_with_fixed_header">
				<table class="table_with_fixed_header">
					<tr class="fixed_header">
						<th class="s_10">従業員ID</th>
						<th>名前</th>
						<th class="s_10">申請数</th>					
						<th class="s_10">申請中</th>
						<th class="s_10">承認済</th>
						<th class="s_10">打刻エラー</th>
						<th class="s_10">要確認エラー</th>
						<th class="s_10">管理者1</th>
						<th class="s_20" colspan="2">管理者2</th>
					</tr>
					<tr v-for="employee in employees" v-cloak>
						<td>@{{ employee['presentation_id'] }}</td>
						<td>
							@can('view_attendance_employee_working_month_page')
								<a :href="urlToWorkingMonthPage(employee['id'])">
									<span v-if="employee['schedule_type_icon']" class="employ_ico">@{{ employee['schedule_type_icon'] }}</span>
									@{{ employee['name'] }}
								</a>
							@else
								<span v-if="employee['schedule_type_icon']" class="employ_ico">@{{ employee['schedule_type_icon'] }}</span>
								@{{ employee['name'] }}
							@endcan
						</td>
						<td>@{{ employee['snapshot_number'] }}</td>
						<td class="red">@{{ employee['considering_snapshot_number'] }}</td>
						<td>@{{ employee['approved_snapshot_number'] }}</td>
						<td class="red">@{{ employee['timestamp_errors_number'] }}</td>
						<td>@{{ employee['confirm_needed_errors_number'] }}</td>
						<td><img v-if="employee['is_all_day_concluded_level_one']" class="check_blue ico_ico_arrow3" src="{{ asset('images/ico_check_blue.svg') }}"></td>
						<td>
							<p v-if="employee['is_all_day_concluded_level_two']" class="button">
								<a v-if="canConclude" class="ss_size s_height btn_red" @click="unconcludedLevelTwo(employee['id'])"><img class="ico_ico_arrow2" src="{{ asset('images/ico_check_red.svg') }}"></a>
								<img v-else class="check_red ico_ico_arrow4" src="{{ asset('images/ico_check_red.svg') }}">
							</p>
							<p v-else class="button"><a v-if="canConclude" class="ss_size s_height btn_white" @click="concludeLevelTwo(employee['id'])">締める</a></p>
						</td>
						<td class="s_10">@{{ employee['concluded_level_two_manager_name'] }}</td>
					</tr>
				</table>
			</section>
			<paginator :total="total" :current-page="currentPage" :per-page="perPage" :sum-line="false" @changed="changePage"></paginator>
		</section>
	</main>
@endsection
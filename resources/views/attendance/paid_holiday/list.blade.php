@extends('layouts.master')

@section('title', '有給休暇管理')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/multiple-select.js') }}"></script>
    <script defer src="{{ asset('/js/components/work_location_picker.js') }}"></script>
    <script defer src="{{ asset('/js/pages/paid_holiday_list.js') }}"></script>
@endpush

@section('content')
    <main id="attendance_detail">
        <section class="title">
            @include('layouts.attendance_breadcrumbs')
            <div class="title_wrapper">
                <h1>有給休暇管理</h1>
                <div class="worklocation">
                    <div class="worklocation_inner">
                        <span class="right_10">{{ $current_work_location }}</span>
                        @if (isset($picker_list))
                            <p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
                        @endif
                    </div>
                    @include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'list_paid_holiday', 'singular' => false])
                </div>
            </div>
        </section>
        <section id="paid_holiday_searcher">
            <section class="search_box_wrapper">
                <div class="search_box_innner2">
                    <div>
                        <div class="check_box_wrap search_setting">
                            <label class="checkbox_text"><input v-model="conditions['only_updateable_employee']" type="checkbox">更新必要者のみ</label>
                        </div>
                        <div class="search_setting">
                            <span class="right_10">従業員ID</span>
                            <input class="s_size" v-model="conditions['presentation_id']" type="text">
                        </div>
                        <div class="search_setting">
                            <span class="right_10">従業員名</span>
                            <input type="text" class="vue_place_holder m_size" v-cloak>
                            <autocomplete custom-class="m_size" :suggestions="employeeNames" :allow-approx ="true"
                                :initial-value="conditions['employee_name']"
                                @selected="employeeNameSelected"
                                @changed="employeeNameChanged">
                            </autocomplete>
                        </div>
                        <div class="search_box search_setting">
                            <span class="right_10 left">採用形態</span>
                            <div class="selectbox left">
                                <select class="m_size" v-model="conditions['employment_type']">
                                    <option value=""></option>
                                    @foreach ($presentation_data['employment_types'] as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="search_box search_setting">
                            <span class="right_10 left">雇用状態</span>
                            <div class="selectbox left">
                                <select class="s_size" v-model="conditions['work_status']">
                                    <option value=""></option>
                                    @foreach ($presentation_data['work_statuses'] as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="search_box search_setting">
                            <span class="right_10">部署</span>
                            <section class="form-group mm_size">
                                <select class="mm_size" id="ms" v-model="conditions['departments']" autocomplete="off" multiple="multiple">
                                    @foreach ($presentation_data['departments'] as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </section>
                        </div>
                        <div class="search_box search_setting">
                            <span class="right_10 left">週勤務日数</span>
                            <div class="selectbox left">
                                <select class="s_size" v-model="conditions['holiday_bonus_type']">
                                    <option value=""></option>
                                    @foreach ($presentation_data['holiday_bonus_types'] as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="search_box search_setting">
                            <span class="right_10">入社日</span>
                            <input class="m_size right_10" v-model="conditions['joined_date_start']" type="text">
                            <span class=" right_10">〜</span>
                            <input class="m_size right_10" v-model="conditions['joined_date_end']" type="text">
                        </div>
                        <div class="search_box search_setting">
                            <span class="right_10">勤続年数</span>
                            <input class="ss_size" v-model="conditions['start_worked_years']" type="text">
                            <span class="select_both_space">年</span>
                            <input class="ss_size right_4" v-model="conditions['start_worked_months']" type="text">
                            <span class="right_10">ヶ月</span>
                            <span class=" right_10">〜</span>
                            <input class="ss_size right_10" v-model="conditions['end_worked_years']" type="text">
                            <span class="right_10">年</span>
                            <input class="ss_size right_4" v-model="conditions['end_worked_months']" type="text">
                            <span>ヶ月</span>
                        </div>
                        <div class="search_setting">
                            <span class="right_10">有給更新日</span><input class="ss_size" v-model="conditions['holidays_update_day']" type="text">
                        </div>
                        <div class="search_box search_setting">
                            <span class="right_10">有給更新日が</span>
                            <input class="ss_size" v-model="conditions['start_holidays_update_date']" type="text">
                            <span class=" right_10">〜</span>
                            <input class="ss_size right_10" v-model="conditions['end_holidays_update_date']" type="text">
                            <span class="right_10">のうち従業員の出勤率が</span>
                            <input class="ss_size right_4" v-model="conditions['attendance_rate']" type="text">
                            <span>％未満</span>
                        </div>

                        <div class="check_box_wrap search_setting">
                            <label class="checkbox_text"><input v-model="conditions['paid_holiday_exception']" type="checkbox" >有給対象外</label>
                        </div>
                        <div class="button bottom_10 search_setting right_10">
                            <a class="s_size s_height btn_greeen" @click="search">検索</a>
                        </div>
                        <div class="button bottom_10 search_setting">
                            <a class="s_size s_height btn_gray" @click="resetConditions">リセット</a>
                        </div>
                    </div>
                </div>
            </section>
            <paginator :total="total" :current-page="currentPage" :per-page="perPage" @changed="changePage"></paginator>
            <section class="pager">
                @can('change_work_data_paid_holiday_management')
                    <section class="left_position">
                        <section class="side_input_block right_10">
                            <span class="right_10">チェックしたものを</span>
                            <p class="button"><a class="ss_size s_height btn_white" @click="updateCheckedEmployees">更新</a></p></section>
                        <section class="side_input_block">
                            <p class="button"><a class="s_size s_height btn_white" @click="updateAllEmployeesInTheSearchResult">全員を更新</a>
                        </section>
                    </section>
                @endcan
            </section>
            <section class="approval_table">
                <table>
                    <tr>
                        <th class="s_4">
                            <div class="check_onle_wrap">
                                <label class="checkbox_box"><input v-model="checkedAllUpdatableEmployee" type="checkbox"></label>
                            </div>
                        </th>
                        <th class="s_6">従業員ID</th>
                        <th :class="showWorkLocationName ? 's_8' : ''">従業員名</th>
                        <th v-if="showWorkLocationName" class="s_12">所属先</th>
                        <th class="s_6">週勤務日数</th>
                        <th class="s_8">入社日</th>
                        <th class="s_6">勤続年数</th>
                        <th class="s_4">更新日</th>
                        <th class="s_8">更新前年度出勤率</th>
                        <th class="s_5">付与日数</th>
                        <th class="s_8"><p>繰越日数</p><p>日:時間:分</p></th>
                        <th class="s_8"><p>残日数</p><p>日:時間:分</p></th>
                        <th class="s_8">操作年月日</th>
                        @can('change_work_data_paid_holiday_management')
                            <th class="s_5">更新</th>
                        @endcan
                        @can('view_work_data_paid_holiday_detail')
                            <th class="s_5"></th>
                        @endcan
                    </tr>
                    <tr v-for="employee in employees" v-cloak>
                        <td>
                            <div class="check_onle_wrap">
                                <label v-if="employee['can_be_updated']" class="checkbox_box"><input v-model="toBeUpdatedEmployeeIds" :value="employee['id']" type="checkbox"></label>
                            </div>
                        </td>
                        <td>@{{ employee['presentation_id'] }}</td>
                        <td><span v-if="employee['schedule_type_icon']" class="employ_ico">@{{ employee['schedule_type_icon'] }}</span>@{{ employee['employee_name'] }}</td>
                        <td v-if="showWorkLocationName">@{{ workLocationNames[employee['work_location_id']] }}</td>
                        <td>@{{ employee['holiday_bonus_type'] }}</td>
                        <td>@{{ employee['joined_date'] }}</td>
                        <td>@{{ employee['worked_years'] + '年' + employee['worked_months'] + 'ヶ月' }}</td>
                        <td>@{{ employee['holiday_update_day'] }}</td>
                        <td>@{{ employee['can_be_updated'] ? '' : employee['last_period_attendance_rate'] }}</td>
                        <td>@{{ employee['can_be_updated'] ? '' : employee['last_period_provided_days'] }}</td>
                        <td>@{{ employee['can_be_updated'] ? '' : (employee['last_period_carried_forward_days'] ? employee['last_period_carried_forward_days'] : '0') + '日:' + (employee['last_period_carried_forward_hour'] ? convertToHourMinute(employee['last_period_carried_forward_hour']) : '0:0') }}</td>
                        <td>@{{ employee['can_be_updated'] ? '' : (employee['last_period_available_days'] ? employee['last_period_available_days'] : '0') + '日:' + (employee['last_period_available_hour'] ? convertToHourMinute(employee['last_period_available_hour']) : '0:0') }}</td>
                        <td>@{{ employee['last_period_last_update'] }}</td>
                        @can('change_work_data_paid_holiday_management')
                            <td>
                                <p v-if="employee['can_be_updated']" class="button"><a class="ss_size s_height btn_white" @click="updateThisEmployee(employee['id'])">更新</a></p>
                            </td>
                        @endcan
                        @can('view_work_data_paid_holiday_detail')
                            <td>
                                <p class="button"><a class="ss_size s_height btn_gray" @click="goToEditPaidHoliday(employee['id'])">詳細</a></p>
                            </td>
                        @endcan
                    </tr>
                </table>
            </section>
            <paginator :total="total" :current-page="currentPage" :per-page="perPage" :sum-line="false" @changed="changePage"></paginator>
        </section>
	</main>
@endsection
@extends('layouts.master')

@section('title', '有給休暇詳細')

@section('header')
    @include('layouts.header', [ 'active' => 2 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/multiple-select.js') }}"></script>
    <script defer src="{{ asset('/js/components/work_location_picker.js') }}"></script>
    <script defer src="{{ asset('/js/pages/paid_holiday_edit.js') }}"></script>
@endpush

@section('content')
    <main id="attendance_detail">
        <section class="title">
            @include('layouts.attendance_breadcrumbs')
            <div class="title_wrapper">
                <h1>有給休暇詳細</h1>
                <div class="worklocation">
                    <div class="worklocation_inner">
                        <span class="right_10">{{ $current_work_location }}</span>
                        @if (isset($picker_list))
                            <p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
                        @endif
                    </div>
                    @include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'list_paid_holiday', 'singular' => false])
                </div>
            </div>
        </section>
        <section class="select_one bg_light_green bottom_10">
			<section class="select_one_inner">
                @if( BreadCrumbs::canGoPrevious($employee->id) )
                    <section class="right_30 ico_position">
                        <a href="{{ Caeru::route('breadcrumb_go_previous', [ 'pivot_value' => $employee->id ]) }}"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}"></a>
                    </section>
                @endif
				<section class="right_30 text_bold">
					<span class="right_30">{{ $employee->presentation_id }}</span><span class="right_30">{{ $employee->fullName() }}</span><span>{{ $employee->scheduleType() }}</span>
                </section>
                @if( BreadCrumbs::canGoNext($employee->id) )
                    <section class="ico_position">
                        <a href="{{ Caeru::route('breadcrumb_go_next', [ 'pivot_value' => $employee->id ]) }}"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
                    </section>
                @endif
			</section>
		</section>
		<section class="setting_table bottom_30">
			<table>
				<tr>
					<td class="input_items">従業員ID</td>
					<td>{{ $employee->presentation_id }}</td>
				</tr>
                <tr>
					<td class="input_items">従業員名</td>
					<td>{{ $employee->fullName() }}</td>
				</tr>
                <tr>
					<td class="input_items">従業員名(カナ)</td>
					<td>{{ $employee->last_name_furigana . $employee->first_name_furigana }}</td>
				</tr>
                <tr>
					<td class="input_items">採用形態</td>
					<td>{{ $employee->employmentType() }}</td>
				</tr>
                <tr>
					<td class="input_items">有給更新日</td>
					<td>{{ $employee->holidays_update_day }}</td>
				</tr>
                <tr>
					<td class="input_items">部署</td>
					<td>{{ $employee->departmentName() }}</td>
				</tr>
                <tr>
					<td class="input_items">雇用状態</td>
					<td>{{ $employee->workStatus() }}</td>
				</tr>
                <tr>
					<td class="input_items">入社日</td>
					<td>{{ $employee->joined_date }}</td>
				</tr>
                <tr>
					<td class="input_items">勤続年数</td>
					<td>{{ $employee->workedTimeString() }}</td>
				</tr>
                <tr>
					<td class="input_items">週勤務日数</td>
					<td>{{ $employee->paidHolidayBonusType() }}</td>
				</tr>
			</table>
		</section>
		<section>
            <form id="paid_holiday_form" method="POST" form-single-submit action="{{ Caeru::route('update_paid_holiday', [ 'employee' => $employee->id]) }}">
                {{ csrf_field() }}
                <table>
                    <tr>
                        <th class="s_14">期間</th>
                        <th class="s_8">１日の労働時間</th>
                        <th class="s_6">出勤率</th>
                        <th class="s_6">付与日数</th>
                        <th class="s_12" colspan="2"><p>繰越日数</p><p>日:時間:分</p></th>
                        <th class="s_12" colspan="2"><p>取得日数</p><p>日:時間:分</p></th>
                        <th class="s_12" colspan="2"><p>残日数</p><p>日:時間:分</p></th>
                        <th class="s_14">備考</th>
                        <th class="s_8">操作年月日</th>
                        <th>最終変更者</th>
                    </tr>
                    <!--　所定が変更になった場合 -->
                    @foreach($paid_holiday_infos as $info)
                        <tr>
                            <td>{{ $info['period_start'] . '〜' . $info['period_end'] }}</td>
                            <td>{{ $info['work_time_per_day']/60 . '時間' }}</td>
                            @if (!$info->skip)
                                <td rowspan="{{ $info->row_span ? $info->row_span : 1 }}">
                                    @if($loop->first)
                                        @can('change_work_data_paid_holiday_detail')
                                            @component('layouts.form.error', ['field' => 'attendance_rate'])
                                                <input class="s_m_size right_4" name="attendance_rate" type="text" value="{{ old('attendance_rate', $info['attendance_rate']) }}">
                                            @endcomponent
                                            <span>％</span>
                                        @else
                                            {{ $info['attendance_rate'] ? $info['attendance_rate'] . '%' : '' }}
                                        @endcan
                                    @else
                                        {{ $info['attendance_rate'] ? $info['attendance_rate'] . '%' : '' }}
                                    @endif
                                </td>
                                <td rowspan="{{ $info->row_span ? $info->row_span : 1 }}">
                                    @if($loop->first)
                                        @can('change_work_data_paid_holiday_detail')
                                            @component('layouts.form.error', ['field' => 'provided_paid_holidays'])
                                                <input class="s_m_size right_4" name="provided_paid_holidays" type="text" value="{{ old('provided_paid_holidays', $info['provided_paid_holidays']) }}">日
                                            @endcomponent
                                        @else
                                            {{ $info['provided_paid_holidays'] ? $info['provided_paid_holidays'] . '日' : '0日' }}
                                        @endcan
                                    @else
                                        {{ $info['provided_paid_holidays'] ? $info['provided_paid_holidays'] . '日' : '0日' }}
                                    @endif
                                </td>
                            @endif
                            <td>
                                @if($loop->first)
                                    @can('change_work_data_paid_holiday_detail')
                                        @component('layouts.form.error', ['field' => 'carried_forward_paid_holidays'])
                                            <input class="s_m_size right_4" name="carried_forward_paid_holidays" type="text" value="{{ old('carried_forward_paid_holidays', $info['carried_forward_paid_holidays']) }}">日
                                        @endcomponent
                                    @else
                                        {{ $info['carried_forward_paid_holidays'] ? $info['carried_forward_paid_holidays'] . '日' : '0日' }}
                                    @endcan
                                @else
                                    {{ $info['carried_forward_paid_holidays'] ? $info['carried_forward_paid_holidays'] . '日' : '0日' }}
                                @endif
                            </td>
                            <td>
                                @if($loop->first)
                                    @can('change_work_data_paid_holiday_detail')
                                        @component('layouts.form.error', ['field' => 'carried_forward_paid_holidays_hour'])
                                            <input id="carried_forward_paid_hoildays_hour" class="s_m_size right_4" name="carried_forward_paid_holidays_hour" type="text" value="{{ old('carried_forward_paid_holidays_hour', CaeruBlade::toHourMinute($info['carried_forward_paid_holidays_hour'])) }}">
                                        @endcomponent
                                    @else
                                        {{ CaeruBlade::toHourMinute($info['carried_forward_paid_holidays_hour']) }}
                                    @endcan
                                @else
                                    {{ CaeruBlade::toHourMinute($info['carried_forward_paid_holidays_hour']) }}
                                @endif
                            </td>
                            <td>
                                {{ $info['consumed_paid_holidays'] }}日
                            </td>
                            <td>
                                {{ CaeruBlade::toHourMinute($info['consumed_paid_holidays_hour']) }}
                            </td>
                            <td>
                                {{ $info['available_paid_holidays'] }}日
                            </td>
                            <td>
                                {{ CaeruBlade::toHourMinute($info['available_paid_holidays_hour']) }}
                            </td>
                            <td>
                                @can('change_work_data_paid_holiday_detail')
                                    <input class="m_size" name="notes[]" type="text" value="{{ $info['note'] }}">
                                @else
                                    {{ $info['note'] }}
                                @endcan
                            </td>
                            <td>
                                {{ $info['last_modified_date'] }}
                            </td>
                            <td>
                                {{ $info->lastModifiedManagerName() }}
                            </td>
                        </tr>
                    @endforeach
                </table>
                <section class="btn">
                    @can('change_work_data_paid_holiday_detail')
                        <p class="button right_30"><button class="m_size l_height btn_greeen l_font" >保存</button></p>
                        <p class="button right_30"><a class="m_size l_height btn_gray l_font" href="{{ Caeru::route('edit_paid_holiday', [ 'employee' => $employee->id]) }}">キャンセル</a></p>
                    @endcan
                    <p class="button"><a single-click class="m_size l_height btn_gray l_font" href="{{ Caeru::route('breadcrumb_go_back') }}">一覧に戻る</a></p>
                </section>
            </form>
		</section>
    </main>
@endsection
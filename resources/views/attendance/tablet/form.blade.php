@extends('layouts.master')

@section('title', '勤怠データ管理')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
@endpush

@section('content')
    <main>
        <section class="title">
            <h1>This page's purpose is to create a WorkingTimestamp like a tablet does. (For testing)</h1>
        </section>
        <form class="tablet-testing" method="POST" action="{{ Caeru::route('tablet_store_timestamp') }}">
            {{ csrf_field() }}
            <table>
                <tr>
                    <th><span>Employee/従業員 database id:</span></th>
                    <td>
                        @if (isset($working_day))
                            <input type="text" class="mm_size" name="employee_id" value="{{ $working_day->employee_id }}">
                        @else
                            <input type="text" class="mm_size" name="employee_id">
                        @endif
                    </td>
                </tr>
                <tr>
                    <th><span>Date/日付:</span></th>
                    <td>
                        @if (isset($working_day))
                            <input type="text" class="mm_size" name="date" value="{{ $working_day->date }}">
                        @else
                            <input type="text" class="mm_size" name="date">
                        @endif
                    </td>
                </tr>
                <tr>
                    <th><span>WorkingTimestamp's date/打刻時刻 (yyyy-mm-dd hh:mm:ss):</span></th>
                    <td><input type="text" class="mm_size" name="timestamp"></td>
                </tr>
                <tr>
                    <th><span>WorkLocation/勤務地:</span></th>
                    <td>
                        <select name="work_location_id" class="mm_size">
                            @foreach($work_locations as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><span>Timestamp type/打刻タイプ:</span></th>
                    <td>
                        <select name="timestamped_type" class="mm_size">
                            @foreach($timestamp_types as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            </table>
            <section class ="btn">
                <p class="button">
                    <button type="submit" class ="mm_size l_height btn_greeen l_font" >Save/保存</button>
                </p>
            </section>
        </form>
    </main>
@endsection
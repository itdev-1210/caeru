@extends('layouts.master')

@section('title', '勤怠データ管理')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
@endpush

@section('content')
    <main>
        <section class="title">
            <h1>This page's purpose is to delete a WorkingTimestamp. (For testing)</h1>
        </section>
        <table class="tablet-testing">
            <tr>
                <th><span>WorkingTimestamp's date/打刻時刻 (yyyy-mm-dd hh:mm:ss):</span></th>
                <th><span>WorkLocation/勤務地:</span></th>
                <th><span>Timestamp type/打刻タイプ:</span></th>
                <th>
                    <p class="button">
                        <a href="{{ Caeru::route('tablet_create_timestamp', ['working_day' => $working_day->id]) }}" class ="mm_size l_height btn_blue l_font" >Create/作成</a>
                    </p>
                </th>
            </tr>
            @foreach($timestamps as $timestamp)
                <tr>
                    <td>
                        <span>{{ $timestamp->created_at }}</span>
                    </td>
                    <td>
                        <span>{{ $work_locations[$timestamp->work_location_id] }}</span>
                    </td>
                    <td>
                        <span>{{ $timestamp_types[$timestamp->timestamped_type] }}</span>
                    </td>
                    <td>
                        <p class="button">
                            <a href="{{ Caeru::route('tablet_delete_timestamp', ['working_timestamp' => $timestamp->id]) }}" class ="mm_size l_height btn_red l_font" >Delete/削除</a>
                        </p>
                    </td>
                </tr>
            @endforeach
        </table>
    </main>
@endsection
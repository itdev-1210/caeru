@extends('layouts.master')

@section('title', '勤怠データ検索')

@section('header')
	@include('layouts.header', [ 'active' => 3 ])
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@push('scripts')
	<script defer src="{{ asset('/js/multiple-select.js') }}"></script>
    <script defer src="{{ asset('/js/pages/attendance_advance_search.js') }}"></script>
	<script defer src="{{ asset('/js/components/work_location_picker.js') }}"></script>
@endpush

@section('content')
    <main id="attendance_detail">
		<section class="title">
			@include('layouts.attendance_breadcrumbs')
			<div class="title_wrapper">
				<h1>勤怠データ検索</h1>
				<div class="worklocation">
					<div class="worklocation_inner">
						<span class="right_10">{{ $current_work_location }}</span>
						@if (isset($picker_list))
                            <p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
                        @endif
					</div>
					@include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'show_advance_search', 'singular' => false])
				</div>
			</div>
		</section>
		<section id="attendance_advance_search">
			<advance-search></advance-search>
		</section>
	</main>
@endsection
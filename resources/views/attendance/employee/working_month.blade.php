@extends('layouts.master')

@section('title', '個人別詳細')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/employee_working_month.js') . '?v=' . time() }}"></script>
@endpush

@section('content')
    <main id="attendance_detail">
        <section class="title">
            @include('layouts.attendance_breadcrumbs')
            <div class="title_wrapper">
                <h1>個人別詳細</h1>
                <section class="select_one">
                    <section class="select_one_inner">
                        <section class="right_30 ico_position">
                            <a @click="previousMonth"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}"></a>
                        </section>
                        <div class="selectbox side_input_block right_10">
                            <select class="s_size" v-model.number="selectedYear">
                                <option v-for="year in yearRange" :value="year">@{{ year }}年</option>
                            </select>
                        </div>
                        <div class="selectbox side_input_block right_30">
                            <select class="s_size" v-model.number="selectedMonth">
                                <option v-for="month in monthRange" :value="month">@{{ month }}月度</option>
                            </select>
                        </div>
                        <section class="ico_position">
                            <a @click="nextMonth"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
                        </section>
                    </section>
                </section>
                <section class="right_position2">
                <table class="close_btn_inner right">
                    <tr>
                        <td class="text_bold bg_gray">管理者１</td>
                        <td>
                            <!-- <p class="button"><a class="ss_size s_height btn_dark_blue"><img class="ico_ico_arrow2" src="{{ asset('images/ico_check_blue.svg') }}"></a></p> -->
                            @can('conclude_level_one')
                                <p v-if="everydayCanBeConcluded && levelOneConcludedAll" class="button">
                                    <a class="ss_size s_height btn_dark_blue" @click="unConcludeLevelOneOnAllWorkingDays">
                                        <img class="ico_ico_arrow2" src="{{ asset('images/ico_check_blue.svg') }}">
                                    </a>
                                </p>
                                <p v-else-if="everydayCanBeConcluded && !levelOneConcludedAll" class="button"><a class="ss_size s_height btn_white" @click="concludeLevelOneOnAllWorkingDays">締める</a></p>
                            @else
                                <p v-if="levelOneConcludedAll"><img class="check_blue ico_ico_arrow4" src="{{ asset('images/ico_check_blue.svg') }}"></p>
                            @endcan
                        </td>
                        <td class="text_bold bg_gray">管理者2</td>
                        <td>
                            @can('conclude_level_two')
                                <p v-if="everydayCanBeConcluded && levelTwoConcluded" class="button"><a class="ss_size s_height btn_red" @click="unconcludedLevelTwo"><img class="ico_ico_arrow2" src="{{ asset('images/ico_check_red.svg') }}"></a></p>
                                <p v-else-if="everydayCanBeConcluded && !levelTwoConcluded" class="button"><a class="ss_size s_height btn_white" @click="concludeLevelTwo">締める</a></p>
                            @else
                                <p v-if="levelTwoConcluded"><img class="check_red ico_ico_arrow4" src="{{ asset('images/ico_check_red.svg') }}"></p>
                            @endcan
                        </td>
                        <td class="name">@{{ levelTwoConcluded ? levelTwoConcludedManagerName : '' }}</td>
                    </tr>
                </table>
                </section>
            </div>
        </section>
        <section class="select_one bg_light_green">
            <section class="select_one_inner">
                @if( BreadCrumbs::canGoPrevious($employee->id) )
                    <section class="right_30 ico_position">
                        <a href="{{ Caeru::route('breadcrumb_go_previous', [ 'pivot_value' => $employee->id ]) }}"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left3.svg') }}"></a>
                    </section>
                @endif
                <section class="right_30 text_bold">
                    <span class="right_30">{{ $employee->presentation_id }}</span><span class="right_30">{{ $employee->fullName() }}</span><span>{{ $employee->scheduleType() }}</span>
                </section>
                @if( BreadCrumbs::canGoNext($employee->id) )
                    <section class="ico_position">
                        <a href="{{ Caeru::route('breadcrumb_go_next', [ 'pivot_value' => $employee->id ]) }}"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right3.svg') }}"></a>
                    </section>
                @endif
                <section class="right_position3">
                    <p class="button"><a class="ss_size m_height btn_greeen" @click="detailPrint">印刷</a></p>
                </section>
            </section>
        </section>
        <section class="person_result_table">
            <table class="left s_60">
                <tr>
                    <th class="s_8" rowspan="2"></th>
                    <th class="s_10 bg_dark_gray" rowspan="2">勤務日数({{ $summarized_data['have_schedule_days'] }}日)</th>
                    <th class="s_10 bg_dark_gray" rowspan="2">総勤務時間</th>
                    <th class="s_28" colspan="4">所定( {{ CaeruBlade::toHourMinute($summarized_data['schedule_working_time']) }} )</th>
                    <th class="s_10" rowspan="2">時間外</th>
                </tr>
                <tr>
                    <th class="s_10">所定内</th>
                    <th class="s_8">有給</th>
                    <th class="s_8">無給</th>
                    <th class="s_8">不就労</th>
                </tr>
                <tr>
                    <td>実績<span class="light_gray">(予測)</span></td>
                    <td>{{ $summarized_data['real_work_days'] }}<span class="light_gray">( {{ $summarized_data['planned_work_days'] }} )</span></td>
                    <td>{{ CaeruBlade::toHourMinute($summarized_data['real_working_time']) }}<span class="light_gray">( {{ CaeruBlade::toHourMinute($summarized_data['planned_working_time']) }} )</span></td>
                    <td>{{ CaeruBlade::toHourMinute($summarized_data['real_work_span_time']) }}<span class="light_gray">( {{ CaeruBlade::toHourMinute($summarized_data['planned_work_span_time']) }} )</span></td>
                    <td>{{ CaeruBlade::toHourMinute($summarized_data['real_paid_rest_time']) }}<span class="light_gray">( {{ CaeruBlade::toHourMinute($summarized_data['planned_paid_rest_time']) }} )</span></td>
                    <td>{{ CaeruBlade::toHourMinute($summarized_data['real_unpaid_rest_time']) }}<span class="light_gray">( {{ CaeruBlade::toHourMinute($summarized_data['planned_unpaid_rest_time']) }} )</span></td>
                    <td>{{ CaeruBlade::toHourMinute($summarized_data['real_non_work_time']) }}<span class="light_gray">( {{ CaeruBlade::toHourMinute($summarized_data['planned_non_work_time']) }} )</span></td>
                    <td>{{ CaeruBlade::toHourMinute($summarized_data['real_overtime_work_time']) }}<span class="light_gray">( {{ CaeruBlade::toHourMinute($summarized_data['planned_overtime_work_time']) }} )</span></td>
                </tr>
            </table>
            <table class="left s_28 secound both_1">
                <tr>
                    <th class="s_28">深夜勤務</th>
                    <th class="s_28">欠勤日数</th>
                    <th><p>年次有給取得</p><p>日:時間:分</p></th>
                </tr>
                <tr>
                    <td>{{ CaeruBlade::toHourMinute($summarized_data['real_night_work_time']) }}<span class="light_gray">( {{ CaeruBlade::toHourMinute($summarized_data['planned_night_work_time']) }} )</span></td>
                    <td>{{ $summarized_data['real_kekkin_days'] }}日<span class="light_gray">( {{ $summarized_data['planned_kekkin_days'] }} 日)</span></td>
                    <td>{{ $summarized_data['real_taken_paid_rest_days'] }}日{{ CaeruBlade::toHourMinute($summarized_data['real_taken_paid_rest_time']) }}
                        <span class="light_gray">( {{ $summarized_data['planned_taken_paid_rest_days'] }}日{{ CaeruBlade::toHourMinute($summarized_data['planned_taken_paid_rest_time']) }} )</span>
                    </td>
                </tr>
            </table>
            <table class="right s_10 secound">
                <tr>
                    <th class="s_20"><p>年次有給残日数</p><p>日:時間:分</p></th>
                </tr>
                <tr>
                    <td>
                        <span>{{ ($real_available_paid_holidays ? $real_available_paid_holidays : 0) . '日' . ($real_available_paid_holidays_hour ? CaeruBlade::toHourMinute($real_available_paid_holidays_hour) : '00:00') }}</span>
                        <span class="light_gray">({{ ($planned_available_paid_holidays ? $planned_available_paid_holidays : 0) . '日' . ($planned_available_paid_holidays_hour ? CaeruBlade::toHourMinute($planned_available_paid_holidays_hour) : '00:00') }})</span>
                    </td>
                </tr>
            </table>
        </section>
        <section class="select_one2">
            <section class="left_position">
                <div class="left right_10 description_day_off1">法定休日</div>
                <div class="left right_10 description_day_off2">一般休日</div>
                <div class="left right_10 description_confirm_list">要確認</div>
                <div class="left right_10 description_mistake description_text">誤った記録</div>
                <div class="left right_10 description_request description_text">申請中</div>
                <div class="left right_10 description_approval description_text">承認</div>
                <div class="left right_10 description_reject description_text">否決</div>
                <div class="left description_correction description_text">修正</div>
            </section>
        </section>
        <section class="select_one2">
            <section class="left_position">
                @include('layouts.form.radio_display_toggle',['default' => $display_toggle])
            </section>
            <section class="right_position">

                <p class="button">
                    <a class="m_size s_height btn_gray" @click="switchView" v-if="displayWorkPlace == false">勤務地表示</a>
                    <a class="m_size s_height btn_gray" @click="switchView" v-else>詳細表示</a>
                </p>
            </section>
        </section>

        <section>
            <working-month :display-work-place="displayWorkPlace" :manager-can-conclude-level-one="managerCanConcludeLevelOne" :display-toggle="displayToggle" @changed="updateConcludeStatus"></working-month>
            <section class="btn">
                <p class="button"><a single-click class="m_size l_height btn_gray l_font" href="{{ Caeru::route('breadcrumb_go_back') }}">一覧に戻る</a></p>
            </section>
        </section>
    </main>
@endsection
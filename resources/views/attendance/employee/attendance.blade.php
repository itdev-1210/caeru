@extends('layouts.master')

@section('title', '勤怠データ管理')

@section('header')
    @include('layouts.header', [ 'active' => 3 ])
@endsection

@push('scripts')
	<script defer src="{{ asset('/js/multiple-select.js') }}"></script>
	<script defer src="{{ asset('/js/pages/employees_attendance.js') }}"></script>
@endpush

@section('content')
    <main id="attendance">
		<section class="title">
			@include('layouts.attendance_breadcrumbs')
			<div class="title_wrapper">
				<h1>勤怠データ管理</h1>
				<div class="worklocation">
					<div class="worklocation_inner">
						<span class="right_10">{{ $current_work_location }}</span>
						@if (isset($picker_list))
								<p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
						@endif
					</div>
					@include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'employee_attendance', 'singular' => false])
				</div>
				@if ($company->use_night_shift)
				<section>
					<night-shift @updated="updateData"></night-shift>
				</section>
				@endif
			</div>
		</section>
		<section>
			<attendance-table ref="attendanceTable"></attendance-table>
		</section>
		{{-- @include('attendance.employee.attendance_table', ['day_list' => $day_list, 'employees_data' => $employees_data]) --}}
	</main>
@endsection
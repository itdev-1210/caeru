
@foreach($employees_data as $employee)
    @if(isset($employee['work_location_label']) && $employee['work_location_label'] == true)
        <tr class="dynamic_row work_location_label" title="{{ $employee['work_location_id'] }}">
            <td class="visit_place bg_white white_label" colspan="2">{{ $work_locations[$employee['work_location_id']]->name }}</td>
            <td colspan="{{ (isset($display_go_out) && $display_go_out == true) ? '12' : '11' }}" class="bold_line right_align"></td>
            <td colspan="{{ (isset($display_go_out) && $display_go_out == true) ? '12' : '11' }}" class="bold_line right_align"></td>
            <td colspan="{{ (isset($display_go_out) && $display_go_out == true) ? '12' : '11' }}" class="bold_line right_align"></td>
            <td colspan="{{ (isset($display_go_out) && $display_go_out == true) ? '12' : '11' }}" class="bold_line right_align"></td>
            <td colspan="{{ (isset($display_go_out) && $display_go_out == true) ? '12' : '11' }}" class="bold_line right_align"></td>
            <td colspan="{{ (isset($display_go_out) && $display_go_out == true) ? '12' : '11' }}" class="bold_line right_align"></td>
            <td colspan="{{ (isset($display_go_out) && $display_go_out == true) ? '12' : '11' }}" class="bold_line right_align"></td>
        </tr>
    @else
        @for($line_index = 0; $line_index < $employee['max_lines']; $line_index++)
            <tr class="dynamic_row">
                @if($line_index == 0)
                    <td class="l_size bg_gray"  rowspan="{{ 2 * $employee['max_lines'] }}">
                        @can('view_attendance_employee_working_month_page')
                            <a href="{{ Caeru::route('employee_working_month', ['employee' => $employee['id']]) }}">
                        @else
                            <a class="no_underline">
                        @endcan
                            <p>{{ $employee['presentation_id'] }}</p>
                            <p>
                                @if ($employee['schedule_type_icon'])
                                    <span class="employ_ico">{{ $employee['schedule_type_icon'] }}</span>
                                @endif
                                {{ $employee['name'] }}
                            </p>
                        </a>
                    </td>
                @endif
                <td class="s_size bg_gray fixed_column_header_second_column">予定</td>
                @foreach( $employee['data'] as $day_string => $day)
                    @if( count($day) == 0 || !isset($day[$line_index]))
                        <td rowspan="2"></td>
                        <td rowspan="2"></td>
                        <td class="bg_light_gray "></td>
                        <td rowspan="2"></td>
                        <td class="bg_light_gray"></td>
                        <td rowspan="2"></td>
                        <td class="bg_light_gray"></td>
                        <td class="bg_light_gray"></td>
                        @if (isset($display_go_out) && $display_go_out == true)
                            <td class="bg_light_gray"></td>
                        @endif
                        <td class="bg_light_gray"></td>
                        @if ($line_index == 0)
                            <td rowspan="{{ $employee['max_lines'] * 2 }}">
                                @if ($employee['concluded_info'][$day_string]['concluded_level_two'] == true)
                                    <img class="check_red ico_ico_arrow4" src="{{ asset('images/ico_check_red.svg') }}">
                                @elseif($employee['concluded_info'][$day_string]['concluded_level_one'] == true)
                                    <img class="check_blue ico_ico_arrow4" src="{{ asset('images/ico_check_blue.svg') }}">
                                @endif
                            </td>
                            <td class="bold_line" rowspan="{{ $employee['max_lines'] * 2 }}">
                                @can('view_attendance_employee_working_day')
                                    <p class="button"><a class="sss_size ss_height btn_gray" href="{{ Caeru::route('employee_working_day_detail', ['employee_id' => $employee['id'], 'date' => $day_string]) }}">変更</a></p>
                                @endcan
                            </td>
                        @endif
                    @else
                        <td rowspan="2" class="{{ $day[$line_index]['color_statuses']['work_status_id']['color'] }}">{{ $day[$line_index]['work_status_id'] ? $all_work_statuses[$day[$line_index]['work_status_id']] : '' }}</td>
                        <td rowspan="2" class="{{ $day[$line_index]['color_statuses']['rest_status_id']['color'] }}">{{ $day[$line_index]['rest_status_id'] ? $all_rest_statuses[$day[$line_index]['rest_status_id']] : '' }}</td>
                        <td class="bg_light_gray {{ $day[$line_index]['color_statuses']['planned_start_work_time']['color'] }}">{{ CaeruBlade::toHms($day[$line_index]['planned_start_work_time']) }}</td>
                        <td rowspan="2" class="{{ $day[$line_index]['color_statuses']['real_start_work_time']['color'] }}">{{ CaeruBlade::toHms($day[$line_index]['real_start_work_time']) }}</td>
                        <td class="bg_light_gray {{ $day[$line_index]['color_statuses']['planned_end_work_time']['color'] }}">{{ CaeruBlade::toHms($day[$line_index]['planned_end_work_time']) }}</td>
                        <td rowspan="2" class="{{ $day[$line_index]['color_statuses']['real_end_work_time']['color'] }}">{{ CaeruBlade::toHms($day[$line_index]['real_end_work_time']) }}</td>
                        <td class="bg_light_gray {{ $day[$line_index]['color_statuses']['planned_total_early_arrive_and_overtime']['color'] }}">{{ $day[$line_index]['planned_total_early_arrive_and_overtime'] }}</td>
                        <td class="bg_light_gray {{ $day[$line_index]['color_statuses']['planned_break_time']['color'] }}">{{ $day[$line_index]['planned_break_time'] }}</td>
                        @if (isset($display_go_out) && $display_go_out == true)
                            <td class="bg_light_gray {{ $day[$line_index]['color_statuses']['planned_go_out_time']['color'] }}">{{ $day[$line_index]['planned_go_out_time'] }}</td>
                        @endif
                        <td class="bg_light_gray {{ $day[$line_index]['color_statuses']['planned_total_late_and_leave_early']['color'] }}">{{ $day[$line_index]['planned_total_late_and_leave_early'] }}</td>
                        @if ($line_index == 0)
                            <td rowspan="{{ $employee['max_lines'] * 2 }}">
                                @if ($employee['concluded_info'][$day_string]['concluded_level_two'] == true)
                                    <img class="check_red ico_ico_arrow4" src="{{ asset('images/ico_check_red.svg') }}">
                                @elseif($employee['concluded_info'][$day_string]['concluded_level_one'] == true)
                                    <img class="check_blue ico_ico_arrow4" src="{{ asset('images/ico_check_blue.svg') }}">
                                @endif
                            </td>
                            <td class="bold_line" rowspan="{{ $employee['max_lines'] * 2 }}">
                                @can('view_attendance_employee_working_day')
                                    <p class="button"><a class="sss_size ss_height btn_gray" href="{{ Caeru::route('employee_working_day_detail', ['employee_id' => $employee['id'], 'date' => $day_string]) }}">変更</a></p>
                                @endcan
                            </td>
                        @endif
                    @endif
                @endforeach
            </tr>
            <tr class="dynamic_row">
                <td class="bg_gray fixed_column_header_second_column">実績</td>
                @foreach ($employee['data'] as $day)
                    @if (count($day) == 0 || !isset($day[$line_index]))
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        @if (isset($display_go_out) && $display_go_out == true)
                            <td></td>
                        @endif
                        <td></td>
                    </template>
                    @else
                        <td class="{{ $day[$line_index]['color_statuses']['timestamped_start_work_time']['color'] . ((isset($day[$line_index]['start_time_red_number']) && $day[$line_index]['start_time_red_number'] == true) ? ' red' : '') }}">{{ CaeruBlade::toHms($day[$line_index]['timestamped_start_work_time']) }}</td>
                        <td class="{{ $day[$line_index]['color_statuses']['timestamped_end_work_time']['color'] . ((isset($day[$line_index]['end_time_red_number']) && $day[$line_index]['end_time_red_number'] == true) ? ' red' : '') }}">{{ CaeruBlade::toHms($day[$line_index]['timestamped_end_work_time']) }}</td>
                        <td class="{{ $day[$line_index]['color_statuses']['real_total_early_arrive_and_overtime']['color'] }}">{{ $day[$line_index]['real_total_early_arrive_and_overtime'] }}</td>
                        <td class="{{ $day[$line_index]['color_statuses']['real_break_time']['color'] }}">{{ $day[$line_index]['real_break_time'] }}</td>
                        @if (isset($display_go_out) && $display_go_out == true)
                            <td class="{{ $day[$line_index]['color_statuses']['real_go_out_time']['color'] . ((isset($day[$line_index]['go_out_red_number']) && $day[$line_index]['go_out_red_number'] == true) ? ' red' : '') }}">{{ $day[$line_index]['real_go_out_time'] }}</td>
                        @endif
                        <td class="{{ $day[$line_index]['color_statuses']['real_total_late_and_leave_early']['color'] }}">{{ $day[$line_index]['real_total_late_and_leave_early'] }}</td>
                    @endif
                @endforeach
            </tr>
        @endfor
    @endif
@endforeach
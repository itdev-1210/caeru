@extends('layouts.print')

@section('title', '個人別詳細')

@push('scripts')
  <script defer src="{{ asset('/js/mics.js') }}" defer></script>
  <script defer src="{{ asset('/js/pages/employee_detail_print.js') }}"></script>
@endpush

@section('content')
  <section id="print_wrapper">
    <section id="print">
      <section class="fix_position">
        <section class="fix_position_inner">
          <section class="left">
            <p class="button right_10"><a class="m_size l_height btn_gray l_font" href="{{ Caeru::route('breadcrumb_go_back') }}">戻る</a></p>
          </section>
            <section class="right">
              <p v-if="displayWorkPlace" class="button right_10"><a class="m_size l_height btn_gray l_font" @click="switchView">詳細表示</a></p>
              <p v-else class="button right_10"><a class="m_size l_height btn_gray l_font" @click="switchView">勤務地表示</a></p>
              <p class="button"><a class="ss_size l_height btn_greeen l_font" @click="detailPrint">印刷</a></p>
          </section>
        </section>
      </section>
      <main id="attendance_detail">
        <section class="text_bold top_60 lll_font">
          <span class="right_30">@{{ year }}年@{{ month }}月度</span>
          <span class="right_30">{{ $employee->presentation_id }}</span><span class="right_30">{{ $employee->fullName() }}</span><span>{{ $employee->scheduleType() }}</span>
        </section>
        <section class="person_result_table">
          <table class="left s_60">
            <tr>
              <th class="s_8" rowspan="2"></th>
              <th class="s_10 bg_dark_gray" rowspan="2">勤務日数<p>({{ $summarized_data['have_schedule_days'] }}日)</p></th>
              <th class="s_10 bg_dark_gray" rowspan="2">総勤務時間</th>
              <th class="s_28" colspan="4">所定( {{ CaeruBlade::toHourMinute($summarized_data['schedule_working_time']) }} )</th>
              <th class="s_10" rowspan="2">時間外</th>
            </tr>
            <tr>
              <th class="s_10">所定内</th>
              <th class="s_8">有給</th>
              <th class="s_8">無給</th>
              <th class="s_8">不就労</th>
            </tr>
            <tr>
              <td>実績</td>
              <td>{{ $summarized_data['real_work_days'] }}</td>
              <td>{{ CaeruBlade::toHourMinute($summarized_data['real_working_time']) }}</td>
              <td>{{ CaeruBlade::toHourMinute($summarized_data['real_work_span_time']) }}</td>
              <td>{{ CaeruBlade::toHourMinute($summarized_data['real_paid_rest_time']) }}</td>
              <td>{{ CaeruBlade::toHourMinute($summarized_data['real_unpaid_rest_time']) }}</td>
              <td>{{ CaeruBlade::toHourMinute($summarized_data['real_non_work_time']) }}</td>
              <td>{{ CaeruBlade::toHourMinute($summarized_data['real_overtime_work_time']) }}</td>
            </tr>
          </table>
          <table class="left s_28 secound both_1">
            <tr>
              <th class="s_28">深夜勤務</th>
              <th class="s_28">欠勤日数</th>
              <th><p>年次有給取得</p><p>日:時間:分</p></th>
            </tr>
            <tr>
              <td>{{ CaeruBlade::toHourMinute($summarized_data['real_night_work_time']) }}</td>
              <td>{{ $summarized_data['real_kekkin_days'] }}日</td>
              <td>{{ $summarized_data['real_taken_paid_rest_days'] }}日{{ CaeruBlade::toHourMinute($summarized_data['real_taken_paid_rest_time']) }}</td>
            </tr>
          </table>
          <table class="right s_10 secound">
            <tr>
              <th class="s_20"><p>年次有給残日数</p><p>日:時間:分</p></th>
            </tr>
            <tr>
              <td>
                <span>{{ ($real_available_paid_holidays ? $real_available_paid_holidays : 0) . '日' . ($real_available_paid_holidays_hour ? CaeruBlade::toHourMinute($real_available_paid_holidays_hour) : '00:00') }}</span>
              </td>
            </tr>
          </table>
        </section>
        <section class="select_one2">
          <section class="left_position">
            <div class="left right_10 description_day_off1">法定休日</div>
            <div class="left right_10 description_day_off2">一般休日</div>
            <div class="left right_10 description_confirm_list">要確認</div>
            <div class="left right_10 description_mistake description_text">誤った記録</div>
            <div class="left right_10 description_request description_text">申請中</div>
            <div class="left right_10 description_approval description_text">承認</div>
            <div class="left right_10 description_reject description_text">否決</div>
            <div class="left description_correction description_text">修正</div>
          </section>
        </section>
        <section>
          <preview-print :display-work-place="displayWorkPlace"></preview-print>
        </section>
      </main>
    </section>
  </section>
@endsection
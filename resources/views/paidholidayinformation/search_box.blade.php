<section class="search_box_wrapper">
    <div class="search_box_innner2">
        <form action="{{Caeru::route('search_paid_holiday')}}" method="post">
            {{csrf_field()}}
            <div class="check_box_wrap search_setting">
                <label class="checkbox_text"><input name="isCheck" type="checkbox" {{ session()->get('isCheck_old_input', '1') == 1 ? ' checked':'' }} value="1">更新必要者</label>
            </div>
            <div class="search_setting">
                <span class="right_10">従業員ID</span>
                <input class="s_size"  name="presentation_id" type="text" value="{{ session()->get('presentation_id_old_input', '') }}">
            </div>
            <div class="search_setting">
                <span class="right_10">従業員名</span>
                <input type="text" name="first_name" class="m_size" value="{{ session()->get('first_name_old_input', '') }}">
            </div>
            <div class="search_box search_setting">
                <span class="right_10 left">採用形態</span>
                <div class="selectbox left">
                    <select class="m_size" name="employment_type">
                        <option {{ session()->get('employment_type_old_input', '') == '' ? ' selected':'' }} value=""></option>
                        <option {{ session()->get('employment_type_old_input', '') == 1 ? ' selected':'' }} value="1">{{ $employment_types[1] }}</option>
                        <option {{ session()->get('employment_type_old_input', '') == 2 ? ' selected':'' }} value="2">{{ $employment_types[2] }}</option>
                        <option {{ session()->get('employment_type_old_input', '') == 3 ? ' selected':'' }} value="3">{{ $employment_types[3] }}</option>
                        <option {{ session()->get('employment_type_old_input', '') == 4 ? ' selected':'' }} value="4">{{ $employment_types[4] }}</option>
                        <option {{ session()->get('employment_type_old_input', '') == 5 ? ' selected':'' }} value="5">{{ $employment_types[5] }}</option>
                    </select>
                </div>
            </div>
            <div class="search_box search_setting">
                <span class="right_10">部署</span>
                <section  class="form-group mm_size">
                    <select name ="department[]" class="mm_size" id="ms" multiple="multiple">
                        @foreach($departments as $department)
                        <option value="{{ $department->id }}" {{ in_array($department->id, (array)session()->get('department_old_input', []))?' selected':'' }}>{{ $department->name }}</option>
                        @endforeach
                    </select>
                </section>
            </div>
            <div class="search_box search_setting">
                <span class="right_10 left">雇用状態</span>
                <div class="selectbox left">
                    <select class="s_size"  name="work_status">
                        <option {{ session()->get('work_status_old_input', '') == 1 ? ' selected':'' }} value="1">{{ $work_statuses[1] }}</option>
                        <option {{ session()->get('work_status_old_input', '') == 2 ? ' selected':'' }} value="2">{{ $work_statuses[2] }}</option>
                        <option {{ session()->get('work_status_old_input', '') == 3 ? ' selected':'' }} value="3">{{ $work_statuses[3] }}</option>
                    </select>
                </div>
            </div>
            <div class="search_box search_setting">
                <span class="right_10 left">週勤務日数</span>
                <div class="selectbox left">
                    <select class="s_size" name = "holiday_bonus_type">
                        <option {{ session()->get('holiday_bonus_type_old_input', '') == 1 ? ' selected':'' }} value="1">{{ $holiday_bonus_types[1] }}</option>
                        <option {{ session()->get('holiday_bonus_type_old_input', '') == 2 ? ' selected':'' }} value="2">{{ $holiday_bonus_types[2] }}</option>
                        <option {{ session()->get('holiday_bonus_type_old_input', '') == 3 ? ' selected':'' }} value="3">{{ $holiday_bonus_types[3] }}</option>
                        <option {{ session()->get('holiday_bonus_type_old_input', '') == 4 ? ' selected':'' }} value="4">{{ $holiday_bonus_types[4] }}</option>
                        <option {{ session()->get('holiday_bonus_type_old_input', '') == 5 ? ' selected':'' }} value="5">{{ $holiday_bonus_types[5] }}</option>
                        <option {{ session()->get('holiday_bonus_type_old_input', '') == 6 ? ' selected':'' }} value="6">{{ $holiday_bonus_types[6] }}</option>
                    </select>
                </div>
            </div>
            <div class="search_box search_setting">
                <span class="right_10">入社日</span>
                <input class="m_size right_10" name="joined_day_start" type="text" value="{{ session()->get('joined_day_start_old_input', '') }}" placeholder="yyyy-mm-dd">
                <span class=" right_10">〜</span>
                <input class="m_size right_10" name="joined_day_end" type="text"  value="{{ session()->get('joined_day_end_old_input', '') }}" placeholder="yyyy-mm-dd">
            </div>
            <div class="search_box search_setting">
                <span class="right_10" >勤続年数</span>
                <input class="ss_size" name="joined_year_start" type="text"  value="{{ session()->get('joined_year_start_old_input', '') }}">
                <span class="select_both_space">年</span>
                <input class="ss_size right_4" name="joined_month_start" type="text" value="{{ session()->get('joined_month_start_old_input', '') }}">
                <span class="right_10">ヶ月</span>
                <span class=" right_10">〜</span>
                <input class="ss_size right_10" name="joined_year_end" type="text" value="{{ session()->get('joined_year_end_old_input', '') }}">
                <span class="right_10">年</span>
                <input class="ss_size right_4" name="joined_month_end" type="text" value="{{ session()->get('joined_month_end_old_input', '') }}">
                <span>ヶ月</span>
            </div>
            <div class="search_setting">
                <span class="right_10">有給更新日</span>
                <input class="ss_size" name="holidays_update_day" type="text" value="{{ session()->get('holidays_update_day_old_input', '') }}" placeholder="mm-dd">
            </div>
            <div class="search_box search_setting">
                <span class="right_10">有給更新日が</span>
                <input class="ss_size" name="holidays_update_day_start_month" type="text" value="{{ session()->get('holidays_update_day_start_month_old_input', '') }}" placeholder="mm">
                <span class="select_both_space">月</span>
                <input class="ss_size right_4" name="holidays_update_day_start_day" type="text" value="{{ session()->get('holidays_update_day_start_day_old_input', '') }}" placeholder="dd">
                <span class="right_10">日</span>
                <span class=" right_10">〜</span>
                <input class="ss_size right_10" name="holidays_update_day_end_month" type="text" value="{{ session()->get('holidays_update_day_end_month_old_input', '') }}" placeholder="mm">
                <span class="right_10">月</span>
                <input class="ss_size right_4" name="holidays_update_day_end_day" type="text" value="{{ session()->get('holidays_update_day_end_day_old_input', '') }}" placeholder="dd">
                <span class="right_10">日のうち従業員の出勤率が</span>
                <input class="ss_size right_4" name="attendance_rate" type="text" value="{{ session()->get('attendance_rate_old_input', '') }}">
                <span>％未満</span>
            </div>
            
            <div class="check_box_wrap search_setting">
                <label class="checkbox_text">
                    <input name="paid_holiday_exception" type="checkbox" {{ session()->get('paid_holiday_exception_old_input', '') == 1 ? ' checked':'' }} value="1">有給対象外
                </label>
            </div>
            <div class="button bottom_10 search_setting right_10">
                <input type="hidden" name="isSearch" value="1">
                <button type="submit" class="s_size s_height btn_greeen" href="#">検索</button>
            </div>
            <div class="button bottom_10 search_setting">
                <button type="reset" class="s_size s_height btn_gray" href="#">リセット</button>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    $("button[type=reset]").on('click', function(e) {
        e.preventDefault();
        $(this).closest('form').find('input[type=text], select').val('');
        $(this).closest('form').find('input[name=paid_holiday_exception]').prop("checked", false);

    });

</script>
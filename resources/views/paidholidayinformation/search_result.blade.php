<section class="pager">
    <section class="left_position">
        <section class="side_input_block right_10">
            <form action="{{Caeru::route('paid_holiday_apart_koushin')}}" method="post">
                <span class="right_10">チェックしたものを</span>
            <p class="button">
                {{csrf_field()}}
                <input type="hidden" name="employeeIdArray" value="">
                <button type="submit" class="koushin ss_size s_height btn_white" href="#">更新</button>
             </p>
            </form>
        </section>
        <section class="side_input_block">
            <form action="{{Caeru::route('paid_holiday_all_koushin')}}" method="post">
                {{csrf_field()}}
                <p class="button">
                        <button type="submit" class="s_size s_height btn_white" href="#">全員を更新</button>
                </p>              
            </form>
        </section>
    </section>

    {{ $employees->links(null, ['sum_line' => true]) }}

</section>
<section class="approval_table">
    <table _fixedhead='rows:1; cols:1;'>
        <tr>
            <th class="s_4">
                <div class="check_onle_wrap" >
                    <label class="checkbox_box"><input id ="multicheck" name="" type="checkbox" value="hoge"></label>
                </div>
            </th>
            <th class="s_6">従業員ID</th>
            <th class="s_8">従業員名</th>
            @if($current_work_location==='会社')
            <th class="s_12">所属先</th>
            @endif
            <th class="s_6">週勤務日数</th>
            <th class="s_8">入社日</th>
            <th class="s_6">勤続年数</th>
            <th class="s_4">更新日</th>
            <th class="s_8">更新前年度出勤率</th>
            <th class="s_5">付与日数</th>
            <th class="s_8"><p>繰越日数</p><p>日:時間:分</p></th>
            <th class="s_8"><p>残日数</p><p>日:時間:分</p></th>
            <th>操作年月日</th>
            <th class="s_5">更新</th>
            <th class="s_5"></th>
        </tr>
        @foreach($employees as $employee)
            @php
                list($availableHour, $availableMinute) = $employee->getAvailablePaidHolidayHour();
                list($consumedHour, $consumedMinute) = $employee->getConsumedPaidHolidayHour();
                list($carriedforwardHour, $carriedforwardMinute) = $employee->getCarriedForwardPaidHolidayHour();
            @endphp
            <tr>
                <td>
                    <div class="check_onle_wrap">
                        <label class="checkbox_box"><input name="employeekoushin" type="checkbox" value="{{ $employee->id }}"></label>
                    </div>
                </td>
                <td>{{ $employee->presentation_id }}</td>
                <td>{{ $employee->last_name . $employee->first_name }}</td>
                @if($current_work_location==='会社')
                <td>{{ $employee->name }}</td>
                @endif
                <td>{{ $holiday_bonus_types[$employee->holiday_bonus_type] }}</td>
                <td>{{ $employee->joined_date }}</td>
                <td>{{ $employee->getEmployeeJoinedDate($employee->joined_date) }}</td>
                <td>{{ $employee->holidays_update_day }}</td>
                <td>
                    @if($employee->hasNotFirstKoushin())
                        @if(!is_null($employee->attendance_rate))
                            {{ $employee->attendance_rate . '%' }}
                        @endif
                    @else
                        <span></span>
                    @endif
                </td>
                <td>
                    @if($employee->hasNotFirstKoushin())
                        {{ $employee->provided_paid_holidays }}
                    @else
                        <span></span>
                    @endif
                </td>
                <td>
                    @if($employee->hasNotFirstKoushin())
                        {{ $employee->carried_forward_paid_holidays }}日{{ $carriedforwardHour}}:{{ $carriedforwardMinute }}
                    @else
                        <span></span>
                    @endif

                </td>
                <td>
                    @if($employee->hasNotFirstKoushin())
                        {{ $employee->available_paid_holidays }}日{{ $availableHour}}:{{ $availableMinute }}
                    @else
                        <span></span>
                    @endif
               </td>
                <td>
                    @if($employee->hasNotFirstKoushin())
                        {{ $employee->last_modified_date }}
                    @else
                        <span></span>
                    @endif
                </td>
                <td>
                    @if($employee->hasKoushin())
                    <form action="{{Caeru::route('paid_holiday_koushin', [$employee->id]) }}" method='post'>
                        {{csrf_field()}}                        
                        <input type="hidden" name="carried_forward_paid_holidays" value="{{ $employee->carried_forward_paid_holidays }}">
                        <input type="hidden" name="available_paid_holidays_hour" value="{{ $employee->carried_forward_paid_holidays_hour }}">   
                        <input type="hidden" name="consumed_paid_holidays" value="{{ $employee->consumed_paid_holidays }}">
                        <input type="hidden" name="consumed_paid_holidays" value="{{ $employee->consumed_paid_holidays_hour }}">    
                        <input type="hidden" name="available_paid_holidays" value="{{ $employee->available_paid_holidays }}">
                        <input type="hidden" name="available_paid_holidays" value="{{ $employee->available_paid_holidays_hour }}">
                        <p class="button"><button type="submit" class="ss_size s_height btn_white" href="#">更新</button></p>
                    </form>
                    @endif
                </td>
                <td>
                    <p class="button">
                        <a class="ss_size s_height btn_gray" href="{{Caeru::route('edit_paid_holiday', [$employee->id, $employees->currentPage()]) }}">詳細</a>
                    </p>
                </td>
            </tr>
        @endforeach
    </table>
</section>
<section class="pager">
    {{ $employees->links(null, ['sum_line' => false]) }}
</section>
<script>
    $(".koushin").closest('form').on('submit', function(){
        var employeeIdArray = [];
        $('input[name="employeekoushin"]:checked').each(function() {
            employeeIdArray.push($(this).val());
        });
        $('input[name="employeeIdArray"]').val(employeeIdArray.join(','));
        $(this).submit();
    });

    $('#multicheck').on('click',function(){ 
        $('input[name="employeekoushin"]:checkbox').prop("checked", $(this).prop("checked"));
    });
</script>
@extends('sinsei.layouts.master')

@section('title', 'ログイン')

@push('scripts')
    <script defer src="{{ asset('/js/mics.js') }}" defer></script>
    <script defer src="{{ asset('/js/components/alert_module.js') }}" defer></script>
@endpush

@section('content')
    <main>
        <section class="alert_wrapper">
            <section class="alert_innner">
                <section class="alert_box save_green"><span class="alert_ico right_10"><img src="{{ asset('../../../images/ico_alert_save.svg') }}"></span>
                    @if (session()->has('success'))
                        <span class="alert_content">{{ session('success') }}</span>
                    @endif
                </section>
                <section class="alert_box save_yellow"><span class="alert_ico right_10"><img src="{{ asset('../../../images/ico_alert_dakoku.svg') }}"></span>
                    @if (session()->has('warning'))
                        <span class="alert_content">{{ session('warning') }}</span>
                    @endif
                </section>
                <section class="alert_box save_red"><span class="alert_ico right_10"><img src="{{ asset('../../../images/ico_alert_error.svg') }}"></span>
                    @if (count($errors) || session()->has('error'))
                        <span class="alert_content">{{ session()->has('error') ? session('error') : '入力に誤りがあります' }}</span>
                    @endif
                </section>
            </section>
        </section>
        <section id ="login_wrapper">
            <form method="POST" form-single-submit action="{{ Caeru::route('ss_show_login') }}">
                {{ csrf_field() }}
                <section class="login_logo bottom_20"><img src="{{ asset('../../../images/logo.svg') }}"></section>
                <div>
                    @if ($errors->has('auth_failed'))
                        <p class="auth_failed_message">{{ $errors->first('auth_failed') }}</p>
                    @endif
                    <section class="input_panel_row">
                        <span class="s_size left">従業員ID</span>
                        @component('layouts.form.error', ['field' => 'presentation_id'])
                            <input type="text" name="presentation_id" class="m_size left" value="{{ old('presentation_id') }}">
                        @endcomponent
                    </section>
                    <section class="input_panel_row">
                        <span class="s_size left">パスワード</span>
                        @component('layouts.form.error', ['field' => 'password'])
                            <input type="password" name="password" class="m_size left">
                        @endcomponent
                    </section>
                </div>
                <section class ="btn">
                    <p class="button">
                        <button type="submit" class ="mm_size l_height btn_greeen l_font" >ログイン</button>
                    </p>
                </section>
            </form>
        <section class="browser">当サイトはGoogle Chrome・Microsoft Edgeを推奨しております</section>
    </main>
@endsection
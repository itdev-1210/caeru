@extends('sinsei.layouts.master')

@section('title', '勤怠申請管理 ')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 1, 'requester_mode' => false ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/sinsei/approver/find_employee_working_month.js') }}"></script>
@endpush

@section('content')
    <main id="manager_list">
        <h1>勤怠申請管理</h1>
        <section class="select_one2">
                <section class="select_one_inner">
                    <section class="right_30 ico_position">
                        <a @click="previousMonth"><img class="ico_ico_arrow" src="{{ asset('images/sinsei/ico_arrow_left1.svg') }}"></a>
                    </section>
                    <div class="selectbox side_input_block right_10">
                        <select class="s_size" v-model.number="selectedYear">
                                <option v-for="year in yearRange" :value="year">@{{ year }}年</option>
                        </select>
                    </div>
                    <div class="selectbox side_input_block right_30">
                        <select class="s_size" v-model.number="selectedMonth">
                                <option v-for="month in monthRange" :value="month">@{{ month }}月度</option>
                        </select>
                    </div>
                    <section class="ico_position">
                        <a @click="nextMonth"><img class="ico_ico_arrow" src="{{ asset('images/sinsei/ico_arrow_right1.svg') }}"></a>
                    </section>
                </section>
        </section>
        <section class="pager">
            {{ $employees_approval->links(null, ['sum_line' => true]) }}
        </section>
        <section class="manager_table have_table_with_fixed_header">
            <table _fixedhead='rows:1; cols:2;' class="table_with_fixed_header">
                <tr class="short_height fixed_header">
                    <th class="s_10">従業員番号</th>
                    <th>就労形態</th>
                    <th class="s_30">従業員名</th>
                    <th class="s_20">申請中</th>
                </tr>
                @foreach ($employees_approval as $list)
                    <tr>
                        <td>{{ $list->presentation_id }}</td>
                        <td>{{ Constants::scheduleTypes()[$list->schedule_type] }}</td>
                        <td><a href="{{ Caeru::route('sn_approve_manager_approve_detail', [$list->id, $business_month]) }}">{{ $list->last_name . $list->first_name}}</a></td>
                        <td>
                            @if(isset($list->number_of_request))
                                <a href="{{ Caeru::route('ss_show_approval_form_with_month', [$list->id, $business_month]) }}">{{$list->number_of_request}}件</a>
                            @else
                                <a href="">{{0}}件</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </section>
      <section class="pager">
            {{ $employees_approval->links(null, ['sum_line' => false]) }}
        </section>
    </main>
@endsection
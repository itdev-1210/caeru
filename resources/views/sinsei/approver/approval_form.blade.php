@extends('sinsei.layouts.master')

@section('title', '申請中')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 1, 'requester_mode' => false ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/sinsei/approver/approval_form.js') }}"></script>
@endpush

@section('content')
    <main id="request_page">
        <snapshot-day-component v-for="(snapshot_day, index) in snapshot_days" :key="index"
            :pkey="index"
            :snapshot-day="snapshot_day"
            :is-snapshot-days="is_snapshot_days"
            :work-locations="work_locations"
            :rest-days="rest_days"
            :national-holidays="national_holidays"
            :flip-color-day="flip_color_day"
            :timestamp-types="timestamp_types"
            :mode-approval="true"
            :employee-id="employee_id"
            @update-snapshot-days="updateSnapshotDays">
        </snapshot-day-component>
    </main>
@endsection
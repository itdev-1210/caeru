@extends('sinsei.layouts.master')

@section('title', '勤怠申請管理 ')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 1, 'requester_mode' => false ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/sinsei/approver/find_employee_working_month.js') }}"></script>
@endpush

@section('content')
    <main id="manager_list">
    <section class="select_one2 bottom_20">
            <section class="select_one_inner request_number">
                <span class="right_10">従業員番号</span><span class="right_30">{{$employees_approval->presentation_id}}</span><span class="right_10">{{ $employees_approval->last_name . $employees_approval->first_name}}</span><span class="right_30">申請
                    @if(isset($total))
                        <a href="{{ Caeru::route('ss_show_approval_form_with_month', [$employees_approval->id, $business_month]) }}">{{$total}}件</a>
                    @else
                        <a href="">{{0}}件</a>
                    @endif
                </span>
            </section>
        </section>
        <section class="select_one2">
            <section class="select_one_inner">
                    <section class="right_30 ico_position">
                        <a @click="previousMonth"><img class="ico_ico_arrow" src="{{ asset('images/sinsei/ico_arrow_left1.svg') }}"></a>
                    </section>
                    <div class="selectbox side_input_block right_10">
                        <select class="s_size" v-model.number="selectedYear">
                                <option v-for="year in yearRange" :value="year">@{{ year }}年</option>
                        </select>
                    </div>
                    <div class="selectbox side_input_block right_30">
                        <select class="s_size" v-model.number="selectedMonth">
                                <option v-for="month in monthRange" :value="month">@{{ month }}月度</option>
                        </select>
                    </div>
                    <section class="ico_position">
                        <a @click="nextMonth"><img class="ico_ico_arrow" src="{{ asset('images/sinsei/ico_arrow_right1.svg') }}"></a>
                    </section>
                </section>
        </section>
        <section class="person_result_table">
            <table class="left s_60">
                <tr>
                    <th class="s_8" rowspan="2"></th>
                    <th class="s_10 bg_dark_gray" rowspan="2">勤務日数({{ $summarized_data['have_schedule_days'] }}日)</th>
                    <th class="s_10 bg_dark_gray" rowspan="2">総勤務時間</th>
                    <th class="s_28" colspan="4">所定({{ CaeruBlade::toHourMinute($summarized_data['schedule_working_time']) }})</th>
                    <th class="s_10" rowspan="2">時間外</th>
                </tr>
                <tr>
                    <th class="s_10">所定内</th>
                    <th class="s_8">有給</th>
                    <th class="s_8">無給</th>
                    <th class="s_8">不就労</th>
                </tr>
                <tr>
                    <td>実績<span class="light_gray">(予測)</span></td>
                    <td>{{$summarized_data['real_work_days']}}<span class="light_gray">({{$summarized_data['planned_work_days']}})</span></td>
                    <td>{{CaeruBlade::toHourMinute($summarized_data['real_working_time'])}}<span class="light_gray">({{CaeruBlade::toHourMinute($summarized_data['planned_working_time'])}})</span></td>
                    <td>{{CaeruBlade::toHourMinute($summarized_data['real_work_span_time'])}}<span class="light_gray">({{CaeruBlade::toHourMinute($summarized_data['planned_work_span_time'])}})</span></td>
                    <td>{{CaeruBlade::toHourMinute($summarized_data['real_paid_rest_time'])}}<span class="light_gray">({{CaeruBlade::toHourMinute($summarized_data['planned_paid_rest_time'])}})</span></td>
                    <td>{{CaeruBlade::toHourMinute($summarized_data['real_unpaid_rest_time'])}}<span class="light_gray">({{CaeruBlade::toHourMinute($summarized_data['planned_unpaid_rest_time'])}})</span></td>
                    <td>{{CaeruBlade::toHourMinute($summarized_data['real_non_work_time'])}}<span class="light_gray">({{CaeruBlade::toHourMinute($summarized_data['planned_non_work_time'])}})</span></td>
                    <td>{{CaeruBlade::toHourMinute($summarized_data['real_overtime_work_time'])}}<span class="light_gray">({{CaeruBlade::toHourMinute($summarized_data['planned_overtime_work_time'])}})</span></td>
                </tr>
            </table>
            <table class="left s_28 secound both_1">
                <tr>
                    <th class="s_28">深夜勤務</th>
                    <th class="s_28">欠勤日数</th>
                    <th><p>年次有給取得</p><p>日:時間:分</p></th>
                </tr>
                <tr>
                    <td>{{CaeruBlade::toHourMinute($summarized_data['real_night_work_time'])}}<span class="light_gray">({{CaeruBlade::toHourMinute($summarized_data['planned_night_work_time'])}})</span></td>
                    <td>{{$summarized_data['real_kekkin_days']}}日<span class="light_gray">({{$summarized_data['planned_kekkin_days']}}日)</span></td>
                    <td>{{$summarized_data['real_taken_paid_rest_days']}}日{{ CaeruBlade::toHourMinute($summarized_data['real_taken_paid_rest_time'])}}
                        <span class="light_gray">({{$summarized_data['planned_taken_paid_rest_days']}}日{{CaeruBlade::toHourMinute($summarized_data['planned_taken_paid_rest_time'])}})</span>
                    </td>
                </tr>
            </table>
            <table class="right s_10 secound">
                <tr>
                    <th class="s_20"><p>年次有給残日数</p><p>日:時間:分</p></th>
                </tr>
                <tr>
                    <td>
                        <span>{{ ($real_available_paid_holidays ? $real_available_paid_holidays : 0) . '日' . ($real_available_paid_holidays_hour ? CaeruBlade::toHourMinute($real_available_paid_holidays_hour) : '00:00') }}</span>
                        <span class="light_gray">({{ ($planned_available_paid_holidays ? $planned_available_paid_holidays : 0) . '日' . ($planned_available_paid_holidays_hour ? CaeruBlade::toHourMinute($planned_available_paid_holidays_hour) : '00:00') }})</span>
                    </td>
                </tr>
            </table>
        </section>
        <section class="select_one3">
            <section class="left_position">
                <section class="side_input_block right_30"><span class="right_10">チェックした勤怠の</span>
                    <p class="button"><button id="btn_selectday" @click="callMutilpleDate({{ $employees_approval->id }})" class="s_size m_height btn_greeen">詳細</button></p>
                    @include('layouts.form.radio_display_toggle',['default' => $display_toggle])
                </section>
            </section>
            <section class="right_position">
                <section class="side_input_block">
                    <div class="left right_10 description_day_off1">法定休日</div>
                    <div class="left right_10 description_day_off2">一般休日</div>
                    <div class="left right_10 description_confirm_list">要確認</div>
                    <div class="left right_10 description_mistake description_text">誤った記録</div>
                </section>
                <section>
                    <div class="left right_10 description_request description_text">申請中</div>
                    <div class="left right_10 description_approval description_text">承認</div>
                    <div class="left right_10 description_reject description_text">否決</div>
                    <div class="left description_correction description_text">修正</div>
                </section>
            </section>
        </section>
        <section class="search_table have_table_with_fixed_header">
            <table _fixedhead='rows:3; cols:1;' class="table_with_fixed_header">
                <tr class="short_height fixed_header">
                    <th class="s_4" rowspan="3"></th>
                    <th class="s_10" colspan="1" rowspan="3">日付</th>
                    <th class="s_14" colspan="1" rowspan="3">勤務地</th>
                    <th class="s_4" rowspan="3"><p>勤務</p><p>形態</p></th>
                    <th class="s_4" rowspan="3"><p>休暇</p><p>形態</p></th>
                    <th colspan="2">出勤</th>
                    <th colspan="2">退勤</th>
                    <th class="s_4" rowspan="3">休憩</th>
                    <th class="s_4" rowspan="3">(内)深休</th>
                    <th class="s_4" rowspan="3">外出</th>
                    <th rowspan="3">遅・早</th>
                    <th class="s_14" colspan="3" rowspan="3">所定内+時間外＝総労働時間</th>
                    <th rowspan="3" class="s_3"></th>
                </tr>
                <tr class="short_height fixed_header_second_row">
                    <th class="s_8" v-if="displayToggle == 1">予定</th>
                    <th class="s_8" v-else>所定</th>
                    <th class="s_8" rowspan="2">計算時刻</th>
                    <th class="s_8" v-if="displayToggle == 1">予定</th>
                    <th class="s_8" v-else>所定</th>
                    <th class="s_8" rowspan="2">計算時刻</th>
                </tr>
                <tr class="short_height fixed_header_third_row">
                    <th>打刻時刻</th>
                    <th>打刻時刻</th>
                </tr>
                @foreach ($working_days as $day => $day_info)
                    @if (count($day_info['data']) == 0)
                        <tr>
                            <td rowspan="2">
                                <div class="check_onle_wrap">
                                    <label class="checkbox_box"><input type="checkbox" value="{{$day}}" v-model="dates"></label>
                                </div>
                            </td>
                            <td rowspan="2">
                                <span class="
                                        {{ ($day_info['day_of_week'] == \Carbon\Carbon::SUNDAY) || ($day_info['national_holiday'] == true) ? 'sunday' : '' }}
                                        {{ $day_info['day_of_week'] == \Carbon\Carbon::SATURDAY ? 'saturday' : '' }}
                                        {{ $day_info['law_rest_day'] == true ? 'pink_holiday' : '' }}
                                        {{ $day_info['normal_rest_day'] == true ? 'blue_holiday' : '' }}
                                    "
                                >
                                    <a href="{{ Caeru::route('ss_show_approval_form', [$employees_approval->id, $day]) }}">{{ $day }}({{ CaeruBlade::toJPDayOfWeek($day_info['day_of_week']) }})</a>
                                </span>
                            </td>
                            <td rowspan="2"></td>
                            <td rowspan="2"></td>
                            <td rowspan="2"></td>
                            <td class="bg_light_gray"></td>
                            <td rowspan="2"></td>
                            <td class="bg_light_gray"></td>
                            <td rowspan="2"></td>
                            <td class="bg_light_gray"></td>
                            <td class="bg_light_gray"></td>
                            <td class="bg_light_gray"></td>
                            <td class="bg_light_gray"></td>
                            <td class="bg_light_gray line_right_none"></td>
                            <td class="bg_light_gray line_right_none"></td>
                            <td class="bg_light_gray"></td>
                            <td rowspan="2">
                                @if ($day_info['concluded_level_two'] == true)
                                    <img class="check_red ico_ico_arrow4" src="{{ asset('images/ico_check_red.svg') }}">
                                @elseif($day_info['concluded_level_one'] == true)
                                    <img class="check_blue ico_ico_arrow4" src="{{ asset('images/ico_check_blue.svg') }}">
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="line_right_none"></td>
                            <td class="line_right_none"></td>
                            <td></td>
                        </tr>
                    @else
                        @foreach ($day_info['data'] as $key => $working_info)
                            <tr>
                                @if($key == 0)
                                    <td rowspan="{{count($day_info['data'])*2}}">
                                        <div class="check_onle_wrap">
                                            <label class="checkbox_box"><input type="checkbox" value="{{ $day }}" v-model="dates"></label>
                                        </div>
                                    </td>
                                    <td rowspan="{{count($day_info['data'])*2}}">
                                        <span class="
                                                {{ ($day_info['day_of_week'] == \Carbon\Carbon::SUNDAY) || ($day_info['national_holiday'] == true) ? 'sunday' : '' }}
                                                {{ $day_info['day_of_week'] == \Carbon\Carbon::SATURDAY ? 'saturday' : '' }}
                                                {{ $day_info['law_rest_day'] == true ? 'pink_holiday' : '' }}
                                                {{ $day_info['normal_rest_day'] == true ? 'blue_holiday' : '' }}
                                            "
                                        >
                                            <a href="{{ Caeru::route('ss_show_approval_form', [$employees_approval->id, $day]) }}">{{ $day }}({{ CaeruBlade::toJPDayOfWeek($day_info['day_of_week']) }})</a>
                                        </span>
                                    </td>
                                @endif
                                <td rowspan="2" class="{{ $working_info['color_statuses']['planned_work_location_id']['color'] }}">
                                    @if ($working_info['planned_work_location_id'] !== null)
                                        {{
                                            ($working_info['planned_work_address_id'] === null) ? $work_locations[$working_info['planned_work_location_id']]['name'] :
                                            $work_locations[$working_info['planned_work_location_id']]['name'] . "&nbsp;&nbsp;&nbsp;&nbsp;" . $work_locations[$working_info['planned_work_location_id']]['addresses'][$working_info['planned_work_address_id']]
                                        }}
                                    @endif
                                </td>
                                <td rowspan="2" class="{{ $working_info['color_statuses']['work_status_id']['color'] }}">{{ $working_info['work_status_id'] ? $all_work_statuses[$working_info['work_status_id']] : '' }}</td>
                                <td rowspan="2" class="{{ $working_info['color_statuses']['rest_status_id']['color'] }}">{{ $working_info['rest_status_id'] ? $all_rest_statuses[$working_info['rest_status_id']] : '' }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['planned_start_work_time']['color'] }}" v-if="displayToggle == 1">{{ CaeruBlade::toHms($working_info['planned_start_work_time']) }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['schedule_start_work_time']['color'] }}" v-else>{{ CaeruBlade::toHms($working_info['schedule_start_work_time']) }}</td>
                                <td rowspan="2" class="{{ $working_info['color_statuses']['real_start_work_time']['color'] }}">{{ CaeruBlade::toHms($working_info['real_start_work_time']) }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['planned_end_work_time']['color'] }}" v-if="displayToggle == 1">{{ CaeruBlade::toHms($working_info['planned_end_work_time']) }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['schedule_end_work_time']['color'] }}" v-else>{{ CaeruBlade::toHms($working_info['schedule_end_work_time']) }}</td>
                                <td rowspan="2" class="{{ $working_info['color_statuses']['real_end_work_time']['color'] }}">{{ CaeruBlade::toHms($working_info['real_end_work_time']) }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['planned_break_time']['color'] }}">{{ $working_info['planned_break_time'] }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['planned_night_break_time']['color'] }}">{{ $working_info['planned_night_break_time'] }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['planned_go_out_time']['color'] }}">{{ $working_info['planned_go_out_time'] }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['planned_total_late_and_leave_early']['color'] }}">{{ $working_info['planned_total_late_and_leave_early'] }}</td>
                                <td class="bg_light_gray line_right_none {{ $working_info['color_statuses']['planned_work_span']['color'] }}">{{ CaeruBlade::toHourMinute($working_info['planned_work_span']) }}</td>
                                <td class="bg_light_gray line_right_none {{ $working_info['color_statuses']['planned_total_early_arrive_and_overtime']['color'] }}">{{ CaeruBlade::toHourMinute($working_info['planned_total_early_arrive_and_overtime']) }}</td>
                                <td class="bg_light_gray {{ $working_info['color_statuses']['planned_working_hour']['color'] }}">{{ CaeruBlade::toHourMinute($working_info['planned_working_hour']) }}</td>
                                <td rowspan="2">
                                    @if ($day_info['concluded_level_two'] == true)
                                        <img class="check_red ico_ico_arrow4" src="{{ asset('images/ico_check_red.svg') }}">
                                    @elseif($day_info['concluded_level_one'] == true)
                                        <img class="check_blue ico_ico_arrow4" src="{{ asset('images/ico_check_blue.svg') }}">
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="{{ $working_info['color_statuses']['timestamped_start_work_time']['color'] . (isset($working_info['start_time_red_number']) && $working_info['start_time_red_number'] == true ? ' red' : '') }}">{{ CaeruBlade::toHms($working_info['timestamped_start_work_time']) }}</td>
                                <td class="{{ $working_info['color_statuses']['timestamped_end_work_time']['color'] . (isset($working_info['end_time_red_number']) && $working_info['end_time_red_number'] == true ? ' red' : '') }}">{{ CaeruBlade::toHms($working_info['timestamped_end_work_time']) }}</td>
                                <td class="{{ $working_info['color_statuses']['real_break_time']['color'] }}">{{ $working_info['real_break_time'] }}</td>
                                <td class="{{ $working_info['color_statuses']['real_night_break_time']['color'] }}">{{ $working_info['real_night_break_time'] }}</td>
                                <td class="{{ $working_info['color_statuses']['real_go_out_time']['color'] . ( isset($working_info['go_out_red_number']) && $working_info['go_out_red_number'] == true ? ' red' : '') }}">{{ $working_info['real_go_out_time'] }}</td>
                                <td class="{{ $working_info['color_statuses']['real_total_late_and_leave_early']['color'] }}">{{ $working_info['real_total_late_and_leave_early'] }}</td>
                                <td class="line_right_none {{ $working_info['color_statuses']['real_work_span']['color'] }}">{{ CaeruBlade::toHourMinute($working_info['real_work_span'], true) }}</td>
                                <td class="line_right_none {{ $working_info['color_statuses']['real_total_early_arrive_and_overtime']['color'] }}">{{ CaeruBlade::toHourMinute($working_info['real_total_early_arrive_and_overtime'], true) }}</td>
                                <td class="{{ $working_info['color_statuses']['real_working_hour']['color'] }}">{{ CaeruBlade::toHourMinute($working_info['real_working_hour'], true) }}</td>
                            </tr>
                        @endforeach
                    @endif
                @endforeach
            </table>
        </section>
    </main>
@endsection
@push('scripts')
    <script defer src="{{ asset('/js/mics.js') }}" defer></script>
    <script defer src="{{ asset('/js/components/alert_module.js') }}" defer></script>
@endpush

@if ($requester_mode)
	<section class="no_fixed_position" id="no_fixed_position">
		<header>
			<section class="alert_wrapper">
				<section class="alert_innner">
					<section class="alert_box save_green"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_save.svg') }}"></span>
						@if (session()->has('success'))
							<span class="alert_content">{{ session('success') }}</span>
						@endif
					</section>
					<section class="alert_box save_yellow"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_dakoku.svg') }}"></span>
						@if (session()->has('warning'))
							<span class="alert_content">{{ session('warning') }}</span>
						@endif
					</section>
					<section class="alert_box save_red"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_error.svg') }}"></span>
						@if (count($errors) || session()->has('error'))
							<span class="alert_content">{{ session()->has('error') ? session('error') : '入力に誤りがあります' }}</span>
						@endif
					</section>
				</section>
			</section>
			<a href="{{ Caeru::route('ss_requester_working_month') }}">
				<div id="logo" class="left"></div>
			</a>
			<div class="left">
				<p class="conditions text_bold">個人モード
				</p>
			</div>
			<div class="left name_setting left_30">
				<p class="side_input_block right_30"><span class="right_10">勤務形態</span><span>{{ Constants::scheduleTypes()[session('sinsei_user')->schedule_type] }}</span></p>
				<p class="side_input_block right_30"><span class="right_10">従業員番号</span><span>{{ session('sinsei_user')->presentation_id }}</span></p>
				<p class="side_input_block"><span class="right_10">名前</span><span>{{ session('sinsei_user')->last_name . session('sinsei_user')->first_name }}</span></p>
			</div>
			<div id="sign_out">
				<a href="{{ Caeru::route('ss_logout') }}">ログアウト</a>
			</div>
			<section class="right right_30 request_btn">
				@if(isset($sumtotal))
				<p class="button"><a class="m_size l_height btn_red" href="{{ Caeru::route('sn_approve_manager_approve',[$business_month]) }}">申請中@if($sumtotal == null){{0}}@else{{$sumtotal}}@endif件</a></p>
				@endif
			</section>
		</header>
		<nav>
			<ul class="gnav" id="dropmenu">
				<li>
					<a href="{{ Caeru::route('ss_requester_working_month', [$business_month]) }}" class="{{ $active == 1 ? 'nav_select':'' }}"><img src="{{ asset('images/ico_kintai.svg') }}">勤怠情報</a>
				</li>
				<li>
					<a href="{{ Caeru::route('ss_show_account_setting') }}" class="{{ $active == 2 ? 'nav_select':'' }}"><img src="{{ asset('images/ico_account.svg') }}">アカウント情報</a>
				</li>
				@if($can_view_payment_page)
				<li>
					<a href="#" class="{{ $active == 3 ? 'nav_select':'' }}"><img src="{{ asset('images/ico_payment.svg') }}">前払</a>
					<ul>
						@if($can_view_maebarai_page)
						<li><a href="{{ Caeru::route('ss_maebarai_page') }}">前払</a></li>
						@endif
						<li><a href="{{ Caeru::route('ss_maebarai_setting') }}">口座情報</a></li>
					</ul>
				</li>
				@endif
			</ul>
		</nav>
	</section>
@else
<section class="no_fixed_position" id="header_weapper">
	<header>
		<section class="alert_wrapper">
			<section class="alert_innner">
				<section class="alert_box save_green">
					<span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_save.svg') }}"></span>
					@if (session()->has('success'))
						<span class="alert_content">{{ session('success') }}</span>
					@endif
				</section>
				<section class="alert_box save_yellow">
					<span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_dakoku.svg') }}"></span>
					@if (session()->has('warning'))
						<span class="alert_content">{{ session('warning') }}</span>
					@endif
				</section>
				<section class="alert_box save_red">
					<span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_error.svg') }}"></span>
					@if (count($errors) || session()->has('error'))
						<span class="alert_content">{{ session()->has('error') ? session('error') : '入力に誤りがあります' }}</span>
					@endif
				</section>
			</section>
		</section>
		<a href="{{ Caeru::route('sn_approve_manager_approve', [$business_month]) }}">
			<div id="logo" class="left"></div>
		</a>
		<div class="left">
			<p class="conditions text_bold">管理モード</p>
		</div>
		<div id="sign_out">
			<a href="{{ Caeru::route('ss_logout') }}">ログアウト</a>
		</div>
		<section class="right right_30 request_btn">
			<p class="button">
				<a class="m_size l_height btn_red" href="{{ Caeru::route('ss_requester_working_month', ['business_month' => 'reset']) }}">個人ページへ</a>
			</p>
		</section>
	</header>
	<nav id="manager">
		<ul>
			<li>
				<a href="{{ Caeru::route('sn_approve_manager_approve') }}" class="nav_select"><img src="{{ asset('images/ico_sinsei.svg') }}">勤怠申請管理</a>
			</li>
		</ul>
	</nav>
</section>
@endif


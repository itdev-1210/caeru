@extends('sinsei.layouts.master')

@section('title', '前払')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 3, 'requester_mode' => true ])
@endsection

@section('content')
    <main id="payment_account">
        <h1>現在の前払可能額</h1>
        <form id="maebarai_page" method="POST" form-single-submit action="{{ Caeru::route('ss_maebarai_decision') }}">
            {{ csrf_field() }}
            <input name="can_pay_money" type="hidden" value="{{ $maebarai_money }}">
            <section class="account_inner">
                <p class="description bottom_20"><span class="lll_font">{{ $maebarai_money }}円</span></p>
                <div class="money_input">
                    <input class="m_size right_10" name="maebarai_pay_money" type="text" value="{{ old('maebarai_pay_money') }}">
                    <p class="ll_font">円</p>
                </div>
            </section>
            <section class="btn">
                <p class="button"><button type="submit" class="m_size l_height btn_greeen">前払</button></p>
            </section>
        </form>
        <section class="btn">
            <p class="button"><a class="m_size l_height btn_greeen" href="{{ Caeru::route('ss_maebarai_history_page') }}">前払履歴</a></p>
        </section>
    </main>
@endsection
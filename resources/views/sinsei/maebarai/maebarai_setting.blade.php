@extends('sinsei.layouts.master')

@section('title', '口座情報')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 3, 'requester_mode' => true ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/sinsei/requester/maebarai_setting.js') }}"></script>
@endpush

@section('content')
    <main id="personal_account">
        <section id="bank_info">
            <h1>口座情報</h1>
            <section class="account_inner">
                <div class="new_pass bottom_20">
                    <span class="mm_size left right_30">前払計算時給</span>
                    <template v-if="data.settings.maebarai_salary_per_hour == null">
                        <span class="red">前払計算時給が設定されておりません。管理者にお問い合わせください。</span>                                    
                    </template>
                    <template v-else>
                        @{{ data.settings.maebarai_salary_per_hour }}
                        <span>円</span>
                    </template>
                </div>
                <div class="new_pass bottom_20">
                    <span class="mm_size left right_30"><span class="required">必須</span>金融機関コード</span>
                    <error-display :message="errors.maebarai_bank_code">
                        <input class="s_size left" ref="maebarai_bank_code" type="text" v-model="data.settings.maebarai_bank_code" :disabled="data.settings.maebarai_salary_per_hour == null">
                    </error-display>
                </div>
                <div class="new_pass bottom_20">
                    <span class="mm_size left right_30"><span class="required">必須</span>金融機関名</span>
                    <error-display :message="errors.maebarai_bank_name">
                        <input class="l_size left" ref="maebarai_bank_name" type="text" v-model="data.settings.maebarai_bank_name" :disabled="data.settings.maebarai_salary_per_hour == null">
                    </error-display>
                </div>
                <div class="new_pass bottom_20">
                    <span class="mm_size left right_30"><span class="required">必須</span>支店コード</span>
                    <error-display :message="errors.maebarai_branch_code">
                        <input class="s_size left" ref="maebarai_branch_code" type="text" v-model="data.settings.maebarai_branch_code" :disabled="data.settings.maebarai_salary_per_hour == null">
                    </error-display>
                </div>
                <div class="new_pass bottom_20">
                    <span class="mm_size left right_30"><span class="required">必須</span>支店名</span>
                    <error-display :message="errors.maebarai_branch_name">
                        <input class="l_size left" ref="maebarai_branch_name" type="text" v-model="data.settings.maebarai_branch_name" :disabled="data.settings.maebarai_salary_per_hour == null">
                    </error-display>
                </div>
                <div class="new_pass bottom_20">
                    <span class="mm_size left right_30"><span class="required">必須</span>口座番号</span>
                    <error-display :message="errors.maebarai_account_number">
                        <input class="m_size left" ref="maebarai_account_number" type="text" v-model="data.settings.maebarai_account_number" :disabled="data.settings.maebarai_salary_per_hour == null">
                    </error-display>
                </div>
                <div class="new_pass bottom_40">
                    <span class="mm_size left right_30"><span class="required">必須</span>口座名義（カナ）</span>
                    <error-display :message="errors.maebarai_account_name">
                        <input class="m_size left" ref="maebarai_account_name" type="text" v-model="data.settings.maebarai_account_name" :disabled="data.settings.maebarai_salary_per_hour == null">
                    </error-display>
                </div>
            </section>
            <section class="btn">
                <p class="button right_30"><button class="m_size l_height btn_greeen l_font" v-on:click="submit">保存</button></p>
                <p class="button"><a class="m_size l_height btn_gray l_font" href="{{ Caeru::route('ss_maebarai_setting') }}">キャンセル</a></p>
            </section>
        </section>
    </main>
@endsection
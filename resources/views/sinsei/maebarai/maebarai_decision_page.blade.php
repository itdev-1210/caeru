@extends('sinsei.layouts.master')

@section('title', '前払')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 3, 'requester_mode' => true ])
@endsection

@section('content')
    <main>
        <section id="payment_account">
            <h1>処理内容確認</h1>
        </section>
        <section id="payment_select">
            <table>
                <tr>
                    <th class="s_14">前払金額</th>
                    <th class="s_14">振込金額</th>
                    <th class="s_14">振込手数料</th>
                    <th class="s_14">API利用料</th>
                </tr>
                <tr>
                    <td class="right_align">{{ $maebarai_pay_money }}円</td>
                    <td class="right_align">{{ $real_pay_money }}円</td>
                    <td class="right_align">-{{ $transaction_fee }}円</td>
                    <td class="right_align">-{{ $api_fee }}円</td>
                </tr>
            </table>
            <form id="maebarai_page" method="POST" form-single-submit action="{{ Caeru::route('ss_maebarai_complete_page') }}">
                {{ csrf_field() }}
                <input name="maebarai_pay_money" type="hidden" value="{{ $maebarai_pay_money }}">
                <section class="btn">
                    <p class="button"><button type="submit" class="m_size l_height btn_greeen">支払申請</a></p>
                    <p class="button"><a class="m_size l_height btn_gray" href="{{ Caeru::route('ss_maebarai_page') }}">戻る</a></p>
                </section>
            </form>
        </section>
    </main>
@endsection
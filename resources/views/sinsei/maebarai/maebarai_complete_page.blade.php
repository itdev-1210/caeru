@extends('sinsei.layouts.master')

@section('title', '前払')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 3, 'requester_mode' => true ])
@endsection

@section('content')
    <main>
        <section id="payment_account">
            <h1>{{ $title }}</h1>
            <p class="bottom_10 center_align">{{ $text }}</p>
        </section>
        <section id="payment_select">
            <section class="btn">
                <p class="button"><a class="m_size l_height btn_gray" href="{{ Caeru::route('ss_maebarai_page') }}">戻る</a></p>
            </section>
            <section class="btn">
                <p class="button"><a class="m_size l_height btn_greeen" href="{{ Caeru::route('ss_maebarai_history_page') }}">前払履歴</a></p>
            </section>
        </section>
    </main>
@endsection
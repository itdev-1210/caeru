@extends('sinsei.layouts.master')

@section('title', '前払履歴')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 3, 'requester_mode' => true ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/sinsei/requester/maebarai_history_page.js') }}"></script>
@endpush

@section('content')
    <main>
        <section id="payment_account">
            <h1>前払履歴</h1>
        </section>
        <section id="maebarai_history">
            <section id="attendance_detail">
                <section class="select_one2">
                    <section class="select_one_inner">
                        <section @click="preMonth" class="right_30 ico_position">
                            <a><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}"></a>
                        </section>
                        <div class="selectbox side_input_block right_10">
                            <select class="s_size" v-model="currentYear" @change="yearChanged" v-cloak>
                                <option v-for="year in yearRange" :value="year">@{{ year }}年</option>
                            </select>
                        </div>
                        <div class="selectbox side_input_block right_30">
                            <select class="s_size" v-model="currentMonth" @change="monthChanged" v-cloak>
                                <option v-for="month in 12" :value="month">@{{ month }}月度</option>
                            </select>
                        </div>
                        <section @click="nextMonth" class="ico_position">
                            <a><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
                        </section>
                    </section>
                </section>
            </section>
            <section id="payment_select" class="top_10 have_table_with_fixed_header">
                <table class="table_with_fixed_header">
                    <tr class="fixed_header">
                        <th class="s_14">実行日時</th>
                        <th class="s_10">支払形式</th>
                        <th class="s_10">支払者</th>
                        <th class="s_10">前払金額</th>
                        <th class="s_10">振込金額</th>
                        <th class="s_10">振込手数料</th>
                        <th>API利用料</th>
                        <th class="s_10">エラー</th>
                        <th class="s_14">エラー日時</th>
                    </tr>
                    <tr v-for="data in datas" v-cloak>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">
                            <span class="right_10">@{{ data['date'] }}</span>
                            <span>@{{ data['time'] }}</span>
                        </td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">@{{ data['payment_format'] }}</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">雇用先企業</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake right_align' : 'right_align']">@{{ data['maebarai_amount'] }}円</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake right_align' : 'right_align']">@{{ data['transfer_amount'] }}円</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake right_align' : 'right_align']">@{{ data['transfer_fee'] }}円</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake right_align' : 'right_align']">@{{ data['api_usage_fee'] }}円</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">@{{ data['error_text'] }}</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">
                            <span class="right_10">@{{ data['error_date'] }}</span>
                            <span>@{{ data['error_time'] }}</span>
                        </td>
                    </tr>
                </table>
            </section>
        </section>
        <section class="btn">
            <p class="button"><a class="m_size l_height btn_gray" href="{{ Caeru::route('ss_maebarai_page') }}">戻る</a></p>
        </section>
    </main>
@endsection
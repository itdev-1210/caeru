@extends('sinsei.layouts.master')

@section('title', 'アカウント設定')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 2, 'requester_mode' => true ])
@endsection

@section('content')
    <main id="personal_account">
        <form method="POST" form-single-submit action="{{ Caeru::route('ss_account_password_change') }}">
            {{ csrf_field() }}
            <section>
                <h1>パスワード変更</h1>
                <section class="account_inner">
                    <div class="new_pass bottom_30">
                        <span class="mm_size left right_30">現在のパスワード</span>
                        @component('layouts.form.error', ['field' => 'current_password'])
                            <input class="mm_size left" name="current_password" value="{{ old('current_password') }}" type="password">
                        @endcomponent
                    </div>
                    <div class="new_pass bottom_20">
                        <span class="mm_size left right_30">新しいパスワード</span>
                        @component('layouts.form.error', ['field' => 'new_password'])
                            <input class="mm_size left" name="new_password" value="{{ old('new_password') }}" type="password">
                        @endcomponent
                    </div>
                    <div class="new_pass bottom_40">
                        <span class="mm_size left right_30">新しいパスワード（再入力）</span>
                        @component('layouts.form.error', ['field' => 'new_password_confirmation'])
                            <input class="mm_size left" name="new_password_confirmation" value="{{ old('new_password_confirmation') }}" type="password">
                        @endcomponent
                    </div>
                </section>
                <section class="btn">
                    <p class="button right_30"><button class="m_size l_height btn_greeen">保存</button></p>
                    <p class="button right_30"><a class="m_size l_height btn_gray" href="{{ Caeru::route('ss_show_account_setting') }}">キャンセル</a></p>
                </section>
            </section>
        </form>
       
        <form method="POST" form-single-submit action="{{ Caeru::route('ss_account_email_change') }}">
            {{ csrf_field() }}
            <section class="top_60">
                <h1>メールアドレス変更</h1>
                <section class="account_inner">
                    <div class="new_pass bottom_30">
                        <span class="mm_size left right_30">現在のメールアドレス</span>
                        @component('layouts.form.error', ['field' => 'current_email'])
                            <input class="mm_size left" name="current_email" value="{{ old('current_email') }}" type="text">
                        @endcomponent
                    </div>
                    <div class="new_pass bottom_20">
                        <span class="mm_size left right_30">新しいメールアドレス</span>
                        @component('layouts.form.error', ['field' => 'new_email'])
                            <input class="mm_size left" name="new_email" value="{{ old('new_email') }}" type="text">
                        @endcomponent
                    </div>
                    <div class="new_pass bottom_40">
                        <span class="mm_size left right_30">新しいメールアドレス（再入力）</span>
                        @component('layouts.form.error', ['field' => 'new_email_confirmation'])
                            <input class="mm_size left" name="new_email_confirmation" value="{{ old('new_email_confirmation') }}" type="text">
                        @endcomponent
                    </div>
                </section>
                <section class="btn">
                    <p class="button right_30"><button class="m_size l_height btn_greeen">保存</button></p>
                    <p class="button right_30"><a class="m_size l_height btn_gray" href="{{ Caeru::route('ss_show_account_setting') }}">キャンセル</a></p>
                </section>
            </section>
        </form>
	</main>
@endsection
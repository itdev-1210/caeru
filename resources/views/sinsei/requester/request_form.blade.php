@extends('sinsei.layouts.master')

@section('title', '承認')

@section('header')
    @include('sinsei.layouts.header', [ 'active' => 1, 'requester_mode' => true ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/pages/sinsei/requester/request_form.js') }}"></script>
@endpush

@section('content')
    <main id="request_page">
        <snapshot-day-component v-for="(snapshot_day, index) in snapshot_days" :key="index"
            :pkey="index"
            :snapshot-day="snapshot_day"
            ref="snapshot_days"
            :is-snapshot-days="is_snapshot_days"
            :work-locations="work_locations"
            :company-separate-info="company_separate_info"
            :rest-days="rest_days"
            :national-holidays="national_holidays"
            :flip-color-day="flip_color_day"
            :timestamp-types="timestamp_types"
            :list-date-sinsei-chu="list_date_sinsei_chu"
            :concluded="snapshot_day.isConcluded"
            :mode-approval="false"
            @transfer-snapshot-day="transferSnapshotDay"
            @remove-transfer-snapshot-day="removeTransferSnapshotDay"
            @save-furikae-case="saveFurikaeCase"
            @update-list-sinsei="updateListSinsei"
            @update-snapshot-day="updateSnapshotDay">
        </snapshot-day-component>
    </main>
@endsection

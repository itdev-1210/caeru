@extends('layouts.master')

@section('title', '前払設定 | CAERU管理画面')

@section('header')
    @include('layouts.header', [ 'active' => 4 ])
@endsection

@push('scripts')
	<script defer src="{{ asset('/js/fixed_midashi.js') }}"></script>
	<script defer src="{{ asset('/js/pages/maebarai_setting.js') }}"></script>
@endpush

@section('content')
    <main id="payment">
		<section class="title">
			@include('layouts.attendance_breadcrumbs')
			<div class="title_wrapper">
				<h1>前払設定</h1>
			</div>
		</section>
		<section id="maebarai_setting">
			<section class="setting_table">
				<table>
					<tr>
						<td class="input_items">前払</td>
						<td>
							@can('change_maebarai_setting_page')
							<label class="radio_text right_30"><input type="radio" value="0" v-model="data.settings.maebarai_enable">利用しない</label>
							<label class="radio_text"><input type="radio" value="1" v-model="data.settings.maebarai_enable">利用する</label>
							@else
							@{{ data.settings.maebarai_enable == 1 ? '利用する' : '利用しない'}}
							@endcan
						</td>
					</tr>
					<tr>
						<td class="input_items"><span class="required">必須</span>前払上限</td>
						<td><span class="right_10">確定給与の</span>
							@can('change_maebarai_setting_page')
							<error-display :message="errors.maebarai_payment_rate">
								<input class="s_m_size right_10" ref="maebarai_payment_rate" type="text" v-model="data.settings.maebarai_payment_rate" :disabled="data.settings.maebarai_enable == 0">
							</error-display>
							@else
							@{{ data.settings.maebarai_payment_rate }}
							@endcan
							<span>%</span>
						</td>
					</tr>
					<tr>
						<td class="input_items"><span class="required">必須</span>給与の確定</td>
						<td>
							@can('change_maebarai_setting_page')
							<section class="check_box_wrap side_input_block">
								<label class="checkbox_text">
									<input type="checkbox" value="1" v-model="data.settings.maebarai_auto_approve" :checked="data.settings.maebarai_auto_approve == 1">勤怠データを自動承認する
								</label>
							</section>
							@else
							@{{ data.settings.maebarai_auto_approve == 1 ? '勤怠データを自動承認する' : '近態データを自動承認しない'}}
							@endcan
						</td>
					</tr>
					<tr>
						<td class="input_items"><span class="required">必須</span>前払方法</td>
						<td>
							@can('change_maebarai_setting_page')
							<label class="radio_text right_30">
								<input type="radio" value="0" v-model="data.settings.maebarai_payment_method">自社口座から現金で支払う
							</label>
							@else
							@{{ data.settings.maebarai_payment_method == 0 ? '自社口座から現金で支払う' : ''}}
							@endcan
						</td>
					</tr>
					<tr>
						<td class="input_items"><span class="required">必須</span>支払口座</td>
						<td>
							<span class="right_30">セブン銀行</span>
							
							<span class="right_10">支店番号</span>
							@can('change_maebarai_setting_page')
							<error-display :message="errors.maebarai_branch_code">
								<input class="s_size right_30" ref="maebarai_branch_code" type="text" v-model="data.settings.maebarai_branch_code" :disabled="data.settings.maebarai_enable == 0">
							</error-display>
							@else
							<span class="right_30">@{{ data.settings.maebarai_branch_code }}</span>
							@endcan
							
							<span class="right_10">口座番号</span>
							@can('change_maebarai_setting_page')
							<error-display :message="errors.maebarai_account_number">
								<input class="m_size right_30" ref="maebarai_account_number" type="text" v-model="data.settings.maebarai_account_number" :disabled="data.settings.maebarai_enable == 0">
							</error-display>
							@else
							<span class="right_30">@{{ data.settings.maebarai_account_number }}</span>
							@endcan

							<span class="right_10">口座名義(カナ)</span>
							@can('change_maebarai_setting_page')
							<error-display :message="errors.maebarai_account_name">
								<input class="mm_size right_30" ref="maebarai_account_name" type="text" v-model="data.settings.maebarai_account_name" :disabled="data.settings.maebarai_enable == 0">
							</error-display>
							@else
							<span class="right_30">@{{ data.settings.maebarai_account_name }}</span>
							@endcan
						</td>
					</tr>
				</table>
			</section>
			@can('change_maebarai_setting_page')
				<section class="btn">
					<p class="button right_30"><button type='button' v-on:click="submit" class="m_size l_height btn_greeen l_font">保存</button></p>
					<p class="button"><a class="m_size l_height btn_gray l_font" href="{{ Caeru::route('maebarai_setting') }}">キャンセル</a></p>
				</section>
			@endcan
		</section>
	</main>
@endsection
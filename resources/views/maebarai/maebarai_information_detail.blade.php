@extends('layouts.master')

@section('title', '前払情報詳細 | CAERU管理画面')

@section('header')
    @include('layouts.header', [ 'active' => 4 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/fixed_midashi.js') }}"></script>
    <script defer src="{{ asset('/js/multiple-select.js') }}"></script>
	<script defer src="{{ asset('/js/pages/maebarai_information_detail_page.js') }}"></script>
@endpush

@section('content')
    <main id="payment">
        <section id="payment_information_detail">
            <section class="title">
                @include('layouts.attendance_breadcrumbs')
                <div class="title_wrapper">
                    <h1>前払情報詳細</h1>
                    <section class="select_one">
                        <section class="select_one_inner">
                            <section @click="preMonth" class="right_30 ico_position">
                                <a><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}"></a>
                            </section>
                            <div class="selectbox side_input_block right_10">
                                <select class="s_size" v-model="currentYear" @change="yearChanged" v-cloak>
                                    <option v-for="year in yearRange" :value="year">@{{ year }}年</option>
                                </select>
                            </div>
                            <div class="selectbox side_input_block right_30">
                                <select class="s_size" v-model="currentMonth" @change="monthChanged" v-cloak>
                                    <option v-for="month in 12" :value="month">@{{ month }}月度</option>
                                </select>
                            </div>
                            <section @click="nextMonth" class="ico_position">
                                <a><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
                            </section>
                        </section>
                    </section>
                </div>
            </section>
            <section id="find_employee">
                <section class="select_one bg_light_green bottom_10">
                    <section class="select_one_inner">
                        <section v-if="can_go_previous==true" class="right_30 ico_position">
                            <a @click="prevEmployee()"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}"></a>
                        </section>
                        <section class="right_30 text_bold">
                            <span class="right_30">{{ $presentation_id }}</span><span>{{ $name }}</span>
                        </section>
                        <section v-if="can_go_next==true" class="ico_position">
                            <a @click="nextEmployee()"><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
                        </section>
                    </section>
                </section>
            </section>
			<section class="approval_table have_table_with_fixed_header">
				<table class="table_with_fixed_header">
					<tr class="fixed_header">
                        <th class="s_14">支払日時</th>
                        <th class="s_10">支払形式</th>
                        <th class="s_10">支払者</th>
                        <th class="s_10">前払金額</th>
                        <th class="s_10">振込金額</th>
                        <th class="s_10">振込手数料</th>
                        <th>API利用料</th>
                        <th class="s_10">エラー</th>
                        <th class="s_14">エラー日時</th>
					</tr>
					<tr v-for="data in datas" v-cloak>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">
                            <span class="right_10">@{{ data['date'] }}</span>
                            <span>@{{ data['time'] }}</span>
                        </td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">@{{ data['payment_format'] }}</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">雇用先企業</td>
						<td v-bind:class="[data.status == 2 ? 'bg_mistake right_align' : 'right_align']">@{{ data['maebarai_amount'] }}円</td>
						<td v-bind:class="[data.status == 2 ? 'bg_mistake right_align' : 'right_align']">@{{ data['transfer_amount'] }}円</td>
						<td v-bind:class="[data.status == 2 ? 'bg_mistake right_align' : 'right_align']">@{{ data['transfer_fee'] }}円</td>
						<td v-bind:class="[data.status == 2 ? 'bg_mistake right_align' : 'right_align']">@{{ data['api_usage_fee'] }}円</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">@{{ data['error_text'] }}</td>
                        <td v-bind:class="[data.status == 2 ? 'bg_mistake' : '']">
                            <span class="right_10">@{{ data['error_date'] }}</span>
                            <span>@{{ data['error_time'] }}</span>
                        </td>
					</tr>
                </table>
            </section>
            <section class="btn">
                <p class="button"><a class="m_size l_height btn_gray l_font" href="{{ Caeru::route('breadcrumb_go_back') }}">一覧に戻る</a></p>
            </section>
		</section>
	</main>
@endsection
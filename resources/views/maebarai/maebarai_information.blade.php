@extends('layouts.master')

@section('title', '前払情報 | CAERU管理画面')

@section('header')
    @include('layouts.header', [ 'active' => 4 ])
@endsection

@push('scripts')
	<script defer src="{{ asset('/js/fixed_midashi.js') }}"></script>
	<script defer src="{{ asset('/js/components/work_location_picker.js') }}"></script>
	<script defer src="{{ asset('/js/pages/maebarai_information_page.js') }}"></script>
@endpush

@section('content')
    <main id="payment">
		<section class="title">
			@include('layouts.attendance_breadcrumbs')
			<div class="title_wrapper">
                <h1>前払情報</h1>
                <div class="worklocation">
					<div class="worklocation_inner">
						<span class="right_10">{{ $current_work_location }}</span>
						@if (isset($picker_list))
                            <p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
                        @endif
					</div>
					@include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'maebarai_information', 'singular' => true])
				</div>
			</div>
		</section>
		<section id="payment_information">
			<section class="select_one bg_light_green bottom_10">
				<section class="select_one_inner">
					<section @click="preMonth" class="right_30 ico_position">
						<a><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}"></a>
					</section>
					<div class="selectbox side_input_block right_10">
						<select class="s_size" v-model="currentYear" @change="yearChanged" v-cloak>
							<option v-for="year in yearRange" :value="year">@{{ year }}年</option>
						</select>
					</div>
					<div class="selectbox side_input_block right_30">
						<select class="s_size" v-model="currentMonth" @change="monthChanged" v-cloak>
							<option v-for="month in 12" :value="month">@{{ month }}月度</option>
						</select>
					</div>
					<section @click="nextMonth" class="ico_position">
						<a><img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}"></a>
					</section>
				</section>
			</section>
			<section class="search_box_wrapper">
				<div class="search_box_innner2">
					<div>
						<div class="search_setting">
							<span class="right_10">従業員ID</span><input class="s_size" v-model="conditions['presentation_id']" type="text">
						</div>
						<div class="search_setting">
							<span class="right_10">従業員名</span>
							<autocomplete :suggestions = "employeeNames" custom-class="m_size" :initial-value="conditions['name']" :allow-approx ="true" @selected="employeeNameSelected" @changed="employeeNameChanged">
							</autocomplete>
						</div>
						<div class="button bottom_10 search_setting right_10">
							<a class="s_size s_height btn_greeen" @click="submit(1)">検索</a>
						</div>
						<div class="button bottom_10 search_setting">
							<a class="s_size s_height btn_gray" @click="resetConditions">リセット</a>
						</div>
					</div>
				</div>
			</section>
			<paginator :total="total" :current-page="currentPage" :per-page="perPage" @changed="changePage"></paginator>
			<section class="approval_table have_table_with_fixed_header">
				<table class="table_with_fixed_header">
					<tr class="fixed_header">
						<th class="s_20">従業員ID</th>
						<th class="s_20">名前</th>
                        <th class="s_12">前払金額</th>
                        <th class="s_12">振込金額</th>
						<th class="s_12">振込手数料</th>
						<th class="s_12">API利用料</th>
						<th></th>
					</tr>
					<tr v-for="data in datas" v-cloak>
						<td>@{{ data['presentation_id'] }}</td>
						<td>@{{ data['name'] }}</td>
						<td class="right_align">@{{ data['prepay_money'] }}円</td>
						<td class="right_align">@{{ data['realpay_money'] }}円</td>
						<td class="right_align">@{{ data['send_fee'] }}円</td>
						<td class="right_align">@{{ data['api_fee'] }}円</td>
						<td>
							<p class="button"><a class="ss_size s_height btn_gray" @click="detail(data['id'])">詳細</a></p>
						</td>
					</tr>
				</table>
			</section>
			<paginator :total="total" :current-page="currentPage" :per-page="perPage" :sum-line="false" @changed="changePage"></paginator>
		</section>
	</main>
@endsection
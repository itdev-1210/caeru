<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>エラー | CAERU管理画面</title>
	<link href= "{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
	<section id="error_wrapper">
		<section class="caeru">
			<img src="{{ asset('/images/caeru.svg') }}">
		</section>
        <section class="message">
<!--        
            <h1>お探しのページは見つかりません</h1>
            <p>お手数ですが、URLをご確認のうえ、再度アクセスをお願いいたします。</p> 
-->

            <h1>ページが表示できません</h1>
            <p>サーバエラーが発生しました。</p>
            <p>ご不便をおかけし申し訳ございません。しばらくたってからアクセスをお願いいたします。</p>
            <p>しばらくたって再度アクセスしても改善されない場合はお手数ですが、弊社サポート（082‑258‑2988）までご連絡をお願いいたします。</p>
<!-- 403error
            <h1>該当のページにアクセスできませんでした</h1>
            <p>ホームまたは別のページにアクセスをお願いいたします。</p>
 -->
        </section>
	</section>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>エラー | CAERU管理画面</title>
    <link href= "{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('/js/bootstrap.js') }}" type="text/javascript"></script>
</head>
<body>
    <section id="error_wrapper">
        <section class="caeru">
            <img src="{{ asset('/images/caeru.svg') }}">
        </section>
    <section class="message">
        <!--        
            <h1>お探しのページは見つかりません</h1>
            <p>お手数ですが、URLをご確認のうえ、再度アクセスをお願いいたします。</p> 
        -->

            <h1>ページの期限が切れています</h1>
            <!-- <p>Ctrl + F5 でブラウザのリフレッシュをお願いいたします。</p> -->
            <p class="button"><button class="top_10 mm_size btn_greeen">リフレッシュ</button></p>

        <!-- 403error
            <h1>該当のページにアクセスできませんでした</h1>
            <p>ホームまたは別のページにアクセスをお願いいたします。</p>
        -->
        </section>
    </section>
</body>
<script>
    $(document).ready(function() {
        $('button').click(function() {
            window.location = window.location;
        });
    })
</script>
</html>
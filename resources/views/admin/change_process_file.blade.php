@extends('layouts.master')

@section('title', 'ADMINの処理ファイルの変更')

@push('scripts')
    <script defer src="{{ asset('/js/mics.js') }}" defer></script>
    <script defer src="{{ asset('/js/components/alert_module.js') }}" defer></script>
    <script defer src="{{ asset('/js/pages/admin_change_process_file.js') }}" defer></script>
@endpush

@section('content')
    <section id="header_wrapper">
		<section class="alert_wrapper_fixer">
			<section class="alert_wrapper">
				<section class="alert_innner">
					<section class="alert_box save_green"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_save.svg') }}"></span>
						@if (session()->has('success'))
							<span class="alert_content">{{ session('success') }}</span>
						@endif
					</section>
					<section class="alert_box save_yellow"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_dakoku.svg') }}"></span>
						@if (session()->has('warning'))
							<span class="alert_content">{{ session('warning') }}</span>
						@endif
					</section>
					<section class="alert_box save_red"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_error.svg') }}"></span>
						@if (count($errors) || session()->has('error'))
							<span class="alert_content">{{ session()->has('error') ? session('error') : '入力に誤りがあります' }}</span>
						@endif
					</section>
				</section>
			</section>
		</section>
        <header>
            <a><div id ="logo"></div></a>
            <div id ="sign_out"><a href ="{{ Caeru::route('admin_logout') }}">ログアウト</a></div>
        </header>
    </section>
    <section id ="login_wrapper">
        <form method="POST" form-single-submit action="{{ Caeru::route('admin_change_attendance_file') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <section class="login_logo bottom_20">
                <h1 class="center">勤怠データ処理ファイル：</h1>
            </section>
            <div class="change_process_file_wrapper">
                <section class="input_panel_row">
                    <span class="s_size left">会社コード：</span>
                    @component('layouts.form.error', ['field' => 'attendance_company_code'])
                        <input id="attendance_company_code" type="text" name="attendance_company_code" class="m_size left" value="{{ old('attendance_company_code') ?? session('last_attendance_company_code') }}">
                    @endcomponent
                    <p class="button">
                        <button id="downloadCurrentAttendance" class ="mmm_size l_height btn_greeen l_font" >現在勤怠データ処理ファイルを取得</button>
                    </p>
                </section>
                <section class="input_panel_row">
                    <span class="s_size left">ファイル：</span>
                    @component('layouts.form.error', ['field' => 'attendance_file'])
                        <label for="attendance_file" class="file-btn s_height ss_size btn_white">
                            選択
                            <input type="file" name="attendance_file" id="attendance_file">
                        </label>
                    @endcomponent
                    <span id="attendance_file_name"></span>
                </section>
                <section class ="btn">
                    <p class="button">
                        <button class ="s_size l_height btn_greeen l_font" >送信</button>
                    </p>
                </section>
            </div>
        </form>
    </section>
    <section id ="login_wrapper">
        <form method="POST" form-single-submit action="{{ Caeru::route('admin_change_summary_file') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <section class="login_logo bottom_20">
                <h1 class="center">集計データ処理ファイル：</h1>
            </section>
            <div class="change_process_file_wrapper">
                <section class="input_panel_row">
                    <span class="s_size left">会社コード：</span>
                    @component('layouts.form.error', ['field' => 'summary_company_code'])
                        <input id="summary_company_code" type="text" name="summary_company_code" class="m_size left" value="{{ old('summary_company_code') ?? session('last_summary_company_code') }}">
                    @endcomponent
                    <p class="button">
                        <button id="downloadCurrentSummary" class ="mmm_size l_height btn_greeen l_font" >現在集計データ処理ファイルを取得</button>
                    </p>
                </section>
                <section class="input_panel_row">
                    <span class="s_size left">ファイル：</span>
                    @component('layouts.form.error', ['field' => 'summary_file'])
                        <label for="summary_file" class="file-btn s_height ss_size btn_white">
                            選択
                            <input type="file" name="summary_file" id="summary_file">
                        </label>
                    @endcomponent
                    <span id="summary_file_name"></span>
                </section>
                <section class ="btn">
                    <p class="button">
                        <button class ="s_size l_height btn_greeen l_font" >送信</button>
                    </p>
                </section>
            </div>
        </form>
    </section>
@endsection
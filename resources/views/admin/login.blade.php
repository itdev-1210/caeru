@extends('layouts.master')

@section('title', 'ADMINのログイン')

@push('scripts')
    <script defer src="{{ asset('/js/mics.js') }}" defer></script>
    <script defer src="{{ asset('/js/components/alert_module.js') }}" defer></script>
@endpush

@section('content')
	<section class="alert_wrapper_fixer">
        <section class="alert_wrapper">
            <section class="alert_innner">
                <section class="alert_box save_green"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_save.svg') }}"></span>
                    @if (session()->has('success'))
                        <span class="alert_content">{{ session('success') }}</span>
                    @endif
                </section>
                <section class="alert_box save_yellow"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_dakoku.svg') }}"></span>
                    @if (session()->has('warning'))
                        <span class="alert_content">{{ session('warning') }}</span>
                    @endif
                </section>
                <section class="alert_box save_red"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_error.svg') }}"></span>
                    @if (count($errors) || session()->has('error'))
                        <span class="alert_content">{{ session()->has('error') ? session('error') : '入力に誤りがあります' }}</span>
                    @endif
                </section>
            </section>
        </section>
	</section>
    <section class="login_title">管理者ログイン</section>
        <section id ="login_wrapper">
            <form method="POST" form-single-submit action="{{ Caeru::route('admin_login') }}">
                {{ csrf_field() }}
                @if ($errors->has('auth_failed'))
                    <p class="auth_failed_message">{{ $errors->first('auth_failed') }}</p>
                @endif
                <div class="right">
                    <section class="sample">
                        <section class="input_panel_row">
                            <span class="s_size left">ADMIN ID</span>
                            <div class="input_wrapper {{ $errors->has('username') ? 'error':'' }}">
                                <input type="text" name="username" class="m_size right" value="{{ old('username') }}">
                                @if ($errors->has('username'))
                                    <div class='error_wrapper'>
                                        <span class="login_tool_error">{{ $errors->first('username') }}</span>
                                    </div>
                                @endif
                            </div>
                        </section>
                        <section class="input_panel_row">
                            <span class="s_size left">パスワード</span>
                            <div class="input_wrapper {{ $errors->has('password') ? 'error':'' }}">
                                <input type="password" name="password" class="m_size right">
                                @if ($errors->has('password'))
                                    <div class='error_wrapper'>
                                        <span class="login_tool_error">{{ $errors->first('password') }}</span>
                                    </div>
                                @endif
                            </div>
                        </section>
                        <section class ="btn">
                            <p class="button">
                                <button type="submit" class ="mm_size l_height btn_greeen l_font" >ログイン</button>
                            </p>
                        </section>
                    </section>
                </div>
                <section class="login_logo right">
                    <img src="{{ asset('images/logo_vertical.svg') }}">
                </section>
            </form>
        </section>
    <section class="browser">当サイトはGoogle Chrome・Microsoft Edgeを推奨しております</section>
@endsection
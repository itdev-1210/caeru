<section class="pager">
    {{ $employees->links(null, ['sum_line' => true, 'force_url' => 'totalization_list']) }}
</section>
<section class="approval_table">
    <table _fixedhead='rows:1; cols:2;'>
        <tr>
            <th class="s_10">従業員ID</th>
            <th>名前</th>
            <th class="s_10">申請数</th>                   
            <th class="s_10">申請中</th>
            <th class="s_10">承認済</th>
            <th class="s_10">打刻エラー</th>
            <th class="s_10">判定エラー</th>
            <th class="s_10">管理者1</th>
            <th class="s_10">管理者2</th>
        </tr>
        <tr v-for="employee in totalizations.employees.data" v-cloak>
            <td>@{{ employee.presentation_id }}</td>
            <td><a href="">@{{ employee.first_name + employee.last_name }}</a></td>
            <td>@{{ employee.totalRequestStatus }}</td>
            <td>@{{ employee.consideringRequeststatus }}</td>
            <td>@{{ employee.approvedRequestStatus }}</td>
            <td>@{{ employee.totalTimestampError }}</td>
            <td>@{{ employee.totalConfirmNeeded }}</td>
            <td>
                <span v-if="employee.hasConcludedOne"> 
                <img class="check_blue ico_ico_arrow3" src="{{ asset('images/ico_check_blue.svg') }}">
                </span>
            </td>
            <td>
                <p class="button">
                    <span v-if="employee.hasConcludedTwo">
                        <a class="ss_size s_height btn_red" href="#">
                            <img class="ico_ico_arrow2" src="{{ asset('images/ico_check_red.svg') }}">
                        </a>
                    </span>
                    <span v-else>
                    <a class="ss_size s_height btn_white" href="#">締める</a>
                    </span>
                </p>
                
            </td>
        </tr>
    </table>
</section>
<section class="pager">
    {{ $employees->links(null, ['sum_line' => false, 'force_url' => 'totalization_list']) }}
</section>
<script defer type="text/javascript">
    window.totalizations = {!! $totalizationsJson !!};
    {{--window.totalizationsHistory = {!! $totalizationsHistory !!}
    window.totalizationsDisplayHistory = {!! $displayHistory !!} --}}
</script>
<section class="check_table">
    <section class="s_49 left">
        <p class="ll_font titile_wrap">打刻エラー</p>
        <table>
            <tr>
                <th class="s_18"></th>
                <th>日付</th>
                <th class="s_18">従業員ID</th>
                <th class="s_22">名前</th>
                <th class="s_14"></th>
            </tr>
            <tr v-for="checklist in checklists.checklists_timestamp_error" v-cloak>
                <td>
                    <span>@{{ checklist.error_name }}</span>
                </td>
                <td>
                    <span :class="getClassDate(checklist.date)">@{{ checklist.date | formatDate }}</span>
                    {{-- <span>@{{ checklist.date }}</span> --}}
                </td>
                <td>@{{ checklist.employee_presentation_id }}</td>
                <td>@{{ checklist.employee_name }}</td>
                <td>
                    @can('view_attendance_employee_working_day')
                        <p class="button"><a class="ss_size s_height btn_gray" :href="fullUrl(checklist.employee_id, checklist.date)">変更</a></p>
                    @endcan
                </td>
            </tr>
        </table>
    </section>
    <section class="s_49 right">
        <p class="ll_font titile_wrap">要チェックリスト</p>
        <table>
            <tr>
                <th class="s_22"></th>
                <th>日付</th>
                <th class="s_18">従業員ID</th>
                <th class="s_22">名前</th>
                <th class="s_14"></th>
            </tr>
            <tr v-for="checklist in checklists.checklists_confirm_needed" v-cloak>
                <td>
                    <span>@{{ checklist.error_name }}</span>
                </td>
                <td>
                    <span :class="getClassDate(checklist.date)">@{{ checklist.date | formatDate }}</span>
                </td>
                <td>@{{ checklist.employee_presentation_id }}</td>
                <td>@{{ checklist.employee_name }}</td>
                <td>
                    @can('view_attendance_employee_working_day')
                        <p class="button"><a class="ss_size s_height btn_gray" :href="fullUrl(checklist.employee_id, checklist.date)">変更</a></p>
                    @endcan
                </td>
            </tr>
        </table>
    </section>
</section>

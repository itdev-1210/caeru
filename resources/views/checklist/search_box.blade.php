<section class="checklist-search">   
    <section class="select_one bg_light_green bottom_10">
        <section class="select_one_inner">
            <form action="">
                <section @click="preMonth" class="right_30 ico_position">
                    <img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_left1.svg') }}">
                </section>
                <div class="year-change selectbox side_input_block right_10">
                    <select v-model="yearSelected" class="s_size" @change="yearChanged">
                        @for($i=5; $i>=1; $i--)
                        <option value="{{ $currentYear-$i }}">{{ $currentYear-$i }}年</option>
                        @endfor
                        <option value="{{ $currentYear }}">{{ $currentYear }}年</option>
                        @for($i=1; $i<=5; $i++)
                        <option value="{{ $currentYear+$i }}">{{ $currentYear+$i }}年</option>
                        @endfor
                    </select>
                </div>
               
                <div class="month-change selectbox side_input_block right_10">
                    <select v-model="monthSelected" class="s_size" @change="monthChanged">
                        @for($i= 1; $i<=12; $i++)
                        <option value="{{ $i }}"{{ $i==$currentMonth?" selected='selected'":""}}>{{ $i }}月度</option>
                        @endfor
                    </select>
                </div>
                <p class="term right_30" v-cloak>
                    <span>@{{ beginDateString }}</span>
                   {{--  <span>@{{ showBeginDayOfWeek }}</span> --}}
                    <span class="right_10 left_10">〜</span>
                    <span>@{{ endDateString }}</span>
                   {{--  <span>@{{ showEndDayOfWeek }}</span> --}}

                </p>
                <section @click ="nextMonth" class="ico_position">
                    <img class="ico_ico_arrow" src="{{ asset('images/ico_arrow_right1.svg') }}">
                </section>
            </form>
        </section>
    </section>
    <section class="search_box_wrapper">
            <div class="search_box_innner1 vue">
                <form action="" @submit.prevent="">
                    <div class="search_box_innner2">
                        <div>
                            <div class="search_setting">
                                <span class="right_10 left">現在の勤務地</span>
                                <div class="selectbox left">
                                    <select class="mm_size" v-model.number="searchTarget" id="search_target">
                                        <option value="1">に所属している従業員</option>
                                        <option value="2" selected>の勤務がある従業員</option>
                                    </select>
                                </div>
                            </div>
                            <div class="search_setting">
                                <span class="right_10">従業員ID</span><input class="s_size" v-model ="employeeId" name="employeeId" type="text" value="">
                            </div>
                            <div class="search_setting">
                                <span class="right_10">従業員名</span>
                                <autocomplete :suggestions = "employeeNameList" custom-class="m_size" :initial-value="employeeName" :allow-approx ="true" @selected="employeeNameSelected" @changed="employeeNameChanged">
                                </autocomplete>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" :value="consts['START_WORK_ERROR']"  v-model="checkedEr">出勤エラー</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" value="true" v-model="checkedEndWorkError">退勤エラー</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" value="true" v-model="checkedReturnError">外出・戻りエラー</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" :value="consts['LATE_OR_LEAVE_EARLY_TYPE']" v-model="checkedEr">遅刻・早退</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" :value="consts['OFF_SCHEDULE_TIME']" v-model="checkedEr">時間外</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" :value="consts['STATUS_MISTAKEN']" v-model="checkedEr">形態</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" :value="consts['OVERLIMIT_BREAK_TIME']" v-model="checkedEr">休憩・外出</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" :value="consts['WORK_WITHOUT_SCHEDULE']" v-model="checkedEr">休出</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" :value="consts['HAVE_SCHEDULE_BUT_OFFLINE']" v-model="checkedEr">欠勤</label>
                            </div>
                            <div class="check_box_wrap search_setting">
                                <label class="checkbox_text"><input class="left" type="checkbox" :value="consts['DIFFERENCE_FROM_SCHEDULE']" v-model="checkedEr">予定外</label>
                            </div>
                            <div class="search_box search_setting">
                                <span class="right_10">部署</span>
                                <div class="form-group mm_size">
                                    <select class="mm_size ms" v-model="departments" autocomplete="off" multiple="multiple"></select>
                                </div>
                            </div>
                            <div class="button bottom_10 search_setting right_10">
                                <button class="s_size s_height btn_greeen" @click="submit">検索</button>
                            </div>
                            <div class="button bottom_10 search_setting">
                                <a class="s_size s_height btn_gray" @click="resetConditions">リセット</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
    </section>
</section>
@extends('layouts.master')

@section('title', '従業員詳細')

@section('header')
    @include('layouts.header', [ 'active' => 2 ])
@endsection

@push('scripts')
    <script defer src="{{ asset('/js/components/work_location_picker.js') }}"></script>
    <script defer src="{{ asset('/js/multiple-select.js') }}"></script>
    <script defer src="{{ asset('/js/pages/employee_edit_maebarai_page.js') }}"></script>
    <script defer src="{{ asset('/js/components/employee_searcher.js') }}"></script>
@endpush

@section('content')
    <main id="basic">
        <section class="title">
            <p class="breadcrumb"><span>基本情報</span>&emsp;&#62;&emsp;<a href="{{ Caeru::route('employees_list', ['page' => $page]) }}">従業員一覧</a><span>&emsp;&#62;&emsp;従業員詳細</span></p>
            <div class="title_wrapper">
                <h1>従業員詳細</h1>
                <div class="worklocation">
                    <div class="worklocation_inner">
                        <span class="right_10">{{ $current_work_location }}</span>
                        @if (isset($picker_list))
                            <p class="button"><a class="modal-open ss_size s_height btn_gray" @click="open">変更</a></p>
                        @endif
                    </div>
                    @include('layouts.work_location_picker', ['list' => $picker_list, 'target' => 'employees_list', 'singular' => false])
                </div>
            </div>
        </section>
        @include('employee.search_box')
        @include('layouts.search_result_navigation')
        <section class="select_one2">
            <section class="tab">
                @can('view_employee_basic_info')
                    <p class="tab_button"><a class="tab_size tab_btn_gray left right_10" href="{{ Caeru::route('edit_employee', [$employee->id, $page]) }}">基本</a></p>
                @endcan
                @can('view_employee_work_info')
                    <p class="tab_button"><a class="tab_size tab_btn_gray left right_10" href="{{ Caeru::route('edit_employee_work', [$employee->id, $page]) }}">勤怠</a></p>
                @endcan
                <p class="tab_button"><a class="tab_size btn_sky_blue left right_10" href="">前払</a></p>
            </section>
        </section>
        <section id="container">
            <section class="setting_table bottom_30">
                <table>
                    <tr>
                        <td class="input_items">前払</td>
                        <td>
                        @can('change_employee_maebarai_info')
                            <label class="radio_text right_30"><input type="radio" value="1" v-model="data.settings.maebarai_enable">利用する</label>
                            <label class="radio_text"><input type="radio" value="0" v-model="data.settings.maebarai_enable">利用しない</label>
                        @else
                            {{ $maebarai_settings[$employee->maebarai_enable] }}
                        @endcan
                        </td>
                    </tr>
                    <tr>
                        <td class="input_items">前払計算時給</td>
                        <td>
                            @can('change_employee_maebarai_info')
                                <error-display :message="errors.maebarai_salary_per_hour">
                                    <input class="s_size right_10" ref="maebarai_salary_per_hour" type="text" v-model="data.settings.maebarai_salary_per_hour">
                                </error-display>
                            @else
                                {{ $employee->maebarai_salary_per_hour }}
                            @endcan
                            <span>円</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="input_items" rowspan="6">口座情報</td>
                        <td>
                            <span class="right_10"><span class="red">※</span>金融機関コード</span>
                            @can('change_employee_maebarai_info')
                                <error-display :message="errors.maebarai_bank_code">
                                    <input class="s_size" ref="maebarai_bank_code" type="text" v-model="data.settings.maebarai_bank_code">
                                </error-display>
                            @else
                                {{ $employee->maebarai_bank_code }}
                            @endcan
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="right_10"><span class="red">※</span>金融機関名</span>
                            @can('change_employee_maebarai_info')
                                <error-display :message="errors.maebarai_bank_name">
                                    <input class="l_size" ref="maebarai_bank_name" type="text" v-model="data.settings.maebarai_bank_name">
                                </error-display>
                            @else
                                {{ $employee->maebarai_bank_name }}
                            @endcan
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="right_10"><span class="red">※</span>支店コード</span>
                            @can('change_employee_maebarai_info')
                                <error-display :message="errors.maebarai_branch_code">
                                    <input class="s_size" ref="maebarai_branch_code" type="text" v-model="data.settings.maebarai_branch_code">
                                </error-display>
                            @else
                                {{ $employee->maebarai_branch_code }}
                            @endcan
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="right_10"><span class="red">※</span>支店名</span>
                            @can('change_employee_maebarai_info')
                                <error-display :message="errors.maebarai_branch_name">
                                    <input class="l_size" ref="maebarai_branch_name" type="text" v-model="data.settings.maebarai_branch_name">
                                </error-display>
                            @else
                                {{ $employee->maebarai_branch_name }}
                            @endcan
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="right_10"><span class="red">※</span>口座番号</span>
                            @can('change_employee_maebarai_info')
                                <error-display :message="errors.maebarai_account_number">
                                    <input class="m_size" ref="maebarai_account_number" type="text" v-model="data.settings.maebarai_account_number">
                                </error-display>
                            @else
                                {{ $employee->maebarai_account_number }}
                            @endcan
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="right_10"><span class="red">※</span>口座名義(カナ)</span>
                            @can('change_employee_maebarai_info')
                                <error-display :message="errors.maebarai_account_name">
                                    <input class="m_size" ref="maebarai_account_name" type="text" v-model="data.settings.maebarai_account_name">
                                </error-display>
                            @else
                                {{ $employee->maebarai_account_name }}
                            @endcan
                        </td>
                    </tr>
                </table>
                <section class="btn">
                    @can('change_employee_maebarai_info')
                        <p class="button right_30"><button class="m_size l_height btn_greeen l_font" v-on:click="submit">設定を保存</button></p>
                        <p class="button right_30"><a class="m_size l_height btn_gray l_font" href="{{ Caeru::route('edit_employee_maebarai', [$employee->id, $page]) }}">キャンセル</a></p>
                    @endcan
                    <p class="button"><a class="m_size l_height btn_gray l_font" href="{{ Caeru::route('employees_list', ['page' => $page]) }}">一覧に戻る</a></p>
                </section>
            </section>
        </section>
    </main>
@endsection
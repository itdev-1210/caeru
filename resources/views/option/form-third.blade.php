<div id="form-third">
    
    @if (Session::get('current_work_location') != "all")
        @can('change_worktime_info')
        <section class="add_item">
            <div class="search_box right_10 left">
                <span>勤務時間名</span>
            </div>
            <div class="search_box right_30 left">
                <div class="input_wrapper" :class="checkError">
                    <input v-model="data.name_option" ref='search' class="m_size" @keydown.enter.prevent="addRows" type="text">
                    <div class='error_wrapper'>
                        <span class="tool_error">@{{ data.message_error }}</span>
                    </div>
                </div>
            </div>
            <p class="button left"><button class="s_size s_height btn_gray" v-on:click="addRows">追加</button></p>
        </section>
        @endcan
    
        <section class="default_table">
            <table id="勤務時間" v-cloak>
                <tr>
                    <th class="s_10">勤務時間</th>
                    <th class="s_14">出勤</th>
                    <th class="s_14">退勤</th>
                    <th class="s_14"><span class="red">※</span>休憩</th>
                    <th class="s_14">(内)深休</th>
                    <th class="s_14"><span class="red">※</span>所定</th>
                    @can('change_worktime_info')
                        <th class="s_10">利用</th>
                        <th class="s_10"></th>
                    @endcan
                </tr>
                <tr v-for="(work_time_default, key) in data.list_work_time_default">
                    <td class="s_10">@{{work_time_default.name}}</td>

                    <td class="s_14" v-if="data.isAdd || data.index !== key">
                        @{{work_time_default.start_work_time | dropTheSecond}}
                    </td>
                    <td class="s_14" v-else>
                        <input class="ss_size" type="text" v-model="data.start_work_time">
                    </td>

                    <td class="s_14" v-if="data.isAdd || data.index !== key">
                        @{{work_time_default.end_work_time | dropTheSecond}}
                    </td>
                    <td class="s_14" v-else>
                        <input class="ss_size" type="text" v-model="data.end_work_time">
                    </td>

                    <td class="s_14" v-if="data.isAdd || data.index !== key">
                        @{{work_time_default.break_time}}
                    </td>
                    <td class="s_14" v-else>
                        <error-display :message="errors.break_time">
                            <input class="ss_size" type="text" ref='break_time' v-model="data.break_time">
                        </error-display>
                    </td>

                    <td class="s_14" v-if="data.isAdd || data.index !== key">
                        @{{work_time_default.night_break_time}}
                    </td>
                    <td class="s_14" v-else>
                        <input class="ss_size" type="text" v-model="data.night_break_time">
                    </td>

                    <td class="s_14" v-if="data.isAdd || data.index !== key">
                        @{{work_time_default.working_hour | dropTheSecond}}
                    </td>
                    <td class="s_14" v-else>
                        <error-display :message="errors.working_hour">
                            <input class="ss_size" type="text" ref='working_hour' v-model="data.working_hour">
                        </error-display>
                    </td>
                    @can('change_worktime_info')
                    <td class="s_10" v-if="data.isAdd || data.index !== key">
                        <label class="radio_text right_30"><input type="radio" value="1" v-model="work_time_default.visible" v-on:change="setVisible(key)">有</label>
                        <label class="radio_text"><input type="radio" value="0" v-model="work_time_default.visible"  v-on:change="setVisible(key)">無</label>
                    </td>
                    <td class="s_10" v-else>
                        <label class="radio_text right_30"><input type="radio" value="1" v-model="data.visible">有</label>
                        <label class="radio_text"><input type="radio" value="0" v-model="data.visible">無</label>
                    </td>
                    <td class="s_10"><a><img class="ico_delete" src="{{ asset('images/ico_delete.svg') }}" v-on:click="removeRows(data.list_work_time_default,key)"></a></td>
                    @endcan
                </tr>
            </table>
        </section>
    
        @can('change_worktime_info')
        <section class="btn">
            <p class="button right_30"><button type='button' v-on:click="submit" class="m_size l_height btn_greeen l_font">{{ (Session::get('current_work_location') == "all") ? "勤務地に反映" : "保存"  }}</button></p>
            <p class="button right_30"><a class="m_size l_height btn_gray l_font" href="{{ Caeru::route('edit_option_work_time') }}">キャンセル</a></p>
        </section>
        @endcan
    @endif
</div>


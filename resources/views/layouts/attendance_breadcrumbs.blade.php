<p class="breadcrumb">
    @foreach($breadcrumbs as $breadcrumb)
        @if(!$loop->last)
            @if($breadcrumb->isTraversable() == true)
                <a href="{{ Caeru::route('breadcrumb_go_to_level', [ 'level' => $breadcrumb->getLevel() ]) }}">{{ $breadcrumb->getName() }}</a>
            @else
                <span>{{ $breadcrumb->getName() }}</span>
            @endif
            &emsp;&#62;&emsp;
        @else
            <span>{{ $breadcrumb->getName() }}</span>
        @endif
    @endforeach
        <!-- &emsp;&#62;&emsp;<span>勤怠データ管理</span> -->
</p>
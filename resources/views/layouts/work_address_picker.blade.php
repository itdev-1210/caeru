<transition name="fade">
    <div class="work_address_picker_wrapper" v-cloak v-show="display">
        <div class="work_address_picker">
            <section class="select_one">
                <section class="select_one_innner">
                    <h2>訪問先変更</h2>
                    <section class="right_position">
                        <p class="button left_10"><a class="mm_size s_height btn_gray modal-open" data-target="con1" @click="toggleDisable">無効な勤務地も表示</a></p>
                    </section>
                </section>
            </section>
            @if (isset($list))
                <section class="table">
                    <table>
                        <tr>
                            <th class="s_5">訪問先ID</th>
                            <th class="s_12">訪問先</th>
                            <th class="s_5"></th>
                        </tr>
                        @foreach ($list as $work_address)
                            @if (!$work_address['enable'])
                                <tr class="light_gray" v-show="displayDisabled">
                            @else
                                <tr>
                            @endif
                                    <td>{{ $work_address['presentation_id'] }}</td>
                                    <td>{{ $work_address['name'] }}</td>
                                    <td>
                                        @if (isset($restricted) && ($restricted == true))
                                        @else
                                            <p class="button"><a class="ss_size s_height btn_gray" single-click href="{{ Caeru::route('work_address_working_month_list', ['work_address' => $work_address['id'], 'business_month' => $business_month]) }}">選択</a></p>
                                        @endif
                                    </td>
                                </tr>
                        @endforeach
                    </table>
                </section>
            @endif
            <section class="btn">
                <p class="button"><a class="modal-close m_size l_height btn_gray l_font" @click="close">キャンセル</a></p>
            </section>
        </div>
        <div class="modal-overlay" @click="close"></div>
    </div>
</transition>
<div class="plans_switch_wrap">
    <div class="plans_switch_wrap_inner">
        <input name="onoff" value="1" type="radio" class="on" @change="displayToogle" {{ isset($default) ? $default == 1 ? 'checked' : null : null }}>
        <span class="left_plans_text">予定表示</span>
        <span class="right_plans_text">所定表示</span>
        <input name="onoff" value="0" type="radio" class="off" @change="displayToogle" {{ isset($default) ? $default == 0 ? 'checked' : null : null }}>
        <div id="selector">
            <div></div>
        </div>
    </div>
</div>
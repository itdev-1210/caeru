
@push('scripts')
    <script defer src="{{ asset('/js/mics.js') }}" defer></script>
    <script defer src="{{ asset('/js/components/alert_module.js') }}" defer></script>
@endpush

<section id="header_wrapper">
	<section class="alert_wrapper_fixer">
		<section class="alert_wrapper">
			<section class="alert_innner">
				<section class="alert_box save_green"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_save.svg') }}"></span>
					@if (session()->has('success'))
						<span class="alert_content">{{ session('success') }}</span>
					@endif
				</section>
				<section class="alert_box save_yellow"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_dakoku.svg') }}"></span>
					@if (session()->has('warning'))
						<span class="alert_content">{{ session('warning') }}</span>
					@endif
				</section>
				<section class="alert_box save_red"><span class="alert_ico right_10"><img src="{{ asset('images/ico_alert_error.svg') }}"></span>
					@if (count($errors) || session()->has('error'))
						<span class="alert_content">{{ session()->has('error') ? session('error') : '入力に誤りがあります' }}</span>
					@endif
				</section>
			</section>
		</section>
	</section>
    <header>
        <a href ="{{ Caeru::route('dashboard') }}"><div id ="logo"></div></a>
    </header>
    <nav>
        <ul id="dropmenu" class="gnav">
            <li class="home"><a class="{{ $active == 1 ? 'nav_home':'' }}" href ="{{ Caeru::route('dashboard') }}"></a></li>
            <li class ="green">
                <a href ="#" class ="green {{ $active == 2 ? 'nav_basic':'' }}">基本情報</a>
                <ul>
                    @can('view_company_info')
                        <li><a href ="{{ Caeru::route('edit_company') }}">会社情報</a></li>
                    @endcan
                    @can('change_manager_info')
                        <li><a href ="{{ Caeru::route('managers_list') }}">管理者情報</a></li>
                    @endcan
                    @can('view_work_location_info')
                        <li><a href ="{{ Caeru::route('work_locations_list') }}">勤務地情報</a></li>
                    @endcan
                    @if ( Auth::user()->company->use_address_system == true )
                        @can('view_work_address_info')
                            <li><a href ="{{ Caeru::route('work_address_list') }}">訪問先情報</a></li>
                        @endcan
                    @endif
                    @can('see_employee_tab')
                        <li><a href ="{{ Caeru::route('employees_list') }}">従業員情報</a></li>
                    @endcan
                    @can('view_calendar')
                        <li><a href ="{{ Caeru::route('edit_calendar') }}">カレンダー</a></li>
                    @endcan
                    @can('view_setting_info')
                        <li><a href ="{{ Caeru::route('edit_setting') }}">設定</a></li>
                    @endcan
                    @can('view_option_item_info')
                        @can('view_option_info')
                            <li><a href ="{{ Caeru::route('edit_option_work_rest') }}">項目設定</a></li>
                        @elsecan('view_department_info')
                            <li><a href ="{{ Caeru::route('edit_option_department') }}">項目設定</a></li>
                        @else
                        <li><a href ="{{ Caeru::route('edit_option_work_time') }}">項目設定</a></li>
                        @endcan
                    @endcan
                </ul>
            </li>
            <li class ="yellow">
                <a href ="#" class ="yellow {{ $active == 3 ? 'nav_attendance':'' }}">勤怠管理</a>
                <ul>
                    @can('view_attendance_management_page')
                        <li><a href="{{ Caeru::route('employee_attendance', ['reset_search_condition' => true]) }}">勤怠データ管理</a></li>
                    @endcan
                    @can('view_attendance_advance_search_page')
                        <li><a href="{{ Caeru::route('show_advance_search', ['reset_search_condition' => true]) }}">勤怠データ検索</a></li>
                    @endcan
                    @can('view_attendance_month_summary_page')
                        <li><a href="{{Caeru::route('show_month_summary',['reset_search_condition' => true]) }}">勤怠集計</a></li>
                    @endcan
                    @can('view_work_data_paid_holiday_management')
                        <li><a href="{{Caeru::route('list_paid_holiday', ['refresh_hitory' => true])}}">有給休暇管理</a></li>
                    @endcan
                    @if ( Auth::user()->company->use_address_system == true && Auth::user()->can('view_work_address_attendance_page'))
                        <li><a href="{{ Caeru::route('work_address_attendance', ['reset_search_condition' => true]) }}">訪問先別勤務情報</a></li>
                    @endif
                    <li><a href ="{{Caeru::route('checklists_list', ['refresh_hitory' => true])}}">チェックリスト</a></li>
                </ul>
            </li>
            @can('view_maebarai_page')
            <li class="skyblue">
                <a class="skyblue {{ $active == 4 ? 'chosen_skyblue':'' }}" href="#">前払管理</a>
                <ul>
                    @can('view_maebarai_setting_page')
                        <li><a href="{{ Caeru::route('maebarai_setting') }}">前払設定</a></li>
                    @endcan
                    @can('view_maebarai_information_page')
                        <li><a href="{{ Caeru::route('maebarai_information', ['reset_search_condition' => true]) }}">前払情報</a></li>
                    @endcan
                    {{-- <li><a href="settlement_information.html">決済情報</a></li> --}}
                </ul>
            </li>
            @endcan
        </ul>
    </nav>
    <div id ="sign_out"><a href ="{{ Caeru::route('logout') }}">ログアウト</a></div>
</section>
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'IDまたはパスワードが正しくありません。',
    'throttle' => '後 :seconds　秒にもう一度ログインしてみてください。',
    'mismatched_ip' => "IPアドレスの制限により、ログインする事ができません。"
];

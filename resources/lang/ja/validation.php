<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'これが認められる必要があります。',
    'active_url'           => '正確なURLを入力してください。',
    'after'                => ':dateより後の日にちを入力してください。',
    // 'after_or_equal'       => ':date以降の日にちを入力してください。',
    'after_or_equal'       => ':dateより後の日にちを入力してください。',
    'alpha'                => 'この項目は文字だけ入力してください。',
    'alpha_dash'           => 'この項目は文字と数字と「_」だけ入力してください。',
    'alpha_num'            => 'この項目は文字と数字だけ入力してください。',
    'array'                => '配列を入力してください。',
    'before'               => ':dateより前の日にちを入力してください。',
    'before_or_equal'      => ':date以前の日にちを入力してください。',
    'between'              => [
        'numeric' => ':min 〜:max の間を入力してください。',
        'file'    => ':min kbから :max kbまでのファイルを使ってください。',
        'string'  => '文字を　:min から :max　まで 入力してください。',
        'array'   => ':min　から :max までのアイテムを使ってください。',
    ],
    'boolean'              => '「true」または「false」を入力してください。',
    'confirmed'            => '同じ　:attribute　を入力してください。',
    'date'                 => '正確な日付を入力してください。',
    'date_format'          => '形式: :formatの日付を入力してください。',
    'different'            => ':other　と違う値を入力してください。',
    'digits'               => ':digits桁の数字を入力してください。',
    'digits_between'       => ':max桁以内の数字を入力してください。',
    'dimensions'           => '適切なサイズの画像を選択してください。',
    'distinct'             => '重複値を入力しないでください。',
    'email'                => '正確なメールを入力してください。',
    'exists'               => '入力された値は存在していません。',
    'file'                 => 'ファイルを入力してください。',
    'filled'               => 'この項目を入力してください。',
    'image'                => '画像を選択してください。',
    'in'                   => '選択した値は正確ではありません。',
    'in_array'             => '「:attribute」は「 :other」の中ではありません。',
    'integer'              => '整数を入力してください。',
    'ip'                   => '正確なIPアドレスを入力してください。',
    'json'                 => '正確なJSON文字列を入力してください。',
    'max'                  => [
        'numeric' => ':max以下の値を入力してください。',
        'file'    => ':maxkb以下のファイルを選択してください。',
        'string'  => ':max文字以内で入力してください。',
        'array'   => ':maxアイテム以下の配列を入力してください。',
    ],
    'mimes'                => ':valuesの形式のデータを入力してください。',
    'mimetypes'            => ':valuesの形式のデータを入力してください。',
    'min'                  => [
        'numeric' => ':min以上の値を入力してください。',
        'file'    => ':minkb以上のファイルを選択してください。',
        'string'  => ':min文字以上で入力してください。',
        'array'   => ':minアイテム以上の配列を入力してください。',
    ],
    'not_in'               => '入力された値は正確ではありません。',
    'numeric'              => '数字を入力してください。',
    'present'              => ':attributeの存在必要があります。',
    'regex'                => '入力されたデータの形式は正確ではありません。',
    'required'             => 'この項目を入力してください。',
    'required_if'          => ':otherは:valueになれば、この項目が必要があります。',
    'required_unless'      => ':otherは:valuesではないなら、この項目が必要があります。',
    'required_with'        => '全ての項目を入力してください。',
    'required_with_all'    => ':valuesが入力されているなら、入力する必要があります。',
    'required_without'     => ':valuesが入力されていないなら、入力する必要があります。',
    'required_without_all' => ':valuesが入力されていないなら、入力する必要があります。',
    'same'                 => ':attributeと:otherに同じ値を入力してください。',
    'size'                 => [
        'numeric' => ':sizeを入力してください。',
        'file'    => ':sizekbファイルを入力してください。',
        'string'  => ':size文字を入力してください。',
        'array'   => '配列のサイズは:sizeにしてください。',
    ],
    'string'               => '文字列を入力してください。',
    'timezone'             => '正確なタイムゾーンを入力してください。',
    'unique'               => '入力された値は既に登録されています。',
    'uploaded'             => 'アップロードが失敗しました。',
    'url'                  => '正しいリンクを入力してください。',

    // Caeru custom validation rule
    'year'                 => '正確な年を入力してください。',
    'time'                 => '時間を入力してください。',
    'furigana'             => '入力されたデータの形式は正確ではありません。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'card_code' => [
            'not_in' => 'すでに登録されカードです。',
            'in' => '無効なカードです。',
        ],
        'employee_presentation_id' => [
            'in' => '無効な従業員IDです。',
        ],
        'work_location_number' => [
            'in' => '無効な勤務地IDです。',
        ],
        'work_time_change_date' => [
            'after' => '今日より後の日にちを入力してください'
        ],
        'change_date' => [
            'after' => '未来の日付を入力してください。',
            'date' => '1990-01-01の形式で入力してください。',
            'date_format' => '1990-01-01の形式で入力してください。'
        ],
        'working_hour' => [
            'working_hour' => '正確時間を入力してください。'
        ],
        'new_password' => [
            'confirmed' => '新しいパスワードが一致していません。',
        ],
        'new_email' => [
            'confirmed' => '新しいメールアドレスが一致していません。'
        ],
        'password' => [
            'confirmed' => '同じパスワードを入力してください。'
        ],
        'candidate_number' => [
            'required_if' => '候補の場合なら、必要人数を入力してください。'
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'latitude' => '緯度',
        'longitude' => '経度',
        'login_range' => '打刻許容範囲',
        'start_date' => '勤務開始日',
    ],

];

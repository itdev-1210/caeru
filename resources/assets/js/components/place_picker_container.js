import PlacePicker from './place_picker';

// The root element
var app = new Vue({
    el: '#form',
    data: {
        placePickerDisplay : false,
        workLatitude: $("[name='latitude']").val(),
        workLongitude: $("[name='longitude']").val(),
        address: $("[name='address']").val(),
        todofuken: parseInt($("[name='todofuken']").val()),
    },
    methods: {
        open: function() {
            this.placePickerDisplay = true;
        },
        close: function() {
            this.placePickerDisplay = false;
        },
        changeGeocode: function(latitude, longitude) {
            this.workLatitude = latitude;
            this.workLongitude = longitude;
        },
        changeAddress: function(address) {
            this.address = address;
        },
        changeTodofuken: function(newValue) {
            this.todofuken = newValue;
        }
    },
    components: {
        'place-picker': PlacePicker,
    },
})
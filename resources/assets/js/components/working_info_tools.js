export default {
    methods: {
        // Utility functions

        // The getter for all of the attributes of EmployeeWorkingInformation instance
        getFromRootOrChanged: function(field_name) {
            if (this.changed[field_name] !== null)
                return this.changed[field_name];
            else
                return this.root[field_name];
        },

        // This is the counter-part of getCarbonInstance in the model.
        getMomentInstance: function(time_string) {
            if (time_string !== null && time_string !== '') {

                if (time_string.indexOf(' ') !== -1) {
                    return moment(time_string, 'YYYY-MM-DD HH:mm:ss');
                } else {

                    let day_string = (this.root.day_of_the_upper_limit !== null) ? this.root.day_of_the_upper_limit : this.today;
                    let instance = moment(day_string + ' ' + time_string, 'YYYY-MM-DD HH:mm:ss');

                    if (this.root.date_upper_limit !== null) {
                        let dateUpperLimit = moment(this.root.date_upper_limit, 'YYYY-MM-DD HH:mm:ss');
                        if (instance.isBefore(dateUpperLimit))
                            instance.add(1, 'days');
                    }

                    return instance;
                }

            } else {
                return null;
            }
        },

        // Validate the time string
        validateTimeFormat: function(string) {
            let time = moment(string, ['HHmm', 'H:mm', 'H:mm:ss'], true);

            if (time.isValid()) {

                // Set time's date to today or to the root's date of upper limit (if it exists)
                let dayBase = (this.root.day_of_the_upper_limit !== null) ? this.root.day_of_the_upper_limit : this.today;
                let dateData = _.split(dayBase, '-');

                time.year(dateData[0]);
                // Have to minus 1 because the month is zero-indexed.
                time.month(dateData[1]-1);
                time.date(dateData[2]);

                if (this.root.date_upper_limit !== null) {
                    let dateUpperLimit = moment(this.root.date_upper_limit, 'YYYY-MM-DD HH:mm:ss');
                    if (time.isBefore(dateUpperLimit))
                        time.add(1, 'days');
                }

                // Then return the full-fledge time string
                return time.format('YYYY-MM-DD HH:mm:ss');

            }
            return null;
        },
        // Compare two moment instance and make the second one bigger if it's smaller or equal to the first one.
        makeTheSecondOneBigger: function(momentStart, momentEnd) {
            if (momentStart !== null && momentEnd !== null) {
                while (momentEnd.isBefore(momentStart)) {
                    momentEnd.add(1, 'days');
                }
                return momentEnd;
            } else {
                return null;
            }
        },

        // Blur and enter.key_down event handler for some inputs
        updateAndReload: function(computedProperty, event, reformatTime = true) {
            this[computedProperty] = event.target.value;
            let reloadedValue = reformatTime ? this.$options.filters.reformatTime(this[computedProperty]) : this[computedProperty];
            event.target.value = reloadedValue;
        },

        // Validate number string
        validateNumber: function(number, mustBeSmallerThan24Hours = true) {
            if (!isNaN(number) && number !== '') {
                if (mustBeSmallerThan24Hours) {
                    if (0 <= number && number <= 1440)
                        return _.toSafeInteger(number);
                } else {
                    if (0 <= number)
                        return _.toSafeInteger(number);
                }
            }
            return null;
        },
    },
    filters: {
        reformatTime: (value) => {
            let moment = window.moment(value, ['YYYY-MM-DD HH:mm:ss', 'HH:mm:ss'], true);
            return moment.isValid() ? moment.format('H:mm') : null;
        },
    }
}
export default {
    data: function() {
        return {
            // Date picker
            datePickerData: window.date_picker_data,
            showDatePicker: false,
            datePickerTimeNavigationData: null,
        }
    },
    computed: {
        currentDateInArray: {
            get: function() {
                return (this.datePickerTimeNavigationData === null) ? _.map(_.split(this.anchorDate, '-'), (value) => {return _.toInteger(value)}) : this.datePickerTimeNavigationData;
            },
            set: function(data) {
                this.datePickerTimeNavigationData = data;
            },
        },
        momentAnchorDate: function() {
            return moment(this.anchorDate);
        },
        datePickerOptions: function() {
            return this.datePickerData !== undefined ?
                {
                    'year' : this.currentDateInArray[0],
                    'month' : this.currentDateInArray[1],
                    'nationalHolidays' : this.datePickerData['national_holidays'],
                    'restDays' : this.datePickerData['rest_days'],
                    'flipColorDay' : this.datePickerData['flip_color_day'],
                    'startColor' : (this.currentDateInArray[1] % 2) === 0,
                    'pickerMode' : true,
                } :
                {
                    'year' : this.currentDateInArray[0],
                    'month' : this.currentDateInArray[1],
                    'startColor' : (this.currentDateInArray[1] % 2) === 0,
                    'pickerMode' : true,
                }
            ;
        }
    },
    methods: {
        // DatePicker
        toggleDatePicker: function() {
            this.showDatePicker = !this.showDatePicker;
            if (this.showDatePicker === true) {
                this.$nextTick(function() {
                    this.repositionByHeight();
                });

            } else {
                this.datePickerTimeNavigationData = null;
            }
        },
        repositionByHeight: function() {
            let popUp = $('.normal_date_picker .caeru_calendar_date_picker_popup');
            let diffHeight = window.innerHeight - popUp.outerHeight();
            let diffWidth = window.innerWidth - popUp.outerWidth();
            let scrollOffset = $(window).scrollTop();
            popUp.offset({ top: (scrollOffset + diffHeight/2), left: diffWidth/2 });
        },
        anchorDateChanged: function(value) {
            this.anchorDate = value;
            this.toggleDatePicker();
        },
        datePickerChangeTime: function(year, month) {
            this.datePickerTimeNavigationData = [year, month];
        },

        // When click '表示' button or moving around with 4 arrow buttons
        goToThatDay: function() {
            let data = {
                'anchor_date': this.anchorDate,
            }
            this.getDataBydate(data);
        },
        nextDay: function() {
            let data = {
                'anchor_date': this.momentAnchorDate.clone().add(1, 'days').format('YYYY-MM-DD'),
            }
            this.getDataBydate(data);
        },
        previousDay: function() {
            let data = {
                'anchor_date': this.momentAnchorDate.clone().subtract(1, 'days').format('YYYY-MM-DD')
            }
            this.getDataBydate(data);
        },
        nextWeek: function() {
            let data = {
                'anchor_date': this.momentAnchorDate.clone().add(1, 'weeks').format('YYYY-MM-DD')
            }
            this.getDataBydate(data);
        },
        previousWeek: function() {
            let data = {
                'anchor_date': this.momentAnchorDate.clone().subtract(1, 'weeks').format('YYYY-MM-DD')
            }
            this.getDataBydate(data);
        },
    },
    mounted: function() {
        // The calendar pop up
        this.repositionByHeight();
        this.$nextTick(function() {
            window.addEventListener('resize', this.repositionByHeight);
        });
    },
}
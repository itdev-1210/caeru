export default {
    data: function() {
        return {
            consts: {
                SUNDAY: window.consts_sunday,
                SATURDAY: window.consts_saturday,
            }, 
        }
    },
    methods: {
        dateClass: function(day) {
            return {
                'sunday': (day['day_of_week'] == this.consts.SUNDAY) || (day['national_holiday'] == true),
                'saturday': day['day_of_week'] == this.consts.SATURDAY,
                'pink_holiday': day['law_rest_day'] == true,
                'blue_holiday': day['normal_rest_day'] == true,
            };
        },
        japaneseDayOfWeek: function(dayIndex) {
            switch (dayIndex) {
                case 0:
                    return '日';
                case 1:
                    return '月';
                case 2:
                    return '火';
                case 3:
                    return '水';
                case 4:
                    return '木';
                case 5:
                    return '金';
                case 6:
                    return '土';
                default:
                    return null;
            }
        },
        reformatHourString: function(hourString) {
            let momentHour = moment(hourString);
            if (momentHour.isValid()) {
                return momentHour.format('HH:mm');
            }
        },
        convertToHourMinute: function(minutes, filterEmpty = false) {
            if (filterEmpty === false || minutes !== 0) {
                return (!_.isNaN(minutes)) && (minutes != null) ? _.padStart(_.floor(_.toInteger(minutes)/60), 2, '0') + ':' + _.padStart(_.toInteger(minutes)%60, 2, '0') : minutes;
            } else {
                return null;
            }
        },
    },
}
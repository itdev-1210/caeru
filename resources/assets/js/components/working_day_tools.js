export default {
    data: {
        // Japanese characters for days of week
        daysOfWeek: ['日', '月', '火', '水', '木', '金', '土'],

        showDatePicker: false,
        datePickerOptions: null,
        datePickerTimeNavigationData: null,
        datePickerData: {
            flipColorDay: window.flip_color_day,
            restDays: window.rest_days,
            nationalHolidays: window.national_holidays,
        },

        calendar_rest_day_consts: {
            LAW_BASED_REST_DAY  : 1,
            NORMAL_REST_DAY     : 2,
            NOT_A_REST_DAY      : 0,
        },
    },
    computed: {
        currentDateInArray: {
            get: function() {
                return (this.datePickerTimeNavigationData === null) ? _.map(_.split(this.currentDate, '-'), (value) => {return _.toInteger(value)}) : this.datePickerTimeNavigationData;
            },
            set: function(data) {
                this.datePickerTimeNavigationData = data;
            },
        },
    },
    methods: {
        // Get the string of the date in japanese
        formatDate: function(date_string) {
            if (!!date_string) {
                let date = new Date(date_string);
                return date.getFullYear() + '年' + (date.getMonth()+1) + '月' + date.getDate() + '日(' + this.daysOfWeek[date.getDay()] + ')';
            }
        },

        // These function are for the date navigation panel
        nextDay: function() {
            let currentDate = new Date(this.currentDate);
            currentDate.setDate(currentDate.getDate()+1);
            return this.setDateToLink(currentDate);
        },
        previousDay: function() {
            let currentDate = new Date(this.currentDate);
            currentDate.setDate(currentDate.getDate()-1);
            return this.setDateToLink(currentDate);
        },
        setDateToLink: function(date_object) {
            let link_parts = _.split(window.location.pathname, '/');
            return _.join(_.initial(link_parts), '/') + '/' + date_object.getFullYear() + '-' + _.padStart((date_object.getMonth()+1), 2, '0') + '-' + _.padStart(date_object.getDate(), 2, '0');
        },

        // DatePicker
        toggleDatePicker: function() {
            this.showDatePicker = !this.showDatePicker;
            if (this.showDatePicker === true) {
                this.$nextTick(function() {
                    this.repositionByHeight();
                });
            }
        },
        datePickerChangeTime: function(year, month) {
            this.datePickerTimeNavigationData = [year, month];
            this.processDatePickerOptions();
        },
        goToThisDay: function(date) {
            let urlParts = _.split(window.location.href, '/');
            urlParts.pop();
            urlParts.push(date);
            window.location.href = _.join(urlParts, '/');
        },
        // filterByYearAndMonth: function(collection) {
        //     let filtered = _.filter(collection, (item) => {
        //         return (item[0] === this.currentDateInArray[0]) && (item[1] === this.currentDateInArray[1]);
        //     });
        //     return _.map(filtered, (item) => {return item[2];});
        // },
        processDatePickerOptions: function() {
            // let nationalHolidays = [];
            // let lawRestDay = [];
            // let normalRestDay = [];

            // _.forEach(this.datePickerData['restDays'], (day) => {
            //     if (day['type'] === this.calendar_rest_day_consts['LAW_BASED_REST_DAY']) {
            //         lawRestDay.push( _.map(_.split(day['assigned_date'], '-'), (data) => { return _.toInteger(data)}) );
            //     } else if (day['type'] === this.calendar_rest_day_consts['NORMAL_REST_DAY']) {
            //         normalRestDay.push( _.map(_.split(day['assigned_date'], '-'), (data) => { return _.toInteger(data)}) );
            //     }
            // });

            // nationalHolidays = _.map(this.datePickerData['nationalHolidays'], (day) => {
            //     return _.map(_.split(day, '-'), (data) => { return _.toInteger(data)});
            // });

            this.datePickerOptions = {
                'year' : this.currentDateInArray[0],
                'month' : this.currentDateInArray[1],
                // 'nationalHolidays' : this.filterByYearAndMonth(nationalHolidays),
                // 'lawRestDay' : this.filterByYearAndMonth(lawRestDay),
                // 'normalRestDay' : this.filterByYearAndMonth(normalRestDay),

                'nationalHolidays' : this.datePickerData['nationalHolidays'],
                'restDays' : this.datePickerData['restDays'],
                'flipColorDay' : this.datePickerData['flipColorDay'],
                'startColor' : (this.currentDateInArray[1] % 2) === 0,
                'pickerMode' : true,
            }
        },
        repositionByHeight: function() {
            let popUpDatePicker = $('.normal_date_picker .caeru_calendar_date_picker_popup');
            let diffHeight = window.innerHeight - popUpDatePicker.outerHeight();
            let diffWidth = window.innerWidth - popUpDatePicker.outerWidth();
            let scrollOffset = $(window).scrollTop();
            popUpDatePicker.offset({ top: (scrollOffset + diffHeight/2), left: diffWidth/2 });

            let popUpApproverForm = $('#approver_form');
            let diffHeightApprover = window.innerHeight - popUpApproverForm.outerHeight();
            let diffWidthApprover = window.innerWidth - popUpApproverForm.outerWidth();
            popUpApproverForm.offset({ top: (scrollOffset + diffHeightApprover/2), left: diffWidthApprover/2 });
        },
    }
}
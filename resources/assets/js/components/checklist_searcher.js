import Hub from '../components/hub.js';
import Autocomplete from '../components/caeru_autocomplete';

const hub = Hub;
// Search when change Month and Year
var now = new Date();
var currentYear = now.getFullYear();
var currentMonth = now.getMonth() + 1;

// Wait for the document to be ready, so that the $.companyCodeIncludedUrl() can be available to both instances
$(document).ready(function() {
    var searcherDay = new Vue({
        el: '.checklist-search',
        data: () => ({
            yearSelected:currentYear,
            monthSelected:currentMonth,
            checklists: window.checklists,

            // Search for employees who are registered to the current work locations: 1
            // Search for employees who have EWI of the current work locations: 2
            searchTarget: null,

            employeeId: "",
            employeeName: "",
            departments: [],
            checkedEr:[],
            totaldakoku:0,
            totalhyou:0,
            checklistsHistory: window.checklistsHistory,
            employeeNameList: window.employee_names,
            beginDateString: '',
            endDateString: '',
            consts: {
                'START_WORK_ERROR': 101,
                'END_WORK_ERROR': 102,
                'GO_OUT_RETURN_ERROR': 103,
                'FORGOT_END_WORK_ERROR': 104,
                'FORGOT_RETURN_ERROR': 105,
                'LATE_OR_LEAVE_EARLY_TYPE': 201,
                'OFF_SCHEDULE_TIME': 202,
                'STATUS_MISTAKEN': 203,
                'OVERLIMIT_BREAK_TIME': 204,
                'WORK_WITHOUT_SCHEDULE': 205,
                'HAVE_SCHEDULE_BUT_OFFLINE': 206,
                'DIFFERENCE_FROM_SCHEDULE': 207,
            },
            defaultSearchConditions: {
                'searchTarget': 2,
                'employeeId': "",
                'employeeName': "",
                'checkedEr': [],
            }
        }),
        computed: {
            checkedEndWorkError: {
                get: function() {
                    return _.indexOf(this.checkedEr, this.consts['END_WORK_ERROR']) !== -1 && _.indexOf(this.checkedEr, this.consts['FORGOT_END_WORK_ERROR']) !== -1;
                },
                set: function(value) {
                    if (value == true) {
                        this.checkedEr.push(this.consts['END_WORK_ERROR']);
                        this.checkedEr.push(this.consts['FORGOT_END_WORK_ERROR']);

                    } else {
                        this.checkedEr.splice(_.indexOf(this.checkedEr, this.consts['END_WORK_ERROR']), 1);
                        this.checkedEr.splice(_.indexOf(this.checkedEr, this.consts['FORGOT_END_WORK_ERROR']), 1);
                    }
                },
            },
            checkedReturnError: {
                get: function() {
                    return _.indexOf(this.checkedEr, this.consts['GO_OUT_RETURN_ERROR']) !== -1 && _.indexOf(this.checkedEr, this.consts['FORGOT_RETURN_ERROR']) !== -1;
                },
                set: function(value) {
                    if (value == true) {
                        this.checkedEr.push(this.consts['GO_OUT_RETURN_ERROR']);
                        this.checkedEr.push(this.consts['FORGOT_RETURN_ERROR']);

                    } else {
                        this.checkedEr.splice(_.indexOf(this.checkedEr, this.consts['GO_OUT_RETURN_ERROR']), 1);
                        this.checkedEr.splice(_.indexOf(this.checkedEr, this.consts['FORGOT_RETURN_ERROR']), 1);
                    }
                },
            }
        },
        methods: {
            yearChanged: function(){
                this.submit();
            },
            monthChanged: function(){
                this.submit();
            },
            nextMonth: function() {
                this.monthSelected++;
                if(this.monthSelected > 12) {
                    this.yearSelected++;
                    this.monthSelected = 1;
                }
                this.submit();
            },
            preMonth: function() {
                this.monthSelected--;
                if(this.monthSelected < 1) {
                    this.yearSelected--;
                    this.monthSelected = 12;
                }
                this.submit();
            },
            updateTheDateStrings: function() {
                var beginDate = new Date(this.checklists.beginDate);
                var endDate = new Date(this.checklists.endDate);
                this.beginDateString = beginDate.getFullYear() + '年 ' + (beginDate.getMonth() + 1) + '月 ' + beginDate.getDate() + '日';
                this.endDateString = endDate.getFullYear() + '年 ' + (endDate.getMonth() + 1) + '月 ' + endDate.getDate() + '日';
            },
            resetConditions: function(){
                Object.assign(this.$data, this.defaultSearchConditions);
                this.multipleSelectBox.multipleSelect('uncheckAll');
                hub.$emit('reset-autocomplete');
            },
            employeeNameSelected: function(id) {
                this.employeeName = this.employeeNameList[id].name;
            },
            employeeNameChanged: function(value) {
                this.employeeName = value;
            },
            submit: function(refreshSession){
                var that = this;
                var data = {
                    'searchTarget': this.searchTarget,
                    'year': this.yearSelected,
                    'month': this.monthSelected,
                    'errlist': this.checkedEr,
                    'employeeId': this.employeeId,
                    'employeeName': this.employeeName,
                    'departments' : this.departments,
                    'refreshSession': refreshSession || false,
                };
                this.updateTheDateStrings();
                axios.post($.companyCodeIncludedUrl('/checklist/search'), data).then(response => {
                    //window.location.reload();
                    checklistView.checklists = that.checklists = response.data.checklistsJson;
                    that.checklistsHistory = response.data.checklistsHistory;

                    that.totaldakoku = that.checklists.totaldakoku || 0;
                    that.totalhyou = that.checklists.totalhyou || 0;
                });
            },
            appendData: function (array) {
                let select_box = $("select.ms");
                select_box.empty();
                $.each(array , function(index, val) {
                    select_box.append("<option value="+ index +">"+ val +"</option>")
                });
            }
        },
        watch: {
            checklists : function() {
                this.updateTheDateStrings();
            },
        },
        created: function() {
            this.updateTheDateStrings();
            this.searchTarget = this.checklistsHistory.searchTarget || this.defaultSearchConditions.searchTarget;
            this.employeeId = this.checklistsHistory.employeeId || this.defaultSearchConditions.employeeId;
            this.employeeName = this.checklistsHistory.employeeName || this.defaultSearchConditions.employeeName;
            this.checkedEr = this.checklistsHistory.errlist || this.defaultSearchConditions.checkedEr;
            this.yearSelected = this.checklists.year;
            this.monthSelected = this.checklists.month;

            this.totaldakoku = this.checklists.totaldakoku || 0;
            this.totalhyou = this.checklists.totalhyou || 0;
        },
        components: {
            autocomplete: Autocomplete,
        },
        mounted: function() {
            // The department multiple select
            let select_box = $("select.ms");
            this.appendData(window.departments);
            this.multipleSelectBox = select_box.multipleSelect({
                width: 250,
                selectAll: true,
                minimumCountSelected: 4,
            });
            select_box.change(() => {
                this.departments = $(select_box).val();
            });

            let search_target = $("#search_target");
            search_target.change( () => {
                let departments = search_target.val() == 2 ? window.departments : window.departmentWorkLocations;
                this.appendData(departments);

                this.multipleSelectBox = select_box.multipleSelect({
                    width: 250,
                    selectAll: true,
                    minimumCountSelected: 4,
                });
            })
        },
    });


    var checklistView = new Vue({
        el: '.check_table',
        data: {
            checklists: window.checklists,
        },
        methods: {
            getClassDate: function(s){
                //console.log(s);
                var dateOfWeek = (new Date(s)).getDay();
                var strDay =　['sunday', '', '', '', '', '', 'saturday'];
                return strDay[dateOfWeek];
            },
            fullUrl: function(employeeId, date){
                let url = $.companyCodeIncludedUrl('/employee_working_day/' + employeeId + '/' + date);
                return location.protocol + '//' + location.hostname + url;
            }
        },
        filters : {
            formatDate : function(dispalydate){
                var displayday;
                var dateOfWeek = (new Date(dispalydate)).getDay();
                displayday = dispalydate.replace(/-/g,'/');
                var strDay =['(日)', '(月)','(火)','(水)','(木)','(金)','(土)'];
                return displayday + strDay[dateOfWeek];
            }
        },
    });
})


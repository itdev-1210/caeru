//// Some extended utility function ////
// Check exists
$.fn.exists = function () {
    return this.length !== 0;
}

// Include the company_code part to the url
$.companyCodeIncludedUrl = function(url) {
    let currentCompanyCode = _.split(window.location.pathname, '/')[1];
    return '/' + currentCompanyCode + url;
}

axios.interceptors.request.use(function (config) {
    $('#globalLoader').show();
    return config;
}, function (error) {
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    $('#globalLoader').hide();
    return response;
}, function (error) {
    $('#globalLoader').hide();
    return Promise.reject(error);
});

$(document).ready(function() {



    // A function to update the fixed_table_header in the case you change the table header layout
    document.caeruChangeFixedHeader = function caeruChangeFixedHeader() {

        $('#fakeTableHeader:hidden').remove();

        var table_with_fixed_header = $('table.table_with_fixed_header');
        var fixed_header = table_with_fixed_header.find('tr.fixed_header').clone();

        // In the case the header that we want to fix position have multiple row
        var have_second_row = table_with_fixed_header.find('tr.fixed_header_second_row');
        var fixed_header_second_row = (have_second_row.length > 0) ? have_second_row.clone() : false;
        var have_third_row = table_with_fixed_header.find('tr.fixed_header_third_row');
        var fixed_header_third_row = (have_third_row.length > 0) ? have_third_row.clone() : false;
        var no_fixed_position = $('section.no_fixed_position').length;
        // appear position
        var header_height = 0;
        if(no_fixed_position >= 1){
            header_height = 0;
        }else {
             header_height = $('section#header_wrapper').height();
        }
        var left_position = table_with_fixed_header.offset().left;

        var fake_table = $("<table/>",{
            "id" : 'fakeTableHeader',
            "css" : {
                "position" : "fixed",
                "display"  : "none",
                "top"      : header_height,
                "left"     : left_position
            }
        }).appendTo("section.have_table_with_fixed_header");
        fake_table.append(fixed_header);

        // For each extra row, append it to the fake table element
        if (have_second_row.length > 0) {
            fake_table.append(fixed_header_second_row);
        }
        if (have_third_row.length > 0) {
            fake_table.append(fixed_header_third_row);
        }

        document.fake_table = fake_table;
    }

    // When an iput has an error, and when that input's value was changed, remove the error class so that the style change
    $('.input_wrapper.error > input, .input_wrapper.error > select, .input_wrapper.error.radioes input, .input_wrapper.error > label.file-btn > input').change(function() {
        $(this).parents('.input_wrapper.error').removeClass('error');
    });

    // Fixed table header
    var table_with_fixed_header = $('table.table_with_fixed_header');
    if (table_with_fixed_header.length) {

        // Identify the table with fixed header and clone the header.
        document.caeruChangeFixedHeader();
        var no_fixed_position = $('section.no_fixed_position').length;

        var header_height = 0;
        if(no_fixed_position >= 1){
            header_height = 0;
        }else {
             header_height = $('section#header_wrapper').height();
        }

        // handle scroll event
        $(window).bind("scroll", function() {
            var offset = $(this).scrollTop();
            //Because Nui Noi changed the container section element ('.default_table') to position relative so we now we have to get the position of the container to calculate.
            // var appear_offset = table_with_fixed_header.find('tr.fixed_header').position().top - header_height;
            // UPDATE: we have to put it here, so that this variable can be re-calculated in real time.
            var appear_offset = $('.have_table_with_fixed_header').position().top - header_height;

            if (!$('.modal-overlay').length || $('.modal-overlay').is(':hidden')) {
                if (offset >= appear_offset) {
                    document.fake_table.show();
                }
                else if (offset < appear_offset) {
                    document.fake_table.hide();
                }
            }
        });
    }

    // prevent multiple submit mechanism
    var submitting = false;

    $('[form-single-submit]').submit( (event) => {
        if (!submitting) {
            submitting = true;
        } else {
            event.preventDefault();
        }
    })

    // prevent multiple click mechanism
    var clicked = false;

    $('[single-click]').click( (event) => {
        if (!clicked) {
            clicked = true;
        } else {
            event.preventDefault();
        }
    })

});
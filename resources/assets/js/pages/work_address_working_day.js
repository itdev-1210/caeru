import Hub from '../components/hub.js';
import Common from '../components/working_day_tools.js';
import Calendar from '../components/caeru_calendar.vue';
import WorkingInfo from '../components/work_address_working_info_component';
import WithoutPlan from '../components/without_plan_employee_component';

const hub = Hub;

let work_address_working_day = new Vue({
    mixins: [Common],
    el: 'main#attendance_detail',
    data: {
        currentDate: window.current_date,
        workingInfos: window.work_address_working_infos,
        workingDayId: window.working_day_id,
        dateUpperLimit: window.date_upper_limit,
        dayOfTheUpperLimit: window.day_of_the_upper_limit,
        withoutPlanEmployees: employees_without_ewi_but_have_timestamps,
        withoutPlannedPopupDisplay: false,

        sendingRequest: false,
        successResult:  true,
        newWorkingInfoCounter: 0,
    },

    methods: {

        // Create a new WorkAddressWorkingInformation and put it into the array (but not send request yet).
        addWorkingInfo: function() {
            let temporaryId = 'new' + this.newWorkingInfoCounter;
            let newWorkingInfo = {
                id: temporaryId,
                new: true,
                schedule_start_work_time: null,
                schedule_end_work_time: null,
                candidate_number: null,
                note: null,
                date_upper_limit: this.dateUpperLimit,
                day_of_the_upper_limit: this.dayOfTheUpperLimit,
                modified_manager_id: null,
                work_address_working_employees: [],
            }
            this.workingInfos.unshift(newWorkingInfo);
            this.newWorkingInfoCounter++;
        },
        saveWorkingInfo: function(data, info) {
            if (this.sendingRequest == false) {
                this.sendingRequest = true;

                let url = info.new ? '/work_address_working_information' : '/work_address_working_information/' + info.id;

                if (info.new) {
                    data['work_address_working_day_id'] = this.workingDayId;
                }

                axios.post($.companyCodeIncludedUrl(url), data).then((response) => {
                    document.caeru_alert('success', response.data['success']);
                    this.updateWorkingInfoData(info.id, response.data['data']);
                    this.distributeValidationErrors(info.id, null);
                    this.sendingRequest = false;
                    this.successResult = true;

                }).catch((error) => {
                    if (error.response) {
                        document.caeru_alert('error', error.response.data['error'], 2000);
                        this.distributeValidationErrors(info.id, data, error.response.data);
                    }
                    this.sendingRequest = false;
                    this.successResult = false;
                });
            }
        },
        deleteWorkingInfo: function(data) {
            let conf = confirm('本当に削除しますか？');
            if (!!conf) {
                if (data.new) {
                    this.removeWorkingInfoFromDataSet(data['id']);

                } else if (this.sendingRequest == false) {
                    this.sendingRequest = true;

                    let url = '/work_address_working_information/' + data['id'];

                    axios.delete($.companyCodeIncludedUrl(url)).then((response) => {
                        document.caeru_alert('success', response.data['success']);

                        this.removeWorkingInfoFromDataSet(data['id']);
                        this.sendingRequest = false;
                        this.successResult = true;

                    }).catch((error) => {
                        if (error.response) {
                            document.caeru_alert('error', error.response.data['error'], 2000);
                        }
                        this.sendingRequest = false;
                        this.successResult = false;
                    });
                }
            }
        },


        // Assign given data to specific WorkAddressWorkingInformation
        updateWorkingInfoData: function(id, data) {
            let index = _.findIndex(this.workingInfos, (info) => {
                return info.id == id;
            })

            if (index != -1) {
                _.forEach(data, (value, key) => {
                    this.workingInfos[index][key] = value;
                });

                if (this.workingInfos[index]['new'] && this.workingInfos[index]['new'] == true) this.workingInfos[index]['new'] = false;
            }
        },
        // Remove specific WorkAddressWorkingInformation from the data set
        removeWorkingInfoFromDataSet: function(id) {
            let index = _.findIndex(this.workingInfos, (info) => {
                return info.id == id;
            })

            if (index != -1) {
                this.workingInfos.splice(index, 1);
            }
        },
        // Assign validation errors into the WorkAddressWorkingInformation data so that they can be rendered by their components
        distributeValidationErrors: function(id, data, returnedErrors) {
            let index = _.findIndex(this.workingInfos, (info) => {
                return info.id == id;
            })
            if (index != -1) {
                this.$set(this.workingInfos[index], 'errors', returnedErrors ? returnedErrors : null);
            }
        },

        showWithoutPlanPopUp: function() {
            this.withoutPlannedPopupDisplay = true;
        },
        hideWithoutPlanPopUp: function() {
            this.withoutPlannedPopupDisplay = false;
        },
    },
    created: function() {
        this.processDatePickerOptions();
    },
    mounted: function() {
        this.repositionByHeight();
        this.$nextTick(function() {
            window.addEventListener('resize', this.repositionByHeight);
        });
    },
    components: {
        'calendar': Calendar,
        'working-info': WorkingInfo,
        'without-plan': WithoutPlan,
    }
})
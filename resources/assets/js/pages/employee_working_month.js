import WorkingMonthComponent from '../components/employee_working_month_component';

let app = new Vue({
    el: 'main#attendance_detail',
    data: {
        employeeId: window.employee_id,
        businessMonth: window.business_month,
        displayWorkPlace: false,
        monthNavigator: {
            month: null,
            year: null,
        },
        everydayCanBeConcluded: window.everyday_can_be_concluded,
        levelOneConcludedAll: window.level_one_concluded_all,
        levelTwoConcluded: window.level_two_concluded,
        levelTwoConcludedManagerName: window.level_two_concluded_manager_name,
        managerCanConcludeLevelOne: window.manager_can_conclude_level_one,
        displayToggle: window.display_toggle,
    },
    computed: {

        currentYear: function() {
            return _.toInteger(_.split(this.businessMonth, '-')[0]);
        },
        currentMonth: function() {
            return _.toInteger(_.split(this.businessMonth, '-')[1]);
        },

        yearRange: function() {
            return _.range(this.currentYear - 1, this.currentYear + 2);
        },
        // I want to do this
        monthRange: function() {
            return _.range(1, 13);
        },

        selectedYear: {
            get: function() {
                return (this.monthNavigator.year !== null) ? this.monthNavigator.year : this.currentYear;
            },
            set: function(value) {
                this.monthNavigator.year = value;
            }
        },
        selectedMonth: {
            get: function() {
                return (this.monthNavigator.month !== null) ? this.monthNavigator.month : this.currentMonth;
            },
            set: function(value) {
                this.monthNavigator.month = value;
            }
        },
    },
    methods: {
        previousMonth: function() {
            if (this.selectedMonth == 1) {
                if (this.selectedYear != this.yearRange[0]) {
                    this.selectedYear -= 1;
                    this.selectedMonth = 12;
                }
            } else {
                this.selectedMonth -= 1;
            }
        },
        nextMonth: function() {
            if (this.selectedMonth == 12) {
                if (this.selectedYear != this.yearRange[this.yearRange.length-1]) {
                    this.selectedYear += 1;
                    this.selectedMonth = 1;
                }
            } else {
                this.selectedMonth += 1;
            }
        },
        moveToAnotherMonth: function() {
            let urlParts = _.split(window.location.href, '/');
            let newBusinessMonth = this.selectedYear + '-' + this.selectedMonth;
            let lastParameter = _.last(urlParts);
            
            // If the last parameter is business month, we need to remove it before adding the new one
            if (lastParameter.indexOf('-') !== -1) urlParts.pop();

            urlParts.push(newBusinessMonth);

            window.location.href = _.join(urlParts, '/');
        },

        switchView: function() {
            this.displayWorkPlace = !this.displayWorkPlace;

            // Since these tables have fixed headers, so when we change them, we also have to change the fixed table headers.
            this.$nextTick(() => {
                // This function is extended to the document object. For more information, see mics.js
                document.caeruChangeFixedHeader();
            });
        },

        concludeLevelOneOnAllWorkingDays: function() {
            var conf = confirm('全て日のデータを締めます、よろしでしょうか？');

            if (!!conf) {
                let url = '/employee_working_month/concluded_level_one_all_days/' + this.employeeId + '/' + this.businessMonth;
                window.location.href = $.companyCodeIncludedUrl(url);
            }
        },
        unConcludeLevelOneOnAllWorkingDays: function() {
            var conf = confirm('全て日のデータを解除します、よろしでしょうか？');

            if (!!conf) {
                let url = '/employee_working_month/unconcluded_level_one_all_days/' + this.employeeId + '/' + this.businessMonth;
                window.location.href = $.companyCodeIncludedUrl(url);
            }
        },
        concludeLevelTwo: function() {
            var conf = confirm('全て日のデータを締めます、よろしでしょうか？');

            if (!!conf) {
                let url = '/conclude_level_two/' + this.employeeId + '/' + this.businessMonth;
                window.location.href = $.companyCodeIncludedUrl(url);
            }
        },
        unconcludedLevelTwo: function() {
            var conf = confirm('全て日のデータは「締めない」状態になります、よろしでしょうか？');

            if (!!conf) {
                let url = '/unconcluded_level_two/' + this.employeeId + '/' + this.businessMonth;
                window.location.href = $.companyCodeIncludedUrl(url);
            }
        },

        updateConcludeStatus: function(data) {
            this.everydayCanBeConcluded = data['everydayCanBeConcluded'];
            this.levelOneConcludedAll = data['levelOneConcludedAll'];
        },

        detailPrint: function() {
            let url = '/employee_detail_print/' + this.employeeId + '/' + this.businessMonth + '/' + (this.displayWorkPlace ? '1' : '0');
            window.location.href = $.companyCodeIncludedUrl(url);
        },

        displayToogle: function ({ type, target }) {
            this.displayToggle = parseInt(target.value)
            axios.post($.companyCodeIncludedUrl('/employee_working_month/save-session-display-schedule-time'),{
                display_toggle: this.displayToggle
            }).then(response => {
                console.log(response.data)
            }).catch(error => {
                console.log(error)
            });
        }
    },
    watch:{
        selectedYear: function() {
            this.moveToAnotherMonth();
        },
        selectedMonth: function() {
            this.moveToAnotherMonth();
        },
    },
    components: {
        workingMonth: WorkingMonthComponent,
    }
})
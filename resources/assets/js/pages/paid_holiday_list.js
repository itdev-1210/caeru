import Hub from '../components/hub.js';
import Autocomplete from '../components/caeru_autocomplete';
import Paginator from '../components/caeru_paginator';

const hub = Hub;

$(document).ready(function() {
    let paid_holiday_search = new Vue({
        el: 'section#paid_holiday_searcher',
        data: {
            conditions: window.conditions,
            employees: window.result,
            showWorkLocationName: window.show_work_location_name,
            workLocationNames: window.work_location_names,
            employeeNames: window.employee_names,
            sendingRequest: false,
            toBeUpdatedEmployeeIds: [],

            defaultSearchConditions: {
                'only_updateable_employee': true,
                'presentation_id': null,
                'employee_name': null,
                'employment_type': null,
                'departments': [],
                'work_status': 1, //config('constants.working'),
                'holiday_bonus_type': null, //config('constants.normal_bonus'),
                'joined_date_start': null,
                'joined_date_end': null,
                'start_worked_years': null,
                'start_worked_months': null,
                'end_worked_years': null,
                'end_worked_months': null,
                'holidays_update_day': null,
                'start_holidays_update_date': null,
                'end_holidays_update_date': null,
                'attendance_rate': null,
                'paid_holiday_exception': false,
            },

            // Paginator
            currentPage: window.current_page,
            perPage: window.per_page,
            total: window.total,
        },
        computed: {
            updatableEmployeesIds: function() {
                return _.map(_.filter(this.employees, employee => {
                    return employee['can_be_updated'] == true;
                }), employee => {
                    return employee['id'];
                });
            },
            checkedAllUpdatableEmployee: {
                get: function() {
                    return this.toBeUpdatedEmployeeIds.length === this.updatableEmployeesIds.length;
                },
                set: function(checked) {
                    if (!!checked) {
                        this.toBeUpdatedEmployeeIds = this.updatableEmployeesIds.slice();
                    } else {
                        this.toBeUpdatedEmployeeIds = [];
                    }
                }
            }
        },
        methods: {
            submit: function(page) {
                if (this.sendingRequest === false) {
                    this.sendingRequest = true;

                    let url = '/paid_holiday';

                    if (!!page) {
                        url += '/' + _.toInteger(page);
                    }

                    let data = {
                        'conditions': this.conditions,
                    };

                    axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                        this.employees = response.data.result;
                        this.currentPage = response.data.current_page;
                        this.perPage = response.data.per_page;
                        this.total = response.data.total;

                        this.sendingRequest = false;
                    })
                    .catch(error => {
                        if (error.response) {
                            document.caeru_alert('error', '');
                        }
                        this.currentPage = 1;

                        // Reset blocking multiple request
                        this.sendingRequest = false;
                    });
                }
            },
            search: function() {
                this.submit(1);
            },
            resetConditions: function(){
                Object.assign(this.conditions, this.defaultSearchConditions);
                this.multipleSelectBox.multipleSelect('uncheckAll');
                hub.$emit('reset-autocomplete');
            },

            // Update the paid holiday information of one employee
            updateThisEmployee: function(id) {
                if (this.sendingRequest === false) {
                    this.sendingRequest = true;

                    let url = '/update_paid_holiday/' + id;
                    axios.get($.companyCodeIncludedUrl(url)).then(response => {
                        if (!!response.data.success) {
                            window.location.reload();
                        }
                    })
                    .catch(error => {
                        if (error.response) {
                            document.caeru_alert('error', error.response.data.error);
                        }
                        this.sendingRequest = false;
                    })
                }

            },

            // Update Paid Holiday Informations for multiple employee (the checked ones)
            updateCheckedEmployees: function() {
                if (this.sendingRequest === false && this.toBeUpdatedEmployeeIds.length != 0) {
                    this.sendingRequest = true;

                    let url = '/update_paid_holiday';
                    let data = {
                        'employee_ids': this.toBeUpdatedEmployeeIds,
                    };

                    axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                        if (!!response.data.success) {
                            window.location.reload();
                        }
                    })
                    .catch(error => {
                        if (error.response) {
                            document.caeru_alert('error', error.response.data.error);
                        }
                        this.sendingRequest = false;
                    })
                }
            },

            // Update Paid Holiday Informations for multiple employee (the ones from the search result)
            updateAllEmployeesInTheSearchResult: function() {
                if (this.sendingRequest === false) {
                    this.sendingRequest = true;

                    let url = '/update_paid_holiday_all';
                    axios.get($.companyCodeIncludedUrl(url)).then(response => {
                        if (!!response.data.success) {
                            window.location.reload();
                        }
                    })
                    .catch(error => {
                        if (error.response) {
                            document.caeru_alert('error', error.response.data.error);
                        }
                        this.sendingRequest = false;
                    })
                }
            },

            // Employee Name Autocomplete
            employeeNameSelected: function(id) {
                this.conditions['employee_name'] = this.employeeNames[id].name;
            },
            employeeNameChanged: function(value) {
                this.conditions['employee_name'] = value;
            },

            // Paginator
            changePage: function(newPage) {
                this.submit(newPage);
            },

            goToEditPaidHoliday: function(id) {
                let url = '/edit_paid_holiday/' + id;
                window.location.href = $.companyCodeIncludedUrl(url);
            },

            convertToHourMinute: function(minutes, filterEmpty = false) {
                if (filterEmpty === false || minutes !== 0) {
                    return _.isNumber(minutes) ? _.padStart(_.floor(_.toInteger(minutes)/60), 2, '0') + ':' + _.padStart(_.toInteger(minutes)%60, 2, '0') : minutes;
                } else {
                    return null;
                }
            },
        },
        mounted: function() {
            // The department multiple select
            let select_box = $("select#ms");
            this.multipleSelectBox = select_box.multipleSelect({
                width: 250,
                selectAll: true,
                minimumCountSelected: 4,
            });
            select_box.change(() => {
                // console.log($(select_box).val());
                this.$set(this.conditions, 'departments', $(select_box).val());
            });
        },
        components: {
            autocomplete: Autocomplete,
            paginator: Paginator,
        },
    });
})
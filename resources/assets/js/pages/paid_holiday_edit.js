$(document).ready(function () {

    let carried_forward_time_input = $('#carried_forward_paid_hoildays_hour');
    let current_valid_value_of_carried_forward_time = carried_forward_time_input.val()
    $('#carried_forward_paid_hoildays_hour').focusout(() => {
        validateTime();
    });
    $('#paid_holiday_form').submit(() => {
        validateTime();
    });

    let validateTime = () => {
        let time_string = carried_forward_time_input.val();

        let time = moment(time_string, ['hm', 'Hm', 'H:m', 'H:m:s'], true);

        if (time.isValid()) {
            carried_forward_time_input.val(time.format('HH:mm'));
            current_valid_value_of_carried_forward_time = time.format('HH:mm');
        } else {
            carried_forward_time_input.val(current_valid_value_of_carried_forward_time);
        }
    };
})


import WorkingMonth from '../components/work_address_working_month_component';

let workAddressWorkingMonth = new Vue({
    el: 'section#attendance_detail',
    data: {
        businessMonth: window.business_month,
        monthNavigator: {
            month: null,
            year: null,
        },
    },
    computed: {
        currentYear: function() {
            return _.toInteger(_.split(this.businessMonth, '-')[0]);
        },
        currentMonth: function() {
            return _.toInteger(_.split(this.businessMonth, '-')[1]);
        },

        yearRange: function() {
            return _.range(this.currentYear - 1, this.currentYear + 2);
        },
        // I want to do this
        monthRange: function() {
            return _.range(1, 13);
        },

        selectedYear: {
            get: function() {
                return (this.monthNavigator.year !== null) ? this.monthNavigator.year : this.currentYear;
            },
            set: function(value) {
                this.monthNavigator.year = value;
            }
        },
        selectedMonth: {
            get: function() {
                return (this.monthNavigator.month !== null) ? this.monthNavigator.month : this.currentMonth;
            },
            set: function(value) {
                this.monthNavigator.month = value;
            }
        },
    },
    methods: {
        previousMonth: function() {
            if (this.selectedMonth == 1) {
                if (this.selectedYear != this.yearRange[0]) {
                    this.selectedYear -= 1;
                    this.selectedMonth = 12;
                }
            } else {
                this.selectedMonth -= 1;
            }
        },
        nextMonth: function() {
            if (this.selectedMonth == 12) {
                if (this.selectedYear != this.yearRange[this.yearRange.length-1]) {
                    this.selectedYear += 1;
                    this.selectedMonth = 1;
                }
            } else {
                this.selectedMonth += 1;
            }
        },
        moveToAnotherMonth: function() {
            let urlParts = _.split(window.location.href, '/');
            let newBusinessMonth = this.selectedYear + '-' + this.selectedMonth;

            // Remove all the parameter related to business month
            urlParts = _.take(urlParts, 6);

            urlParts.push(newBusinessMonth);

            window.location.href = _.join(urlParts, '/');
        },
    },

    watch:{
        selectedYear: function() {
            this.moveToAnotherMonth();
        },
        selectedMonth: function() {
            this.moveToAnotherMonth();
        },
    },
    components: {
        'working-month': WorkingMonth,
    }
})
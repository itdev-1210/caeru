import PreviewPrintComponent from '../components/preview_print_component';

let app = new Vue({
    el: 'section#print',
    data: {
        employeeId: window.employee_id,
        businessMonth: window.business_month,
        displayWorkPlace: window.displayWorkPlace,
        month: null,
        year: null,
    },
    methods: {
        switchView: function() {
          console.log(this.displayWorkPlace)
          let url = '/employee_detail_print/' + this.employeeId + '/' + this.businessMonth + '/' + (!this.displayWorkPlace ? '1' : '0');
          window.location.href = $.companyCodeIncludedUrl(url);
        },
        detailPrint: function() {
            window.print();
        }
    },
    mounted: function() {
      this.year = _.toInteger(_.split(this.businessMonth, '-')[0]);
      this.month = _.toInteger(_.split(this.businessMonth, '-')[1]);
    },
    watch:{
    },
    components: {
        previewPrint: PreviewPrintComponent,
    }
})
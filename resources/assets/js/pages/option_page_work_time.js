import Hub from '../components/hub.js';
import ErrorDisplay from '../components/caeru_error_display';

const hub = Hub;
var example1 = new Vue({
    el: 'section#option_item_content',
    data: function() {
        return {
            data: {
                list_work_time_default: list_work_time_default,
                name_option: '',
                message_error: null,
                sendingRequest: false,
                isAdd: true,
                start_work_time: null,
                end_work_time: null,
                break_time: null,
                night_break_time: null,
                working_hour: null,
                index: -1,
                visible: 1,
            },
            errors: {
                start_work_time: null,
                end_work_time: null,
                break_time: null,
                night_break_time: null,
                working_hour: null,
            }
        }
    },
    computed: {
        checkError: function() {
            return this.data.message_error != null ? 'error' : '';
        }
    },
    filters: {
        dropTheSecond: function(time_string) {
            let time = _.split(time_string, ':');

            return (!!time[1]) ? (time[0] + ':' + time[1]) : time_string;
        },
    },
    methods: {
        addRows: function (event) {
            if (!this.data.isAdd) return;
            var object = {};
            object.name = $.trim(this.data.name_option);
            this.data.message_error = null
            if (this.data.name_option == '') {
                this.data.message_error = "この項目を入力してください。";
                this.$refs.search.focus();
                return;
            }
            
            if (!this.checkInValid(this.data.list_work_time_default, object.name)) 
            {
                this.data.index = this.data.list_work_time_default.length;
                this.data.list_work_time_default.push(object);
                this.data.isAdd = false;
                this.data.name_option = '';
            } else {
                this.data.message_error = "同じ登録名は追加できません";
                this.$refs.search.focus();
                return;
            }
        },
        removeRows: function (listObject, key) {
            if (key == this.data.index)
            {
                Vue.delete(listObject, key);
                this.reset();
            } else {
                var conf = confirm('本当に削除しますか？');
                if (!!conf) {
                    axios.delete($.companyCodeIncludedUrl('/workAndTime/') + this.data.list_work_time_default[key].id).then(function(response) {
                        document.caeru_alert('success', response.data['success']);
                        Vue.delete(listObject, key);
                    }).catch(function(error) {
                        if (error.response) {
                            document.caeru_alert('error', '');
                        }
                    });
                }
            }
        },
        checkInValid: function (listObject, name) {
            for(var index in listObject){
                if (listObject[index].name == name) return true;
            }
            return false;
        },
        submit: function (event) {
            if (this.data.isAdd) return;
            event.preventDefault();
            var url = '/updateWorkAndTime';
            this.data.list_work_time_default[this.data.index].start_work_time = this.data.start_work_time;
            this.data.list_work_time_default[this.data.index].end_work_time = this.data.end_work_time;
            this.data.list_work_time_default[this.data.index].break_time = this.data.break_time;
            this.data.list_work_time_default[this.data.index].night_break_time = this.data.night_break_time;
            this.data.list_work_time_default[this.data.index].working_hour = this.data.working_hour;
            this.data.list_work_time_default[this.data.index].visible = this.data.visible;
            var data = {
                list_work_time_default: this.data.list_work_time_default,
            };
            axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                document.caeru_alert('success', response.data['success']);
                this.data.list_work_time_default = response.data.list_work_time_default;
                this.reset();
            }).catch(error => {
                console.log(error.response.data);
                document.caeru_alert('error');
                this.showError(null);
                this.showError(error.response.data);
                // window.location.reload();
            })
        },

        // Set WorkTime visible
        setVisible: function(key) {
            var url = '/visibleWorkAndTime';
            var data = {
                id: this.data.list_work_time_default[key].id,
                visible: this.data.list_work_time_default[key].visible,
            };
            axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                document.caeru_alert('success', response.data['success']);
            }).catch(error => {
                document.caeru_alert('error');
            })
        },

        // Calculate the working_hour
        calculateWorkTime: function() {
            var startTime = this.getValidTime(this.data.start_work_time);
            var endTime = this.getValidTime(this.data.end_work_time);

            if (!!startTime && !!endTime) {
                // If the endTime is smaller then the startTime then it's from the next day
                if (endTime.isBefore(startTime)) {
                    endTime.add(1, 'days');
                }

                var breakTime = _.toSafeInteger(this.data.break_time);
                var workingMinutes = endTime.diff(startTime, 'minutes') - breakTime;
                if (workingMinutes > 0) {
                    this.data.working_hour = _.floor(workingMinutes/60) + ':' + _.padStart(workingMinutes%60, 2, '0');
                }
            }
        },

        // Get a valid Date() object from a time string. In the case of invalid string, return false.
        getValidTime: function(string) {

            let time = moment(string, ['Hm', 'H:m', 'H:m:s'], true);

            if (time.isValid())
                return time;

            return false;
        },
        showError: function(returnedErrors) {
            if (returnedErrors != null) {
                for (var key in returnedErrors) {
                    if (key.includes('break_time'))
                        this.errors['break_time'] = returnedErrors[key][0];
                    else if (key.includes('working_hour'))
                        this.errors['working_hour'] = returnedErrors[key][0];
                    else
                        this.errors[key] = returnedErrors[key][0];
                }
            } else {
                for (var key in this.errors) {
                    this.errors[key] = null;
                }
            }
        },
        reset: function() {
            this.data.isAdd = true;
            this.data.start_work_time = null;
            this.data.end_work_time = null;
            this.data.break_time = null;
            this.data.night_break_time = null;
            this.data.working_hour = null;
            this.data.index = -1;
            this.data.visible = 1;
        },
    },
    watch: {
        // data: {
        //     handler: function(newData) {
        //         //We have to ignore the first tick of the watcher, because this tick is due to the first change when the component is rendered
        //         if (this.wait == 0) {
        //             this.wait ++;
        //         } else {
        //             hub.$emit('changed', this.modelData.id);
        //         }
        //     },
        //     deep: true
        // },

        // When the start_work_time and end_work_time are provided, anychange to these fields will provoke the function
        // to re-calculate the working_hour.
        'data.start_work_time': function(value) {
            if (!!value && !!this.data.end_work_time) this.calculateWorkTime();
        },
        'data.end_work_time': function(value) {
            if (!!value  && !!this.data.start_work_time) this.calculateWorkTime();
        },
        'data.break_time': function() {
            if (!!this.data.start_work_time && !!this.data.end_work_time) this.calculateWorkTime();
        },
    },
    created: function() {
        // Register event handler
        hub.$on('submit', this.submit);
    },
    beforeDestroy: function() {
        // Get rid of the event handler
        hub.$off('submit', this.submit);
    },
    components: {
        'error-display': ErrorDisplay,
    },
});
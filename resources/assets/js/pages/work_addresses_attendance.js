import WorkAddressAttendance from '../components/work_address_attendance_component';

$(document).ready(function() {
    let employees_attendance = new Vue({
        el: 'section#employee_attendance',
        data: {
        },
        components: {
            workAddressAttendance: WorkAddressAttendance
        },
    });
})
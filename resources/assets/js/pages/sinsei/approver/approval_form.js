import SnapshotDayComponent from '../../../components/sinsei/snapshot_day_component.vue';

var app = new Vue({
    el: 'main#request_page',
    data: {
        snapshot_days: snapshot_days,
        work_locations: work_locations,
        rest_days: rest_days,
        national_holidays: national_holidays,
        flip_color_day: flip_color_day,
        timestamp_types: timestamp_types,
        employee_id: employee_id,
    },
    computed: {
        is_snapshot_days:function () {
            return (_.size(this.snapshot_days) > 1) ? true : false;
        },
    },
    components: {
        'snapshot-day-component' : SnapshotDayComponent,
    },
    methods: {
        updateSnapshotDays: function (data_from, data_to) {
            var index_furikyu = _.findIndex(this.snapshot_days, function(o) { return o.date === data_from.date; });
            var index_furide = _.findIndex(this.snapshot_days, function(o) { return o.date === data_to.date; });
            this.snapshot_days[index_furikyu].snapshots = data_from.snapshots;
            this.snapshot_days[index_furikyu].snapshot_day_color = data_from.snapshot_day_color;
            this.snapshot_days[index_furikyu] = data_from;
            this.snapshot_days[index_furide].snapshots = data_to.snapshots;
            this.snapshot_days[index_furide].snapshot_day_color = data_to.snapshot_day_color;
            this.snapshot_days[index_furide] = data_to;
        },
    }
})
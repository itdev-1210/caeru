import Hub from '../../../components/hub.js';
import ErrorDisplay from '../../../components/caeru_error_display';

const hub = Hub;
var sinsei_maebarai_Setting = new Vue({
    el: 'section#bank_info',
    data: function() {
        return {
            data: {
                settings: maebarai_setting,
            },
            errors: {
                maebarai_salary_per_hour: null,
                maebarai_bank_code: null,
                maebarai_bank_name: null,
                maebarai_branch_code: null,
                maebarai_branch_name: null,
                maebarai_account_number: null,
                maebarai_account_name: null
            }
        }
    },
    methods: {
        submit: function(event) {
            event.preventDefault();
            this.reset();
            if (this.data.settings.maebarai_bank_name != null && this.data.settings.maebarai_bank_name != undefined)
                this.data.settings.maebarai_bank_name = this.data.settings.maebarai_bank_name.replace(/ /g,'');
            if (this.data.settings.maebarai_branch_name != null && this.data.settings.maebarai_branch_name != undefined)
                this.data.settings.maebarai_branch_name = this.data.settings.maebarai_branch_name.replace(/ /g,'');
            if (this.data.settings.maebarai_account_name != null && this.data.settings.maebarai_account_name != undefined)
                this.data.settings.maebarai_account_name = this.data.settings.maebarai_account_name.replace(/ /g,'');

            var url = '/sinsei/update_maebarai_setting';
            var data = {
                'maebarai_bank_code': this.data.settings.maebarai_bank_code, 
                'maebarai_bank_name': this.data.settings.maebarai_bank_name, 
                'maebarai_branch_code': this.data.settings.maebarai_branch_code, 
                'maebarai_branch_name': this.data.settings.maebarai_branch_name, 
                'maebarai_account_number': this.data.settings.maebarai_account_number,
                'maebarai_account_name': this.data.settings.maebarai_account_name
            };
            axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                document.caeru_alert('success', response.data['success']);
                this.data.settings = response.data.settings;
                window.location.href = $.companyCodeIncludedUrl('/sinsei/maebarai_setting');
            }).catch(error => {
                document.caeru_alert('error');
                this.showError(null);
                this.showError(error.response.data.errors);
            })
        },
        showError: function(returnedErrors) {
            if (returnedErrors != null) {
                for (var key in returnedErrors) {
                    this.errors[key] = returnedErrors[key][0];
                }
            } else {
                for (var key in this.errors) {
                    this.errors[key] = null;
                }
            }
        },
        reset: function() {
            this.errors.maebarai_bank_code = null;
            this.errors.maebarai_bank_name = null;
            this.errors.maebarai_branch_code = null;
            this.errors.maebarai_branch_name = null;
            this.errors.maebarai_account_number = null;
            this.errors.maebarai_account_name = null;
        }
    },
    created: function() {
        // Register event handler
        hub.$on('submit', this.submit);
    },
    beforeDestroy: function() {
        // Get rid of the event handler
        hub.$off('submit', this.submit);
    },
    components: {
        'error-display': ErrorDisplay,
    },
});
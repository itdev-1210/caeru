import SnapshotDayComponent from '../../../components/sinsei/snapshot_day_component.vue';

var app = new Vue({
    el: 'main#request_page',
    data: {
        snapshot_days: snapshot_days,
        work_locations: work_locations,
        company_separate_info: company_separate_info,
        rest_days: rest_days,
        national_holidays: national_holidays,
        flip_color_day: flip_color_day,
        timestamp_types: timestamp_types,
        list_date_sinsei_chu: list_date_sinsei_chu,
        sendingRequest: false,
        furide_delete_key : [
            'left_requester_note',
            'left_approver_note',
            'left_approval_time',
            'left_approver_id',
            'left_requester_modify_time',
            'left_status',

            'right_status',
            'right_timestamped_start_work_time_work_location_id',
            'right_timestamped_start_work_date',
            'right_timestamped_start_work_time',
            'right_timestamped_end_work_time_work_location_id',
            'right_timestamped_end_work_date',
            'right_timestamped_end_work_time',
            'right_work_status_id',
            'right_rest_status_id',
            'right_paid_rest_time_start',
            'right_paid_rest_time_end',
            'right_paid_rest_time_period',
            'right_switch_planned_schedule_target',
            'right_planned_work_location_id',
            'right_real_work_location_id',
            'right_planned_early_arrive_start',
            'right_real_early_arrive_start',
            'right_planned_early_arrive_end',
            'right_real_early_arrive_end',
            'right_planned_work_span_start',
            'right_real_work_span_start',
            'right_planned_work_span_end',
            'right_real_work_span_end',
            'right_planned_overtime_start',
            'right_real_overtime_start',
            'right_planned_overtime_end',
            'right_real_overtime_end',
            'right_planned_work_span',
            'right_real_work_span',
            'right_planned_break_time',
            'right_real_break_time',
            'right_planned_night_break_time',
            'right_real_night_break_time',
            'right_planned_late_time',
            'right_real_late_time',
            'right_planned_early_leave_time',
            'right_real_early_leave_time',
            'right_planned_go_out_time',
            'right_real_go_out_time',
            'right_requester_note',
            'right_approver_note',
            'right_requester_modify_time',
            'right_approval_time',
            'right_approver_id',
        ],
        furikyu_store_key : [
            'left_planned_work_location_id', 
            'employee_working_day_id', 
            'id', 
            'employee_working_information_id',
            'left_requester_note',
        ],
        convert_furikyu_to_furide_key : [
            'left_schedule_start_work_time',
            'left_schedule_end_work_time',

            'left_paid_rest_time_start',
            'left_paid_rest_time_end',

            'left_planned_early_arrive_end',
            'left_planned_early_arrive_start',
            'left_planned_work_span_end',
            'left_planned_work_span_start',
            'left_planned_overtime_end',
            'left_planned_overtime_start',
        ],
        consts: {
            statuses: {
                APPROVED: {
                    id: 1,
                    name: '承認',
                    class_name : "ico_approval",
                },
                CONSIDERING: {
                    id: 2,
                    name: '申請中',
                    class_name : "ico_request",
                },
                CURRENT: {
                    id: 3,
                    name: '現在',
                    class_name : "ico_now",
                },
                PREVIOUS: {
                    id: 4,
                    name: '申請前',
                    class_name : "ico_before_request",
                },
                DENIED: {
                    id: 5,
                    name: '否決',
                    class_name : "ico_negation",
                },
            },
            work: {
                KEKKIN    : 1,
                FURIKYUU  : 2,
                FURIDE    : 3,
                HOUDE     : 4,
                KYUUDE    : 5,
                ZANGYOU    : 6,
            },
        },
    },
    computed: {
        is_snapshot_days:function () {
            return (_.size(this.snapshot_days) > 1) ? true : false;
        },
    },
    components: {
        'snapshot-day-component' : SnapshotDayComponent,
    },
    methods: {
        transferSnapshotDay: function (date, key_snapshot_day) {
            date = moment(date, 'YYYY-M-DD').format('YYYY-MM-DD');
            this.processTranferSnapshotDay(date, key_snapshot_day);
        },
        processTranferSnapshotDay: function (date, key_snapshot_day) {
            var toTheSnapshotDay = JSON.parse(JSON.stringify(this.snapshot_days[key_snapshot_day]));
            toTheSnapshotDay.timestampes = [];

            _.forEach(toTheSnapshotDay.snapshots, (employee_working_information) => {
                employee_working_information.left_switch_planned_schedule_target = toTheSnapshotDay.date;
                if (employee_working_information.left_work_status_id != this.consts.work.ZANGYOU)
                    employee_working_information.left_work_status_id = this.consts.work.FURIDE;

                _.forEach(this.convert_furikyu_to_furide_key, (value) => {
                    employee_working_information[value] = (employee_working_information[value]) ?
                        this.transferValueDateToDate(toTheSnapshotDay.date, date, employee_working_information[value]) : null;
                });

                this.furide_delete_key.forEach(function(key_name) {
                    delete employee_working_information[key_name];
                });
                employee_working_information.left_status = this.consts.statuses.CURRENT.id;
                employee_working_information.isFurikae = true;
            });
            toTheSnapshotDay.date = moment(date, 'YYYY-M-D').format('YYYY-MM-DD');
            _.forEach(toTheSnapshotDay.snapshot_day_color, (snapshot_color) => {
                snapshot_color.splice(0,snapshot_color.length)
            });

            _.forEach(this.snapshot_days[key_snapshot_day].snapshots, (employee_working_information) => {
                _.forEach(employee_working_information, (value, key) => {
                    if (_.findIndex(this.furikyu_store_key, function(o) { return o === key; }) >= 0) return;
                    employee_working_information[key] = null;
                });
                employee_working_information.left_work_status_id = this.consts.work.FURIKYUU;
                employee_working_information.left_switch_planned_schedule_target = date;
                employee_working_information.left_status = this.consts.statuses.CURRENT.id;
            });
            var fromTheSnapshotDay = JSON.parse(JSON.stringify(this.snapshot_days[key_snapshot_day]));

            this.snapshot_days.splice(key_snapshot_day+1, 0, toTheSnapshotDay);
            this.snapshot_days.splice(key_snapshot_day, 1, fromTheSnapshotDay);
        },
        removeTransferSnapshotDay: function (oldValue, switch_schedule_target, key_working_day) {
            switch_schedule_target = moment(switch_schedule_target, 'YYYY-M-DD').format('YYYY-MM-DD');

            var index_furikyu = (oldValue== this.consts.work.FURIKYUU) ?
                key_working_day : _.findIndex(this.snapshot_days, function(o) { return o.date === switch_schedule_target; });

            var index_furide = (oldValue== this.consts.work.FURIDE) ?
                key_working_day : _.findIndex(this.snapshot_days, function(o) { return o.date === switch_schedule_target; });

            var back_up = (index_furikyu <= index_furide) ?
                JSON.parse(JSON.stringify(this.snapshot_days[index_furikyu])) : JSON.parse(JSON.stringify(this.snapshot_days[index_furide]));

            back_up.snapshots = JSON.parse(JSON.stringify(back_up.previousSinseiCard));
            back_up.snapshot_day_color = JSON.parse(JSON.stringify(back_up.previousSinseiCardColor));

            _.forEach(back_up.snapshots, (snapshot) => {
                snapshot.edit_text = true;
            });

            //Merge current object to object's back up
            back_up = this.mergeCurrentObjectToBackUpObject(this.snapshot_days[index_furide], back_up);

            //Remove both object furikyu and furide, re-store back up
            this.snapshot_days.splice(index_furikyu, 1, back_up);
            if (index_furide > -1)
                this.snapshot_days.splice(index_furide, 1);
        },
        mergeCurrentObjectToBackUpObject: function (current_object, back_up_object) {
            //code
            return back_up_object;
        },
        saveFurikaeCase: function (work_status_id, switch_schedule_target, key_working_day) {
            if (!this.sendingRequest) {
                this.sendingRequest = true;

                var url = '/sinsei/request_forms';
                switch_schedule_target = moment(switch_schedule_target, 'YYYY-M-DD').format('YYYY-MM-DD');

                var index_furide = (work_status_id == this.consts.work.FURIDE) ?
                    key_working_day : _.findIndex(this.snapshot_days, function(o) { return o.date === switch_schedule_target; });

                var index_furikyu = (work_status_id == this.consts.work.FURIKYUU) ?
                    key_working_day :_.findIndex(this.snapshot_days, function(o) { return o.date === switch_schedule_target; });

                // Check limit 24 hour
                let check_error_index = _.findIndex(this.$refs.snapshot_days[index_furide].$refs.snapshots, (snapshot) => {
                    return !_.isNil(snapshot.computedLeftPlannedWorkingHour) && snapshot.computedLeftPlannedWorkingHour >= 1440
                });
                if (check_error_index != -1) {
                    document.caeru_alert('error', '１つの勤務の総労働時間は24時間未満で登録してください');
                    this.sendingRequest = false;
                    return false;
                }

                axios.post($.companyCodeIncludedUrl(url), {
                        snapshotFrom : this.snapshot_days[index_furikyu].snapshots,
                        snapshotTo : this.snapshot_days[index_furide].snapshots,
                        date_to: this.snapshot_days[index_furide].date,
                        date_from: this.snapshot_days[index_furikyu].date,
                }).then(response => {
                    //document.caeru_alert('success', response.data['success']);
                    this.snapshot_days[index_furide].snapshots = response.data['snapshotTo'].snapshots;
                    this.snapshot_days[index_furikyu].snapshots = response.data['snapshotFrom'].snapshots;
                    this.snapshot_days[index_furide] = response.data['snapshotTo'];
                    this.snapshot_days[index_furikyu] = response.data['snapshotFrom'];
                    this.updateListSinsei(response.data['list_date_sinsei_chu']);
                    this.sendingRequest = false;
                    window.location.reload();
                }).catch(error => {
                    if (error.response)
                        document.caeru_alert('error', '');
                    this.sendingRequest = false;
                })
            }
        },
        updateListSinsei: function (list) {
            this.list_date_sinsei_chu = list;
        },
        transferValueDateToDate: function (date_from, date_to, value_from) {
            if (_.isNil(value_from)) return value_from;
            var diffValueWithDay = (moment(date_to, 'YYYY-MM-DD').diff(moment(date_from, 'YYYY-MM-DD'), 'days'));
            return moment(value_from, 'YYYY-MM-DD HH:mm:ss').add(diffValueWithDay, 'days').format('YYYY-MM-DD HH:mm:ss');
        },
        updateSnapshotDay: function (snapshot_day, key) {
            this.snapshot_days.splice(key, 1, snapshot_day);

            this.$refs.snapshot_days[key].canRegisterAbsorb =
                !_.some(this.snapshot_days[key].snapshots, {'temporary': 1, 'left_status': this.consts.statuses.CONSIDERING.id, 'left_soon_to_be_absorb': 1})
        },
    }
})
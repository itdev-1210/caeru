let app = new Vue({
    el: 'main#attendance_detail',
    data: {
        businessMonth: window.business_month,
        monthNavigator: {
            month: null,
            year: null,
        },
        dates: [],
        displayToggle: window.toggle_display,
    },
    computed: {

        currentYear: function() {
            return _.toInteger(_.split(this.businessMonth, '-')[0]);
        },
        currentMonth: function() {
            return _.toInteger(_.split(this.businessMonth, '-')[1]);
        },

        yearRange: function() {
            return _.range(this.currentYear - 1, this.currentYear + 2);
        },
        // I want to do this
        monthRange: function() {
            return _.range(1, 13);
        },

        selectedYear: {
            get: function() {
                return (this.monthNavigator.year !== null) ? this.monthNavigator.year : this.currentYear;
            },
            set: function(value) {
                this.monthNavigator.year = value;
            }
        },
        selectedMonth: {
            get: function() {
                return (this.monthNavigator.month !== null) ? this.monthNavigator.month : this.currentMonth;
            },
            set: function(value) {
                this.monthNavigator.month = value;
            }
        },
    },
    methods: {
        previousMonth: function() {
            if (this.selectedMonth == 1) {
                if (this.selectedYear != this.yearRange[0]) {
                    this.selectedYear -= 1;
                    this.selectedMonth = 12;
                }
            } else {
                this.selectedMonth -= 1;
            }
        },
        nextMonth: function() {
            if (this.selectedMonth == 12) {
                if (this.selectedYear != this.yearRange[this.yearRange.length-1]) {
                    this.selectedYear += 1;
                    this.selectedMonth = 1;
                }
            } else {
                this.selectedMonth += 1;
            }
        },
        moveToAnotherMonth: function() {
            let urlParts = _.split(window.location.href, '/');
            let newBusinessMonth = this.selectedYear + '-' + this.selectedMonth;
            let lastParameter = _.last(urlParts);

            // If the last parameter is business month, we need to remove it before adding the new one
            if (lastParameter.indexOf('-') !== -1 || lastParameter.indexOf('reset') !== -1) urlParts.pop();

            urlParts.push(newBusinessMonth);

            window.location.href = _.join(urlParts, '/');
        },
        callMutilpleDate: function () {
            if (this.dates.length != 0)
                window.location.href = $.companyCodeIncludedUrl('/sinsei/request_form/') + _.replace(_.toString(this.dates), new RegExp(",","g"), '|');
        },
        goToday: function () {
            window.location.href = $.companyCodeIncludedUrl('/sinsei/request_form/') + moment().format('YYYY-MM-DD');
        },
        displayToogle: function ({ type, target}) {
            this.displayToggle = target.value
            axios.post($.companyCodeIncludedUrl('/sinsei/save-session-display'),{
                display_toggle_sensei: this.displayToggle
            }).then(response => {
                console.log(response.data)
            }).catch(error => {
                console.log(error)
            });
        }
    },
    watch:{
        selectedYear: function() {
            this.moveToAnotherMonth();
        },
        selectedMonth: function() {
            this.moveToAnotherMonth();
        },
    }
})
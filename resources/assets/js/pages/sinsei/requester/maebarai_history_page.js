let payment_history = new Vue({
    el: 'section#maebarai_history',
    data: {
        currentYear: window.conditions['year'],
        currentMonth: window.conditions['month'],
        conditions: window.conditions,
        sendingRequest: false,

        datas: window.datas,
    },
    computed: {
        yearRange: function() {
            return _.range(this.currentYear - 5, this.currentYear + 6);
        },
    },
    methods: {
        yearChanged: function(){
            this.submit();
        },
        monthChanged: function(){
            this.submit();
        },
        nextMonth: function() {
            this.currentMonth++;
            if(this.currentMonth > 12) {
                this.currentYear++;
                this.currentMonth = 1;
            }

            // The search result from  the same condition in a work location will be the same regardless of the business month.
            // So it's safe to assume the pagination's result will be the same => the number of pages will be the same.
            // So it's safe to use the current page.
            this.submit();
        },
        preMonth: function() {
            this.currentMonth--;
            if(this.currentMonth < 1) {
                this.currentYear--;
                this.currentMonth = 12;
            }

            // The same reason above
            this.submit();
        },

        // Send request
        submit: function() {
            if (this.sendingRequest === false) {
                this.sendingRequest = true;

                let url = '/sinsei/maebarai_history_page';

                // Re-assign date data
                this.conditions['month'] = this.currentMonth;
                this.conditions['year'] = this.currentYear;

                let data = {
                    'conditions': this.conditions,
                };

                axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                    this.datas = response.data.datas;
                    this.sendingRequest = false;
                })
                .catch(error => {
                    if (error.response) {
                        document.caeru_alert('error', '');
                    }

                    // Reset blocking multiple request
                    this.sendingRequest = false;
                });
            }
        },

    },
});

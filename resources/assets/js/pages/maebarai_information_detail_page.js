let payment_information_detail = new Vue({
    el: 'section#payment_information_detail',
    data: {
        currentYear: window.conditions['year'],
        currentMonth: window.conditions['month'],
        conditions: window.conditions,
        sendingRequest: false,

        datas: window.datas,
        id: window.id,
        can_go_previous: window.can_go_previous,
        can_go_next: window.can_go_next,
    },
    computed: {
        yearRange: function() {
            return _.range(this.currentYear - 5, this.currentYear + 6);
        },
    },
    methods: {
        yearChanged: function(){
            this.submit();
        },
        monthChanged: function(){
            this.submit();
        },
        nextMonth: function() {
            this.currentMonth++;
            if(this.currentMonth > 12) {
                this.currentYear++;
                this.currentMonth = 1;
            }

            // The search result from  the same condition in a work location will be the same regardless of the business month.
            // So it's safe to assume the pagination's result will be the same => the number of pages will be the same.
            // So it's safe to use the current page.
            this.submit();
        },
        preMonth: function() {
            this.currentMonth--;
            if(this.currentMonth < 1) {
                this.currentYear--;
                this.currentMonth = 12;
            }

            // The same reason above
            this.submit();
        },
        prevEmployee: function() {
            this.findEmployee(1);
        },

        nextEmployee: function() {
            this.findEmployee(2);
        },

        findEmployee: function(step) {
            if (this.sendingRequest === false) {
                this.sendingRequest = true;

                let url = '/maebarai_information_detail_find/' + this.id;
                // Re-assign date data
                this.conditions['month'] = this.currentMonth;
                this.conditions['year'] = this.currentYear;

                let data={
                    'step': step,
                    'conditions': this.conditions,
                }

                axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                    let url = '/maebarai_information_detail/' + response.data.id;
                    window.location.href = $.companyCodeIncludedUrl(url);
                })
                .catch(error => {
                    if (error.response) {
                        document.caeru_alert('error', '');
                    }

                    // Reset blocking multiple request
                    this.sendingRequest = false;
                });
            }
        },

        // Send request
        submit: function() {
            if (this.sendingRequest === false) {
                this.sendingRequest = true;

                let url = '/maebarai_information_detail/' + this.id;

                // Re-assign date data
                this.conditions['month'] = this.currentMonth;
                this.conditions['year'] = this.currentYear;

                let data = {
                    'conditions': this.conditions,
                };

                axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                    this.datas = response.data.datas;
                    this.id = response.data.id;
                    this.sendingRequest = false;
                })
                .catch(error => {
                    if (error.response) {
                        document.caeru_alert('error', '');
                    }

                    // Reset blocking multiple request
                    this.sendingRequest = false;
                });
            }
        },

    },
});

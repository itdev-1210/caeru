$(document).ready(function() {
    let attendance_file_name = $("#attendance_file_name");
    $('#attendance_file').change(function() {
        attendance_file_name.text(this.files[0].name);
    })

    let summary_file_name = $("#summary_file_name");
    $('#summary_file').change(function() {
        summary_file_name.text(this.files[0].name);
    })

    // Error message frame in the case of when the user didnt input the company code, and there is no error message template at that time
    let error_message_head = "<div class='error_wrapper'><span class='tool_error'>";
    let error_message_tail = "</span></div>";

    let send_request = false;

    $('#downloadCurrentAttendance').click(function(event) {
        event.preventDefault();
        let code_input = $('#attendance_company_code');
        let company_code = code_input.val();

        if (company_code == '') {
            showErrorMessage(code_input);
        } else {
            let url = '/download_attendance_file/' + company_code;
            downloadProcessFile(url);
        }
    })

    $('#downloadCurrentAttendance').click(function(event) {
        event.preventDefault();
        let code_input = $('#attendance_company_code');
        let company_code = code_input.val();

        if (company_code == '') {
            showErrorMessage(code_input);
        } else {
            let url = '/download_attendance_file/' + company_code;
            downloadProcessFile(url);
        }
    })

    $('#downloadCurrentSummary').click(function(event) {
        event.preventDefault();
        let code_input = $('#summary_company_code');
        let company_code = code_input.val();

        if (company_code == '') {
            showErrorMessage(code_input);
        } else {
            let url = '/download_summary_file/' + company_code;
            downloadProcessFile(url);
        }
    })

    /**
     * Show error message. Receive a DOM element as parameter. It should be the input element of the company code
     *
     * @param {DOM} element
     */
    function showErrorMessage(element)
    {
        let parent = element.parent();
        let previous_message = parent.find('.tool_error');
        if (previous_message.length == 0) {
            parent.append(error_message_head + "会社コードを入れてください" + error_message_tail);
        } else {
            previous_message.text('会社コードを入れてください');
        }
        parent.addClass('error');
    }

    /**
     * Download the process file. Receive an url as parameter. It should be the url to download.
     *
     * @param {string} url
     */
    function downloadProcessFile(url)
    {
        if (!send_request) {
            send_request = true;

            axios.get($.companyCodeIncludedUrl(url))
            .then(response => {
                let disposition = response.headers['content-disposition'];
                let matches = /"([^"]*)"/.exec(disposition);
                let filename = (matches != null && matches[1] ? matches[1] : 'file.php');
                let blob = new Blob([response.data], { type: 'text/x-php' });
                let link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = filename;

                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);

                send_request = false;
            })
            .catch(error => {
                if (error.response) {
                    document.caeru_alert('error', error.response.data.error);
                }

                send_request = false;
            })
        }
    }
});
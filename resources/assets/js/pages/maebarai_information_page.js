import Paginator from '../components/caeru_paginator';
import Autocomplete from '../components/caeru_autocomplete';
import Hub from '../components/hub.js';

const hub = Hub;

let payment_information = new Vue({
    el: 'section#payment_information',
    data: {
        currentYear: window.conditions['year'],
        currentMonth: window.conditions['month'],
        conditions: window.conditions,
        employeeNames : window.employee_names,
        sendingRequest: false,
        defaultConditions: {
            'presentation_id': null,
            'name': null,
        },

        // Paginator
        currentPage: window.current_page,
        perPage: window.per_page,
        total: window.total,
        datas: window.datas,

    },
    computed: {
        yearRange: function() {
            return _.range(this.currentYear - 5, this.currentYear + 6);
        },
    },
    methods: {
        yearChanged: function(){
            this.submit(this.currentPage);
        },
        monthChanged: function(){
            this.submit(this.currentPage);
        },
        nextMonth: function() {
            this.currentMonth++;
            if(this.currentMonth > 12) {
                this.currentYear++;
                this.currentMonth = 1;
            }

            // The search result from  the same condition in a work location will be the same regardless of the business month.
            // So it's safe to assume the pagination's result will be the same => the number of pages will be the same.
            // So it's safe to use the current page.
            this.submit(this.currentPage);
        },
        preMonth: function() {
            this.currentMonth--;
            if(this.currentMonth < 1) {
                this.currentYear--;
                this.currentMonth = 12;
            }

            // The same reason above
            this.submit(this.currentPage);
        },

        resetConditions: function(){
            Object.assign(this.conditions, this.defaultConditions);
            hub.$emit('reset-autocomplete');
        },

        // Paginator
        changePage: function(newPage) {
            // this.submit(newPage);
            let url = '/maebarai_information/0/' + newPage;
            window.location.href = $.companyCodeIncludedUrl(url);
        },

        // Send request
        submit: function(page) {
            if (this.sendingRequest === false) {
                this.sendingRequest = true;

                let url = '/maebarai_information/search';

                if (!!page) {
                    url += '/' + _.toInteger(page);
                }

                // Re-assign date data
                this.conditions['month'] = this.currentMonth;
                this.conditions['year'] = this.currentYear;

                let data = {
                    'conditions': this.conditions,
                };

                axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                    this.datas = response.data.datas;
                    this.currentPage = response.data.current_page;
                    this.perPage = response.data.per_page;
                    this.total = response.data.total;
                    this.sendingRequest = false;
                })
                .catch(error => {
                    if (error.response) {
                        document.caeru_alert('error', '');
                    }
                    this.currentPage = 1;

                    // Reset blocking multiple request
                    this.sendingRequest = false;
                });
            }
        },
        detail: function(id) {
            let url = '/maebarai_information_detail/' + id;
            window.location.href = $.companyCodeIncludedUrl(url);
        },
        employeeNameSelected: function(id) {
            this.conditions['name'] = this.employeeNames[id].name;
        },
        employeeNameChanged: function(value) {
            this.conditions['name'] = value;
        },

    },
    components: {
        autocomplete: Autocomplete,
        paginator: Paginator,
    },
});

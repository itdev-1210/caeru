import Hub from '../components/hub.js';
import WorkingInfo from '../components/employee_working_info_component';
import Autocomplete from '../components/caeru_autocomplete';
import ErrorDisplay from '../components/caeru_error_display';
import Calendar from '../components/caeru_calendar.vue';
import SnapshotDayComponent from '../components/sinsei/snapshot_day_component.vue';
import Common from '../components/working_day_tools.js';
import PlacePicker from '../components/place_picker';

const hub = Hub;

var employee_working_day = new Vue({
    mixins: [Common],
    el: 'main#attendance_detail',
    data: {
        workingDayInstanceId: window.working_day_id,
        currentDate: window.current_date,
        canMakeYesterdayTimestamp: window.can_make_yesterday_timestamp,
        currentEmployee: window.current_employee,
        workingInfos: window.working_infos,
        scheduleTransferData : window.schedule_transfer_data,
        // alertSettingData : window.alert_setting_data,
        workLocations: window.work_locations,
        allWorkStatuses: window.all_work_statuses,
        allRestStatuses: window.all_rest_statuses,
        timezone: window.timezone,
        canChange: window.can_change_data,
        concluded: window.concluded,
        havingConsideringRequest: window.having_considering_request,

        // Timestamp section
        timestamps: window.working_timestamps,
        timestampTypes: window.timestamp_types,
        timestampPlaces: window.timestamp_places,
        autocompleteWorkTimeData: window.autocomplete_work_time_data,
        displayAutocompleteWorkTime: window.display_autocomplete_work_time,
        newTimestamp: {
            enable: true,
            processed_date_value: window.current_date,
            processed_time_value: null,
            // Default type is: 2 (出勤)
            timestamped_type: 2,
            work_location_id: null,
            work_address_id: null,
        },
        timestampFormErrors: {
            enable: null,
            processed_date_value: null,
            processed_time_value: null,
            timestamped_type: null,
            work_location_id: null,
            work_address_id: null,
        },
        showTimestampForm: false,
        initialFirstWorkingInfosPlannedWorkLocationId: window.working_infos.length > 0 ? window.working_infos[0].planned_work_location_id : null,
        initialFirstWorkingInfosPlannedWorkAddressId: window.working_infos.length > 0 ? window.working_infos[0].planned_work_address_id : null,

        // General purpose

        sendingRequest: false,
        newWorkingInfoCount: 0,
        // Place picker
        placePickerSwitches : null,

        ////////////////////////////////////
        /////// Approver Form //////////////
        ////////////////////////////////////
        showApproverForm: false,

        snapshots: window.snapshot_days,
        employeeId: window.employee_id,
    },
    computed: {
        notHaveWorkAddressKinmu: function() {
            let result = _.find(this.workingInfos, (workingInfo) => {
                return !!workingInfo['planned_work_address_id'] || !!workingInfo['real_work_address_id'];
            });
            return (result === undefined) ? true : false;
        },

        isSnapshotDays:function () {
            return (_.size(this.snapshots) > 1) ? true : false;
        },

        // This gives the planned_work_location_id of the current first EmployeeWorkingInformation.
        workLocationOfTheFirstWorkingInfo: {
            get: function() {
                return this.initialFirstWorkingInfosPlannedWorkLocationId;
            },
            set: function(newId) {
                this.initialFirstWorkingInfosPlannedWorkLocationId = newId;
            },
        },
        // This gives the planned_work_address_id of the current first EmployeeWorkingInformation.
        workAddressOfTheFirstWorkingInfo: {
            get: function() {
                return this.initialFirstWorkingInfosPlannedWorkAddressId;
            },
            set: function(newId) {
                this.initialFirstWorkingInfosPlannedWorkAddressId = newId;
            },
        },

        // This gives the index of the planned_work_location_id and planned_work_address_id of the current first EmployeeWorkingInfo in the list of timestampPlaces
        indexOfThePlaceOfTheFirstWorkingInfo: function() {
            if (!!this.workLocationOfTheFirstWorkingInfo) {
                return _.findIndex(window.timestamp_places, (place) => {
                    return place.work_location_id == this.workLocationOfTheFirstWorkingInfo && place.work_address_id == this.workAddressOfTheFirstWorkingInfo;
                });
            } else {
                return null;
            }
        },
        // Why did I add this, even though the havingConsideringRequest is already existing ?!
        // haveConsideringSnapshot: function() {
        //     if (this.snapshots) {
        //         let hasIt = _.findIndex(this.snapshots[0].snapshots, (snapshot) => {
        //             return snapshot.left_status == 2; // EmployeeWorkingInformationSnapshot::CONSIDERING
        //         })
        //         return hasIt !== -1 ? true : false;
        //     } else {
        //         return false;
        //     }
        // },
        haveOnlyTemporaryWorkingInfos: function() {
            return _.every(this.workingInfos, (workingInfo) => {
                return workingInfo.temporary == 1;
            });
        },
        haveOnlyHoudeOrKyudeWorkingInfos: function() {
            return _.every(this.workingInfos, (workingInfo) => {
                return workingInfo.planned_work_status_id == 4 ||        // WorkStatus::HOUDE
                        workingInfo.planned_work_status_id == 5;         // WorkStatus::KYUDE
            })
        },
    },
    methods: {
        // Determine if the given EWI is a temporary EWI and also contains a work address.
        isTemporaryEmployeeWorkingInformationWithWorkAddress: function(info) {
            return info['temporary'] == true && !!info['planned_work_address_id'];
        },

        // validate the time format type
        validateTimeFormat: function(string) {
            if (string.indexOf(':') === -1) {
                let temp = moment(string, 'Hmm', true);
                if (temp.isValid())
                    return temp.format('H:mm');
            } else {
                let temp = moment(string, 'H:mm', true);
                if (temp.isValid())
                    return temp.format('H:mm');
            }
            return false;
        },

        // create an empty WorkingInformation
        createNewWorkingInfo: function() {
            let newUniqueId = 'new_info_' + this.newWorkingInfoCount;
            let newData = { id : newUniqueId, new: true };
            this.workingInfos.push(newData);
            this.newWorkingInfoCount++;
        },

        // Remove WorkingInformation
        removeWorkingInfo: function(id) {
            this.workingInfos.splice(_.findIndex(this.workingInfos, (info) => {return info.id === id}), 1)
        },

        // After a WorkingInformation was saved successfully
        workingInfoSaved: function(oldId, data, newOrNot, newScheduleTransferData, newAlertData) {

            this.refreshWorkingInformations();

            // Re-assign all the neccessary data
            let info = _.find(this.workingInfos, (instance) => {return instance.id === oldId});
            if (newOrNot === true) info.new = false;
            _.forEach(data, (value, key) => {
                info[key] = value;
            });

            let transferInfo = _.find(this.scheduleTransferData, (instance) => {return instance['working_info_id'] === data.id});
            if (transferInfo !== undefined) {
                _.forEach(newScheduleTransferData, (value, key) => {
                    transferInfo[key] = value;
                })
            } else {
                this.scheduleTransferData.push(newScheduleTransferData);
            }

            // let alertData = _.find(this.alertSettingData, (instance) => {return instance['working_info_id'] === data.id});
            // if (alertData !== undefined) {
            //     _.forEach(newAlertData, (value, key) => {
            //         alertData[key] = value;
            //     })
            // } else {
            //     this.alertSettingData.push(newAlertData);
            // }
        },

        // Extract the schedule_transfer_data by working_info_id
        extractScheduleTransferData: function(workingInfoId) {

            let extractedData = _.find(this.scheduleTransferData, (data) => {
                return data['working_info_id'] === workingInfoId;
            });
            return (extractedData !== undefined) ? extractedData : null;
        },
        // Extract the alert setting data by working_info_id
        // extractAlertSettingData: function(workingInfoId) {

        //     let extractedData = _.find(this.alertSettingData, (data) => {
        //         return data['working_info_id'] === workingInfoId;
        //     });
        //     return (extractedData !== undefined) ? extractedData : null;
        // },

        // Transfer working information
        scheduleTransfer: function(day) {
            if (!this.sendingRequest) {
                this.sendingRequest = true;

                let url = '/schedule_transfer';
                let data = {
                    employee_id:    this.currentEmployee.id,
                    from_date:      this.currentDate,
                    to_date:        day,
                }

                axios.post($.companyCodeIncludedUrl(url), data).then(response => {

                    this.sendingRequest = false;
                    window.location.reload();

                }).catch(error => {
                    if (error.response) {
                        document.caeru_alert('error', '');
                        this.sendingRequest = false;
                    }
                });
            }
        },

        // show the newTimestamp form
        createTimestamp: function() {
            this.newTimestamp.work_location_id = this.workLocationOfTheFirstWorkingInfo;
            this.newTimestamp.work_address_id = this.workAddressOfTheFirstWorkingInfo;
            this.showTimestampForm = !this.showTimestampForm;
        },

        // Get the next day date_string
        tomorrow: function() {
            return moment(this.currentDate, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD');
        },
        // Get the day before date_string
        yesterday: function() {
            return moment(this.currentDate, 'YYYY-MM-DD').subtract(1, 'days').format('YYYY-MM-DD');
        },

        // Validate the input and set value
        insertDateToNewTimestamp: function() {
            let new_value = $('#newTimestamp').val();
            let valid_value = this.validateTimeFormat(new_value);
            if (valid_value !== false && valid_value !== this.newTimestamp.processed_time_value)
                this.newTimestamp.processed_time_value = valid_value;
        },

        placeSelected: function(index) {
            if (index !== null) {
                this.newTimestamp.work_location_id = this.timestampPlaces[index]['work_location_id'];
                this.newTimestamp.work_address_id = this.timestampPlaces[index]['work_address_id'];
            }
        },

        // Show the validation error for the newTimestamp form
        showError: function(returnedErrors) {
            if (returnedErrors != null) {
                for (var key in returnedErrors) {
                    this.timestampFormErrors[key] = returnedErrors[key][0];
                }
            } else {
                for (var key in this.timestampFormErrors) {
                    this.timestampFormErrors[key] = null;
                }
            }
        },

        // send the request to create a new WorkingTimestamp
        sendNewTimestamp: function() {
            if (!this.sendingRequest) {
                this.sendingRequest = true;

                let url = '/working_timestamp/' + this.workingDayInstanceId;

                axios.post($.companyCodeIncludedUrl(url), this.newTimestamp).then(response => {

                    document.caeru_alert('success', response.data['success']);
                    this.showError(null);
                    this.timestamps = response.data['timestamps'];
                    this.showTimestampForm = false;
                    this.resetTheNewTimestamp();
                    this.sendingRequest = false;
                    this.refreshWorkingInformations();

                }).catch(error => {
                    if (error.response) {

                        document.caeru_alert('error', '');
                        this.showError(null);
                        this.showError(error.response.data);
                        this.sendingRequest = false;
                    }
                })
            }
        },

        // toggle the status of a WorkingTimestamp
        toggleStatusTimestamp: function(key) {
            if (!this.sendingRequest) {
                this.sendingRequest = true;

                let url = '/working_timestamp/' + this.timestamps[key]['id']
                let data = {
                    'enable': this.timestamps[key]['enable'],
                }

                axios.patch($.companyCodeIncludedUrl(url), data).then(response => {

                    document.caeru_alert('success', response.data['success']);
                    this.sendingRequest = false;
                    this.timestamps = response.data['timestamps'];
                    this.refreshWorkingInformations();

                }).catch(error => {
                    if (error.response) {

                        document.caeru_alert('error', '');
                        this.timestamps[key]['id'] = !this.timestamps[key]['id'];
                        this.sendingRequest = false;
                    }
                })
            }
        },

        // After the user change something in the working timestamp section, refresh the working info section
        refreshWorkingInformations: function() {
            if (!this.sendingRequest) {
                this.sendingRequest = true;

                let url = '/employee_working_day_by_id/' + this.workingDayInstanceId;

                axios.get($.companyCodeIncludedUrl(url)).then(response => {

                    this.workingInfos = response.data['working_infos'];
                    this.scheduleTransferData = response.data['schedule_transfer_data'];
                    this.sendingRequest = false;
                }).catch(error => {
                    if (error.response) {

                        document.caeru_alert('error', '');
                        this.sendingRequest = false;
                    }
                })
            }
        },

        resetTheNewTimestamp: function() {
            this.newTimestamp = {
                enable: true,
                processed_date_value: window.current_date,
                processed_time_value: null,
                // Default type is: 2 (出勤)
                timestamped_type: 2,
                work_location_id: null,
                work_address_id: null,
            };
        },


        // Conclude Functions
        concludeLevelOne: function() {
            var conf = confirm('この勤務を本当に締めますか？');

            if (!!conf) {
                let url = '/conclude_level_one/' + this.workingDayInstanceId;
                window.location.href = $.companyCodeIncludedUrl(url);
            }
        },

        unconcludedLevelOne: function() {
            var conf = confirm('この勤務の締めを解除しますか？');

            if (!!conf) {
                let url = '/unconcluded_level_one/' + this.workingDayInstanceId;
                window.location.href = $.companyCodeIncludedUrl(url);
            }
        },

        // For the place picker
        openPlacePickerOf: function(index) {
            this.$set(this.placePickerSwitches, index, true);
        },
        closePlacePickerOf: function(index) {
            this.$set(this.placePickerSwitches, index, false);
        },

        //////////////////////////////
        /// Approver Form function ///
        //////////////////////////////
        toggleApproverForm: function() {
            this.showApproverForm = !this.showApproverForm;
            if (this.showApproverForm === true) {
                this.$nextTick(function() {
                    this.repositionByHeight();
                });
            }
        },
        updateSnapshotDays: function (data_from, data_to) {
            var index_furikyu = _.findIndex(this.snapshots, function(o) { return o.date === data_from.date; });
            var index_furide = _.findIndex(this.snapshots, function(o) { return o.date === data_to.date; });
            this.snapshots[index_furikyu].snapshots = data_from.snapshots;
            this.snapshots[index_furikyu].snapshot_day_color = data_from.snapshot_day_color;
            this.snapshots[index_furikyu] = data_from;
            this.snapshots[index_furide].snapshots = data_to.snapshots;
            this.snapshots[index_furide].snapshot_day_color = data_to.snapshot_day_color;
            this.snapshots[index_furide] = data_to;
        },
        refreshPage: function() {
            window.location.reload();
        },

        firstWorkingInfosPlannedWorkLocationChanged: function(id, workAddress) {
            if (workAddress != true) {
                this.initialFirstWorkingInfosPlannedWorkLocationId = id;
            } else {
                this.initialFirstWorkingInfosPlannedWorkAddressId = id;
            }
        },
    },
    created: function() {
        this.processDatePickerOptions();

        // This is for Placepicker, it's an array of booleans use to switch the placepickers of timestamps
        this.placePickerSwitches = new Array(this.timestamps.length).fill(false);
    },
    mounted: function() {
        this.repositionByHeight();
        this.$nextTick(function() {
            window.addEventListener('resize', this.repositionByHeight);
        });
    },
    components: {
        'working-info' : WorkingInfo,
        'autocomplete' : Autocomplete,
        'error-display' : ErrorDisplay,
        'calendar': Calendar,
        'snapshot-day-component' : SnapshotDayComponent,
        'place-picker' : PlacePicker,
    }
})
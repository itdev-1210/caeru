import Hub from '../components/hub.js';
import Autocomplete from '../components/caeru_autocomplete';
import Paginator from '../components/caeru_paginator';

const hub = Hub;

let month_summary = new Vue({
    el: 'section#month_summary_page',
    data: {
        employees: window.employees_data,
        currentYear: window.conditions['year'],
        currentMonth: window.conditions['month'],
        conditions: window.conditions,
        employeeNames : window.employee_names,
        sendingRequest: false,
        canConclude: window.can_conclude,
        defaultConditions: {
            'presentation_id': null,
            'name': null,
            'work_status': 1, // 勤務中
            'only_show_considering' : false,
            'only_show_timestamp_error' : false,
            'only_show_confirm_needed_error' : false,
            'only_show_month_unconcluded' : false,
        },

        // Paginator
        currentPage: window.current_page,
        perPage: window.per_page,
        total: window.total,

        // Employee work statuses
        employeeWorkStatuses: window.employee_work_statuses,
    },
    computed: {
        yearRange: function() {
            return _.range(this.currentYear - 5, this.currentYear + 6);
        },
        calculatedStartDate: function() {
            let startDate = null;
            if (this.conditions['salary_accounting_day'] == 0) {
                startDate = moment(this.currentYear + '-' + this.currentMonth + '-1', 'YYYY-M-D');

            } else {
                startDate = moment(this.currentYear + '-' + this.currentMonth + '-' + this.conditions['salary_accounting_day'], 'YYYY-M-D');
                startDate.subtract(1, 'months');
                startDate.add(1, 'days');
            }
            return startDate;
        },
        startDateString: function() {
            let startDate = this.calculatedStartDate;
            return startDate.format('YYYY年 M月 D日') + '(' + this.japaneseDayOfWeek(startDate.day()) + ')';
        },
        calculatedEndDate: function() {
            let endDate = null;
            if (this.conditions['salary_accounting_day'] == 0) {
                endDate = moment(this.currentYear + '-' + this.currentMonth + '-1', 'YYYY-M-D');
                endDate.add(1, 'months');
                endDate.subtract(1, 'days');

            } else {
                endDate = moment(this.currentYear + '-' + this.currentMonth + '-' + this.conditions['salary_accounting_day'], 'YYYY-M-D');
            }
            return endDate;
        },
        endDateString: function() {
            let endDate = this.calculatedEndDate;
            return endDate.format('YYYY年 M月 D日') + '(' + this.japaneseDayOfWeek(endDate.day()) + ')';
        },

        summaryDownloadLink: function() {
            let url = "/download_summary";
            return $.companyCodeIncludedUrl(url);
        }
    },
    methods: {
        yearChanged: function(){
            this.submit(this.currentPage);
        },
        monthChanged: function(){
            this.submit(this.currentPage);
        },
        nextMonth: function() {
            this.currentMonth++;
            if(this.currentMonth > 12) {
                this.currentYear++;
                this.currentMonth = 1;
            }

            // The search result from  the same condition in a work location will be the same regardless of the business month.
            // So it's safe to assume the pagination's result will be the same => the number of pages will be the same.
            // So it's safe to use the current page.
            this.submit(this.currentPage);
        },
        preMonth: function() {
            this.currentMonth--;
            if(this.currentMonth < 1) {
                this.currentYear--;
                this.currentMonth = 12;
            }

            // The same reason above
            this.submit(this.currentPage);
        },

        resetConditions: function(){
            Object.assign(this.conditions, this.defaultConditions);
            hub.$emit('reset-autocomplete');
        },

        // Paginator
        changePage: function(newPage) {
            // this.submit(newPage);
            let url = '/month_summary/0/' + newPage;
            window.location.href = $.companyCodeIncludedUrl(url);
        },

        urlToWorkingMonthPage: function(employeeId) {
            let url = '/employee_working_month/' + employeeId + '/' + this.currentYear + '-' + this.currentMonth;
            return $.companyCodeIncludedUrl(url);
        },

        // Conclude functions
        concludeLevelTwo: function(employeeId) {
            var conf = confirm('こちらの従業員の「' + this.currentYear + '-' + this.currentMonth + '」分を本当に締めますか？');

            if (!!conf) {
                let url = '/conclude_level_two/' + employeeId + '/' + this.currentYear + '-' + this.currentMonth;
                window.location.href = $.companyCodeIncludedUrl(url);
            }
        },
        unconcludedLevelTwo: function(employeeId) {
            var conf = confirm('こちらの従業員の「' + this.currentYear + '-' + this.currentMonth + '」分を本当に解除しますか？');

            if (!!conf) {
                let url = '/unconcluded_level_two/' + employeeId + '/' + this.currentYear + '-' + this.currentMonth;
                window.location.href = $.companyCodeIncludedUrl(url);
            }
        },

        // Send request
        submit: function(page) {
            if (this.sendingRequest === false) {
                this.sendingRequest = true;

                let url = '/month_summary';

                if (!!page) {
                    url += '/' + _.toInteger(page);
                }

                // Re-assign date data
                this.conditions['month'] = this.currentMonth;
                this.conditions['year'] = this.currentYear;
                this.conditions['start_date'] = this.calculatedStartDate.format('YYYY-MM-DD');
                this.conditions['end_date'] = this.calculatedEndDate.format('YYYY-MM-DD');

                let data = {
                    'conditions': this.conditions,
                };

                axios.post($.companyCodeIncludedUrl(url), data).then(response => {
                    this.employees = response.data.employees_data;
                    this.currentPage = response.data.current_page;
                    this.perPage = response.data.per_page;
                    this.total = response.data.total;
                    this.canConclude = response.data.can_conclude;

                    // Toggle the concluded/unconclude all buttons
                    if (this.canConclude == true) {
                        if (response.data.all_concluded == true) {
                            $('a#unconclude_all_button').removeClass('hidden');
                            $('a#conclude_all_button').addClass('hidden');

                        } else {
                            $('a#conclude_all_button').removeClass('hidden');
                            $('a#unconclude_all_button').addClass('hidden');
                        }
                    } else {
                        $('a#unconclude_all_button').addClass('hidden');
                        $('a#conclude_all_button').addClass('hidden');
                    }

                    this.sendingRequest = false;
                })
                .catch(error => {
                    if (error.response) {
                        document.caeru_alert('error', '');
                    }
                    this.currentPage = 1;

                    // Reset blocking multiple request
                    this.sendingRequest = false;
                });
            }
        },

        employeeNameSelected: function(id) {
            this.conditions['name'] = this.employeeNames[id].name;
        },
        employeeNameChanged: function(value) {
            this.conditions['name'] = value;
        },
        japaneseDayOfWeek: function(dayIndex) {
            switch (dayIndex) {
                case 0:
                    return '日';
                case 1:
                    return '月';
                case 2:
                    return '火';
                case 3:
                    return '水';
                case 4:
                    return '木';
                case 5:
                    return '金';
                case 6:
                    return '土';
                default:
                    return null;
            }
        },

        // Download Summary Data
        downloadSummaryData: function() {
            $('#downloadSummaryForm').submit();
        },
    },
    components: {
        autocomplete: Autocomplete,
        paginator: Paginator,
    },
});

$('a#conclude_all_button').click(()=> {
    var conf = confirm('本当に全てのデータを締めますか？');

    if (!!conf) {
        let currentMonth = $("input[type='hidden'][name='current_month']").val();
        let currentYear = $("input[type='hidden'][name='current_year']").val();
        let url = '/month_summary_all_concluded/' + currentYear + '-' + currentMonth;

        $('#globalLoader').show();
        window.location.href = $.companyCodeIncludedUrl(url);
    }
});

$('a#unconclude_all_button').click(()=> {
    var conf = confirm('本当に全てのデータを解除しますか？');

    if (!!conf) {
        let currentMonth = $("input[type='hidden'][name='current_month']").val();
        let currentYear = $("input[type='hidden'][name='current_year']").val();
        let url = '/month_summary_all_unconcluded/' + currentYear + '-' + currentMonth;

        $('#globalLoader').show();
        window.location.href = $.companyCodeIncludedUrl(url);
    }
});
import Hub from '../components/hub.js';
import WorkingInfo from '../components/employee_working_info_component';
import Autocomplete from '../components/caeru_autocomplete';
import ErrorDisplay from '../components/caeru_error_display';
import Calendar from '../components/caeru_calendar.vue';

const hub = Hub;

var approve_employee_working_day = new Vue({
    el: 'main#request_page',
    data: {
        currentDate: window.array_date,
        scheduleTransferData : window.schedule_transfer_data,
        workLocations: window.work_locations,
        allWorkStatuses: window.all_work_statuses,
        allRestStatuses: window.all_rest_statuses,
        timezone: window.timezone,
  
        // Timestamp section
        timestamps: window.working_timestamps,
        timestampTypes: window.timestamp_types,
        timestampPlaces: window.timestamp_places,
        newTimestamp: {
            enable: false,
            processed_date_value: null,
            processed_time_value: null,
            timestamped_type: null,
            work_location_id: null,
            work_address_id: null,
        },
        timestampFormErrors: {
            enable: null,
            processed_date_value: null,
            processed_time_value: null,
            timestamped_type: null,
            work_location_id: null,
            work_address_id: null,
        },
        showTimestampForm: false,

        // General purpose
        daysOfWeek: ['日', '月', '火', '水', '木', '金', '土'],
        sendingRequest: false,
        newWorkingInfoCount: 0,
        showDatePicker: false,
        datePickerOptions: null,
        datePickerTimeNavigationData: null,
        datePickerData: {
            flipColorDay: window.flip_color_day,
            restDays: window.rest_days,
            nationalHolidays: window.national_holidays,
        },

        calendar_rest_day_consts: {
            LAW_BASED_REST_DAY  : 1,
            NORMAL_REST_DAY     : 2,
            NOT_A_REST_DAY      : 0,
        },
    },
    computed: {
        currentDateInArray: {
            get: function() {
                return (this.datePickerTimeNavigationData === null) ? _.map(_.split(this.currentDate, '-'), (value) => {return _.toInteger(value)}) : this.datePickerTimeNavigationData;
            },
            set: function(data) {
                this.datePickerTimeNavigationData = data;
            },
        },
    },
    methods: {
        // validate the time format type
        validateTimeFormat: function(string) {
            if (string.indexOf(':') === -1) {
                let temp = moment(string, 'Hmm', true);
                if (temp.isValid())
                    return temp.format('H:mm');
            } else {
                let temp = moment(string, 'H:mm', true);
                if (temp.isValid())
                    return temp.format('H:mm');
            }
            return false;
        },

        // Get the string of the date in japanese
        formatDate: function(date_string) {
            if (!!date_string) {
                let date = new Date(date_string);
                return date.getFullYear() + '年' + (date.getMonth()+1) + '月' + date.getDate() + '日(' + this.daysOfWeek[date.getDay()] + ')';
            }
        },

        // These function are for the date navigation panel
        nextDay: function() {
            let currentDate = new Date(this.currentDate);
            currentDate.setDate(currentDate.getDate()+1);
            return this.setDateToLink(currentDate);
        },
        previousDay: function() {
            let currentDate = new Date(this.currentDate);
            currentDate.setDate(currentDate.getDate()-1);
            return this.setDateToLink(currentDate);
        },
        setDateToLink: function(date_object) {
            let link_parts = _.split(window.location.pathname, '/');
            return _.join(_.initial(link_parts), '/') + '/' + date_object.getFullYear() + '-' + _.padStart((date_object.getMonth()+1), 2, '0') + '-' + _.padStart(date_object.getDate(), 2, '0');
        },


        // show the newTimestamp form
        createTimestamp: function() {
            this.showTimestampForm = !this.showTimestampForm;
        },

        // Get the next day date_string
        tomorrow: function() {
            return moment(this.currentDate, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD');
        },

        placeSelected: function(index) {
            if (index !== null) {
                this.newTimestamp.work_location_id = this.timestampPlaces[index]['work_location_id'];
                this.newTimestamp.work_address_id = this.timestampPlaces[index]['work_address_id'];
            }
        },

        // DatePicker
        toggleDatePicker: function() {
            this.showDatePicker = !this.showDatePicker;
            if (this.showDatePicker === true) {
                this.$nextTick(function() {
                    this.repositionByHeight();
                });
            }
        },
        datePickerChangeTime: function(year, month) {
            this.datePickerTimeNavigationData = [year, month];
            this.processDatePickerOptions();
        },
        goToThisDay: function(date) {
            let urlParts = _.split(window.location.href, '/');
            urlParts.pop();
            urlParts.push(date);
            window.location.href = _.join(urlParts, '/');
        },
   
        processDatePickerOptions: function() {
   
            this.datePickerOptions = {
                'year' : this.currentDateInArray[0],
                'month' : this.currentDateInArray[1],
                'nationalHolidays' : this.datePickerData['nationalHolidays'],
                'restDays' : this.datePickerData['restDays'],
                'flipColorDay' : this.datePickerData['flipColorDay'],
                'startColor' : (this.currentDateInArray[1] % 2) === 0,
                'pickerMode' : true,
            }
        },
        repositionByHeight: function() {
            let popUp = $('.normal_date_picker .caeru_calendar_date_picker_popup');
            let diffHeight = window.innerHeight - popUp.outerHeight();
            let diffWidth = window.innerWidth - popUp.outerWidth();
            let scrollOffset = $(window).scrollTop();
            popUp.offset({ top: (scrollOffset + diffHeight/2), left: diffWidth/2 });
        },
    },
    created: function() {
        this.processDatePickerOptions();
    },
    mounted: function() {
        this.repositionByHeight();
        this.$nextTick(function() {
            window.addEventListener('resize', this.repositionByHeight);
        });
    },
    components: {
        'working-info' : WorkingInfo,
        'autocomplete' : Autocomplete,
        'error-display' : ErrorDisplay,
        'calendar': Calendar,
    }
})